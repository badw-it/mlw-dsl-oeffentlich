LEMMA
satisfactio (satis factio, -ccio)

GRAMMATIK
subst. III; -onis f.

SCHREIBWEISE
    script.:
        -ficat-:
            * CHART. Neocell. Brixin. ; 89
        -atio:
            * {=> fati}{=>satisfacio/satisfatio}
        -acio:
            * {=> satisfacio}{=> satisfacio/satisfacio}
        -ciacio:
            * {=> satisfacio/satisfaciacio}
              
        
BEDEUTUNG
actus satisfaciendi -- Genugtuung, Zufriedenstellung:

U_BEDEUTUNG
gener.:

UU_BEDEUTUNG
strictius:

UUU_BEDEUTUNG
in univ.:

* RADBERT. corp. Dom. ; 14,20 "quaerat quilibet ob #satisfaction.|em sui, quid expectet."
* ANNAL. Fuld. Ratisb. ; a. 883 p. 109,27 "cum ... longius victurus putatus est {(sc. papa)}, quam eorum {(sc. iniquorum)} #satisfacti.|o esset cupiditati."
* LIBELL. de Willig. ; praef. p. 743 "spes ... obeditionis dignae vires mihi ... promiserat #satisfaction.|is implendae."

ANHÄNGER
    de cultu, reverentia, obsequio (‘Huldigung’):
* ARBEO Corb. ; 20 "pape Gregorii stravit se vestigiis; #satisfaction.|e surgere imperans sedere sibi iuxta se deposita praecipiens {(v. notam ed.)}."

UUU_BEDEUTUNG
excusatio, expurgatio, defensio -- Entschuldigung, Abbitte, Rechtfertigung:
* EPIST. Col. ; 10 p. 254,30 "quod ... nullo modo liceat eis {(sc. archiepiscopis depositis)} nec in alia sinodo restitutionis spem aut locum habere #satisfaction.|is ((* {inde} ANNAL. Fuld. II ; a. 864 (add. 3)))."
* VITA Mathild. I; 6 "nec mora pacis ad reconciliationem #satisfaction.|e percepta dotalem regni partem concessit."
* II ; 14 p. 171,10 "comperiens ... Heinricus iuvenis ... regem Ottonem suum fratrem tanta #satisfaction.|e inclitam sibi reconciliasse matrem {eqs.}"
* ARNOLD. LUB. chron. ; 7,20 "#satisfacti.|o scriptoris: veniam legentium peto, ne quis presumptionis vel temeritatis arguere me velit {eqs.}"

UU_BEDEUTUNG
latius:

UUU_BEDEUTUNG
persuasio, informatio -- Überzeugung, Information:

* COD. Karol. ; 21 p. 524,6 "pro vestra {(sc. regis)} amplissima #satisfaction.|e adprobationem fecimus in praesentia ... vestrorum fidelium missorum ..., et satisfacti {@ satisfacti} sunt vestri missi de tantis iniquitatibus."
* 58 p. 583,32 "pro magna sublimitatis vestre {(sc. regis)} #satisfaction.|e ... affatos infra haec nostra scripta vobis directa posuimus." {al.}

UUU_BEDEUTUNG
ratio -- Rechenschaft:

* {v.} {=> satisfacio/ratio}

U_BEDEUTUNG
publ. et iur.:

UU_BEDEUTUNG
spectat ad pecuniam solvendam, debitum dissolvendum, tributum dandum sim.:

* LEX Baiuv. ; 16,4 "quicquid ad conparatę rei profectum ... emptor adiecerit, a ... iudicibus aestimetur, et ei, qui laborasse cognoscitur, a venditore iuris alieni #satisfacti|o (#satisfa.|cio, {@ satisfacio} #satisfa.|c͞co, #satis facios, satisfacto {var. l.}) iusta reddatur."
* FORM. Senon. I ; 3 "quod si ... tardus ... apparuero ..., sine ullo iudice interpellationis pro duplum {(sc. mutui)} #satisfaction.|e me reteneor debitore."
* CHART. Rhen. med. ; III 286 p. 232,13 (a. 1226) "in manus fratrum Everbac eadem bona resignaverunt {Engelscalcus et Nantoch} pacem et #satisfaction.|em annualem pro ipsis bonis ... promittentes."

UU_BEDEUTUNG
spectat ad iniuriam, culpam expiandam, delictum expiandum, detrimentum sanandum sim.:

UUU_BEDEUTUNG
proprie:

* LIUTG. Greg. ; 9 p. 74,24 "ut ad #satisfaction.|em et mitigationem doloris sui, quali vellet morte, ipse eos {(latrones)} interfici iuberet."
* LEX Frision. ; 9,8 "componat {(sc. violator)} ei {(sc. puellae virgini)} weregildum eius ... ad #satisfaction.|em et ad partem regis similiter."
* CHART. Tirol. ; I 552 p. 33,20 (spur. a. 1204) "donec ad plenam venerit {offensor} #satisf.|ationem {@ fati} hiis, quibus offensio facta fuerit."
* CHART. Stir. ; II 49 p. 83,15 "sive mortem hominis exigat {causa} vel #satisfaction.|em pecuniariam requirat." {saepius.}

ANHÄNGER
adde de spontanea actione penitentiae, pacifica compositione litis, deditione 
 ((* {de re v.} G. Althoff, op. cit. [{=> satisfacio/althoff}])):

* THANGM. Bernw. ; 25 "ratione imperatoris ... compuncti #satisfaction.|em promittunt {Romani} {eqs.}"
* LEX fam. Worm. ; 8 "si quis ... alicui ... aliquid iniustitie fecerit, ius erit familie, ut se ... et suos viros una #satisfaction.|e reconciliet {eqs.}"
* BERTH. chron. B ; a. 1073 | p. 217,14 "prevenientes eum {(regem)} Saxones #satisfaction.|em illi, si iustitias maiorum suorum illis concederet, ... promittebant." 
* p. 217,18 "regi falsam denuo #satisfaction.|em in natali Domini se facturos ... condixerant Saxones."
* CHART. Verd. ; 314 "episcopo ... ad #satisfaction.|em pro quibusdam excessibus, quos homines nostri {(sc. ducis)} fecerunt tempore gwerre, dedimus {eqs.}" {saepe.}

UUU_BEDEUTUNG
meton de rebus satisfaciendo datis:

* CHART. Advoc. ; 66 p. 31,42sqq. (a. 1237) "in omnibus ..., in quibus nobis {(sc. abbatissae)} vel nostris aliqua fuerit iniuria irrogata, de qua aliqua #satisfacti.|o competit advocato, #satisfaction.|em nobis vel nostris faciet exhiberi, antequam pro se aliquam ipse #satisfaction.|em acceptet."
* REGISTR. Patav. ; I p. 31,16 "hec est #satisfacti.|o, per quam Ludwicus de Hagenow est Pataviensi ecclesie reformatus pro iniuriis irrogatis: in Imelcheim molendinum {eqs.}"

U_BEDEUTUNG
eccl. et theol. spectat ad paenitentiam; c. gen. explicativo: {=> genexplic}:

* RIMB. Anscar. ; 18 p. 39,22 "pro reatu commissi ... Christo voluntariam #satisfaction.|em vovit {potens}."
* ANNAL. Fuld. II ; a. 867 "eam {(sc. Waldratam)} ... a consortio sanctae aecclesiae usque ad poenitentiae #satisfaction.|em {@ genexplic} sequestravit {Nicolaus}."
* ADSO Frodob. ; 7 ((MGMer. V ; p. 77,1)) "pro facti temeritate debitae #satisfaction.|is penitudinem indicit."
* HIST. Welf. ; 13 p. 462,13 "volens Deo excessibus suis difficiliorem #satisfaction.|em exhibere Hierosolimitanum iter arripuit {Guelfo}."
* BURCH. URSB. chron. ; p. "116,10 multi pessimi sine penitentia et #satisfaction.|e mortui." {persaepe. v. et {=> satisfacio/satisfactio}.}

ANHÄNGER
de ipsa confessione peccati:
* RUD. FULD. Leob. ; 5 "ad pedes eius {(abbatissae)} provoluta est {soror} eique culpam neglegentiae suae humili #satisfaction.|e patefecit."

U_BEDEUTUNG
canon.:
* BRUNO MAGD. bell. ; 64 p. 56,26 "ubi {(sc. in concilio)} episcopi ... iniuriarum ... canonicam reciperent #satisfaction.|em."


AUTOR
Staub

UNTERDRÜCKE WARNUNG 525