LEMMA 3. *scario

GRAMMATIK 
    verbum IV; -ivi, -itum, -ire

ETYMOLOGIE *scara
            {et} theod. vet. scerian: * {de re v.} Stotz, Handb.; 2,VI § 109.5
            * A. de Sousa Costa, op. cit. {[{=> scara1/2scario_6}]}; p. 274sq. 

SCHREIBWEISE
    script.:
        es-: {=> 3scario_1}
        is-: {=> 3scario_3}
        excarri-: {=> 3scario_2}
        sacr-: {=> 3scario_4}

GEBRAUCH
    partic. perf. usu adi.: {=> 3scario_2; 3scario_5b}
    in coniug. periphrastica: {=> 3scario_5a}


BEDEUTUNG (in scaram q. d., turmam) conscribere, eligere, (scarae q. d., turmae) attribuere -- (in eine Truppe, Schar) einberufen, ausheben, (einer Truppe, Schar) zuteilen:

    * CAPIT. reg. Franc.; 54,2 "ut medio mense Augusto cum #excarritis {@ 3scario_2} hominibus ad nos esse debeant {(sc. episcopi, abbates, comites)}."; 281,17 "si ipse {(comes palatii)} pro aliqua necessitate defuerit, ... unus eorum, qui cum eo #scari.|ti sunt, causas teneat."


BEDEUTUNG scara q. d., turma instruere -- mit einer Truppe, Schar ausrüsten:

    * ACTA imp. Winkelm.; I 33 p. 26,14 (dipl. Otton. IV. a. 1209) "asseveravit ... marchio ... consulibus {(sc. communis Saonae)}, quod domini Albuzole ... ei facere debent fidelitatem, et quod ei munito seu #scari.|to {@ 3scario_5b} eam partem, quam habet in Albuzola veture, non debent, quin inde guerram et pacem, cui voluerit, faciat."


BEDEUTUNG statuere -- anordnen, bestimmen:

    * CAPIT. reg. Franc.; 69,9 "ut marca nostra secundum, quod ordinatum vel #scari.|tum {@ 3scario_5a} habemus, custodiant {(sc. episcopi)} una cum missis nostris."; 278,4 "ut fidelitatem nobis promittant {(sc. homines Carlomanni)}, sicut tunc #scari.|vimus et scriptam comitibus nostris dedimus."



SUB_LEMMA *scaritus

GRAMMATIK
    subst. II; -i m.

BEDEUTUNG miles (scarae q. d. attributus) -- (einer Schar zugeteilter) Soldat; plur. fere i. q. scara q. d. -- Truppe, Schar:

    * CHRON. Fred.; 4,37 p. 138,9 "Theudericus cum #e|.scarit.|us {@ 3scario_1} (#scarit.|us, #scarit.|os, #scarit.|is, #i|.scarit.|is {@ 3scario_3}, #sacritus {@ 3scario_4} {var. l.}) ... decem milia accessit."

    * CHRON. Fred. cont.; 52 "Pippinus in quattuor partes comites suos, #scarit.|os et leudibus suis ad persequendum Waiofarium transmisit."

    * CAPIT. reg. Franc.; 216,5 "ni generalis exigat utilitas, <ut> cum #scarit.|is veniat, in statutis {(sc. locis)} iuxta domibus maneat {(sc. fidelis)}."


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 525