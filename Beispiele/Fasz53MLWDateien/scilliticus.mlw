LEMMA scilliticus (squi-)

GRAMMATIK
    adi. I-II; -a, -um

ETYMOLOGIE
    σκιλλητικός, σκιλλιτικός

SCHREIBWEISE
    script.:
        essillat-: {=> scilliticus_1}
        squili-: {=> scilliticus_5}
        scelet-: {=> scilliticus_3}
        scelest-: {=> scilliticus_4}
        scili-: * ANTIDOT. Glasg.; p. 147,28
        -let-: {=> scilliticus_2a; scilliticus_2b; scilliticus_2c}
        -icito: {=> scilliticus_6}


BEDEUTUNG qui scillae est -- Meerzwiebel-:

    UNTER_BEDEUTUNG iunctura "bulbus #scillitic.|us, cepe #scillitic.|um":
    
        UU_BEDEUTUNG scilla -- Meerzwiebel:

            * GLOSS. med.; p. 75,15 "scilla aizon sive stichias Greci dicunt, quod a Latinis bulbi #scill.|etici {@ scilliticus_2b} (#sceles|.tic.|i {@ scilliticus_4} {var. l.}) dicuntur ((* {cf.} André, op. cit. ({=> scilla1/scilliticus_7})))."

            * DYNAMID. Hippocr.; 2,97 "#scillitic.|i bulbi medium, quod est album, ... in oxymelle coctum ... dato."

            * WILH. SALIC. chirurg.; 1,3 p. 305^B "fricetur cutis ... cum coepis vel cum alliis vel synapi vel coepe #squ|.illitic.|o."

        UU_BEDEUTUNG ?Pancratium maritimum {L.} -- ?(Dünen-)Trichternarzisse: 

            * GLOSS. med.; p. 51,8 "panatrion ({leg.} pancration): bulbi #scill.|etici {@ scilliticus_2a} (#scele|.tic.|i {@ scilliticus_3} {var. l.})."

    UNTER_BEDEUTUNG scilla commixtus, confectus -- mit Meerzwiebel versetzt, aus Meerzwiebel hergestellt:

    * AURELIUS; 25 p. 134,11 "potum dato {!acetum} #scill.|eticum {@ scilliticus_2c} ((* ANTIDOT. Glasg.; p. 109,9 "cocliarios III aceti #ess|.ill.|atici {@ scilliticus_1}." * THADD. FLORENT. cons.; 138,16 "cum ... aceto #squili|.tic.|o {@ scilliticus_5}." {saepe}))."

    * PAUL. AEGIN. cur.; 54 p. 31,22 "{!oximel} #scillitic.|um ((* $PAUL. AEG.; 3,13,2 "σκιλλητικόν")) cotidie potatum ((* CIRCA INSTANS; p. 7^{a}r "fit o. #squ|.illitic.|um hoc modo: {eqs.}" * WALTH. AGIL. med.; 3 p. 90,18 "cum oximelle #squilli.|cito {@ scilliticus_6}." {al.}))."

    * TRACT. de aegr. cur.; p. 133,13 "{!siropum} #squ|.illitic.|um dabis ((p. 302,13sq. "da diacostum cum siropo #squ|.illitic.|o ... vel dragagantum cum siropo #squ|.illitic.|o"))."

AUTORIN Niederer

UNTERDRÜCKE WARNUNG 525