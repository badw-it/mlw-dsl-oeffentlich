LEMMA  scabellum (-bil-)

GRAMMATIK  substantivum II; -i n.

BEDEUTUNG  (parvum) scamnum (suppedaneum) -- (Fuß-)Bank, Bänkchen, Schemel(chen):

    U_BEDEUTUNG  proprie:
    
        UU_BEDEUTUNG  in univ.:

            UUU_BEDEUTUNG  usu communi ((per compar.: {=> scabellum003}{=> scabellum004})):

                * AETHICUS; 21^{b},4 "si deorsum aspicias a superius, nubes tamquam #scab.|illum {@ scabellum003} pedum credas."

                * PAUL. DIAC. Lang.; 2,28 p. 88,19 "adprehenso ... #scabell.|o (#scab.|illo {var. l.}) subpedaneo se ... defendit {Arboin}."

                * HRABAN. univ.; 22,11 p. 608^{A} "#scabell.|um ... et suppedaneum dicitur, nam Graeci upopodion dicunt, Latini #scabell.|um."

                * RATHER. prael.; 5,10 l. 297 "#scabell.|um tapete Gothico tectum."

                * CHRON. rhythm. Col.; 5,13 "antea qui suberant, pedibus quasi pressa #scabell.|a {@ scabellum004} nunc hiis, qui preerant, audent indicere bella."

                * WILH. HIRS. const.; 1,20 p. 314,3 "sub #scabell.|o, quod est ante lectum, debent servari {(sc. vestes exutae)}."

                * HILDEG. phys.; 1,111 p. 117,3 "cum ... balneum intraverit, super #scabell.|um herbas calidas ponat et desuper sedeat {mulier}."

                * ALBERT. M. animal.; 2,33 "cornu ... equicervi est latum valde sicut asser, ita quod #scabell.|a fiunt ex ipso." {persaepe.}

                    ANHÄNGER  alleg.:

                        * HRABAN. univ.; 1,1 p. 19^{B} "#scabell.|um Domini homo assumptus a Verbo; ... item #scabell.|um est humiliatorum subiectio, ut in psalmo '{(Vulg. psalm. 98,5)}'."

            UUU_BEDEUTUNG  proverb.:

                * WIBALD. epist. I; 305 p. 645,16 "permittit {coabbas}, ut vulgo dicitur, '#scabell.|a {@ scabellum002} in scamna ascendere' {(v. notam ed.)}."
    
            UUU_BEDEUTUNG  per compar. de terra, templo sim. loco habitationis Dei (({loci spectant ad} $VULG. Is.; 66,1. al.)):

                * THEOD. TREV. phys.; 1890 "cuius {(Dei)} sedile caelum extat, terra #scabell.|um."

                * ALBERT. M. Is.; 66,1 | p. 620,35sqq. "#scabell.|um sub ima calcatum est et terra faex elementorum ad ima detrusa et longissima distantia a nobilitate caelestium bonorum elongata; propter hoc #scabell.|um dicitur."; p. 620,60sqq. "#scabell.|um etiam aliquando dicitur, ubi apparet vestigium, quo venit {Deus} ad bonos terrenos, sicut templum dicitur #scabell.|um, quia per gratiam, quae dispensatur in templo, venit ad inhabitandum cor terrenum." {ibid. al.}

        UU_BEDEUTUNG  de sustentaculis infirmorum:

            * VITA Pard. 10; ((MGMer. VII; p. 31,1)) "claudus quidam ..., qui ab utero matris sue ... nihil aliud nisi cum #scabell.|is (scamellis {var. l.}) gradiebatur."

            * ANSCAR. mirac. Willeh.; 11 (10) "quae {(femina)} ... ita constracta extitit, ut ... nec quoquam ire nisi cum #scabell.|o trahendo posset."

            * VITA Rigob.; 20 ((MGMer. VII; p. 74,13)) "priscae debilitatis inditia, videlicet bacilli et #scabell.|a."

            * THIETM. chron.; 7,3 p. 402,10 "quodam viro diu debili et cum #scabell.|is diu ambulanti tribuit ... Deus facilem gressum."

            * UDALSC. Adalb.; p. 8,23 "gressuum officio carens non nisi #scabell.|is reptans in terra movebatur {claudus quidam}." {al.} {v. et} {=> scamellum/scamellum003}{=> scamellum/scamellum004}{=> scamellum/scamellum005}

        UU_BEDEUTUNG  de fulcimento pro oratione adhibito:

            * CATAL. thes. Germ.; 43,19 (a. 1025) "sellę sex cum #scabell.|is."

            * WILH. HIRS. const.; 1,9 p. 270,15 "quando ... cuncti {(sc. fratres)} debent esse inclines longiori mora ..., isti ... sedent super scamna; ... quando illi super formas, isti, qui stant ante formas, super #scabell.|a ante se posita iacent."

            * CONSUET. Mell.; 33 "loti ... procedant {(sc. fratres)} ad ecclesiam aspersique aqua benedicta prosternant se ... in choro super #scabell.|a."

        UU_BEDEUTUNG  de auxilio ad equos ascendendos adhibito:

            * THEOPH. sched.; 1,22 "#scabell.|a et caetera, quae sculpuntur et non possunt corio ... cooperiri."

        UU_BEDEUTUNG  de instrumento cruciandi:

            * WALTH. SPIR. Christoph. II; 6,121 "bis sex ulnarum iussit {Pyragmon} conflare #scabell.|um."

                ANHÄNGER  spectat ad visionem Christi crucifixi:

                    * OLIV. epist.; 2 p. 31,36 "prout visu cerni poterat, corpus ac manus ... apparebant ..., clavi in pedibus et manibus affixi crucis patibulo, #scabell.|um (scabulum {var. l.}) sub pedibus {eqs.}"

        UU_BEDEUTUNG  de pulpito:

            * CATAL. thes. Germ.; 136,98 (a. 1105/1246) "retro capellam super #scabell.|um sunt octo texti aurei."

        UU_BEDEUTUNG  in stamine ad telam texendam:

            * CONR. MEND. Attal.; p. 135,32 "sive ... fila nova litiis intricatis connecteret {mulier} sive #scabell.|a nimio terram attingentia alternatis pedibus elevando et deprimendo calcaret, ... omnia ... in nihilum cedebant."

    U_BEDEUTUNG  in imag.:

        * EPIST. Worm. I; 42 p. 78,1 "archidilecto magistro suo E. E. #scabell.|um emulorum votivum servitutis donum."

ABSATZ

BEDEUTUNG  de mensura vineae i. q. diaeta, iugerum -- 'Tagwerk', 'Morgen' (({de re v.} {@ scabellum005} * O. Gönnenwein, ZRGGerm. 1963.; p. 172)):

    * CHART. Bern.; I 4,152 p. 367,10 (a. 1115?) "#scabell.|um vinee in Clauso de Balgest {(sc. donavit Lambertus)}."

    * CHART. Rhen. med.; I 456 p. 514,1 "trado {(sc. Ioannes sacerdos)} ... in iugo Ugonis iurnalis et dimidius ..., in Sleika olka una in media villa, in medio montis tria #scabell.|a; item octo trilę cum uno #scabell.|o, item VI trilę cum uno #scabell.|o, item duo #scabell.|a, iuxta Ensce duo #scabell.|a."

VERWEISE  scamnellum

AUTOR  Fiedler