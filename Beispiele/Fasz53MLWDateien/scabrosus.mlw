LEMMA  scabrosus

GRAMMATIK  adiectivum I; -a, -um

SCHREIBWEISE

    script.:

        scra-: {=> scabrositas/scabrosus006}{=> scabrosus002}{=> scabrosus003}{=> scabrosus007} {al.}

STRUKTUR

    usu subst.: {=> scabrosus007}{=> scabrosus004}

BEDEUTUNG  proprie ((in imag.: {=> scabrosus008})):

    U_BEDEUTUNG  inaequalis, asper -- uneben, rau:

        * SEDUL. SCOT. carm.; II 55,7 "mites agnos #scabros.|o dente ... rodens: 'vos' inquit 'rabidos valde cavete lupos'."

        * VITA Bav. II; 2,221 "permittit ... famem corpus sentire frequentem undique #scabros.|is nimio pre frigore menbris."

        * WALTH. SPIR. Christoph. II; 4,37 "sentit ... ingratam #scabros.|a rubigo repulsam."

        * THEOD. THEOL. Conr.; 4 p. 216^{b},52 "per devexa #scabros.|i (#scr|.abrosi {@ scabrosus002} {var. l.}) montis (({sim.} * PETR. CAS. [?] chron.; 4,44 p. 512,20 "declivi montis #scabros.|a [#scr|.abrosa {@ scabrosus003} {var. l.}] via equitatura concidens ... crurium fracturam passus est {Ugo miles}"))."

        * CHART. Aquens.; 55 l. 12 "predio attinebant tres iurnales terre #scabros.|e et inculte."

        * ALBERT. M. veget.; 6,256 "ulmus ... est arbor magnae quantitatis habens asperum corticem et #scabros.|um."; animal.; 1,120 "est ... os intus #scabros.|um et extra planum." {saepe.}

            ANHÄNGER  in imag.:

                * THEOD. TRUD. Bav.; 13 "magisteriali ungue #scabros.|am {@ scabrosus008} vitam polire."

            ANHÄNGER  usu subst. spectat ad elementa terrea:

                * ALBERT. M. animal.; 20,6 "similiter ... grossum, #scr|.abros.|um {@ scabrosus007} repugnant apprehensioni, qua {eqs.}"

    U_BEDEUTUNG  scabie laborans -- krätzig, räudig ((usu subst.: {=> scabrosus004})):

        * PETR. DAM. epist.; 40 p. 459,17 "leprosus leproso praebet aurum, quod profecto rutilat nulla #scabros.|arum (scabiosarum {@ scabrosus001} {var. l.}) manuum varietate perfusum."

        * HILDEG. fragm.; 2,45 "mali humores in homine erumpunt, ita quod homines ulceribus inplentur et pecora #scabros.|a, id est scebech, erunt."

            ANHÄNGER  spectat ad plantas i. q. scabidus -- schorfig:

                * ALBERT. M. veget.; 7,176 "omnia ... vetera et #scabros.|a {@ scabrosus004} et cortex pendens a vite rescindantur."

    U_BEDEUTUNG  impurus -- schäbig:

        * CONR. EBERB. exord.; 3,25 l. 33 "cum ... altare ... dirui praecepisset {famulus Dei}, reperit in eo capsulam #scabros.|am ac veterem, sanctorum reliquias continentem."

BEDEUTUNG  translate fort. i. q. periculosus -- viell.: gefährlich:

        * OTTO FRISING. gest.; 2,40 p. 148,37 "tanta erat saxi eminentia, tanta fuit hispidae rupis #scabros.|a malicia."

AUTOR  Fiedler