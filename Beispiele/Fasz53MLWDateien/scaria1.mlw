LEMMA 1. *scaria

GRAMMATIK
    subst. I; -ae f.

ETYMOLOGIE
    {retrograde ex} 2. *scario, *scarius {formatum, ut vid.}

SCHREIBWEISE
    script.:
        as-: {=> scaria_1} * {adde} CHART. Tirol. I; 1184 p. 231,3
        ess-: {=> scaria_2}
        scha-: {=> scaria_3}


ABSATZ
BEDEUTUNG (prima) sedes vilicationis episcopalis, curtis vilicalis -- (Haupt-)Sitz der bischöflichen Gutsverwaltung, Meierhof ((* {de re v.} H. v. Voltelini{@ scario_1}, Archiv f. österr. Geschichte. 94. 1907.; p. 368sqq.)) ((* N. Wagner {@ scario_2}, Histor. Sprachforschung. 109/1. 1996.; p. 127sqq.)):

    * CHART. Tirol. I; 435 p. 232,38 (a. 1188) "quas {(sc. Petri et Iohannis)} terras Armanus posuit in #scari.|am; et terra #scari.|e ... solvebat XV modios ficti {(v. ind. p. 387)}."

    * COD. Wang. Trident.; 1 p. 524,9 "comes ... iure ... pignoris investivit ... episcopum de #scari.|a sua de Nano."

    * REGISTR. Trident.; I 63 "quod {(vinum)} trahitur ... ad #sch|.ari.|am {@ scaria_3} ... canonicorum."

    * CHART. Tirol. I; 1035 p. 80,29 "in plebatu Piani ante #ess|.cari.|am {@ scaria_2} dominorum canonicorum Tridentinorum."; 1037 p. 83,14 "in curia #a|.scari.|e {@ scaria_1} canonicorum."

    * CHART. Tirol. notar.; I| 71^a p. 33,37 "in domo #scari.|e ... abbatis (({sim.} 86^{a}.; 105.; 201 p. 98,1 "in curte #scari.|e." {ibid. al.}))."

    {persaepe.}


AUTORIN Niederer