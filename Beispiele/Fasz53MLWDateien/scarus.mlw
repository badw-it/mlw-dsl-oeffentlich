LEMMA scarus

GRAMMATIK
    subst. II; -i m.

ETYMOLOGIE σκάρος

SCHREIBWEISE
    script.:
        es-: {=> scarus_2a; scarus_2b}
        (e)scaur(us): {=> scarus_1a; scarus_2b; scarus_1b}

FORM
    form.:
        nom. sg.:
            -ras: {=> scarus_4}
            -ros: {=> scarus_3}


BEDEUTUNG de pisce marino cibum delicatum aestimato i. q. Sparisoma cretense -- Europäischer Papageifisch ((* {de re v.} H. Leitner, Zool. Terminologie beim älteren Plinius. 1972.; p. 217sq.)):

    * IOH. DIAC. cen.; 2,148 "Agar #scaurum {@ scarus_1a} moecha tulit."

    * CARM. didasc.; 691 "non petit #e|.scar.|us {@ scarus_2a} (#escaurus {@ scarus_2b} {var. l.}) properans, quas ruminet, escas."
    
    * ALPHITA; S 98 "scorpena, #scar.|us (#scaurus, #scar.|as {@ scarus_4} {var. l.}), sepia nomina sunt piscium {(v. comm. ed. p. 535)}."

    * ALBERT. M. animal.; 7,25 "'qui {(piscis)} vocatur graece feroz ((* $ARIST.; p. 591^b,22 "σκάρος"))' et latine #scar.|us (#scar.|os {@ scarus_3} {a. corr.})."; 24,54 "#scaurus {@ scarus_1b} piscis delectabilis, qui solus ruminat inter pisces."

    {al.}

        ANHÄNGER per errorem de avi:

        * ALBERT. M. animal.; 8,89 "'avis ... iesar vocata ... habet fere quantitatem #scar.|i{@ scarus_5}' ((* {cf.} $ARIST.; p. 617^b,26sq. "ψάρος ... μέγεθος δ' ἐστὶν ἡλίκον κόττυφος"))."


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 525