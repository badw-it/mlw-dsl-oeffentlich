LEMMA
satureia
(-egia)

GRAMMATIK
subst. I; -ae f.

SCHREIBWEISE
   script.:
      satar-: {=> satareia}
      * {adde} FORMA mon. Sangall. ; 7,11; 43,14
      -ega: {=> saturega} 

BEDEUTUNG
Satureja L. -- Bohnenkraut, ‘Saturei’ ((* {cf.} I. Stirling, Lexicon nominum herbarum, arborum fruticumque linguae latinae. IV. 1998. ; p. 68sq.)):

* ANTIDOT. Lond. ; p. 20,42 "recipit {(sc. unguentum)} ... #sature|gia, idiosmu, balsamita."
* RECEPT. Lauresh. ; 1,2 "piperis albi ∻ I, gingiber ∻ II, #saturei|ae {!seminis} ∻ I ((* FRAGM. mul. III ; 47 "#sat|areia {@ satareia} simen." {al.}))"; 1,198 "#sature|giae fasciculum unum."
* ANON. herm. ; p. 104,14 "timbra, id est #saturei|a."
* ANTIDOT. Glasg. ; p. 119,31 "conficis {(sc. unguentum)} sic: #sature|ga {@ saturega} viride - VI {eqs.}"
* DYASC. ; p. 182^b "#sature|gia calefacit et desiccat nimis et extenuat manifeste, ideo urinam et menstrua movet."
* CHIRURG. Sudhoff ; II p. 434 "de #saturei|a (gloss.: id est hencolla) dimidium marcam."
* ALBERT. M. veget. ; 6,449 "#sature|gia est herba habens folia sicut ysopus {eqs.}"
* ALBERT. M. animal. ; 23,89 "accipe malvam et #satu|tregiam ({ed.;} #sature|giam {cod. Colon. W 258^a,396^v}) cum adipe porcino {eqs.}" {saepe. v. et {=> satyrus/satureia}}

AUTOR
Staub