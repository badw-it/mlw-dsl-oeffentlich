LEMMA
saturitas

GRAMMATIK
subst. III; -atis f.

BEDEUTUNG
satietas, saturatio -- Sattheit, Sättigung:

U_BEDEUTUNG
proprie:

* MIRAC. Richar. ; 2,1 ((MGScript. XV ; p. 918,6)) (s. IX.) "unde olim enormis educta fuerat #saturita.|s, inde nunc nigra egreditur fames."
* OTLOH. prov. ; M 47 "morbi plures ex #saturitat.|e nimia eveniunt."
* CONSTANT. AFRIC. theor. ; 6,29 p. 29b^v "post #saturitat.|em vel nimium coitum balneantes flegmate abundent, necesse est."
* HIST. peregr. ; p. 163,36 "omnes ... citra #saturitat.|em et nimis exiliter cenaverunt."
* ANON. coit. ; 2,4,12 "potacio cotidiana supra #saturitat.|em (#saturita|s {var. l.})."
* TRAD. Westph. ; IV p. 147,12 "inter duos {(sc. convivas)} ponetur siligo et simella et cervisia ad plenam #saturitat.|em." {al.}

U_BEDEUTUNG
in imag.:

* EPIST. var. I ; 40 p. 563,39 "uti fidei pensum, cuius est, cernas {(sc. Guarnerius)} ipsum munus inexaustum contuendo per secula divinum, #saturitat.|e xasa ((* sacra {ci.} A. Hauck, Kirchengesch. Deutschl. ^{8}II. 1954. ; p. 6083)) sat conspiciendo Deum."
* WALTH. SPIR. Christoph. ; I 1 "ut ... seculi ... delicias modesta, prout naturę necessitas exigebat, #saturitat.|e componeret."

U_BEDEUTUNG
meton.:

UU_BEDEUTUNG
in interpr. nominis:

* CHART. ord. Teut. (Hass.) ; 54 p. 52,30 (epist. papae a. 1235) "o dulcis Elisabeth, dicta Dei #saturita.|s ((* {ex} $HIER. nom. hebr. ; add. p. 140,3)), que pro refectione pauperum panem meruit angelorum."

UU_BEDEUTUNG
de cibo digesto:

* ALBERT. M. animal. ; 7,48 "egerit {lupus} duobus aut tribus diebus #saturitat.|em."


U_BEDEUTUNG
translate i. q. plenitudo, ubertas -- Fülle, Reichtum, Überfluss:

* GLOSS. psalt. Lunaelac. ; 36,20 "erit ({cod.}; erunt {ed.}) ... eorum, qui in paena futuri sunt fame non habentes, ... #saturitat.|em gloriae regni."
* EKKEB. SCHON. opusc. ; 1 p. 245,10 "magna pre cunctis mortalibus gratie #saturitat.|em assecuta es {(sc. Maria)}."
* HILDEG. epist. ; 373 l. 50 "qui {(sc. filius Dei)} esurie exspectationis sue cessante in magna #saturitat.|e gaudiorum in omnem terram exivit."
* HILDEG. div. op. ; 1,2,19 l. 7 "introduxit {cunctorum rector} me ... in plenitudinem donorum suorum, ubi omnem #saturitat.|em virtutum invenio."

AUTOR
Staub