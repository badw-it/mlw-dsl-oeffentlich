LEMMA scelestus

GRAMMATIK
    adi. I-II; -a, -um

VEL *scelestis

GRAMMATIK
   adi. III; -e

ETYMOLOGIE {forma} scelestis {per contam. c.} caelestis {formata, ut vid.}: * {cf.} Stotz, op. cit. [{=> sceleriter/Stotz_1}]; 4,VIII § 5.7

SCHREIBWEISE
    script.:
        exc- ((* {cf.} Stotz, Handb.; 3,VII § 84.2.; 85.4sq.)): {=> scelestus_3}
        c-: {=> scelestus_1a; scelestus_1b; scelestus_1c; scelestus_1d; scelestus_1e} {al.}

VERWECHSELBAR
    confunditur c.:
        excellens: {=> scelestus_5a; scelestus_5b}


BEDEUTUNG adi. i. q. sceleratus, nefarius, incestus, impius -- frevlerisch, verbrecherisch, schändlich,  gottlos, 'böse':

        * PIRMIN. scar.; 14 p. 44,3 "nihil est #ex|.celest.|ius {@ scelestus_3} (#c|.elest.|ius {@ scelestus_1a}, #scelest.|ius, escelentius {@ scelestus_5a}, excelentius {@ scelestus_5b} {var.l.}) quam amare pecuniam ((* {spectat ad} VET. LAT. Sirach; 10,9sq.)) {(v. notam ed.)}."

        * DONAT. Trud.; 32 ((MGMer. VI; p. 294,36)) "pater illius pueri, qui ... servum interfecit, ... ad ęcclesiam ... una cum #scelest.|i filio cucurrit."

        * ALCUIN. epist.; 174 p. 288,14 "quae {(ecclesia)} ... #scelest.|is (#scelesti.|bus, #c|.elesti.|bus {@ scelestus_1b} {var. l.}) pessimorum ausibus maculata {(sc. est)}."

        * CARM. de Frid. I. imp.; 1565 "ut ... bellum gerant {Mediolanenses} cum gente #scelest.|a."

        {al.}

            ANHÄNGER de pretio pro scelere perpetrando promisso:

                * STEPH. Wilfr.; 27 "promittens Efyrvine dux ... ei {(Aldgislo regi)} ... modium plenum solidorum aureorum dare, pretium utique #scelest.|e (#c|.eleste {@ scelestus_1c} {var. l.}), si {eqs.}"


BEDEUTUNG subst masc. i. q. homo sceleratus, nefarius, peccator -- Frevler, Gottloser, Sünder; in proverbio: {=> scelestus_2a; scelestus_6}:

    * PAULIN. AQUIL. c. Fel.; 2,4 l. 52 "ipse {(sc. Christus)} se pro nobis misericorditer humiliavit, ... clemens pro #scelesti.|bus."

    * PASS. Ludm.; ((MGScript. XV; p. 574,18)) "mater novelli principis facto consilio cum #scelesti.|bus (ministris sceleris {2}) {eqs.}"

    * CONR. MUR. nov. grec.; 2,180 "esto celestis et non sociare #scelest.|is {@ scelestus_2a} (#c|.elestis {@ scelestus_1d} {var. l.}) {(cf. {=> scelestus_6})}."

    * EBERH. HEIST. Betel; 105 "ianua celestis est Christus, clausa #scelest.|is (#c|.elestis {@ scelestus_1e} {cod.}; gloss.: id est sceleratis et malis; ut sis celestis, non assimilare #scelest.|is{@ scelestus_6}) ((* {cf.}  H. Walther, Lat. Sprichwörter u. Sentenzen des Mittelalters. V.; nr. 32560))." 


    ANHÄNGER  de paganis:

        * DIPL. Frid. I.; 502 p. 433,25 "pro ... fratre Neronis fundatorem habet {Aquisgranum} sanctissimum Karolum, pro pagano et #scelest.|o imperatorem catholicum ((* {cf.} DIPL. Karoli M.; 295 p. 442,1))."


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 525