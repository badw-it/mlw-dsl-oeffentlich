LEMMA 1. saxum

GRAMMATIK 
    subst. II; -i n.
    
VEL {raro ({=> saxum_1A; saxum_1B; saxum_1C})} saxus

GRAMMATIK
    subst. II; -i m.


SCHREIBWEISE
    script.: 
        saso: {=> saxum_3A; saxum_3B} 
        sassum: {=> saxum_2A; saxum_2B}

FORM

    form.:
        nom. sg.: 
            saso: {=> saxum_3A}
        
        gen. pl. decl. I.:
                -arum: {=> saxum_5}




BEDEUTUNG lapis (magnus), molaris, petra -- Stein(brocken), Felsbrocken, -block:

    UNTER_BEDEUTUNG proprie:

        UU_BEDEUTUNG in univ.: 

            UUU_BEDEUTUNG usu vario:

                * LEG. Burgund. const. I; 102 "quicumque Iudaeus in christianum manum praesumpserit mittere pugno aut calce ... aut #sax.|o (#sax.|a, de #sax.|o {var. l.}) ..., iubemus, ut manus incisione damnetur."

                * AGIUS epic. Hath.; 599 "murus ... factus de #sax.|is pluribus extat." 

                * GERH. AUGUST. Udalr.; 2,29 l. 26 "uterque {(sc. ministrorum)} #sax.|um tantae magnitudinis portans, quantum levare potuit."

                * VITA Heinr. IV.; 7 p. 25,8 "grande #sax.|um super trabem ad feriendum desuper caput imperatoris posuit {(sc. quidam)}." 

                * CASUS Petrish.; 3,26 p. 150,16 "iuvenculi ... cęperunt #sax.|a evolvere {(sc. e supercilio montis)} in vallem. " 

                {al.} 


            UUU_BEDEUTUNG in lusu verborum (spectat ad Saxones):

                * BENZO ad Heinr. IV.; 1,17 p. 148,9sq. (vs.) "#sax.|a diu fregi {(sc. Karolus)}, pedibus fragmenta subegi; sic, sic victor eris {(sc. Heinricus)}, si crebro #sax.|a teris {(v. notam ed.)} (({sim.} 6,5 p. 552,15 [vs.]))." 

            UUU_BEDEUTUNG in {@ saxum_12} explicatione nominis gentis Saxonum:

                * HROTSV. gest.; 5 "iussit {rex regum} Francorum transferri ... regnum ad claram gentem Saxonum nomen habentem a #sax.|o per duritiam mentis bene firmam." 

                * VITA Norb. II; 89 "quae gens utraque {(sc. Sclavorum et Saxonum)} ex re trahit nomen; ista {eqs.}, ista #sax.|orum pondus et lapidei cordis caecitatem ... contestans." 

        UU_BEDEUTUNG de lapidibus missilibus:

                * WALAHFR. Mamm.; 24,7 "#sax.|a volant sanctumque ruunt." 
                
                * FROUM. carm.; 11,7 "missile ... #sax.|um manibus non caute relictum stridenti sonitu male raucum fertur in aethrem."
                
                * VITA Heinr. IV.; 6 p. 23,22 "qui in Urbe erant, tela, #sax.|a, praeustas sudes ignemque iactabant." 
                
                * OTTO FRISING. gest.; 2,23 p. 126,27 "non eum {(stratorem)} iaculorum seu #sax.|orum ab arce ad modum imbrium non interpolati iactus revocare poterant."

                {al.} 

            ANHÄNGER ?in ioco:

                * FROUM. carm.; 19,16 "nunc varios sum fac<turus> cum carmine ludos, quod genus omne hominum simul aggreget et pecus <omne>, qui certant ut cęnosum te {(sc. cuculum)} perdere #sax.|o (faxo {var. l., v. notam ed.})." 


        UU_BEDEUTUNG de lapidibus terminalibus, #sax.|is terminum indicantibus:

            * CHART. Stir.; I 323 p. 317,17 (c. 1150) "hic {(iudex)} ... positis #sax.|is terminos iustos novalibus ipsis {(sc. mansi)} prefixit." 
               
            * DIPL. Frid. I.; 178 "confirmamus ... possessionem ... cum evidentissima circumscripcione, videlicet a #sax.|is terminalibus ad orientem usque ad altam stratam in occidente." 

            * CHART. march. Misn.; II 510 p. 352,4 "tendit {(sc. situs loci)} ... usque in cumulum Gronowa, inde rursus usque ad #sax.|um {@ saxum_1A} grandem iuxta Striguz {eqs.}" 
               
      
            * CHART. Tirol. I; 936 p. 336,5 "a #sax.|o superiori et a via superiori inferius {eqs.}" 


        UU_BEDEUTUNG de lapide a paganis adorato:

            * SERMO de sacril.; 2 "qui {(sc. christianus)} ... ad arbores et ad #sax.|a et ad alia loca vadet, ... sciat se fidem ... perdedisse {(v. notam ed.)}" 

        UU_BEDEUTUNG de imaginibus deorum antiquorum:

            * WALTH. SPIR. Christoph. II; 5,70 "incassum tenet intestina sacerdos non impar mutis, quibus offert munera, #sax.|is." 
            
    UNTER_BEDEUTUNG per compar.:

    * THIETM. chron.; 7,43 "sociorum ira ferro et #sax.|is durior non mollitur ((* {spectat ad} $VULG. prov.; 27,3))." 

    * ALBERT. METT. div. temp.; 2,21 p. 84,20 "ut ... eos {(sc. milites)} quasi #sax.|i {@ saxum_1B} immobiles stantes interficerent {Frisii}." 

    * COSMAS chron.; 1,15 "qui {(Wratizlav)}  accepit uxorem ... de durissima gente Luticensi, et ipsam #sax.|is duriorem {(v. notam ed.)}."; 2,11 p. 98,19 "audiant te {(sc. Occardum)} Saxones #sax.|is rigidiores (({sim.} 3,27 p. 195,15)) {(cf. {=> saxum_12{sqq.}})}." 
    
    
ABSATZ
BEDEUTUNG rupes -- Fels(formation, -vorsprung, -klippe, -kluft, -wand); saepius usu plur.:
    
    UNTER_BEDEUTUNG in univ.:

        UU_BEDEUTUNG usu vario:

            * ARBEO Corb.; 40 p. 228,5 "repertus si fuisset {(sc. puer in praecipitium lapsus)} #sax.|arum {@ saxum_5} (#sax.|orum {1 corr. m.^2}) aciebus divisus scopulis {eqs.}" 

            * HROTSV. prim.; 261 "vidit {(sc. Hathumoda)} residere columbam in ... praecelso vertice #sax.|i." 

            * VITA Heinr. IV.; 7 p. 25,21 "pleps ... semianime corpus {(sc. insidiatoris)} per rupes, per #sax.|a tractum in frusta discerpsit." 

            * OTTO FRISING. gest.; 2,40 p. 148,12 "cernis eam, quae super arcem dependet, rupem, eminentia sua terribilem, confragosis locis #sax.|orum_que asperitate quasi inaccessibilem." 


            * CHART. Tirol. notar.; I| 60^a p. 27,20 "fuerunt {(sc. Concius et Oldericus)} confessi ... habuisse ... clausuram unam de vineis positam subtus #sax.|um de sancto Martino (({sim.} 60^b p. 28,9))." 

            {al.} 


        UU_BEDEUTUNG in descriptione terminorum:

                * CHART. Bund.; 509 p. 26,15 (a. 1206) "cui {(silvae)} coheret a mane Vuitardi ser Orti et Anrici Forciani de Bellaxio, a meridie #sassum {@ saxum_2A}, a sero {eqs.} (({sim.} 882 p. 340,17 "coheret {(sc. petiae terrae)}  ... a nullora #sassum {@ saxum_2B} sive platedum"))." 

                * CHART. Tirol. I; 1007^b p. 61,37sq. "ultra viam comunis est #saso {@ saxum_3A} montis Ciani ..., in quo #saso {@ saxum_3B} est una magna albedine valde alta parum longe {eqs.}" 
                 {cf. {=> saxum_1C{sq.}}}

                    ANHÄNGER fort. huc spectat: 
                    * CHART. Tirol. I; 700\* (a. 1217) "ab I latere vallis, apud quem stat Engelmarius, et a capitibus est #sax.|um."
                        


    UNTER_BEDEUTUNG de loco excelso tutelam praebente, arce naturali; fort. in nomine loci: {=> saxum_11}:

            * VITA Sadalb.; 14| ((MGMer. V; p. 57,22)) "in cacumine #sax.|i sita munitionem robustam obtinuit {urbs}"; p. 58,4 "altrinsecus #sax.|a naturalia ambiunt." 

            * GREG. CAT. chron.; I p. 158,31sq. "cuius {(gualdi)} fines ... sic reperiuntur: ... sub #sax.|os {@ saxum_1C} Teudericini et Aionis, deinde per fossatum in viam publicam, recte in #sax.|a inter Rimonem et ipsum gualdum {eqs.}" 

            * DIPL. Frid. I.; 75 "concedimus predictę ęcclesię ... nativum #sax.|um {@ saxum_11}, quod antiquo vocabulo Tutela vocatur ((* {?inde} DIPL. Loth. I.; 145 [spur.])) {(cf. comm. ed. p. 326sqq.)}." 



AUTORIN Strika