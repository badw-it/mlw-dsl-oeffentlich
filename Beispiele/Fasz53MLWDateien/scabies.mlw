LEMMA  scabies

GRAMMATIK  substantivum V; -ei

VEL  scabea (-bia)

GRAMMATIK  subst. I; -ae f.

SCHREIBWEISE

    script.:

        es-: {=> scabies007} {adde} * ANTIDOT. Glasg.; p. 125,38

        exc-: {=> scabies002}

        scalb-: * CONR. MUR. nov. grec.; 8,880 {(var. l.)}

STRUKTUR

    form.:

        acc. sg.:

            -a: {=> scabies001}{=> scabies003}{=> scabies004}{=> scabies006} {al.}

        abl. pro acc. pl.:

            -is: {=> scabies002}

BEDEUTUNG  asperitas cutis cum pruritu, ψώρα -- Krätze, Räude, Schorf:

    U_BEDEUTUNG  proprie:
    
        UU_BEDEUTUNG  gener. ((per compar.: {=> scabies005})):

            UUU_BEDEUTUNG  apud homines:

                * PAUL. DIAC. Lang.; 4,45 "fuit clades #scab.|earum (#scab.|iarum, scaberum, #scabi|erum, scabi eorum {var. l.}) ita, ut nullus potuisset mortuum suum agnoscere propter nimium inflationis tumorem (({sim.} * MARIAN. chron.; a. Chr. 640 "secuta est clades in populo, id est percussio #scab.|iarum, ita ut {eqs.}"))(({inde} * SIGEB. GEMBL. chron.; a. 618 "#scabi.|erum"))."

                * CARM. Sangall. II; 6,3,3,4 "si quis ... hos {(sc. libros Ezechielis et Danielis)} aufert, gyppo #scabi.|e_que redundet." {saepe.}

            UUU_BEDEUTUNG  apud animalia:

                * WALTH. SPIR. Christoph.; I 21 "Dagnus rex ... ita secum murmurasse narratur: '... totum gregem velut ovis morbida incrementata #scabi.|e {@ scabies005} commaculabit' (({sim.} * II 6,14 "in totum #scabi.|es descendet ovile"))."
                
                * GLOSS. Roger. III; 389 "contra #scabi.|em in tybiis equorum valet oleum de fraxino iuniperino."

                * ALBERT. M. animal.; 22,31 "si ... nimis longos habeat {canis} pilos et spissos, tunc sub pilis corrumpitur ei pellis et convertitur in fetorem et #scabi.|em."; 22,82 "#scabi.|es quaedam infirmitas est in cute equorum et dicitur #scabi.|es, eo quod {eqs.}"; animal. quaest. 4,1/2 p. 139,37 "aqua maris valet contra pruritum et #scabi.|em abstergendo, unde pellis cancri rubea est {eqs.}" {al.}

        UU_BEDEUTUNG  medic.:

            UUU_BEDEUTUNG  in univ.:

                * PS. GALEN. puls.; 369 "#scab.|ias (#scabi.|es, #scab.|eas , #scab.|ia {@ scabies001}, #scabi.|em {var. l.}) in vesica cum quis habuerit, ... dolorem futurum dicit."

                * ANTIDOT. Sangall.; p. 78,26 "sanat {antidotum} ... mala #scab.|ias et vulnera fagidineca (({sim.} * ANTIDOT. Berolin.; 1 p. 67,31 "curat ... malas #scab.|ias"))."

                * BOTAN. Sangall.; 26,4 "ad #ex|.cab.|iis {@ scabies002} aut inpitiginis: herba marubium ... omnes #scab.|ias et inpitiginis de corpora tollit (({sim.} * RECEPT. Sangall. I; 35 "ad #scab.|ia {@ scabies003} aut impetigine: ... ad grave #scab.|ia {@ scabies004} {eqs.}"))."

                * ANTIDOT. Augiens.; p. 54,13 "facit ... ad eos, qui assidue cufas ponunt, quod vulgo #scab.|ea {@ scabies006} dicunt."

                * ANTIDOT. Glasg.; p. 125,22 "medicamen ad #e|.scab.|ias {@ scabies007}."

                * HILDEG. phys.; 1,32 "qui minutam #scabi.|em ... habet, ... quenulam ... tundat." {persaepe.}

            UUU_BEDEUTUNG  spectat ad morbum oculorum:

                * PAUL. AEGIN. cur.; 81 "emfysima ... gravitas est tumida palpebre, psothalmia (({i.} ψωροφθαλμία)) vero pruritus palpebre #scabi.|es (($PAUL. AEG.; 3,22,8 ψωρίασις)) ex salsis humoribus facta."

                * CONSTANT. AFRIC. theor.; 9,15 p. 43b^{v} "#scabi.|ei {(sc. palpebrarum)} quattuor sunt genera: unum {eqs.}"

                * BENVEN. GRAFF. ocul. B; 16 p. 37,4 "Sarraceni vocant infirmitatem ... axfunutaxarim, id est #scabi.|es occulorum."

    U_BEDEUTUNG  in imag. et alleg.:

            * HRABAN. univ.; 18,5 p. 503^{A} "#scabi.|em in corpore habet, quem luxuria carnis devastat in mente."

            * EPIST. Worm.; I 59 p. 99,32 "de grege mihi {(sc. clerico)} commisso quosque ... erroris #scabi.|es ... a pascuis abegerit, ... omnes ... in Dei ... reducendos ovile ... procurabo."

            * HERM. AUGIENS. chron.; a. 1052 p. 130,33 "quosdam hereticos ..., ne heretica #scabi.|es latius serpens plures inficeret, in patibulo suspendi iussit {imperator}."

    U_BEDEUTUNG  translate:

        UU_BEDEUTUNG  in univ.:

            * FRANCO LEOD. circ.; 4,203 "ex quo tempore obsoleta est ... antiquata geometricae facultatis exercitatio, quae #scabi.|es fere ante ipsum Boetium omnem philosophorum gregem ... invasit, nullus id existimare praesumpsit ..., ut {eqs.}"

        UU_BEDEUTUNG  spectat ad peccatum, labem interiorem:

            * BENZO ad Heinr. IV.; 7,3 p. 610,15 "si quis non habet #scabi.|em, mittat in eam {(adulteram)} lapidem {(spectat ad Vulg. Ioh. 8,6sq.)}."

AUTOR  Fiedler