LEMMA sartago

GRAMMATIK  
    subst. III; -inis f.

SCHREIBWEISE 
    script.:
        ca-: {=> sartago_3b; sartago_3c} * {adde} TRAD. Michaelb.; 52
        ser-: * SERVITIA mensae; ((cod. Guelf. Gud. 131 f. 20^v)) {(a. corr.)}
        sarr-: *WALTH. AGIL. med.; 66| p. 169,5; p. 169,10. {ibid. al.}
        -tha-: {=> sartago_3b; sartago_3c}

FORM
    form.:
        abl. pl. decl. II.: {=> sartago_4}


BEDEUTUNG frixorium, patella, lebes, aenum -- Pfanne, Becken, Kessel:

    UNTER_BEDEUTUNG in univ.:

        UU_BEDEUTUNG proprie:

            * WALAHFR. hort.; 140 "videmus ... ardenti #sartagin.|e (gloss. $L: fannum) pinguem combibere {(sc. cucurbitam)} arvinam."

            * ANTIDOT. Glasg.; p. 155,3 "cernis cinus et melle et sale ..., frigis in #sartagin.|em ({cod.,} #rast|.aginem {ed.})."

            * HROTSV. Dulc.; praef. p. 127 "mente captus ollas et #sartagin.|es pro virginibus amplectendo osculabatur."

            * CHRON. Reinh.; a. 1190 p. 546,36 "in #sart.|hagine ({ed.}, #carth|.agin.|e {@ sartago_3b} {cod.}) excocto cadavere {(sc. principis)}."

            * ALBERT. M. animal.; 23,87 p. 1480,12 "ovum cum lacte caprino fac bullire ... in #sartagin.|e mundissima."

            {saepius.}


        UU_BEDEUTUNG in imag. et per compar.; c. gen. explicativo: {=> sartago_2}:

            * AETHICUS; 5,5 "eius {(sc. angelorum lapsorum)} ... condicione ({i.} condicioni) fieri {(sc. dicit)} tormentis ac poenis perpetuis sub terra collocata #sartagin.|e ({ci. ed.,} #carth|.agin.|e {@ sartago_3c}, catagine, catagina {codd.})."

            * EKKEH. IV. pict. Mog.; 259 "Iob satan ... velud in #sartagin.|e frixit."

            * CHART. Mog. A; II 573 p. 945,20 (epist.) "rex noster {(sc. Richardus, rex Anglorum)} in frixorio {(i. in carcere)} est, et occasione illius in quamplures provincias huius malitie #sartag.|o {@ sartago_2} desevit."

            {al.}


    UNTER_BEDEUTUNG de patella ad sal coquendum apta, salina:

        UU_BEDEUTUNG proprie:
        
            UUU_BEDEUTUNG spectat ad possessionem, reditus #sartagin.|is:

                * DIPL. Arnulfi; 170 "concessimus ... sal, quod ... vel a #sartagini.|bus (#sartagin.|is {@ sartago_4} {a. corr.}) aut a locis #sartagin.|um vel de areis ... ecclesiae redimatur ((* {sim.} DIPL. Otton. I.; 431 "salinam ... donavimus cum ... #sartagini.|bus locisque #sartagin.|um." {al.}))."

                * TRAD. Ratisb.; 211^b "vir ... tradidit talem portionem, sicut ille habuit unius #sartagin.|is."

                * CHART. archiep. Magd.; 349 p. 461,35 "duas casas salis, in quibus octo #sartagin.|um usus habetur, ... ecclesie dedimus."

                * CHART. Hamb.; 742 "Godefridus ... contulit ... chorum salis ... in #sartagin.|e, que wechpanne wulgariter appellaretur, et dimidium chorum ... in #sartagin.|e, que guncpanne vulgariter appellaretur." 

                {saepius.}

            UUU_BEDEUTUNG usu vario:

                * CHRON. Mont. Ser.; a. 1224| p. 212,7 "invenit {(sc. parrochianus ad morientem veniens)} hominem ex his, quibus est salis coquendi officium, qui se in #sartagin.|em sui operis bulientem iniecerat."; p. 212,22 "salina in #sartagin.|e buliente."
 

        UU_BEDEUTUNG meton. de copia salis in una patella cocti:

            * CHART. archiep. Magd.; 366 p. 480,26 (a. 1180) "quatuor #sartagin.|es salsuginis ... contulimus."; 397 p. 522,37 "VIII solummodo #sartagin.|es de fonte, qui Matheriz appellatur, et relique de Theutonico et Slavico fonte procurantur."

            * CHRON. Ebersh.; 13 p. 438,8 (chart.) "dedit {Odilia} ... monasterio ... reditus salis ad terciam dimidiam #sartagin.|em in Marsal et in Metiwich."


    UNTER_BEDEUTUNG de aeno ad cervisiam coquendam apto:

        * CAPIT. reg. Franc.; 128,25 p. 254,12 "invenimus in Asnapio, fisco dominico, ... utensilia: ... #sartagin.|em I (({item} 128,30))."

        * ACTA civ. Wism. A; 978 "#sartag.|o pertinet singulariter domicelle."; * B; 85 "dant {(sc. Vredeka et puer filii eius)} ... unum granarium et #sartagin.|em." 

    UNTER_BEDEUTUNG de aeno balneario:

        * CHART. Gosl.; II 223 (a. 1275) "structuram ... domus et stupe, utensilium quoque, videlicet #sartagin.|is, cupe, caldariorum et aliarum rerum ..., expensis propriis emendabit {Iohannes}."

AUTORIN Niederer