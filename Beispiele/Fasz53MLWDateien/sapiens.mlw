LEMMA  sapiens

GRAMMATIK  adiectivum III; -entis

SCHREIBWEISE

    script.:

        sip-: {=> sapiens009}

        -nci-: {=> sapiens005}{=> sapiens006} {al.}

STRUKTUR

    form.:

        acc. sg.:

            -te: {=> sapiens008}{=> sapiens015}

        gen. pl.:
        
            -um: {=> sapiens001}{=> sapiens002} {al.}

        {?}abl. pl.:
        
            -is: {=> sapiens004}

GEBRAUCH

    usu praed.: {=> sapiens007}{=> sapiens010}{=> sapiens014}

BEDEUTUNG  σοφός, intellegens, prudens, rationalis, sciens -- weise, einsichtig, verständnisvoll, klug, vernünftig, wissend:

    U_BEDEUTUNG  adi.:

        UU_BEDEUTUNG  strictius:

            UUU_BEDEUTUNG  de hominibus:

                UUUU_BEDEUTUNG  in univ.:

                    * FORM. Senon. II; add. 3,4 "contra adversarium consilio satis te putas #sapient.|e {@ sapiens008}, sed {eqs.}"

                    * CAPIT. reg. Franc.; 210 p. 81,5 "quia populo suo tam piissimum tamque #sapient.|issimum (sanctissimum {var. l.}) principem dedit {Deus}."

                    * HRABAN. carm.; 39,24,1 "utere {(sc. Adam)} eis {(viventibus ac terra germinantibus)} #sapien.|s {@ sapiens010} omnia quaevis comedens."

                    * NOTKER. BALB. gest.; 2,6 p. 57,23 "quam #sapient.|issimos (caros #sapient.|es {var. l.}) homines praeclarissimus Karolus habuerit."

                    * THANGM. Bernw.; 38 "more #sapient.|issimi architecti."

                    * ALFAN. premn. phys.; 2,113 p. 50 "#sapient.|issime reprehensor."

                    * EPIST. Hild.; 134,9 "quanto quisque fuerit generosior, #sapient.|ior atque sublimior, tanto Deo et hominibus debet esse subiectior." {persaepe.}

                UUUU_BEDEUTUNG  pro cognomine:

                    * CHART. Brem.; 281 (a. 1257) "rei testes sunt: ... Kerstianus in Superiori platea, Bernardus #S|.apien.|s {eqs.}"

                    * CHART. Hamb.; 728 "testes sunt: ... Iohannes, filius Zeghefridi, Hinricus #S|.apien.|s."

                    * ACTA civ. Wism. A; 483 "fideiussores sunt Iohannes #S|.apien.|s et Everhardus, frater suus." {al.}

                UUUU_BEDEUTUNG  de auctoribus ((sacrae scripturae: {=> sapiens013})):

                    * NOTKER. BALB. gest.; 1,11 p. 16,7 "contra interdictum #sapient.|is {@ sapiens013} viri."

                    * THANGM. Bernw.; 5 p. 759,52 "iuxta illud viri #sapient.|is: '{eqs.}'"

                    * THIETM. chron.; 2,45 "quod a quodam #sapient.|e presago futurorum scriptum est: '{eqs.}'"

                UUUU_BEDEUTUNG  in titulo honorifico:

                    * HUGEB. Willib.; 5 p. 104,1 "glorioso gentium <<gubernatore #sapient.|e>> (gubernatori #sapient.|i {var. l.; i. Gregorio papae}) ... serie sui iteneris ex ordine intimavit."

                    * LIUTG. Greg.; 7 p. 73,8 "#sapien.|s architectus et electus Dei pontifex Bonifatius."

                    * HRABAN. epist.; 28 p. 444,22 "placuit mihi te {(imperatorem)} unum ac solum iudicem benevolum et #sapient.|issimum expetere."

                    * THANGM. Bernw.; 2 p. 759,26 "qui {(Otto imperator)} septennis ... puer cum ... #sapient.|issima matre domna Theophanu augusta rebus praeerat."

                    * DIPL. Heinr. II.; 301 p. 375,14 "quod ({scriptum)} ... imperatrix Adelehida #sapien.|s {@ sapiens007} fecit predicto episcopio."

                    * EPIST. Hild.; 134,20 "apostolica ... persona circumscribitur: sanctissimus ..., #sapient.|issimus {eqs.}"

                    * HIST. de exp. Frid. imp.; p. 69,15 "ad nutum #sapient.|issimi imperatoris." {persaepe.}

            UUU_BEDEUTUNG  de diabolo:

                * HRABAN. carm.; 39,18,4 "serpens lubricus, #sapient.|ior omnibus bestiis."

            UUU_BEDEUTUNG  de Deo, Christo:

                * HRABAN. univ.; 15,1 p. 418^{D} "#sapien.|s ... Deus in Iob dicitur."

                * CONST. Constant.; 41 "#sapien.|s retro semper Deus edidit ex se ... verbum."

                * HROTSV. gest.; 64 "non est illo {(Christo)} penitus #sapient.|ior ullus inter ... mortales ... #sapient.|es {@ sapiens012}."

                * THIETM. chron.; 1,7 "consilium Deus #sapiens.|s {@ sapiens014} fatuavit."

                * ALFAN. premn. phys.; 5,14 p. 64 "creator #sapien.|s artificiose effecit, ut {eqs.}"

                * OTTO FRISING. gest.; 1,55 p. 80,24 "Deus est bonus, #sapien.|s, omnipotens." {al.}

            UUU_BEDEUTUNG  de rebus:

                * WALAHFR. carm.; 5,2,13 "miramur ... #sapient.|is munera mentis: doctrinam, mores {eqs.}"

                * NOTKER. BALB. gest.; 2,6 p. 53,14 "non videtur occultanda sapientia, quam #sapient.|i Grecie ... missus aperuit."

                * TRAD. Ratisb.; 218 "#sapient.|i usi consilio Oͮdalrih comes ... et uxor illius tradiderunt {eqs.}"

                * THANGM. Bernw.; prol. p. 757,35 "divinae providentiae #sapient.|em ammirabilemque dispositionem ... ammirari."

                * WOLFHER. Godeh. I; 10 p. 175,31 "#sapient.|issima dulcissimaque rethorizacione."

                * IDUNG. PRUF. dial.; 2,341 "#sapient.|issimo et brevissimo responso." {al.}

        UU_BEDEUTUNG  latius i. q. peritus, doctus -- erfahren, kundig:

            UUU_BEDEUTUNG  gener.:

                * EPIST. Hild.; 134,6 p. 198,10 "hoc {(sc. artem rhetoricam sine ordine vitiorum plenam esse)} Tullius in Rhetoricis testatur, unusquisque #sapien.|s in suis epistolis."

                * ANON. IV mus.; 2 p. 49,26 "si quis fuerit #sapien.|s, iuxta regulas supradictas posset invenire reliqua necessaria per se."

            UUU_BEDEUTUNG  publ., iur., canon.:

                * LEX Baiuv.; prol. p. 201, "Theuderichus rex ... elegit viros #sapient.|es, qui ... legibus antiquis eruditi erant."

                * CAPIT. reg. Franc.; 202 p. 64,1 "de iudicibus inquiratur, si nobiles et #sapient.|es et Deum timentes constituti sunt."; 295 p. 424,29 "ea ..., quae ... de episcopalibus capitulis cum inlustribus viris et #sapienti.|bus baronibus vestris observanda delegistis."

                * CONC. Karol. A; app. 9 p. 841,1 "sequentia ... capitula ... a #sapienti.|bus comitibus et scabinis dominicis sunt prolata."

                * DIPL. Loth. III.; 121^e "fratres tui sicut #sapient.|es iuri {(sic)} cum magna discretione nobis locuti sunt."

                * CHART. Tirol. I; 551^a "sicuti notarius vel #sapien.|s homo precipiet." {persaepe.}

ABSATZ

    U_BEDEUTUNG  subst. masc. vel fem.:

        UU_BEDEUTUNG  gener.:

            * IULIAN. TOLET. Wamb.; 9 p. 509,4 "ut quidam #sapien.|s ait {eqs.} (({sim.} 26.; insult. 9))."

            * ALCUIN. epist.; 226 p. 370,37 "multitudo #sapient.|um {@ sapiens002} sanitas est orbis et laus civitatis."

            * HRABAN. univ.; 15,1 p. 419^{A} "#sapient.|es ... huius mundi stulti sunt apud Deum."
            
            * GERH. AUGUST. Udalr.; 1,5 l. 21 "de vasallis suis semper secum aliquos #sapient.|issimos habere voluit."

            * THIETM. chron.; 3,14 p. 114,7 "positum est Romae concilium ...; #sapient.|issimi conveniunt {eqs.}"

            * BERTH. chron. B; a. 1077 p. 291,19 "regni #sapient.|ioribus et melioribus consentire."; a. 1078 p. 319,11 "regni totius primates, #sapient.|es et quique optimates praeter {eqs.}"

            * DIPL. Conr. III.; 101 p. 180,22 "prudenti consilio ... episcopi aliorumque #sapient.|um."

            * CHART. Turic.; 597 p. 103,9 "licet ... Grecis et barbaris #sapienti.|bus et insipientibus debitores simus {eqs.}" {persaepe.} {v. et} {=> sapiens012}

        UU_BEDEUTUNG publ., iur., canon.:
    
            UUU_BEDEUTUNG  usu communi spectat ad urbes, societates, instituta sim. ((* {de re v.} H. Planitz, Die deutsche Stadt im MA. 1954.; p. 256sqq.)):

                UUUU_BEDEUTUNG  in univ.:

                    * STEPH. Wilfr.; 11 p. 205,2 "reges ... consilium cum #sapienti.|bus suae gentis ... inierunt."

                    * FORM. Salisb. II; 3,20,3 "inter #si|.pient.|issimos {@ sapiens009}."
        
                    * CHART. Argent.; I 616 p. 471,20 "per concilium #sapient.|um mutabitur {moneta} secundum aliam formam, non secundum pondus."

                    * CHART. scrin. Col. A; II 1 p. 298,25 "notum sit ..., quod #sapient.|es civitatis hoc consulti sunt, quod {eqs.}"

                    * GESTA Trud. cont. II; 4,2 "#sapient.|iores et primos ecclesiae ..., quid ... super hoc {(sc. translationis)} negotio agendum esset, consuluit {episcopus}."

                    * CHART. Pomm. B; 789 p. 140,2sq. "quoniam #sapien.|cium {@ sapiens005} ... interest ea ordinare, que sunt bonum ad commune, propter hoc noveritis nos cum #sapien.|cioribus {@ sapiens006} nostris ... sic concordasse, quod {eqs.}" {persaepe.}

                UUUU_BEDEUTUNG  spectat ad consilium procerum i. q. conventus, credentia q. d. -- Rat(sversammlung) (({de re v.} * E. Mayer, Ital. Verfassungsgesch. II. 1909.; p. 549)) ((usu plur.)):

                    * OTTO MOR. hist.; p. 6,20 "omnes consules aliosque de Laude #sapient.|es, qui de credentia fuerant, ad se ... venire precepit {Sicherius}."; p. 93,11 "consules Cremenses et Mediolanenses cum #sapienti.|bus (#sapient.|ioribus {var. l.}) Mediolanensibus." {ibid. saepe.}

                UUUU_BEDEUTUNG  iunctura "consilium -um" sim. (({de re v.} * G. Chittolini, D. Willoweit, Statuten, Städte und Territorien zw. MA u. Neuzeit in Italien. 1992.; p. 30. 358)):
                    
                    * CHART. Col.; II 55 p. 67,21 (epist. papae a. 1217) "iuxta consilium #sapient.|is."

                    * CHART. Bund.; 814 p. 277,21 "habito conscilio #sapient.|is {@ sapiens004} partibus presentibus, scilicet {eqs.}"; 818 p. 281,33 "habito consilio #sapient.|um." {al.}
            
            UUU_BEDEUTUNG  iuris peritus -- Rechtsgelehrter, -verständiger (({de re v.} * H. Dahm, Die Ehrenprädikate der geistlichen und weltlichen Stände des Mittelalters. 1943.; p. 116sqq.)):

                * CAPIT. reg. Franc.; 22 p. 58,20 "iudici diligenter discenda est lex a #sapienti.|bus populo conposita."

                * LEX Frision.; add. tit. p. 80,1 "additio #sapient.|um."

                * DIPL. Otton. III.; 111 "communi consilio fidelium nostrorum ... episcoporum #sapient.|um_que laicorum."

                * CHART. Ital. Ficker; 54 p. 78,43 "prolate sunt carte et lecte per Iohannem de Apollinari #sapient.|e {@ sapiens015} rerum."

                * CHART. Tirol. I; 1079 "quod quilibet ... habeat suum #sapient.|em ad procedendum in causa."

                * ACTA imp. Böhmer; 984 p. 686,25 "consortium per capitula distincta formatum vos filii capitanei, confanonarii, #sapient.|es et alii servare perpetuo promisistis." {saepe.}

        UU_BEDEUTUNG  gramm. et rhet. (de grammaticis):

            * PAUL. DIAC. carm.; 15,2,1,2 "post has nectit subsequentes in secunda {(sc. iunctione, v. notam ed.)} species septimam atque octavam #sapient.|um studium."

        UU_BEDEUTUNG  philos.:

            UUU_BEDEUTUNG  def.:

                * BRUNO QUERF. Adalb. A; 21 p. 27,1 "#sapient.|em vocamus, cui fallere ingenium callet, cui in ore mel, in corde fel latet."

                * ALBERT. M. metaph.; 1,2,1 p. 18,48 "hunc dicimus #sapient.|iorem circa omnem scientiam, qui certior est et doctissimus causarum {eqs.}"; eth. II; 4,2,1 p. 295^a,8 "#sapien.|s est, qui seipsum in libra rationis ponit."

            UUU_BEDEUTUNG  exempla:
        
                * AGIUS epic. Hath.; 413 "#sapient.|um {@ sapiens001} haec satis est vera et pia traditio: {eqs.}"

                * ALFAN. premn. phys.; 1,69 p. 17 "nullus #sapien.|s dicet propter angelos ea facta fuisse."

                * HERM. CARINTH. essent.; 2 p. 164,2 "probabilis est {(sc. differentia)}, que apud #sapient.|es ... constat."

                * ALBERT. M. Is.; 11,2 p. 169,20 "Aristoteles quattuor dat diffinitiones #sapient.|is." {al.}

        UU_BEDEUTUNG  eccl. et theol.:
    
            UUU_BEDEUTUNG  spectat ad quosdam auctores sacrae scripturae (sc. Vulg. prov., eccl., cant., Vet. Lat. sap., Sirach):

                * HRABAN. epist.; 10 p. 397,7 "te {(sc. episcopum)} ... magis ... provocet recordatio, quae per quendam #sapient.|em voluit dicere: '{eqs.}'"

                * GUNZO epist.; 6 | p. 33,1 "retardabar ... pręcepto #sapient.|is, quo dicitur: '{eqs.}'";  p. 33,4 "respondeo nunc monente eodem #sapient.|e: '{eqs.}'"
        
                * IDUNG. PRUF. argum.; 246 "iuxta illud #sapient.|is: {eqs.}"
        
                * OTTO FRISING. gest.; 2,25 p. 129,7 "inducto #sapient.|is vel psalmigraphi testimonio." {al.}

            UUU_BEDEUTUNG  spectat ad sanctos sim.:

                * HRABAN. univ.; 15,1 p. 418^{D} "#sapient.|es sancti vocitantur in Osee."

                * IDUNG. PRUF. dial.; 1,320 "iuxta dictum #sapient.|is." {al.}
    
        UU_BEDEUTUNG  alch.:

            * PS. ARIST. magist.; p.643^{a},28 "aperi ... duo caldaria, et in unoquoque invenies saporem #sapient.|um albi coloris et saporem viridem a viridate aeris."

SUB_LEMMA  sapienter

GRAMMATIK  adverbium

BEDEUTUNG  σοφῶς, prudenter, rationaliter, diligenter -- einsichtsvoll, vernünftig, verständig, klug, weise, umsichtig:

    U_BEDEUTUNG  in univ.:

        * WILLIB. Bonif.; 1 p. 7,3 "quem {(abbatem)} ... #sapiente.|r ... adlocutus est."

        * EIGIL Sturm.; 12 p. 142,22 "sanctus ... Bonifacius ... ad Karlomannum ... regem perrexit et humiliter #sapiente.|r_que ad eum locutus est."

        * HROTSV. gest.; 250 "omnia compositis #sapiente.|r dicere verbis."

        * HERM. AUGIENS. mus.; 15 p. 47,38 "quomodo #sapiente.|r cantant, qui nihil de praedictis sciunt, qui tropum tropo permutantes confundunt {eqs.}?"

        * WOLFHER. Godeh. II; 13 "erat {Bernwardus} ... in tota mundanae utilitatis sagacitate #sapiente.|r providus."

        * CHART. Aquens.; 19 l. 82 "machinantur contra eos, quorum accusatio et machinatio #sapiente.|r et caute ęcclesiasticis iudicibus providendę sunt." {persaepe.}

    U_BEDEUTUNG  modo iurium perito -- auf rechtskundige Weise:

        * LEX Sal. Pipp.; epil. | E p. 191,4 "titulos Chlotharius a germano suo ... excepit, sic et ipse similiter cum <<regni sui #sapiente.|r invenit>> (({cf.} D p. 180,4 "regnum suum perinvenit"))."

AUTOR  Fiedler