LEMMA sceptrum

GRAMMATIK
     subst. II; -i n.

ETYMOLOGIE σκῆπτρον

SCHREIBWEISE
     script.: 
         c-: {=> sceptrum_1; sceptrum_8} *{adde} RHYTHM. in Odon.; 1,3 ((MGPoet. IV; p. 137))
         se-: {=> sceptrum_7}
         scae-: {=> sceptrum_9}
         scip-: {=> sceptrum_3}

FORM
     form.:
         decl. IV.: {=> sceptrum_4}

VERWECHSELBAR
     confunditur c.:
          scriptum: {=> sceptrum_6}

BEDEUTUNG baculum regnantis -- Herrscherstab, 'Szepter'; saepe usu plur., e. g. {=> sceptrum_3; sceptrum_5}:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG in univ.:
             UUU_BEDEUTUNG usu communi:
                 * WALAHFR. carm.; 77,10,1 "purpuram, #sceptr.|um, diadema, fasces ... decus et paternum temnit {(sc. Ianuarius martyr)}, ut Christi melius honorem comprobet in se."
                 * IOH. SCOT. carm.; 9,78 "da nostro regi Karolo, cui #sci|.ptr.|a {@ sceptrum_3} dedisti {(sc. Deus)}, ut semper famulus tibi vivat mente benigna."             
                 * WIPO gest.; 3 p. 20,27 "si Carolus Magnus cum #sceptr.|o vivus adesset."
                 * HONOR. AUGUST. gemm.; 1,73 "procedit pontifex cum baculo quasi imperator cum #sceptr.|o."
                 {saepius.}

                  ANHÄNGER in fabula:
                     * ECBASIS capt.; 491 "#sceptr.|um regis tetigit {vulpis} sub federe pacis."; 560 "gestamen #sceptr.|i vulpi concesserat {leo}."

             UUU_BEDEUTUNG locut.:
                 UUUU_BEDEUTUNG "#sceptr.|um, #sceptr.|a sumere, suscipere, relinquere":
                     * LEG. Wisig.; 2,1,7 p. 52,16 "cum divine voluntatis imperio principale caput regnandi sumat #sceptr.|um {eqs.}
                     ((* {sim.} HELM. chron.; 50 p. 98,29 "sumpto #sceptr.|o"))."
                     * ALCUIN. Willibr.; capit. 1,13 ((MGMer. VII; p. 115,11)) "quod Pippinus dux diem obiit et Carolo filio suo {!regni} #sceptr.|a reliquit
                     ((
                     * LEG. Wisig. chron.; p. 461,8 "suscepit ... dominus noster Ervigius r. #sceptr.|a." {al.}
                     ))."
                     * VITA Boniti; 3 ((MGMer. VI; p. 139,21)) "pronepos eius {(principis)} suscepit #sceptr.|a."
                     {al.}              
             UUUU_BEDEUTUNG "#sceptr.|um, #sceptr.|a tenere, gubernare" sim.:
                 UUUUU_BEDEUTUNG de rege, regina, imperatore, imperatrice:
                     * BONIF. epist.; 73 p. 146,24 "domno carissimo ... inclita Anglorum imperii #sceptr.|a gubernanti Aethilbaldo regi Bonifatius ... salutem {(sc. dicit)}
                     ((
                         *{sim.} WANDALB. martyr.; dedic. 3 "caesar adesto, #sceptr.|a parentum qui pietate ... gubernas {eqs.}" {al.}
                     ))."
                     * PAUL. DIAC. carm.; 26,20 "tu {(sc. Hildegardis regina)} sola inventa es, fueris quae digna tenere multiplicis regni aurea #sceptr.|a {@ sceptrum_5} manu."
                     * HRABAN. carm.; 4,2,1 "inclita #sceptr.|a tenens ..., regina
                     ((
                         *{sim.} POETA SAXO; 4,18 "augusto Carolo magno ... imperii ... Romani #scę|.ptr.|a {@ sceptrum_9} tenenti gloria, prosperitas {(sc. sit)}." {al.}
                     ))."
                     * ADAM gest.; 3,21 p. 152,1 "Iacobus ... in Suedia #sceptr.|um habuit (tenuit {var. l.})."
                     * CARM. de Frid. I. imp.; 1807 "agmina tanta sequi Fredericum #sceptr.|a regentem."
                     {persaepe.}        
                
                 UUUUU_BEDEUTUNG de Deo, s. Petro:
                     * HRABAN. carm.; 11,57 "qui triplex simplex caelorum #sceptr.|a gubernas."
                     * CARM. imag.; 2,4,2 "virtutum doctor, pie noster ad ęthera ductor, salvans terrigenas, cęli qui #c|.eptr.|a {@ sceptrum_1} gubernas ..., Petrus."

                 UUUU_BEDEUTUNG de genetrice Dei:
                     * CARM. Cantabr. A; 36,2,2 "tu, regina celi summa, castitatis tenes #sceptr.|a." 

            
         UU_BEDEUTUNG de insigni imperiali; {de re v.} LexMA. VII. p. 623sqq. s. v. 'Reichsinsignien':
             UUU_BEDEUTUNG in univ.:
                 * ANNAL. Fuld. II; a. 840 p. 31,19 "missis ei {(Lothario)} insigniis regalibus, hoc est #sceptr.|o (#sceptr.|um {var. l.}) imperii et corona."
                 * WIDUK. gest.; 1,29 "huic {(Arnulfo)} Oda et diadema et #sceptr.|um et cetera regalia ornamenta obtulit."     
                 * EPIST. Heinr. IV.; 39 p. 56,9 "coronam, #sceptr.|um, crucem, lanceam, gladium misi Moguntiam."
                 * CARM. de Frid. I. imp.; 1914 "tu mihi Romani #sceptr.|um et diadema gerendum imperii, licet indigno, pater alme, dedisti."
                 {saepius.}

             UUU_BEDEUTUNG in actu symbolico, in investitione episcopi:
                     * DIPL. Loth. III.; 120 p. 201,35 "electum {(sc. episcopum)} nobis per #sceptr.|um investiendum representent {(sc. conventus Casinensis)}."
                     * GERHOH. tract.; p. 83,12 "ut episcopi Teutonici in presentia regis eligerentur et regalia per #sceptr.|um acciperent."
                     * LAUR. LEOD. gest.; 29 p. 508,10 "electionem eius {(Alberonis)} curia laudavit ... datis ei per #sceptr.|um temporalibus episcopii."
                     * ANNALISTA SAXO; a. 992 p. 254,15 "interfuit ... dedicationi Otto rex ..., qui et #sceptr.|um suum aureum ad manum pontificis sacrificii tempore obtulit."
                     {al.}
                 ANHÄNGER in investitione abbatis:
                     * DIPL. Heinr. II.; 518 p. 668,18 "prohibemus, ne missi nostri ... aliquid tollant ... nec pro investitura, quae per #sceptr.|um regium fieri debet, ab abbate de novo creato aliquid expetatur."
             
         UU_BEDEUTUNG de #sceptr.|o Dei, Christi:
          * HRABAN. carm.; 2,2,23 "testans per #sceptr.|a Tonantis
                 ((
                  *{item}; 19,43 
                 ))."; 16,55 "continet haec {(sc. arca)} tabulas, urnam #sceptr.|um_que triumphi."
             * CARM. imag.; 18,6 "Marcus in hoc regis Christi fert #sceptr.|a leone."
             * CARM. Cantabr. A; 5,11,3 "regnum cuius finem nescit, #sceptr.|um splendet nobile, celo sedens ... factor facta continens."
       
ABSATZ
     U_BEDEUTUNG meton.:

         UU_BEDEUTUNG  regnum, imperium -- Herrschaft:
                 UUU_BEDEUTUNG de imperio regum, imperatorum:
                     UUUU_BEDEUTUNG in univ.:
                     * HRABAN. carm.; 3,9 "iudex per #sceptr.|a potentum notatur Christus."
                     * ALTFR. Liutg.; 1,4 "qui {(Carolus)} multas gentes #sceptr.|is adiecit Francorum
                     ((*{ex} ALCUIN. Willibr.; 1,13 ((MGMer. VII; p. 127,6))
                     ))."
                     * POETA SAXO; 4,189 "subicitur ... #sceptr.|is Burgundia paene tota pii regis Hludowici."                
                     * ADAM gest.; 2,56 "cui {(Heinrico)} successit in #sceptr.|um (#sceptr.|o {var. l.}) fortissimus cesar Conradus."
                     * EPIST. Wibald. I; 93 p. 163,24 "que {(Belgica)} nostris {(sc. Heinrici regis)} #sceptr.|is ex maiori parte subdita est."
                     {persaepe.}        
                     ANHÄNGER c. gen. inhaerentiae:
                         * DIPL. Ludow. II.; 70 p. 204,3 (spur. a. 1111/52) "quas {(curas et sollicitudines)} tot pro fidelium regimine sub imperii #sceptr.|o nostri degentium sustinemus."
                         * DIPL. Constantiae; 63 p. 198,37 "sicut nostri #sceptr.|um regiminis a divine protectione clementie credimus feliciter protegi ..., sic {eqs.}"
                     UUUU_BEDEUTUNG locut. "in #sceptr.|o, #sceptr.|is agere":
                         * STEPH. COL. Maurin.; 4 p. 277^D "agente ... in #sceptr.|is divo Ottone."
                         * ODILO CLUN. Adelh.; praef. p. 28,4 "primo Ottone in #sceptr.|is feliciter agente."
                         * THEOD. TREV. transl. Celsi; 5 "quando ... Otto secundus ... princeps agebat in #sceptr.|o."
                         * THEOD. THEOL. Conr.; 2 p. 214^b,37 "quartus Heinricus ... strennue in #sceptr.|is agebat."
    

                 UUU_BEDEUTUNG de imperio Dei:
                     * WALAHFR. carm.; 43,3,2 "hic {(sc. in altari)} matri Domini sociaris, sancte Gregori, cum qua pro nobis poscito #sceptr.|a Dei."         
                     * THANGM. Bernw.; 38 " quae {(catervae)} divinis #sceptr.|is sub eius vexillis militabant."
                     * METELL. exp. Hieros.; 6,9 "tellus te {(sc. Ierusalem urbem)} mediata fovet, per te renovata, orbis ubi centrum, cęli terrę quoque #sceptr.|um ... concludit virgineus rex."

         UU_BEDEUTUNG territorium imperii -- Herrschaftsgebiet, Reich:
             UUU_BEDEUTUNG de "#sceptr.|o" hominum:
                 * EPIST. Bonif.; 49 p. 79,16 "si cui nostrum {(sc. Denehardi, Luli, Burchardi)} contingit huius Bretanicae telluris #sceptr.|a visitare {eqs.}"; 98 p. 219,7 "postquam Britannice telluris inclita #sceptr.|a ... deserui {(sc. Lul)}."
                 * TRANSL. Epiph.; 1 "cum Beringarius ... #sceptr.|a Italici regni invaderet
                 ((*{sim.} * HEINR. TEG. Quir.; I 1 "Decius tyrannus ... #sceptr.|a invadens, ne dicam polluens, ... christianos delevit"))."
                 * IOH. VEN. chron.; p. 146,19 "imperator universis suo #sceptr.|ui {@ sceptrum_4} adiacentibus edictum et inevitabile intulit preceptum, ut {eqs.}"
                 {v. et {=> sceptrigeralis/sceptrum_2; sceptrigeralis/sceptrum_10}}

             UUU_BEDEUTUNG de #sceptr.|o Dei:
                 * AEDILV. carm.; prol. 5 "hunc {(poetam)} memorare libens semper, lectissime presul, sancta supernorum conscendens #sceptr.|a polorum."         
                 * GESTA Bereng.; 2,51 "tertius {(sc. miles)} alta poli scandit supremaque ponti tristis, ut almificis sese sustollere #sceptr.|is forte queat."
      
         UU_BEDEUTUNG regnans, imperator, imperatrix -- Herrscher(in), Gebieter(in):
             UUU_BEDEUTUNG de hominibus:
                 * HRABAN. carm.; 37,62 "Samuel quotiens Saulis tolerabat aerumnas, et David latitans impia #sceptr.|a fugit (({spectat ad} * $VULG.; I reg. 21,10sqq.))?"
                 * WIDUK. gest.; 1 praef. "nostra ... humilitas presumit de proxima #sceptr.|is semper clementia."
                 * GERB. epist.; 28 "ea, quę est Hierosolimis, universali ęcclesię #sceptr.|is regnorum imperanti {(v. notam ed.)}."
                 * OTLOH. prov.; I 81 "iudicium durum super haec est #sceptr.|a futurum ((*{spectat ad} $VET. LAT.; sap. 6,6))."
                 * PASS. Thiem. I; 83 "contempnens regis pacem pro federe legis atque minas #sceptr.|i pro pietate Petri."

             UUU_BEDEUTUNG de Deo:
                 * HRABAN. carm.; 34,14 "Stephanus #sceptr.|a superna precans."; 53,15 "cum aethereis turmis placantes {(sc. Maria et archangeli)} #sceptr.|a superna propitium nobis celsithronum facite."

             UUU_BEDEUTUNG de Roma urbe:
                 * ALCUIN. carm.; 25,4 "tu {(sc. Roma)} ... iustitiae #sceptr.|um, lux sedis, honoris perpetuis valeas Christo donante triumphis."

         UU_BEDEUTUNG dignitas, potestas, auctoritas -- Ansehen, Macht, Autorität:
             * HRABAN. carm.; 4,3,15 "et decus et #sceptr.|um, virtus et gloria mundi transit."
             * WANDALB. mens.; 10 "qui {(Ianus rex)} ... urbem Laurentum #sceptr.|is populoque et legibus auxit."
             * DIPL. Conr. I.; 17 p. 16,39 "{!regali} #sceptr.|o {@ sceptrum_6} ({corr. ex} scripto) sancimus, ut {eqs.}
             ((* {inde} DIPL. Conr. II.; 39 p. 43,31. * DIPL. Otton. I.; 128 "r. #sceptr.|o iubemus"))."
             * DIPL. Otton. II.; 155 p. 175,35 "{!imperiali} #sceptr.|o sancimus, ut {eqs.}
             ((* {inde} DIPL. Heinr. II.; 497 p. 634,31. {al.} * DIPL. Otton. I.; 31 p. 117,35 "quatenus illorum {(sc. monachorum)} quietudo nostro defendatur i. #sceptr.|o"))."
             * THIETM. chron.; 2 prol. 17 "maximus in #sceptr.|o ter denos sedit {Otto} et octo annos in regno. "
             * CHART. Carniol.; II 93 p. 67,1 (dipl. Frid. II.) "quanto ... celesti munere spaciis opibusque terrarum potenter extollitur #sceptr.|um nostrum, tanto {eqs.}"
             {al.}

             ANHÄNGER c. gen. inhaerentiae:
                 * DIPL. Ludow. Inf.; 58 p. 186,29 "per quod {(praeceptum)} ei {(sc. episcopo)} {!regiae auctoritatis} scripto ({ed.}, #sceptr.|o {codd.}) licentiam concedimus ... mercatum et monetam habere
                 ((*{inde} DIPL. Conr. I.; 36 p. 33,27 "r. a. #sceptr.|o."
                 * DIPL. Heinr. I.; 10 "r. a. #sceptr.|o iubemus, quatenus {eqs.}"))"         
         
  
ABSATZ
     U_BEDEUTUNG translate:
         UU_BEDEUTUNG baculum -- Stock:
             UUU_BEDEUTUNG in univ.:
                 * CARM. Bur.; 92,73,1 "#sceptr.|o (#se|.ptro {@sceptrum_7} {var. l.}) puer {(sc. Amor)} nititur floribus perplexo."
             UUU_BEDEUTUNG de baculo magistri:
                 * CARM. pro schola Wirz.; 25 "imperio Christi moderando #sceptr.|a magistri pręter scripturę studium nihil est sibi {(sc. rectori)} curę, cultor virtutis manet ęternęque salutis."
         UU_BEDEUTUNG malleus -- Hammer:
             * ADAM gest.; 4,26 "Thor ... cum #sceptr.|o Iovem simulare videtur."
         UU_BEDEUTUNG trulla -- (Rühr-)Kelle:
             * THEODULF. carm.; 27,85 "Hispani potus Hardberd servator avarus ... calidum #sceptr.|o versat caldare culinis, ut bibat hoc gelidum, quod movet hic calidum."
         UU_BEDEUTUNG virga, surculus -- Pfropfreis:
             * ALBERT. M. veget.; 5,43 "#sceptr.|a (#c|.eptr.|a{@ sceptrum_8}, sectura {var. l.}) in arboribus divisa per longitudinem nihil omnino variant in sapore fructus et figura, quando fuerint sanata."; 5,54 "inseritur #sceptr.|um unius speciei in truncum eiusdem speciei."

AUTORIN Weber