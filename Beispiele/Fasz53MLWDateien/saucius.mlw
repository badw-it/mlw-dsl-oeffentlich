LEMMA saucius

GRAMMATIK
adi. I-II; -a, -um

BEDEUTUNG vulneratus -- verwundet, verletzt:
     U_BEDEUTUNG adi.; usu subst.: {=>  saucio_1/saucius_3; saucio_1/saucius_1; saucius_2} {al.}:
         UU_BEDEUTUNG strictius:
             UUU_BEDEUTUNG proprie:
                 UUUU_BEDEUTUNG de hominibus:
                     UUUUU_BEDEUTUNG in univ.:
                         * CHRON. Fred.; 2,48 "imperatur Valens cum {!sagitta} #sauci.|us fugerit {eqs.}
                         ((
                             ; 4,34 "s. #sauci.|us (sauciatus{@ saucio_9}, toxicata {var. l.}) moritur {Gundoaldus}."
                             * VITA Serv.; 17 p. 64,16 "imperatorem s. #sauci.|um ... in vilissimam ... casam ministri eius occultandum deportavere {Romani}"
                         ))."                       
                         * WALAHFR. Mamm.; 22,24 "cernere erat ... cuncta ... {!membra} loco vel caesa carere priori #sauci.|a vel remanere suo
                          ((
                            * ARNULF. SAG. invect.; 4 p. 98,26 "velut ab illeso capite m. #sauci.|a suae sortirentur initia sanitatis." {al.}
                          ))."
                         * WILH. APUL. gest.; 4,332 "cum pars iam multa fuisset #sauci.|a Turchorum."
                         * CASUS Petrish.; 2,34 "Saxones ... adepta victoria suos #sauci.|os et occisos levantes conciti abierunt."                    
                         * CONR. MEND. Attal.; p. 132,10 "videns ... se #sauci.|um et graviter vulneratum ... lipotomiam incidit {(sc. sartor quidam dives)}."             
                         {saepius.}
                         
                     UUUUU_BEDEUTUNG c. acc. vel abl. respectus:
                         * MIRAC. Mart. Vert.; 10 ((MGMer. III; p. 574,31)) (s. X.) "graviter latus #sauci.|us cuncta ..., quae fures diripuerant, revocavit {(sc. Bodilo)}."
                         * ODO MAGDEB. Ern.; 7,375 "unum ... relinquunt {(sc. gens Cananaea)} retro, qui claudus multum pede #sauci.|us uno a duce detentus ... dimittere fustem ... compellitur."
                 UUUU_BEDEUTUNG de animalibus:
                     * LIBER accip.; 2,1 "eger {(sc. accipiter)} #sauci.|o gratulenter incedens."
                     * GESTA Bereng.; 2,172 "#sauci.|us (gloss.: pro sauciatus, i. e. vulneratus) extremo donec cum sanguine frenos respuit {equus} et iuncta domino cervice recumbit."
                     * CONR. MUR. physiol.; 781 "cede vel vulnere #sauci.|us herbe ulcera subiectat {ursus}, que Greco nomine flomus fertur."
                     * ODO MAGDEB. Ern.; 8,10 "veluti leo nuper in arvis #sauci.|us Hircanis ... assuescit vincula collo scire pati."
                    
                 

             UUU_BEDEUTUNG in imag. vel translate; interdum i. q. maestus, sollicitus, pressus -- betrübt, bekümmert, belastet:
                 UUUU_BEDEUTUNG in univ.:
                     * CHRON. Fred.; 4,68 p. 154,30 "Sicharius ... menas adversus Samonem loquitur ...; Samo respondens iam #sauci.|us (caucius {var. l.}) dixit: {'eqs.'}"
                     * VULF. Marc. II; 337 "#sauci.|a nunc trepidant nostrae precordia mentis extima sacrati promere gesta viri."
                     * HUCBALD. carm.; app. 2,3,2 "quod nequimus consequi reatu multo #sauci.|i, id Lebuino tribuas {(sc. Deus)}."           
                     * CARM. Bur.; 119,3,1 "igne novo Veneris #sauci.|a mens, que prius non novit talia."
                     {al.}          
                 UUUU_BEDEUTUNG de animabus peccatorum:
                     * RHYTHM.; 148,49,2 "pessimorum peccatorum #sauci.|us sum {(sc. peccator paenitens)} vulnere."
                     * TRANSL. Eug. Tolet.; 23 p. 272,16 "populus ... #sauci.|us delictorum vulnere."
                     * HARIULF. chron.; 4,18 p. 220,15 "eorum {(sc. hominum scelestorum)} ... animas #sauci.|as sanctarum formentis scripturarum resolidans {(sc. Gervinus)}."
                     * ARNOLD. LUB. Greg.; prol. l. 42 "stabulum, in quo #sauci.|i {@ saucius_2} curantur et morbidi, sancta et catholica est ecclesia ((*{spectat ad} $VULG.; Luc. 10,34sq.))."
                     {al.}

         UU_BEDEUTUNG latius:
             UUU_BEDEUTUNG debilitatus -- geschwächt, schwach:
                 * BRUNO QUERF. Adalb. A; 21 p. 27,26 "cessit civilibus bellis #sauci.|a civitas."
                 * MIRAC. Adalb. Wirz.; 6 p. 50,5 "vix vitalis halitus in pectusculo {(sc. militis aegritudine laborantis)} #sauci.|o spirabat."
             UUU_BEDEUTUNG superatus, victus -- geschlagen, besiegt:
                 * CARM. var. Walther; 1,87,4 "tractant {(sc. gentiles)} cum disceptatu, quis quem posset vincere ac de suo principatu #sauci.|um deicere."

ABSATZ
     U_BEDEUTUNG subst. neutr. i. q. vulnus, morbus -- Wunde, Krankheit:
         * UDALSC. carm.; p. 115,30 "Christi nos sanguis sanat velut eneus anguis, qui nescit virus, sed curat #sauci.|a visus ((*{spectat ad} $VULG.; num. 21,9))."

AUTORIN Weber