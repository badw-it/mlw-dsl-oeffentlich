LEMMA 1. schisma (ci-, sci-)
GRAMMATIK subst. III; -atis n.
f.: {=> femininum_1; femininum_2}
ETYMOLOGIE σχίσμα
SCHREIBWEISE
  script.:
    cysma:  {=> cysma}
    si-:  {=> sisma}  {adde} * CONST. imp. I; 249,5.
    scim(a):  {=> scima}  {adde} * GERB. epist.; 181 p. 213,2.



ABSATZ
BEDEUTUNG proprie i. q. scissio, scissura -- Spaltung, Trennung; in imag.: {=> imag_1; imag_2}:

  U_BEDEUTUNG strictius:

    UU_BEDEUTUNG eccl. de ecclesiae #schismat.|e i. q. scissio, scissura ecclesiae, fidei -- Kirchenspaltung, Glaubensspaltung, 'Schisma' ((* {de re v.} LThK. ^{3}IX.; p. 147sqq.)):

      UUU_BEDEUTUNG in univ.:
      * RHYTHM.; 145,6,1  "exorta #scisma {@ femininum_1} ... ab aquilone parte {eqs.}"
      * HRABAN. epist.; 25 p. 431,34  "#sci|.sma.|ta in aecclesia Christi oriuntur, cum rectores populi Dei in doctrina sua ab invicem discrepant."
      * DIPL. Karoli III.; 87 p. 141,9  "quod quidam ... episcopus nomine Iohannes a #sci|.smat.|e Arriano eandem ecclesiam ad fidem quondam catholicam converterit."
      * VITA Eberh. Salisb. II; 5 p. 100,25  "#ci|.sma.|ti resistere et canonica servare suos coegit pontifices."
      * TRAD. Salisb. II; 223  "tempore #sci|.smat.|is ((* CHART. Garz.; 10 p. 99,11  "t. #ci|.smat.|is." {al.}))."
      * CAES. HEIST. hom. exc.; 279  "lepra capitis heresis est ...; lepra barbe #scisma {@ femininum_2} est, que tunicam inconsutilem, id est ecclesie unitatem, scindit; lepra cutis simonia est, que {eqs.}"
      {persaepe.}

      ANHÄNGER in imag.:
      * TIT. metr. I; 9,43  "sanasti {(sc. Hadrianus papa)} patriae laceratum #sci|.smat.|e {@ imag_1} corpus restituens propriis membra revulsa locis."

      UUU_BEDEUTUNG in lusu verborum de synodo:
      * WOLFHER. Godeh. I; 21   p. 183,1  "ab hac {(sc. Gandersheimi habita)} sinodo, quae non sinodus, sed #scisma iustius vocaretur."

    UU_BEDEUTUNG publ. de regni #schismat.|e:
    * SALOM. III. carm. I; 1,2,108  "omne ... regnum contra se #sci|.smat.|e scissum destruitur (({spectat ad} * $VULG.; Luc. 11,17))."
    * RAHEW. gest.; 3,37 p. 210,21  "ut ... #sci|.smat.|e regni gauderet {(sc. civitas Mediolanum)} et geminorum potius dominorum quam unius super se iuste regnantis affectaret principatum."
    * OTTO FRISING. chron.; 7,7 p. 316,10  "signa ... ac prodigia ... tam #scisma (#scima {@ scima} {var. l.}) regni quam iter Hierosolomitanum portendentia ab aliis posita sufficiant."

    UU_BEDEUTUNG usu communi (in imag. de vulneratione, vulnere fisso):
    * AETHICUS; 43  "reliqua {(i. ambigua)} ... magno studio indaganda, ne #scisma {@ imag_2} indagationum inducat cicatricem errorum inter (fideles {addidit ed.}) philosophorum astutia."

U_BEDEUTUNG latius:

  UU_BEDEUTUNG discordia, dissensio, discidium, distractio, controversia -- Uneinigkeit, Zwietracht, Entzweiung, Zerwürfnis, Streit:
      UUU_BEDEUTUNG gener.:
      * BONIF. epist.; 50 p. 84,17  "ut aecclesię sacerdotibus vel populo christiano inde {(sc. ex matrimonio illicito)} scandala et #sci|.sma.|ta vel novi errores non oriantur et concrescant."
      * DIPL. Otton. I.; 239 p. 333,31  "ut penitus praeterita lis et #schism.|a evelleretur (({inde} *DIPL. Heinr. II.; 71 p. 89,21 "#si|.sma {@ sisma}"))"
      * CHART. Turic.; 1327 p. 40,19  "ne ... subrepat #cysma {@ cysma} pacis et quietis inimicum."
      * MEMOR. Mediol.; a. 1219 ((MGScript. XVIII; p. 401,43))  "eo anno fuit comutatio et #cisma de quartirolo."
      * ALBERT. M. pol.; 5,8^l p. 534^b,24  "nomen {(sc. Cypselidarum)} est cuiusdam gentis tyrannice imperantis, quae semper #schism.|a ponit inter suos, ne eleventur contra tyrannum."
      {al.}  {v. et} {=> HILDEG}

        ANHÄNGER c. gen. inhaerentiae:
        * BERTH. chron. B; a. 1077 p. 291,5  "inaudita hactenus {!discordiarum} #sci|.sma.|ta ((* CONST. imp. II; 200,4 "d. #sci|.smati.|bus"))."
        * ; a. 1078 p. 323,21  "controversie #sci|.sma.|ta ... sedata."
        * CONSUET. Marb.; 60 "ut ... dissensionis #scisma (#schism.|a {var. l.}) vitetur."

      UUU_BEDEUTUNG publ.:
      * HIST. Lang. Goth.; 5 p. 10,2  "plures annos #scisma et bella inter Langobardos et Romanos fuerunt."
      * REGINO chron.; a. 890 p. 135,15 "erat ... inter Alanum et Vidicheil, ducibus Brittonum, non parva de partitione regni dissensionum controversia; in hoc ... #sci|.smat.|e et divisione, non tantum terrarum quantum mentium, pagani ... super eos {(Brittones)} ... irruunt."
      * BERTH. chron. B; a. 1077 p. 257,21  "Teutonum gentes omnino inter se #sci|.smat.|e non modico discordes."
      * ARNOLD. LUB. chron.; 2,18 p. 138,11  "cum de electione regis #scisma esset inter eos {(sc. principes et episcopos)} {eqs.}"
      * CAES. HEIST. hom. exc.; 248  "tempore #sci|.smat.|is inter Ottonem et Philippum, reges Romanorum."
      {al.}

      UUU_BEDEUTUNG eccl.:
      * GUNDECH. lib. pont.; p. 252,15  "tempore #ci|.smat.|is ecclesie Eistetensi."
      * ANNAL. Engelberg.; a. 1159 ((MGScript. XVII; p. 279,11))  "quo {(papa)} defuncto factum est #scisma inter cardinales de sede apostolica."
      * CHRON. reg.; a. 1184 p. 133,35  "Arnoldus Trevirorum episcopus obiit; cui per #scisma eligentium duo subrogati sunt, Rudolfus ... et Volmarus."
      * ANNAL. Spir.; p. 81,49  "#scisma de celebracione adventus Domini decisum est."
      {al.}

  UU_BEDEUTUNG defectio, apostasia -- Abspaltung, Abtrünnigkeit, Abfall:
      UUU_BEDEUTUNG publ. et eccl.:
      * CONC. Karol. A; 50^{D},9 p. 616,7  "cum ... in apostasiam aut infidelitatis aut heretice pravitatis aut fraterni #schismat.|is prolabitur {(sc. baptizatus)} {eqs.}"
      * BERTH. chron. B; a. 1077| p. 274,16  "non minima #sci|.smat.|um diversiclinia per totum regnum exorta sunt."; p. 281,13  "regi Heinrico, omnium bonorum suorum {(sc. populi)} incensori et devastatori et tot heresium et #sci|.smat.|um auctori et defensori ... semper adherere." {al.}
      * BERNOLD. CONST. libell.; X 24 p. 122,19  "quemlibet ex heresi vel #sci|.smat.|e conversum sanctae ecclesiae per manus impositionem redintegrari."
      * OTTO FRISING. gest.; 2,28 p. 133,17  "cuiusmodi hominum ingenia ad fabricandas hereses #sci|.smat.|um_que perturbationes sunt prona."

      UUU_BEDEUTUNG theol.:
      * PASS. Petri et Pauli; 347  "de Anania et Saphira; principio nascens dampnatur #schism.|a nefandum, voce Petri et coniunx ictus uterque ruit."
      * HILDEG. scivias; 1,3 l. 420  "praesumptio huiusmodi {(sc. stellarum et al.)} sciscitationis orta est in primo #schismat.|e, videlicet cum homines Deum ita oblivioni dedissent, quod {eqs.}"

  UU_BEDEUTUNG cessio -- Abtretung:
  * CONST. imp. III; 141,4 (a. 1277)  "petivimus, ut servitores nostri intacti a corpore regni nostri nobis et nostris heredibus integraliter remanerent, quolibet #sci|.smat.|is ad personam aliam scrupulo relegato."



ABSATZ
BEDEUTUNG meton.:

  U_BEDEUTUNG doctrina erratica (#schism.|a efficiens) -- falsche, 'schismatische' Lehre, Anschauung, Irrlehre:
  * AETHICUS; 58^{c}, 6  "Histria multa induxit #sci|.sma.|ta hereticorum magistrantium."
  * MANEG. c. Wolfh.; 23 p. 101,19  "discreti viri ... #scisma vestrum libera detestatione inpugnantes."
  * CARM. de Frid. I. imp.; 821  "expulit ecclesie doctorem {(sc. Arnoldum)} #scisma docentem."
  * CHART. Turg.; III 396 p. 157,21  "comes ... et universitas civium ... ad refrenandam intencionem emulorum et seminatorum #ci|.smat.|is ... talem tractatum ... confirmarunt."
  * ALBERT. M. summ. theol. II; 7,28,3 p. 315^{a},29  "illa {(tentatio)} ..., quae est aliter sapere, quam res se habet cum bono animo, non usque ad singularitatem sensus condendi #schismat.|is vel haeresis, sumpta est penes opus rationis."

  U_BEDEUTUNG tempus scissionis, scissurae -- Zeit(raum) der Spaltung, des 'Schismas':
  * CHRON. Hild.; p. 848,15sqq.  "Volcmarus Mindensis episcopus in #sci|.sma.|te obiit a<nno> 1097; Erpo Monasteriensis episcopus in #sci|.smat.|e obiit a<nno> 1097 IV. Id. Nov.; ... Eppo praepositus ... Wormaciensi episcopo Adalberto superponitur in #sci|.smat.|e, quod fuit inter Romanam aecclesiam et Heinricum quartum regem."

  U_BEDEUTUNG de parte ex #schismat.|e proveniente:

    UU_BEDEUTUNG mus. i. q. dimidium commatis (Pythagoreici) -- Hälfte eines (pythagoräischen) Kommas ((*{de re v.} LexMusLat. II.; p. 1100sq.)):
    * ANON. mus. Bernhard II; 72 "est ... comma ultimus sonus auditui subiacens, dimidium commatis unum #schism.|a dicitur."
    * FRUTOLF. brev.; 4 p. 44,32  "dimidium commatis #cisma ... vocatur."
    * CONR. HIRS. dial.; 1697  "quae sit maioris quaeve minoris semitonii ratio, quae limmatum, #sci|.smat.|um et diascismatum natura et constitutio."

    UU_BEDEUTUNG eccl. i. q. pars, factio -- (Streit-)Partei:
    * LAMB. TUIT. Herib.; 1,4 p. 743,5  "Colonię metropolitanus a sęculo migrat, et post exequias ... utrinque his et illis parti suę faventibus dividitur ęcclesia; tum eiusdem apostolicę sedis prępositus ... medium se offert in utroque #sci|.smat.|e."



ABSATZ
BEDEUTUNG translate:

  U_BEDEUTUNG dubitatio, divisio in seipso -- Zweifel, Ungewissheit, innerer Zwiespalt:
  * BERENG. TREV. laud.; 3,7 p. 974^{A}  "in quorumdam cordibus magnum inde saepius ortum est #schism.|a, quod hic de Eusebio et illic a Sylvestro legitur {(sc. Constantinus imperator)} accepisse baptisma."
  * HILDEG. epist.; 42  "lux {(Pitra, "lex" ed., v. notam ed.)} dat vitam et tenebrae #schisma.|ta et nocturnale tempus tristitiam; homo, qui vitam habere cupit, #schisma.|ta non habeat."

  U_BEDEUTUNG maleficium -- Übeltat:
  * EGBERT. fec. rat.; 1,1328  "mispilus et cerasus puerorum #sci|.sma.|ta questę ab divis quondam pacem petiisse feruntur."

  U_BEDEUTUNG diversitas -- Unterschied, Verschiedenheit:
  * OTLOH. doctr.; 39 p. 295^{C}  "tu ... divisum per linguae #schisma.|ta mundum et varios ritus coadunasti, pie Flatus (({spectat ad} * $VULG.; gen. 11,7sq.; act. 2,6sq.))."
  * CONR. MUR. nov. grec.; 2,824  "dicas audacter, quod nummus sive caracter nummi sit nummisma, nec in sensu noto #scisma (#cisma {var. l.}), sed nummum, nummisma, numam dare, dico, numisma."

  U_BEDEUTUNG disturbatio, perturbatio -- Zerstörung, Zerrüttung:
  * IUSTIN. Lippifl.; 186  "livor edax, animae virus letale, noverca pacis, amicitiae #scisma, ruina boni."

  U_BEDEUTUNG discordantia, dissonantia -- Dissonanz ((* {usu mus., de re v.} LexMusLat. II.; p. 1101sq.)); in imag. {=> imag_3}:
  * HILDEG. epist.; 169^{R} l. 11  "moveamus {(sc. quattuor seniores)} fistulas varietatis squalidorum morum et deauratas chordas illusorum ac #schisma.|ta {@ imag_3} #schismat.|um {@ HILDEG}."
  * ANON. mus. Sowa; 4 p. 260,19  "sunt, bene qui pangunt, cum voces scemata (gloss.: id est concordantias) tangunt; qui si discordent, tunc voces #sci|.sma.|ta (gloss.: id est discordantias) mordent." {ibid. al.}


AUTOR Berktold

UNTERDRÜCKE WARNUNG 516