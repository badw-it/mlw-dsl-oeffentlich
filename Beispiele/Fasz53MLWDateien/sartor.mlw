LEMMA sartor

GRAMMATIK
    subst. III; -oris m.

SCHREIBWEISE
    script.:
        sca-: * CHART. Tirol. I; 1151 p. 194,18
        ser-: {=> sartor_1a}    * {adde} CHART. Bund.; 1068 p. 517,25
        sest-: {=> sartor_4}
        sir-: * CHART. Neocell. Frising.; 33^C p. 109,39 {(var. l.)}


BEDEUTUNG qui vestes sarcit, facit, consutor, sarcinator -- (Flick-)Schneider, 'Nähter', 'Schröter'; usu attrib.: {=> sartor_3}; per compar.: {=> sartor_6}; spectat ad artem acu pingendi: {=> sartor_5}:

    UNTER_BEDEUTUNG in univ.:

        * LEG. Burgund. const. I; 21,2 "quicumque ... servum suum ... #sartor.|em {@ sartor_3} (#ses|.torem {@ sartor_4} {var. l.}) vel sutorem ... artificium exercere permiserit {eqs}."

        * STATUT. Murb.; p. 444,8 "instruendi sunt ... fullones, #sartor.|es, sutores non forinsecus, ... sed intrinsecus."

        * EPIST. Teg. I; 34 p. 39,4 "qui {(domestici)} nos {(sc. fratres)} ... perturbant victum vestitumque ... exposcentes, #sartor.|es videlicet, ... coci {eqs.}"

        * WILH. HIRS. const.; 2,32 p. 187,15 "adiutor kamerarii portat {(sc. vestes ruptas)} foras ad #sartor.|es {(v. notam ed.)}."

        * BERTH. RATISB. serm. exc.; p. 17,23 "ut ... #sarto.|r {@ sartor_6} sine forfice et acu, ...ita fides sine operibus {(sc. inutilis est)}."

        {al.}


    UNTER_BEDEUTUNG de fratribus collegii opificum: 

        UU_BEDEUTUNG usu vario:

            * CHART. civ. Erf.; 160 p. 92,31 (a. 1256) "cameras pellificum ... super cameras #sartor.|um sibi inherentes ... vendidimus {(sc. vicedominus, scultetus et alii)}."

            * CHART. select. Keutgen; 274 p. 368,39 "cum ... quodlibet genus hominum ... artes mechanicas exercencium ... #sartori.|bus exceptis ... confratrias habeant {eqs.}"

            * NICOL. BIBER. carm.; 1765 "sunt ibi {(sc. in civitate Erfordensi)} #sartor.|es {@ sartor_5} (pictores {G}), quorum manus addere flores novit, ut in veste pictura notetur honeste."

            {persaepe.} 

                ANHÄNGER pro cognomento:

                    * CHART. Ticin.; 16 p. 41,16 (a. 1196) "ego Adam #Se|.rto.|r {@ sartor_1a}, imperialis aule notarius, ... hoc breve scripsi (({sim.} 30 p. 71,7 "ego Allam<anus n>otarius, qui dicor #S|.arto.|r de Lugano"))."

                    * CHART. Turic.; 624 p. 130,15 "Petrus dictus #S|.arto.|r Lucernensis."

                    * CHART. ord. Teut. (Hass.); 317 "Conradus Phisicus, Theodericus Institor, Ripertus #S|.arto.|r et quidam alii."

                    {persaepe.}


        UU_BEDEUTUNG spectat ad diversa #sartor.|um genera:

            * CHART. Babenb.; 232,55 (spur. a. 1276/77) "quod artifices sive operarii manuales, ut sunt ... #sartor.|es tam vestium quam pannorum, ... plus iuris habeant quam exteri civitatis."

            * CONR. MEND. Attal.; p. 132,3 "quidam praedives ... arte sua pellicularum #sarto.|r."


BEDEUTUNG qui calceos facit, sarcit, sutor -- (Flick-)Schuster:

    * HERBORD. Ott.; 1,27 "aiunt illum humilitatis causa ocreas vel subtalares dissutos (dissolutos {var. l.}) plerumque ad #sartor.|em misisse."

AUTORIN Niederer