LEMMA  sapientia (-cia)

GRAMMATIK  substantivum I; -ae f.

SCHREIBWEISE

    script.:

        sapia: {=> sapientia011}
        sabi-: {=> sapientia003} {adde} * ALCUIN. epist.; 174 p. 288,32 {(var. l.)}

STRUKTUR

    form.:
        
        acc. sg.:

            -a: {=> sapientia001} {adde} * FORM. Senon. II; add. 4,2

    confunditur c.:

        patientia: {=> sapientia004}

BEDEUTUNG  σοφία, intellectus, prudentia, ratio, scientia (vera) -- Weisheit, Einsicht, Verstand, Klugheit, Vernunft, (wahres) Wissen:

    U_BEDEUTUNG  def.:
                
        * TRACT. de divis. phil.; p. 36,35 "#sapienti.|a est vera cognitio rerum."

        * ALBERT. M. Iob; 12,12 p. 165,34 "#sapienti.|a duobus modis dicitur: ... #sapienti.|a enim dicitur scientia, cuius finis intus est, et sciendi gratia, et quam difficile est homini scire, et sic dicitur stricte #sapienti.|a; communiter autem dicitur #sapienti.|a virtus scientiae vel artis {eqs.}"; 28,12 p. 315,30 "proprie dicitur #sapienti.|a ... acceptio intellectus de primis, altissimis, difficillimis homini scire eo, quod {eqs.}"; summ. theol. I; 15,60,1 p. 600^{a},43 "magister ... dicit, quod #sapienti.|a vel scientia de omnibus est bonis et malis, praesentibus, praeteritis et futuris, temporalibus et aeternis." {al.}

ABSATZ

    U_BEDEUTUNG  exempla:
    
        UU_BEDEUTUNG  gener.:

            UUU_BEDEUTUNG  spectat ad homines:

                UUUU_BEDEUTUNG  usu communi:

                    * HUGEB. Wynneb.; 4 p. 108,46 "ad meliora spiritalis vitae disciplina et secularis vitae #sapienti.|a {@ sapientia001} proficiebat."

                    * ALCUIN. epist.; 117 p. 173,8 "servus Dei #sapienti.|ae (#sab|.ien.|cia {@ sapientia003} {K2}) gazas in sui pectoris saccos cotidie congregare studeat."

                    * ANAST. BIBL. epist.; 5 p. 407,17 "o #sapienti.|am mundi, quae infatuatur et destruetur."

                    * NOTKER. BALB. Gall.; gest. 1,1 | p. 1,13 "si quis #sapienti.|ę cupidus est, veniat ad nos {(Scottos)}, ... nam venalis est apud nos."

                    * ODILO CLUN. Adelh.; 1 p. 28,20 "incomparabilis in divina et humana #sapienti.|a sacer Hieronimus (({sim.} 10 |))."

                    * WIPO prov.; 7 "melior est #sapienti.|a quam secularis potentia."
                
                    * ABSAL. serm.; 6 p. 46^{B} "sunt ... humanae #sapienti.|ae tres species: ... prima est #sapienti.|a mundialis, ..., secunda est #sapienti.|a carnalis ..., tertia #sapienti.|a animalis." {persaepe.} {v. et} {=> sapientia010}

                UUUU_BEDEUTUNG  in imag.:

                    * THIETM. chron.; 6,59 "habitatorum animos #sapienti.|ae freno edomans {rex}."

                    * EPIST. Hild.; 134,26 "cathedra #sapienti.|ę (philosophie {var. l.}) vernantissima, ... camera #sapienti.|e ... preciosissima, ... suavissimum ... pulvinar #sapienti.|e." {al.}

                UUUU_BEDEUTUNG  spectat ad artes liberales q. d.:

                    * BRUNO QUERF. Adalb. A; 4 "Wogithic ... ad septem panes #sapienti.|ę portatur; traditur ... ad ... Parthenopolim ... liberalibus disciplinis imbuendus."

                UUUU_BEDEUTUNG  meton. de scriptoribus mundanis:

                    * HRABAN. carm.; 23,15 "quicquid #sapienti.|a mundi protulit in medium temporibus variis."

                UUUU_BEDEUTUNG  per prosop.:

                    * SEDUL. SCOT. (?) carm.; app. 2,17 "sophia vocor {(sc. puella)} graece, #sapienti.|a Romae."

                    * CARM. Sangall. II; 5,3,3,4 "o generosa parens cunctis gratissima doctis, ... quam praeclara nites toto #sapienti.|a mundo!"

                    * HROTSV. Sap.; 3,6 "eminentiores Italiae principes fuere mei parentes et vocor #S|.apienti.|a."
                
                    * RAPULARIUS I; 351 "in sacco sedeo, sedet hic #S|.apienti.|a mecum." {al.}

                UUUU_BEDEUTUNG  in formulis aut titulo honorifico:

                    * TRAD. Frising.; 101 (a. 779/83) "per vestram benivolentiam et #sapienti.|am requiratur."

                    * SALOM. II. epist.; 24 p. 409,23 "quatenus apud vestram {(sc. episcopi)} #sapienti.|am aliquid e multis ediscere possit {iuvenculus}."

                    * OTTO FRISING. gest.; (epist. Frid. I. imp.) p. 1,7 "cronica, quae tua #sapienti.|a digessit."

                    * CONST. imp. III; 76 p. 64,37 "sacerdotium et imperium non multo differre merito #sapienti.|a civilis asseruit."

                UUUU_BEDEUTUNG  pro cognomine:

                    * TRAD. Patav.; 33^a p. 29,38 (a. 789/91) "tradidi {(sc. Irminsuind)} cellulam meam ... in manu sanctimoniali nomine #S|.apienti.|a."

                    * LIBER confrat. Trev.; ((ArchMittelrhKirchGesch. 7. 1955.; p. 245,45)) "Uta, Gunther, #S|.sapienti.|a, Tidericus {eqs.} (({sim.} p. 249,49))."

                    * CHART. Eberb.; 277 p. 34,5 "universa predicta ... ydoneis viris presentibus sunt peracta, scilicet ... Ebberardo, Heinrico #S|.apienti.|e, Iohanne {eqs.}"

            UUU_BEDEUTUNG  spectat ad res:

                UUUU_BEDEUTUNG  gener.:

                    * HRABAN. carm.; 18,37 "in hac {(lege caelesti)} vera ... fulget #sapienti.|a."

                    * RUOTG. Brun.; 21 p. 22,22 "animi vigorem ad maiora #sapienti.|ę et virtutis opera direxit."

                    * OTTO FRISING. gest.; 2,29 p. 135,26 "urbs Roma ex senatoriae dignitatis #sapienti.|a ... a mari usque ad mare palmites extendens."; 2,30 p. 136,33 "verba plus arrogantiae tumore insipida quam sale #sapienti.|ae condita."

                UUUU_BEDEUTUNG  anat. de dentibus:

                    * ALBERT. M. animal.; 12,217 "hii {(dentes)} ideo vocantur dentes #sapienti.|ae vel intellectus, eo quod ut frequentius nascuntur in aetate intellectus, quae est XXX annorum."

            UUU_BEDEUTUNG  spectat ad animalia:

                * ALBERT. M. animal.; 8,66 "grues ... quodam genere ordinatissimae #sapienti.|ae utuntur in volando, quando {eqs.}"

        UU_BEDEUTUNG  eccl. et theol.:

            UUU_BEDEUTUNG  in univ.:

                * WILLIB. Bonif.; 3 p. 11,30 "patrum #sapienti.|am imitatus est."

                * HUGEB. Willib.; 2 "in eo ... spiritalis germinabat #sapienti.|e virgultus."

                * RADBERT. corp. Dom.; 21,51 "salutis potum sumimus, quoniam et #sapienti.|am doctrinae ex illo habemus et redemptionis praetium inde pro nobis offerimus."
            
                * HINCM. ord. pal.; 587 "a Deo data #sapienti.|a."
            
                * OTLOH. prov.; L 17 "lucerna corporis est oculus; lux autem mentis #sapienti.|a spiritualis."
            
                * RUP. TUIT. trin.; 35,23 "#sapienti.|a, quam ... spiritus sanctus efficit ..., hoc ... differt a #sapienti.|a {@ sapientia010} saeculi, quod {eqs.}"
            
                * HERM. CARINTH. essent.; 1 p. 88,23 "extra veram divinitatis fidem nullum locum #sapienti.|e esse." {persaepe.}

                    ANHÄNGER  per prosop.:

                        * RADBERT. corp. Dom.; 21,55 "in Salomone legimus, quod #sapienti.|a miscuerit in crateras vinum suum ((* {spectat ad} $VULG. prov.; 9,1sq.))."

                        * GODESC. SAXO gramm.; 1 | p. 406,12 "'subdidit sibi' #sapienti.|a 'gentes', quia nos gentiles, qui ligna lapidesque colebamus, ecce iam sumus ei per suam misericordiam servientes (({item} theol.; 22 p. 297,12))."; p. 417,14 "habet ... #sapienti.|a 'contubernium Dei', quia {eqs.}"

                        * MILO carm.; 3,4,2,329 "hinc Salomon, docuit sua quem #sapienti.|a mater, ... ait: {eqs.}"

                        * RUOTG. Brun.; 26 "#sapienti.|ę hęc {(sc. sacrae scripturae)} verba sunt." {al.}

            UUU_BEDEUTUNG  in locut. "divina -a" spectat ad doctrinam christianam:

                * LIUTG. Greg.; 3 "crevit ... decore #sapienti.|ae divinae secundum formam magistri."
            
                * HRABAN. epist.; 37 p. 474,30 "ad discendam #sapienti.|am divinam."
                
                * ALBERT. M. summ. theol. I; 15,60,1 p. 600^{b},5 "divina #sapienti.|a ... vocatur et praescientia et dispositio et praedestinatio et providentia {eqs.}" {al.}

            UUU_BEDEUTUNG  spectat ad Deum:

                * LIUTG. Greg.; 4 "laici quam clerici videntes #sapienti.|am Dei."

                * HRABAN. epist.; 5 p. 388,20 "quod {(praeceptum)} per vas sibi aptum olim #sapienti.|a protulit dicens: '{eqs.}' (($VET. LAT. Sir.; 7,31.35))."

                * LIBELL. prec. Trec.; 1,4 "tu es {(sc. sancta trinitas)} #sapienti.|a (({cf.} * LIBELL. prec. Paris.; 2,7 "paciencia" {(perperam)})) mea clara; ... tu es #sapienti.|a {@ sapientia004} ((* LIBELL. prec. Paris.; 2,7 "paciencia")) mea robustissima."

                * HRABAN. epist.; 45 p. 499,28 "utrum ... liceret ... trinam et unam deitatem ... et trinam et unam #sapienti.|am dicere."
            
                * ALFAN. premn. phys.; 1,25 p. 10 "ad enarrandum ... #sapienti.|am creatoris."
            
                * ALBERT. M. incarn.; 24 p. 196,7 "incarnatio magis est opus #sapienti.|ae quam bonitatis."
            
                * CATAL. thes. Germ.; 136,115 "in medio ... est depicta ymago #S|.apien.|cie."
            
                * ALBERT. M. sent.; 1,10^Ccapit. "sicut verbum Dei ... dicitur #sapienti.|a, et tamen tota trinitas dicitur #sapienti.|a." {persaepe.}
            
                    ANHÄNGER  de una antiphonarum in vesperis tempore adventus cantandarum:

                        * STATUT. ord. Teut.; p. 12,17 "hic incipitur 'o #sapien.|cia' (({sim.} p. 123,1 "quando illa antiphona 'o #sapien.|cia' incipitur."))." {persaepe.}

            UUU_BEDEUTUNG  spectat ad solum Christum:

                * AUDRAD. carm.; suppl. 1,5 "Filius omnipotens, summi #sapienti.|a Patris (({sim.} * HRABAN. carm.; 34,2 "summi Patris #sapienti.|a, Christe." * IOH. SCOT. carm.; 2,2,57. {al.}))."

                * CAND. FULD. Eigil. II; 9,11 "posce Deo placita, si scis; si poscere nescis aut tibi si desit veri #sapienti.|a verbi {eqs.}"                

                * CONST. Constant.; 43 "quando ... solo <<suae #sapienti.|ae>> (#sapienti.|ę suę {var. l.}) verbo universam ex nihilo formavit {Deus} creaturam."
            
                * ALBERT. M. incarn.; 3 p. 174,1sqq. "donum gratuitum datur a Deo appropriabile alicui personae sicut donum #sapienti.|ae, intellectus vel consilii ... sunt dona appropriabilia Filio, qui est #sapienti.|a Patris." {persaepe.}

            UUU_BEDEUTUNG  spectat ad spiritum sanctum:

                * CONSUET. Marb.; 201 "spiritum #sapienti.|ę et intellectus discretionisque digneris {(sc. Deus)} infundere."

            UUU_BEDEUTUNG  spectat ad ecclesias vel sanctos:

                * TRAD. Patav.; 33^b p. 29,3 "tradicio Yrminsvinde ad eclesiam #S|.apien.|ciae."

                * WETT. Gall.; 24 "Gallus vir Dei ... instructus de scripturis ac plenus #sapienti.|a."

                * WALAHFR. Gall.; 124 "mittitur ad quendam ..., cuius multa viret #sapienti.|a dogmate, Scottum."

                * WALAHFR. Mamm.; 26,15 "sancti #sapienti.|a Moysi." {al.}

            UUU_BEDEUTUNG  spectat ad libros sacrae scripturae:

                * LIUTG. Greg.; 12 p. 76,12 "erat ... vir ... venerabilis secundum #sapienti.|ae dictum, '{eqs.}'"

                * DIPL. Heinr. IV.; 434^a/^b p. 581,5/3 "dicit namque #sapienti.|a: '{eqs.}' (({item} 437 p. p. 585,12))."

                * CONSUET. Marb.; 36 (add.) "per #sapienti.|am dicitur: '{eqs.}'" {al.}

                    ANHÄNGER  in nominibus librorum sacrae scripturae ((* {i.} $VET. LAT. sap.; {al.})):

                        * HRABAN. epist.; 20 p. 425,35 "quod {(volumen)} in explanationem libri #sapienti.|ae ... edidi."

                        * CAND. FULD. Eigil. I; 9 p. 226,44 "propter peccata ... homines famem ... perpessos esse #sapienti.|ae liber ... proclamat." {al.}

        UU_BEDEUTUNG  alleg.:

            * AMALAR. off.; 3,5,19 "postquam transit vicarius Christi ..., #sapienti.|a, id est subdiaconi, vadunt in medio adolescentolarum tympanistriarum {eqs.} (({cf. antea} 3,5,18 "ut moderate prophetia disponatur, habeat ante se subdiaconorum #sapienti.|am, scilicet ut congruo tempore prophetent"))."

            * ALLEG. cam.; 26 "camarariae Christi virtutes spiritales sunt: ... tertia est sancta #sapienti.|a; illa facit luminaria in camara."

        UU_BEDEUTUNG  philos.:

            UUU_BEDEUTUNG  in univ.:

                * ALFAN. premn. phys.; 41,4 p. 132 "vocant theoreticum quidem intellectum, practicum vero rationem; et illud quidem #sapienti.|am, hoc vero prudentiam."

                * HERM. CARINTH. essent.; 1 p. 100,5 "illa {(sc. commixtio est)} inter omnes #sapienti.|e materias fallacissima."

                * OTTO FRISING. chron.; 5 prol. p. 226,17 "qui {(priores)} ante nos #sapienti.|ae studuerunt."

                * ALBERT. M. bon.; 494 p. 257,28 "in fine I ethicorum, ubi virtutem intellectualem videtur ... dividere et ponit tres, scilicet #sapienti.|am, phronesim et intelligentiam." {persaepe.}
    
            UUU_BEDEUTUNG  de studio -ae ((interdum i. q. philosophia -- Philosophie)):

                * LIUTG. Greg.; 9 "secundum stultam #sapienti.|am seculi huius."

                * ALCUIN. epist.; 307 p. 466,23 "#sapienti.|a est, ut philosophi diffinierunt, divinarum humanarumque rerum scientia."

                * HRABAN. univ.; 15,1 p. 418^{D} "#sapienti.|a aliquando huius mundi philosophia nuncupatur."

                * HUGO RIPELIN compend.; 2,10 p. 48^{a},5 "studium #sapienti.|ae coepit in Aegypto."

        UU_BEDEUTUNG  alch.:
        
            UUU_BEDEUTUNG de quodam genere caementi ((* {de re v.} J. H. Zedler, Grosses vollständiges Universal-Lexicon aller Wissenschafften und Künste. 1771-1773.; vol. XVIII. p. 1353)):

                * PS. ARIST. magist.; p. 642^{b},61 "claude os eius {(sc. ollae vitreae)} cum cooperculo lutato luto #sapienti.|ae, ita ut {eqs.}"

                * GEBER. clar.; 1,48 "liga ipsam {(sc. limaturam Martis et quartam partem aeris usti)} in petia panni lini, et facias ipsam petiam cum confectione rotundam sicut pomum, et super istam petiam luta cum luto #sapienit.|e et pone ad desiccandum."

                * ALBERT. M. miner.; 4,1,7 p. 93^a,16 "conglutinatur in loco contactuum {(sc. aurum)} et tenaci luto, quod #sapienti.|ae lutum vocant alchimici."

                * MARC. GRAEC. ign.; 1 "haec simul pista et in vase fictili vitrato et luto #sapiae {@ sapientia011} (prudentiae {var. l.}) diligenter obturato dimitte." {al.}

            UUU_BEDEUTUNG  in nomine ficticio ut vid. de fermento sim.:

                * PS. ARIST. interpr.; p. 387 "rarum ... rerum fixarum naturam liquefactione coaptat et aqua, quae a sapientibus aqua #sapienti.|ae est appellata {eqs.}"

ABSATZ

BEDEUTUNG  doctrina, eruditio, educatio -- Wissen, Bildung, Gelehrsamkeit:

    * GERB. epist.; 187 p. 225,3 "thesauros sibi Grecię ac Romanę repetit {(sc. imperator)} #sapienti.|ę."

    * NADDA Cyriac. I; prol. 9,46 "cuius {(sc. Bedae)} industria ... Scottica terra late pollet #sapienti.|a librorumque opulentia."

    * CONST. Melf.; 2,31 "nos {(sc. augustus)}, qui veram legum #sapienti.|am prosequimur."

AUTOR  Fiedler