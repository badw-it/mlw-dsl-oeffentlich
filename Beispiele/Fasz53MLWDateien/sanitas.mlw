LEMMA sanitas

GRAMMATIK 
    subst. III; -atis f.

FORM 
    gen. pl. (m.):
        -uum: {=> sanitas_1}

METRIK
    metr.:
        săn-: {=> sanitas_2}


BEDEUTUNG (bona) valetudo, incolumitas -- (gute) Gesundheit, (Wohl-)Befinden, Unversehrtheit: 

    UNTER_BEDEUTUNG proprie:

        UU_BEDEUTUNG strictius:

            UUU_BEDEUTUNG gener.; usu plur.: {=> sanitas_4}:

                * EPITAPH. var. I; 3,6 "his {(Boso rex)}, <<dum vita fuit #sanita.|s{@ sanitas_2}>> (bona dum valitudo fuit {$L^2}) et leta maneret, munera multa dedit patrono {(sc. Mauritio)}."

                * DIPL. Arnulfi; 190 p. 295,17 (spur.) "postquam ... Karolus ... ęcclesiam prestitit, nunquam postea victoriam habuit neque corporis et animi #sanitat.|em."

                * CHART. Aquens.; 198 l. 7 "decanus ... in plena #sanitat.|e sui corporis et sui compos integraliter existens {eqs.}"

                * CHART. Mekl.; 879 "ad recreandas infirmantium et debilium #sanitat.|es {@ sanitas_4} conventui ... duas villas ... assignavimus {(sc. praepositus)}."

                {persaepe.}


                    ANHÄNGER in formulis epistularum:

                    * CONC. Karol. A; 44^A p. 478,25 "ut ... scire possimus {(sc. imperatores Romanorum)} vestrae {(sc. Ludowici imperatoris)} dilectionis #sanitat.|em."

                    * EPIST. Worm. I; 5 p. 21,7 "quanta benignitate ... vestro {(sc. episcopi)} se patrocinio ... visitari meminerit {imperatrix}, crebra atque sollicita vestrę #sanitat.|is interrogatio manifestat (({sim.} 29 p. 53,26 "si vestrę {(sc. regis)} #sanitat.|is statum per epistolam vestri scire promererer {(sc. E.)}"))."


            UUU_BEDEUTUNG medic. et natur.:

                * ANTIDOT. Lond.; p. 17,13 "confectio Hadriani ..., qui ... adducit ad pristina #sanitat.|is."

                * CONSTANT. AFRIC. coit.; 8,2 "res, que conservant #sanitat.|em (salutem {var. l.}), sunt exercicium, balneum {eqs.}"

                * FRID. II. IMP. art. ven.; 6| p. 188,8 "nisi falco ... haberet signa #sanitat.|is."; p. 203,29 "potest ... cognosci fortitudo falconis et #sanita|s in renibus."

                * ALBERT. M. Is.; 1,6 p. 16,85 "#sanita.|s ... est adaequatio humorum ad complexionem et proportio virtutum et membrorum ad debitam figurationem corporum."

                {saepe.}

                    ANHÄNGER in titulo libri:

                        * REGIMEN san. Salern.^{tit.}; p. 65 "regimen #sanitat.|is Salerni."

        
        UU_BEDEUTUNG latius i. q. salus, sospitas -- Heil, Wohl(ergehen), Glück:

            UUU_BEDEUTUNG de salute animae ('Seelenheil'):
     
                * GODESC. SAXO theol.; 15| p. 241,6 "disputat {Augustinus} ... de #sanitat.|e sive sanatione animae (({sim.} p. 242,8 "per gratiam Christi vivificantur electi percepta #sanitat.|e a vitio peccati {eqs.}"))."

                * DIPL. Otton. I.; 293 "pro #sanitat.|e ... nostra dilectaeque coniugis nostrae ... duas civitates ... in perpetuam proprietatem transfundimus (({sim.} 312. 319. {al.}))."

                * ALBERT. M. sacram.; 135 p. 91,58sqq. "est {(sc. in anima)} duplex #sanita.|s, scilicet causalis, quae est idem quod iustificatio remittens peccatum, et formalis, quae {eqs.} (({sim.} 138 p. 92,80.; sent.; 4,17,19 p. 689^a,35sqq.))."

                * DAVID compos.; 3,19,2 p. 199,6 "'#sanita.|s cordis' in tribus consistit: {eqs.}"

                    ANHÄNGER iunctura "#sanita.|s perpetua, aeterna":

                        * CAND. FULD. Eigil. I; 7 "rogabatur Aegil ..., ut regimen ... monasterii suscepisset sibi ad praemium, fratribus vero ad exemplum, utrisque autem ... ad perpetuam #sanitat.|em (({sim.} 11 p. 229,9 "ad aeternam ... #sanitat.|em."; 23 p. 232,50))." 


            UUU_BEDEUTUNG de salute populi, mundi:
            
                * LEX Sal. Pipp.; prol. 3 (E) "pacem et felicitatem atque #sanitat.|em per infinita secula tribuat {(sc. Christus Francis)}."

                * ALCUIN. epist.; 226 p. 370,37 "multitudo sapientum #sanita.|s est orbis et laus civitatis, in cuius arce habitat."

      
            UUU_BEDEUTUNG de statu inviolato ecclesiarum:

                * NARR. de lib. Fab.; 9 "qui {(fratres)} ... magnificarent Deum ob loci sui #sanitat.|em ... receptam per apostolicam pietatem."

                * CHART. com. Mansf.; VI 15 p. 340,13 "scriptis autenticis robur addere firmitatis, illis maxime, per quorum mutationem, negligentiam vel oblivionem #sanita.|s ecclesiarum detrimentum valeat aut defectum."


    UNTER_BEDEUTUNG meton.:
    
        UU_BEDEUTUNG sanatio, restitutio, recuperatio bonae valetudinis -- Heilung, Genesung:

            UUU_BEDEUTUNG gener.; fere de sanationibus miraculosis; de sanatione actu superstitioso attentata: {=> sanitas_5a; sanitas_5b}:

                * PASS. Flor.; 9 "in quo {(sc. sepulcri Floriani)} loco fiunt #sanitat.|es magnae."

                * VITA Corb.; 38 " operari dignatus est {Deus} multorum (multarum {var. l.}) #sanitat.|uum {@ sanitas_1} (#sanitat.|um {var. l.}) miracula."

                * PONTIF. Rom.-Germ.; 136,13 p. 240,1sqq. "misisti filium aut filiam ... in fornacem pro aliqua #sanitat.|e {@ sanitas_5a} (necessitate {var. l.})? quinque annos peniteas; arsisti grana ... pro #sanitat.|e {@ sanitas_5b} viventium {eqs.}"

                * RUP. TUIT. Pant.; 1 ((AnalBoll. 55. 1937.; p. 255,2)) "quorum {(martyrum)} meritis #sanitat.|es crebrescunt."

                {persaepe.}


            UUU_BEDEUTUNG medic. et natur. ('Heilungsprozess'):

                * HILDEG. phys.; 2,4 "aqua eius {(sc. 'se')} nec cruda in potu nec cocta in cibo sumpta ad digestionem aut ad alias #sanitat.|es sana {@ sanus_27} est, quia de spuma maris oritur."

                * BRUNUS LONG. chirurg.; 1,8 p. 108^G "#sanita.|s {!vulneris} tardatur aut propter malam complexionem carnis, in qua existit, aut {eqs.} ((* {sim.} GLOSS. Roger. I A; 1,22 p. 553,24 "#sanita.|s v. VII de causis differtur: prima {eqs.}"))."

                * ALBERT. M. metaph.; 6,2,3 p. 309,18 "'aedificatorem ... <<#sanitat.|em facere>>' ((* ARIST.; p. 1026^b,37 "τὸ ὑγιάζειν")) dicimus secundum 'accidens, quia non convenit hoc aedificatori, sed medico' (({sim.} 7,2,10 p. 353,48sqq. {al.}))."

        UU_BEDEUTUNG materia sana -- gesunde Materie; usu medic.:

            * HILDEG. phys.; 6,3 l. 18 "solummodo sordes et putredines extrahit {(sc. vesica pavonis super cocturam posita)} et non #sanitat.|em."


    UNTER_BEDEUTUNG translate:
        
        UU_BEDEUTUNG corpor. i. q. integritas -- Ganzheit, unbeschädigter Zustand:

            * HONOR. AUGUST. spec.; p. 835^B "vas vitreum, quod in multas particulas dissiluit, pristinae #sanitat.|i restituit {(sc. Iohannes evangelista)}."


        UU_BEDEUTUNG spirit. i. q. sinceritas, puritas -- Aufrichtigkeit, Reinheit:

            * VISIO Godesc. A; 1,6 "persone simplicitas in corde versute loqui nescientis, verborum #sanita.|s rebus consona nec a fide dissona ... testimonio erant referenti veritatis."

AUTORIN Niederer

UNTERDRÜCKE WARNUNG 60