LEMMA sapphirinus (saph-)

VEL *sapphirineus

GRAMMATIK
         adi. I-II; -a, -um

ETYMOLOGIE σαπφείρινος

SCHREIBWEISE
         script.:
                 safi-: {=> sapphirinus_1; sapphirinus_4}
                 -hyr-: {=> sapphirinus_2}

BEDEUTUNG qui sapphiri est, ad sapphirum pertinens -- Saphir-:
          * FORM. Sangall.; II 39 "munuscula direximus, hoc est palliolum dium caedrinum et aliud coccineum, tertium #saph|.iri.|ni (#saf|.irini {@ sapphirinus_4} {a. corr. $C}) coloris
          ((
              * {sim.} MAPPAE CLAVIC.; 90 capit. "argentum colorem #saf|.iri.|num{@ sapphirinus_1}."
              * HILDEG. epist.; 117^R "in qua {[sc. mente instabili]} nubes #sapphirin.|ei coloris apparere non potest."
              {saepius}
           ))."
 BEDEUTUNG colore sapphiri praeditus, sapphiri similis -- saphirfarben, -blau, -ähnlich:
         * NOTKER. BALB. gest.; 1,34 p. 46,28 "habitus eorum {(Francorum antiquorum)} erat pallium canum vel #saph|.iri.|num (saphirium {@ sapphirius_1} {var. l.}) quadrangulum duplex."
         * HILDEG. epist.; 117^R "#sapphirin.|ea nubes."
         * ARNOLD. SAXO flor.; 3,44 "iacinthus #saph|.iri.|nus confert divicias."
         * ALBERT. M. animal.; 23,133 "pavo avis est ... longo collo #saph|.iri.|no, sed rufo capite, pectus etiam #saph|.iri.|num habens."; summ. theol. II; 10,39,2,3 p. 469^a,43 "in throno #sapphiri.|no ((*{spectat ad} $VULG.; Ez. 1,26))."
         * VITA Mariae rhythm.; 3159 "#saphyrinus {@ sapphirinus_2} circulus, quo cingebatur illa."
         {al.}

AUTORIN Weber