LEMMA scisco

GRAMMATIK
   verbum III; scivi, scitum, -ere

SCHREIBWEISE
   script. et form.:
      c-: {=> scisco_1} {=> scisco_6} {al.}
      si-: {=> scisco_2}
      -ss(o): {=> scisco_5}
      indic. perf.:
       scevunt: {=> scisco_3}

VERWECHSELBAR
   confunditur c.:
      sistere : {=> scisco_9}


BEDEUTUNG
   gener.: 
   
U_BEDEUTUNG
   sciscitari, percontari -- zu erfahren suchen, sich erkundigen (nach):

   * HARTW. Emm.; 119 "unus choro prosilit #sci.|tum, quis sic advenerit." 

   * CONR. MUR. nov. grec.; 1,2023 "a scio #scisc.|o (#c|.isco {@ scisco_6} {var. l.}), sed a #scisc.|o fert sciscitor (cissitor{@ scisco_7}, scissitor {@scisco_8} {var. l.}) ortum."


U_BEDEUTUNG
   investigare -- untersuchen:

   * ACTA civ. Wism. B; 2778 (a. 1273) "violenter fregerunt {(sc. fures)} nocte domum Willeri ... et dominas in domo spoliaverunt, quo facto Iohannes de Dammenhusen venit ad eos et #sci.|vit cum eis hoc malefactum."

BEDEUTUNG
   publ. et iur.:

U_BEDEUTUNG
    constituere, eligere -- bestimmen, wählen:

    * ANNAL. Zwif. II; a. 1199 (vs.) "suum fratrem proceres #c|.isc.|unt {@ scisco_1} sibi patrem, a quisdam set Oto gaudetque dato sibi sceptro."

U_BEDEUTUNG
    locut. {/fidem #scisc.|ere} i. q. spondere -- geloben, garantieren:

    * ACTA civ. Wism. B; 146 (a. 1273) "Tymmo Crus et Huderus Vergin ... #scevunt {@ scisco_3} fidem cum octo predictis viris, quod {eqs}."

U_BEDEUTUNG
   asciscere, advocare -- hinzuziehen, herbeirufen:

   * ALCUIN. carm.; 1,1154 ((ed. Godman)) "comes hunc {(Iohannem)} alius diverso tempore #scisc.|i ({ed.}; sisti {@ scisco_9}, #sci.|ssi {@ scisco_5} {codd.}) fecit ad ecclesiam Domini de more dicandam."



SUB_LEMMA scitus

GRAMMATIK
   adi. I;  -a, -um


BEDEUTUNG
   scius, sollers -- kundig, klug, aufgeweckt:

   * CONR. MUR. nov. grec.; 1,999 "vivax est #scit.|us, et prudens est homo #scit.|us (citus {var. l.})"

BEDEUTUNG
   aptus -- passend, trefflich:

   * RUOTG. Brun.; 7 "satis laudabile #scit.|um_que doctoris de discipulo testimonium."



SUB_LEMMA 1. scitus

GRAMMATIK
   subst. II; -i m.

BEDEUTUNG
   vir doctus, eruditus -- Gelehrter:

   * EKKEH. IV. pict. Mog.; 334 "virtutum testes redolent de chrismate vestes (gloss.: sacerdotales); #scit.|us (gloss.: expositor) habet fari, quibus (gloss.: rebus) hę similari."



SUB_LEMMA 2. scitum

GRAMMATIK
   subst. II; -i n.

BEDEUTUNG
   decretum, statutum, praeceptum -- Beschluss, Verordnung, Vorschrift:

   * ALCUIN. carm.; 1,129 "ultrices timuit capiti quia quisque secures, provida ni toto servaret pectore #scit.|a."

   * ANNAL. Altah.; a. 1044 p. 37,21 "illis {(sc. Hungaris)} ... concessit rex #scit.|a Teutonica {(v. notam ed.)}."

   * LAMB. HERSF. annal.; a. 1074 p. 192,1 "archiepiscopus eos {(nefarios homines)} ... ad suscipiendam {! secundum canonum} #scit.|a penitentiam ... presto esse iubet ((* BENZO ad Heinr. IV.; 1,9 p. 130,5 "s. #sita {@ scisco_2} {(v. notam ed.)} c|anonum" * DIPL. Frid. I.; 106 p. 180,26 "s. #scit.|a c|anonum" {al.}))."

   * OTTO FRISING. gest.; 2,15 p. 118,14 "ut ... debeant dignitates et magistratus ... secundum #scit.|a legum iurisque peritorum iudicium universa tractari."
   {saepe.}



SUB_LEMMA scite

GRAMMATIK
   adv.

BEDEUTUNG
   prudenter, perite -- klug, verständig:

   * GODESC. SAXO theol.; 6 p. 164,11 "#scit.|e distinxere plerique: {eqs}."

   *; 24 p. 340,6 "nec illud ... satis dictum est #scit.|e, circumspecte, circumcise, perite, quod {eqs}."



AUTORIN
   Orth-Müller
