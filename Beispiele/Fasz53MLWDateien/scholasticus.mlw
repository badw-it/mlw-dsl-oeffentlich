LEMMA scholasticus (sco-)

GRAMMATIK
     adi. I-II; -a, -um

ETYMOLOGIE σχολαστικός

FORM
     form.:
         superl.:
             -cissimus ((*{cf.} Stotz, Handb. 4,IX; § 48.3)): {=> scholasticus_2{sq.}} {al.} 

METRIK
     metr.:
         scō-: {=> scholasticus_1} *{adde} ALCUIN. carm.; 32,17.; 32,23

BEDEUTUNG in interpr. verbi Graeci i. q. otiosus, liber (ad discendum) -- müßig, frei (zum Lernen):
     * AMALAR. off.; 3,11,9 "recipiant {(sc. doctores)} #sco|.lastic.|as mentes, scilicet vacantes ab emptione villae, ab emptione boum quinque iugorum, a ductu uxoris."
ABSATZ
BEDEUTUNG ad scholam, studia pertinens, qui scholae est, scholicus -- zur Schule, zum Studium gehörig, Schul-, Studien-:

     U_BEDEUTUNG adi.:

         UU_BEDEUTUNG de rebus:
             UUU_BEDEUTUNG in univ.:
                 * ALCUIN. Willibr.; 1,4 ((MGMer. VII; p. 118,17)) "in Hibernia #sco|.lastic.|am eruditionem viguisse audivit."
                 * FORM. Augiens.; C 22 "bene procurat {dominus} eum {(fraterculum)} #sco|.lastic.|o pedagogio amicaliter docendo."
                 * BALTH. Fridol.; 1 p. 354,6 "dum in monasterio sancti Galli iuxta ... pedes magistrorum #sco|.lastic.|a vacare {!disciplina} desisterem
                 ((
                     * CHART. civ. Erf.; 50 "sub d. #sco|.lastic.|a. {al.}"
                 * CHART. episc. Hild.; II 768 p. 388,9 "qui {[Iohannes clericus]} diu #sco|.lastic.|is disciplinis insistens adeo proficisse dicitur ..., quod cathedram meruit ascendere magistralem"
                 ))."
                 {al.}

             UUU_BEDEUTUNG de studiis, litteris saecularibus:
                 * EKKEH. IV. prol. Gall.; p. 92,20 "quia in studia nos divina et #sco|.lastic.|a aeque illis {(sc. Notkero et Ratperto)} impendere pigritamur {eqs.}"
                 * GERHOH. glor.; 19,2 p. 1143^D "magister Petrus, egregius multarum et diversarum ecclesiasticarum et #scholastic.|arum .... sententiarum collector."

             UUU_BEDEUTUNG spectat ad aedificium:
                 * HUGEB. Willib.; 3 p. 92,1 "#sco|.lastic.|am scandere meruerunt {Willibaldus et Wynnebaldus} scalam {(sc. sanctam Lateranensem, v. notam ed.)} et preclaram petivere sancti Petri basilicam."

             UUU_BEDEUTUNG in titulo libri:
                 UUUU_BEDEUTUNG usu communi:
                     * OTTO SANBLAS. chron.; 12 p. 13,26 "alter {(sc. magister)} ..., Manducator {(i. Comestor)} scilicet, librum #Sco|.lastic.|e Hystorie utilissimum confecit
                     ((
                       *{sim.} CHART. Wirt.; app. 28 "#Sco|.lastic.|am Hystoriam pro quinque talentis redemit {Bertholdus abbas}. {al.}"
                     ))."

                 UUUU_BEDEUTUNG educationem describens -- die Ausbildung darstellend; usu ellipt.: {=> scholasticus_6}:
                     * WALTH. SPIR. Christoph. II; epist. ad coll. p. 10 "titulum ... libri I #Sco|.lastic.|um propter principia placuit vocari."; 1 prol.^tit. "incipit prologus in #Sco|.lastic.|um {@ scholasticus_6} Waltheri Spirensis aecclesię subdiaconi."; 1^tit. "incipit primus libellus de studio poetę, qui et #Sco|.lastic.|us {(sc. dicitur)}."

             UUU_BEDEUTUNG qui discipuli est -- dem Schüler eigen:
                  * WOLFHER. Godeh. I; praef. p. 168,52 "timui ... pro adolescentiori, quae mihi imminet, aetate ... #sco|.lastic.|ae adulationis, quae sui {(sc. Godehardi)} studium ... in utramque partem ipsa facilis facile deflectit, in his seriis apud increduliores incusari."

             UUU_BEDEUTUNG qui magistri scholae est -- zum Schulmeister gehörig:
                 * CHART. Onold.; 21 p. 27,2 (a. 1164) "ut, qui legitime et canonice predictam habeat canonicam, habeat et curam #sco|.lastic.|am, et si uno careat, altero non fruatur."
                  * CHART. Turic.; 1476| p. 187,19 "ipsum {(Bertoldum)} in ... nostra ecclesia unanimi voto ... in #sco|.lastic.|e dignitatis personatum canonice duximus eligendum
                  ((*{sim.}; p. 188,8 "ad prelibate #sco|.lastic.|e dignitatis personatum"))."

             UUU_BEDEUTUNG pro schola solvendus -- für den Unterricht zu zahlen(d):
                 * CHART. Mog. A; II 532 p. 886,25 (a. 1190) "ad peticionem ... Petri #sco|.lastic.|i ius #sco|.lastic.|um sibi successoribusque suis ... plene reintegramus {(sc. archiepiscopus)}."
                 * CONST. imp. I; 366 p. 515,27 "villici episcopi tenentur ipsi magistro Willelmo ... ratione beneficii #sco|.lastic.|i qualibet curti episcopali dimidium diei servicium annuatim persolvere."
                 * EPIST. Hild.; 105 "ut libros taceam {(sc. scholaris quidam)} et iura #sco|.lastic.|a et plurimos doctrine sumptus."
                 {al.} {v. et {=> scholarius/scholasticus_4}}

             UUU_BEDEUTUNG de libro i. q. in schola adhibitus -- im Unterricht verwendet:
                 * CATAL. biblioth. A; III 64 p. 189,35 "libri #sc|.olastic.|i: Bucolica et Georgica Virgilii, Ovidius in Ibin {eqs.}"
                 * CATAL. cod. Conr. Schir.; ((MGScript. XVII; p. 624,25)) "hii {(sc. librorum donatores)} hos libros comparaverunt: ... libros #sc|.olastic.|os, Tullium Officiorum {eqs.}"
                 * CHART. Livon. A; 198 p. 285 p. 258,25 " addimus {(sc. episcopus)} ... libros nostros #sc|.olastic.|os, quod nobiscum tulimus de scolis."; add. 2731 p. 21,21 "#scholastic.|us tenebitur litteras ecclesiasticas scribere, scolares instruere et eis in libris #sco|.lastic.|is providere." 

             UUU_BEDEUTUNG in schola usitatus, disertus -- in der Schule gebräuchlich, rhetorisch gewandt:
                 * VITA Balth. A; 1 (prol.) "etsi imperitia denegat vires delicatae historiae #sco|.lastic.|orum_que verborum ordinem proferendi {eqs.}"
                 * EPIST. Mog.; 21 p. 354,13 "quia epistolaris brevitas me cursim pandere vetat, quod gestio, #sco|.lastic.|is allegationibus maturitatem vestram gravare vereor."
                 * GERHOH. tract.; p. 53,4 "procul hinc sit omnis disputatio #scolastic.|a."
            
         UU_BEDEUTUNG de hominibus:
             UUU_BEDEUTUNG doctus, studiosus -- gelehrt, (lern)eifrig:
                 * CONST. imp. I; 366 p. 515,25 (a. 1195) "dilectus capellanus noster magister Willelmus Mindensis #sco|.lastic.|us
                 ((
                    *{sim.} ACTA civ. Wism. A; 942 "quas {(marcas)} consules dederunt mag<istr>o Godescalco #sco.|lastic.|o"
                 ))."
                 ANHÄNGER superl.:
                     * EPIST. var. II; 20,12 "vir prudens et #sco|.lastic.|issimus {@ scholasticus_2} Fortunatus."
                     * CHRON. Mich.; 33 "Boetius, vir #scholastic.|issimus (#scholat|.issimus {var. l.}) et in definiendis rebus prudentissimus."
                     * EPIST. Teg. II; 178 "sub #sco|.lastic.|issimo magistro H."

             UUU_BEDEUTUNG iuris peritus, iurisprudens -- rechtskundig, -gelehrt; superl.: {=> scholasticus_3}:
                 * CHART. Ital. Ficker; 52 p. 76,14 (a. 1032) "Petrus #sco|.lasti.|cissimus {@ scholasticus_3} quasi advocatus ... quesivit {eqs.}"
                 * CHART. Stir.; III| 154 "coram ... magistro Heinrico #sco|.lastic.|o."; 209 "Ditrico #sco|.lastic.|o ... subnotario."
                   
      U_BEDEUTUNG subst.: 
         UU_BEDEUTUNG masc.:

             UUU_BEDEUTUNG  discipulus -- Schüler:
                 * FORMA mon. Sangall.; 11,6 "mansiunculae #sco|.lastic.|orum hic."
                 * WALAHFR. Gall.; 2,38 "in ... monasterio inter #sco|.lastic.|os tunc temporis erat quidam puerulus."
                 * PASS. Mauric.; 18 "cum nulla mihi sit copia linguę et vix ad primas gradiar #sco|.lastic.|us {@ scholasticus_1} odas."
                 * VITA Meginr.; 4 "quadam die sumpsit secum nonnullos #sco|.lastic.|os, quos nutrierat."
                 * OTLOH. prov.; prol. 14 "proverbiorum ... hic collectorum dictis parvuli quilibet #scholastic.|i ... possunt apte instrui post lectionem psalterii."
                 {al.}

             UUU_BEDEUTUNG cantor chori -- Chorsänger:
                   * PONTIF. Rom.-Germ.; 99,185 "#sco|.lastic.|i e regione crucis lento gradu veniant ante eam
                   (( *{item}; 99,206))."

             UUU_BEDEUTUNG magister scholae -- Schulmeister, Leiter einer (Kloster-, Dom-)Schule:
                 * VITA Balth. B; 1 (prol.) p. 482^b,13 "quem {(sermonem imperitum)} #sco|.lastic.|i cervicosa inspectione considerantes suos pompaticos videntur condere libros."
                 * EPIST. var. II; 22 "quod {(volumen)} sanctus Alcuinus, summus #sco|.lastic.|us ex variis libris sancti Augustini congregavit in unum."
                 * CHRON. Andag.; 8 p. 21,7 "fuerunt ..., quos ibi {(sc. in monasterio Fuldensi)} invenit {Theodericus}, fratres, videlicet Robertus senex, Ermenfridus decanus, ... Stepelinus exterior #sco|.lastic.|us et interior Balduinus."
                 * WIBALD. epist. I; 142 p. 292,1 "scribo #sco|.lastic.|o et philosophię vias ingredienti {(sc. Manegoldo)}." 
                 {persaepe.}
              
                 ANHÄNGER inter testes:
                     * DIPL. Loth. III.; 41 p. 68,23 "huius rei testes fuerunt ... de fratribus ęcclesię Iohannes dekanus ..., Reinerus #sco|.lastic.|us."
                     * CHART. Steinf.; 36 p. 34,8 "testes horum sunt ... Thidericus Sancti Andree prepositus, Rudolfus maior #sco|.lastic.|us."
                     * CHART. Ror.; 14 "Rudpertus summus #sco|.lastic.|us."
                     {persaepe.}

             UUU_BEDEUTUNG vir doctus -- Gelehrter:
                 * EPIST. Hildeg.; 195 l. 42 "ad erubescentiam modernorum #scholastic.|orum."

             UUU_BEDEUTUNG iurisconsultus -- Rechtsgelehrter, -kundiger, -berater:
                 * CHART. Ital. Ficker; 69 p. 96,34 (a. 1061) "ego Ioannes #scolastic.|us huic iudicio scribens interfui."
                 * DIPL. Frid. II.; 439 p. 28,2 (spur.) "quod nos nec aliquis successorum nostrorum vobis {(sc. burgensibus)} scultetum, sacerdotem, #sco|.lastic.|um, sacristam ... vel aliquem officialem instituat."
                 * CHART. Bern.; II 281 p. 298,23 "#sco|.lastic.|um ..., matricularium, ianitores et preconem per se ... eligant {(sc. burgenses)}."
                 {v. et {=> scholarius/scholasticus_5}}

         UU_BEDEUTUNG fem.:
             UUU_BEDEUTUNG magistra scholae -- Schulmeisterin, Leiterin einer (Kloster-)Schule:
                 * CHART. Rhen. inf.; I 408 (a. 1164) "testes ... Elisabet custos, Gerbergis decana, Sophia de camera, Ǒda #scholastic.|a."
                 * CHART. episc. Hild.; I 746 "actum ... sub Gerdrude priorissa, Goda custode, Adelheide celleraria, Iohanna cameraria, Iudita #sco|.lastic.|a."
                 * CHART. Osn.; II 395 p. 310,19 "Cunegunt preposita, Beatrix decana, Alhedis #sco|.lastic.|a."

             UUU_BEDEUTUNG doctrina (rhetorica) -- (rhetorische, Schul-)Bildung:
                 * VITA Balth. A; 1 (prol.) "minus licet periti #sco|.lastic.|a, sed magis studere volumus patere edificationi plurimorum."
                 * IOH. METT. Ioh.; 83,4 "#sco|.lastic.|am mox super his {(sc. rationibus dialecticis)} sibi aperiendis expetens ab ipsis introductionibus Isagogarum laborem arripuit lectionis."
                 ANHÄNGER usu ellipt.:
                 * ALBERT. M. eth. II; 1,3,1 p. 31^a,1 "gubernationis sicut est #sco|.lastic.|a sapientiae dans operam."

SUB_LEMMA scholastice
     GRAMMATIK adv.

BEDEUTUNG docte, more doctorum -- gelehrt, nach Art der Gelehrten, wissenschaftlich:   
     * EKKEH. IV. pict. Mog.; 376 "sol stetit hic primo, sub Esaia vate secundo: quęritur (gloss.: #scholastic|e), his damnis cyclorum quid cadat annis
     ((
         {spectat ad} * $VULG.; Ios. 10,13.; Is. 38,7sq.
      ))."
     * WIBALD. epist. I; 142 p. 305,13 " verum #sco|.lastic.|e: si possunt in nomine proprio ... duę consonantes ante vocalem iungi, ... cur duę vocales loco consonantium positę conglutinari ... non possunt?"

BEDEUTUNG diserte, eloquenter -- rhetorisch gebildet, beredt, (rede)gewandt:
     * ALCUIN. epist.; 172 p. 284,25 "quod ... in litteris vel distinctionibus non tam #sco|.lastic.|e (#sco|.lastic.|ae {var. l.}) currit, quam ordo et regula artis grammaticae postulat, hoc saepius velocitas animi efficere solet."
     * PETR. DAM. epist.; 117 p. 324,29 " #scolatic.|e disputans quasi descripta libri verba percurrit {frater}, vulgariter loquens Romanae urbanitatis regulam non offendit."

BEDEUTUNG iuxta disciplinam -- gemäß der Schulbildung:
     * OTLOH. tempt.; 52 p. 322,5 "sepe expertus sum mentem lascivam cuiuslibet #sco|.lastic.|e instituti in nullo magis posse constringi quam studio dictandi."

BEDEUTUNG iuxta regulas disciplinae (rhetoricae) -- gemäß den Regeln der (rhetorischen) Schulbildung:
     * GERHOH. ad Innoc.; p. 227,12 "vade tu nunc ad illos tuos determinatores non {!aecclesiastice}, sed #sco|.lastic.|e scripturam Dei legentes
     ((; glor.; 17,7 p. 1136^C "alius ... eidem {[cuidam prudenti]} consona loquens magis #scholastic.|e quam ecclesiastice peroravit"))."
BEDEUTUNG iuxta rationem scholasticam q. d. -- nach der 'scholastischen' Methode, Argumentation:
     * ARNO REICHERSB. apol.; p. 31,6 "his ... tropis in naturalibus #sco|.lastic.|e uti possunt, verum in supernaturalibus Domini nostri, id est Christi, non ita."

AUTORIN Weber