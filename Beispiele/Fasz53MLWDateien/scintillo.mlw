LEMMA scintillo 

GRAMMATIK verbum I; -avi, -atum, -are

VEL {semel ({=> scintillo_07})} scintillor

GRAMMATIK verbum I; -ari

SCHREIBWEISE

    script.:

        c-: {=> scintillo_08}
        si-: {=> scintillo_05} {adde} * ALBERT. STAD. Troil.; 4,267 {(var. l.)}

STRUKTUR

    partic. praes. usu adi.: {=> scintillo_12}{=> scintillo_02} {al.}


BEDEUTUNG intrans. i. q. scintillas emittere, micare, flagrare, ardere, coruscare, vibrare, radiare -- Funken (ver)sprühen, funkeln, blitzen, glitzern, flimmern, flackern, strahlen:

    UNTER_BEDEUTUNG proprie:

        UU_BEDEUTUNG  de igne, ferro sim.; in imag. et per compar.: {=> scintillo_02}{=> scintillo_09}{sqq.}:

            * WALTHARIUS; 1373 "cassis fabrefacta  ... excipit assultum ... et #scintill.|at in altum."

            
            * RUP. TUIT. inc.; 9 p. 451,17 "ignis horribilis crepitans et in partes plurimas #scintill.|ans."

            * VITA Annon. II; 2,30 "qui {(faber)} #scintill.|antis {@ scintillo_12} ferri, quod candens feriebat, incendio lumen utrumque perdiderat."

            * CONR. EBERB. exord.; 4,13 l. 24 "ut devotio sancta ... velut ignis #scintill.|ans {@ scintillo_02} et exaestuans in praecordiis eius {(conversi)} ... dilatata ferventior efficeretur." {al.}


        ANHÄNGER in imag. {@ scintillo_09} de daemonibus, verbis:

            * HERBORD. Ott.; 2,37 p. 133,6 "eorum {(sc. exploratorum)} ... verbis paulatim #scintill.|antibus quasi arundinetum tota civitas incanduit."

            * CAES. HEIST. hom. exc.; 57 "sepius per chorum demones vidit {Hermannus} ... discurrentes, sepe in diversis locis contemplatus est eos horribiliter #scintill.|antes."

        UU_BEDEUTUNG  de lumine, sideribus sim.; in imag.: {=> scintillo_03}{=> scintillo_10}:

            * ERMENR. Har.; 126 "magni viri ... veluti ... magna luminaria infulsere, quorum precum iubare #scintill.|ante {@ scintillo_03} languores nostri deleantur."; 205 "contigit me lucernam in oratorio ... accendisse, sed, quia non bene #scintill.|antem reliqui {eqs.}"

            * HILDEG. scivias; 3,5 l. 653 "in beatitudine fidelium hominum ante oculos Dei #scintill.|antium {@ scintillo_10}."

            * ALBERT. M. eth. I; 379 p. 322,14 "Saturnus ... propter nimiam distantiam videtur #scintill.|are ad modum stellarum fixarum habentibus debiles oculos."; sens. 1,9 p. 40,45 "stellae fixae videntur #scintill.|are." {ibid. saepius.}



            

        UU_BEDEUTUNG de rebus lucem reddentibus:

            * MARB. RED. lap.; 185 "auro chrisolithus micat et #scintill.|at ut ignis ((* {inde} PS. MARB. RED. lap.; 44 "auricolor chrysolitus #scintill.|at velut clibanus"))."

            * ALBERT. M. anim.; 2,3,12 p. 117,51 "est #scintill.|ans ... aqua maris fortiter mota, quia scilicet partes salis combustae multae sunt in ea, in quarum superficie est ignis micans." {al.}

        UU_BEDEUTUNG  de oculis i. q. palpebrare, nictare -- blinzeln, zucken:

            * PETR. DAM. epist.; 57 p. 179,7 "qui {(serpens)} nos ... ambulantes aspexit, continuo #si|.ntill.|ans {@ scintillo_05} oculis ... in nos impetum fecit."

            * IOH. IAMAT. chirurg.; 8,27 p. 68,39 "in leonina {(sc. lepra)} occuli apparent rotundi #scintill.|antes."

        UU_BEDEUTUNG  de colore i. q. variegare -- schillern:

            * PS. HIPPOCR. sang.; 16 "nimis #scintill.|atur {@ scintillo_07} {sanguis} in diversis coloribus."

            * TRACT. de aegr. cur.; p. 358,21 "cutis eorum {(elephantiacorum)} crossa, #scintill.|antem habent aspectum ((*{sim.} GLOSS. Roger. I A; 3,23 p. 705,29))."



    UNTER_BEDEUTUNG translate:

        UU_BEDEUTUNG clarescere, col-, elucere -- erstrahlen, hervorstechen, 'aufblitzen':

            * VITA Landel. Ettenh.; 2,2 "se totam in lamenta continuo relaxabat {vidua}, inter quae tamen ex ratione quidam cordis iubilus #scintilla.|bat."

            * ALBERT. AQUENS. hist.; 3,8 "Boemundi fama semper claruit ..., Godefridi ducis nunc primum nomen #scintilla.|bat."

        UU_BEDEUTUNG  nitere -- brillieren, prunkvoll sein:

            * EPIST. Teg. I; 52 p. 61,10 "#scintill.|antis calami ... ulterior modus cernitur esse quam ammirandus."

BEDEUTUNG trans.:

    UNTER_BEDEUTUNG incendere, inflammare -- entzünden, entflammen; in imag.:

        * EPIST. Hann.; 24 p. 59,4 "quibus {(follibus)} ferrum meas {(sc. episcopi)} semper #scintilla.|turum {@ scintillo_01} iniurias incandescat."

        * ARNOLD. LUB. chron.; 5,30 p. 211,3 "ut ... populo infideli pacem Domini nunciaret {canonicus} et ipsum paulatim calore fidei #scintilla.|ret (#c|.intilla.|ret {@ scintillo_08} {var. l.})."

    UNTER_BEDEUTUNG illuminare -- zum Leuchten bringen, leuchten lassen; in imag.:

        * LAUR. LEOD. gest.; epist. p. 489,21 "qui {(Bertharius)} de ... ruinis incensae urbis et ecclesiae omnia ... praedecessorum praesulum saltem vel nuda nomina eruit vel quaedam gestorum nobis #scintilla.|vit (#scintill.|ant {var. l.})."

SUB_LEMMA scintillatus 

GRAMMATIK adi. I-II; -a, um

BEDEUTUNG scintillae {@ scintillo_33} similis -- funkenartig; cf. {=> scintilla/scintilla_33}:

    * CONSTANT. AFRIC. theor.; 7,15 p. 35a^{r} "quarum {(urinarum)} unaqueque aut diversa in colore ... aut in forma velut ... #scintillat.|a aut arenosa aut orobiforma."


AUTORIN Leithe-Jasper