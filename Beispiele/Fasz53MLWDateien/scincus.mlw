LEMMA scincus VEL stincus

GRAMMATIK
    subst. II; -i

VEL {raro ({=> scincus_7a; scincus_7b; scincus_7c})} *stinco

GRAMMATIK
    subst. III; -onis m.

ETYMOLOGIE
    σκίγκος

SCHREIBWEISE
    script.:
        t-: {=> scincus_7c}
        scyn-: {=> scincus_4}
        -nti: {=> scincus_3}
        -chus: {=> scincus_5}
        -ctus: {=> scincus_2}
        
FORM
    acc. sg.:
        extingun: {=> scincus_6}


BEDEUTUNG de animali:

    UNTER_BEDEUTUNG lacerta, saura -- Eidechse, 'Skink' {(fort. de Scinco scinco ['Apothekerskink'])} ((* {cf.} H. Leitner, op. cit. ({=> scilla1/scincus_1}); p. 103sq.)):

        UU_BEDEUTUNG in univ.:

            * CONSTANT. AFRIC. coit.; 14,5sq. "sunt ... potionibus apta #st|.inc.|us (stercus, stercion {var. l.}) et testiculi vulpis, id est satirion, quia accendit libidinem, sicut #st|.inc.|us."; 16,22 "recipe ... umbilici #st|.inc.|i (#stincti, #st|.inc.|onis {@ scincus_7a}, #st|.inc.|orum {sim. var. l.}) ... ℥ III {(cf. comm. ed. p. 203)}."

            * TRACT. de aegr. cur.; p. 326,27 "detur eis {(sc. gonorrhoea patientibus)} ... cauda #st|.inc.|onis {@ scincus_7b}, ut satis coeant."

            * CONR. MUR. physiol.; 1283 "nascitur in Nylo fluvio #scyncus {@ scincus_4} cocodrillo consimilis, corpus tamen est #sc|.inc.|i minus illo ((* {sim.} ALBERT. M. animal.; 24,55 "#sc|.inc.|i animalia sunt aquatica in Nilo natantia cocodrillis similia {eqs.} {(v. notam ed.)}"))."

            {al.}

                ANHÄNGER de genere lacertae Indico:

                    * DYASC.; p. 184^a "#st|.inc.|hus {@ scincus_5} animal est, quod in India invenitur, quadruplex est simile lacerte, sed multo maius et longius."

            
        UU_BEDEUTUNG per errorem "#sc|.inc.|i" pro piscibus habentur:

            * CIRCA INSTANS; p. 37^{b}r "#st|.inc.|i pisces sunt lacertis similes, in Apulia inveniuntur, sed ultramarini efficatiores sunt ((* {sim.} MATTH. PLATEAR. (?) gloss.; p. 375^{H} (add.) "#sc|.inc.|orum recentium: pisciculi sunt ultra mare {eqs.}" * ALPHITA; S 26 "#st|.inc.|us (#sc|.incus, #scinctus {@ scincus_2} {var. l.}) piscis est similis lacerte aquatice {(v. comm. ed. p. 550)}"))."

            * BERNH. PROV. comm.; 7,1 p. 294,32 "#st|.inc.|i, id est pisciculi de saeta."
            
            * ARNOLD. SAXO flor.; 2,7 p. 64,3 "sunt pisces #stinti {@ scincus_3}: quos si comederit homo, aucmentabitur in eo sperma."



    UNTER_BEDEUTUNG Asterias rubeus -- Seestern:

        * ALBERT. M. animal.; 4,4 "decimum ... genus {(sc. marinum)} est spongia marina, quae vivit in aqua; sub ligneo autem continetur animal, quod vocatur #st|.inc.|us."; 4,73 "ad naturam ... lignei videtur affinitatem habere #st|.inc.|us {eqs.}"; 24,55 "#st|.inc.|us est animal marinum medium inter plantam et animal, quod est sicut stella pentagonum rufi corii, et in medio habet rimas, quibus trahit nutrimentum {(v. notam ed.)}."; mot. proc.; 1,1 p. 48,5 "quaedam {(sc. animalia)} ... sunt, quae a locis moventur, sicut #st|.inc.|us et spongia."


BEDEUTUNG de herba i. q. orchis, satyrion -- Orchidee, Knabenkraut ((* {cf.} André, op. cit. [{=> scilla1/scilliticus_7}]; p. 249sq.)):

    * RECEPT. Sangall. I; 55 "ad virilia hominum levanda: testonis de pullo bibat in aqua; ... #t|.inc.|oni {@ scincus_7c} campano ... radices II sicut testos {eqs.}"

    * DYNAMID. Hippocr.; 2,49 "satyrion Romani priapiscum dicunt, ..., quam vulgus #extingun {@ scincus_6} vocat."

AUTORIN Niederer

UNTERDRÜCKE WARNUNG 525
