LEMMA sarcocolla

GRAMMATIK 
    subst. I; -ae f.

ETYMOLOGIE
    σαρκοκόλλα

SCHREIBWEISE  
    script.:
        ca-: * THADD. FLORENT. cons.; 47,76
        sor-: {=> sarcocolla8}
        sacc-: * ANTIDOT. Bamb.; app. p. 36,26
        saro-: * GLOSS. Roger. I B; p. 131,1 ((ed. Nalesso))
        sarto-: {=> sarcocolla2}
        sarcolla:   {=> sarcocolla7} * {adde} GLOSS. Roger. I B; p. 49,8sqq. ((ed. Nalesso)) {ibid. al.}
        -chollam: * GLOSS. Roger. I B; p. 131,1 ((ed. Nalesso))
        -cocla:  {=> sarcocolla3}
        -culla: {=> sarcocolla8; sarcocolla9}
        -olam:  {=> sarcocolla5}

FORM
    form.:
        gen. sg. decl. III. {(pro forma gen. gr.)}: {=> sarcocolla6}
        ?abl. sg. decl. II.: * THADD. FLORENT. cons.; 167,146

METRIK
    metr.:
        -cŏlam: {=> sarcocolla5}

BEDEUTUNG  σαρκοκόλλα, gummi fruticis persici -- Sarkokoll-Harz, persisches Gummi; praecipue Astragalus sarcocolla {Dym.}, sed et al. ((* {de re v.} André{@sarcocolla10}, Plantes.; p. 227)) ((* J. Martin, Die Ulmer Wundarznei. 1991.; p. 168)):

    * RECEPT. Lauresh.; 4,48 "cimolia ~ I, #sarco.|cla {@sarcocolla3} ~ I."
 
    * PAUL. AEGIN. cur.; 217 p. 158,23  "crisocollis, taurocollis, #sarcocoll.|is {@sarcocolla6} (( * $PAUL. AEG.; 3,53,2 "σαρκοκόλλης")) ... ana ∻ I et media."

    * CIRCA INSTANS; p. 36b^v "gummi est cuiusdam arboris #sarcocoll.|a et in transmarinis partibus reperitur {eqs.}"

    * GLOSS. Salern.; p. 9^b,38  "#sarcocoll.|a: azarud ((* {sim.} BENVEN. GRAFF. ocul. B; 5 p. 22,24 "r<ecipe> azarum album, id est #sarcocoll.|am"))."

    * NOTULAE Wilh. Cong.; 592sqq. "dicta quasi sarcina in collo, quia numquam sine ea debet esse medicus; semper eam in sacculo suo secum deferre debet, vel ... #sarcocoll.|a interpretatur glutinum carnis, quia, dum sanguinem coagulat, carnem generat."
    
    {saepius.}


BEDEUTUNG eupatoria -- Odermennig; Agrimonia eupatoria L. ((* {de re v.} Marzell, Wb. dt. Pflanzennam. I.; p. 139sqq.)) ((* {v. et} André, op. cit. ({=> sarcocolla10}))):

    * GLOSS. med.; p. 73,8 "#sart|.ocoll.|a {@sarcocolla2}: argemonia."

    * ANON. herm.; p. 97,19 "agremunia, id est #sarcoc.|ulla {@ sarcocolla9}."; p. 97,27 "agrimonia, id est #sorcoculla {@ sarcocolla8} sive eupaturium."

    * WALAHFR. hort.; 359 "hic ... #sarcoc.|olam {@sarcocolla5} (gloss. $L: agrimoniam, quia sarculo colitur) ... ordinibus facile est discernere pulchris."


BEDEUTUNG ?sucus caprifolii -- ?Saft eines Geißblatts; fort. Lonicera periclymenum {L.} ((* {de re v.} Daems, Nomina simplicium medicinarum. 1993.; nr. 551)) ((* {v. et} André, op. cit. ({=> sarcocolla10}); p. 240)):

    * GLOSS. Roger. I B; 1,6 p. 667,40 "<<#sarcocoll.|a est sucus matrisilve>> (#sarcolla {@sarcocolla7} vel succus est herbe cuiusdam transmarine, secundum alios matris silve {ed. Nalesso p. 48,24})."
 
AUTORIN Mandrin/Niederer
