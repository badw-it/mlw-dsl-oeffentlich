LEMMA  scabiosus

GRAMMATIK  adiectivum I; -a, -um

SCHREIBWEISE

    script.:

        scob-: {=> scabiosus001}

        -blo-: {=> scabiosus002}

BEDEUTUNG  inaequalis, asper -- uneben, rau:

    * HUCBALD. Leb.; 9 p. 886^{B} "eos {(lapides)}, qui #scabios.|i et sordidi erant, aqua munda purificare studuit {episcopus}."

    * ALBERT. M. veget.; 6,205 "exterius est {(sc. scyphus glandis)} #scabios.|us habens superficiem asperam propter terrestreitatem ipsius."; animal.; 22,50 "pellem habet {elephas} ... quasi pumiceam et #scabios.|am."

        ANHÄNGER  in nomine loci:

                * WILH. APUL. gest.; 3,523 "doctior ... aderat Robertus de #S|.cabios.|o Monte comes dictus."

ABSATZ

BEDEUTUNG  impurus -- unsauber, schäbig:

    * YSENGRIMUS; 3,251 "hinc fuge, sum {(sc. laniger)} potior regi, #scabios.|us es, hirce."

    * CONR. MUR. summ.; p. 160,29 "mulier deridenda ... poterit nominari: ... scenica, scabra{@ scabiosus004}, #scabios.|a."

ABSATZ

BEDEUTUNG  scabie laborans -- krätzig, räudig, aussätzig:

    U_BEDEUTUNG  adi.:

        UU_BEDEUTUNG  spectat ad homines:

            UUU_BEDEUTUNG  in univ.:

                * ANTIDOT. Sangall.; p. 88,15 "facit {emplastrum} ... ad tibias #scabios.|as."; p. 97,13 "oleo milinum ... facit ... ad #scabios.|a ulcera."

                * PAUL. AEGIN. cur.; 92 "hoc {(medicamentum)} et #scabios.|os ((* PAUL. AEG.; {3,22,16} ψωρώδεις)) oculos sanat."

                * BERTH. chron. B; a. 1077 p. 306,1 "pauperibus sordidulis, sed pre ceteris #scabios.|is, leprosis, ulcerosis ... balneandis ... visitandisque operosa Christo ministraverit {Agnes}."

                * PAUL. BERNR. Greg.; 117 "illico #scabios.|us effectus vix per dignos poenitentiae fructus evasit {(sc. presbyter quidam, qui Gregorii casulam usurpavit)}."

                * HILDEG. phys.; 1,120 "ungues de mala ... ariditate humorum #scabios.|i fiunt (({sim.} 1,173 p. 571,32 "qui #scabios.|os ungues habet"))."

                * TRACT. de chirurg.; 581 "<s>i quis #scabios.|us sit vel scabie consumitur, hac medicina in balneo utatur {eqs.}"

                * CAES. HEIST. Engelb.; 3,48 "Anselmus plebanus ... manus habebat ita #scabios.|as (sco- {@ scabiosus001} {var. l.}) ulceribusque plenas, ut {eqs.}"

            UUU_BEDEUTUNG  alopecia affectus -- vom Fuchsgrind befallen:

                * BERNH. PROV. comm.; 8,3 p. 306,43 "si grabiosus, id est #scabios.|us vel allipiciosus, ungatur pice liquida."

        UU_BEDEUTUNG  spectat ad animalia:

            * COSMAS chron.; 1,10 p. 25,3 "potens et inpotens perstrepunt: 'arma, arma'; saltat #scabios.|a (scabio {var. l.}) equa ut acer equus in pugna."

            * ALBERT. M. animal.; 23,97 "si ... #scabios.|us sit accipiter, accipe anxugiam veterem ..., tere simul cum aliquot gariofilis ... et ex eo unge scabiem."

    U_BEDEUTUNG  subst. masc.:

        * CONSTANT. AFRIC. theor.; 5,13 p. 20b^{v} "balneari ... prodest #scabios.|is, ut humores dissolvantur intercutanei."

        * VITA Landel. Ettenh.; 2,1 "#scabios.|i in integram cutem restituti sunt."

        * IOH. IAMAT. chirurg.; 8,3 p. 55,24 "super cavillam facta in tybia loco scarificationis a cavilla unius palmi distante spatio, a genibus vero IIII digitorum conferunt laborantibus de melanconia a salso flegmate, conferunt cogitationi et mentis corruptioni, impetiginosis, epilenticis, pruriginosis, #scabios.|is, visum clarificat." {ibid. al.}

    U_BEDEUTUNG  subst. fem. spectat ad genus plantarum (('Schorfwurz', 'Acker-Witwenblume', Knautia arvensis {L.} Coult.))(({v.} Hegi, Flora VI/1. p. 292sqq.))((J. Kunze, Schorfheide und verwandte Namen. 2007.; p. 53)):
    
        UU_BEDEUTUNG  ad scabiem sanandam:

            * CIRCA INSTANS; p. 38a^{r} "#scabios.|a: ... dicitur #scabios.|a, quia contra scabiem valet." {ibid. al.}

        UU_BEDEUTUNG  ad ulcus sanandum:

            * IOH. IAMAT. chirurg.; 5,17 "#scabios.|a sola cum succo rostri ancipitis contrita superponatur."; 9,31 "affodilli, hermodactyli radicum celidonie, #scabios.|e, levistici agrestis, sumitatum olivarum et mirte buglosse ana unc. I.; herbe et radices contemperentur in aceto per III dies, coquantur {eqs.} (({sim.} * ROGER. SALERN. chirurg.; 1,688 "accipe celidonie, ... levistici agrestis ana manipulum I et #scabios.|e similiter"))."

        UU_BEDEUTUNG  ad oculos sanandos:

            * IOH. IAMAT. chirurg.; 5,18 "quodsi infra tertium diem non profecerit, #scabios.|am predicto modo superpone {(sc. oculo)}."

        UU_BEDEUTUNG  usu communi:

            * TRACT. de aegr. cur.; p. 129,19 "recipe lapati acuti et rafium tere, #scabios.|e, emile {eqs.} (({sim.} * GLOSS. Roger. I A; 2,10 p. 613,11))."

            * ALPHITA; S 35 "#scabios.|a (("#scab.|losa" {@ scabiosus002} {var. l.})) herba est."

            * GLOSS. Roger. I A; 2,2 p. 586,18 "#scabios.|a contrita cum auxungia porci et bene cum vitello ovi et parum masticis incorporata ipsum {(carbunculum)} curat (({sim.} * ROGER. SALERN. chirurg.; 2,143 "fit ... carbunculus de sanguine ferventissimo; supra ... locum ponatur #scabios.|a trita"))."; II 141 "unguentum viride cyrurgicum sic fit: allia, celidonie, #scabios.|e, gallitrici, levistici ... bene tere {eqs.}"; III 207 "radix #scabios.|e decoquatur in oleo communi {eqs.}"

AUTOR  Fiedler