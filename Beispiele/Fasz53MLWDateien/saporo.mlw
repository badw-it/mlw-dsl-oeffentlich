LEMMA saporo

GRAMMATIK
       verbum I; -atum, -are

FORM
     partic. perf.:
             -rit(us): {=> saporo_1}

GEBRAUCH
     usu absol.:
          {=> saporo_2; saporo_4}    

BEDEUTUNG trans.:  
     U_BEDEUTUNG condire, sapidum reddere -- würzen, schmackhaft machen {(alleg.: {=> saporo_3})}:
         * LIBRI Karol.; 4,20 p. 537,18 "illos esse 'sal terrae', condimentum ... populi, quo insulsa quęque condiuntur et insipida #sapor.|antur{@ saporo_3}, qui {eqs.}"
         * REGIMEN san. Salern.; 155 "sal virus refugat et non sapidumque #sapor.|at, nam sapit esca male, quae datur absque sale."
         ANHÄNGER absol.: 
             * RECEPT. Lauresh.; 1,45 "haec omnia simul coquantur propter #sapor.|andum{@ saporo_4}."; 4,88 "ad #sapor.|andum cinamo din<a>r<ium> I {(sc. addes)}"

     U_BEDEUTUNG  palato percipere, gustare -- als Geschmack wahrnehmen, schmecken:
         UU_BEDEUTUNG proprie:
             * CONSTANT. AFRIC. theor.; 9,18 p. 44a^v "quidam {(infirmi)} salsa #sapor.|ant {(sc. in ore)}, quod fit de phlegmate salso."
             * FRID. II. IMP. art. ven.; 2| p. 170,12 "ut ... mordeat {falco} carnes et mordendo #sapor.|et; #sapor.|ando {@ saporo_2} ... allicietur ad comedendum."; p. 192,32 "si erit {tiratorium} de bonis carnibus, falco ... #sapor.|ando dulcedinem et bonitatem carnium affectabit et assuescet comedere."

         UU_BEDEUTUNG in imag.:
             * EPIST. divort. Loth.; 17 p. 237,6 "quicquid nobis {(sc. regi)} a vestra {(sc. papae)} pia paternitate dirigitur, ... omni melle dulcius, omni thesauro carius in palato nostrae mentis dulciter #sapor.|atur."
     
     U_BEDEUTUNG sapere -- schmecken (nach):
         * EKKEH. IV. bened. I; 8,10 "optima bis ternis aqua <<vina #sapor.|at>> (gloss.: #sapor.|atur vina)  in urnis."

BEDEUTUNG intrans. i. q. saporem habere -- Geschmack haben, schmecken:
             * EKKEH. IV. bened. II; 144 "hoc mel dulcoret Deus, ut sine peste #sapor.|et."
             * HILDEG. caus.; 1 p. 53,8 "ibi {(sc. in fluminum aquis)} per coctionem purgantur {escae} et tunc inde suaviores fiunt et melius #sapor.|ant."
             
SUB_LEMMA saporatus
GRAMMATIK
       adi. I-II; -a, -um

BEDEUTUNG conditus, sapidus -- gewürzt, schmackhaft, würzig:
     * EKKEH. IV. cas.; 16 p. 44,19 "nunquam ea domo #saporat.|um monachum {(leg.} monachi; {v. notam ed.)} odorem ferinę hauriunt et carnium."
     * URSO element.; 4 p. 114,26 "res #sapor.|ita {@ saporo_1} (#saporat.|a {var. l.}) immediate lingue approximans ... tam diu ... saporem occultat, donec commasticatione rarefacta ... gustu percipiatur."
     ANHÄNGER ellipt.:
         * ANTIDOT. Augiens.; p. 50,34 "hec omnia pulvera facta distempera cum #saporat.|a {(ci. Thomas, ALMA 5,154, "#saponata" ed.; sc. aqua)} de muscatu."          

SUB_LEMMA saporatum
GRAMMATIK
       subst. II; -i n.
             
 BEDEUTUNG  cibus (bene) conditus  -- (gut) gewürzte, schmackhafte Speise:
       * ECBASIS capt.; 548 " dulce #saporat.|is oneretur discus herilis."

 BEDEUTUNG condimentum -- Würze:
       *ALBERT. M. metaph.; 6,2,3 p. 309,21 "coquus, cuius est 'saporatum ((*ARIST.; p. 1027^a,3 "ἡδονῆς")) quid' in pulmentis 'coniectare', aliud 'facit aliquid' in cibis 'salubre'."

AUTORIN Weber