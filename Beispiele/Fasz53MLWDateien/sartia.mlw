LEMMA *sartia (sarcia)

GRAMMATIK
         subst. I; -ae f.

ETYMOLOGIE ἐξάρτιον 

FORM
    acc. pl. decl. II.:
        -tos: {=> sartia_1}

BEDEUTUNG funus, rudens -- Seil, Tau ((* {de re v.} Reallexikon d. Byzantinistik. A I.; p. 420)):

    UNTER_BEDEUTUNG strictius:

        * ANNAL. Ianuens.; III p. 138,21 "quum ancore vel #sarti.|a eas {(galeas)} tenere non possent, iverunt {velificantes} ad litus Arenzani."

        * CHRON. Ven. Alt.; p. 96,4 "buticelas et butes vasculosque subtus ligna per longitudo extense #sar.|cia navigium erant et per latitudo canalium."

        * ALBERT. MIL. temp.; 220 p. 496,15 "qui {(traditor Ianuensis captus)} debebat incidere catenas et #sart.|os {@ sartia_1} et alia ingenia, quibus pons tenebatur {(v. notam ed.)}."

    UNTER_BEDEUTUNG latius i. q. armamentum (navis) -- (Schiffs-)Ausrüstung(sstück), Takelage:

        * ANNAL. Ianuens.; II| p. 60,24 "qui {(rectores)} ... de galeis ... et #sar.|ciis et custodiae castrorum curam et sollicitudinem habere debebant."; p. 107,13 "consules cum galeis, barcis et #sarti.|a multa ad ipsas {(reliquias beati Iohannis)} iverunt succurendas."; p. 114,22 "consules ... dederunt ei {(comiti)} galeas octo et taridam unam cum universa #sarti.|a."


AUTORIN Weber