LEMMA saxifraga VEL saxifrica

GRAMMATIK 
    subst. I; -ae f.

SCHREIBWEISE 
    script.: 
        xa-: * ANTIDOT. Glasg.; p. 119,6
        sisi-:  {=> saxifraga_2}
        sarxifaga ((* {cf.} R. Strömberg, Griechische Pflanzennamen. 1949.; p. 157. 161)): {=> saxifraga_5}
        sassi-:  {=> saxifraga_3a; saxifraga_3b}
        -reg(i)a:  {=>saxifraga_7} * {adde} RECEPT. Sangall. I; praef. p. 10         
        -iga:   * ANTIDOT. Sangall.; p. 99,17
    separatim scribitur: {=> saxifraga_2}
                

FORM
    form.:
        -gia:   {=> saxifraga_6a}
                {=> saxifraga_6b}
                {=> saxifraga_6f} 
                {=> saxifraga_7}




BEDEUTUNG de diversis herbis medicinalibus i. q. herba ad lapillum frangendum, curandum adhibita -- zum Brechen, Behandeln eines Harnsteins verwendete Pflanze ((* {de re v.} André, Plantes.; p. 228 * {et} I. Stirling, Lexicon nominum herbarum. IV. 1998.; p. 71sq.)):


    UNTER_BEDEUTUNG de {@ saxifraga_8} herbis in schedulis nostris pimpinella vocatis i. q. Pimpinella saxifraga {L.}, Pimpinella maior {(L.) Huds.}, Sanguisorba minor {Scop.} -- Kleine Bibernelle, Große Bibernelle, Kleiner Wiesenknopf ((* {cf.} M. Pradel-Baquerre, Ps.-Apulée, Herbier, introduction, traduction et commentaire. Diss. Montpellier. 2013.; p. 410sq. 545)):  
    

        UU_BEDEUTUNG de synonymis, viribus, descriptione herbae:

            * BOTAN. Sangall.; 51^{praef.} "nomen herba pebinella, quia Greci #saxifri.|ca vocant {(cf. comm. ed. p. 416)}."

            * DYASC.; p. 183^a "#saxifra.|ga nascitur locis montuosis et saxosis {eqs.} ((* {cf.} $DIOSC. gr. Vind.; 4,16))."

            * CIRCA INSTANS; p. 37b^{v} "#saxifrag.|ia {@saxifraga_6a} ... virtutem habet diureticam confringendi lapidem, unde #saxifrag.|ia {@saxifraga_6b} dicitur, id est frangens saxum {@saxum_4} vel lapidem ((* {sim.} MATTH. PLATEAR. (?) gloss.; p. 368^E))."

            * ALBERT. M. veget.; 6,452 "#saxifra.|ga est herba parvula in arenosis locis nascens, calida et sicca {eqs.} (({sim.} 4,64))." {ibid. al.}

                    
        UU_BEDEUTUNG usu vario:

             * PULVIS contra febr.; p. 23,19 "recipit haec: ... quinquefolia m<anipulus> I, #saxifra.|ge (gloss.: steinprehha) sem<en> ∻ I {(cf. comm. ed. p. 28)}."

             * PS. HIPPOCR. phleb.; 140 "capita de alio VII ... et #{sisi frica} {@saxifraga_2} pensante denarios XXI coquatur in unum cum vino vetere."

             * PAUL. AEGIN. cur.; 201 "in renibus ... his, que perforant lapides et incidunt, uti farmachis sine magna caliditate; tales ... sunt basilicon ... et carsia et #sarxifaga {@saxifraga_5} ((*$PAUL. AEG.; 3,45,2 p. 242,3 "σαρξιφαγές"))."

             * THADD. FLORENT. cons.; 1,72 "recipe ... ysopi, eufragie, #sass|.ifra.|ge {@saxifraga_3a} ana drachme dimidium ((sim. 181,10 "recipe ... radicum #sass|.ifra.|ge {@saxifraga_3b}"))." 

             {saepe.} 

                ANHÄNGER fort. add.:

                    * AMARC. serm.; 2,528 "seu fullo est {(sc. homo advena)} aut phyltra parat seu #saxifri.|ce vi elicit urinam mordentem membra pudenda et Marrubio tussim scabiemque celidonia aufert."

                    * URSO element.; 6 p. 197,5 "si ... secundum media sui {(sc. excedat ignis)}, subtilius subiectum redditur ut #saxifra.|ga (#saxifr.|egia {@saxifraga_7}, #saxifrag.|ia {@saxifraga_6f} {var. l.})."

                    * ALBERT. M. animal.; 23,88 "ad pedes ... falconis sanandos millefolii, #saxifra.|gae, verbenae et plantaginis aequaliter accipe et pulverem tritum cum calida sibi da carne." {ibid. al.}
      
        UNTER_BEDEUTUNG de herba pro usu seminum adhibita i. q. lithospermon -- Steinsame (Lithospermum officinale {L.}):

            * RECEPT. Lauresh.; 4,70 "#saxifri.|ca sem<inis> Z."; 4,94 "#saxifri.|ca grana ~ I." 

            * DYNAMID. Hippocr.; 2,28 "#saxifri.|ca, hoc est tiraria, tunsa cum pipere et vino calculos frangit."

            * ANTIDOT. Berolin.; 4 "antidotum ad cauculosos, colicos, nefreneticos: ... #saxifri.|ce semen — I S ((* {sim.} ANTIDOT. Glasg.; p. 109,40 "antidotum  nefreneticum: ... #saxifri.|ca — I S"))."

            * ANTIDOT. Cantabr.; p. 165,49 "ad petras fragendas, que in vesica sunt: #saxifri.|cae semen, hoc est milio salvatico, den. IIII." 

            * ALPHITA; L 2 "litospermatis, semen #saxifra.|ge, quasi sperma, id est semen illius herbe, que frangit lapidem {(v. ind. p. 469)}."


        UNTER_BEDEUTUNG de herba generis Saxifragae {L.} fort. i. q. Saxifraga granulata {L.} -- viell.: Knöllchen-, Körnchen-Steinbrech: 

        * HILDEG. (?) caus.; 429 "si cephaniam non habuerit, #saxifri.|cam eodem pondere, ut verbene est, accipiat {(sc. qui gelewesuth habet)}; ... calor ... verbene et .... frigiditas #saxifri.|ce aliquantum acutos succos habent." * phys.; 1,13 l.19 "qui a paralisi fatigatur, ... his {(sc. herbis)} #saxifra.|gam (#saxifri.|cam {var. l.}) et polipodium equalis ponderis addat."; 1,137 "steinbreche (id est #saxifra.|ga {add. F}) frigida est {eqs.} {(cf. comm. ed. vol. $III. p. 335sq.)}." {ibid. al.} 

        UNTER_BEDEUTUNG ?de generibus filicis fere i. q. calcifraga, asplenos, adiantum -- etwa: Hirschzungenfarn (Asplenium scolopendrium {L.}), Milz-, Schriftfarn (Asplenium ceterach {L.}), Frauenhaarfarn (Adiantum Capillus-Veneris {L.}), Schwarzstieliger Streifenfarn (Asplenium adiantum nigrum {L.}); ni spectat ad {=> saxifraga_8{sqq.}}: 

        * DYNAMID. Hippocr.; 2,65 "#saxifri.|ca, quam Graeci adiantum vocant, alii scolopendriam, trita in vino aut, si febricitet, in aqua calida calculos frangit."

        * ALPHITA; C 221 "colopendria, <<#saxifra.|ga idem>> (#saxifra.|ga, lingua cervina {A}) {(cf. comm. ed. p. 399. 534sq.)}."



AUTORIN Strika