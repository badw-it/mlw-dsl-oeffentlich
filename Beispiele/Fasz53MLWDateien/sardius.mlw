LEMMA sardius (sardus)

GRAMMATIK  
    subst. II; -i m.

SCHREIBWEISE 
    script. et form.:
        abl. pl.:
            -duis: {=> sardius_2}
        nom. sg. decl. III.:
            -o: {=> sardius_4}


BEDEUTUNG de gemma carnei coloris i. q. sarda -- Karneol, 'Sarder'; usu attrib.: {=> sardius_1} ((* {de re v.} H. Lüschen {@ sardonyx_6}, Die Namen der Steine. 1968.; p. 312)):

    UNTER_BEDEUTUNG proprie:

        * AGNELL. lib. pont.; 171 p. 351 l. 19 "facies {(sc. Ermengardae augustae)} stillatas #sard.|uis {@ sardius_2} ..., smaragdis, auro."

        * GODESC. SAXO gramm.; 1 p. 376,9 "saffirus, #sardi.|us ... naturaliter habent paenultimam longam, sed antepaenultima habet accentum."

        * ARNOLD. SAXO flor.; 3 p. 74,46 "#sardi.|us gemma est rubei coloris et clari {eqs.}" 

        * ALBERT. M. miner.; 2,2,17| p. 45^a,6 "sarda {@ sarda_1}, quod alii dicunt #sard.|o {@ sardius_4}, lapis est, qui {eqs.}"; p. 45^a,43 "#sar.|dum sibi in substantia habens {(sc. sardonyx)} admixtum."
    
        {al. v. et {=> sardonyx/sardius_3}}


    UNTER_BEDEUTUNG alleg. ((* {loci spectant ad} $VULG. exod.; 28,17sqq.; apoc.; 4,3.; 21,20)):

        * HRABAN. univ.; 17,7| p. 467^D "#sardi.|us ... martyrum gloria significat ((* {sim.} p. 470^C "in #sard.|is reverendus martyrum cruor exprimitur." {al.}))."

        * RUP. TUIT. vict.; 1,27 p. 40,27 "novem ordines angelorum lapides illi sunt pretiosi: #sardi.|us, topazius {eqs.}"

        * ALEX. MIN. apoc.; 4 p. 51,23 "per ruborem lapidis #sardi.|i {@ sardius_1} designatur hoc nomen Iesus, quia {eqs.}"

        {al.}
        
            ANHÄNGER usu liturg.:

                * HONOR. AUGUST. sacram.; 12 p. 751^B "color iaspidis {(sc. significat)} aquae iudicium, color #sardi.|i iudicium ignis."

AUTORIN Niederer