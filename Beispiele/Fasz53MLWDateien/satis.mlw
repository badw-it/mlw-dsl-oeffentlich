LEMMA satis VEL sat

GRAMMATIK
     adi. unbestimmt; indecl. vel adv.

SCHREIBWEISE
     script.:
         compar.:
             sacius: {=> satis_7; satis_8; satis_9} {al.}

STRUKTUR
     struct.:
         c. gen.: {=> satis_16{sqq.}}
         c. praep.:
             ad: {=> satis_1}
             de: {=> satis_3; satis_4} {al.}

BEDEUTUNG positive:
     U_BEDEUTUNG usu adi. i. q. sufficiens, idoneus, ἱκανός -- genügend, ausreichend (viel), passend; interdum subst. neutr., e. g. {=> satis_15; satis_11} {al.}:
             UU_BEDEUTUNG in univ.:
                 UUU_BEDEUTUNG spectat ad quantitatem; in struct. contam.: {=> satis_13}:
                     * AMALAR. bapt.; 9 "de qua instructione, si quis vult, #sati.|s repperire potest in Agustino de catecizandis rudibus."
                     * DIPL. Otton. I.; 419^b p. 573,12 "obeunte ... episcopo Hildibaldus venerabilis episcopus eius loco #sati.|s pro meritis subrogatus est."
                     * OTLOH. Magn.; 4,25 "postquam ... aves #sati.|s capte sunt."
                     * DIPL. Heinr. IV.; 341 p. 451,2 "presencia domni Heinrici quarti regis ... et reliqui plures cives Parmenses et ceteri homines #sati.|s {@ satis_13} ... astantibus ... in eorum presencia donnus Albertus marchio ... refutavit ... cortem unam."
                     {al.}
                 UUU_BEDEUTUNG spectat ad qualitatem:
                     * VITA Liutb.; 15 "ibi {(sc. loco congruo)} si pietas vestra {(sc. comitis)} cellulam quam parvam fieri precepit, #sati.|s {(om. Br1)} michi habitaculum sufficiet."
                     * ANSELM. GEMBL. chron.; a. 1135 "Hyspanum quendam Burdinum nomine, #sati.|s clericum ei {(sc. Iohanni papae electo)} superordinari fecit {imperator}."
                     * LAMB. LEOD. Matth. I; 376 "vidit {(sc. monachus in visione)} ... rutilare ... stellam..., que simul huic urbi totique #sati.|s fuit orbi."

             UU_BEDEUTUNG pendet {@ satis_16} gen. partitivus:
                 * RUOTG. Brun.; 20 p. 20,29 "presidii #sati.|s {@ satis_11} habundeque nobis {(sc. regi)} est."
                 * WIDUK. gest.; 3,54 "erant ... in Gerone ... in rebus civilibus bona consilia, #sati.|s eloquentiae, multum scientiae."
                 * BRUNO QUERF. fratr.; 13 p. 66,21 "clericorum et sanctimonialium ... ad sanctorum ... exequias #sati.|s convenere."
                 * CONR. MUR. nov. grec.; 2,160 "vivo #sati.|s, mensuro #sati.|s, habeo #sati.|s {@ satis_12} auri."
                 {cf. et {=> satis_14}}

             UU_BEDEUTUNG locut. "#sati.|s habere, dare":
                 * THIETM. chron.; 8,13 p. 508,38 "habes {(sc. successor)} #sati.|s {@ satis_4} de libris, quos ... ab antecessoribus nostris collectos inveni."
                 * CHRON. Thietm.; 4,33 "Dei ... gracia #sati.|s nobis poterit dare."
                 * ALFAN. premn. phys.; 1,41 p. 12 "a solis terrae fructibus eum {(hominem)} <<#sati.|s habere>> ((* NEMES.; ((PG 40,;516^B)) "ἀρκεῖσθαι")) iussit {Deus}."
                 * OTLOH. prov.; E 25 "egens aeque est is, qui non #sati.|s {@ satis_15} habet et cui copia nulla sufficit."
                 * DIPL. Conr. II.; 287 p. 404,9 (spur.) "quatenus ... monachi ... ad sufficientiam victus et vestitus #sat {@ satis_1} habere valeant."
                 {v. et {=> satis_12}}

             UU_BEDEUTUNG locut. "#sati.|s est" sim. usu impers.:
                 * FORM. Senon. II; add. 3,22 "#sati.|s est vel non est?"
                 * HRABAN. epist.; 8 p. 394,3 "singula secundum oportunitatem loci, prout mihi #sati.|s esse videbatur, inserui."
                 * WALAHFR. carm.; 32,7 "#sat erit, si videro gratum."
                 * AGIUS epic. Hath.; 60 "de lacrimis, de planctibus atque dolore non umquam nobis credimus esse #sati.|s{@ satis_3}."
                 * WIDUK. gest.; 1,26 "#sati.|s ... michi est, ut pre maioribus meis rex dicar {(sc. Heinricus)} et designer."
                 * VITA Heinr. IV.; 13 p. 40,31 "pauca dixisse #sat est."
                 {saepius.}
                 ANHÄNGER in fine chartae:
                     * CHART. Sangall. B; 257 (a. 1211) "facta ... sunt hec anno ab incarnatione Domini millesimo CC^{o}XI, regnante imperatore Oͮttone; #sat est."

     U_BEDEUTUNG usu adv. i. q. sufficienter, ample, ἱκανῶς -- zur Genüge, hinlänglich: 
         UU_BEDEUTUNG in univ.:
             UUU_BEDEUTUNG usu communi:
                * WALAHFR. carm.; 45,10 "quantum delectet tua {(sc. Probi presbyteri)} me dilectio ..., vix scio et ipse #sati.|s."; Wett.; 509 "iam #sati.|s est {!dictum} 
                 ((
                 * HRABAN. epist.; 41 p. 480,37 "superius de homicidio #sati.|s d. est." *{sim.} PS. BOETH. geom.; 446 "#sat dictum est"
                 ))."
                 * RIMB. Anscar.; 9 p. 30,26 "regis ... animum ad hoc #sati.|s benivolum, ut ... sacerdotes Dei esse permitteret."           
                 * WALTHARIUS; 1128 "insidiis ... locum circumspexere #sat aptum."
                 * VITA Heinr. IV.; 4 p. 18,3 "qui tacet, #sati.|s laudat."       
                 * THANGM. Bernw.; 51 p. 779,41 (chart.) "#sati.|s indicio est, quantus assurexerit in praeliis David manu fortis."             
                 * LAMB. LEOD. Matth. I; 2529 (add. M) "octo sunt ... genera motus, de quibus in Boetio #sati.|s agitur, ubi dicit: {eqs.}"
                 {persaepe.}
             UUU_BEDEUTUNG  c. negat.:
                 * RIMB. Anscar.; 38 p. 73,7 "qualiter id {(nefas)} emendari potuisset, non #sati.|s ratione colligebat {episcopus}."
                 * RUOTG. Brun.; 2 p. 3,18 "cuius plerique dicta et opera, qui familiarius eum noverant, #sati.|s admirari nequibant."
                 * OTTO FRISING. gest.; 2,30 p. 136,31 "#sati.|s mirari non possumus {(sc. rex)}, quod {eqs.}"
                 {al.}
             UUU_BEDEUTUNG locut. "#sati.|s superque, #sati.|s et ultra" sim.:
                 * FORM. Senon. I; 30 "#sati.|s et super omnia #sati.|s."
                 * RIMB. Anscar.; 40 p. 75,7 "quod ... infirmitas novissima gravis nimium et diutina #sati.|s superque ipsi pro martyrio ... reputari potuisset."
                 * BERTH. chron. B; a. 1077 p. 281,9 "populis ... signorum ostensionibus #sati.|s superque commonitis." {al.}
                 * VITA Leon. Nobil.; 12  ((ASBoll. Nov. III; p. 154^B)) "quos intuens sanctus Leonardus #sati.|s et ultra (quam credi potest {add. 12,15}) super his admiratus est."
                 {cf. et {=> satis_11}}
         UU_BEDEUTUNG ad libitum -- nach Belieben:
             * YSENGRIMUS; 1,790 "ad tua sedisti {(sc. senex)} lucra, morare #sati.|s."; 4,253 "imperet ipsa {(sc. domina)} #sati.|s, nos montem insidimus altum."

         UU_BEDEUTUNG bene -- gut:            
             * HRABAN. epist.; 17^b "horum {(sc. quae exponere praetermisimus)} sensum studiosus lector, cum anteriora bene rimaverit, #sati.|s agnoscere poterit."
             * RIMB. Anscar.; 1 p. 19,5 "quid pro nobis dolere debeamus, #sati.|s intellegimus."; 27 p. 57,37 "quam mentis eius illustrationem frater ille #sati.|s recognovit, quia {eqs.}"
             * RUOTG. Brun.; 2 p. 3,17 "compertum est nuper a plurimis in ... archiepiscopo Brunone ... et #sati.|s a prudentioribus intellectum, homo homini quid prestet."
             * GERH. AUGUST. Udalr.; 1,1 l. 146 "cum ... opus coeptum #sati.|s crescere videretur {eqs.}"

         UU_BEDEUTUNG vi corroborante i. q. aliquanto, valde, vehementer -- ziemlich, sehr, heftig; {(cf. Stotz, Handb. 4,$IX § 46.1)}:
             UUU_BEDEUTUNG c. adi., adv.:
                 UUUU_BEDEUTUNG in univ.:
                     * FORM. Senon. II; add. 3,4 "contra adversarium consilio #sati.|s te putas sapiente, sed {eqs.}"
                     * WIDUK. gest.; 2,4 p. 70,11 "elegit {rex} ... ad hoc officium {(sc. militiae principis)} virum nobilem et industrium #sati.|s_que prudentem nomine Herimannum
                     ((
                     *{sim.} THIETM. chron.; 4,24 "quia #sati.|s prudens vir fuit {Sigifridus}"
                     ))."
                     * GERH. AUGUST. Udalr.; 1,1 l. 135 "coepit sagaciter diruta restaurare #sati.|s_que sensibiliter ordinare."
                     * BERTH. chron. A; a. 1060 p. 189,2 "hyems #sati.|s dura et nivosa."
                     * OTTO FRISING. gest.; 2,18 p. 120,7 "erat in vicino oppidum quoddam #sati.|s populosum."
                     {persaepe.}
                     ANHÄNGER c. negat.:
                         * PAUL. DIAC. Lang.; 1,5 "aput hos {(Scritobinos)} est animal cervo non #sati.|s absimile {(v. notam ed.)}."
                         * BERTH. chron. B; a. 1079 p. 345,3 "Heinricus rex natalem Domini apud Mogontiacum non #sati.|s magnifice celebravit."
                         * LAMB. LEOD. Matth. I; 2132 "non #sati.|s est facile dictus qua {eqs.}"
                         
                 UUUU_BEDEUTUNG  c. compar. {(c. "post": {=> satis_10})} i. q. multo -- viel:
                     * PAULIN. AQUIL. c. Fel.; 1,31 "in praecedentibus ... #sati.|s latius apostolum de his disputasse legitur negotiis."
                     * HROTSV. Mar.; 525 "ipsius {(virginis)} e casto nasci dignarier alvo altithroni prolem mundo #sati.|s antiquiorem."
                     * VITA Hildulfi; 1,9 "cum viro Dei manere decreverunt {(sc. viri multi)}; unde non #sati.|s {@ satis_10} post in militia Christi accreverunt."
                 UUUU_BEDEUTUNG c. superl. i. q. profecto -- wirklich:
                     * WIDUK. gest.; 3,58 "#sati.|s {@ satis_14}  plurimum lacrimarum pro filii interitu fudit {imperator}."
                     * BERTH. chron. B; a. 1077 p. 293,23 "ab insectatione proposita #sati.|s mestissimi cessaverunt {(sc. Rudolfus eiusque milites)}."; a. 1079 p. 350,4 "aliis ... ab optimis quibuslibet optime #sati.|s credebatur." {al.}
                     * COSMAS chron.; 1,10 p. 23,11 "quinta {(sc. regio)} ... dicitur Luca, pulcherrima visu et utillima usu ac uberrima #sati.|s nec non habundantissima pratis." 
         UUU_BEDEUTUNG c. verbo faciendi:
                 * WALAHFR. Wett.; 678 "tum #sati.|s ammonuit {angelus} proprios componere mores."
                 * HROTSV. Mar.; 720 "exanimes fragilis pro consuetudine carnis facti #sat pavitant {(sc. Maria et Ioseph)} puerum laedique timebant."
                 * WIDUK. gest.; 3,16 "hoc facto multi scelerum conscii #sati.|s perterriti {(sc. sunt)}."
                 * BERTH. chron. B; a. 1078 p. 334,12 "ex quibus cardinalis Romanus cum plerisque aliis #sati.|s periclitatus est."
                 {saepius.}
                 ANHÄNGER in struct. contam.:
                             * MEDIC. var.;  ((BullHistMed. 10. 1941.; p. 35)) "non #sati.|s vexantur in pigmenta vel antidota, quia non affert salutem, sed periculum
                             ((
                                 *{cf.} ISID. orig.; 4,1 "qui pigmenta et antidota #sati.|s ... biberint, vexantur; immoderatio enim omnis non salutem, sed periculum affert"
                             ))."

ABSATZ                       
BEDEUTUNG compar.:
     U_BEDEUTUNG usu adi. {("#sat.|ius est" sim. usu impers.)} i. q. melior, magis idoneus -- besser, passender:
             * EINH. Karol.; prol. p. 1,22 "#sat.|ius ... iudicavi eadem {(sc. facta)} ... memoriae posterorum tradere quam regis ... actus pati oblivionis tenebris aboleri."
             * WALTHARIUS; 1217 "est #sat.|ius pulcram per vulnera quaerere mortem quam solum amissis palando evadere rebus {eqs.}"
             * THIETM. chron.; 5,32 "#sacius {@ satis_8} ... arbitror esse alcius de die in diem ascendere quam {eqs.}"; 6,51 "#sacius {@ satis_9} esset huic {(sc. s. Stephani)} ecclesiae, quod numquam natus fuisset homo ille {(sc. Thiedricus episcopus Mettensis)}
             ((
                 *{cf.} $VULG.; Matth. 26,24 "bonum erat"
             ))."
             {saepius.} 
     U_BEDEUTUNG usu adv.:
         UU_BEDEUTUNG usu originario:
             UUU_BEDEUTUNG  (eo) plus -- (um so) mehr:
                 * THIETM. chron.; 4,22 p. 156,26 "urbem nobis #sacius {@ satis_7} ad nocendum eidem {(sc. Kizae militi)} commiserunt {hostes nostri}."

              UUU_BEDEUTUNG nimis, abunde -- zu sehr, zu viel, im Überfluss:
                 * WALAHFR. (?) carm.; 54,34 "nec tibi {(sc. episcopo)} Saxonia #sat.|ius dat saxea dona."
                 * WALTH. SPIR. Christoph. II; 5,140 "undarum nimiis multum contusa procellis ventorumque minas #sat.|ius perpessa furentes nunc primum tuta navim statione locavi {(sc. Nicea)}."
             UUU_BEDEUTUNG melius -- besser:
                 * AMALAR. off.; 3,18,1 "ut #sat.|ius cognoscatur dignitas diaconi."
                 * CAND. FULD. Eigil. I; 5 p. 224,34 "alius ... adfirmabat #sat.|ius eligendum {(sc. esse)} patrem doctrina pollentem."
         UU_BEDEUTUNG  fort. per confusionem c. "suavius" i. q. iucundius -- angenehmer:
             *EPIST. Teg. I; 95 "vestrę {(sc. Iudithae)} pietatis gratiam ... me {(sc. Eberhardum)} habere ... dulcius melle #sacius_ve lacte os meum iocundatur gustu."

AUTORIN Weber

UNTERDRÜCKE WARNUNG 223