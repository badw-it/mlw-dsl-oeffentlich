LEMMA scando

GRAMMATIK verbum III; -di, -surum, -ere

FORM
    form.:
        perf.: 
            -did(i): {=> scando_03}
        coniug. I.: * PAUL. ALBAR. carm.; 9,61
        coniug. II.: {=> scando_01}{=> scando_02}

STRUKTUR
    pass. impers.: {=> scando_04}

BEDEUTUNG  ascendere (in, ad) -- (auf-, empor)steigen (auf, zu), besteigen, betreten, gelangen (zu):

    UNTER_BEDEUTUNG proprie:

        UU_BEDEUTUNG de hominibus:

            UUU_BEDEUTUNG in univ.:

                * WILLIB. Bonif.; 5 p. 20,13 "celocis celeriter {!marginem} #scand.|ens coepit ignotas maris temptare vias ((* HUGEB. Wynneb.; 2 p. 107,25))."
                * HUGEB. Willib.; 3 p. 91,24 "ad ardua Alpium arces pedestrim #scand.|endo gressum dirigebant in altum."
                * WALAHFR. carm.; 66,2,1 "limina ... #scand.|ere templi."
                * VITA Burch. Worm.; 17 "consultius est, ut ... alii altitudinem aquae explorent, alii ... malum #scand.|ant (ascendant {var. l.})."
                * BERTH. chron. B; a. 1077 p. 256,2 "Alpes."
                * BENZO ad Heinr. IV.; 7,2 p. 602,12 "equum." {persaepe.}


            UUU_BEDEUTUNG spectat ad introitum in paradisum sim.:

                * HRABAN. carm.; 13,36 "tu {(sc. episcopus)} humilis #scand.|e regna superna poli."
                * ERMENR. ad Grim.; 1,12,40 (vs.) "ascendens ... polum #scand.|ere quemque monet {Christus}."
                * PAUL. ALBAR. carm.; 9,166 "celsum #scand.|ere {@ scando_01} tribunal."
                * CARM. de Herm.; 11,3 ((MGPoet. II; p. 136)) "fontem vitae #scand.|it (({spectat ad} *$VULG. apoc.; 7,17))."
                * PASS. Quir. Teg.; 12,16 (vs.) "ut ... integer ad superas #scand.|endo (suadendo {var. l.}) volaverit arces." {persaepe.}


            ANHÄNGER de ascensione Christi:

                * CARM. de Quint.; 184 "resurrexit vivens, privilegia reddens prisca suis, victor coelos post #scan.|didit {@ scando_03} altos."
                * CARM. Cantabr. A; 4,4^a,5 "tertia die surgit a morte, ... #scand.|it omnes super celos."


        UU_BEDEUTUNG de avibus:

            UUU_BEDEUTUNG in univ.:

                * WANDALB. creat. mund.; 108 "mox quaecumque natatu complent aequora quaeque #sacnd.|unt aera pennis."
                * FRID. II. IMP. art. ven.; 5 p. 135,5 "aironum ... defensio in volando #scand.|endo in altum."

            UUU_BEDEUTUNG alleg.:

                * PAUL. ALBAR. carm.; 9,57 "Lucas ut taurus magno cum murmure bombat, et pernix aquila #scand.|et {@ scando_02} super ethera Ioannes."

        UU_BEDEUTUNG de sole, luna, sideribus:

            * BEDA temp. rat.; 17,31 "cum sole devexo in brumalem {(sc. circulum)} plena est {luna}, ipsam solstitiali #scand.|ere circulo nox longissima prodit."
            * WANDALB. martyr.; 90 "geminos #scand.|it sol sidere Pisces."; mens.; 109 "#scand.|entis in aethera Tauri frontem Maiades septenis ignibus aurant." {al.}

        UU_BEDEUTUNG de rebus (usu alch.):

            * GEBER. summ.; 40 p. 362 "quoniam commixtio cum fecibus partes comprehendit grossas et tenet illas in aludel fundo depressas nec eas #scand.|ere permittit."
    
    UNTER_BEDEUTUNG in imag.:

        * WILLIB. Bonif.; 3 p. 11,26 "per sanctorum exempla arduam caelestis intellegentiae semitam feliciter #scand.|ens."
        * WALAHFR. carm.; 32,12 "nunc #scand.|it, nunc descendit, rota sic trahit orbis."
        * BRUNO QUERF. Adalb. A; 9 "qui {(Otto $II.)} ... rapidis cruribus montem imperii #scand.|it."
        * DIPL. Heinr. IV.; 246 p. 312,14 "haec {(divina lex)} sola et specialis ad caelum scala est, sine qua numquam ad Deum #scand.|itur {@ scando_04}."


    UNTER_BEDEUTUNG translate:

        UU_BEDEUTUNG gener. de honoribus sim.:

            * FROUM. epist.; 77 p. 86,17 "quanto quisque altius #scand.|it, tanto durius cadit."
            * EGBERT. fec. rat.; 1,1209 "Pytagoras plus quam quindenum #scand.|ere ramum miratur talem similemque per omnia mannis {(v. notam ed.)}."
            * NORB. IBURG. Benn.; 2 p. 873,12 "se gratia ..., qua fuerat parentibus datus, ad altiora quandoque eadem vocante #scan.|surum." {al.}

        UU_BEDEUTUNG math. de numeris; de ludo aleario:

            * GESTA Camer.; 1,89 p. 434,41 "progessio numerorum a ternario ocdenarium #scand.|ens singulae sibi virtutum virtutes legat."

        UU_BEDEUTUNG mus.:

            UUU_BEDEUTUNG spectat ad sonum, melodiam; pass. i. q. educi, evehi -- in die Höhe geführt werden:

                * AUREL. REOM. mus.; 19,29  "prior {(sc. syllaba)} ... plenum reddit sonum; secunda ... alte #scand.|etur."; 19,68 "in versibus introituum ... in tertia {(sc. syllaba)} ... acutus #scand.|etur accentus."
                * NOTKER. BALB. ad Lantb.; p. 38,31 "'s' susum vel sursum #scand.|ere sibilat."
                * HUCBALD. mus.; 21 "exemplum semitonii advertere potes in cithara VI chordarum inter tertiam et quartam chordam seu #scand.|endo seu descendendo {eqs.}"
                * COMM. microl.; 39 p. 167 "si ... prior {(sc. motus)} surgat per semiditonum interposito tono, praepositus #scand.|at per tonum."

            UUU_BEDEUTUNG in discantu q. d., voce acuta cantare -- in hoher Lage singen:

                * YSENGRIMUS; 7,105 "officium, matrina, probo, sed #scand.|is inepte, deficies media voce {(v. notam ed. ad 7,101)}."

        UU_BEDEUTUNG  comput. i. q. (mensura) attingere -- (als Ergebnis) erreichen:

            * COLUMB. epist.; 3 p. 165,6 "dum non eosdem terminos #scand.|unt libri nostrae provinciae et ... liber Gallorum."

ABSATZ

BEDEUTUNG superscandere, superare -- übersteigen, übertreffen:

    * RHYTHM.; 1,5,2 "probatus est {Medardus} quasi metallum auri per incendium - hęc speties #scand.|it argenti {(v. notam ed.)}."

ABSATZ

BEDEUTUNG metiri, mensurare -- abmessen, 'skandieren'; t. t. metr.: 

    * BONIF. (?) metr.; 46 "ex ... quattuor caesuris duae primae ad legem #scand.|endorum versuum sunt dicatae, ceterae {eqs.}"
    * CARM. var. I; 21,2,2 "hic codex pueris plus quam sapientibus aptus, si iam scire pedes malint et #scand.|ere versus."
    * EKKEH. IV. bened. I; 42,50 "ille (gloss.: Servius) legat 'iuvit', legat ista (gloss.: fides) canatque 'iuvavit' (gloss.: longum est 'iu'); nec dignor suetum sibi cantans #scand.|ere metrum (gloss.: 'iu' brevians; {v. notam ed.})." {al.}





AUTORIN Leithe-Jasper