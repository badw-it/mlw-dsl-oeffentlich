LEMMA scandalizo

GRAMMATIK verbum I; -avi, -atum, -are

ETYMOLOGIE σκανδαλίζω

SCHREIBWEISE 
    script.:
        -is(o): * AGIUS vita Hath.; 8

STRUKTUR  
    act. pro pass.:
        {=> scandalizo_03}{=> scandalizo_04}
    usu absol.:
        {=> scandalizo_11}

BEDEUTUNG materiam scandali praebere, offendere, laedere -- Anlass zum Ärgernis geben, anstößig sein (für), verunsichern, verärgern, aufbringen, beleidigen, verletzen; {mediopass. i. q.} offendi, gravari -- Anstoß nehmen, sich stoßen (an), sich unangenehm berührt fühlen (von), nicht mögen:

    UNTER_BEDEUTUNG  in univ.:

        * CAPIT. reg. Franc.; 196,23 p. 37,5 "ut quotiescumque interrogati veritatem proferimus, quamquam nonnulli infirmi sine causa #scandaliz.|entur."
        * EKKEH. IV. bened. I; 37,17 "pręcipe (gloss.: o diacon) maiores non #scandaliz.|are minores."
        * IDUNG. PRUF. dial.; 1,85 "ipsa Veritas ... tributum, cuius debitor non fuit, ne exactores eius #scandaliza.|ret, dedit {eqs.}"
        * CHART. Friberg.; 605 p. 398,25 (epist. papae) "si dixerit vel fecerit {soror} aliquid, unde #scandaliz.|entur sorores."
        * ALBERT. M. sent.; 4,17,50 p. 744^b,17 "quaeritur, utrum #scandaliz.|are {@ scandalizo_11} aliquo modo conveniat bonis."; 4,17,51 p. 745^b,22 "ut ... grammatice quis obiiciat, cum sit verbum transitivum et non est, quo transeat actus eius nisi in passivum, necesse est, quod aliquis #scandaliz.|etur {eqs.}" {saepe. v. et} {=> scandalum/scandalum_05}{=> scandalum/scandalum_06}

    

    UNTER_BEDEUTUNG  sollicitare, vexare -- beunruhigen, erschüttern, schockieren:

        * BONIF. epist.; 91 p. 208,14 "si ille {(sc. presbyter antea fornicans)} modo degradatus fuerit, secretum peccatum revelatum fuerit et #scandaliza.|verit {@ scandalizo_04} (#scandaliza.|vit, #scandaliz.|atur, #scandaliza.|bitur {var. l.}) multitudo plebium et per scandalum plurime peribunt animę."
        * RATHER. epist.; 13 p. 68,1 "#scandaliza.|tum te ... mihi balneo in vigilia circumcisionis Domini dum audio."
        {al.}

    UNTER_BEDEUTUNG  {mediopass. i. q.} consternari, indignari -- sich empören, entrüsten (über):

        * VITA Pard.; 18 ((MGMer. VII; p. 36^a,9;; rec. 1)) "prepositus #scandaliza.|tus (#scandaliz.|ans {@ scandalizo_03} {rec. 2}) atque nimio furore succensus ... carpentarios verberari precepit."
        * LAMB. HERSF. annal.; a. 1075 p. 239,26sq. "cum ... regi in pace et in bello ... semper commodissime affuisset {episcopus}, et #scandaliza.|tis in eo caeteris regni principibus solus ille nunquam #scandaliza.|tus fuisset."
        * CAES. HEIST. hom. exc.; 155 p. 124,27 "erat ... facies eius {(monachi)} quasi fulgor ex vi devocionis ...; graviter #scandaliza.|tus sacerdos putans ex hesterna ingurgitacione vini eidem hoc accidisse ... priori retulit." {saepius.} 

    

    UNTER_BEDEUTUNG  inducere, seducere (ad pravum), (de recto) detrahere -- (zum Bösen) verleiten, zum Straucheln bringen, vom (rechten) Weg abbringen:

        * HRABAN. epist.; 25 p. 432,3 "multi de populo videntes magistrorum dissensionem non mediocriter #scandaliz.|antur."
        * FLOR. LUGD. carm.; 1,164 "#scandaliz.|antem oculum propria vult {(sc. Matthaeus)} sede revelli ((*{spectat ad} VULG. Matth.; 5,29))."
        * ADAM gest.; 4,21 p. 252,16 "mali doctores, dum sua quaerunt, non quae Iesu Christi, #scandaliz.|ant eos, qui possent salvari."
        * RUOTG. Brun.; 38 p. 40,20 "quidam ... sacerdotes Domini ... terrenę plus iusto confisi potentię populum inperitum #scandaliza.|bant."
        * ALBERT. M. summ. theol. II; 9,36,4 p. 397^b,14 "Christus ... pusillus in gratia et virtute numquam fuit nec #scandaliz.|ari ad casum potuit." {saepius.}

    UNTER_BEDEUTUNG  infestare -- gefährden, unsicher machen:

        UU_BEDEUTUNG  in univ.: 

            * CHRON. Fred.; 3,19 "facilius unus feris iurgium, quam omni tempore tu {(sc. Gundobadus)} et tui #scandaliz.|eris a Francis."
            * LIUTPR. leg.; 25 p. 188,26 "regis Ottonis nuntii ... iuramento mihi {(sc. Nicephoro)} promiserunt ..., numquam illum in aliquo nostrum #scandaliz.|are imperium."

        UU_BEDEUTUNG  spectat ad unitatem ecclesiae:

            * BERTH. chron. B; a. 1076 p. 239,15 "corpus Christi, id est unitatem sancte ecclesie, scindere et #scandaliz.|are."
            * CHRON. Sigeb. cont.; a. 1140 ((MGScript. VI; p. 452,10)) "qui {(Abaelardus)} quadam profana verborum vel sensuum novitate aecclesiam #scandaliza.|bat"

AUTORIN  Leithe-Jasper