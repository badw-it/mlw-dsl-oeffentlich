LEMMA 1. scientia (-ncia)

GRAMMATIK
    subst. I; -ae f.

SCHREIBWEISE
    script.:
        essi-: {=> scientia_2}
        si-: {=> scientia_4}


BEDEUTUNG actio, status sciendi, notitia -- das Wissen, Kenntnis, Kunde:

    UNTER_BEDEUTUNG de statu alicuius rei conscio, notitia (fortuito) comperta:

        UU_BEDEUTUNG proprie:
        
            UUU_BEDEUTUNG in univ.:

                UUUU_BEDEUTUNG de #scienti.|a hominum:

                    UUUUU_BEDEUTUNG usu communi:

                    * HINCM. divort.; 5 p. 140,6 "quod plurimorum constat perpetratum notitia, plurimorum fiat sanatum #scienti.|a {(v. notam ed.)}."

                    * GERH. AUGUST. Udalr.; 1,28 l. 9 "quamvis in eius {(sc. Heinrici)} #scienti.|am deveniret, quod {eqs.}"

                    * EPIST. Worm. I; 50 p. 87,23 "communis #scienti.|a ingenii."

                    * CONST. Melf.; 1,94 "si quis ... iura nostra ... scienter ... suppresserit, in quadruplum omnium fructuum a die #scienti.|e (sententie {var. l.}) perceptorum possessorem huiusmodi condempnamus."

                    {al.}


                    UUUUU_BEDEUTUNG locut. "ex certa #scienti.|a" in formulis chartarum ('Rechtskenntnis'); usu plur.: {=> scientia_3}:

                        * DIPL. Conr. III.; 134 "privilegia ... innovantes et iura in eis indulta ex certa #scienti.|a corroborantes."; 109 p. 196,22 (interp. s. XIII.^in.) "nos ... ex certa #si|.enti.|a {@ scientia_4} {(sic)} damus in feodum {eqs.}"

                        * CONST. Melf.; 3,4,1 "qui ... tenuerit aliquid, de quo non habeat ... oraculum ... ex certa nostra #scienti.|a et conscientia impetratum {eqs.}"

                        * CHART. Lux.; III 27 p. 27,28 "hec {(sc. concessionem)} ... ex certis #scienti.|is {@ scientia_3} de proborum et discretorum consilio ... duximus {(sc. comes et comitissa)} statuenda."

                        * CHART. Tirol. notar.; II 582^a p. 371,25 "gastaldios ex certa #scien.|cia revocavit {(sc. episcopus)}."

                        {al.}

                  
                UUUU_BEDEUTUNG de omniscientia q. d., praescientia Dei; de appellatione Dei: {=> scientia_13}:

                    * GODESC. SAXO conf.; p. 67,20 "gemina Iudaeorum #scienti.|a {@ scientia_32} ((* {cf.} p. 53,3sqq))."

                    * HROTSV. Sap.; 3,22 "laudanda est ... mira mundi artificis #scienti.|a, qui {eqs.}"

                    * ALBERT. M. sent.; 1,8,3 p. 223^b,15 "quaeratur ... de aliis {(nominibus Dei)}, quae sunt sapientia, #scienti.|a {@ scientia_13}, bonitas."; 1,35^A p. 175^a,38sq. "in prima {(parte)} agit de #scienti.|a in generali, in secunda autem descendit ad species #scienti.|ae divinae, quae sunt praedestinatio et reprobatio."; summ. theol. I; 15,60,1 p. 599^a,6 "quid sit #scienti.|a Dei et quomodo multiplex sit: {eqs.}" {ibid. al.}

                            ANHÄNGER spectat ad Christum:

                                * GODESC. SAXO theol.; 24 p. 340,20 "creaturae perfectae futurae non adscribitur ... praescientia seu #scienti.|a, qua sempiternaliter ipsa creatura praescita est futura."

                                * ALBERT. M. incarn.; 2,3,4 p. 191,30 "Christus ... perfectionem caelestem, id est spiritualem, habuit ...; conceptus enim fuit plenus #scienti.|a ((* {sim.} 4,1 p. 204,15))."; sent. 3,14,1 p. 254^a,7 "utrum Christus habuerit omnium #scienti.|am sicut Deus vel non." {ibid. al.}

            UUU_BEDEUTUNG c. notione conscientiae, consensus:

                * HINCM. divort.; 5| p. 137,21 "ut nemo fidelium ... initum dissolvat coniugium sine episcopi sui #scienti.|a ((* {sim.} p. 139,3))."

                * DIPL. Otton. II.; 25 "nos #scienti.|a ac voluntate Burghardi, Alamannorum ducis, servis Dei ... theloneum ... perdonavimus."

                * GERH. AUGUST. Udalr.; 1,14 l. 12 "Hortulanus ... verbis episcopi non credens sine eius #scienti.|a praefatas res in eandem speluncam congregare coepit."

                * THIETM. chron.; 7,4 p. 402,23 "ut nullo absque #scienti.|a et consilio suimet {(imperatoris)} umquam sociaretur {Reinilda} marito {eqs.}"

            UUU_BEDEUTUNG sensus -- Bewusstsein:

                * ARBEO Corb.; 4 p. 193,5 "se ab inrationabile animale adductum sine #scienti.|a (("#scienti.|a sui" * VITA Corb.; I 3)) ad virum Dei fuisse professus est {(sc. fur in dorso mulae portatus)}."


        UU_BEDEUTUNG meton.:
        
            UUU_BEDEUTUNG de hominibus (in promulgatione q. d.):

                * DIPL. Otton. III.; 82 "omnium fidelium nostrorum ... #scienti.|ae pateat, qualiter {eqs.} ((* {sim.} * TRAD. Ratisb.; 269 "#scienti.|ę auribusque pie Deum colentium arcius inculcetur, qualiter {eqs.}"  DIPL. Loth. I.; 146 p. 331,7 [spur.] "libuit ..., quod ipsis {(ministerialibus)} liceat vel non liceat, #scienti.|e posterorum transmittere"))."

            UUU_BEDEUTUNG de charta i. q. communicatio, nota -- Mitteilung, Notiz:

                * TRAD. Patav.; 2 (a. 739) "incipit #scienti.|a, qui scire valeat, in quo tempore ędificata erat ecclesia ista {eqs.}"



    ABSATZ
    UNTER_BEDEUTUNG de notitia discendo (vel inspiratione) percepta i. q. cognitio (subtilior, certa), intellegentia, ἐπιστήμη -- (gründliches) Verständnis, (sichere) Erkenntnis, Einsicht:

        UU_BEDEUTUNG proprie:

            UUU_BEDEUTUNG in univ.:

                UUUU_BEDEUTUNG usu communi:

                    * THANGM. Bernw.; 1 p. 758,28 "institi ingenium illius ad #scienti.|am veri sophismatis excitare."

                    * ALFAN. premn. phys.; 18,10 p. 98 "proprie sunt delectationes, quae <<#scienti.|ae boni>> ((* {cf.} $NEMES.; ((PG 40,;680^C)) "κατανοήσει τοῦ θείου")) ... contingunt."

                    * OTTO FRISING. chron.; 2,8 p. 76,10 "ut ... philosophum non solum ad cognoscendae veritatis, sed ad vitandae falsitatis #scienti.|am perfecte informet {Aristoteles}."; 8,28 p. 439,3 "sciendum est duas esse #scienti.|as duasque obliviones: scit enim aliter eger infirmitatem per experientiam, aliter medicus per artis noticiam {(cf. {=> scientia_1})}."

                    * ALBERT. M. metaph.; 5,3,4 p. 262,33sqq. "quaerunt ... multi ... de #scienti.|a, quae est in anima, utrumne ipsa sit accidens."

                    {persaepe.}


                UUUU_BEDEUTUNG locut. "#scienti.|a in particulari, universali" {(cf. {=> scientia_38})}: 

                    * ALBERT. M. incarn.; 4,1,1 p. 205,23sqq. "duplex est notitia, scilicet in particulari et universali: #scienti.|a {@ scientia_1}, quae est in particulari, est per signa; et verum est de hoc, quod melius de hoc {(sc. particulari)} cognoscit aegrotus quam medicus, quia ille experitur signa in seipso; #scienti.|a vero, quae est in universali, est per artem, et secundum illam verius cognoscit medicus quam infirmus."


                UUUU_BEDEUTUNG opp.:

                    UUUUU_BEDEUTUNG "actus, opera" {sim.}:

                        * ALFAN. premn. phys.; 2,55 p. 37 "prior est #scienti.|a actu."

                        * EPIST. Ratisb.; 9 p. 318,18 "si ad hoc pervenerit, ut sciat, quid sit bonum, ... #scienti.|am operibus adornare studeat {(sc. animus)}."

                        * ALBERT. M. eth. II; 7,1,4 p. 472^a,2sqq. "habens quidem #scienti.|am ((* $ARIST.; p. 1146^b,32 "ἐπιστήμῃ")) operabilium in habitu, non utens autem #scienti.|a in agere, ita scilicet, quod #scienti.|a principium actionis sit, scire dicitur {eqs.}"

                    UUUUU_BEDEUTUNG "fides":

                        * RADBERT. corp. Dom.; 2,5sqq. "nemo nescire {(sc. debet)}, quid ad fidem quidve ad #scienti.|am in eo {(sacramento)} pertineat, quia nec fides in mysterio sine #scienti.|a recte defenditur nec #scienti.|a sine fide, quae {eqs.} ((* {sim.} 4,93 "scias ..., quia ... #scienti.|a in fide {(sc. est)}." * {cf.} 4,96 "habeatur #scienti.|a in doctrina salutis"))."


                    UUUUU_BEDEUTUNG "opinio":

                        * ALBERT. M. summ. creat. II; p. 294,27 "#scienti.|a ... et opinio pertinent ad speculationem.";  p. 393,25 "dicit ... Aristoteles in fine primi Posteriorum, quod '#scienti.|a ((* $ARIST.; p. 89^a,1 "ἐπιστήμη")) et opinio sunt circa verum'."; p. 429,61 "in intelligentiis separatis {(sc. post mortem)} non est opinio et ignorantia, sed certa #scienti.|a, in hominibus autem est opinio et ignorantia." {ibid. al.}


                    UUUUU_BEDEUTUNG "sapientia" ((* {cf.} H. Meyer, Die Zahlenallegorese im Mittelalter. 1975.; p. 35sqq.)):

                        * RUP. TUIT. trin.; 40,33 "#scienti.|a omnium bonarum et licitarum artium est notitia, sapientia vero unius tantum rei, id est Dei."; 40,143sqq. "#scienti.|a sapientia_que differunt ut maius et minus, quia plura #scienti.|ae quam sapientiae nomine significamus, et nomen #scienti.|ae quam sapientiae generalius est." {ibid. al.}

                        * HONOR. AUGUST. anim.; 1 "de hoc {(sc. ignorantiae)} exsilio ad patriam {(i. sapientiam)} via est #scienti.|a: #scienti.|a enim in rebus physicis, sapientia vero consideratur in divinis."

                        {saepius. v. et {=> scientia_18}.}


            UUU_BEDEUTUNG de {@ scientia_34} cognitione, revelatione sim. a Deo data:

                UUUU_BEDEUTUNG spectat ad homines:

                    UUUUU_BEDEUTUNG in univ.:
         
                        * MANEG. c. Wolfh.; 21 p. 91,3 "ut ... Spiritu ... 'orbem' replente 'terrarum' multis ... nationibus vocis #scienti.|a traderetur ((* {sim.} EPIST. Ratisb.; 8 p. 311,31 "omnium linguarum #scienti.|am"))." 

                        * HILDEG. div. op.;  1,4,95 l. 72 "ipsa {(anima)} ... quatuor alas {@ scientia_14} habet, scilicet sensum et #scienti.|am, voluntatem et intellectum {eqs.} ((* {cf.} G. Schleusinger-Eichholz, Frühmittelalterl. Stud. 6. 1972.; p. 481sqq.))."

                        * VITA Landel. Ettenh.; 4,5 p. 118,11 "ostendens {(sc. mater filiae curatae)} omnibus #scienti.|as {@ scientia_6} Dei habentibus, quia mirabilis est Deus in sanctis suis."

                        * ALBERT. M.  summ. creat. II; p. 392,10 "divina #scienti.|a, quae accipitur per revelationem, non reducitur ad naturam."

                        {al. v. et {=> scientia_17}.}
                        

                    UUUUU_BEDEUTUNG de distinctione boni et mali in iuncturis "#scienti.|a bona et mala", "#scienti.|a speculativa" ((* {spectat ad} $VULG.; gen. 2,9)) ((* {cf.} B. Widmer, Heilsordnung u. Zeitgeschehen in d. Mystik Hildegards v. Bingen. 1955.; p. 66. 142sq.)):

                        * HILDEG. scivias; 1,4 l. 838 "tu, o homo, in speculativa #scienti.|a bonum et malum inspicis."; 3,2 l. 162 "eis {(hominibus)} palam aperiens {(sc. Deus)} speculativam #scienti.|am (cognitionis {add. cod.}) duarum optionis causarum."; vit. mer.; 2,887 "per cibum ... istum {(sc. in peccato originali sumptum)} bona #scienti.|a obdormivit, et mala #scienti.|a per alienam viam surrexit."; div. op. 1,4,71 l. 34 "qui ... in duabus #scienti.|is plenus est et per bonam #scienti.|am Deum cum bonis operibus amat; quem etiam per malam mala opera cognoscendo timet."; epist.; 103^R,144 "dextera ala {(sc. rationalitatis, cf. {=> scientia_14})} #scienti.|a bona est et sinistra mala #scienti.|a est {eqs.}" {ibid. persaepe.}


                    UUUUU_BEDEUTUNG sapientia, prudentia -- Weisheit, Klugheit:

                        * LEX Sal. Pipp.; prol. 1 "inquerens {gens Francorum} #scien.|ciae (#scienti.|ae, #scien.|cie {sim. var. l.}) clavem ((* {sim.} ALEX. MIN. apoc.; 3| p. 41,25 "'qui {(sanctus et verus)} habet clavem', id est #scienti.|am, 'David'."; p. 41,29. p. 42,5)) ((* {spectat ad} $VULG.; Luc. 11,52))."

                        * HRABAN. epist.; 21 p. 427,15 "in superficie ... et in medulla virtutem tenet {(sc. liber Sapientiae)} #scienti.|ae et pietatis." 

                        * CHART. Heinr. Leon.; 16 "Dominus et vera loquendi audatiam eademque discrete proferendi vobis {(sc. Wibaldo)} ... contulit #scienti.|am."
            

                UUUU_BEDEUTUNG spectat ad angelos, satan:

                    * RUP. TUIT. vict.; 1,8| p. 14,6 "habuit {satanas} ... materiam superbiendi speciositatem, #scienti.|am et magnitudinem proprię conditionis (({sim.} p. 14,13. 14,18))."

                    * ALBERT. M. sent.; 2,3,16^{capit.} "utrum #scienti.|a {@ scientia_33} angelorum sit universalis vel particularis, scilicet quod species vel formae, per quas intelligunt, sunt universales vel particulares."; cael. hier.; 3 p. 48,38 "'hierarchia est ordo divinus' ... 'et' est '#scienti.|a' ((* $DION. AR.; ((PG 3,;164^D)) "ἐπιστήμη")), id est lumen perficiens speculativam partem."



            UUU_BEDEUTUNG facultas (specialis), artis peritia -- Fertigkeit, Fähigkeit, Fachkenntnis, 'Expertise':

                * WILLIB. Bonif.; praef. p. 1,9 "non propriae ludis (laudis {var. l.}) litterari #scienti.|a confidens, sed {eqs.}"

                * BERTH. chron. B; praef.| p. 168,5 "in hac {(arte geometriae)} nemo maiorum tanta #scienti.|a et subtilitate preditus fuit {(v. notam ed.)}."

                * HERM. IUD. conv.; 2 p. 76,14 "#scienti.|am legendi scripturas {@ scriptura_7} assecutus sum."
                
                * OTTO SANBLAS. chron.; 35 p. 52,33 "bellandi."

                * CONST. Melf.; 3,45 "cum testimonialibus litteris de fide et sufficienti #scienti.|a (#scienti.|am {var. l.}) ... ordinatus {(sc. medicus)}."

                * FRID. II. IMP. art. ven.; 1 p. 160,27 "quod exercitium habeat {falconarius} ... omnia faciendi, que facere convenit circa falconem, per #scienti.|am, quam prehabuit ex hoc libro nostro."

                {al.}


            UUU_BEDEUTUNG eruditio, mens docta -- Bildung, Gelehrtheit;  c. gen. inhaerentiae: {=> scientia_11}:

                UUUU_BEDEUTUNG in bonam vel neutram partem:

                    * CONC. Karol. A; 39^B,10 p. 446,11 "neque ..., quae {(sanctaemoniales)} ... #scienti.|ae doctrinis pollent, se ceteris praeferant. "

                    * WALAHFR. Karol. prol.; p. XXVIII "regni ... nebulosam et ... caecam latitudinem totius #scienti.|ae nova irradiatione ... luminosam reddidit."

                    * WIDUK. gest.; 2,36 p. 97,18 "Brun magnus erat ingenio, magnus #scienti.|a."

                    * OTLOH. prov.; Q 89 "quanto maior #scienti.|a uni cuique datur, tanto magis temptari permittitur."

                    * OTTO FRISING. gest.; 1,52 p. 74,29 "a #scienti.|a haut censura morum vitaeque gravitate discordante non iocis vel ludicris, sed seriis rebus mentem applicarat {Gilebertus episcopus}."

                    * EPIST. Hild.; 141 p. 239,15 "florentem vestrę {(sc. discipuli)} profunditatis #scien.|ciam {@ scientia_11} ... cognovimus {(sc. magister)}."

                    {persaepe.}


                UUUU_BEDEUTUNG in malam partem de eruditione vana, saeculari ((* {loci fere spectant ad} $VULG.; I Cor. 8,1)):
        
                    * CAND. FULD. Eigil. I; 9 p. 226,40 "omnes aequaliter ... diligatis {(sc. fratres)} in Christo, ut serviatis Deo et concordiae et non ... tumentis #scienti.|ae fraudulentiis carnisque vitiis."

                    * OTLOH. prov.; D 73 "de Domino vix quid perfecta #scienti.|a sentit."

                    * MANEG. c. Wolfh.; 10 p. 64,7 "#scienti.|am inflatam et mortuam spiritu caritatis animaverunt {(sc. amatores humilitatis)}."

                    * EPIST. Hann.; 79 p. 128,23 "omnia ... ingenia quadam sordidissima #scienti.|a velut fęce oblita sunt."

                    * ALEX. MIN. apoc.; 8 p. 130,21 (add. C) "excelsi {(sc. Macedonius et complices)} ... per #scienti.|am, sed pestiferi per doctrinam."

                    {al.}


        ABSATZ
        UU_BEDEUTUNG meton.:

            UUU_BEDEUTUNG de hominibus, angelis:

                * HUGEB. Willib.; praef. p. 88,2 "ista omnia ... scripta vestra cara coram #scienti.|a presentata ... vestrae {(sc. lectorum)} conmendamus perceptione."

                * THANGM. Bernw.; 1 p. 758,26 "quas {(lectiones)} ... illorum {(puerorum)} #scienti.|ae inprimebat."

                * HERIB. hymn.; 6,2,2 "primum virtutes igneae, mox repletae #scienti.|ae, exin iuvate nos prece, sessiones Dominicae."



            UUU_BEDEUTUNG de actione discendi vel docendi:

                * WALTH. SPIR. Christoph. I; prol. p. 65,12 "cum ętatis inopia #scienti.|ę_que mihi moram fecisset ignavia, vix paucissimas arentium quasi stipularum aristas arripui."

                * CHART. Mog. A; II| 517 p. 852,22 "de quibus {(scholaribus)} ipse {(scholasticus)} nullam curam vellet habere nisi de sola #scienti.|a et victu ((* {sim.} 532 p. 886,34 "magister ... scolares ... nutriat moribus, disciplina, #scienti.|a, vectu et vestitu"))."


            UUU_BEDEUTUNG facultas cognoscendi, intellectus -- kognitive Fähigkeit, Erkenntnisvermögen, Verstand:

                    * ALFAN. premn. phys.; 1,78 p. 20 "inconveniens est merito #scienti.|a ((* $NEMES.; ((PG 40,;529^A)) "φρονήσεως")) expertia ... dicere propter se facta esse."
                    
                    * ALBERT. M. veget.; 1,88 "animal habet perfectiores operationes et #scienti.|as ({ed.}, sententias {codd.}) quam planta."; 1,90 "si #scienti.|as ({ed.}, sententias {codd.}) illi {(animali)} attribuit {Aristoteles} ..., tunc #scienti.|a sumitur metaphorice, quoniam {eqs.}" {ibid. al.}


    ABSATZ
    UNTER_BEDEUTUNG ratio, methodus (sciendi), doctrina, disciplina, ars, studium -- Methode (wissenschaftlicher Erkennnis), (wissenschaftliche) Theorie, Lehre, Disziplin, 'Fach', Kunst, Wissenschaft(szweig) ((* {cf.} L. Schütz, Thomas-Lexikon. ^{2}1895.; p. 724sqq.)):

        UU_BEDEUTUNG proprie:

            UUU_BEDEUTUNG in univ.:

                UUUU_BEDEUTUNG usu communi:

                    * HRABAN. epist.; 14 p. 403,6 "Ebrei cuiusdam ... in legis #scienti.|a ... eruditi opinionem ... inserui."

                    * THANGM. Bernw.; 1 p. 758,44 "fabrili ... #scienti.|a et arte clusoria ... excelluit."

                    * ARS med.; 1 p. 418,4 "Asclepius ... Chironi centauro traditus est ad medicinae #scienti.|am (#ess|.ien.|cie {@ scientia_2} {G}) comprehendendam."

                    * CONST. Melf.; 3,46 p. 413,16sq. "ut nullus studeat in medicinali #scienti.|a, nisi prius studeat triennio in #scienti.|a {@ scientia_8} logicali."

                    * ELIAS SAL. mus.; praef.| p. 16^a "veritas et claritas #scienti.|ae artis musicae ubique partium mundi ... evanuit ((* {sim.} p. 17^b))."

                    {persaepe.}


                UUUU_BEDEUTUNG iuncturae selectae:

                    UUUUU_BEDEUTUNG "#scienti.|a divina, humana":

                        * WILLIB. Bonif.; 8 p. 51,16 "confractis librorum repositoriis ... pro auro volumina et pro argento divinae #scienti.|ae cartas reppererunt {(sc. pars turbae insanientis)}."

                        * ODILO CLUN. Adelh.; 10 "abbatem ... prefecit ..., humana #scienti.|a et divina sapientia doctum."

                        * PAUL. FULD. Erh.; 1,1 "posthabitis mundani sophismatis cavillationibus totum se divinarum #scienti.|arum dedit utilitatibus."

                        * IDUNG. PRUF. argum.; 861 "magister Lantfrancus, divina et humana #scienti.|a valde eruditus."


                    UUUUU_BEDEUTUNG "#scienti.|a liberalis":

                        * THIETM. chron.; 7,72 p. 486,25 "ille {(Reinbernus praesul)} ... liberali ... #scienti.|a a prudentibus magistris educatus gradum episcopalem ascendit ... dignus."

                        * THANGM. Bernw.; 1 p. 758,42 "quamquam vivacissimo igne animi in omni liberali #scienti.|a deflagraret {eqs.}"

                        * OTLOH. prov.; C 57 "clerici liberalis #scienti.|ae nimis ignari nullum sacerdotalem gradum accipere sunt digni."


                UUUU_BEDEUTUNG opp. "ars, virtus":

                    * ALFAN. premn. phys.; 12,2 p. 87 "virtutes, ut iustitia, et #scienti.|ae ((* $NEMES.; ((PG 40,;660^B)) "ἐπιστῆμαι")), ut grammatica, artiumque rationes, ut architectonica."


            UUU_BEDEUTUNG de doctrina per modum procedendi demonstrativum acquisita:

                UUUU_BEDEUTUNG usu communi:
                
                    UUUUU_BEDEUTUNG def. ((* {cf.} N. Schneider, Scientia und Ars im Hoch- und Spätmittelalter. 1994; p. 178sqq.)):

                        * ALBERT. M. summ. creat.; II p. 394,53 "#scienti.|a est eorum, quae cognoscuntur per causam, et quoniam impossibile est aliter se habere."; bon.; 492 p. 256,62sqq. "#scienti.|a accipitur quadrupliciter: quandoque enim dicit habitum speculativum ..., et sic accipitur pro #scienti.|is speculativis {@ scientia_15} ...; secundo accipitur #scienti.|a {@ scientia_18}, prout ab Augustino dividitur contra sapientiam ...; tertio modo dicitur #scienti.|a {@ scientia_17}, prout est donum, ...; quarto modo dicitur #scienti.|a ... id, quod ex ratione est determinatum ..., et tunc est conclusionis proprie ((* {sim.} Is.; 11,2| p. 172,91.; p. 173,5sqq.))."; eth. II; 1,1,2 p. 4^b,4sqq. "in omni #scienti.|a duo sunt: doctrina et usus; doctrina est sicut informans et dirigens, usus autem sicut exsequens et faciens {eqs.}"; Iob 12,12 "#scienti.|a est de rebus demonstrabilibus et necessariis." {ibid. al.}


                    UUUUU_BEDEUTUNG exempla:

                        * ALBERT. M. eth. II; 1,1,2 p. 3^a,5 "non est {virtus} scibilis (discibilis {ed. J. Müller, Natürliche Moral und philosophische Ethik bei Albertus Magnus. 2001. p. 328}), et #scienti.|a de ipsa esse non potest {eqs.}"; 6,2,2 p. 409^b,10sqq. "quamvis ars exerceri possit absque magna #scienti.|a et ratione, tamen doceri non potest sine forma #scienti.|ae {eqs.}"; metaph.; 6,1,2 p. 303,54sqq. "'si omnis #scienti.|a ((* $ARIST.; p. 1025^b,25 "διάνοια")) aut practica' sive moralis 'aut poetica' sive factiva et artificialis 'aut' est 'theorica' {eqs.}"; animal. quaest.; 11,1 p. 218,18sq. "cum ... omnis #scienti.|a faciat scire, ergo omnis #scienti.|a {@ scientia_12} erit demonstrativa; nulla ergo erit narrativa."; 11,6 p. 220,75 "triplex est ordo procedendi in #scienti.|a, scilicet divisivus, diffinitivus et collectivus." {ibid. persaepe.}

 
                UUUU_BEDEUTUNG iuncturae selectae:

                    UUUUU_BEDEUTUNG "#scienti.|a civilis":

                        * ALBERT. M.  bon.; 537 "civilis #scienti.|ae est finis humanum bonum, hoc est, quod homini in vita est optimum."; eth. II; 1,3 p. 30^a,15 "his quatuor virtutibus sive potentialibus #scienti.|is #scienti.|a civilis perficitur, scilicet legis positiva, iudicativa, exercitativa et communicativa." {ibid. al.}


                    UUUUU_BEDEUTUNG "#scienti.|a demonstrativa" {sim.}:

                        * ALBERT. M. animal.; 11,18 "aliter necessarium est in opinabilibus et mutabilibus, quae sunt naturalia, 'et aliter in intelligibilibus' et scibilibus, 'quae sunt <<#scienti.|arum demonstrativarum>>' ((* $ARIST.; p. 640^a,2 "θεωρητικῶν ἐπιστημῶν"))."; metaph.; 3,1,2 p. 107,38 "#scienti.|a {(sc. est)} ..., quando fundatur per demonstrationem"; 3,2,2 p. 115,4 "omnis ... #scienti.|a demonstrativa 'exigit aliquod genus subiectum' {eqs.}" {ibid. al. v. et {=> scientia_12}.}

                    UUUUU_BEDEUTUNG "#scienti.|a logica, logicalis": 
                                
                        * ALBERT. M. metaph.; 1,1,2 p. 3,8 "#scienti.|ae logicae non considerant ens vel partem entis aliquam, sed {eqs.}"; 2,13 p. 104,10 "modus ... iste {(sc. philosophiae)} est in #scienti.|is logicis secundum omnes partes perfectarum et imperfectarum argumentationum." {ibid. al. v. et {=> scientia_8}.}


                    UUUUU_BEDEUTUNG "#scienti.|a moralis":

                        * ALBERT. M. eth. II; 1,1,1 p. 1^a,10sqq. "cum omnis #scienti.|a sit de numero bonorum et honorabilium, tamen ... una alia melior est et honorabilior ...; sola autem moralis sit de bono et honorabili per essentiam dictis {eqs.}"; 1,1,3| p. 7^b,10sq. "si moralis #scienti.|a est #scienti.|a usualis {eqs.}" {ibid. al.}


                    UUUUU_BEDEUTUNG "#scienti.|a naturalis":

                        * ALBERT. M. veget.; 2,73 "hoc certissimum est ex omnibus in #scienti.|a naturali determinatis, quod {eqs.}"; phys.; 1,1,1^{capit.} "quae pars essentialis philosophiae sit #scienti.|a naturalis."; 3,2,1^{capit.} "de probatione per dicta physicorum, quod naturalis (universalis {@ scientia_36} {var. l.}) #scienti.|ae est tractare de infinito."; animal. quaest.; 1,1| p. 77,34sq. " ista {(sc. de animalibus)} #scienti.|a est pars #scienti.|ae naturalis."; p. 78,6 "alia pars <<#scienti.|ae naturalis>> (physicae {var. l.}) est liber physicorum ... et liber de caelo {eqs.}" {ibid. al.}


                    UUUUU_BEDEUTUNG "#scienti.|a {@ scientia_38} particularis, universalis" {sim.}:

                        * ALBERT. M. animal.; 11,5sq. "oportet, quod ... 'sit {doctrina sapientis} <<de cognitione alicuius naturarum et de #scienti.|a particulari>>' ((* {cf.} $ARIST.; p. 639^a,10 "τινος φύσεως ἀφωρισμένης")) accidentium illius 'singulariorum' et propriorum: 'et' secundum hunc modum 'alius' ... 'erit doctus et sapiens' in cognitione 'naturarum alterius rei' singularis et 'propriae', licet una sit communis #scienti.|a ((* {cf.} [[$ARIST.]]; p. 639^a,12 "ἱστορίας")) physicorum, quae est de mobili corpore in communi: haec enim non sufficit, nisi habeantur etiam #scienti.|ae propriae de rebus propriis et appropriatis in natura; in particularibus enim rebus 'omnis #scienti.|a' particularis 'habet differentias notas, per quas separatur ab alia particulari #scienti.|a'." {ibid saepe. v. et {=> scientia_36}. cf. {=> scientia_33}.}


                    UUUUU_BEDEUTUNG "#scienti.|a practica, theorica":

                        * ALBERT. M. eth. II; 1,1,4 p. 11^a,4sqq. "videndum est, utrum sit  #scienti.|a theorica vel practica vel etiam mechanica, ut quibusdam placuit {eqs.}"; pol.; 3,8^a p. 271^a,12 "'in omnibus ... #scienti.|is', practicis scilicet, quae sunt secundum rationem, sicut prudentia, 'et artibus', quae sunt factivae secundum rationem ..., 'bonum finis', scilicet est."; metaph.; 1,1,1^{capit.} "tres sunt #scienti.|ae theoricae, et ista {(sc. metaphysica)} est principalis inter tres."; 1,1,11 p. 16,73 "sunt #scienti.|ae mathematicae, quae simpliciter sunt theoricae, licet etiam ad opus referri possint."; summ. theol.; 1,3,3 p. 13,33 "utrum sit {theologia} #scienti.|a practica vel theorica." {ibid. saepe. v. et {=> scientia_23}.}


                    UUUUU_BEDEUTUNG "#scienti.|a {@ scientia_35} activa, speculativa":

                        * ALBERT. M. summ. creat.; I 4,38,1 p. 550^b,1sqq. "#scienti.|a dicitur multipliciter: dicitur enim #scienti.|a {@ scientia_23} practica et #scienti.|a speculativa; et illa, quae est practica, est cum affectu, quae autem est  speculativa, est finis intelligentiae speculativae {eqs.}"; metaph.; 1,2,1 p. 17,40 "speculativae #scienti.|ae magis nomen sapientiae participant quam activae {eqs.}" {ibid. saepius. v. et {=> scientia_15}.}



        UU_BEDEUTUNG meton. de expositione, libro:

            * ALBERT. M. lin.;^{tit.} p. 498 "incipit #scienti.|a libri de lineis indivisibilibus, quae facit ad #scienti.|am libri sexti Physicorum."; veget.; 4,1 "plantarum vires naturales ... in hoc quarto libro huius #scienti.|ae dicere suscipimus."; animal.; 1,3 "#scienti.|am ... de membris animalium dividemus secundum duplicem considerationem ipsorum {eqs.}"; 1,46 "diximus in #scienti.|a de animae operibus ..., quod {eqs.}" {ibid. al.}


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 516