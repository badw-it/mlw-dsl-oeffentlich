LEMMA scarifatio (-cio) VEL scarificatio

GRAMMATIK 
    subst. III; -onis f.

SCHREIBWEISE
    script.:
        -factio: {=> scarifatio_2a} * {adde} HILDEG. caus.; 263 capit. p. 10.
                                    * ROGER. SALERN. chirurg.; 2,350 (add. M)


BEDEUTUNG t. t. medic. i. q. actio charaxandi, minutio sanguinis per cucurbitulas sim. facta -- das Ritzen, Schröpfen:

    * AESCULAPIUS; 1 p. 2,34 "#scarifi.|cationem cucurbitae cum emissione sanguinis fieri placet."

    * AURELIUS; 6 p. 50,1 "#scari.|fatione[m] (#scarifa.|ctione {@ scarifatio_2a} {var. l.}) adhibita[m]."

    * PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ... cum #scarifi.|catione ({B}, #scari.|fatione {A}) ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως"))."; 71 p. 48,24 "ventosandum ... cum #scarifi.|catione ((*[[PAUL. AEG.]]; 3,20,1 p. 168,23 "ἀμυχῶν"))."

    * TRACT. de chirurg.; 1241 "#scari.|facio ... sicut flebotomia et ceterae inanitiones plus in vere et autumpno ... fieri debent."

    * ALBERT. M. animal.; 22,68 p. 1385,30 "sicut vulnera curentur ipsae plagae #scarifi.|cationis."

    {persaepe.} 

        ANHÄNGER adde:

            * CHART. Rhen. med.; III 289 p. 234,16 (a. 1226) "quod dictus $L. ..., quoties vocatus fuerit ab aliquo nostrum {(sc. conventus)} indigente minutione vel #scarifi.|catione flebotinii {(sic)}, personaliter veniet cum minutore."


AUTORIN Niederer