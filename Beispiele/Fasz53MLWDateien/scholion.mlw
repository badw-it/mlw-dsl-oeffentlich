LEMMA scholion (sco-) VEL scholium (sco-)

GRAMMATIK
    subst. II; -i n.

VEL *scholia (sco-)

GRAMMATIK
    subst. I; -ae f.

ETYMOLOGIE σχόλιον


BEDEUTUNG annotatio, nota, commentarius, explanatio -- Bemerkung, Notiz, Kommentar, Erklärung {(saepius usu plur.)}; usu attrib.: {=> scholion_2} ((* {cf.} U. Dill, Museum Helveticum. 61. 2004.; p. 105sqq.)):

    UNTER_BEDEUTUNG de glossis (in margine scriptis):

        * ANAST. BIBL. epist.; 5 p. 411,10 "quaedam ..., sicut mihi nota erant, ... #scholi.|is in marginibus codicis exaratis annotavi vel ... explanavi."; 13 p. 432,22sqq. "ubi a verbis interpretis #scolia ... dissentire vidi, ut lector, quid de apposita dictione interpres senserit, quid #scolion {@ scholion_1a} insineret, indifficulter agnoscat, et verba interpretis #scolio inserui et {eqs.}" {al.}

        * ALBERT. M. summ. theol. I; 1,5,1 p. 17,31 "Dionysius in Caelesti Hierarchia cap. I: '...'; ubi dicit #scolium {@ scholion_1b} de Graeco in Latinum translatum: {'eqs.'} {(v. notam ed.)}."


            ANHÄNGER per pravam interpr.:

            * ALBERT. M. pol.; 3,9^g p. 289^a,8 "'significat ... Alcaeus, quod elegerunt {Mytilenaei} Pittacum tyrannum, in quodam <<#scholi.|orum {@ scholion_2} versuum>> ((* {cf.} $ARIST.; p. 1285^a,38 "τῶν σκολιῶν μελῶν"))'; #scholi.|um dicitur ..., quod in margine iuxta textum versuum ponitur."


    UNTER_BEDEUTUNG de libro; de epistula introductoria: {=> scholion_1}:

        * CATAL. biblioth. Lauresh.; p. 163,11 "Cyrilli episcopi Alexandriae #scolia {@ scholion_1c} de incarnatione unigeniti ((* {sim.} * CATAL. biblioth. A; I 18 p. 86,2 "hos ... libros ... Hartmotus ... fecit conscribi: ... #scoliam Cyrilli de incarnatione Domini"))."

        * VISIO Alber.; epist. p. 18,5 "necessarium duxi visionis nostre libellum tali #scolia {@ scholion_1} premunire, presertim cum {eqs.}"

        * PETR. CAS. (?) chron.; 4,66| p. 530,28 "#scolias in veteri testamento {(sc. Petrus Diaconus scripsit, v. notam ed.)} ((* {sim.} p. 531,13 "#scolias in diversis sententiis"))."


AUTORIN Niederer