LEMMA scibilis

GRAMMATIK
    adi. III; -e

SCHREIBWEISE
    script.:
        si-: * HUGO HONAUG. (?) ignor.; 18 {(a. corr.)}

METRIK
    metr.:
        scĭb-: {=> scibilis_1}

GEBRAUCH
    pendet:
        gen.: {=> scibilis_2}
        acc.: {=> scibilis_1}


BEDEUTUNG (cog)noscibilis, qui (scientia, intellectu) comprehendi, (certe) sciri potest -- (wissenschaftlich, mit dem Verstand, sicher) erkennbar, wissbar:

    UNTER_BEDEUTUNG adi.:

        * HROTSV. Sap.; 9,5 "te {(sc. Deum)} ... collaudet totius #scibil.|is rei scientia."

        * SIGEB. GEMBL. chron.; a. 1047 "Aristoteles ait: 'circuli quadratura, si est #scibil.|e {@ scibilis_3}, scientia quidem nondum est, illud vero #scibil.|e est'."

        * EPIST. Hildeg.; 215 "quod {(sc. affectionem)}, si quis ... sciret, vel nihil vel ea, que non sunt #scibil.|ia, sciret."

        * ALBERT. M. sent.; 4,43,7 p. 516^a,17 "an #scibil.|e sit tempus adventus Domini ad iudicium."; * cael. hier.; 7 p. 110,17 "adducuntur {(sc. primae essentiae angelorum)} ad 'rationes divinorum operum', quae sunt '#scibil.|es' ((* $DION. AR.; ((PG 3,;208^D)) "ἐπιστημονικούς")) {eqs.}"; * metaph.; 6,2,2^{capit.} "accidens ... est non-ens et ideo non perfecte #scibil.|e."

        {al.}


    UNTER_BEDEUTUNG subst. neutr.:

        UU_BEDEUTUNG philos. et theol.:

        * HROTSV. Pafn.; 1,20 "nec scientia #scibil.|is Deum offendit, sed iniustitia scientis ((* {sim.} 1,21))."

        * ALBERT. M. eth. I; 498 p. 428,29 "'omnis scientia est docibilis', quantum ad docentem, et omne '#scibil.|e' ((* $ARIST.; p. 1139^b,25 "τὸ ἐπιστητόν")) est 'discibile' ex parte discipuli."; * meteor.; 3,2,7 p. 136,32 "curam de #scibili.|bus habuerunt et omnia ad causas visibiles et corporeas retulerunt {Epicuraei}."; * metaph.; 10,2,7 p. 452,7sqq. "'omnis scientia' est scientia '#scibil.|is', sed 'non omne #scibil.|e' est 'scientiae, quia modo quodam scientia mensuratur' per '#scibil.|e'."; * summ. theol. I; 15,60,4 p. 616^a,24 "quomodo scientia Dei se habet ad #scibil.|e." {ibid. persaepe. v. et {=> scibilis_3} {=> scibilitas/scibilis_3a; scibilitas/scibilis_3b}.}
        

            ANHÄNGER res, quaestio per scientiam investiganda -- (Forschungs-)Gegenstand, wissenschaftliche Fragestellung: 
            
            * ALBERT. M. bon.; 81 p. 52,17 "cum ... sancti sequantur divina et philosophi #scibil.|ia {eqs.}"; 420 p. 224,84 "est #scibil.|e theologicum et #scibil.|e ethicum et #scibil.|e iuris civilis vel canonici."

            * sent.; 2,21,1| p. 353^b,29 "hoc {(sc. temptatorem non accipere experimentum scientiae)} ... est intelligendum ... de tentatione tentatoris in #scibili.|bus, quia tentator imitatur dialecticum."; p. 354^a,30 "quandoque ... est {temptatio} interrogatio experimenti in #scibili.|bus secundum intellectum speculativum."
                        
            * phys.; 1,1,1 p. 3,28 "facile innotescit, quo ordine se habet {physica} ad alias partes philosophiae realis: est enim ipsa ordine sui quaesiti et #scibil.|is ultima, sed tamen ordine doctrinae est ipsa prima." {ibid. al.}

        UU_BEDEUTUNG usu communi:

            * ALBERT. STAD. Troil.; 5,948 "totum #scibil.|e scire putat {Anthenor}."


BEDEUTUNG notus -- bekannt:

    * PAUL. FULD. Erh.; 1,5 p. 12,7 "ambos {(sc. Hildolfum et Erhardum)} et praecellere nobilitate ac nobiles scientia #scibil.|es_que {@ scibilis_2} meritorum fore ... non latet memoriam."


BEDEUTUNG sciens, gnarus -- wissend, verstehend:

    * CATAL. Frising.; ((MGScript. XXIV; p. 318,13;; vs.)) "sancta fuit {Chunradus episcopus} #scibil.|is {@ scibilis_1}."

AUTORIN Niederer

UNTERDRÜCKE WARNUNG 223