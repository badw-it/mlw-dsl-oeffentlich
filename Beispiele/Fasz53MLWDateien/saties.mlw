LEMMA saties (sacies)

GRAMMATIK
     subst. V; -ei
       
VEL {semel} {({=> satietas/saties_1})} satias 

GRAMMATIK
     subst. III; -atis f.

BEDEUTUNG satietas -- Sättigung, Sattheit; nimia satietas -- Übersättigung, -füllung: {=> saties_2; saties_3}:

     U_BEDEUTUNG proprie:
             * WALAHFR. Gall.; prol. p. 282,18 "ita pabulosa {(sc. Hibernia est)}, ut pecua ibi, nisi interdum aestate a pastibus arceantur, in periculum agat #satie.|s {@ saties_2} (( *{ex} $SOLIN.; 22 "#sati.|as"))."
             * WALTH. SPIR. Christoph. II; 5,100 "ut ... non ruat {agna} in saltum #saciem factura luporum."
             * VITA Pirmin. II; 14 "rivus ... tantam subito fertur eructuare piscium copiam, ut ... omnibus ... #sati.|em (satietatem {@ satietas_1} {var. l.}) plenam inferret."
             * EKKEH. IV. bened. I; 1,123 "carnis ab ardore supero tutabere {(sc. Maria)} rore, quem tibi primevę #satie.|s mala contulit Evę."
             * METELL. exp. Hieros.; 4,929 "ieiunus miles tuus {(sc. Domini)} insequitur temulentos, pinguis aqualiculi #sacie {@ saties_3} turgente refertos."

     U_BEDEUTUNG translate:
         UU_BEDEUTUNG tranquillitas animi -- Zufriedenheit:
             * RHYTHM.; 124,11,3 "largus in donis, semper vigil et prudens, omnium onus amabili nisu adimere gaudens: #sacies ... haec manebit tibi {(sc. Wilhelmo)}."
             {v. et {=> satietas/saties_1}}

         UU_BEDEUTUNG eventus, effectus -- Erfüllung, Verwirklichung:
             * VITA Poppon.; 3 p. 295,49 "ubi {(sc. ad sepulcrum Domini)} voti sui ... compotes effecti {(sc. Poppo, Rotbertus, Lausus)} optionisque suae #sati.|e (#sacię {var. l.}) preces vel suspiria fundendo contenti {eqs.}"; 4 p. 296,24 "ad limina apostolorum ... pervectus, ubi et voti sui #sati.|em ... dedit."; 18 "tandem ... desiderii sui #sati.|em invenit."

         UU_BEDEUTUNG ubertas, abundantia -- Üppigkeit, Fülle:
             * EKKEH. IV. carm. var. II; 5,21 "tedeat ęqualem (gloss.: Nasoni in carmine) #satie.|s Nili Iuvenalem."

AUTORIN Weber