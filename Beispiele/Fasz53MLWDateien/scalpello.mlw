LEMMA  scalpello

GRAMMATIK  verbum I; -atum, -are

SCHREIBWEISE

    script.:

        scap(p)il(o): {=> scalpello001}{=> scalpello003}

        scapel(o): {=> scalpello004}

        scarpil(o): {=> scalpello002}

STRUKTUR

    form. coniug. III.: {=> scalpello004}

        partic. perf. usu adi.: {=> scalpello005}

BEDEUTUNG  scalpello resecare, scarifare, conficere -- mit dem Skalpell ausschneiden, einritzen, bearbeiten:

    * COMPOS. Luc.; M 19 "qualibet opera picta aut #sca.|ppilata {@ scalpello001} (scapilata {@ scalpello003} {a. corr.}, -rpilata {@ scalpello002} {Mu}) inlucidare super debeas."

    * THEOD. CERV. chirurg.; 3,11 p. 162^{C} "si locus ... in ... nigredinem conversus fuerit, oportet locum aqua calida fomentari suaviter, deinde locum #scalpell.|a."

    * WILH. SALIC. chirurg.; 1,3 p. 305^{A} "quarta ... die locus #scalpell.|etur ..., ut fluat sanguinis multitudo; ... post exitum sanguinis lavetur locus totus #scalpella.|tus{@scalpello005}."

BEDEUTUNG  scalpello abradere -- mit dem Skalpell abschaben:

    * IOH. IAMAT. chirurg.; 8,19 p. 64,26 "ab hac {(cute)} ..., cum #scapeluntur {@ scalpello004}, decidunt crustule piscium squamis similes." {v. et} {=> scalpo/scalpo004}

AUTOR  Fiedler