LEMMA schema (scema)

GRAMMATIK
   subst. III; -atis n.

ETYMOLOGIE σχῆμα

SCHREIBWEISE
     script.:
         cema: {=> schema_11}
         z(c)em(a): {=> schema_2; schema_3} *{adde} CHRON. Erf. mod. II; a. 1215 p. 215,23 {(var. l.)}
         staem(a): {=> schema_4}
         sthem(a): * VITA Dalm.; 11 ((MGMer. III; p. 549,28;; {var. l.})) 
         stem(m)(a): {=> schema_5; schema_6}
         scaem(a): {=> schematicus/schema_1}
         scesm(a): {=> schema_9}
         -meta: {=> schema_10}
         -the:{=> schema_2}
FORM
      form.:
          gen. pl. decl. I.: * ANON. mus. Sowa;  1 p. 96,17     
         
BEDEUTUNG habitus, forma, species, figura -- Form, Aussehen, Erscheinung(sform), Gestalt:
     U_BEDEUTUNG spectat ad res:
        UU_BEDEUTUNG gener.:
             UUU_BEDEUTUNG strictius:
                 UUUU_BEDEUTUNG in univ.:
                     * LAUDES Veron.; 6,1 ((MGPoet. I; p. 120)) (a. 781/810) "dicere lingua non valet huius urbis #scemata (#scemeta {@ schema_10} {var. l.}): intus nitet, foris candet circumsepta laminis."
                     * RUD. FULD. Leob.; 8 p. 125,15 "globum ex eo {(sc. filo)} rotundo #sce|.mat.|e volvendo formavit."
                     * PETR. DAM. epist.; 86 p. 472,2 "hic {(polypus)} affigitur petrae et saxei mentitur #scema coloris."
                     * CARM. imag.; 21,3^e "#scem.|a crucis typicum meditatur vita bonorum."
                     * ARS med.; 5| p. 423,12 "fellita vessica ... #sce|.mat.|e (#sces|.mat.|e {@ schema_9} {var. l.}) oblonga."; p. 424,2 "#sce|.mat.|e est ... matrix sicuti cucurbita medicinalis."
                     * CONR. MUR. nov. grec.; 2,1282 "lapides ... quadros notat, unde paratur arte pavimentum mirando #sce|.mat.|e."
                     {saepius.}    

                 UUUU_BEDEUTUNG spectat ad aedificia:
                     * THANGM. Bernw.; 8 p. 761,37 "inter quae {(aedificia)} quaedam elegantiori #sce|.mat.|e albo ac rubro lapide intermiscens musiva pictura varia pulcherrimum opus reddidit."
                     * ROD. CAMER. Lietb.; 47 capit. p. 842 "quod domnus Lietbertus eadem {(sc. ecclesiola)} intacta aliam ampliori #sce|.mat.|e construxerit."
                     * ANNAL. Rod.; a. 1106| p. 692,12 "Conradus ... construxit monasterium Traiecti #sce|.mat.|e in honore sanctae Mariae."; p. 694,5 "construxerunt criptam ... sacerdos et frater ... iacientes fundamentum monasterii #sce|.mat.|e Langobardino."
                     * CHART. com. Mansf.; VI 23 "quam {(ecclesiam)} antecessores nostri {(sc. Burchardi)} venerabili #sce|.mat.|e fundaverunt."
                     {al.}
                                         
                 UUUU_BEDEUTUNG spectat ad vestimentum:
                     * GISLEM. Droct.; 20 ((MGMer. III; p. 539,26)) "qui {(abbas)} ... Droctoveum insignire studuit #sce|.mat.|e monastico
                     ((
                      *{sim.} * HIST. Hirs.; 4 p. 257,19 "monachili #sce|.mat.|e vestiri desiderat {Gebehardus}." {al.}
                     ))." 
                     * HUCBALD. Leb.; 12 p. 888^C "clericali ... #schemat.|e insignitus ... crucis signum praeferebat manibus."
                     * CONSUET. Vird.; 23 p. 408,8 "vesperas agent fratres in cotidiano #sce|.mat.|e."
                     * CHART. Salem.; 575 p. 190,4 "cum ... paucis diebus sub vili #zemathe {@ schema_2} deguissem."
                     {al.}
                     ANHÄNGER in imag.:
                         * NOTKER. BALB. Gall.; 1^a,2,4 "tu {(sc. Hartmannus)} prophetarum celebres Camenas ... aemulans tempus sine laude nudum #sce|.mat.|e vestis."            
                 
             UUU_BEDEUTUNG latius:
                 UUUU_BEDEUTUNG dispositio, configuratio -- Anordnung:
                     * CARM. Salisb. I; 13,1 "stellarum in caelo fabulose #sce|.mat.|a quidam signa ferunt formata."    
                
                 UUUU_BEDEUTUNG modus, typus -- Art, Weise, Typ:
                     * CARM. Paul. Diac. app. I; 5,10 "cum Petrus ... ad moenia finibus omni #schemat.|e virtutum plenus venisset eois."
                     * WILLIR. cant.; 8,5 p. 241,5 "affluit omnigenę virtutis #schemat.|e plenę, viribus haud propriis fidit {(sc. ecclesia)}."
                     * ARS med.; 7 p. 428,18 "de catagmate; generalis ... est divisio ossis in <<quolibet #sce|.mat.|e>> (qualibet #escemata {G}), aliquando curabilis et periculosa."
                     * ALBERT. M. pol.; 6,3^d p. 577^a,40 "'fuit #schem.|a' ((*$ARIST.; p. 1318^b,26 "σχῆμα")) sive forma 'in democratiis antiquis.'"
                     {al.}

                 UUUU_BEDEUTUNG consuetudo, regula -- Vorschrift, Regel:
                      * PASS. Thiem. II; 10 p. 57,49 "fuit ... quidam religiosus ex illis, qui apud canonicos et monachos exteriores fratres in cappa vocantur, cuius #scema ordinis et habitus is etiam Noricae regioni primus intulisse narratur."

                 UUUU_BEDEUTUNG versus, carmen -- Vers, Gedicht:
                     * CARM. de Rob.; 30 "leta ... tibi debebunt #scemata scribi, his animadversis cum tam bonus atque tener sis."
     
                 UUUU_BEDEUTUNG exemplar, ratio, norma -- Vorbild, Beispiel, Muster:
                     * ERMENR. ad Grim.; 1,10,56 "monoceros corpore parvo, cornu tamen confligere audet: eo #schemat.|e et ego putto ... a facie ipsius magni Homeri non fugio."
                     *COSMAS chron.; praef. p. 3,11 "scio nonnullos affore emulos ..., cum viderint #scema (#scemata, #schem.|a {var. l.}, scena {a. corr. A}) huius operationis."
                     * CARM. Bur.; 43,5,2 "ludit ad interitum rerum coniectura quodam vili #sce|.mat.|e, docet ut natura."

                     ANHÄNGER spectat ad parabolam:
                         * CHART. Brem.; 323 p. 360,21 (a. 1266) "sicut in evangelio in homine, qui descendit ab Ierusalem in Iericho et incidit in latrones, pio (pro {var. l.}) #sce|.mat.|e declaratur
                         ((*{spectat ad} $VULG.; Luc. 10,30sq.))."

         UU_BEDEUTUNG geom. ('Figur'):           
             * GERB. geom.; 6,4 p. 90,3 " si ... ager ... orthogonii #scema (#scemas {var. l.}) tenens proponitur."

         UU_BEDEUTUNG anat. et medic. i. q. positio, collocatio -- Lage, Stellung:
             * MAURUS progn.; 16 p. 34^a,27 "virtus regitiva nequid regere membra in proprio #sce|.mat.|e et propria positione."
             * GLOSS. Roger. I B; 4,3 p. 709,41 "medicus ... ossa coniungat et reducat ad ipsum locum vel #scema."
             * BRUNUS LONG. chirurg.; praef. p. 105^C "quae {(operatio)} ... fit in ossibus dislocatis a suo proprio #schemat.|e et appellatur coniunctio disiunctorum."

             ANHÄNGER c. gen. inhaerentiae:
                 * IOH. IAMAT. chirurg.; 6,11 p. 40,24 "eodem positionis observato #sce|.mat.|e, ut in fractura tybie uti consuevimus, reitera ... mutationem."

         UU_BEDEUTUNG rhet.:
             UUU_BEDEUTUNG figura rhetorica -- Redefigur:
             
                 UUUU_BEDEUTUNG in univ.:
                     * CAPIT. reg. Franc.; 29 p. 79,33 "cum ... in sacris paginis #schema.|ta, tropi et caetera his similia inserta inveniantur
                     ((
                     *{sim.} EPIST. var. II; suppl. 5 p. 621,6 "cum nulla sim scientia liberalium artium obpletus, non syllogismorum #sce|.mat.|um_que (#schemat.|um_que {var. l.}) ac troporum figuris ornatus {eqs.} {al.}"
                     ))."
                     * THEOD. AMORB. Bened. I; praef. p. 362,45 "quare, inquies {(sc. Richardus abbas)} ..., ironicis #schemati.|bus nostram ... inscientiam tam impudenter verbosando deludis?"
                     * HIST. peregr.; p. 116,12 "si minus in hoc opusculo venustas carminis seu verborum #scemata lectoris aures demulcent."
                     * SALUTARIS POETA; 192 "rhetoricae vultu gestuque loquendo refulge, verbula pinge tua #sce|.mat.|e sive tropo."
                     {persaepe.}

                     ANHÄNGER in malam partem:
                         * GESTA Apoll.; 361 "scin {(sc. Taliarchus minister)} ab Apollonio subtili #sce|.mat.|e structo actibus ex nostris religatur quaestio turpis?"

                 UUUU_BEDEUTUNG de certo #schemat.|e:
                     * OTFR. ad Liutb.; 81 "non quo series scriptionis huius {(sc. evangeliorum Theodiscorum)} metrica sit subtilitate constricta, sed #schem.|a (#scema {P, $V a. corr.}) omoeoteleuton assidue quaerit."
                     * GODESC. SAXO gramm.; 1| p. 390,10 "#scemata dianoeas ad oratores pertinent, ad grammaticos lexeos
                     ((*{sim.}; p. 413,18))."; 2| p. 482,2 "#schem.|a, quod dicitur auxesis, id est augmentum."
                     {v. et} {=> schematicus/schema_1}          

             UUU_BEDEUTUNG stilus -- Stil:
                 * WALAHFR. carm.; 80,2,5 "accipe {(sc. magister)} litterulas deformi #sce|.mat.|e factas."
                 * ISO Otm.; 2,1 p. 52^a,45 "sancti patris nostri miracula hactenus a nobis vili sermonum #sce|.mat.|e prolata."

         UU_BEDEUTUNG mus. i. q. consonantia -- Konsonanz; de re v. LexMusLat. vol. II. s. v.:
             * ODO ARET. ton. A; p. 249^b,9 "organum anoton, symphonia varietas proti, chorda vero scembs, uti primum tonum incipiens <<et #scemata>> (#schem.|a et #schemata {var. l.}) D."
             * ANON. mus. Sowa; praef. p. 66,19 "metrica {(sc. musica)} consistit in metris melicis variis #sce|.mati.|bus purpuratis."; 1 p. 96,36 "#sce|.mat.|is, id est concordantie alicuius, #sce|.mat.|is dico {eqs.}"
             {ibid. al.}

     U_BEDEUTUNG spectat ad homines:
         * VITA Audoini; 3 ((MGMer. V; p. 555,17)) "sub purpura auro nitente contexta pro fidei ardore propria corporis #stema {@ schema_5} (#staema{@ schema_4}, #stemma{@ schema_6}, #schem.|a, #scema {var. l.}) dura premebat cilicia."
         * GOSB. carm.; 38 ((MGPoet. I; p. 621)) "Guillelmo dinami, sophia, #sce|.mat.|e compto Gosbertus tapinos, micros, apodemus et exul."
         * GESTA Ern. duc. I; 2,209 "soli virgini nate regis pepercit, quam ob #sce|.mat.|is et forme divalis claritudinem ... secum abduxit."
         {al.}
         ANHÄNGER de evangelistae symbolo:
             * CARM. imag.; 23^h,1,2 "primus {(sc. evangelista Matthaeus)} ab humana quia coepit promere gente, humani formam #schemat.|is inde vehit."
    
     U_BEDEUTUNG spectat ad Christum:
         * WALAHFR. carm.; 83,5,5 "qui pius mundi Deus ac redemptor, martyr et miles radians herilis rore robustus roseo salutis #sce|.mat.|e servi."

     U_BEDEUTUNG spectat ad angelum:
         * GERHOH. Antichr.; 1,35 p. 344,6 "an forte zema {@ schema_3} vel habitum illius angeli, qui resurrectionis Christi testis in fulgureo vultu atque in niveo vestium candore apparuit, ... nituntur {episcopi} imitari, ut {eqs.}"

     U_BEDEUTUNG spectat ad daemones:
         * EKKEH. IV. pict. Sangall.; 59 "temptantur {(sc. Gallus eiusque socii)} nudis mulierum #scę|.mat.|e (gloss.: specie) larvis."
         ANHÄNGER meton. de daemonibus ipsis:
         * EKKEH. IV. pict. Sangall.; 142 "plures larvarum (gloss.: #sce|.mat.|um) cinis (gloss.: Galli) hic fugat Asmodearum (gloss.: demonicarum)."

ABSATZ
 BEDEUTUNG imago -- Bild, Abbild(ung):
     U_BEDEUTUNG spectat ad res corporeas:
         UU_BEDEUTUNG de carminibus figuratis:
             * HUGO FLOR. hist.; p. 364,14 "qui {(Hrabanus)} ... de laude crucis librum diversis #sce|.mati.|bus decoratum metrice composuit."
         UU_BEDEUTUNG (de)lineatio, diagramma -- Zeichnung, Diagramm:
             * ALBERT. M. caus. element.; 1,2,1 p. 61,41 "linea DB erit quantitas longitudinis solis a zenith capitum nostrorum ..., huius .... figurae istud est #schem.|a; isto #schemat.|e sic disposito {eqs.}"
         UU_BEDEUTUNG adumbratio -- Umriss, 'Schemen':
                 * VITA Virg. Salisb. II; prol. p. 88,32 "patuerunt indicia, et picturae vetustioris deaurata ...  visa sunt #scemata
                 ((
                     *{ed.} MGMer. VI; p. 545,27 "picture vetustioris colorata visa sunt illic #scemata (#ce|.mata {@ schema_11} {cod.})"
                 ))
                 ."
                 
         UU_BEDEUTUNG spectat ad nummi formam impressam:
             * CONST. imp. II; 24,6 "legatus sub anathemate prohibebit monetam in Coloniensi #sce|.mat.|e cudi Aquis numquam vel alibi extra civitatem Coloniensem."
             
     U_BEDEUTUNG spectat ad res incorporeas:
         UU_BEDEUTUNG imaginatio -- Vorstellung:
             * PETR. DAM. epist.; 70 p. 321,7 "sepe ... michi malignus hostis hoc #scema proposuit et illos esse felices ac beatissimos, qui tam iocunde viverent, persuadere temptavit."

         UU_BEDEUTUNG visio -- Vision:
             * ERMENR. Har.; 43 "tercio ... cum hoc signum {(sc. campanarum sonitum)} #sce|.mat.|e audisset."           
             * LAMB. HERSF. annal.; a. 1056 p. 68,8 "vidit eodem rursus #sce|.mat.|e Dominum residere."
             * VITA Altm.; 37 "eodem #sce|.mat.|e et Sigiberch cum Annone episcopo et suis fratribus vidit {(sc. quidam saecularis)}."
             ANHÄNGER de figura in visione:
                 * LAMB. TUIT. Herib.; 2,19 p. 242,5 "hic {(paralyticus)} in visu admonetur prępulchro #sce|.mat.|e, ut {eqs.}"

         UU_BEDEUTUNG signum, aenigma -- Zeichen, Sinnbild:
             * RADBERT. corp. Dom.; app. l. 53 "profiteris {(sc. Fredegardus)} te ... legisse, quod tropica locutio sit, ut corpus Christi et sanguis esse dicatur; quod si figurata locutio est vel #scema potius quam veritas, nescio, inquis, qualiter illud sumere debeam."
             * CARM. imag.; 19,2 "#sce|.mat.|e sermonis Marcus gerit ora leonis."
             * RUP. TUIT. dial.; 1,239 "signum foederis, signaculum fidei, circumcisio carnis ... quoddam #schem.|a vel habitus fuit professionis."
             * ALBERT. M. eccl. hier.; 5,29 p. 134,58 "'pedum', id est utrorumque genuum, 'ministrativam dispositionem', id est diaconum ordinem, '#schemat.|e' ((* $DION. Ar.; ((PG 3,;516^A)) "σχήματι")), id est figura, 'consummante', id est implente {eqs.}"
             

AUTORIN Weber