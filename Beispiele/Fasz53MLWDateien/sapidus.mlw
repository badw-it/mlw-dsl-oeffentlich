LEMMA  sapidus

GRAMMATIK  adiectivum I; -a, -um

GEBRAUCH

    usu subst.: {=> sapidus003}

BEDEUTUNG  proprie i. q. saporus -- schmackhaft, geschmackvoll ((in imag.: {=> sapidus001}{=> sapidus004})):

    U_BEDEUTUNG  in univ.:

        * AESCULAPIUS; 5 p. 10,4 "in dimissionis tempore cibos #sapid.|os accipiant {(sc. melancholici)}."

        * EKKEH. IV. bened. I; 36,17 "nectar apes #sapid.|is stipant super ęthera sucis."

        * CARM. Cantabr. B; 3,20,3 "lactatus {(sc. Christus)} #sapid.|o ubere matris."

        * RUODLIEB; XI 47 "quam bene sint #sapid.|i, videant {famuli}, panes africani."

        * CONR. MUR. physiol.; 1330 "huius {(scorpionis)} sunt #sapid.|e carnes." {al.}

            ANHÄNGER  in imag. {@ sapidus001}:

                * WALAHFR. carm.; 18,18 "te {(sc. abbatem)} ... sit certum ... et me atque alios exemplorum verbique lucerna instruere et #sapid.|is infundere posse salivis."

                * BERNO epist.; 3 p. 21,25 "in huius scedulae calce nostrum ... animadvertite {(sc. archiepiscopus)} commonitorium, etsi minus eloquentiae melle #sapid.|um."                    

            ANHÄNGER  subst. {@ sapidus003} neutr. pl. i. q. cibi sapori -- schmackhafte Speisen:

                * VITA Udalr. Cell. II; 67 "nec dulcia avidius nec minus #sapid.|a gustans despectius."

    U_BEDEUTUNG  de eucharistia i. q. dulcis, suavis -- lieblich, süß (in imag.):

        * ELIS. SCHON. epist.; 14 p. 147,26 "quo amplius epulamur gustu suavitatis eius, tanto nobis #sapid.|ior {@ sapidus004} et appetibilior est {Deus}."

    U_BEDEUTUNG  c. sensu negat. i. q. saporem fortem, acrem habens -- einen starken, ausgeprägten Geschmack habend:

        * CONSTANT. AFRIC. theor.; 5,99 p. 24a^{r} "nix ... cadens ... supra metallinos montes aut terras #sapid.|as et fetentes omnimodo cavenda est."

BEDEUTUNG  translate i. q. sapiens, prudens -- weise, klug, verständig:
  
    * SALOM. III. carm. I; 1,192 "si non absurdum cuiquam vel forte molestum esset, in hoc #sapid.|o (#sapid.|i {cod.}) vellem sermone morari {eqs.}"

    * DIPL. Otton. II.; 283 p. 330,1 "#sapid.|a indagine perscrutari cepimus cenobia {eqs.}"

    * EPITAPH. var. II; 94,8 "virtutum cupidus vir bonus ac #sapid.|us {(sc. Gebehardus)}."

    * TRAD. Frising.; 1415 "Werinhario #sapid.|o canonicorum provisori annuente."

    * WOLFHER. Godeh. II; 18 p. 206,25 "pueros, quos ... bonae indolis et #sapid.|os invenit, per diversa scolarum studia circumquaque dispertivit."

    * EPIST. Ratisb.; 8 p. 305,5 "sunt quedam interserenda, quę ... non minoris tamen sunt utilitatis quam virtutis, si tantum non minus #sapid.|a invenirent audientium precordia." {al.}
    
VERWEISE

* sapitus

SUB_LEMMA  sapide

GRAMMATIK  adverbium

BEDEUTUNG  sapienter -- auf kluge Weise:

    * EPITAPH. var. II; 87,7 "munia quam #sapid.|e sibi credita rexit {pontifex} ubique, produnt mirifica plura sui merita."

    * CHART. Brand. A; VIII 103 "quoniam ... eis, que geruntur ab hominibus, propter eorum subitam mutationem semper solet oblivio novercari, #sapid.|e huic obvians ratio eadem suevit scripturarum testimonio perhennare."

AUTOR  Fiedler