LEMMA scilicet

GRAMMATIK
    adv.


SCHREIBWEISE
    script.:
        essi-: {=> scilicet_3}
        c-: * CHART. Bund.; 1241 p. 37,1
        si-: {=> scilicet_2} * CHART. Bund.; 546 p. 55,2. * CHART. Traiect.; 1256. {saepe.}
        -izet: * CHART. Bund.; 1051 p. 499,33.
        scilia et {(in loco corrupt.)}: {=> scilicet_1}

METRIK
    metr. {(in fine versus hexametri pravi vel corrupt.)}:
        scĭlīcet: * CARM. libr. III; 80,10


BEDEUTUNG videlicet, nempe, certe -- selbstverständlich, natürlich, sicherlich, freilich; vi vocis fere evanida: {=> scilicet_2}:

    UNTER_BEDEUTUNG in univ.:

        * WILLIB. Bonif.; 7 p. 38,8 "quattuor ... hiis {(parrochiis)} praesedere fecit episcopos, quos ordinatione #scilice.|t facta in episcopatus gradum sublevavit."

        * RIMB. Anscar.; 14 p. 36,18 "cui {(Gauzberto)} ... augustus ... cellam ... quasi locum refugii tradidit, ut #scilice.|t ad ipsius {(sc. pontificatus)} ministerium officii perpetua stabilitate deserviret."

        * CHRON. Salern.; 80 p. 78,18 "ille {(Landolfus)} ... ei {(Sikenolfo)} ... omnem voluntatem #si|.lice.|t {@ scilicet_2} adimplevit  {(v. comm. ed. p. 314)}."

        * VITA Heinr. IV.; 7 p. 26,12 "relicto in Italia filio Choͮnrado ... regressus est, #scilice.|t {(supra l.)} qui se grassanti Mathildae ... opponeret."

        {al.}


    UNTER_BEDEUTUNG sequitur contrarium quoddam:

        * TRAD. Patav.; 70 (a. 820/26) "quamvis iam dudum antea #scilice.|t traditio ... esset conprobatum, attamen {eqs.}"

        * ANTIDOT. Glasg.; p. 101,10 "ieiuno propinabis {(sc. antidotum diatessaron)} #ess|.ilice.|t {@ scilicet_3}, et post diiesta confeccionem ad massando pastillum dabis."

        * BRUNO QUERF. Adalb. A; 22 "hęc <<tunc #scilice.|t fuerunt>> (ita gesta sunt {B}), sed quando digna indigni scribimus, nunc est mortuus ... frater."


    UNTER_BEDEUTUNG ut notum est -- bekanntlich:

    * CAND. FULD. Eigil. I; 10| p. 227,42 "pater Aeigil praesentabatur eius {(imperatoris)} obtutibus, vir #scilice.|t et maturae aetatis et gravis aspectu."; p. 228,46 "ne ... per adulatores et accusatores, vasa #scilice.|t intellegibilis serpentis, corrumpatur sensus tuus." {ibid. al.}


BEDEUTUNG enim, id est -- nämlich, und zwar, das heißt:

    * WALAHFR. (?) carm.; 54,31 "Suevia ... ullum si profert amne lapillum lacteolum, niveum, Parium seu #scilice.|t album {eqs.}"

    * ANNAL. Xant.; a. 864 p. 66,12 "Guntharius ... episcopus preesse videbatur, nepos #scilice.|t Hildiuuini iunioris."

    * DIPL. Karoli III.; 2 "noverit ... omnium fidelium nostrorum, presentium #scilice.|t et futurorum, industria, quia {eqs.}"

    * DIPL. Heinr. I.; 12 p. 49,11 "quatenus ... praecepta ab antecessoribus nostris, regibus #scilice.|t et imperatoribus, ... monasterio ... exibita ... roboraremus."

    {saepe.}


        ANHÄNGER in loco corrupt.:

            * ANTIDOT. Cantabr.; p. 167,27 "abrotanum viride pastellum super cancrum per dies IX ligabis mutando #scilice.|t  ({ci.,} #{scilia et} {@ scilicet_1} {cod., ed.}) subinde diebus singulis ipsum pastellum."


BEDEUTUNG autem, ergo -- aber, jedoch, also, nun:

    * DIPL. Ludow. Germ.; 85 "qui {(reges)} ... fecerunt causam iuramenti pro vineis peractam ante se recoli; cum #scilice.|t causa ... ante illos duceretur {eqs.}"

    * PS. BOETH. geom.; 701 "tetragonicum latus si inquisieris, VII esse experieris; quos #scilice.|t VII si copulatis catheto et basi aggreges, XXX efficies."

    * GERH. AUGUST. Udalr.; 1,27 l. 86 "Riuuinus de palatio rediens intravit et legationem imperatoris eo audiente recitavit; eo #scilice.|t viso legationeque audita {eqs.}"

    * TIT. metr. IV; 28,5 "tibi Heimrammo tradens {(sc. Encilmar dalmaticam)} ...; #scilice.|t exora Dominum {eqs.}"

    {persaepe.}


AUTORIN Niederer