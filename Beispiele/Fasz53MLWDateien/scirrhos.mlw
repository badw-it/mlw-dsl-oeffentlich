LEMMA scirros (-rhos, -rus)

GRAMMATIK
   subst. II; -i m.

ETYMOLOGIE σκίρρος

SCHREIBWEISE
   script. et form.:
        nom. sg.:
          sephiros: {=> scirros_1}
          superos: {=> scirros_2}
          scyrus: {=> scirros_3}
          {?}sciceronis: {=> scirros_4}
          {?}scireronis: {=> scirros_5}
        
        acc.:
          sg.:
            sciron: {=> scirros_7}
        
          pl.:
            cyros: {=> scirros_8}

BEDEUTUNG
   t. t. medic. i. q. tumor induratus, nodus -- verhärtetes Geschwür, Knoten:

   * GLOSS. med.; p. 31,3 "antequam perindurescat {hepar} et #sciron {@ scirros_7} faciat, potest ... sanari."

   *; p. 75,16 "#scireronis {@ scirros_5} (sciceronis {@ scirros_4} {codd.}): extensio nimium dura."

   * ANTIDOT. Sangall.; p. 82,41 "#cyros {@ scirros_8} in splenis solvit {(sc. emplastrum)}."

   * ARS med.; 7 p. 427,9 "#scirr.|us (scyrus {@ scirros_3} {H}, scirosis {G} {@ scirrhosis}) dicitur, cum callositate ({leg.} callositas) ... circa musculorum capita venire solet; quae {eqs}."

   * IOH. IAMAT. chirurg.; 7,8 "solet ... humor melancholicus ... in solidam sustantiam ... commutari, que #superos {@ scirros_2} nuncupatur."

   * BRUNUS LONG. chirurg.; 2,5,5 p. 123^{H} "differt #sephiros {@ scirros_1} a cancer {eqs}."

   * THEOD. CERV. chirurg.; 3,11 p. 161^{H} "ex cholera nigra {(sc. fit apostema)} et dicitur #scirr.|hos aut cancer."
   {al.}
  
  
VERWEISE  *scleros


AUTORIN
   Orth-Müller