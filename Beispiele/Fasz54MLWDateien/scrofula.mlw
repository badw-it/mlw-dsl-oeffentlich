LEMMA scrofula (-ophula, -ff-)

GRAMMATIK
    subst. I; -ae f.

SCHREIBWEISE
    script.:
        es-: {=> scrofula_1}
        sco-: * CONR. MUR. nov. grec.; 8,866 {(var. l.)}
        scruphu-: * GLOSS. Roger. I A; 2,5 capit. p. 575
        scruf-: {=> scrofula_2}
        -opu-: * CONR. MUR. nov. grec.; 8,866 {(var. l.)}
        -fol(a): {=> scrofula_1} {=> scrofa_1/scrofula_50} * {adde} ANTIDOT. Glasg.; p. 151,21


BEDEUTUNG struma, tumor (gulae, colli) -- (Hals-, Halsdrüsen-)Geschwulst, 'Skrofel':

        UNTER_BEDEUTUNG proprie:

            UU_BEDEUTUNG def.:

                * CONSTANT. AFRIC. theor.; 8,11 p. 38a^{v} "#scro.|phula est apostema quasi glans nascens in molli carne colli aut ... sub ascellis vel inguinibus ...; unaqueque pelliculis circumdatur, quare #scro.|phula vocatur, quod in collis porcorum sepe nascuntur; vel {eqs.}"

                * THEOD. CERV. chirurg.; 3,24 p. 166^F "#scro.|ffulae et glandulae sunt dura apostemata {eqs.}"

                * WILH. SALIC. chirurg.; 1,23 p. 311^C "#scro.|ffulae sunt apostemata dura ad tactum nascentia ex phlegmate indurato et exiccato."
   

               {al.}

                    ANHÄNGER de tumore equorum:

                        * IORDAN. RUFF. equ.; p. 111,18 "quae {(sc. superfluitates carnium)} dicuntur vulgariter glandulae sive testudines aut #scroful.|ae nuncupantur."

            UU_BEDEUTUNG exempla:

                * ANTIDOT. Augiens.; p. 61,5 "#scr.|ufulas {@ scrofula_2} et parotidas in collo et glandolas sanat {emplastrum}."

                * ANTIDOT. Glasg.; p. 120,2 "ad #e|.scrof.|olas {@ scrofula_1} mirificus est {(sc. emplastrum)}."

                * HILDEG. phys.; 1,92 "si quis ... #scroful.|as habet, ... accipiat lactucam {eqs.}"

                * ALBERT. M. animal.; 22,19 "si fiat ex unguibus combustis emplastrum, dissolvit #scroful.|as et curat fissuram pellis."

                {saepe. v. et {=> scrofa_1/scrofula_50}.}

                ANHÄNGER de tumore porcorum: 

                    * ALBERT. M. animal. quaest.; 7,33/9 p. 186,65 "quare porcis maxime accidunt #scro.|phulae."


        UNTER_BEDEUTUNG translate i. q. efflorescentia -- Ausblühung, Effloreszenz: 

                * MARC. GRAEC. ign.; 14 "sal petrosum est minera terrae et reperitur in #scro.|phulis contra lapides."


BEDEUTUNG sus novella, porcella -- junge Sau, Ferkel:

    * BERNH. PROV. comm.; 5,4 p. 288,36 "mulieres Salernitane, que diu fleverunt propter suos amicos mortuos, accipiunt cor #scro.|phule, id est suicule parve."



AUTORIN Strika


