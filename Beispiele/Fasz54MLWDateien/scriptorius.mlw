LEMMA scriptorius

GRAMMATIK 
    adi. I-II; -a, -um

SCHREIBWEISE
    script.:
        -tur-: {=> scriptorium_1}

BEDEUTUNG adi. i. q. ad scribendum aptus, qui scriptoris est -- Schreib-, Schreiber-:

    UNTER_BEDEUTUNG iunctura "atramentum #scriptori.|um":

        * MAPPAE CLAVIC.; 37 "cum fuerit {(sc. solutio)} atramenti #scriptori.|i pinguedinem ((* {sim.} COMPOS. Matr.; 52,1 "ex atramento #scriptori.|<o> pinguedinem ... habebis preparatum"))."

        * DYASC.; p. 43^b "atramentum #scriptori.|um ex teclarum ({leg.} tedarum) lignis fit." 
        
    UNTER_BEDEUTUNG iunctura "ars #scriptori.|a":

        * VITA Vulfr.; 6 ((MGMer. V; p. 666,18)) "quia erat {Ovo presbyter} arte #scriptori.|a eruditus, plurimos codices in praedicto transcripsit monasterio."
        
        * GESTA Camer.; 1,29 "quo {(tempore)} ipsi {(sc. erudiendi)} pueri ab ... magistro rogati ad #scriptori.|am artem liquorem facere deberent."


    UNTER_BEDEUTUNG iunctura "penna #scriptori.|a":

        * HONOR. AUGUST. imag.; 1,57 "de profundis aquarum emergamus et #scriptori.|a penna in aere suspendamus."
        
        * LAMB. ARD. hist.; 17 "cum de eis {(sc. comitibus)} nichil dicere proposuimus, pennam subtrahimus, ut morosius Eustacio primogenito #scriptori.|a penna observiamus."; 95 p. 606,44 "ad Ardensium historiam ... #scriptori.|am pennam transferamus."


ABSATZ
BEDEUTUNG subst.:

    UNTER_BEDEUTUNG neutr.:

        UU_BEDEUTUNG scriptum, instrumentum, charta -- (Rechts-)Schreiben, Dokument, Urkunde:

            * LEG. Burgund. const. I; 51,1 p. 83,14 "quisque patrum filiis conpetentes substantiae non tradiderit portiones, nihil contrarium, nihil per #scriptori.|um (#script.|urium {@ scriptorium_1}, inscriptorium, scriptorum, scripturam {var. l.}) in praeiudicium faciat filiorum."


        UU_BEDEUTUNG conclave, camera scribendi, bibliotheca -- Schreibstube, Bücherzimmer, 'Skriptorium':

            UUU_BEDEUTUNG de sala in monasteriis scriptioni librorum destinata:

                * PONTIF. Rom.-Germ.; 197 "(oratio {G}) in #scriptori.|o." 

                * EKKEH. IV. cas.; 36 p. 232,6 "convenire in #scriptori.|o collationesque ... de scripturis facere." 

                * WILH. HIRS. const.; 1,24 p. 252,7 "pro signo #scriptori.|i ... manus extensas super utrumque genu equaliter ponas adiungens scribendi signum."

                * ANSELM. GEMBL. chron.; a. 1117 p. 376,40 "fulmen ... tertium {(clericum)} de #scriptori.|o aecclesiae proximo egredientem in ipso aecclesiae ingressu extinxit."

                {al.}


                    ANHÄNGER  spectat ad libros collectos:

                        * THANGM. Bernw.; 6 "#scriptori.|a ... non in monasterio tantum, sed in diversis locis studebat, unde et copiosam bibliothecam tam divinorum quam philosophicorum codicum comparavit."



            UUU_BEDEUTUNG ?de cella, aedicula ad scribendum deputata ('Schreibkabinett, -nische'):

                * CHRON. Vill.; 30 ((MGScript. XXV.; p. 208,32)) (s. XIII.^{med.}) "cum ... desiit {(sc. Arnulphus abbas)} abbatizare, adeptus est #scriptori.|um, quod est in auditorio prioris {eqs.} {(v. notam ed.)} ((* {sim.} CHRON. Vill. cont.; 5 ((MGScript. XXV; p. 210,2)) "fecit {(sc. Almericus abbas)} fieri #scriptori.|um, quod est retro altare sancte Katherine"))."


        UU_BEDEUTUNG opus scribendi -- Schreibarbeit, -dienst:

            * HIST. Hirs.; 2 p. 256,4 "ut #scriptori.|um inter alios scriptores {@ scriptor_12} habere perhibeatur {Fridericus abbas.}"


    UNTER_BEDEUTUNG fem.:

        UU_BEDEUTUNG ars scribendi -- das Schreiben, Schreibkunst:

            * ALBERT. M. summ. creat.; II p. 17,2 "quae {(potentia)} est ... remota ab actu, ut potentia pueri ad aedificare vel ad scribere vel militare, quia nec aedificatoriam vel #scriptori.|am vel militarem scit."


        UU_BEDEUTUNG iunctura "officium #scriptori.|ae" i. q. munus scriptoris, notarii -- Amt, Aufgabe eines Schreibers, Sekretärs {(spectat ad cancellariam apostolicam)}:

            * ORD. canc. pap.; 3,4,1 "ad decorem officii #scriptori.|e servandum ego Guillelmus ...,  Romane ecclesie vicecancellarius, de mandato ... summi pontificis ... moneo scriptores suos omnes detinentes publice concubinas, quod {eqs.}"; 3,4,8 "scriptores, qui litteras curie non scripserint, antequam alias incipiant scribere, per VII dies a #scriptori.|e officio sint suspensi." {al.} 

            * CHART. Bund.; 1014  p. 464,30 (epist. papae) "ipsum {(canonicum Curiensem)}  ... privari fecimus officio #scriptori.|e."

            
AUTORIN Niederer