LEMMA  1.*scur(i)a (scoria)

GRAMMATIK  substantivum I; -ae f.

VEL  {semel ({=> scuria001})} scurius

GRAMMATIK  substantivum II; -i m.

VEL  scurium

GRAMMATIK  substantivum II; -i n.

ETYMOLOGIE

    theod. vet. sciura
    {vel} sûr:    * {v.} O. Schade, Altdt. Wörterb. 1882.; II. p. 799,814

SCHREIBWEISE

    script.:

        scruria: * LEX Sal. Merov.; 16,4 {(var. l.)}

        -icia: {=> scuria003}

STRUKTUR

    form.:

        acc. sg.:

            -a: {=> scuria004} {al.}

        acc. pro abl.: {=> scuria002} {adde} * REGISTR. Prum.; 114 p. 197,28

BEDEUTUNG  stabulum, horreum -- Stall, Scheuer:

    U_BEDEUTUNG  in univ.:

        * PACTUS Alam.; 21,4 "si intus in #scu.|ra (("-rta, curia, #scur.|ia, #scur.|iam" {var. l.; sc. ingressus fuerit})), XII solidos conponat."

        * LEX Alam.; 76,2 "si ... domus infra curte incenderit aut #scur.|ia (-ra, -iam {var. l.}) aut granica {eqs.} (({sim.} 77,3 "#scu.|ra (-iam, -iam vel granicam, granica, granicas {var. l.}) servi si incenderit {eqs.}" * LEX Sal. Karol.; 16,4))."

        * LEX Baiuv.; 2,4 "quando aliqui defendere volunt casas vel #scur.|ias (curias, scaras {var. l.}; gloss.: stadala), ubi fenum ... inveniunt."; 10,2^{tit.} "de #scur.|ia (scolariis, scolia {var. l.}) liberi (({item} 10,2 "de #scur.|ia (scolaria, escolaria {var. l.}) liberi"))."

        * ARBEO Emm.; 16 p. 49,19 "in ospiti ... #scur.|iam (curiam {var. l.}), ubi grana condere videbatur, deductus est."

        * TRAD. Frising.; 245 "de contentione, qui fuit ... de curte ... et de domo et de #scur.|io {@ scuria001} {eqs.}" {persaepe.}

    U_BEDEUTUNG  in formula pertinentiae q. d. chartarum sim.:

        * TRAD. Weiss.; 221 (a. 755/56) "ego Erlolfus ... dono ad monasterium ... casa, #scur.|ia cum peculio {eqs.} (({sim.} * COD. Lauresh.; 858 l. 9 "mansum I {(sc. confirmant Witherius et Lantfridus)} cum casa et #scu.|ra desuper."; 879 l. 4.; {persaepe}))."

        * TRAD. Ratisb.; 11 p. 10,22 "Deotpertus de eadem re Adalwinum episcopum ... revestivit ... cum omnibus ... casis vel #scu.|ris {eqs.} (({sim.} 9 | p. 8,3 "tradidimus {[sc. Einhardus et Erhardus]} ... aecclesiam cum edificiis ..., casas vel #scur.|ias {@ scuria002} {eqs.}"; p. 8,10 * TRAD. Patav.; 90 p. 77,20))."

        * TRAD. Lunaelac.; 19^{2} p. 118,22 "Folchrihus tradidit ad Sulzipah kasa, #scur.|ia {@ scuria004} (("-icia" {@ scuria003} {cod.})) cum terra {eqs.}"; 66 "cum domibus, edificiis, curtiferis, #scoriis (({sim.} 104 "casas cum curtiferis, #scoria cum terris {eqs.}"))." {al.}

AUTOR  Fiedler