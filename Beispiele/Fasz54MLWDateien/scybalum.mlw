LEMMA  scybalum (scib-, scypha-)

GRAMMATIK  substantivum II; -i

VEL  {fere} squibalum

GRAMMATIK  substantivum II; -i n.

VEL  squibala

GRAMMATIK  substantivum I; -ae f.

ETYMOLOGIE  σκύβαλον

SCHREIBWEISE

    script. et form.:

        esquir(a): {=> scybalum004}

        is cephalum: {=> scybalum001}

        squicol(a): {=> scybalum002}{=> scybalum003}

        -bul-: {=> scybalum006}{=> scybalum007}

        {?}-liores: {=> scybalum005}

STRUKTUR

        {?}acc. f.:
        
            -a: {=> scybalum002}{=> scybalum003}

BEDEUTUNG  stercus, faeces, excrementum -- Kot, Dung, Exkrement:

    U_BEDEUTUNG  hominum:

        * AESCULAPIUS; 1 p. 1,34 "si febris fuerit cum dolore capitis, requirendum erit, ne forte ventrem non purgent, quia et <<#{is cephalum}>> {@ scybalum001} (("#sci|.bal.|a" {cod. Aug. CXX f. 38^{r}})) stomachi opprimit et dolorem capitis nimium praestat."; 33 p. 52,11 "non possunt nisi vicina loca tumescere ... renum cum #scyphalorum duricia {eqs.}"

        * RECEPT. Lauresh.; 2,110 "si nimia duritia ventris #sc|.ybal.|a ragadium fecerit {eqs.}"

        * PAUL. AEGIN. cur.; 239 p. 177,11 "#squ|.ibal.|um retinetur afflicto longaone."

        * ANTIDOT. Augiens.; p. 63,21 "ut ... vetustas et paluginosas #squ|.ibal.|as facile proiciat {calasticum dialtea} (({sim.} * ANTIDOT. Glasg.; p. 124,10 "#esquiras {@ scybalum004} resolvit {dealtea calasticum}"))."

        * WILH. CONG. chirurg.; 1535 "#squibula {@ scybalum006} ... excoriationem augmentant."

        * ALBERT. M. animal.; 8,44 "#squ|.ibal.|um ... hominis ... habet tyriacae virtutem." {saepe.}

    U_BEDEUTUNG  animalium:

        * BOTAN. Sangall.; 40,3 "si quis mulier non conceperit, ut concipiat: ... prendis #squicola {@ scybalum002} leporis et #squicola {@ scybalum003} aseni, ... simul facis potionem {(cf. comm. ed. p. 383)}."

        * UNIBOS; 144,3 "sub cauda laxat {bestia} #squibulas{@ scybalum007}, sicut solet, foedissimas."

        * TRACT. de aegr. cur.; 94,14 "accipe #squ|.ibal.|am muris et idem {(sc. suppositorium)} facias."

            ANHÄNGER  fort. huc spectat:

                * ALBERT. M. veget.; 7,7 "asininum et equinum et ovinum et caprinum a rusticis stercus assumitur; haec enim animalia #squ|.ibal.|iores {@ scybalum005} (({ni leg.} "#squ|.ibal.|as rariores {[v. notam ed.]}")) (("squilialiores, siliquaraliores {var. l.}, vel squibalitas" {add. $CV})) et siccas habent egestiones."

AUTOR  Fiedler