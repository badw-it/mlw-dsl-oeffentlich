LEMMA scopulus (-polus)

GRAMMATIK
     subst. II; -i m.

VEL {raro} scopulum

GRAMMATIK
     subst. II; -i n.

ETYMOLOGIE
     σκόπελος

BEDEUTUNG saxum, rupes, cautes -- Fels, Klippe, Riff:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG in univ.:
         * AETHICUS; 94 "inter quas {(urbes)} ... Rhodus metropolis earum est, #scop.|olis_que magnis atque rupibus vallatae."
         * ERMENR. Sval.; 10 p. 161,35 "grave ... est mihi ... nil aliud cottidianis obtutibus quam saxea #scopul.|a et tedas aspicere."
         * GERH. AUGUST. Udalr.; 1,15 l. 23 "aperta collocationis sanctorum spelunca in #scopul.|o exciso."
         * LIGURINUS; 4,544 "vix inter #scopul.|os et acutas ... cautes conspexere {(sc. milites)} locum, quo {eqs.}"
         {al.}

         ANHÄNGER c. gen. inhaerentiae:
             * IONAS BOB. Columb.; 1,30 p. 222,1 "cum per prerupta {!saxorum} #scopul.|a (#scopul.|os {var. l.}) trabes ex abietibus inter densa saltus locis inaccessibilibus cederentur
             ((
                 * VITA Corb.; 32 "quis aliud de ipso arbitrabatur puero {(sc. lapso)} nisi mortuum inter s. #scopul.|a (#scopul.|os {var. l.}) ingentia disruptum."
             ))."

         UU_BEDEUTUNG in descriptione terminorum:
             * CHART. Mog. A; I 260 p. 164,27sq. (a. 1019) "hinc ... ascendendum in montana ad #scopul.|um 'Falkenstein' appellatum, ab illo ... #scopul.|o ad quandam crucem in stipite fagi dolatam."
             * INNOC. III. registr.; 2,71 "alie tres partes mare circumdant, una cum mammena omnibusque #scopul.|is ad istud promontorium subiacentia."
             * CHART. Austr. sup. II; 50 p. 56,17sq. "cuius {(silvae)} termini sunt a via, que de Thissingen per nigrum montem usque ad #scopul.|um 'Bernsteine' dirigitur et ab illo #scopul.|o recta linea limitatur usque ad fluvium Wlta."

     U_BEDEUTUNG in imag.:
         * PAULIN. AQUIL. c. Fel.; 1,8 l. 11 "modo dentatis falsitatum inpingit in #scopul.|is, modo abruptis vesaniae versari prospicitur praecipitiis, modo {eqs.}"
         * ADAM gest.; 3,1 p. 142,24 "ita plena sunt omnia #scopul.|is invidiae detractionumque asperitatibus, ut {eqs.}"
         * LAMB. LEOD. Matth. I; 14 "nec semel illisus #scopul.|is, cum carmine lento ludicra prosequerer, ad seria subrigo mentem."

AUTORIN Weber