LEMMA secretalis

GRAMMATIK
    adi. III; -e

BEDEUTUNG adi.:

    UNTER_BEDEUTUNG separatus, solitarius -- abgesondert, abgeschieden {(usu praed.)}:

        * HIST. Walc.; 13 "domus comitis inter densitatem nemoris paullo minus constabat invisibilis, quo ... remotis a se plebibus frequenter morabatur #secretal.|is {@ secretalis_1}."


    UNTER_BEDEUTUNG occultus -- verborgen:

        * PAULIN. AQUIL. c. Fel.; 1,1 l. 12 "scaturientibus internorum viscerum #secretali.|bus flabris redundantia oris conloquia."


    UNTER_BEDEUTUNG secretus, negotio secreto instructus -- geheim, mit einer geheimen Aufgabe betraut:

        * BERTH. chron. B; a. 1079 p. 357,2 "patriarcha ... nuntium #secretal.|em, qui voluntatem illius {(regis)} et obedientiam exploraret, regi ... Ratisbonam ... direxit."



BEDEUTUNG subst.:

    UNTER_BEDEUTUNG masc. i. q. consilii socius, intimus, consiliarius -- enger Gefährte, Vertrauter, (persönlicher) Berater ((* {cf.} Waitz, Verf.-Gesch. VI.; p. 375 adn. 5)): 

        UU_BEDEUTUNG de consiliariis principis, abbatis sim.:

            * DIPL. Ludow. Pii; 417 p. 1030,31 (spur. a. 855/66) "rogatu et peticione ... #secretal.|is nostri, Rabani abbatis, offerimus nos Deo ... quasdam res proprietatis nostre {eqs.}"

            * BERTH. chron. B; a. 1078 p. 294,5 "hunc {(monachum)} ... quidam ex #secretali.|bus eius {(sc. regis)} ... captum ... incarcerabant."

            * VITA Annon. I; 1,8 p. 471^a,9 "superextant ..., quae per nocturnas tenebras solo Deo teste, rarissimis #secretal.|ium suorum dumtaxat exceptis, operatus est."

            * EBO BAMB. Ott.; 1,3 p. 12,19 "ut eum quasi unicum amplectens filium #secretal.|em intimum ... poneret {imperator (v. notam ed.)}."

            * BERNH. GEIST. palp.; 1,555 "si stas exclusus a consilioque retrusus, et #secretal.|es intra sunt ostia tales: {eqs.}"


        UU_BEDEUTUNG de amicis:

            * ARNO REICHERSB. scut.; p. 1500^C "ut suis ipse {(sc. frater)} in Christo #secretali.|bus fateri solet."


    UNTER_BEDEUTUNG fem. i. q. intima, dilecta -- Vertraute, Geliebte:

        * HONOR. AUGUST. cant.; 1,8 p. 374^A "dicit ... sponsus consolans sponsam: 'o amica mea', id est #secretal.|is mea {eqs.}"


AUTORIN Niederer