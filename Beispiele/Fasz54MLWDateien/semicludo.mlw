LEMMA semicludo

GRAMMATIK
    verbum III; -clusum {vel} -clausum, -ere

FORM
    partic. perf. separatim scribitur: {=> semicludo_1}


BEDEUTUNG partim claudere -- halb schließen:

    * MAURUS progn.; 3 p. 25^b,46 "dum palpebre #semiclud.|untur."



SUB_LEMMA semiclusus VEL semiclausus

GRAMMATIK
    adi. I-II; -a, -um

BEDEUTUNG partim clausus -- halb geschlossen:

    UNTER_BEDEUTUNG proprie:

        * LIBRI Karol.; 4,19 p. 535,28 "ad singulas materias {(sc. imaginum exornationis)} ... sublevatis reverenter ac plerumque #semicl.|ausis luminibus a fidelibus est currendum?"

        * CARM. Bur.; 72,5^b,2 "subridens {amasia} tremulis #semicl.|ausis (#{semi clausis}{@ semicludo_1} {var. l.}) oculis."

        * CONSTANT. AFRIC. theor.; 10,11 p. 55b^r "si albedo oculorum in somno apparuerit et palpebre sint #semicl.|ause ((* {sim.} * MAURUS progn.; 3 p. 25^b,41 "#semic.|luse"))."

        * CAES. HEIST. mirac. I; 2,12 p. 80,17 "cuius {(ecclesiae)} ianuam campanarius exiens reliquerat #semicl.|ausam."


    UNTER_BEDEUTUNG translate de sensibus somno impeditis:

        * ALBERT. M. somn.; 2,2,4 p. 174^b,25 "expergefacti ... cognoverunt {(sc. qui lumen in somnio senserunt)} ... idem esse lumen et eiusdem lucernae, quod et prius tacite dormientes senserunt #semicl.|ausis sensibus ({cf. antea:} sensus ... debili somno et tenui ligati)."


AUTORIN Niederer