LEMMA segnis

GRAMMATIK
     adi. III; -e

SCHREIBWEISE
     script. et form.:
         sig-: {=> segnis_1; segnis_8; segnis_10} *{adde} PS. GALEN. puls.; 217 {(var. l.)}
         seng-: {=> segnis_2}
         nom. sg. m.:
             -es: * CARM. Cent.; 148,19.
         decl. II.: {=> segnis_9} *{adde} PS. GALEN. puls.; 217 {(var. l.)}

STRUKTUR
     struct. c. praep.:
         ab: {=> segnis_4}
         ad: {=> segnis_3; segnis_5}

BEDEUTUNG tardus, lentus, neglegens, piger -- langsam, träge, nachlässig, säumig (spectat tam ad mentem quam ad corpus):
     U_BEDEUTUNG de hominibus; de pulsu: {=> segnis_11}:
         UU_BEDEUTUNG in univ.:
             UUU_BEDEUTUNG usu communi:
                 * PS. GALEN. puls.; 212 "#segn.|is {@ segnis_11} (#si|.gnis{@ segnis_8}, #segn.|em, #segn.|us {@ segnis_9} {var. l.}) est pulsus, qui tardo motu ictum suum direxerit."
                 * AGNELL. lib. pont.; 1 p. 149 l. 35 "ille {(Theodorus)} non in illa {(sc. sarcophago)} positus est, nam #segn.|is ipse fuit eam stabilire."
                 * CONSUET. Eins.; 41 p. 215,6 "nullus ... nimium dimittat sanguinem, ne corde palpitet et in opere Dei sit #segn.|ior."
                 * WIDUK. gest.; 2,14 "urbani ... videntes hostes et ex itinere et ex pluvia ... #segn|iores (seniores {var. l.}) audacter erumpunt portis."             
                 * CONR. MUR. nov. grec.; 1,949 "quis #segn.|is erit animi, caret ille vigore."
                 {saepius.}
                 ANHÄNGER usu subst.:
                     * VITA Meginr.; 10 "o #segn.|is {(sc. Richarde)}, cur eum in caput non percutis, ut plagam mortalem accipiat?"

             UUU_BEDEUTUNG in figura rhet. litotes q. d.:
                 * WALAHFR. Mamm.; 1,15 "non #segn.|ior ille repertus."
                 * IOH. METT. Ioh.; 32,7 "Iohannes ad totius probabilis officii semper exsecutionem non #segn.|is{@ segnis_5}."
                 * GESTA crucig. Rhen.; 2 p. 32,24 "quos {(sc. Martinum et Petrum)} audacter sequitur non #segn.|is turba suorum."
                 {al.}
                 
         UU_BEDEUTUNG c. praep.:
             * CAPIT. episc. ; III p. 116,5 "si moribus incomptus et domo sua turpiter versans #segn.|is_que {@ segnis_4} ab officio {(sc. aliquis claudicans exstiterit)} ..., volumus, ut ... ammoneatur."        
             * VISIO Godesc. B; 6,1 "quia ad audiendum verbum Dei #segn.|es {@ segnis_3} eratis {(sc. peccantes)} ..., idcirco venit super vos hec tribulacio."

     U_BEDEUTUNG de animalibus:
         * WALAHFR. carm.; 8,25 "quae tam #segn.|is erit donis ingrata supernis, quae se hoc {(sc. floris de germine Iesse)} non sponte nectare pascat apes?"

     U_BEDEUTUNG de rebus; fere incorporeis; corporeis: {=> segnis_12; segnis:13; segnis_14}:
         UU_BEDEUTUNG in univ.:
             * AGNELL. lib. pont.; prol. A "scriptorum ... Ravennatum per circiter annos penna diu #segn.|is {@ segnis_12} neglexit centies octo almae summorum moderamina clara per orbem pontificum vitae passim narrasse suorum."
             * HEIRIC. Germ. I; 3,234 "hoc {(sc. farre)} pastae volucres vitiosa silentia noctis deponunt #segn.|em_que diem concentibus urgent."
             * WIPO gest.; 39 p. 59,13 "imperator finem sibi imminere sentiens ... in extremis nihil #segn.|ioris fidei permansit."
             * SCHOL. enchir.; 1,22 "quod {(sc. quod tota cantilena laeditur)} fit, ubi, quod canitur, aut #segn.|i (#si|.gni {@ segnis_10} {var. l.}) remissione gravescit aut non rite in sursum cogitur."
             * CONSUET. Rod.; 170,14 "sopor #segn.|em menti torporem inferens."
             * CARM. de Frid. I. imp.; 853 "vita quid arta nimis, que semper #segn.|ia sprevit otia."   
             {al.}

             ANHÄNGER in figura rhet. litotes q. d.:
                 * CARM. Paul. Diac. app. I; 34,10 "curre per Ausoniae, non #segn.|is {@ segnis_13} epistola, campos."
                 * WALAHFR. hort.; 376 "herbarum in numero, quas hortulus ille recenti semper prole creat, nepetae non #segn.|ior {@ segnis_14} exit surculus."

         UU_BEDEUTUNG serus -- spät (eintretend):
             * YSENGRIMUS; 1,632 "nescis {(sc. vulpes)}, quod cupidos #segn.|ia lucra necant? tarda magis cupidos quam perdita lucra molestant."
             * WARN. paracl.; 633 "nulli promisit Deus, ut mors #segn.|is (segnies, segnitiei {var. l.}) ei sit."

         UU_BEDEUTUNG paulatim obrepens -- schleichend, nach und nach eintretend:
             * CHART. episc. Hild.; III 413 p. 195,16 (a. 1275) "eo studio, quo #sengnis {@ segnis_2} oblivio excluditur, ... tenax memoria includitur rebus gestis."           

SUB_LEMMA segniter
GRAMMATIK
     adv.
BEDEUTUNG tarde, lente, neglegenter, pigre -- auf langsame, träge, nachlässige, säumige Weise:
     U_BEDEUTUNG in univ.:
         * RIMB. Anscar.; 25 p. 56,22 "nunc et sacrificia solita subtrahitis {(sc. incolae)} et vota spontanea #segn.|ius offertis."
         * CAPIT. episc. ; II p. 61,22 "filios suos #segn.|ius ac lenius, quam debuerit, corripuerat {(sc. Heli sacerdos)}
         ((
         *{spectat ad} $VULG.; I reg. 4,12sqq.
         ))."
         * CHRON. Salern.; 155 "princeps Guaimarius non #si|.gnite.|r {@ segnis_1} gessit, sed ... eum {(sc. patrem)} comprehendit."
         * EUPOLEMIUS; 1,336 "cui {(sc. dominanti)} quo non eriperentur {(sc. captivi)}, #segnite.|r affectans agiles belloque probatos deformem tremulumque senem ... vir bonus ... direxit {(v. notam ed.)}."
         {al.}
         ANHÄNGER in figura rhet. litotes q. d.:
             * RUD. FULD. Leob.; 9 "quorum {(doctorum)} adminiculo iniunctam sibi legationem non #segnite.|r administravit {(sc. Bonifatius)}"
             * HROTSV. Gall.; I 13,2 "quam {(Constantiam)} haut #segnite.|r emi vitae pretio
             ((
              *{cf.} $BOETH.; cons. 2,4,5
             ))"
             * WALTH. SPIR. Christoph. I; 2 "zelum Dei non #segn.|ius mente frequentavit et opere."
             * DIPL. Heinr. III.; 393 p. 546,9 (spur.) "si iusticia a Dei dispensatoribus non #segnite.|r fuerit recta."

     U_BEDEUTUNG compar. i. q. minus -- weniger:
         * CHART. Heinr. Leon.; 53 "#segn.|ius irritant animos dimissa per aures, que sunt oculis oblata fidelibus."
    
AUTORIN Weber