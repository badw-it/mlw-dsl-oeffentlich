LEMMA scrobis VEL {raro} scrobs

GRAMMATIK 
subst. III; -is f. vel m.

SCHREIBWEISE
    script.:
        scrodi-: {=> scrobis_2C}

FORM
    form.:
        abl. pl.:
            -obus: {=> scrobis_3A}
            -/bus: * POETA SAXO; 3,181 {(G^1)}
        
        decl. I. vel II.:
            {?}-eis: {=> scrobis_5}
            -is: {=> scrobis_4}

BEDEUTUNG fovea, fossa, foramen -- Grube, Graben, Loch; de sepulcro: {=> scrobis_11}:

    UNTER_BEDEUTUNG in univ.: 

        UU_BEDEUTUNG usu communi:

            * WALAHFR. hort.; 71 "quae sicca fere et translata subactis suscepit #scrobi.|bus (#scr.|obus {var. l.}) {@ scrobis_3A}, redivivo plena virore restituit {(sc. areola)} reparans numeroso semina fructu."

            * WANDALB. mens.; 69 "de ... locis steriles primis tum ferre radices arborum et ignotis #scrobi.|bus (#scrod|.ibus {var. l.}) {@ scrobis_2C} deponere suetum {(sc. est)}."

            * VITA Burch. Wirz.; 6 p. 113,7 "postquam tellus egesta est, non alta #scrob.|e {@ scrobis_11} inventa sunt corpora martyrum ita fraglantia, ut {eqs.}"

            * DONIZO Mathild.; 1,619 "coenosa procul a me {(sc. Canossa)} #scro.|bs et aquosa."

            * METELL. exp. Hieros.; 4,533 "tollitur ex atra #scrob.|e Iesu lancea sacra."

            * GESTA Halb.; p. 108,50 (vs.) "scemate pleni sub #scrob.|e ceni proiciuntur {cives}."

                {al.}

                ANHÄNGER fort. add.:

                   * ODO GLANN. Maur.; 34 p. 1059^b "Raimbaldus ... sole perosus et aere ipsaque communi indignus luce <<in restalibus>> (infernalibus {ci.}) #scrob.|eis {@ scrobis_5} divina percussus animadversione consortibus suis applicatus est daemonibus."

        UU_BEDEUTUNG proverb. et in imag.:

            * CARM. var. III; 79,9 "via recta patet, #scrobi.|bus sed plena cavatis extat."

            * BENZO ad Heinr. IV.; 4,30 p. 360,2 (vs.) "semper fraus in #scrob.|em cadit cum suis sequacibus."; 4,36 p. 408,14 (vs.) "heresis latenter fodit #scrob.|es mille mortium."

    UNTER_BEDEUTUNG metalla, fodina -- Bergwerk, Mine:

    * CAPIT. reg. Franc.; 32,62 "ut unusquisque iudex ..., quid ... de ferrariis et #scrob.|is {@ scrobis_4}, id est fossis ferrariciis vel aliis fossis plumbariciis, ... habuerint, ... nobis notum faciant."

    * METELL. Quir.; 1,37 "auri more rudis, rudera quod #scrob.|is aut limus madidis contribulant vadis, lectum diluitur de patriis aquis." 

    * CHRON. Reinh.; a. 1193 p. 551,6 "castris eius {(fratris)} acriter inminebat {marchio}, cui de subterraneis #scrobi.|bus argentum natura prebebat."

    UNTER_BEDEUTUNG cuniculus -- Tunnel, Gang:
    
    * CHRON. Reinh.; a. 1204| p. 568,8 "civitatenses de subterraneis #scrobi.|bus prodeuntes eorum {(sc. adversariorum)} propugnaculis ... ignem ... apposuerunt."; p. 580,34 "#scrob.|es subterraneas infodit {Otto}."

    UNTER_BEDEUTUNG vulnus, incisura -- Wunde, Vertiefung, Spalte {(c. gen. inhaerentiae: {=> scrobis_100})}:

    * GLOSS. Sangall.; 45 ((ed. B. Bischoff, Anecdota. 1984.; p. 46)) (s. XI.^{med.}) "decostatur Adam, Deus hinc sibi fabricat Ęvam carnis materia fragili lateris #scrob.|e farsa."

    * UNIBOS; 150,1 "illa {(sc. iumenti)} #scro.|bs {@ scrobis_100} alti vulneris invenienti profuit, dum commovit pecuniam inclinus hordeaceam."

BEDEUTUNG spelunca, latebra -- Höhle, Schlupfwinkel: 

    * MIRAC. Trud.; 8 "in supradicto ... pago campania est late patentibus campis solis ardore exusta et nullis humani negotii usibus apta, sed solummodo latronum #scrobi.|bus plena, de quibus {eqs.}"

BEDEUTUNG barathrum, profundum -- Schlund, Abgrund:

    * PETR. DAM. epist.; 86 p. 487,23 "solus ... draco in subterraneae #scrob.|is (#scrob.|is fosse {var. l.}) voraginem mergitur."


AUTORIN Strika