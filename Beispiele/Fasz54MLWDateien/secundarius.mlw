LEMMA secundarius

GRAMMATIK
    adi. I-II; -a, -um


BEDEUTUNG adi.:

    UNTER_BEDEUTUNG secundus, (primum) sequens, posterus -- zweite(r), (nach)folgend, spätere(r):

        UU_BEDEUTUNG in univ.:

            * RUP. TUIT. Herib.; 15,10 "signum crucis ... imitari honore #secundari.|i (#secundari.|o {var. l.}) debent pastores ..., ut vice Christi honorentur, quos vicem constat agere Christi."

            * HIST. de exp. Frid. imp.; p. 47,12 "duobus ... #secundari.|is nuntiis ... per longum tempus ... detentis."

            * CONST. imp. III; 37 "cum primo primarias et secundo #secundari.|as pro Heinrico clerico ... vobis {(sc. decano et capitulo)} ... obtulerimus preces nostras ({postea:} nunc tercio)."

            * ANON. IV mus.; 7 p. 87,4 "omnis punctus trium primus longus ...; omnis punctus #secundari.|us (duplarius  {var. l.}) trium longus ...; omnis ultimus trium {eqs.}"

            {al.}


       UU_BEDEUTUNG renovatus, iteratus -- erneut, wiederholt:

            * TRAD. Teg.; 294^b (a. 1157/63) "huius #secundari.|ę traditionis testes auriculatim adtracti sunt: {eqs.}"

            * TRAD. Salem.; p. 84,8 "donationem #secundari.|am fecit {comes} de omnibus, quę ... ęcclesię ... donasse videbatur."



    UNTER_BEDEUTUNG diversus, contrarius -- kontrastierend, Gegen-:

            * SALIMB. chron.; p. 184,7sqq. "cum ... fecisset {Henricus Pisanus} ... pulchrum atque ad audiendum suavem {(sc. cantum)} ..., frater Vita fecit ibi #secundari.|um cantum, id est contracantum; semper enim, quando inveniebat aliquem fratris Henrici simplicem cantum, libenter ibidem faciebat #secundari.|um cantum ((* {cf.} p. 183,12 "secundum {@ secundus_30} cantum."; p. 384,7 "contracantum"))."



    UNTER_BEDEUTUNG (primo) inferior, subiectus, ordine secundo positus, (primi) proximus, subalternus -- (dem Ersten) unter-, nachgeordnet, untergeben, abhängig, zweitrangig, zweithöchste(r), Unter-, Neben-:

        UU_BEDEUTUNG publ., eccl., iur., canon.:

            * DIPL. Loth. III.; 86 p. 136,5 "#secundari.|os ... advocatos, immo exactores, omnino interdicimus."
        
            * CHART. ord. Teut. (Hass.); 56 p. 56,1 "Crafto miles pro se fideiussit et ... patrem uxoris eius #secundari.|um fideiussorem statuit, quod vulgo nachburge dicitur {eqs.}" {al.}

            * CHART. Lamb. Leod.; 350 p. 429,18 "cum controversia verteretur inter ecclesiam maiorem Leodiensem ex una parte et #secundari.|as {@ secundarius_1b} ecclesias conventuales canonicorum eiusdem civitatis ex altera." 

            * CHART. Xant.; 200 p. 135,5 "non ad altare beati Victoris neque missam summam, sed alias missas #secundari.|as {@ secundarius_3} et in altaribus #secundari.|is tales {(sc. vicarii et simplices)} sacerdotes poterunt celebrare."

            {al.}

                ANHÄNGER spectat ad ordinem feodalem q. d.:
                    
                    * EBERH. BAMB. epist.; 11 p. 525^B "vassallus domino aequalis in possessione est? ... absit! alius enim possessor principalis, alius #secundari.|us est: unus enim suo nomine possidet, alius autem alieno, eius videlicet, qui principalis possessor est."


        UU_BEDEUTUNG mus.:

            * ANON. IV mus.; 1,1| p. 31,13 "iste {(sc. primus tertii imperfecti)} ordo dicitur solus ordo, quia #secundari.|um ordinem non habet, ut modi supradicti, nisi {eqs.}"; p. 31,20.; 7| p. 85,34 "istae sex {(sc. concordantiae organi puri regulares)} dicuntur primae concordantiae ...; #secundari.|ae (duplariae {var. l.}) dicuntur quinque tantum {eqs.}"

            * HIER. MOR. mus.; 15,59 "modorum ... consonancium quidam sunt per se consonantes, que eciam dicuntur primarie consonancie, quidam vero per accidens, que et consonancie dicuntur #secundari.|e ((* {sim.} 15,322))."; 20,123 "tonorum ... alii principales sive autenti, ... alii #secundari.|i sive plagales."


        UU_BEDEUTUNG gramm.:

            * GERHOH. Sim.; p. 254,36 "oratio 'Deus verax est' omnino est integra, quia non caret aliqua parte principaliter constitutiva, licet careat #secundari.|is partibus orationis, coniunctione, prepositione ac ceteris {eqs.}"



        UU_BEDEUTUNG natur. et philos.:

            UUU_BEDEUTUNG in univ.:

                * ALBERT. M. animal.; 1,73 "quod quaedam membrorum suorum {(animalium)} sint principalia et quaedam #secundari.|a principalibus membris deservientia {eqs.} ((* {cf.} 1,74))."; 1,564 "principalis ... operatio intestinorum inferiorum est praeparatio stercoris ad exeundum, et #secundari.|a est ad digerendum {eqs.} ((* {sim.} 1,566.; 3,152))."

            UUU_BEDEUTUNG deductivus, subsidiarius -- mittelbar, 'sekundär':

                * HERM. CARINTH. essent.;  1 p. 86,28 "prima ... et efficiens causa universitatis est ipse ... auctor omnium, Deus; #secundari.|a vero instrumentum eius."
 
                * URSO qual.; 101 "#secundari.|i {(sc. effectus)} sunt, qui #secundario {@ secundarius_4} ab eis {(qualitatibus)} descendunt, ut a caliditate dissolvere ..., a frigiditate condempsare."
    
                * ALBERT. M. eth. II; 4,1,1 p. 272^a,11 "cum principales virtutes ... dicantur cardinales, illae, quae sunt circa exteriora ..., dicuntur adiunctae et #secundari.|ae.";
                * metaph.; 7,4,1| p. 368,80 "ipsa ... natura composita, quae est #secundari.|um hominis significatum, cui {eqs.}"

                {persaepe.}


ABSATZ
BEDEUTUNG subst. masc.:

    UNTER_BEDEUTUNG in bonam partem i. q. adiutor, magister auxiliarius -- Gehilfe, Hilfslehrer:
    
        * CHART. Aschaff.; 8 p. 37,2 (interp. s. XII.^2)  "Alemarus, dicti dydascali #secundari.|us."

    UNTER_BEDEUTUNG in malam partem i. q. administer, complex -- Helfershelfer, Komplize:

        * CONR. EBERB. exord.; 5,10 p. 295,5 "cum ... infirmarius conversorum, qui quasi #secundari.|us (archirusticus {add. KS}) eiusdem {(sc. intestinae discordiae)} conspirationis esse videbatur, ... ei {(sc. auctori conspirationis)} occurrisset {eqs.}"


SUB_LEMMA secundarie (-ę) VEL *secundario

GRAMMATIK
    adv.


BEDEUTUNG iterum, rursus, denuo -- zum zweiten Mal, erneut, nochmals, wieder(um):

        * CHART. archiep. Magd.; 321 p. 416,7 (a. 1166) "quo {(Gerhardo praeposito)} defuncto ... tradicio ... #secundari.|o facta est a prenominato Ottone preposito."

        * EPIST. Hildeg.; 156,15 "eadem fidei devotione vos #secundari.|o rogo {(sc. abbatissa)}, ut {eqs.}"

        * GERLAC. annal.; a. 1182 p. 693,46 "eodem anno Stragoviensis ęcclesia #secundari.|e dedicata est {eqs.}"

        * ADILB. AUGUST. Simp.; 12 "gens Hunnorum ... ecclesiam beatae Afrae #secundari.|e exussit."
        {saepius.}


BEDEUTUNG loco primi, ad substitutionem -- anstelle des Ersten, als Alternative, Ersatz:

        * EPIST. Bamb.; 29 p. 524,20 (a. 1135) "tres ... alias personas {(sc. eligendas)} eiusdem eclesie #secundari.|o designantes {(sc. canonici)}, ut, si huius {(sc. Martini)} canonice reiceretur, unam istarum obtinerent."


BEDEUTUNG secundo -- zweitens, in zweiter Linie; theol. spectat ad interpr. alleg.: {=> secundarius_2}:

    UNTER_BEDEUTUNG in univ.:

        * GERHOH. psalm.; 81 p. 678,26 "principaliter quidem psalmus hic visibiles inimicos Christi verberat Iudaeos, #secundari.|e {@ secundarius_2} autem invisibiles militis Christi hostes, spiritus malignos."

        * CHART. Brixin.; 65 p. 70,13 (a. 1221) "predecessor noster {(sc. episcopi)} ... principaliter divini premii amore inductus, #secundari.|o vero ... Wintheri ... hortatu succensus {eqs.}"

        * HIER. MOR. mus.; 24,26 "in componendis cantibus hoc primum musicus facere debet, ut {eqs.}; #secundari.|o necessarium est cantum componenti, quod {eqs.}; tercio {eqs.}"


    UNTER_BEDEUTUNG tum -- darauf, danach:

        * CHART. Basil. A; I 360 p. 537,14 (a. 1234) "fures ... inventi et detenti primo reddantur episcopo, #secundari.|o {@ secundarus_6} comiti."




BEDEUTUNG ordine inferiore, modo subiecto -- in untergeordneter Stellung, Weise, in abhängiger Weise:

    UNTER_BEDEUTUNG publ. et iur. {(spectat ad ordinem feodalem q. d.)}:

        * CHART. Naumb. I; 277 p. 261,2 (a. 1165/70) "que {(bona)} Erchenbertus ..., fidelis noster {(sc. episcopi)}, de nostra manu, #secundari.|o vero Petrus de Guderin ab eo in beneficio possedit."
        * CHART. Stir.; IV 392 p. 235,41 "homaii seu dominii iure in summa redditum eorundem {(sc. decem marcarum)} semper Seccoviensis episcopis principaliter et meis {(sc. Gysilae relictae)} heredibus #secundari.|o reservato."

        * CHART. Bern.; III 293 "V scoposas ..., quas #secundari.|o possederam a sorore mea Bertha, ... contuli."


    UNTER_BEDEUTUNG eccl.:
        
        * TRACT. Ebor.; f. 159 p. 156,17 "summi et cęlestis imperatoris et secundi terrenique una eademque potestas est, sed cęlestis principaliter, terreni #secundari.|ę."


    UNTER_BEDEUTUNG natur. et philos. i. q. modo deductivo, subsidiario -- in mittelbarer, 'sekundärer' Weise:

        * HUGO HONAUG. hom.; 1,33,4 "tercius modus {(sc. subsistendi)} oritur ex primo principaliter, ex secundo #secundari.|o; non enim {eqs.}"

        * MAURUS urin. I; p. 17,23 "urina subrubea vel rubea mediocriter tenuis causonidem significat, quae fit in principio ex colera, #secundari.|o ex sanguine (({sim.} p. 17,28))."

        * ALBERT. M. veget.; 1,91 "hoc {(semen formatum in organa animae)} est semen feminae #secundari.|o et semen masculi principaliter {eqs.}"; 
        * metaph.; 7,3,5 p. 361,10 "perpendi potest, quod illius, quod est universale et quid absolutum, est diffinitio, particularis autem per se nec nomen est nec diffinitio nisi aequivoce et #secundari.|o." {ibid. al. v. et {=> secundarius_4}.}



AUTORIN Niederer