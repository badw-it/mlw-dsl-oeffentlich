LEMMA scriptio (-cio)

GRAMMATIK 
    subst. III; -onis f.

SCHREIBWEISE
    script.:
        -iti(o): * CHART. Sangall. A; 462 {(cod.)}


BEDEUTUNG actio scribendi -- das (Auf-)Schreiben:

    UNTER_BEDEUTUNG exscriptio, transcriptio -- das Abschreiben, Kopieren:

        * VITA Willeh.; 7 p. 845^A "cum lectioni et orationi continue operam daret, tum praecipue #scription.|i interdiu deditus erat; namque scripsit ... epistolas beati Pauli {eqs.}"


    UNTER_BEDEUTUNG subscriptio, subsignatio -- Unterschrift, Unterzeichnung; de nominibus vel signis subscriptis: {=> scriptio_3a; scriptio_3b}:

        * FORM. Andec.; 47 "cuius {(sc. hominum)} nomina vel #scrip.|cionibus {@ scriptio_3b} adque signaculum subter teniuntur inserta."

        * LEG. Wisig.; 2,5,16 "in quibus {(scripturis)} testatoris suscriptio (subscriptio, #scripti.|o {@ scriptio_3a} {var. l.}) repperitur ...; testes idonei ... scripturam sua denuo suscriptione (subscriptione, #scription.|e {var. l.}) confirment ((* {sim.} 2,5,17))."

        * STEPH. Wilfr.; 47 p. 241,15 "ut ... per #scription.|em (#scription.|i {var. l.}) propriae manus confirmes eorum {(sc. episcoporum et al.)} ... iudicium."

        * CAPIT. reg. Franc.; 197,8 p. 55,32 "ut unusquisque episcoporum, qualiter haec res {(sc. confessio Ludowici imperatoris)} acta fuerit, in propriis cartulis insereret eamque sua #scription.|e roboraret."


    UNTER_BEDEUTUNG actio litteris mandandi, conscriptio -- das Verfassen, Abfassung:

        * EINH. Karol.; prol.| p. 1,8 "ut nova #scription.|e non offendantur, qui vetera ... monumenta fastidiuntur ((* {sim.} p. 1,18 "ab huiuscemodi #scription.|e (discriptione, descriptione {var. l.}) non existimavi temperandum"))."


        * GUNDR. ad Ermenr.; ((MGScript. XV; p. 154,38)) "ut ... uterque cumulum sudoris recipiat in futuro, ego petitionis et tu #scription.|is."



ABSATZ
BEDEUTUNG nota(e), littera(e) scripta(e), verbum, verba -- Schrift(zeichen, -zug), Buchstaben, Wort(e), Wortlaut:

    UNTER_BEDEUTUNG gener.:
    
        UU_BEDEUTUNG in univ.:

            * OTFR. ad Liutb.; 77 "patitur {(sc. lingua Theodisca)} ... metaplasmi figuram nimium ..., quam doctores grammaticae artis vocant sinalipham, ... literas interdum #scription.|e servantes, interdum vero ... vitantes."


        UU_BEDEUTUNG de notis magicis i. q. signum, character -- Zeichen, Symbol:

            * CAPIT. Bened.; 2,72 "ut clerici vel laici filacteria vel falsas inscriptiones (#scription.|es {var. l.}) aut ligaturas, quae inprudentes pro febribus ... adiuvare putant, nullo modo ab illis vel a quoquam christiano fiant, quia magicae artis insignia sunt."

        UU_BEDEUTUNG inscriptio, titulus -- Auf-, Inschrift:

            * CARM. de Iuda; 2,421 "causa suo {(sc. Christi)} capiti regali proxima liti ponitur apicta; sic est ea #scripti.|o dicta: 'hic Iudeorum rex et salvator eorum'."


        UU_BEDEUTUNG pars textus -- Textabschnitt:

            * COD. Karol.; 70 p. 600,31 "Petrus ... coepiscopus ... obtulit nobis pseudopittatium ... habens in supera #scription.|e adbreviarium Caledonensis concilii."


    
    UNTER_BEDEUTUNG techn.; de encausto: {=> scriptio_4}:

        * COMPOS. Luc.; N 13 "#scripti.|o similis auri {eqs.} ((* {item} MAPPAE CLAVIC.; 46. * COMPOS. Matr.; 22,1))."

        * MAPPAE CLAVIC.; 47 "auri alia #scripti.|o sine auro: {eqs.}"; 60 "ad auri #scription.|em adicies elenusiam."

        * COMPOS. Matr.; 1,6 "scribe ...; cum siccaverit ipsa #scripti.|o {@ scriptio_4}, poli cum emathite lapide."

        {al.}


ABSATZ
BEDEUTUNG scriptum, scriptura, textus, liber -- das Schreiben, Schrift(stück, -werk), Text, Buch:

    UNTER_BEDEUTUNG gener.:

        UU_BEDEUTUNG in univ.:

                * OTFR. ad Liutb.; 80 "non quo series #cription.|is huius metrica sit subtilitate constricta, sed schema omoeoteleuton assidue quaerit."

                * HUCBALD. Rictr.; praef. p. 94,16 ((ed. Levison)) "cuius {(nativitatis Christi)} agebatur annus nongentesimus septimus ... anno huius editae #scription.|is."

                * CONR. HIRS. dial.; 495 "in quibus {(fabulis ad morum finem relatis)} auctor #scription.|um suarum sensum et fructum voluit demonstrare."

                {al.}


                    ANHÄNGER iunctura "caelestis #scripti.|o" de libro vitae q. d. ((* {spectat ad} $VULG.; Phil. 4,3.; apoc. 3,5. {al.})):

                        * WALAHFR. Mamm.; 7,13 "mihi restat adhuc dubitatio nominis huius: ... sit quodcumque, tamen caelestis #scripti.|o servat, quodcqumque exprimimus, sanctum signamus eundem."


        UU_BEDEUTUNG expressius de scriptura sacra ('die Schrift'): 

                * LIOS MON.; 536 "ut ... accidat ... vobis {(sc. hominibus)}, alibi quod #scripti.|o fatur: {'eqs.'}"
        
                * SALOM. III. carm. I; 1,2,204 "exemplum nobis authentica #scripti.|o ponit monstrans {eqs.} {(v. notam ed.)}."; 2^a,6 "gaudenti congaudere et cum flente dolere #scripti.|o, quam dictat Tarsigena, imperitat."

                * CARM. var. III A; 5,30 p. 603 "hoc {(sc. Christum virum dici)} ... apostolici testatur #scripti.|o verbi, uni quippe viro qui vult ... Christo dispondere animas."


            
        UU_BEDEUTUNG epistula -- Brief:

            * HIST. de exp. Frid. imp.; p. 51,5 "<<ipso ... sermone>> (ipsa ... #scription.|e {PP^1}) vel responso suo ... imperator  Constantinopolitanum imperatorem aliquantum correxit, ut {eqs.}"



        UU_BEDEUTUNG descriptio, expositio -- Beschreibung, Darlegung:

                * AMALAR. ad Petr.; 7 "non me praefero magistrum de hac #scription.|e, sed discipulum inquirentem, quod diligo scire."; off.; 3,39,1 "quamvis ordo rerum teneat post #scription.|em nativitatis sancti Iohannis nativitatem Christi scribere."; 4,3,20 "cur responsorius dicatur {(sc. liber)}, in #scription.|e missae dictum est ((* {sim.} 4,42,10))."


    UNTER_BEDEUTUNG publ. et iur.:

        UU_BEDEUTUNG ascriptio (in tabulam publicam facta) -- Eintrag (im öffentlichen Register) {(spectat ad tabulam condemnatorum)}:

                * ALBERT. M. pol.; 6,6^{a}| p. 608^a,4 "septimus {(principatus)} ..., qui est circa actiones condemnatorum et #scription.|es ((* ARIST.; p. 1322^a,1 "ἐγγραφάς")) et circa custodias corporum et huiusmodi ad condemnatos pertinentium ((* {cf.} p. 609^b,41 "secundum inscriptiones"))."


        UU_BEDEUTUNG syngrapha -- Schuldverschreibung, Schuldschein {(spectat ad impignorationem)}:

                * ACTA civ. Wism. B; 829 (a. 1279) "quod nulli in civitate nostra teneatur tempore huius #scription.|is {@ scriptio_2a} Ekkebertus ((* {sim.} 837\* "Tidemannus non habuit annos discretionis tempore huius #scription.|is {@ scriptio_2b}"))."; 1119\* "super defectu istius {(sc. ad impignorationem pertinentis)} #scription.|is {@ scriptio_2c} promiserunt Theodericus et Gerardus et Walterus ..., quod excipient eos {(viros)} sine dampno."


        UU_BEDEUTUNG littera obligatoria -- Verpflichtung(sschreiben) {(spectat ad obligationem accusatoris recompensandi accusatum de sumptibus)}:

                * FORM. extrav.; I 4^{tit.} "#scription.|e {@ scriptio_1} de <sumptibus> litis et expensis (({postea:} "inscriptione")) ((* {cf.} FORM. Turon.; 29))."


        UU_BEDEUTUNG libellus, delatio -- Anklage(schrift), Anzeige:

                * LEX Raet. Cur.; 9,27 "ego {(sc. accusator)} #scrip.|cionem feci, nec illa causa, qui super eum {(sc. accusatum)} dixi, probare non possum."



        UU_BEDEUTUNG perscriptio (negotii, conventi), instrumentum, charta -- schriftliche Aufzeichnung (eines Rechtsgeschäfts, einer Übereinkunft), (Rechts-)Dokument, Urkunde:

            UUU_BEDEUTUNG in univ.:

                * TRAD. Ratisb.; 5 (a. 778) "ego Taugolfus cum #scription.|e firmavi ((* {cf.} 1 "ego Taugolf presbyter scribsi hanc traditionis causam {(v. comm. ed.)}"))."

                * TRAD. Weiss.; 156 l. 26 "huic facto #scrip.|cionis vigorem adhibere censuimus {(sc. abbas)}."

                * CONST. imp.; I 13,1 "si ... utraque pars sive altera cartis seu #scriptioni.|bus (inscriptionibus {var. l.}) praedium sibi vendicare voluerit."

                * DIPL. Otton. I.; 245^a p. 350,5 "ut ... precepta ac legales #scription.|es ... confirmaremus."

                * CONST. Melf.; 1,73,1^B p. 244,29 "ut ... baiulus, iudex atque notarius <<pro #scriptioni.|bus>> (praescriptionibus {var. l.}) et subscriptionibus consequantur (({sim.} 1,75 p. 248,26))."
        
                {persaepe.}


        UUU_BEDEUTUNG charta traditionis, traditio bonorum (per scriptum facta) -- Besitzüberschreibungs-, Besitzübertragungsurkunde, (schriftliche) Besitzübertragung:

            * CHART. Sangall. A; 127 p. 119,23 (a. 790) "ut ... ad monasterium ... per #scription.|is {!titulum} aliquid conferre deberemus ((* CAPIT. reg. Franc.; 105,9 p. 217,27 "si quis praepositus ... res ... per aliquem #scription.|is t|itulum cuiquam concesserit." * DIPL. Conr. III.; 68 p. 119,43. {al.}))."

            * IOH. VEN. chron.; p. 163,13 "pallium ..., quod pro pacti federe a Veneticis  ... persolvebatur, ... duci perpetua #scription.|e donabat {caesar}."
                                   
            * DIPL. Otton. III.; 300 p. 725,28 "per precepta vel mundiburdia vel aliqua legalium #scription.|um munimina."

            * DIPL. Heinr. II.; 24 p. 27,27 "ea, quae ... Otto ... sibi {(episcopo)} ... preceptali #scription.|e possidenda concessit."
                    
            {al.}

                ANHÄNGER per confusionem i. q. possessio per scriptum, chartam acquisita -- mittels einer schriftlichen Übertragung erworbenes Gut {(usu meton.)}:

                    DIPL. Otton. I.; 248 p. 356,26 "ut tam libellarii et precarii quamque in prędictis libellariis atque #scriptioni.|bus commorantes et in omnibus ... rebus ... residentes ... lites ... ante eundem {(episcopum)} ... definiant ({antea:} de libellariis, precariis, commutationibus alisque rebus aliquo #scription.|is titulo adquisitis molestare)."


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 60