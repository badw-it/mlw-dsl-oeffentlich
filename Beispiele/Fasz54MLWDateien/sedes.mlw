LEMMA sedes

GRAMMATIK subst. III; -is f.; m.: {=> sedis_02}{=> sedis_03}{=> sedis_04}{. al.}

SCHREIBWEISE
    script.:
        sae-: {=> sedis_01} *{adde} ANNAL. Altah.; a. 1044 p. 37,18 {(cod.)}
        sęd(es): {=> sedis_09}
        sid(es): {=> sedis_08}{=> sedis_07}

FORM
    form.:
        nom. sg.:
            -is: {=> sedis_19} {adde} * TRAD. Frising.; 341. {saepius.}

METRIK
    metr.:
        {?}sĕd-: * EPITAPH. var. II; 109,3


BEDEUTUNG  quo sedetur, sedile, sessibulum, scamnum, subsellium, sella, cathedra -- Sitz(platz), Bank, Schemel, Sessel, Stuhl:

    UNTER_BEDEUTUNG proprie:

        UU_BEDEUTUNG strictius:

            UUU_BEDEUTUNG in univ.:

                * FORMA mon. Sangall.; 18,1 "infra #sed.|es scribentium, supra bibliotheca."; 24,4 "infra refectorium, ... #sed.|es in circuitu."
                * LEX fam. Worm.; 17 "si quis in placito iniustum clamorem fecerit aut iratus de sua #sed.|e recesserit."
                * THEOPH. sched.; 3,2^capit. "de #sed.|e operantium."
                * VITA Norb. II; 28 "ad reficiendum ... nullam #sed.|em praeparare, non mensam apponere sibi vir sanctus sinebat; terra #sed.|es et genua mensa illi erant."
                * CHART. Rhen. med.; II app. 15 p. 426,26 "curtis scampna et sedilia et #sed.|es parare debet {villicus}, in quibus ad placitum venientes sedeant." {saepe.}

            ANHÄNGER de parte heredidatis viduae non attributa ((*{de re v.} Dt. Rechtswb. IV.; p. 255sq. s. v. 'Gerade')):

                * CHART. Hall.; 224,30 (a. 1235) "domina, que recipit rade, procurabit lectum et mensam et #sed.|em illius, qui spectat ad heredidatem, secundum quod decet."

            UUU_BEDEUTUNG solium, thronus -- erhabener Sitz, Thron: 

                UUUU_BEDEUTUNG in univ.:

                    * WALTH. SPIR. Christoph. II; 3,102 "de culmine #sed.|is corruit {rex}."
                    * ORD. coron. imp.; 14,2 "ibi {(sc. ante portas)} ... sedet dominus papa in #sed.|e sua."
                    * ARNOLD. LUB. chron.; 4,8 p. 171,24 "regina ... dedit domno imperatori ... #sed.|em ... eburneam."
                    * CHART. Salisb. II; 740^a p. 265,33 "tibi {(sc. praeposito)} ... concedimus {(sc. archiepiscopus)} ..., ut plicatas #sed.|es ..., quemadmodum ritus est episcopis, ... in perpetuum habeatis." {saepius.}

                ANHÄNGER alleg. de throno Dei, luciferi:

                    * HRABAN. univ.; 1,1 p. 18^C "#sed.|es Domini, id est ... angeli vel sancti, quod in his Dominus sedeat."
                    * HRABAN. carm.; 13,35 "ille {(sc. lucifer)} tumens cecidit celsa de #sed.|e superbus."
            
                UUUU_BEDEUTUNG spectat ad iudices apocalypsis ((*{loci spectant ad} VULG. apoc.; 4,4)):

                    * RIMB. Anscar.; 3 p. 22,34 "viginti et quattuor seniores ... in #sedi.|bus sedentes."; 42 p. 77,35 "qui in sublimi illa cum apostolis iudicum #sed.|e in regeneratione sessurus."

                UUUU_BEDEUTUNG iunctura "in #sed.|e iudiciaria sedere" sim. i. q. iudicium exercere, facere, ius dicere -- Gericht halten, Recht sprechen:

                    * CHART. march. Misn.; II 113 (a. 1136) "cum in iudiciaria #sed.|e Wirziburc resideret {imperator}."
                    * CHART. Basil. C; I 55 p. 40,13 "si episcopus et advocatus simul sederint in #sed.|e iudiciaria."
                    * CHART. Sil. D; I 321 p. 237,33 "tempore placiti quicumque scabinum in #sed.|e iudiciaria sedentem in querimoniam ponere presumpserit."
            
            UUU_BEDEUTUNG de scamnis in choro ecclesiae positis:

                * CATAL. thes. Germ.; 136^f (c. 1105) "matricularii debent ... quatuor #sed.|es, que sunt in choro, apportare extra chorum {eqs.}"
                * ANNAL. Aquic.; a. 1208 ((MGScript. XVI; p. 505,26)) "qui {(abbas)} ... omnes veteres officinas deposuit et ... magnas edificationes et chori #sed.|es incomparabiles construxit."
                * CHART. Brixin.; 166 p. 177,15 "Marquardus cancellos nimis adiunctos altari sancte Marie elargavit pulpitos ibi faciens et fratrum ibidem Deo serviencium #sed.|es sive staciones ... construxit." {al.}

            ANHÄNGER iunctura "fratres #sed.|ium" ('Stuhlbrüder')((*{de re v.} Dt. Rechtswb. XIV.; p. 596)):

                * CHART. episc. Spir.; 368 (a. 1273) "sorores ... inter #sed.|ium fratres ad comperacionem decimę ... viginti quatuor libras hallencium contulerunt."

            UUU_BEDEUTUNG de scamnis latrinae:

                * WILH. HIRS. const.; 2,20 p. 94,14 "si quid forte in claustro neglectum offendit; perspicit {prior} quoque omnes necessariarum #sed.|es."
                * STATUT. ord. Teut.; p. 63,18 "in locis nature necessariis ante #sed.|es et in #sedi.|bus districtum a fratribus silencium semper teneatur."
                * CHRIST. cal. Mog.; 7 ((MGScript. XXV; p. 242,35)) "unus sedens in #sed.|e nature, ut purgaret ventrem, omnia intestina in cloacam eiecit {eqs.}"

            UUU_BEDEUTUNG de sedili cruciario, ostentui sceleratorum destinato:

                * CHART. Livon. A; 77,29 (a. 1225) "dabit {(sc. bigamus convictus)} X marcas argenti urbi vel praecipitabitur de #sed.|e scuppestol ((*{sim.} CHART. Brand. A; XXIII 3 p. 4,26 "qui falsa et nequam emptione seu vendicione promeruerit sedere in #sed.|e, que dicitur scupstol {eqs.}")) ((*{de re v.} Dt. Rechtswb. XII.; p. 1448 s. v. 'Schupfstuhl'))." 
                * ALBERT. STAD. chron.; a. 1196 "quem {(Iordanum)} imperator captum in #sed.|em ferream candentem posuit."

            

        UU_BEDEUTUNG latius:

            UUU_BEDEUTUNG de sedili avibus apto:

                * CARM. Cantabr. A; 23,1,3 "canunt de celsis #sedi.|bus palumbes carmina cunctis."
                * FRID. II. IMP. art. ven.; 2 p. 174,22sq. "cum naturaliter esse consueverit {falco} et stare super firmiores #sed.|es, oportet, ut dicamus de #sedi.|bus, super quas ponendus est ad standum." {al.}

            ANHÄNGER in imag.:

                * BERTH. WERD. narr.; 6 p. 770,37 "episcopus ... relatae sibi visioni non credens, ... levitatem ... fidei ... irridens vulgari ... sermone respondit ...: 'si ... cum te haec vidisse contigit, #sed.|em galli tenuisses, inhonestae ruinae fuisses'."

            UUU_BEDEUTUNG de scamnis, mensis ad vendendum merces aptis:

                * DIPL. Conr. III.; 18 p. 32,24 "sunt ... in Wormacia quatuor curtes et addictę vendendis panibus X #sędes {@ sedis_09}." 
                * CHART. Worm.; 368 p. 235,33 "pauperes ... census ... pro subpignore obligarunt, videlicet libram denariorum Wormaciensium, de #sed.|e panum in inferiori foro."

            UUU_BEDEUTUNG de machina textoria:

                * CHART. select. Keutgen; 278,9 (c. 1280) "ne ... misceantur inter aliquos pannos qualescumque ... crines cadentes sub #sed.|es, ubi textuntur panni."

ABSATZ

    UNTER_BEDEUTUNG meton.:

        UU_BEDEUTUNG de personis eorumque consortio functionem iudicandi, regendi, administrandi exercentibus; c. gen. explicativo: {=> sedis_18}:             

            UUU_BEDEUTUNG spectat ad iudices:

                UUUU_BEDEUTUNG in univ.:

                    * CHART. Rhen. inf.; I 336 (a. 1139) "famuli ęcclesię non cogantur advocati sive prefecti iudiciariam #sed.|em {@ sedis_18} adire, sed tantum abbatis ... iussis obtemperent."
                    * CHART. Westph.; VII 1353 "ut ... Albero ad prosequendum ius suum ante #sed.|em liberam et ante liberum comitem ... secure venire posset ((*{de re v.} Dt. Rechtswb. III.; p. 828 s. v. 'Freistuhl'))."
                    * CHART. Rapp.; 126 p. 110,4 "cum auctoritate ... langravii Alsatie presideremus {(sc. advocatus)} #sed.|i iusticiarie provincialis placiti." {al.}
                    

                UUUU_BEDEUTUNG spectat ad iudices apocalypsis ((*{spectat ad} VULG. apoc.; 4,4)):

                    * OTTO FRISING. chron.; 8,17 p. 414,22sqq. "'quia illic sederunt #sed.|es in iudicio', sedentne #sed.|es? quae sunt istae #sed.|es? ... non possunt sedere nisi vivae #sed.|es {eqs.}"

            
            UUU_BEDEUTUNG spectat ad regem, imperatorem sim.:

                * DIPL. Otton. II.; 130 p. 147,9 "omnia, quę egit {comes} circa #sed.|em imperii et honoris nostri, ab eo repellimus."
                * RANGER. Anselm.; 2523 "quando minor #sed.|es {(i. imperator)} maiorem {(i. papam)} dampnat?"

            UUU_BEDEUTUNG spectat ad (archi)episcopum, papam sim.:

                UUUU_BEDEUTUNG in univ.:

                    * LEX Baiuv.; 1,9 "si quis presbytero ..., quem ecclesiastica #sed.|es probatum habet, iniuriam fecerit."
                    * THANGM. Bernw.; 13 p. 764,28 "domnam Sophiam, si ad Hildenesheimensem #sed.|em ... oboedientiam profiteretur, interrogavit {eqs.}"
                    {al.}

                UUUU_BEDEUTUNG iunctura "#sed.|es apostolica, Romana"; 'Heiliger Stuhl':

                    * WILLIB. Bonif.; 5 p. 22,1 "apostolicae #sed.|is ... litteris ((*6 p. 30,16. {al.}))."; 8 p. 41,13 "Romanae ... sedis legatus ((* DIPL. Loth. III.; 10. {saepe}))."
                    * FORM. Sal. Merk.; 49 p. 259,9 "domino sancto et apostolico #sed.|e {@ sedis_03} colendo."
                    * ANNAL. Fuld. II; a. 746 "Bonifacius ... cum auctoritate #sed.|is apostolicae ... duas #sed.|es episcopales constituit."
                    * GERB. epist.; 14 p. 36,15 "#sed.|em apostolicam si {!appello} ((* CHART. Tirol. notar.; II 447. {al.}))."
                    * BERTH. chron. B; a. 1077 p. 303,9 "contemptor Dei et apostolice #sed.|is."
                    * OTTO FRISING. gest.; 1,69 p. 98,5 "a Romana #sed.|e ratihabitionem optinuit." {persaepe.}

         

        UU_BEDEUTUNG de dignitate, munere, officio, potestate, regimine sim.:

            
            UUU_BEDEUTUNG spectat ad iudices sim.:

                UUUU_BEDEUTUNG in univ.:

                    * MEGINH. FULD. Alex.; 6 p. 431,6 "qui {(inimici cuiusdam viri)} #sed.|em legislatorum possidebant {eqs.}"
                    * CHART. Friburg.; 81 "presentibus nobis {(sc. plebano)} in #sed.|e iudiciaria in ecclesia Sancti Martini."
                    * CHART. episc. Misn.; 165 p. 139,22 "ut #sed.|es synodales in civitatibus ... tam archidiaconi quam archipresbyteri exerceant." {al.}

                
                UUUU_BEDEUTUNG spectat ad Christum:

                    * RUP. TUIT. vict.; 13,14 p. 418,32 "quid est illud {(sc. consummatio victoriae Verbi Dei)}? profecto destructio illius iniqui et iudiciaria #sed.|es sive sessio ... Christi."

                                

            UUU_BEDEUTUNG spectat ad regem, imperatorem sim.:

                * ANNAL. Mett.; a. 718 p. 25,29 "Carolus ... #sed.|em ... sibi {(duci)} regalem sub sua ditione concessit."
                * ANNAL. Sangall. Bal.; a. 801 "hoc anno domnus rex imperialem #sed.|em accepit."
                * REGINO chron.; a. 838 "<<in imperiali #sed.|e>> (in #sed.|em imperialem {A}) restituitur {Ludowicus}."
                * WIPO gest.; 6 p. 28,29 "post Caroli Magni tempora aliquem regali #sed.|e digniorem non vixisse."
                * HIST. Welf.; 28 "rex ... Friderico fratrueli suo #sed.|em regni reliquit." {saepius.}

            ANHÄNGER spectat ad diabolum; per compar.: {=> sedis_13}:
                
                * CONST. imp. II; 235 p. 324,3 (a. 1241) "post domitam ... primogeniti filii nostri dementiam, qui contra nos sibi #sed.|em {@ sedis_13} assumpserat aquilonis ((*{sim.} ACTA imp. Winkelm.; I 725 p. 571,14 "quoniam ille aquilonarius princeps sathanas ... non tantum presumpsisse videtur, qui deitati solis suam ex obposito #sed.|em voluit adequare (({de re v. R. M. Kloos, DtArch. 13. 1957.; p. 166sqq.}))"))."

            UUU_BEDEUTUNG spectat ad (archi)episcopum, papam sim.:

                * TRAD. Frising.; 522 p. 448,8 (a. 825) "successor meus ..., qui ... ordinatur in ipsum #sed.|em {@ sedis_04} episcopalem Frigisiensis."
                * POETA SAXO; 3,512 "pontificem ... vicem Petri ... #sed.|em ... tenentem suscepit {eqs.}"
                * DIPL. Otton. I.; 271 "patriarcha vel qui pro tempore in ... #sed.|e ordinati fuerint."
                * RUOTG. Brun.; 38 p. 40,10 "Ratherus ... cum ... ab honore proprię #sed.|is esset expulsus."
                * VITA Mathild. II; 2 p. 150,5 "que {(abbatissa Mathild)} ... #sed.|em possedit abbatiae."
                * BERTH. chron. A; a. 1061 p. 191,20 "episcopus de Luca ... apostolicam #sed.|em sibi usurpavit."
                * ALEX. MIN. apoc.; 4 p. 53,18 "qui {(XXIV sacerdotes cardinales)} ... #sed.|es, id est titulos, habent in urbe {eqs.}" {persaepe.}


            ANHÄNGER de tempore regiminis:

                * CATAL. thes. Germ.; 82,2 "anno ab incarnatione Domini ... DCCCLXX indictione III. et a. XIII. #sed.|is domni Adventii."
                * ANNAL. Engol.; ((MGScript. XVI; p. 485,26)) "a passione ... Christi usque ad #sed.|em ... Marcellini papae sunt anni CCLXXVI."
                * ANNAL. Rod.; a. 1104 p. 691,24 "anno ... #sed.|is ... Obberti ... episcopi XIII^{o}." {al.}

            UUU_BEDEUTUNG spectat ad animalia in fabula:

                * ECBASIS capt.; 639 "'horas cantate, gentacula parta vorate deque cibo celeres proprias cognoscite {(sc. conventus)} #sed.|es; non strepitus vocis nec sit mutatio #sed.|is'."



        UU_BEDEUTUNG de districtu administrativo, paroechia; 'Verwaltungseinheit':


            UUU_BEDEUTUNG spectat ad (archi)episcopatum (papalem) sim.:

                * BONIF. epist.; 109 p. 236,7 "ut non sit episcopalis #sed.|is {@ sedis_19} subiecta Romano pontifici predicans gentem Fresorum."
                * HUGEB. Willib.; 5 p. 103,6 "{!apostolice} #sed.|is pontifex Gregorius III^us ((* LIUTG. Greg.; 10. {saepe}))."
                
                * CONC. Karol. A; 34 p. 249,3 "Narbonensis #saedis {@ sedis_01} archiepiscopus."
                * COZROH. praef.; p. 1,5 "Hitto episcopus ... in cathedram episcopalem Frigisiensi #sed.|is {@ sedis_02} honorifice provectus."
                * RUOTG. Brun.; 21 p. 22,27 "diversis congregationibus ad ... honorabilem #sed.|em pertinentibus."
                * CHART. Westph.; IV 198 p. 130,4 "cum ... sex #sed.|es archidiaconales per episcopatum Paderburnensem ... distinxissemus {(sc. visitatores)}."
                * CONST. imp. II; 400,6 p. 511,11 "liquet ... undecim ... archiepiscopales et multas episcopales #sed.|es ... vacare." {persaepe.}


            UUU_BEDEUTUNG spectat ad abbatiam:

                * THIETM. chron.; 2,42 "abbaciam mundiburdio ... Bremensis archiepiscopi subdit {comes}; sed prepositae huic #sed.|i ... venerabiles matrones ... obiere."



        UU_BEDEUTUNG de loco (administrativo) principali (regni sim.) aedificiisque eius:

            UUU_BEDEUTUNG spectat ad regnum, imperium sim.:

                * ANNAL. Sangall. Bal.; a. 768 "reges Karlus et Karlomannus benedictionem regalem acceperunt, ... Karolus in Noviomaco civitate et Carlomannus in Suessiones civitate in #sed.|e patris sui."
                * NITH. hist.; 4,1 p. 40,3 "quod {(Aquis palatium)} tunc #sed.|es prima Frantiae erat."
                * CHART. Rhen. med.; I 95 p. 100,18 "actum villa publica #sed.|is nostrę {(sc. principis Brittonum)} Bedulcampo."
                * REGINO chron.; a. 869 p. 98,13 "Aquisgrani palatium ingressus est {Carolus}, eo quod #sed.|es regni esse videretur."; a. 876 p. 111,20 "apud Franconofurt principalem #sed.|em orientalis regni residebat {Ludowicus}."
                * ADAM gest.; 4,5 "civitas ... maxima Roscald, #sed.|es regia Danorum."
                * RAHEW. gest.; 3,50 p. 226,24 "princeps Romanus ... aput Modoicum, #sed.|em regni Italici, coronatur." {persaepe.}

            


            UUU_BEDEUTUNG spectat ad (archi)episcopatum sim.:

                UUUU_BEDEUTUNG in univ.:

                    * HUGEB. Willib.; 4 p. 102,12 "venerunt ad urbem, que vocatur Neapule; ... ibi est #sed.|is archiepiscopi."
                    * BREV. NOTIT.; p. A 2 "breves noticie de constructione ecclesie sive #sed.|is episcopatus ((*{postea:} p. A 2 "eligere ... locum ad episcopii #sed.|em et ęcclesias construendas"))."
                    * TRAD. Patav.; 63 "actum est ... ad ipsum #sed.|em {@ sedis_05} sancti Stephani."
                    * THIETM. chron.; 7,26 "corpus suum {(sc. archiepiscopi)} ad #sed.|em propriam delatum."
                    * NARR. itin. nav.; p. 196,15 "Terraconia ..., in qua #sed.|es est archiepiscopalis magne dignitatis."
                    * OTTO FRISING. gest.; 1,64 p. 90,32 "quae {(Iuvavia)} nunc Salzeburga dicta Baioariae metropolitana #sed.|es esse noscitur." {saepe.}

                UUUU_BEDEUTUNG de basilica episcopali:

                    * TRAD. Frising.; 125 (a. 789?) "Frigisinga, ubi honor #sed.|is Sanctae Mariae celebratur ((*{sim.} 341 "#sed.|is episcopii Sanctae Mariae." {al.}))."
                    * RAHEW. gest.; 4,15 p. 255,3 "civitas Frisingensis ... conflagravit adeo, quod, ut taceam de maioribus aecclesiis ... #sed.|e_que ipsa et palatio, nec una quidem de minoribus capellis ... superfuit."
                    

        UU_BEDEUTUNG de statu, tempore nuptiali, matrimoniali, viduitatis; 'Brautstuhl', 'Ehestuhl', 'Witwenstuhl' ((*{de re v.} Dt. Rechtswb. II.; p. 473. 1249)):

            * CHART. Biel.; 4,9 (ante 1214) "in #sed.|e nuptiarum dant sponsus et sponsa mutuo res suas, nisi velint interponere differentiam."
            * CHART. Aquens.; 240 l. 9 "Agnes ... terrena ... despiciens in #sed.|e sue viduitatis transitoria pro eternis cupiens commutare."
            * CHART. Brand. A; XIV 17 p. 12,20 "si pater et mater ... puerum in #sed.|e matrimoniali legitime locaverint."

ABSATZ

BEDEUTUNG ubi sedetur, collocatur, habitaculum, domicilium -- (Aufenthalts-, Stand-)Ort, (An-, Wohn-)Sitz:

    UNTER_BEDEUTUNG spectat ad anim.:

        UU_BEDEUTUNG ad homines:

            UUU_BEDEUTUNG ipsos:

                UUUU_BEDEUTUNG in univ.:

                    * BAUDON. Radeg.; 2,4 ((MGMer. II; p. 381,4)) "despexit #sed.|em patriae ..., elegit exsul fieri."
                    * LEX Alam.; 53,1 "quidquid de #sed.|e paternica secum adtulit {uxor}, omnia in potestate habeat secum auferendi."
                    * RUD. FULD. Alex.; 1 p. 424,2 "promissis ... {!habitandi} #sedi.|bus ((* CONR. MUR. nov. grec.; 8,468))."
                    * VITA Meginr.; 4 "accensus ... erat nimium amore solitariae #sed.|is."
                    * ALBERT. METT. div. temp.; 2,16 "qui {(sc. archiepiscopus)} eius {(Baldrici)} recentes calamitates commiserans #sed.|em in civitate delegavit stipendiumque constituit."
                    * OTTO FRISING. chron.; 2,36 capit. p. 16 "quomodo ... Hannibal ... Romanos devicit in tantum, ut etiam #sed.|es mutare cogitarent." {saepe.}

                ANHÄNGER de domo ipsa; c. gen. inhaerentiae:

                    * ARCHIPOETA; 10,2,2 "supra petram ponere #sed.|em {@ sedis_12} fundamenti ((*{spectat ad} VULG. Luc.; 6,48))."

            
                UUUU_BEDEUTUNG iunctura "#sed.|es paternae" sim. i. q. patria -- Heimat:

                    * CONC. Merov.; p. 164,29 "ne nos seve hiemis procellosa tempestas a #sidibus {@ sedis_08} propriis ... arceat."
                    * POETA SAXO; 1,158 "ad #sed.|es ... cupiens remeare paternas."
                    * CARM. de Frid. I. imp.; 1594 "dux ... Polonicus ... fratre suo genitos ... #sedi.|bus expellit ... paternis." {al. v. et} {=> sedis_33}

                UUUU_BEDEUTUNG statio (militaris), praesidium, castra -- (Militär-)Quartier, Posten, Stützpunkt, (Feld-)Lager:

                    * ANNAL. Lauresh.; a. 797 "fecit {rex Carlus} #sed.|em suam iuxta locum ..., quem Heristelli appellavit, eo, quod ab exercitu suo fuerant constructae ipsae mansiones, ubi habitabant."
                    * ANNAL. Ved.; a. 881 p. 51,9 "Nortmanni ... in Haslao sibi #sed.|em firmant ad hiemandum."
                    * ALBERT. AQUENS. hist.; 2,22 p. 94,29 "dux Lotharingie princeps ... constitutus est; Boemundus, princeps Sicilie, ... vicinam #sed.|em collocavit."; 3,38 p. 198,25 "Tancredus primus secus Altalon #sed.|em ponit."
                    * OTTO SANBLAS. chron.; 15 p. 18,6 "in ea {(sc. Lauda)} #sed.|em belli contra Mediolanenses constituit {imperator}." {al.}

                UUUU_BEDEUTUNG deversorium -- Unterkunft, Logis:

                    * TRAD. Ratisb.; 343 (c. 1020/28) "ut ... puer ... certam ... normalem annonam et #sed.|em obtineret."

                UUUU_BEDEUTUNG mansus -- Gut, Hof:

                    * CHART. Wirt.; 311 p. 10,28 (epist. papae a. 1139) "villam Walda et Eckha ... et quasdam #sed.|es tributarias in Wormatia."
                    * METELL. peripar.; 2,56 "cogens {daemon} verbere strictum templo reddere {(sc. comitem)} vicum, qui glomerans plebes nunc est praetoria #sed.|es."

                UUUU_BEDEUTUNG locus requiei, sepulcrum -- Ruhestätte, Grab:

                    * WALAHFR. Blaithm.; 144 "suis de #sedi.|bus arcam tollentes tumulo terra posuere cavato."; carm.; 77,18,3 "subi {(sc. sanctus Ianuarius)} #sed.|es tibi praeparatas."
                    * CAND. FULD. Eigil. II; 17,43 "rapitur Christi martyr de #sed.|e priore."
                    * GERH. AUGUST. Udalr.; 1,13 l. 9 "ante altare sancte UUaldburge virginis in #sed.|e unius tafi ambos {(sc. fratrem nepotemque)} ... sepelivit."

                UUUU_BEDEUTUNG iuncturae "#sed.|es caelestis, aeterna" sim. de paradiso, caelo:

                    * PIRMIN. scar.; 2 "de illa caeleste #side {@ sedis_07} (#sed.|e {var. l.})."
                    
                    * CAND. FULD. Eigil. II; 6,6 "quem {(Hludowicum)} Deus ... sospem de #sed.|e {@ sedis_33} paterna conlocet in regnum lucis #sed.|em_que beatam."
                    * LIBELL. de conv. Baiuv.; 9 "sidereas conscendit {Liuprammus} #sed.|es."
                    * PONTIF. Rom.-Germ.; 62,5 "ut ... inter angelicos choros eternarum #sed.|ium solio sublimetur {episcopus}."
                    * DIPL. Heinr. III.; 371 "#sed.|e beatitudinis." {saepe.}

        UUU_BEDEUTUNG ad animam:

            * HRABAN. epist.; 57 p. 515,12 "scripsi ... quaedam de diffinitione animae: ... ubi ... #sed.|em habere credatur."
            * CARM. anon. 1,147; ((SBMünch. 1873.; p. 700)) "#sed.|es animarum in stellis esse." {al.}

        UUU_BEDEUTUNG ad partes corporis:

            UUUU_BEDEUTUNG proprie:

                * WALAHFR. Gall.; 2,26 p. 330,9 "oculorum #sed.|es."
                * ALFAN. premn. phys.; 28,25 p. 139 "membra, quae graece dicuntur adenes ..., <<#sed.|es et sustentaculum>> ((* NEMES.; PG 40,716^B "ὄχημα καὶ στήριγμα")) sunt vasorum."
                * CONSTANT. AFRIC. theor.; 3,22 p. 12a^{v} "ossa cartilaginosa, que cordis #sed.|es sunt appellata." {al.}

            UUUU_BEDEUTUNG meton. de natibus, ano:

                * RECEPT. Lauresh.; 2,107 p. 174 "#sed.|is vitiis: medicamentum ad anum, ad ragadia {eqs.}"

        UU_BEDEUTUNG ad animalia:

            * WANDALB. mens.; 65 "apibus #sed.|es ... parandae."

        

        


ABSATZ

    UNTER_BEDEUTUNG spectat ad res i. q. locus (aptus, proprius), punctum, campus, area, positio, basis -- (passender, angestammter) Platz, Ort, Stelle, (Ausgangs-)Punkt, Feld, Position, Basis:

        UU_BEDEUTUNG corpor.:

            UUU_BEDEUTUNG in univ.:

                * NOTKER. BALB. gest.; 2,5 "cum ... de #sed.|e Saxonici belli legatos ad regem Constantinopoleos destinaret."
                * WALTH. SPIR. Christoph. II; 6,260 "quassatam ventis, proreta, carinam tranquilla patrii portus in #sed.|e locato!"

            UUU_BEDEUTUNG de plantis:

            * WALAHFR. hort.; 109 "visitur ... rubens <<aliena in #sed.|e>> (gloss.: in ulmo) racemus dependere."

            UUU_BEDEUTUNG de sole; meton. in nomine loci:

            * ANNAL. Rom.; p. 478,10 "per viam, que pergit ad Sanctum Stephanum in Celio monte, insecuti sunt {ceteri} eos {(fideles)} usque ad #Sedem Solis {(v. notam ed.)}."
                

            UUU_BEDEUTUNG de campo in tabellis ludorum sito:

                * CARM. Scot. II; 1 sol. 5,4 "uno sub nigro #sed.|em retinente propinquam."
                * CARM. var.; 82,65 ((ed. H. Hagen. 1877.; p. 139)) "candida si #sed.|es fuerit sibi {(reginae)} prima tabella."

            
            UUU_BEDEUTUNG de loco salinae sim.; 'Pfannstatt' ((*{de re v.} Dt. Rechtswb. X.; p. 766 *{et} O. Volk, Salzproduktion und Salzhandel mittelalt. Zisterzienserklöster. 1984.; p. 21)):

                * DIPL. Heinr. V.; 32 ((ed. M. Thiel, Die Urkunden Heinrichs V. und der Königin Mathilde. MG Dipl. reg. et imp. Germ. VII. {[ed. sub prelo]})) "tres #sed.|es sartaginum in villa Marsula."
                * CHART. Salisb. I; 304 p. 425,14 "tria ... quartalia et plus melioris saline in duobus asseribus fontis salinarii apud Halle cum #sedi.|bus patellariis ... fratres ... possederunt."
                * ACTA imp. Winkelm.; I 14 p. 11,21 "apud Marsallam duas #sed.|es patellarum cum patellis ab omni exactione liberas."


            UUU_BEDEUTUNG de loco molendini; 'Mühlstatt' ((*{de re v.} Dt. Rechtswb. IX.; p. 946)):

                * DIPL. Conr. III.; 92 p. 164,19 "#sed.|em molendini iuxta claustrum ... stabilimus."
                * INNOC. III. registr.; 1,545 p. 789,23 "#sed.|em batarini, in quo molendinum edificastis {(sc. sorores)}."
                * ACTA imp. Winkelm.; I 14 p. 11,9 "quatuor #sed.|es molendinorum."

            UUU_BEDEUTUNG de loco ad mercedem deponendam constituto; 'Stapel' ((*{de re v.} Dt. Rechtswb. XIII.; p. 1494sqq.)):

                * CHART. Lub.; I 405 (a. 1280) "ut ... generalis #sed.|es et depositio mercationum existat in Ordenburg."


        UU_BEDEUTUNG incorpor.:

            UUU_BEDEUTUNG gener.:

                * PURCH. Witig.; 136 "qui templum pneumatis almi factus divinae #sed.|es est atque sophiae."
                * EPIST. Teg. I; 130 p. 147,1 "quoniam ... omnia hec {(sc. scientiam)}, que mihi assignasti, lectionis assiduę continua vacacione in te #sed.|em posuisse recognoscis."
                * OTLOH. prov.; A 8 "anima iusti #sed.|es est sapientiae."
                * EPIST. Hild.; 134,26 "vir ille ... #sed.|es prudentię micantissima." {saepius.}


            UUU_BEDEUTUNG geom.:

                * PS. BOETH. geom.; 390 "cuius {(verticis)} #sed.|em si subtendens linea perpendiculari fuerit iuncta, efficiet triangulum rectiangulum."; 395 " qui {(acutus angulus)} si a recta linea, quae #sed.|is loco fuerit, rectam lineam ... emiserit {eqs.}"

            UUU_BEDEUTUNG math. in abaco:

                * GERB. epist. math. I; 1,2 p. 15^a,19 "articuli ... dividendorum obtinebunt #sed.|es."
                * HERIG. reg.; 1 p. 208,2 "multiplicatio singularium, quorum multiplicatores in sua #sed.|e constituunt digitos et in secundo a se loco mittunt articulos."
                * MATHEM. var. Bubnov; p. 248,28 "quintam ... #sed.|em obtinentes mille millies mille millia {eqs.} ({antea:} tertio ... loco)." {saepius.}
            
            UUU_BEDEUTUNG comput. de initiali computationum die ((*{de re v.} H. Grotefend, Taschenbuch der Zeitrechnung. ^{13}1991.; p. 6sq.)):

                * BEDA ad Wicth.; 4 "quod scribens non prima #sed.|is aequinoctialis tempora, sed ultima signavit."
                * CALEND. Karol. A; p. 497,10 "#sed.|es epactarum {(v. notam ed.)}."
                * HEIMO BAMB. chron.; 6 p. 491,7 "XVI Kal. Aprilis, ubi Iudei ponunt #sed.|em concurrentium." {al.}

            UUU_BEDEUTUNG mus. de loco soni in systemate sonorum:

                    * SCHOL. enchir.; 1,32 "quisque {(sc. sonus)} in suo tantum ordine propriam retinet qualitatem nec in sua #sed.|e alteri dat locum."
                    * GUIDO ARET. reg. rhythm.; 123 "suam quaeque {(vox)} facit neumam alteri dissimilem neque suae #sed.|is locum dat neumae extraneae."
                    * ARIBO mus.; p. 13,6 "nec naturaliter inter ... superiores et excellentes diapente procedunt species, ubi diatesseron naturales habent #sed.|es, nec {eqs.}"
                    * COMM. microl.; 9 p. 117 "videndum ... est, cum phthongi quilibet a naturali loco non moveantur ..., quomodo descendere ..., ascendere vero ... dicantur, cum a suis #sedi.|bus penitus immobiles esse videantur." {al.}

               

            UUU_BEDEUTUNG gramm.:

                * EBERH. ALEM. labor.; 183 "quae partes {(sc. sententiae)} sint prima #sed.|e locatae, quae sint, quae #sed.|e posteriore sedent."
            
            UUU_BEDEUTUNG metr. de pede metrico:

                * EBERH. ALEM. labor.; 824 "pentameter praeter dissyllaba cuncta relegat #sed.|is postremae de regione suae."


        

AUTORIN Leithe-Jasper