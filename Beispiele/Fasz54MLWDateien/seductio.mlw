LEMMA seductio

GRAMMATIK
subst. III; -onis f.


STRUKTUR
    c. gen. explicativo: {=> seductio_1515}

BEDEUTUNG sensu originario:

    UNTER_BEDEUTUNG actio in errorem ducendi, temptatio -- Verleitung, Verführung, Versuchung {(spectat ad diabolum)}:

        * HRABAN. epist.; 6 p. 391,16 "adiunximus sermonem de variis errorum et vitiorum #seductioni.|bus {@ seductio_1515}, cum quibus antiquus hostis humanum genus deludit ac decipit, hoc est {eqs.}"

         * ALEX. MIN. apoc.; 21 p. 495,25 "nec intrabit in illam aliquod coinquinatum et faciens abominationem, id est omnes, qui diaboli #seductioni.|bus consentiunt."

         {v. et {=> seductor/seductio_300}}
    
    UNTER_BEDEUTUNG deceptio, fallaciae, circumductio -- Täuschung, Gaukelei, das Hinters-Licht-Führen:

        * UNIBOS; 109,4 "sponsaecida praepositus privatus mentis sensibus ad domum portat propriam #seduction.|is bucinam."

    UNTER_BEDEUTUNG insidiae, perfidia -- Intrige, Treuebruch, Verrat; in iuramento: {=> seductio_1; seductio_2}:

        * ANNAL. Mett.; a. 830 p. 96,21 "causa #seduction.|is et malae voluntatis hoc idem {(sc. filium parvulum eicere et imperatricem deponere)} facere nitebantur {(sc. aliqui optimatum)}."

        * NITH. hist.; 2,5 p. 18,9 "graviter Karolus ferens #seduction.|es, quas patri fecerat et actenus illi faciebat {Bernardus}, timens, ne aliter illum comprehendere posset, subito in illum irruere statuit."

        * CAPIT. reg. Franc.; 269 p. 296,19 "absque ulla dolositate aut #seduction.|e {@ seductio_1}... secundum meum ministerium ... fidelis vobis {(sc. regi)} adiutor ero {(sc. fidelis)} ((* {sim.} 220 p. 100,6 "promitto ego, quia ... seniori meo ... fidelis ... et adiutor ... in omnibus ero absque fraude ... et absque ulla dolositate <<vel #seduction.|e>> {@ seductio_2} {(om. 3^b)} seu deceptione"))."

BEDEUTUNG per confusionem i. q. seditio, rebellio -- Aufstand, Rebellion:

    * THEGAN. Ludow.; 22 "inventi sunt ... nonnulli in hac {(sc. Bernhardi)} sedicione (sedictione, #seduction.|e {var. l.}) esse lapsos."

AUTORIN Strika