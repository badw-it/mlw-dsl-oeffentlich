LEMMA sedo

GRAMMATIK verbum I; -avi, -atum, -are

SCHREIBWEISE
    script.:
        ce-: {=> sedo_02} {adde} * CHART. Osn.; III 515 p. 358,27.
        {?}scend(o): {=> sedo_01}
        

BEDEUTUNG trans. i. q. placare, quietare, pacare, componere, exstinguere, sopire, tranquillare, mitigare -- zur Ruhe bringen, beruhigen, beschwichtigen, schlichten, beilegen, beseitigen, hemmen, stillen, löschen, mildern:

    UNTER_BEDEUTUNG spectat ad res naturales, phaenomena:

            * WALAHFR. carm.; 5,42 "pelagi ferventia murmura #sed.|at {(sc. Deus)}."
            * WIDUK. gest.; 1,34 p. 47,14 "angelo flammas #sed.|ante."
            * TRANSL. Epiph.; 11 "quotiens ... aeris intemperiem vel pestis obortae illuviem illius reliquiis nostram urbem lustrando #seda.|vimus ((*{sim.} THANGM. Bernw.; 9 pestilentiam))?" {al.}

    ANHÄNGER in imag.:

            * WALTH. SPIR. Christoph. I; 13 "cum ... quasi minima undarum commotio #seda.|ta transcurreret, maior ... regi tempestas incubuit."
            * DIPL. Heinr. II.; 238 p. 275,33 "quatinus ... perturbationis procella in perpetuum #seda.|retur, adiit ... abba ... postulans a nobis, ut {eqs.}"
            * OTTO FRISING. gest.; 1,22 p. 36,10 "#seda.|tis omnium tempestatum motibus cum triumpho et victoria de Italia rediens {princeps}." {al.}

    UNTER_BEDEUTUNG spectat ad homines eorumque actiones, affectus {sim.}:

        UU_BEDEUTUNG ipsos:

            * CARM. var. I; 13,25 "fortia Francorum #seda.|vit {Radelcar} regna."
            * DIPL. Otton. II.; 299 "precibus ... pauperum ... gentis #seda.|ti pacem et fedus inivimus."
            * THIETM. chron.; 5,31 "rebelles munerum dulcedine ... #seda.|vit (selavit {a. corr.}) {rex} ((*{sim.} HERM. AUGIENS. chron.; a. 1026 "#seda.|tis ... rebellibus"))."
            * EMBR. Mahum.; 447 ((Coll. Latomus 52. 1962.; p. 66)) "#seda.|vit proceres et conciliavit." {al.}

        UU_BEDEUTUNG spectat ad aegritudines {sim.}:

            * PS. GALEN. puls.; 405sq. (cod. Vr) "apostema non rumpitur, digeritur, hoc est tumor ille #seda.|tur, quia #seda.|tus fuerit ex equisuci {eqs.}"
            
            * ALBERT. M. veget.; 3,106 "his {(odoribus)} associandi sunt, de quibus diximus, qui #sed.|ant anhelitum."; animal.; 3,131 "quies ... et frigus et somnus multiplicant fleuma et #sed.|ant humores."

        UU_BEDEUTUNG spectat ad animum, desiderium {sim.}:  

            * WILLIB. Bonif.; 2 p. 8,20 "inlecebrosa ... adulescentiae incentiva et carnalium desideriorum impetus ... #seda.|vere (#seda.|ssent {var. l.}) {(sc. studia)}."
            * RIMB. Anscar.; 30 p. 62,16 "necdum ... animi iuvenum #sed.|ari poterant."
            * THIETM. chron.; 5,32 p. 256,15 "ob #sed.|andam ... arrogantiam." {al.}

        UU_BEDEUTUNG spectat ad clamorem {sim.}:

            * AGIUS epic. Hath.; 474 "#seda.|to planctu."
            * VITA Heinr. IV.; 13 p. 43,12 "tanta laeticia oborta est, ut voces gratulantium vix #sed.|ari possent."
            * OTTO FRISING. gest.; 1,54 p. 76,15 "#seda.|to clamore."

        UU_BEDEUTUNG spectat ad seditiones, conflictum {sim.}:

            * WILLIB. Bonif.; 8 p. 44,20 "Pippinus ... regnum suscepit et iam aliquantulum #sed.|ante (#seda.|ta {p. corr. cod. Sangall. 577 p. 229^b }) populorum perturbatione in regem sublevatus est {(v. comm. ed. p. $XIV; ni spectat ad {=> sedo_04})}."
            * POETA SAXO; 3,459 "furores hostiles."
            * SALOM. III. carm. I; 2,127 "seditiones."
            * RUOTG. Brun.; 25 "bella."
            * DIPL. Heinr. II.; 34 p. 38,20 "rebellionem."
            * THIETM. chron.; 7,50 p. 460,19 "odium." {persaepe.}

        UU_BEDEUTUNG spectat ad discordiam {sim.}:

            * LEG. Wisig.; 2,2,10 p. 87,6 "non solum difficile #sed.|antur altercantium negotia, sed {eqs.}"
            * WIDUK. gest.; 1,22 p. 31,19 "summus ... pontifex mittitur ad #sed.|andam tam ingentem {!discordiam} ((* BERTH. chron. B; a. 1077 p. 286,15. {al. sim.} * CHART. Trident.; 14 p. 18,3 "pro #scendanda {@ sedo_01} discordia"))."
            * THANGM. Bernw.; 43 p. 777,50 "archiepiscopus ... {!lite} #seda.|ta praesulem ... omni honore ... dilexit ((* CHART. Heinr. Leon.; 47 p. 67,31. {al. sim.} * CHART. Pomm.; 668 p. 60,30 "discat felix successio futurorum litem et discordiam ... pacificatam esse ... ac #c|.eda.|tam {@ sedo_02}"))."
            * TRAD. Ratisb.; 340 "omni contradictione #seda.|ta."
            * CHART. Brixin.; 149 p. 161,21 "questiones ... debent #sed.|ari." {saepius.}

    

    UNTER_BEDEUTUNG spectat ad animalia:

        * HARIULF. Arnulf.; 3,8 "bestia immanis {(sc. balena)}, quamvis fortiter iaculis et lanceis fuisset vulnerata, capi vel #sed.|ari nequaquam potuit." 

BEDEUTUNG intrans. {@ sedo_04} i. q. conquiescere, cessare -- sich beruhigen, legen, nachlassen:
    
    * THIETM. chron.; 6,82 "quidam ... miles ... a muribus intra cubiculum inpugnatur ineffabilibus ..., arca ... includitur...; cum exterius haec {(sc. murium)} plaga #seda.|ret (("#seda.|retur" * CHRON. Thietm.)) ..., ab aliis usque ad mortem corrosus invenitur."

AUTORIN Leithe-Jasper