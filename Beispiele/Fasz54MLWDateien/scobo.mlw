LEMMA scobo
GRAMMATIK verbum III; -ere

ETYMOLOGIE  {cf.} scobis, scabere; {M. Petschenig, Wiener Studien VII. 1885. p. 35}

SCHREIBWEISE
   script.:
      scrob(o): {=> scobo_3}
      scop(o): {=> scobo_1} {=> scobo_2}

BEDEUTUNG  strictius i. q. (de)runcinare, scabere -- (herunter)hobeln, abfeilen, -kratzen: 

* HILDEG. phys.; 3,30,15  "de eodem {(sc. ligno)} calefacto #scob.|at {aegrotus} et super locum, ubi dolet, ponat." 

BEDEUTUNG  latius i. q. rimari, effodere -- durchforschen, aus-, umgraben:

    UNTER_BEDEUTUNG proprie:

     * VITA Richardi; 8  p. 284,6 "dum ipsum monasterii ambitum amplificare ... vellet et fundo tenus summa diligentia cuncta #scob.|ere faceret, repperit in ipsius templi parte meridiana octo episcoporum sepulcra ((* {cf.}  $HIER. tract. in psalm. I 76,7 l. 80 ))." 

    UNTER_BEDEUTUNG 
    in imag. vel translate; loci spectant ad Vulg. psalm. 76,7:

     * SIGEB. GEMBL. Theod.; 22 p. 482,13  "#scobe.|bat spiritum suum."

     * OTLOH. Wolfk.; 11 "omni humilitatis sarculo suum #sco.|pebat {@ scobo_1} (#scr|.obe.|bat {@scobo_3} {var. l.}) spiritum."

     * CONR. MEND. Attal.; p. 120,1 "meditata in cubili suo exercebat et #sco.|pebat {@scobo_2} spiritum obnixe Deum exorans ..., ut {eqs}."

VERWEISE
    scabo

AUTORIN
    Mandrin / Orth-Müller