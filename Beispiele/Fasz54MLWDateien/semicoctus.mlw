LEMMA semicoctus

GRAMMATIK
    adi. I-II; -a, -um

SCHREIBWEISE
    script.:
        semis coctus: {=> semicoctus_1}

BEDEUTUNG partim coctus, semicrudus -- halb gekocht, halbgar, halb roh; usu subst.: {=> semicoctus_2}:

    UNTER_BEDEUTUNG gener.:

        * DECRET. Burch.; 19,106 "si quis #semicoct.|um {@ semicoctus_2} comederit inscius, tres dies poeniteat."

        * WILH. HIRS. const.; 1,60 p. 489,12 "una {(cuppa)} ad fabas reservandas, quando fuerint ... #semicocta.|e {(v. notam ed.)}."

        * PLATEAR. pract.; 25,40 "ova appala, elixa vel alia #semicoct.|a (semina cocta {var. l.}) dentur."

        * THADD. FLORENT. cons.; 62,173 "brodium caulium #semicoct.|orum."

        {al.}

                ANHÄNGER de homine salina ferventi vulnerato:

                * CHRON. Mont. Ser.; a. 1224 p. 212,8 "qui {(parrochianus)} ... invenit hominem ..., qui se in sartaginem ... bulientem iniecerat, #semicoct.|um et toto pene corpore inutilem effectum."


    UNTER_BEDEUTUNG natur. i. q. partim, non satis digestus -- halb, noch nicht ganz verdaut, zersetzt:

        UU_BEDEUTUNG de cibo:


            * CONSTANT. AFRIC. coit.; 8,26 "si quis coierit circa mediam noctem, peccat, quia cibus <<est #semicoct.|us>> (omnino indigestus est {var. l.})."

        UU_BEDEUTUNG de humoribus:

            * URSO anat.; 226 "est {(phlegmaticus)} humor frigidus et humidus, #semicoct.|us et insipidus."


BEDEUTUNG ?ad dimidias decoctus -- ?auf die Hälfte eingekocht, reduziert:

    * ANTIDOT. Bamb.; app. p. 39,1 "cera, aspalto trito, oleo et aceto modico in caccabo mittis et resolvis, et sic supermittis picem et resinam; quando autem #{semis coctus} {@ semicoctus_1} ({cod.}, #semisc|.oct.|us {ed.}) est, levas caccabo."


AUTORIN Niederer