LEMMA 1. scopa

GRAMMATIK
     subst. I; -ae

VEL scopae

GRAMMATIK
     subst. I; -arum f.

SCHREIBWEISE
     script.:
         scup(a): {=> scopa_1}
         scov(a): {=> scopa_4}

BEDEUTUNG virgula -- kleiner Zweig, Reis:
         U_BEDEUTUNG de herba:            
                 UU_BEDEUTUNG "#scop.|a regia" i. q. Hypericum perforatum L. -- Echtes Johanniskraut:
                     * GLOSS. Salern.; p. 7^a,22 "herba sancti Iohannis, herba perforata, ypericon, #scop.|a regia."
                     * ALPHITA; H 2 "hypericon, herba sancti Iohannis, herba perforata idem; quidam etiam nominant eam #scop.|am regiam."
                 UU_BEDEUTUNG "#scop.|a stella" i. q. Pimpinella saxifraga L., Sanguisorba minor Scop. -- kleine Bibernelle, kleiner Wiesenknopf:
                     * GLOSS. Salern.; p. 9^a,37 "pimpinella et <<#scop.|a stella>> {(ni confunditur c.} scopostella, sorbastella {sim.)} ((*{cf.} W. F. Daems, Nomina simplicium medicinarum. 1993.; p. 229)) idem est; tota herba utimur."

ABSATZ
         U_BEDEUTUNG virga, fascis virgarum -- Rute(nbündel), Besen; fere usu plur.:
             UU_BEDEUTUNG strictius:
                 UUU_BEDEUTUNG spectat ad actionem verrendi, tergendi sim.:
                     UUUU_BEDEUTUNG proprie:
                         * VISIO Baront.; 14 "si ... fratrem ad monasterium reducis, per singulos dominicos tuum sepulchrum #scop.|is (#scop.|is mundet et, scopet {@ scopo_9} et mundet et, excopabit {@ scopo_10} et mundet et {var. l.}) munditiam adhibeat."
                         * CHART. Epternac.; 83 p. 147,19 "si quis ... ecclesiam #scop.|is mundat singulis sabbatis, accipiat ... panem I."
                         * RHYTHM.; 146,18^tit. "de #scop.|a (#scupa {@ scopa_1} {var. l.})"
                         * VITA Liutg. I; 24 "collegerunt {discipuli} ... #scop.|is mel per pavimentum fluens."
                         * WILH. HIRS. const.; 1,60 p. 490,14 "#scop.|ae ... duae, quibus caldaria post coctionem scopentur{@ scopo_5}."
                         * CONR. MUR. nov. grec.; 2,1183 "#scop.|a domum mundat."
                         {al.}

                     UUUU_BEDEUTUNG per compar.:
                         * VITA Godefr. Cap. I; 12 p. 120,9 "opto ..., ut, quemadmodum #scop.|a usu detrita novissime flammis inicitur, sic corpus meum ... tribulationis demum igne consummetur."

                     UUUU_BEDEUTUNG in imag. vel alleg.:
                         * HILDEG. vit. mer.; 3,500 "idem vir, scilicet Deus, respondit, quod #scop.|is, id est iudiciis et castigationibus suis, eas {(sc. elementa)} sit purgaturus."
                         * VITA Karoli M.; 2,13 p. 55,29 "in primis dignissimum ac iustissimum vere est habitaculum vere fidei scopare {@ scopo_1} #scop.|is vere confessionis."

                 UUU_BEDEUTUNG spectat ad castigationem, punitionem:
                     UUUU_BEDEUTUNG proprie:
                         * GERH. AUGUST. Udalr.; 1,11 l. 35 "episcopum cum #scop.|is ei flagella imponere pro Christi nomine postulavit {(sc. fur paenitens)}."
                         * BRUNO QUERF. Adalb. A; 5 "#scop.|e {@ scopa_3} tergum verrunt, et ferientia flagella dolentem carnem frangunt."
                         * PETR. CAS. (?) chron.; 4,55 "in cymiterio fratrum nudus permanens et psalmos canens duris #scop.|arum ictibus carnem suam affligere non desinebat {(sc. Almannus monachus)}."
                         * ANNAL. Erf. Praed.; a. 1238 p. 95,11 "tres viri nobiles nudi in processione incedentes tribus sacerdotibus cum #scop.|is ipsos sequentibus et disciplinaliter corrigentibus monasterium usque pervenerunt."
                         {v. et {=> scopo_1/scopa_2}}
                         {saepe.}
                         
                     UUUU_BEDEUTUNG meton.:
                         UUUUU_BEDEUTUNG de homine:
                             * ALBERT. STAD. Troil.; 5,982 "tu {(sc. rex)} decus es patrium ..., aura serena bonis, #scop.|a severa malis."
                         UUUUU_BEDEUTUNG ictus (virga effectus), verber -- Ruten-, Gertenhieb:
                             * PETR. DAM. epist.; 18 p. 173,21 "dantur ... ex more tria milia #scop.|arum pro unius anni paenitentia."
                         UUUUU_BEDEUTUNG flagellatio -- Geißelung;  de sodalitate flagellatorum q. d. ((*{v.} LexMA. IV.; p. 509sqq.)):
                             * ANNAL. Mant.; a. 1260 ((MGScript. XIX; p. 23,46)) "#sco.|va {@ scopa_4} sive liberatio incepta fuit in civitate Mantue in festo sancti Martini."

             UU_BEDEUTUNG latius i. q. baculum, clava -- Stock, Knüppel:  
                 * SIGEH. Maxim.; 16 p. 29^D "#scop.|is, quas secum detulerant, altare diverberant {duo iuvenes}."

AUTORIN Weber