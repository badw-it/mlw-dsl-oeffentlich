LEMMA  scutum

GRAMMATIK  substantivum II; -i n.

VEL  1. scutus

GRAMMATIK  substantivum II; -i m.

SCHREIBWEISE

    script.:

        es-: {=> scutum001}

        stur(um): * PHILO; 126 ((Festschrift A. E. Furtwängler. 2009. p. 592)) (({var. l.}))

        schut(um): {=> scutum002}

BEDEUTUNG  ἀσπίς, clipeus -- Schild:

    U_BEDEUTUNG  proprie:

        UU_BEDEUTUNG  in univ.:

            UUU_BEDEUTUNG  gener. ((per compar.: {=> scutum003}{=> scutum004}{=> scutum007})) ((per prosop.: {=> scutum008}{=> scutum009})):

                UUUU_BEDEUTUNG  usu communi:

                    * LEX Sal. Merov.; 30,6 "si quis alteri inputaverit, quod #scut.|um iactasset {eqs.} (({item} * LEX Sal. Karol.; 30,6 "#scut.|um [#e|.scutum {@ scutum001} {var. l.}]"))."               

                    * CARM. de Tim.; 71 "cum ferro ferrum, cum #scut.|is {@ scutum008} #scut.|a repugnant."

                    * FROUM. carm.; 20,32 "arma iacent, et #scut.|a {@ scutum009} silent."

                    * HONOR. AUGUST. gemm.; 1,133 "secundum formam rotundi #scut.|i pinguntur {(sc. coronae sanctorum)}, quia divina protectione ut #scut.|o nunc muniuntur."
                
                    * EBO BAMB. Ott.; 3,5 p. 104,4 "nisi validissima Dei protectione tamquam #scut.|o {@ scutum003} inexpugnabili obumbratus fuisset, ... interiisset (({sim.} * HERM. IUD. conv.; 4 p. 80,2 "velut #scut.|o {@ scutum004} inexpugnabili communiti ... obiectionum ... telis ... contraire"))."
                
                    * ANON. IV mus.; 2 p. 42,29 "supra ultimam {(sc. ligatam)} pone recte quadrangulum et iunge dexteram <partem> unius cum dexter<a> alterius cum tractu uno; et fiet ad modum #scut.|i{@ scutum007}." {persaepe.}

                UUUU_BEDEUTUNG  proverb.:

                    * EGBERT. fec. rat.; 1,452 "#scut.|a die portat, sed nocte in stercore iactat {(v. notam ed.)}."

                UUUU_BEDEUTUNG  in nomine loci:

                    * DIPL. Heinr. IV.; 281 p. 364,36 "sextus {(sc. terminus)} ... usque ad #S|.cut.|um; septimus de #S|.cut.|o usque <ad Cę>cum fluvium {eqs.}"

            UUU_BEDEUTUNG  publ. et iur.:
            
                UUUU_BEDEUTUNG  spectat ad poenam solvendam:

                    * LEX Ribv.; 40,11 "#scut.|o cum lancia pro duos solid. tribuat."

                    * PACTUS Alam.; 1,4 "si quis alteri caput frangit, sic ut ossus de capite ipsius tollatur et super via in #scut.|o sonat, solvat solidos VI (({sim.} * LEX Alam.; 57,4 "si ... de capite ossum fractum de plaga tullerit, ita ut ... in #scut.|o [#scut.|um, secuto {var. l.}] sonaverit ille ossus, cum VI solidis conponat." * LEX Frision.; 22,71 {al.}))."

                    * LEX fam. Worm.; 23 "si quis ... filiam vi rapuerit ..., amicis ... XII #scut.|a ... pro reconciliatione persolvat."

                UUUU_BEDEUTUNG  spectat ad inaugurationem equitum sim.:

                    * CAPIT. reg. Franc.; 25,4 "qui {(servi)} ... in bassallatico honorati sunt cum domini sui et caballos, arma et #scut.|o et lancea, spata et senespassio habere possunt (({sim.} 44,5 "de armis infra patria non portandis, id est #scut.|is et lanceis et loricis." {saepe}))."; 32,64 "volumus, ut ... ad unumquodque carrum #scut.|um et lanceam, cucurum et arcum habeant."

                    * DIPL. Karoli M.; 269 p. 396,35 (spur. s. XIII.^{2}) "qui {(Frisones)} #scut.|um militie a dicto potestate recipere debent, in quo {eqs.}"

                    * CHART. Basil. A; I 336 p. 505,15 "in iure ... largiendi arma gentilitia cum #scut.|is retortis et erectis iuxta imperii Romani consuetudines laudabiles."

                UUUU_BEDEUTUNG  spectat ad pignus capiendum vel captum:

                    * CHART. Tirol. notar.; 742^{b} (a. 1237) "Iacobus viator dixit, quod dedisset  tenutam Cuncio notario supra bonis Morfini ... et supra I lebetem et supra II hostios de ferra et supra I #scut.|um {eqs.}"; 805 "dixit {Eberhardus}, quod ei {(domino Morhardo)} staret <pignus> I gilium pro VIIII libris, item VI #scut.|os."

            UUU_BEDEUTUNG  anat.:
            
                UUUU_BEDEUTUNG spectat ad homines:
                
                    UUUUU_BEDEUTUNG  cartilago -- Knorpel (({de re v.} * B. Castelli, Lexicon medicum Graeco-Latinum. 1713.; p. 659)):

                        * PS. GALEN. incis.; 71 "genae et mandibulae et confinia eorum coniunguntur ossi radicis aurium et vocantur #scut.|a, et in ipsis sunt dentes."

                    UUUUU_BEDEUTUNG  os frontale -- Stirnbein:

                        * ALBERT. M. animal.; 1,111 "a nonnullis clipeus vel potius #scut.|um cerebri vocatur {coronale capitis}."

                UUUU_BEDEUTUNG  spectat ad animalia i. q. testudo -- Schildpanzer:

                    * ALBERT. M. animal.; 7,77 "sunt ... milites maris, qui ... habent ... #scut.|a super dorsum, et isti sunt tortuca maris {eqs.}"; 24,12 "efficitur {tortuca maris} valde magna, ita quod #scut.|um eius octo vel novem pedum invenitur; ... hoc animal piscatores ... militem vocant, eo quod habet #scut.|um et galeam; est autem #scut.|um eius, ac si de quinque asseribus sit compositum." {ibid. al.}

        UU_BEDEUTUNG  in donationibus sim.:

            * TRAD. Lunaelac.; 5 (a. 747/84) "donavimus {(sc. abbas)} ... Odalmanno duos caballos ... et #scut.|a et lancea {eqs.}"

            * TRAD. Frising.; 268^{a} "donavit Atto episcopus ... duobus fratribus ... unum caballum cum #scut.|o et lancea (({sim.} * CHART. Sangall. A; 191 "ego ... Adalhram trado ad monasterium Sancti Galli, quicquid ... non usatum reliquerim, id est ... #scut.|a cum lanceis {eqs.}" {al.}))." {v. et} {=> scutum010}

        UU_BEDEUTUNG  in duello:

            * EDICT. Roth.; 164 "grave et impium videtur esse, ut talis causa sub uno #scut.|o (#scut.|um {var. l.}) per pugnam dimittatur (({sim.} 165.; 166))."

            * CAPIT. reg. Franc.; 41,4 p. 117,32 "si ille, qui causam quaerit, duodecim hominum sacramentum recipere noluerit, aut cruce aut #scut.|o et fuste contra eum decertet (({sim.} 134,1 p. 268,8.; 135,1. {al.}))."

        UU_BEDEUTUNG  spectat ad servitium regis vel clericorum superiorum:

            * DIPL. Ludow. Germ.; 70 p. 100,34 "statuimus ..., ut annuatim dona nostrę serenitati veniant ..., id est caballi cum #scut.|is et lanceis (({inde} * DIPL. Arnulfi; 103^{a} p. 152,31.; 146 p. 223,26))."

            * VITA Meinw.; 70 "episcopus eum {(sc. virum nobilem)} ad militem suscipiens XXX aratra in predicto pago in beneficium dedit ea ratione, ut in expeditionem IV #scut.|a transmitteret." {al.}

        UU_BEDEUTUNG  spectat ad foedus ictum:

            * CHRON. Pol.; 3,16 | p. 142,11 "qui {(caesar)} se cum Bolezlao unum #scut.|um coniunxerat {(v. notam ed.)}."; p. 143,7 "Suatopolc Bolezlavo iuravit, quia ... semper fidus eius amicus unumque #scut.|um utriusque persisteret."

        UU_BEDEUTUNG  lanx, scutella -- Tablett:

            UUU_BEDEUTUNG  gener.:

                * MIRAC. Audom.; 2 ((MGMer. V; p. 775,3)) "#scut.|um ... cum panibus et caseis plenum."
                
                * VITA Bard.; 19 p. 337,9 "carne imposita #scut.|is cottidianum sumptum providit hospitibus suis."

                * RUODLIEB; II 18 "iussere cocos prunis assare minores {(sc. pisces)}, maiores #scut.|o regi portant."

            UUU_BEDEUTUNG  canon.:

                * ORD. iud. Dei; B 17,1 p. 688,13 "super unum #scut.|um ante sanctum altare benedicere debes."

            UUU_BEDEUTUNG  liturg.:

                * ADAM gest.; 3,45 p. 187,19 "haec sunt munera, quae rex misit ad reaedificationem Hammaburg: ... unum vas chrismale argenteum, #scut.|um {@ scutum010} argenteum deauratum, psalterium aureis scriptum litteris {eqs.} (({inde} CATAL. thes. Germ.; 130,5))."

ABSATZ  

    U_BEDEUTUNG  in imag.:

        UU_BEDEUTUNG  in univ.:

            * HUGEB. Wynneb.; 13 p. 117,47 "contra atroces satane insidias #scut.|o {!fidęi} ... dimicabat ((* EIGIL Sturm.; 7 "#scut.|o fidei pectus muniens ... ad certamen contra diabolum processit." {al.}))."

            * PONTIF. Rom.-Germ.; 20,22 p. 45,7 "famulam tuam ... tuae protectionis #scut.|o circumtege {(sc. Deus)} {eqs.} (({sim.} * DIPL. Conr. III.; 17 p. 30,26 "humilitatem nostram #schuto {@ scutum002} protectionis sue speramus obumbrari." * CHART. Heinr. Leon.; 107 p. 164,23 {al.}))."

            * LIBELL. de Willig.; 4 p. 745,39 "murum mentis virtutum #scut.|is circumposuit ..., ut {eqs.}"

            * VITA Richardi; 11 p. 286,11 "frater ... #scut.|o piae devotionis munitus medios ignes ingreditur."

            * CHART. Lub.; 167 "munitio nostra {(sc. civitatis)} ... terrarum infra iacentium #scut.|um erit et antemurale." {persaepe.}

        UU_BEDEUTUNG  in titulo libri:

            * ARNO REICHERSB. scut.; p. 1493^{A} "subsequens opusculum, quod '#S|.cut.|um canonicorum' dicitur, ... confeci ego fr. Arno."

ABSATZ

    U_BEDEUTUNG  alleg.:

        * HRABAN. univ.; 20,12 p. 543^{B} "#scut.|um ... fidem significat ... aut adiutorium divinum; ... item #scut.|um obduratio cordis vel peccati defensio."

        * ALBERT. M. Iob; 41,6 p. 494,9sqq. "de compositione exteriori addit ...: 'corpus eius' ... 'quasi #scut.|a fusilia', hoc est #scut.|a aerea sive de aere fusa; haec #scut.|a significant defensionem, qua se defendit in malitia."

ABSATZ

    U_BEDEUTUNG  meton. i. q. miles -- Soldat:

        * DONIZO Mathild.; 1,538 "exiit {Chonradus} ex lucis cum quingentis fere #scut.|is."

        * CHRON. Albr.; p. 675,42 "Britonum Maddan annis XL, cui successit Mempricius annis XX, post quem Ebraucus annis XL, Britonum Brutus, viride #scut.|um, annis XVI {eqs.}"

ABSATZ

    U_BEDEUTUNG  translate:

        UU_BEDEUTUNG  de coopertorio vasorum:

            * PS. GERH. CREM. sal. I; 10 "renovetur ei {(sc. sali armoniaco)} invice omni, inde invenies ... ipsum simile nivi in #scut.|o aludel."; sept.; p. 329,16 "sal ... armoniacus inprimis sublimandus est, donec fiet albus cum rebus, quas nunc dicam; et hoc est, ut fiet ei aluthel tenue et #scut.|um de conca vitreata."

        UU_BEDEUTUNG  spectat ad focale quoddam clericorum:

            * CHART. Mog. A; I 2^{b} p. 2^{b},13 (spur. s. XII.^{med.}) "aream ... acquisivi {(sc. Bilehilt)} cum rubeis #scut.|is XII auro paratis et totidem equis nigris (({inde} * VITA Bilhild.; ((Francia. 21,1. 1994.; p. 67)) "areolam a prefato episcopo cum rubris #scut.|is duodecim auro paratis totidemque nigris equis adepta est {(v. comm. ed. p. 30sq.)}"))."

        UU_BEDEUTUNG  spectat ad insignia:

            * CATAL. thes. Germ.; 111,30 "de eodem {(sc. Graeco)} panno dorsale cum #scut.|o et ymagine."
    
        UU_BEDEUTUNG  auxilium, praesidium, protectio -- Schirm, Schutz:

            * LEX Sal. Merov.; 44,1 "sicut adsolet homo moriens et viduam dimiserit, qui eam voluerit accipere, ... in ipso mallo #scut.|um habere debet (({sim.} 46,1. {de re v.} * E. Goldmann, Neue Beiträge z. Gesch. d. fränk. Rechts. 1928.; p. 74sqq.))."

            * EGBERT. fec. rat.; 1,468 "qualiter adversus fortes pugnabit inermis? #scut.|a Dei, qui materialia non potes, obde {(v. notam ed.)}."

            * THEOD. EPTERN. chron.; 2 ((MGScript. XXIII; p. 48,14)) "auxit {Arnoldus} comitatum Lucelburgensem ad supplendum #scut.|um regalis exercitus."; libell.; ((MGScript. XXIII; p. 67,26)) "ut ... comitatus Luzelburgensis ad #scut.|um regalis exercitus complendum de bonis eius {(ecclesiae Epternacensis)} sint sagaciter ordinate." {ibid. iterum.}

        UU_BEDEUTUNG  auctoritas -- 'Autorität':

            * RHYTHM.; 115,48,2 "magistri #scut.|o Bede sui mensis pingitur {(sc. dies)}, predicatur proventura, resque numquam cernitur {(v. notam ed.)}."

AUTOR  Fiedler

UNTERDRÜCKE WARNUNG 516