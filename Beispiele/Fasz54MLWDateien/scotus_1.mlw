LEMMA 1. *scotus

GRAMMATIK
     subst. II; -i m.

ETYMOLOGIE {orig. slav., cf. polon.} skojec: *{v} F. v. Schrötter, Wörterbuch der Münzkunde. 1930.; p. 619      

SCHREIBWEISE
     script.:
         scut(us): {=> scotus_1}

BEDEUTUNG  moneta quaedam (slavica) vel pondus quoddam -- (slawische) Münze oder Gewicht, 'Schot', 'Skot'; valoris quatuor et vicesimae partis marcae (({cf.} *Dt. Rechtswb. XII.; p. 1116sq.)):
     U_BEDEUTUNG in univ.:
         * CHART. Sil. D; I 83 p. 56,44 (a. 1202/03) "monetarius ... de singulis marcis singulos #scot.|os accipiat."
         * CHART. Pruss. I; 141 p. 106,30sqq. "currus gerens pannos de unius equi sarcina in Gnezna duos #scot.|os castellano dabit ...; quotquot equi fuerint, de quolibet equo duos #scot.|os castellano dabit et duos monetario."
         * CHART. Sil. D; III 284 p. 189,7 "que {(curiae)} solvunt dimidium #scot.|um vel integrum #scot.|um." 
         ANHÄNGER fort. huc spectat:
             * CHART. Grimm.; 251| p. 177,35 (a. 1251) "hec ... pertinent ad parrochias supradictas: ... in Klitsowe decime de quibusdam agris, in Begenowiz duos #scot.|os."; p. 178,6 "iste sunt decime ad Antiquum Belger pertinentes: ... Stele de quatuor mansis et tres #scot.|i, Wochtewiz duodecim #scot.|i et modius siliginis {(sed cf. ind. p. 437)}."

     U_BEDEUTUNG c. gen.:
         * CHART. Pommerell.; 54 p. 47,3 (a. 1236) "confirmamus {(sc. dux)} ... de tabernis ... V #scot.|os denariorum accipiendos singulis hebdomadibus."
         * CHART. Hans.; 272 "cum plaustra descendant, ad quodvis castrum duos #scot.|os {!argenti} {(sc. dabunt)}
         ((
             * CHART. Pommerell.; 74 "ut de quolibet lastone unum #scot.|um a. solvant {burgenses de Lubec}."
             * CHART. Sil. D; III 486 "possessores mansorum de sex #scutis {@ scotus_1} a. puri rationem annis singulis sint reddituri."
         ))."

AUTORIN Weber