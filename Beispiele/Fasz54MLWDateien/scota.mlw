LEMMA *scota

GRAMMATIK
     subst. I; -ae f.

VEL {raro ({=> scotus_4; scotus_5})} *scotus

GRAMMATIK
     subst. II; -i m.

ETYMOLOGIE theod. vet. schôt, schoet:
     *{cf.} Lexer, Mittelhochdt. Handwb. II. 1876.; p. 773

SCHREIBWEISE
     script.:
         sot(a): {=> scotus_1; scotus_2; scotus_3}
         scat(a): {=> scotus_6}

BEDEUTUNG mensura quaedam -- eine Maßeinheit, 'Schött', 'Schott'; de re v. Dt. Rechtswb. XII. p. 1119sq.:
     U_BEDEUTUNG de mensura agri:
         * TRAD. Brixin.; 93^a (a. 1050/65) "pratorum iugera XV, quod vulgo dicunt V #scôti {@ scotus_5} (#scóti {var. l.})."; 276  "quidam nobilis homo ... VI iugera agri et unam #scot.|am prati ... donavit."; 295 "quoddam pratum #scot.|am I valens."
     U_BEDEUTUNG de mensura frumenti:
         * TRAD. Brixin.; 436 (a. 1115/25) "unde {(sc. praedio)} annuatim tributum, quod sunt VI #scot.|ę, provenire debet."; 442^a "tantum predii, unde tributum novem #scot.|orum {@ scotus_4} annuatim provenire debet." {ibid. al.}
     U_BEDEUTUNG de quodam numero, fasciculo sim. rerum:
         * REGISTR. Udalr. August.; 2,13 p. 207,23 (a. 1157/64) "de secundo manso ... hoc persolvitur: ... IIII ova et dimidium, II^o #scot.|e fęni."
         * COD. Wang. Trident.; 191| p. 952,28 "Ainricus in Ronahe solvit anuatim V sotas {@ scotus_1} {!tegularum}
         ((; p. 953,3 "Gotexalcus solvit VI #sotas {@ scotus_2} t. cum aportatu."; p. 953,9 "quod {[praedium]} solvit domino III #sotas {@ scotus_3} t. cum aportatu"))."
         * REGISTR. Garz.; 47 "de Hûs vas vini, X #scot.|as lini non purgati."      
     U_BEDEUTUNG de mensura casei:    
         * COD. Wang. Trident.; 186 p. 937,18 (a. 1211) "de monte Favazeti, quem dicebant esse de redditu anuatim XL #scatarum {@ scotus_6} casei."   

AUTORIN Weber