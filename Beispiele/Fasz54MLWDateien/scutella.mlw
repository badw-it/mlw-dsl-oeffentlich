LEMMA  scutella (schu-, scot-, -tt-, -ela)

GRAMMATIK  substantivum I; -ae f.

VEL  {raro ({=> scutella006}{=> scutella007})} *scutellum

GRAMMATIK  substantivum II; -i n.

SCHREIBWEISE

    script.:

        c-: {=> scutella005}

        sent-: {=> scutella008}

        scult-: {=> scutella009}{=> scutella001}{=> scutella004}

BEDEUTUNG  (parva) scutra, lanx, catinus, cophinus -- (kleine) Schale, Schüssel(chen), Korb:

    U_BEDEUTUNG  gener.:

        UU_BEDEUTUNG  in univ.:

            * ALCUIN. epist.; 189 p. 317,29 "Liudgarda ... #scutell.|am argenteam ... in aelymosynam ... vestrae {(sc. archiepiscopi)} sanctitati direxit."

            * EKKEH. IV. bened. II; 107 "#scul|.tell.|ę {@ scutella009} porci procul omnis sit dolus Orci."

            * CHRON. Pol.; 1,6 p. 21,4 "lances et #scutell.|as (scul- {@ scutella001} {var. l.}) et cornua de mensis congregare precepit {Bolezlavus}."

            * GESTA Trud. cont. I; 13,6,1 "II^a feria et IIII^a et VI^a feria dabantur #scutell.|ę refertę bonis piscibus unicuique fratrum."

            * CONR. HIRS. dial.; 159 "est ... carmen satyricum, quod ... dictum a satyra, id est #scutell.|a magna."
        
            * CHART. Laus.; 47 p. 91,27 "discordia ita fuit pacificata ..., quod ... dictus Vu. ... mitteret unam #c|.utell.|am {@ scutella005} milii cocti in domo dicti Iacobi." {persaepe.}

        UU_BEDEUTUNG  usu geom.:

            * ANON. geom. I; 4,8 "posito in speculo centro vel in media #scutell.|a plena aquae constituatur in plano arvo, donec {eqs.}"

        UU_BEDEUTUNG  spectat ad signum a monachis dandum:

            * WILH. HIRS. const.; 1,15 p. 946^{C} "pro signo #scutell.|ae manum eleva digitosque non coniunctos aliquantulum inflecte, eo quod ipsa concava sit."

        UU_BEDEUTUNG  discus (ad serviendos cibos aptus) -- (Servier-)Platte:
            
            * CARM. Bur.; 130,5,1 "in #scutell.|a iaceo {(sc. cycnus)} et volitare nequeo."

        UU_BEDEUTUNG  spectat ad partes librae:

            * SALIMB. chron.; p. 651,12sqq. "dixit {Bertholdus} michi {(sc. dominae nobili)}, ut dicerem ..., quod poneretis {(sc. campsor)} denarios ex una parte in #scutell.|am statere et ego in alteram #scutell.|am sufflarem, et hoc signo poteritis cognoscere, quantum valet; posuit igitur denarios larga manu et implevit #scutell.|am statere, ipsa vero insufflavit in alteram, et statim preponderavit, et denarii subito sunt elevati, ac si conversi fuissent in plumeam levitatem."; p. 651,19 "tanto pondere eam fixit spiritus sanctus, ut #scutell.|a lance, que erat ex parte domine plena flatu, elevari denariorum ponderositate nullatenus posset."

    U_BEDEUTUNG  publ., iur., canon. (spectat ad vectigalia, tributa):

        UU_BEDEUTUNG  in univ.:

            * CHART. march. Misn. II; 55 p. 47,14 (s. XII.^{med.}) "statuerunt {(sc. viri praenominati)} dari servitium advocato ter in anno ... triginta caseos, dimidium sexagene #scutell.|arum {eqs.}"

            * CHART. Naumb. I; 391 | p. 350,34 "solvunt {(sc. quattuor viri)} ... IIII #scutell.|as milii (({sim.} p. 352,5 "#sch|.utellas"))."
        
            * LEX minist. Col. II; 3 p. 61,3 "quicunque ... dabit ... V ligaturas de lucio, quarum queque habeat X lucios, quorum quilibet more curię partiri queat in quatuor #scu.|ttellas."
            
            * CHART. Rhen. med.; II app. 16 | p. 440,42 "recipit aratrum quatuor moytales et #scutell.|am pise (({sim.} p. 450,5 "#scutell.|um {@ scutella007} pise"))."
            
            * CHART. Mekl.; 1504 p. 610,5 "dabunt {milites et vassalli} ... nobis {(sc. comitibus)} singulis annis in recompensationem dicte monete ... duos denarios de #scu.|ttella." {persaepe.}

        UU_BEDEUTUNG  spectat ad pignora solvenda:

            * ANNAL. Pegav.; p. 240,28 "acceptis ... obsidibus cum argento, quod exegerat {imperator}, quingentis scilicet pateris et #scutell.|is aureis et argenteis totidem et quattuor milibus marcharum." {ibid. iterum.}

        UU_BEDEUTUNG  spectat ad munus pictantiam q. d. administrandi i. q. sportula -- Spende (({v.} * LexMA.; VI. p. 2188 s. v. 'Pitanz')):

            * CHART. Rhen. med. III; 730 p. 550,24 (a. 1241) "ortus adiacens XII denarios {(sc. solvit)} ... ei, qui ... procurat officium #sco|.tell.|arum in ecclesia B. Paulini."

                ANHÄNGER  fort. huc spectat:

                    * CHART. Merseb.; 375 p. 311,19 (a. 1271) "in Hamersleuen quatuor liberi mansi ... et due aree, ibidem duo iugera preconis, duo #scutell.|arum, duo hortulani {eqs.}"

    U_BEDEUTUNG  liturg. de ciborio, patena sim.:

        * WILH. HIRS. const.; 1,84 p. 1011^{A} "est patena mediocris et argentea et eiusdem metalli #scutell.|a habens operculum {eqs.}"

        * LIBER ordin. Rhenaug.; p. 131,10 "postquam sacerdos ... corpus Dominicum incensaverit et is, qui est expeditis manibus, illud, ut est in #scutell.|a, acceperit {eqs.} {(v. notam ed.)}."

        * VITA Otton. Bamb. I; 23 "pleraque ... loco ornamenta contulit, inter quae duas #scutell.|as (("#scul|.tellas{@ scutella004}, #sch|.utellas" {var. l.})) argenteas ad suscipiendas oblationes."

        * CHART. episc. Halb.; 449 l. 45 "#scutell.|am argenteam non modice quantitatis ad calicem deportandum {(sc. ecclesiae conferendam duxit episcopus)}."

        * CATAL. thes. Germ.; 73,16 "habemus eucharistię pertinentem #scutell.|am argenteam."; 31,29 "#scutell.|a argentea ad patenam." {al.} {v. et} {=> scutula/scutella011}

            ANHÄNGER  in ritu pagano:

                * GESTA Trev.; 6 "thure vel alia qualibet odorifera specie #scutell.|ae iniecta." {v. et} {=> scutula/scutella010}

    U_BEDEUTUNG  alch.:

        * THEOPH. sched.; 3,35 "cum forcipe tene vasculum super #scutell.|am latam siccam et funde in illud vivum argentum cum auro."

        * PS. ARIST. magist.; p. 646^{b},3 "os sacculi liga stricte et pone ipsum in #scutell.|a vitreata {eqs.} (({sim.} p. 658^{a},10 "occulti ... lapidis albi recentis, quantum vis, assume, quem subtiliter commiscebis in vitreata #scutell.|a {eqs.}" * IOH. sacerd.; 112))."

        * GEBER. clar.; 1,53 "tere supra porfidum et lava in una #scutell.|a cum aqua dulci, donec salsedo recedat."

        * IOH. sacerd.; 112 "si in vitreo vase vel #scutell.|o {@ scutella006} Ispanie operari volueris, argenteum runcinum ... accipies {eqs.}"

        * PS. AVIC. anim.; 6,6 p. 239 "sunt, qui faciunt {(sc. aludel)} in modum #scutell.|ae profundę." {saepius.}

    U_BEDEUTUNG  medic.:

        * MATTH. PLATEAR. (?) gloss.; p. 382^{B} "eligendum est {(sc. semen psyllii)}, quod est post ventilationem, in fundo #scutell.|ae resideat: {eqs.}"

        * ROGER. SALERN. chirurg.; 607 "in paropside (gloss.: id est in #sen|.tell.|a{@ scutella008}) terrea ... conficiantur {(sc. camphora et margarita)}."

        * ALBERT. M. animal.; 22,92 p. 1398,31 "herba ganda ... teratur, et de illa plena #scutell.|a cum aliquanta aqua equo ad transgluciendum detur."

        * THEOD. CERV. chirurg.; 3,25 p. 167^{H} "accipias unam #scutell.|am et, si nodus sit in tergo manus, ... percutias super nodum fortiter cum #scutell.|a." {al.}

AUTOR  Fiedler