LEMMA seduco

GRAMMATIK 
        verbum III; -duxi, -ductum, -ere

SCHREIBWEISE
        script.:
            sedic(o): {=> seduco_20}

STRUKTUR
    struct. c.:
        praep.:
            ab: {=> seduco_2; seduco_6}
            ad: {=> seduco_1}
        dat.: {=> seduco_100}


GEBRAUCH
        depon.:* ALCUIN. epist.; 188 p. 315,33 {(codd.)}
        usu:
            absol. vel ellipt.: {=> seduco_21; seduco_21A; seduco_200}
            refl.: {=> seduco_22}
                
        partic. usu adi.: 
            praes.: {=> seduco_5A}   
            perf.: {=> seduco_3B; seduco_3D; seduco_3E; seduco_1000} 


BEDEUTUNG sensu originario:

    UNTER_BEDEUTUNG seorsum ducere, semovere -- beiseite führen, absondern:

        UU_BEDEUTUNG corpor.:

            UUU_BEDEUTUNG refl. i. q. discedere, abire -- sich entfernen, weggehen:

                 * ANNAL. regni Franc.; a. 763 "Tassilo, dux Baioariorum, ... per malum ingenium se inde {(sc. a placito)} #sedu.|xit {@ seduco_22}."
            
            UUU_BEDEUTUNG abducere, eripere -- entführen:

                * LEX Ribv.; 39,3 "si quis ingenuam puellam vel mulierem ... accipere vel #seduc.|ere (reducere {var. l.}) sine parentum voluntatem de mundepurdae abstulerit {(cf. comm. ed. p. 149)}."

            UUU_BEDEUTUNG (a cursu) avehere -- (vom Kurs) abbringen, weglenken:

                UUUU_BEDEUTUNG in bonam partem:

                    * WALTH. TER. Karol.; 41,19 "totis cepit {(sc. Burchardus)} viribus uti et, ut eam {(sc. naviculam)} a ripa #seduce.|ret {@ seduco_2}, toto conamine niti."
                
                UUUU_BEDEUTUNG in malam partem:

                    * CONR. MUR. nov. grec.; 1,2630 "fallens #seduc.|it (se ducit {var. l.}) naves ad littora ducens."

            UUU_BEDEUTUNG educere, purgare -- abführen, ableiten {(usu medic.)}:

                * ANTIDOT. Sangall.; p. 98,2 "menstrua insecunda ({cod.}, in secunda {ed.}) #seduc.|it {oleum samsucinum}."

                * ANTIDOT. Bamb.; 35 "menstrua mulieribus #seduc.|it {(sc. hiera Galieni)}."
    
        UU_BEDEUTUNG spirit.:

            UUU_BEDEUTUNG demere -- wegnehmen:

                UUUU_BEDEUTUNG subterducere -- entziehen:

                    * RHYTHM.; 2,15,2 "<<nec amenae tuae {(sc. Medardi)} ... paradisi epulae nosmet ... #seduc.|ant (#sedi|.cant {@ seduco_20} {cod.}) memoriae>> {(ci. Meyer, v. notam ed.)}."

                    * CONR. MUR. nov. grec.; 1,2631 "qui clam removet, #seduc.|ere {@ seduco_21A} (#seduc.|itur {var. l.}) dicitur ille."
                
                UUUU_BEDEUTUNG deducere -- subtrahieren {(usu math.)}:

                    * ANON. geom. I; 4,7 "medietas hinc {(sc. a XXIV)} sumatur; ex his basis #seduc.|atur, id est VIII."

            UUU_BEDEUTUNG avertere, vitare -- abwenden, vermeiden:

               * ECBASIS capt.; 880 "unusquisque gemat, capitalia crimina tergat, confluat ad lacrimas, si vult #seduc.|ere penas."

            UUU_BEDEUTUNG ?(a consilio) revocare -- ?(von einem Vorhaben) abbringen, umstimmen:

                * HELM. chron.; 25 p. 51,7 "soli Sturmarii, Holzacii et Thethmarchi egressi sunt cum brevi numero; hos ... facile uno verbo #seduc.|am {(sc. traditor)} et faciam redire ad loca sua."
        
    UNTER_BEDEUTUNG (in errorem) inducere, transversum agere, pellicere, illicere, temptare -- auf Abwege führen, vom rechten Weg abbringen, verleiten, verlocken, verführen, in Versuchung führen:

        UU_BEDEUTUNG in univ.:

            UUU_BEDEUTUNG usu communi:

                UUUU_BEDEUTUNG usu vario:

                    * WILLIB. Bonif.; 7 p. 40,19 "Aeldebercht et Clemens a via veritatis populum profana pecuniarum cupiditate #seduc.|ti iugi averterunt studio."

                    * WALAHFR. Wett.; 761 "#seduc.|ta {@ seduco_3B} cohors sequitur per cuncta magistram."

                    * HROTSV. Abr.; 7,3 "quis te {(sc. Mariam)} #sedu.|xit (deduxit {a. corr.})?"

                    * VITA Bonif. II; 2 p. 94,5 "delirando errabundus et vagus et numquam stabilis, ... #seduc.|tus {@ seduco_3D} et #seduc.|ens {@ seduco_5A}, ita ut pene venerandum principem Karolum in eandem simulationem adduxerat {(sc. Adelbertus pseudopropheta)}."

                    * BERTH. chron. B; a. 1078 p. 314,23 "legati ... regis Heinrici ... muneribus, mendaciis, promissis ... corruptos, delusos, #seduc.|tos {@ seduco_3E} ... omnes in favorem sui regis attrahere semper non cessabant."

                    * OTTO FRISING. gest.; 2,40 p. 149,14 "ductus sum {(sc. latrunculus)} ab iniquis et #seduc.|tus {@ seduco_1} ad haec infortunia {(sc. latrocinium)}."

                    {v. et {=> seductor/seduco_ABC}}

                    {saepius.}

                UUUU_BEDEUTUNG de adulescentia facile seductibili:

                    * BERTH. chron. B; a. 1068 "hic {(Heinricus rex)} adolescentie sue errore #seduc.|tus legitime coniugis adeo obliviscitur ..., ut {eqs.}"

                    * HELM. chron.; 67 p. 126,14 "Ethelerus ... abiit ad Kanutum in dolo dataque pecunia principibus exercitus #sedu.|xit adolescentiam Kanuti, ut rediret in terram suam."

                UUUU_BEDEUTUNG de temptatione diaboli:

                    * EIGIL Sturm.; 17 p. 151,12 "hi {(falsi fratres)} cum ... suasionibus diaboli essent #seduc.|ti, ... in Lulli episcopi suffragium confisi perrexerunt ad regem et beatum virum apud illum accusabant."

                    * CAND. FULD. Eigil. I; 10 p. 228,45 "serpens #sedu.|xit Evam."

                    * DIPL. Otton. II.; 206 p. 235,17 "si quis ... diabolico instinctu #seduc.|tus aliquando contra hanc nostre auctoritatis paginam aliquid agere ... presumpserit."

                    * IDUNG. PRUF. dial.; 3,494 "Christus ... offenditur, quando ... propter fratrem a primo et maximo fure, scilicet diabolo, #seduc.|tum {@ seduco_1000}, subtractum et in seculum retractum ... nec specialis fit oratio."

                        {al.}
        
            UUU_BEDEUTUNG spectat ad fidem, religionem:

                * CONC. Karol. A; 5 p. 43,14 "Aldebertus ... #seduc.|ens populum diversis erroribus ... sit ab omni officio sacerdotali alienus {eqs.}"

                * EPIST. Col.; 9 p. 252,38 "miramur {(sc. rex)}, quae subiecta persona molitur ... #seduc.|endo {@ seduco_21} corrumpere vestrae {(sc. papae)} stabilitatem fidei."

                * WALTH. SPIR. Christoph. I; 16 "num ... vos {(sc. Niceam et Aquilinam)} illius malefici potio venenata #sedu.|xit ((* {sim.} 17))."

                * BERTH. chron. B; a. 1077 p. 274,14  "turba ... plebeiorum ab huiusmodi personis {(sc. haereticis)} #seduc.|ta nihil aliud credidit ... nisi {eqs.}"

                * CHRON. Erf. min.; p. 662,10 "tres ... #sedux.|erunt totum mundum, videlicet Moyses Hebreos, Christus Christianos et Machometus barbaros {(v. notam ed.)}."

                    {al.}

                ANHÄNGER struct. c. dat.:

                    * RHYTHM.; 88,4,2 "nemo vobis ... #seduc.|at {@ seduco_100} per suam epistolam nec per sermonem nec per signa nec per vana gloria."
    

        UU_BEDEUTUNG impulsu libidinis temptare -- sexuell verführen:

            * CARM. Bur.; 178,4,4 "nec me vincit mulier tam subito, que #seduc.|at {@ seduco_200} oculis ac digito."
    
        UU_BEDEUTUNG decipere, fallere -- täuschen, hinters Licht führen; usu mediopass.: {=> seduco_500}:

            * HINCM. divort.; 7 p. 163,7 "quasi 'iudex omnium' ... errore #seduc.|i {@ seduco_500} potuerit, componere possumus, quod {eqs.}"; 10 p. 170,15 "providere ... debet episcopus ..., ut non errore #seduc.|tus aut timore coactus quisquam mendaciter in se dicat."

            * MANEG. c. Wolfh.; 5 p. 53,2 "sicut in his, quę sensibus subiecta sunt, ita quoque in intellectualibus ... tam multę et varię species sunt, ut, nisi 'spiritus pietatis' adsit, ... 'cor hominis pravum et inscrutabile' pronum sit verisimili ratione #seduc.|i."

        UU_BEDEUTUNG devocare -- weglocken (von):

            * CARM. libr. II; 5,1,2 "en tibi perpetuae cupiens infundere vitae gaudea, lector prudens, ne #seduc.|aris {@ seduco_6} ab istis."

        UU_BEDEUTUNG ad defectionem impellere -- zum Abfall verleiten, abtrünnig machen:

            * VITA Heinr. IV.; 9 p. 32,21 "cum ... imperatoris fuga cognita esset, eventus ille multos ab eo #sedu.|xit {eqs.}"

ABSATZ
BEDEUTUNG pro verbo simplici i. q. ducere, degere -- verbringen:

        * ALBERT. STAD. Troil.; 3,779 "noctes insompnes infelix ducit Achilles, ... et nunc cantando longos #seduc.|ere soles ... studet."


AUTORIN Strika
        
        