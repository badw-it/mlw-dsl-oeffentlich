LEMMA sedile

GRAMMATIK subst. III; -is n. (-is, -is m.: {=> sedile_04}{=> sedile_05})

VEL {raro ({=> sedile_02}{=> sedile_07})} sedilium

GRAMMATIK subst. II; -i n. (-us, -i m.: {=> sedile_08})

SCHREIBWEISE
    script.:
        sid-: {=> sedile_01}{=> sedile_06}
        sedel-: {=> sedile_09}

FORM
    form.:
        gen. pl.:
            -lum: {=> sedile_10}
        acc.:
            sg.:
                -em: {=> sedile_04}
            pl.:
                -es: {=> sedile_05}
                -os: {=> sedile_08}

        abl. sg.:
            -e: {=> sedile_03}{=> sedile_12}

BEDEUTUNG usu subst.:

    UNTER_BEDEUTUNG sedes, sessibulum, scamnum -- Sitz(platz), Sessel, Stuhl, Bank:

        UU_BEDEUTUNG strictius:

            UUU_BEDEUTUNG proprie:

                UUUU_BEDEUTUNG in univ.:

                        * ORD. Rom.; 16,4 "statim accedunt et sedent sacerdotes in #si|.dil.|ia {@ sedile_01} sua."
                        * CATAL. thes. Germ.; 10^b,10 "vestimenta #sedili.|orum {@ sedile_02} VIIII comparavit {abbas}."
                        * WILH. HIRS. const.; 1,26 p. 261,1 "si ... est {(sc. frater)} de his, qui in ordine sunt altiores, locum habet in aliquo #sedil.|ium (#sedi.|lum {@ sedile_10} {var. l.}) ad parietes."; 1,98 p. 491,3 "duo #sedil.|ia, quae bancos vulgo appellant."; 2,53 p. 257,13 "si est duodecim lectionum festum, qui sunt firmiores, super misericordias #sedil.|ium, qui vero imbeciliores, depositis #sedili.|bus super ea vel super scabella sedent {eqs.}"
                        * ACTA imp. Stumpf; 205 p. 286,34 "ut in domo ... construant {(sc. Bonacursus eiusque fratres)} fenestras, uscias, apothecas, introitum, exitum et #sedil.|ia de foris." {persaepe.}

                ANHÄNGER c. gen explicativo:

                    * WOLFHARD. Waldb.; 2,4^c p. 218,20 "mucro ... iacens patule in sambucae #sedil.|e {@ sedile_03}."
                    * CAES. HEIST. mirac. I; 12,19 p. 329,10 "quem {(sc. maritum post mortem apparentem)} cum vidisset {uxor eius} ..., ad se illum vocans super #sedil.|e lecti sui residere fecit."

                UUUU_BEDEUTUNG thronus -- Thron:

                        * RUP. TUIT. vict.; 4,10 p. 130,16 " cadendo de sella retrorsum ... moriendo palam fecit {Heli}, quod neque #sedil.|e neque coronam auream mereretur habere inter iudices alios."
                        * THEOD. PALID. annal.; a. 1027 "Canutus rex Anglorum ... inperii #sedil.|em {@ sedile_04} suum in litore maris statutum adscendens."
                    

                ANHÄNGER de throno Dei iudicis:

                        * DIPL. Zwent.; 16 p. 46,20 "quidquid ... conferimus, in ultimo tremendi examinis die ante summi arbitri #sedil.|e {@ sedile_11} eternis premiis auctum nobis presentari ... confidimus."

                UUUU_BEDEUTUNG de scamnis latrinae:

                        * FORMA mon. Sangall.; 23,10 "exitus ad necessarium, lucerna, #sedil.|ia."

                UUUU_BEDEUTUNG sella -- Sattel:

                        * IOH. VEN. chron.; p. 164,6 "eburneum #sedil.|e cum suo subsellio ... recepit {imperator}."

                UUUU_BEDEUTUNG de adminiculo paralytici i. q. scabellum, sustentaculum -- Schemel, Stütze:

                        * TRANSL. Germ.; 11 ((MGScript. XV; p. 8,55; s. IX.)) "ut ... pedes eius {(sc. paralytici)} ... immobiles ex artificiosis #sedili.|bus, quibus collocati tenebantur, summa velocitate exilirent."

                UUUU_BEDEUTUNG de sede falconibus apta:

                    * FRID. II. IMP. art. ven.; 2 p. 176,14 "#sedil.|e vocamus ..., quod de ligno vel de petra fit {eqs.}" {ibid. saepius.}

            UUU_BEDEUTUNG in imag. de numeris (in aenigmate calculorum):

                    * ACBRANN. carm.; 33 ((ed. H. Hagen, Carmina inedita. 1877.; p. 144)) (cod. s. X.) "semita producet ... prima novenos, totque secunda geret, tertia quarta novem, quinque cubant laterum tergo #sedil.|e {@ sedile_12} quaterno, calce et principio tot residentque duo."
                 
            UUU_BEDEUTUNG meton.; usu plur.:

                UUUU_BEDEUTUNG ossa natium, nates -- Sitzknochen, Gesäß:

                    * TRACT. de chirurg.; 768 "coxarum ossa sunt duo, id est cranos <vel ideranos>, quod grece #sedil.|ia."

                UUUU_BEDEUTUNG societas, consortium (consedentium) -- Vereinigung, 'Bank':

                    * CHART. Hans.; 593 (a. 1263) "in #sedil.|ia et consortia nostra in civitate Wisbuy ipsos {(sc. de Saltwedele)} recepimus {(sc. civitas Lubycensis)}."


        UU_BEDEUTUNG latius i. q. locus requietionis, cubile -- Rastplatz, Lager:

            UUU_BEDEUTUNG in univ.:

                    * WOLFHARD. Waldb.; 2,6^b p. 226,7 "apta #sedil.|ia peregrinis requirentes {fratres}."

            UUU_BEDEUTUNG {?}sepulcrum -- {?}Grab, Ruhestätte:

                    * GISLEB. ELNON. inc.; 3,198 "relocatur sarcina dulcis intra ... secreta #sedil.|ia criptae."

    UNTER_BEDEUTUNG locus ad coaedificandum assignatus, coaedificatus, casale, area (domus), mansus --  für die Bebauung mit (Hof- und Wirtschafts-)Gebäuden bestimmtes, verwendetes Grundstück, Hofstatt, Hofstelle; usu publ. et iur.:

        * DIPL. Loth. I.; 118 p. 271,35 "inter #sedil.|es {@ sedile_05} et terram arabilem et pratum ... bonnarios XXIIII ad proprium tribuimus."
        * CHART. Traiect.; 49 p. 47,6 "mansa iacent binorthan Flieta; #si|.dil.|ia {@ sedile_06} autem, que ofstedi dicuntur, bisuthan Flieta."
        * DIPL. Otton. I.; 140 p. 221,13 "dedimus ... de abbatia sancti Amantii sortes decem cum #sed.|elibus {@ sedile_09} ad easdem sortes pertinentibus."
        * REGISTR. Bert.; 25 "habet ibi #sedili.|um {@ sedile_07} unum."; 29 "#sedili.|os {@ sedile_08} X." {ibid. iterum.}
        * CHART. Carinth.; III 1216 p. 456,37 "viginti tria #sedil.|ia domorum in civitate Halla." {saepe.}

ABSATZ

BEDEUTUNG usu adi. i. q. ad sedem pertinens -- zu einem Sitz, Sessel gehörig, passend:

    * CHART. Fuld. A; 508 p. 224,12 (a. 837) "pallia linea VI, pulvilli XIIII, lectisternia VI, pallia #sedil.|ia II." 

AUTORIN Leithe-Jasper