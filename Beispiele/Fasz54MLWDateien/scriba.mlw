LEMMA scriba

GRAMMATIK
      subst. I; -ae m. 
      f.: {=> scriba_2}

SCHREIBWEISE
     script.:
         es-: {=> scriba_1}
         schr-: {=> scriba_3} *{adde} CHART. Bund.; 1018 p. 467,24.
         -ipb(a): * CHART. Bund.; 961 p. 411,15


BEDEUTUNG qui scribit, scriptor -- Schreiber:
     U_BEDEUTUNG cancellarius, notarius -- Urkundenschreiber, Kanzlist, Notar:
         UU_BEDEUTUNG in univ.:
             UUU_BEDEUTUNG usu communi:
                 * VITA Ansb.; 4 ((MGMer. V; p. 621,23)) "coepit esse {!aulicus} #scrib.|a {!doctus} conditorque regalium privilegiorum
                 ((
                  * THANGM. Bernw.; 51 p. 779,49 (chart.) "cum essem a. #scrib|a d."
                 ))."
                 * LEG. Lang.; p. 418^a,23 "#scrib.|a ..., qui cartulam ipsam {(sc. venditionis)} scripserit, non aliter presumat scribere nisi {eqs.}"
                 * CHRON. Salern.; 150 p. 547,6 "Ursus, filius Iohannis, qui ... {!palatii} #scrib.|a fuerat
                 ((* CONST. imp. II; 328 p. 438,14sqq. "ego Guilelmus ... notarius et modo #scrib.|a p. comunis Mediolani ... subscripsi {eqs.}"))."
                 * ANNAL. Ianuens.; I p. 229,25 "in causis ... Lanfranco et Ogerio #scrib.|is, Oberto cancellario existentibus."
                 {saepe.} {v. et {=> scribania/scriba_4; scriba_6}}
             UUU_BEDEUTUNG inter testes:
                 * CHART. Wirt.; 419 p. 205,18 (a. 1179) "testes ... sunt: ... Gotefridus, cancellarius imperatoris et #scrib.|a imperatoris, huius privilegii scriptor, Welfo dux."
                 * CHART. Tirol. notar.; I 729^a "in presencia ... Riprandi #sch|.rib.|e{@ scriba_3}."
                 * CHART. Austr. sup. II; 226 p. 220,33 "testes: ... magister Heinricus, Witigo #scrib.|a Anesi."
                 {persaepe.}
                 ANHÄNGER pro (cog)nomine:
                     *CHART. Wirt.; 2290 p. 201,21sqq. (a. 1272) "Ulricus #S|.rib.|a iunior ...; Ulricus #S|.crib.|a senior, Otto Rufus ... et B. humillimus #scrib.|a {@ scriba_6} civitatis."

         UU_BEDEUTUNG in fabula:
             * CARM. Otton. III.; 20,4,21 "#scrib.|a camelus annotat ista tempora velox atque calente omnia cęra addocet omnes {(v. H. Bloch, NArch. 22. 1897. p. 123sqq.)}"
     
         UU_BEDEUTUNG fem. i. quae scribit, scriptrix -- Schreiberin, Vorsteherin der Schreibstube:
             * NECROL.; II Iun. 4 p. 297 (a. 1105/58) "Mahtilt m<onacha> n<ostrae> c<ongregationis> #scrib.|a{@ scriba_2}, Gisila c<on>v<ersa> n<ostrae> c<ongregationis>."

     U_BEDEUTUNG homo (sacrae scripturae) doctus, litteratus -- (Schrift-)Gelehrter:
             * RHYTHM.; 82,27,3 "Esdras #e|.scrib.|a {@ scriba_1} {(ed.,} #scrib.|a {cod.)}."
             * WETT. Gall.; 20 "nec dissimilis fere effectus est {levita} euangelico #scrib.|ae, cum vetera novaque rimabat de corde
             ((
                 {spectat ad} * $VULG.; Matth. 13,52
             ))."
             * HROTSV. Mar.; 89 "quem {(sc. Ioachim)} Ruben templi dum vidit #scrib.|a sacrati {eqs.}
             ((
                 {spectat ad} * $PS. MATTH.; euang. 2,1
             ))."
             * CAES. HEIST. hom. exc.; 274 "quasi ex #scrib.|is, id est scientibus, in synagogis nostris flagellamus {(sc. aliquos)}."
             * CONR. MUR. nov. grec.; 9,247 "doctorem quemvis ... Persa magum, #scrib.|am Iudea, Latina magistrum lingua vocat."
             {saepe.} {v. et {=> scriptor/scriba_5}}

     U_BEDEUTUNG auctor, compositor -- Schriftsteller, Verfasser:
         * MILO Amand. I; 8 p. 482,14 "ad ipsius signi {(sc. ignis divinitus accensi)} magnitudinem linguae plectro et calamo #scrib.|e (#scrib.|ae, scribendam {var. l.}) accedamus."
         * WALTH. SPIR. Christoph. II; praef. 115 "ne te {(sc. lectorem)} modici bellaria #scrib.|ae offendant."; 1,258 "nos, Christofore, placido si conspicis ore atque tuos #scrib.|as vultu ridente serenas {eqs.}"
         * METELL. exp. Hieros.; 6,22 "Salem dictus locus est, ut lege peritus #scrib.|a vetustarum Iosephus probat historiarum."      

AUTORIN Weber