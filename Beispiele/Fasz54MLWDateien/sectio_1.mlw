LEMMA 1. sectio

GRAMMATIK
   subst. III; -onis f.

SCHREIBWEISE
   script.:
      seti(o): {=> sectio_4}
      sexi(o): * CONC. Merov.; p. 82,15 {(var. l.)}
      secci(o): {=> sectio_1}

METRIK
   metr.:
      -iŏ: {=> sectio_5}

VERWECHSELBAR
    confunditur c.:
      secutio: {=> sectio_2} {=> sectio_3}

BEDEUTUNG
   actio (scalpro, cultro) incidendi, (con-, ex-, in)secandi -- das (Ab-, Auf-, Be-, Ein-, Zer-)Schneiden, Zerstückeln, Spalten, (Ab-)Schlagen, (Ein-)Schnitt:

U_BEDEUTUNG
   proprie:

UU_BEDEUTUNG
    gener.:

UUU_BEDEUTUNG 
    in univ.:

UUUU_BEDEUTUNG
    usu communi; c. notione necandi: {=> sectio_13}:

    * EPIST. Wisig.; 9 p. 674,11 "granum sinapis nequaquam recipit #section.|em."

    * WALAHFR. Gall.; 2,26 p. 880,17 "cute, quae oculis supercrevit, velut gladii #section.|e recisa ... de luminis amissi restitutione gavisus est {(sc. aegrotus)}."

    * THANGM. Bernw.; 9 "cum ex tribus particulis sancti ligni quartam ... excidere temptaret ... nec gracilitas vel parvitas ... #section.|em admitteret, ... ecce {eqs.}"

    * ALBERT. AQUENS. hist.; 7,24 p. 518,21 "Wicherus Alemannus in ictu gladii et Turci loricati #section.|e {@ sectio_13} laudabilis."

    * ALBERT. M. veget.; 5,41 "cum ... secatur {(sc. planta)} vel inciditur, tunc ... in ipsa #sectione.|e totum corpus plantae concutitur."
    {al.}

UUUU_BEDEUTUNG
   spectat ad circumcisionem:

   * LEG. Wisig.; 12,2,7 "non servus, non ingenuus aut libertus ... eiusdem detestande #section.|is (sanctionis, #set|.ionis {@ sectio_4}, sectationis, secta, factionis {var. l.}; {i. Iudaeorum circumcisionis}) obprobrium ... perferet aut alteri inferre presumat."

UUUU_BEDEUTUNG
    spectat ad supplicium, martyrium sim.:

    * ARBEO Emm.; 17 p. 50,12 "quinque electi sunt, qui eius membra #sectioni.|bus absciderentur ((*; {sim.} 30 p. 70,24 "membrorum #section.|em passus fuerat"))."

    * DIPL. Conr. II.; 201 p. 270,35 "quę {(sancta Maxellendis)} ... propter inextorquibilem sui celibatus pudicitiam ... capitis #section.|e (#section.|em {var. l.}) martirii consecuta est palmam."

    * PASS. Thiem. I; 243 "omnis membrorum flexura vel articulorum est incisa sibi ...; a manibus sursum cepit pedibusque deorsum #secti.|o {@ sectio_5} feralis."

    * EPIST. Hild.; 1 p. 55,7 "quisquis ... scripto huic contrarius esse presumpserit, #section.|e membrorum dampnabitur."

    {al.}
  

UUU_BEDEUTUNG
   c. colore quodam:

UUUU_BEDEUTUNG
    actio (de)metendi -- das Mähen, Mahd; de iure: {=> sectio_6}:

    *EPIST. var. I; 13 p. 512,7 "cum ... tempus #section.|is foeni est {eqs.}"

    *TRAD. Ratisb.; 257 "absque ... foeni ... #sectioni.|bus {@ sectio_6}."

    *CHART. Const.; 36 p. 42,16 "cives proponebant se habere quoddam ius in prato eorum, ... videlicet, ut post primam graminum #section.|em ipsorum ibidem animalia pascerentur."



UUUU_BEDEUTUNG
    actio caedendi, succidendi -- das Fällen; de iure: {=> sectio_7} {=> sectio_8}:

    *CHART. Sigeb.; 80 (a. 1205) "didicimus {(sc. archiepiscopus)} ecclesiam ... arborum #section.|em {@ sectio_7} et glandium pastionem ... in silva nostra ... optinuisse."

    *CHART. Xant.; 119 p. 90,7 "cum ecclesia ... movisset ... questionem super quibusdam nemoribus ... et pastura porcorum, lignorum #section.|e {@ sectio_8} et piscatura."

    *CHART. Rhen. inf. II; 694 p. 506,9 "dominus de Montfort iudicabit de omnibus, que fiunt in dicto nemore per indebitam #section.|em, secundum iusticiam et gratiam suam."

UUUU_BEDEUTUNG
    ?laesio, vulneratio -- ?Verletzung, Verwundung:

    * ANNAL. Magd.; a. 1140 "obiit Lothowigus rex Franciae relinquens duos filios ..., quorum alter ... regnum obtinuit, alter {(sc. Philippus)} vero <<per #section.|em>> ({ci. ed.,} perfectionem {cod.}) vitalium vitam finivit {(v. notam ed.)}."


UU_BEDEUTUNG
   medic.:

UUU_BEDEUTUNG
   in univ.:

   *GLOSS. med.; p. 24,1 "dierese, id est #section.|es."

   *VITA Heinr. II; A 11 p. 280,10 "obdormiens vidit sanctum Benedictum sibi assistere et ferrum sectorium {@ sectorius} ad medicinales #section.|es aptatum manu tenere."

   *BRUNUS LONG. chirurg.; praef. p. 105^{C} "quae {(operatio chirurgica)} fit in membris mollibus, subdividitur tripliciter: ... vel in pulsatilibus {(sc. venis fit)} et dicitur #secti.|o {eqs.}"

   *; 1,17 p. 115^{B} "ut fiant #section.|es duae secundum figuram crucis." {ibid. al.}

   *THEOD. CERV. chirurg.; 1,4 p. 136^{C} "si vulnus fuerit #secti.|o, de quo ... nihil cecidit, sufficit strictura {eqs}."

UUU_BEDEUTUNG
   actio scalpello secandi, operatio chirurgica -- Operation; de incisione post mortem facta: {=> sectio_9}:

   *ANTIDOT. Bamb. app.; p. 37,35 "ypnoticum adiutorium, id est somnificum conveniens his, qui cirurgiam operantur aut #section.|es."

   *ANON. anat.; 3sq. "quae {(vivorum anatomia)} fit ductu solo et #section.|e rationis; unde anathomia mortuorum est illa, quae consistit in actu #section.|is {@ sectio_9}."


UUU_BEDEUTUNG
    actio amputandi -- 'Amputation': {v.} {=> seco/sectio}

ABSATZ
U_BEDEUTUNG
   translate:

UU_BEDEUTUNG
   figura corporum intersecantium -- Überschneidung (usu anat.):

   * ALBERT. M. animal.; 1,338 "#secti.|o ... horum {(musculorum)} duorum parium, quam faciunt villi eorum, est secundum angulos rectos."

   * ; 1,358sqq. "#secti.|o ... sic perficitur, quod concava nervorum in puncto #section.|is unum concavum efficiuntur: quia aliter non proveniret aliqua utilitas illius #section.|is in homine."


UU_BEDEUTUNG
   scissura, partitio, divisio -- (Auf-)Spaltung, (Auf-, Unter-)Teilung:


UUU_BEDEUTUNG
   gener.:

   * RATHER. phren.; 7,186 "quidam {(libelli)} continuum, ut opuscula quorumdam, obtinent statum; quidam illorum sensu atque materia continuante dividuam videantur #sectio.|em habere."

   * HONOR. AUGUST. spec.; p. 961^{C} "ab ipso {(Christo)} ... tanta sanitate solidantur {(sc. sancti)}, ut sicut nunc radius solis nullam #section.|em, ita ipsi tunc nullam corporis passionem patiantur."

   * SIGEBOTO Paulin.; 38 p. 928,44 "illis solis competit inhabitatio planicies et latitudo camporum, quos excludit a sanctis vita reproba et detestabilis #secti.|o morum?"


UUU_BEDEUTUNG
   geom.: 

   * GUNZO epist.; 14 p. 48,10 "possunt {(sc. lineae)} ... inveniri, quę nulla societate aliis copulari queant, quas non immerito alogas dicunt, quę in qualitate, quantitate, #section.|e positioneque consistunt."

   * GERB. geom.; 2,3 "linea est longitudo sine latitudine, haecque solum in longitudine sui #section.|em seu divisionem admittit."

UUU_BEDEUTUNG
   math. ('Division'):

   * HROTSV. Sap.; 3,21 "quis {(sc. numerus)} est impariter par? qui non solum unam recipit #section.|em, sicut pariter par, sed etiam et secundam ... vel plures, sed tamen usque ad indivisibilem non perveniet unitatem (({sim.} * ODO CAMER. (?) rhythmimach.; add. h 162sq. "plus quam unam recipit {(sc. impariter par numerus)} #section.|em; ... in eo vero, quod usque ad I #secti.|o illa non ducitur, disconvenit cum pariter par." * RUP. TUIT. off.; 7,627 "imparium et eorum, qui praeter unitatem nullam aliam recipiunt #section.|em, primus est {ternarius} numerorum"))."

   * ABBO FLOR. calc.; 3,49 "unius denarii in XII partibus aequaliter divisi duodecimam partem unciam regulariter appellare malui, ut possit agnosci, quid in #section.|e cuiuslibet rei debeat observari."
   {al.}

UUU_BEDEUTUNG
   publ. et iur.; c. notione alienationis: {=> sectio_11} {=> sectio_12}:

   * VITA Ludow. Pii; 22 p. 618,45 "recitato paterno testamento nihil relictum est paternorum bonorum, quin secundum eius partiretur #section.|em (praeceptionem {var. l.})."

   * CHART. episc. Misn.; 73 p. 70,6 ((epist. papae 1205)) "in ipso {(arbitrio)} approbatur #secti.|o {@ sectio_11} praebendarum, quam reprobat concilium Turonense (({sim.} * CHART. Sil. D; I 139 (epist. papae) "cum Turonensis statuta concilii #section.|em {@ sectio_12} inhibeant prebendarum"))."

   * CHART. Lub.; I 25 "que {(discordia)} inter advocatum, consules et burgenses ... super mortuo ... et #section.|e domus ... vertebatur."

   * CHRON. Reinh.; a. 1205 p. 568,40 "unde ... regni #sec.|cio {@ sectio_1} et voluntatum diversarum sit facta commocio."
   {saepius.}


UUU_BEDEUTUNG
   philos. et theol.:

   * BOVO CORB. Boeth.; 335 "de ... animae #section.|e in duos orbes posset esse aliquanta dubietas."

   * OTTO FRISING. gest.; 1,65 p. 91,12 "caetera ... ab eo {(Deo)} non ex ipsius essentiae #section.|e, sed ex eiusdem bonitatis denominatione bona dicuntur."


UUU_BEDEUTUNG
   log. i. q. distinctio -- Einteilung, Unterscheidung; c. gen. inhaerentiae: {=> sectio_10}:

   * OTTO FRISING. gest.; 1,5 p. 19,13 "divisionis #section.|em {@ sectio_10} quomodo reciperet {forma}, quae omnibus, in quas dividi posset, speciebus caret?"

   * ALBERT. M. metaph.; 7,4,3 p. 371,35 "cavendum est, quod non fiat divisio 'secundum accidens'; 'si' enim 'dividat' sic dicens: pedalium 'aliud album, aliud nigrum, erunt tot' divisiones, quot sunt '#section.|es' ((*$ARIST.; p. 1038^{a},28 "τομαί"))."


ABSATZ
U_BEDEUTUNG
   meton. i. q. pars, particula, membrum -- (An-)Teil, Abschnitt, 'Sektion':

UU_BEDEUTUNG
   anat.:

   * ALBERT. M. animal.; 1,201 "quae {(tela oculi)} distinguit inter secundinam quamdam glacialis et humorem albugineum, ... et haec vocatur a quibusdam #secti.|o tunicae glacialis."
   
   *; 1,362 "haec {(tertia)} ... pars sive ramus in tres dividitur #section.|es, cum separatur ab orbita occuli. "
   {ibid. al.}


UU_BEDEUTUNG
   geom.:

   * PS. BOETH. geom.; 244sq. "si recta linea per aequalia ac per inaequalia secetur, quod sub inaequalibus totius #sectioni.|bus rectilineum continetur, cum eo quadrato, quod ab ea describitur, quae inter utrasque est #section.|es, aequum est ei quadrato, quod describitur ab ea, quae constat ex adiecta atque dimidia."

   * ALBERT. M. metaph.; 2,9 p. 101,59sqq. "'qui' intelligendo lineam 'procedit per infinitum', numquam 'enumerabit #section.|es' ((* $ARIST.; p. 994^{b},25 τομάς)), in quas dividitur ...; qui est alius intelligendi modus quam per #section.|um divisiones."

UU_BEDEUTUNG
   alch.:

   * PS. GERH. CREM. sal. I; 65 "est {stagnum} ex secutione {@ sectio_2} (#section.|e {corr. Ruska p. 19}) Iovis ((*; sal. II; 55 "partibus"))."

   *; 68 "est {plumbum} ex secutione {@ sectio_3} (#section.|e {corr. Ruska p. 19}) Keion ((*; sal. II; 58 p. 74,33 "est plumbum de partibus Saturni"))."

  

AUTORIN
   Orth-Müller