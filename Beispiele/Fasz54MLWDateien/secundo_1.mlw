LEMMA 1. secundo

GRAMMATIK
    verbum I; -avi, -atum, -are

SCHREIBWEISE
    script.:
        secon-: {=> secundo_1}
STRUKTUR
    struct.:
        c. praep.:
            in {c. acc.}: {=> secundo_3}

GEBRAUCH
    partic. usu adi.:
        praes.: {=> secundo_1}
        perf.: {=> secundatio/secundo_2}


BEDEUTUNG sensu originario:

    UNTER_BEDEUTUNG trans.:

        UU_BEDEUTUNG spectat ad ventos i. q. prosperum, tranquillum facere -- günstig machen, beruhigen {(usu absol.)}:

            * CHRON. Salern.; 175 p. 179,4 "illud Virgilii: 'ventisque secundis', unde et verbum derivatum est: #secund.|o, #secund.|as, #secund.|at, id est tranquillo ... seu prosperum facio vel quietum et suave facio, mitigo, plago, propicium facio."


        UU_BEDEUTUNG spectat ad vota, incepta sim. i. q. fovere, adiuvare -- begünstigen, fördern, unterstützen:

            * UFFING. Ida; 1,19 p. 481,14 "cuius {(mulieris debilis)} ... votis divina medicina adfuit #secund.|andis."

            * VITA Mathild. II; 22 p. 189,13 "Deus #secund.|et vestra incepta, nostra {(sc. imperatoris)} non deerunt auxilia."

            * THIETM. chron.; 6 "hoc {(sc. adnullationis apostasiae)} votum ... Deus omnipotens #secund.|et."

            * ARNULF. delic.; 36 "hosce ({ci. Voigt}, nosce {ed. Huemer}, posce {var. l.; sc. versuum componendorum}) favoriflua ceptus pietate #secund.|a {(sc. Iesus)}."


                ANHÄNGER absol.:

                    * CARM. Bur.; 73,1^b,3 "celum ... Cynthius emundat et terrena #secund.|ante (#sec.|ondante {@ secundo_1} {var. l.}, mediante {ci. Schumann [cf. ed. Schumann, Bischoff, p. 201]}) aere fecundat."

    UNTER_BEDEUTUNG intrans.:

        UU_BEDEUTUNG prodesse -- nützen:

            * COD. Eberh.; II p. 347,12 (a. 1134) "qui superstes {(sc. mariti et uxoris)} fuerit, quod definitum est, ad prefata loca dimidiabit; post cuius finem possessio tota, sicut divisum est, in servicium Dei #secunda.|bit {@ secundo_3}."


        UU_BEDEUTUNG bene proficere, procedere -- gedeihen, Erfolg haben:

            * DIPL. Otton. III.; 319 p. 746,11 "multi ... tradiderunt partem prediorum ...; talibus successibus in manu Dei feliciter #secund.|antibus ... Aribo cępit mente ... tractare {eqs.} ((* {de interpunctione cf.} EPIST. Teg. I; 27 p. 31,3))."



BEDEUTUNG sensu novato:

    UNTER_BEDEUTUNG gener.:

        UU_BEDEUTUNG iterare, renovare -- wiederholen, erneuern:

            * CHART. episc. Hild.; I 684 p. 651,26 (a. 1216) "prius tamen a quibusdam facta, tunc ab omnibus presentibus canonicis est ordinatio #secunda.|ta hoc adiecto, quod {eqs.}"


        UU_BEDEUTUNG iunctura "colloquia #secund.|are" i. q. responsum reddere, respondere -- Antwort geben, antworten:

            * CARM. Bur.; 76,3,3 "cum custode ianue {(sc. templi)} parum requievi; erat virgo nobilis ... #secund.|ans colloquia in sermone levi {(v. comm. ed. Vollmann p. 1038)}."


        UU_BEDEUTUNG loco alterius adhibere, substituere -- anstelle eines anderen, als Ersatz verwenden, substituieren: 

            * DYASC.; p. 104^b "semen {(sc. hyoscyami)} utile est album; quod si minime inventum fuerit, ruffum #secund.|atur, nigrum vero inutile est."


        UU_BEDEUTUNG accedere (ad), imitari -- heranreichen (an), nachahmen:

            * CARM. de exord. Franc.; 8 ((MGPoet. II; p. 141)) "iure ... sis {(sc. Karolus)} decus omne tuis unusque habearis et omnes, hos {(sc. avos armis conspicuos)} referens armis, illos {(sc. avos pietate probos)} pietate #secund.|ans."



    
    UNTER_BEDEUTUNG t. t. math. {@ secundus_1} i. q. in secundam lineam ponere, transferre -- in die zweite Kolonne (ver)setzen: 

        * GERB. epist. math. I; 1,2 p. 21,15 "articulos, a quibus denominationes fiunt multiplicationis, #secunda.|bis ad digitos."

        * HERIG. reg.; 2,11 "quae {(sc. denominationum)} partes, postquam ... proprium obtinuerint locum, #secunda.|buntur sibi in decenis compositis, tertiabuntur in centenis compositis {eqs.}"

        * ANON. minut.; 3,4 p. 243,8 "de CXX medietas sumenda est, quae est VI in denario limite; haec ad denominationem #secund.|etur {eqs.}"

        * PS. BOETH. geom.; 510 "sumpta differentia et primis articulis [vel secundatis] {(interp.)} dividendo appositis {(v. notam ed. et comm. p. 85 adn. 23)}."

        {saepius. v. et {=> secundatio/secundo_2}.}

AUTORIN Niederer