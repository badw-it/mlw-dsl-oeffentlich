LEMMA scotoma

GRAMMATIK
     subst. III; -atis n.

VEL
 *scotomia
GRAMMATIK
     subst. I; -ae f.

ETYMOLOGIE σκότωμα

SCHREIBWEISE
     script.:
         cho-: * PLATEAR. pract.; 5,13 {(var. l.)}
         scomi(a): * PS. GALEN. urin.; p. 19,13
         -tho-: {=> scotoma_1; scotosis/scotoma_6} {adde} * PLATEAR. pract.; 5,13 {(var. l.)} * WALTH. AGIL. med.; 12| p. 111,5sqq.
         -mya: {=> scotoma_2} *{adde} ALBERT. M. animal.; 7,102

FORM
     form. sing:
         nom.:
             -mis: {=> scotosis/scotoma_6}
         acc. decl. II.: {=> scotoma_3}

BEDEUTUNG t. t. medic. i. q. caligo (oculorum), vertigo -- Dunkelheit vor den Augen, Schwindel(anfall):
     U_BEDEUTUNG spectat ad homines:
         * ANTIDOT. Bamb.; 10 "ad #scoto.|mum {@ scotoma_3} capitis vel ad capitis dolore."
         * ARNOLD. RATISB. Emm.; 2 p. 1035^A "tam gravem sub vitio pituitae vel #scotom.|iae incidit tentationem, quo ... oculorum suorum penitus amiserit lumen."
         * PS. ODO MAGD. herb.; 83 "obtenebrat {bryonia} oculos, quod saepe #scoto.|ma."
         * MAURUS urin. I; p. 29,41 "urina alba ... quasi cum quadam parva viriditate significat #scotom.|iam."
         * WALTH. AGIL. med.; 12 p. 111,8 "significatio #scot.|homie {@ scotoma_1} est, quod {eqs.}"
         {al.} {v. et {=> scotomaticus/scotoma_4; scotosis/scotoma_5}}
     U_BEDEUTUNG spectat ad animalia:
         * ALBERT. M. animal.; 4,79 "postquam terrentur {(sc. delphini)} sono ictorum illorum {(lignorum)}, cadunt aliquando, sicut patiantur #scotom.|yam {@ scotoma_2} aut epilensiam."; 7,106 "alia ... duplex aegritudo porcis accidit frequentius, dolor et ponderositas capitis sive #scotom.|ia." {ibid. al.}

AUTORIN Weber