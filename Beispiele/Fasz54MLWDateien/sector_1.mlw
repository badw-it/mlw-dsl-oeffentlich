LEMMA 1. sector

GRAMMATIK
   verbum I; -atus sum, -ari

VEL secto

GRAMMATIK
   verbum I; -avi, -atum, are

GEBRAUCH
   partic. praes. usu subst.: {=> sector_1}

BEDEUTUNG
   (per)sequi -- (beharrlich) folgen:

U_BEDEUTUNG
   proprie:

UU_BEDEUTUNG
   gener.:

   * WALTHARIUS; 357 "cursus ambage recurvos #sect.|antes tremulos variant {(sc. Waltharius et Hiltgunt)} per devia gressus."

UU_BEDEUTUNG
   mus. i. q. comitari, prosequi -- begleiten, zeitgleich folgen:

   * CARM. de mus.; 363 p. 114^b "D #sect.|atur G."


U_BEDEUTUNG
   in imag. et translate:

UU_BEDEUTUNG
   in univ.:

   * GODESC. SAXO theol.; 13 p. 236,15 "sponte #sect.|or et amplector, ubicumque verum dixit {(sc. Hieronymus)}, et e contrario ... praetereo, ubi sicut homo aliena dictavit."

   * DIPL. Conr. III.; 109 p. 196,35 "mores insuper et vestigia nostrorum predecessorum #sect.|antes {eqs}."

UU_BEDEUTUNG
   consectari, aemulari -- eifrig anhängen, nacheifern:

   * HUGEB. Wynneb.; 13 p. 117,35 "sanctum #sect.|ate mente et moribus, exempla imitate."

   * WETT. Gall.; 1 "humilitate ... pollebat magistrum suum in studiis divinis #sect.|ans."

   * DIPL. Otton. II.; 192 p. 219,28 "nos praedecessorum nostrorum religiosam devotionem #sect.|ari studentes ... ecclesias ... pręaugmentare cogitamus."

   * OTTO FRISING. chron.; 4,10 p. 196,30 "Iulianus ex subtilitate ingenii philosophos ... #sect.|are (#sect.|ari {var. l.}) volens in tantam super se levatur vesaniam, ut {eqs.}"
   {saepe.}

UU_BEDEUTUNG
    tenere, observare, obtemperare -- sich halten (an), Folge leisten, befolgen:

    * WALAHFR. carm.; 25^a,18,2 "Christe, da nobis tua iussa vero corde #sect.|ari."

    *; Mamm. 11,15 "quod didici, #secta.|bor."

    * RADBERT. corp. Dom.; 8,255 "etsi medicina est #sect.|anda, tamen {eqs.}"

    * DIPL. Karoli III.; 47 p. 77,37 "solet imperialis maiestas {(ci., {/maiestastas} ed., A, {/magestas} B)} praedecessorum regum ... decreta ... diligenter scrutari et ea, quae congrua visa sunt, libenter #sect.|ari."
    {saepe.}

UU_BEDEUTUNG
   (ap)petere, operam dare, se dedere -- trachten nach, nachgehen:

   * CONC. Karol. A; 38 app. B p. 298,12 "ut sacerdotes ... ioca inlicita atque ebrietatem non #sect.|entur."

   * HROTSV. Pafn.; 7,4 "frivola, quae #secta.|batur {Thais}, odiendo refugit."

   * MANEG. c. Wolfh.; 9 p. 62,23 "qui {(poetae)} ... figmentis et immodestis laudibus animas vana #sect.|antium {@ sector_1} oblectati sunt."

   * OTTO FRISING. chron.; 7,23 p. 347,7 "alii ... amore pecuniae ... plus quam iusticiae pacem eum {(Rogerium)} #sect.|ari dicunt."
   {al.}

AUTORIN
   Orth-Müller