LEMMA segnities (-cies)

GRAMMATIK
     subst. III; -ei

VEL segnitia
 GRAMMATIK
     subst. I; -ae f.

SCHREIBWEISE
     script.:
         sig-: {=> segnities_1} *{adde} IONAS BOB. Columb.; 1,17 p. 182,8 {(var. l.)} {al.}
         sen-: * PETR. DAM. epist.; 110 p. 228,28 {(var. l.)} {al.}

BEDEUTUNG ignavia, inertia, tarditas, desidia, neglegentia -- Trägheit, Untätigkeit, Langsamkeit, Säumigkeit, Nachlässigkeit; spectat tam ad mentem quam ad corpus:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG spectat ad homines:
             UUU_BEDEUTUNG in univ.:
                 * CARM. var. I; 9,2,21 "tolle {(sc. lector)} hic #segniti.|em, pone fastidia mentis, ... doctior hinc redies." 
                 * CHART. Stab.; 78 p. 178,4 "propter longinquitatem terre et lingue diversitatem seu rectorum #si|.gniti.|em {@ segnities_1} paulatim servitium ... abnegare ceperunt {(sc. villae habitatores)}."
                 * MIRAC. Bav.; 1,2 p. 591,47 "qui mox ut semina divini verbi sedula mente percepit, non ea passus est dampnosa #segniti.|a (#segniti.|e {var. l.}) praefocari."
                 * EPIST. Mog.; 23 p. 359,9 "cur illius {(sc. sanctae ecclesiae)} increpo senectutem, cum nostram {(sc. episcoporum)} potius increpare deberem #segniti.|em."
                 * ARCHIPOETA; 4,9,4 "scripta sua corrigunt eciam diserti, versus volunt corrigi denuoque verti, ne risum #segni.|cies pariat inerti."
                 * DAVID expos. reg.; 5,6 "multa vitia per otium nutriuntur: #segnitie.|s, verbositas, taedium boni."
                 {persaepe.}
             UUU_BEDEUTUNG c. gen. inhaerentiae:
                 * WOLFHARD. Waldb.; 3,1 p. 258,11 "importuni soporis #segniti.|em mitte {(sc. puella caeca)}."
                 * LAMB. TUIT. Herib.; 2,4 p. 212,24 "abiciens a se torporis et ignavię #segniti.|em adit domum Domini {(v. notam ed.)}."
                 ANHÄNGER abund.:
                     * THEOPH. sched.; 3 prol. p. 63,1 "per spiritum fortitudinis omnem #segnitie.|i torporem excutis."
             UUU_BEDEUTUNG in tautologia q. d.:
                 * MIRAC. Hucberti; 8 p. 822^A "propter vestram {(sc. parentum)} ... desidiam  #segniti.|em_que ac cordis perversitatem hactenus meorum lumine careo {(sc. mulier)} oculorum."
                 * ODO MAGDEB. Ern.; 6,340 "facto mane ... #segniti.|em somnumque oculis dux excutit alta voce vocans socios."
             UUU_BEDEUTUNG torpor -- Betäubung:
                 * WILH. APUL. gest.; 4,116 "his {(sc. Basilachio suisque)} sopor et vini dederat violentia multa #segniti.|em."
            
         UU_BEDEUTUNG spectat ad hiemem:
             * CARM. Bur.; 135,1,4 "cedit, hiems, tua durities ..., torpor et improba #segnitie.|s ({corr. ex} seu vicies, {ut vid.}), pallor et ira, dolor, macies."
     U_BEDEUTUNG meton. de homine:
          * ALCUIN. epist.; 308 p. 471,22 "quamquam vestrae prudentiae ... quaestiones notissimas esse sciamus, tamen, ne senilis taceat #segnitie.|s, quibusdam interrogationum stimulis somnigeram illius socordiam excitare voluistis {(sc. imperator)}."

AUTORIN Weber