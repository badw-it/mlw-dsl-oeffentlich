LEMMA segregatio

GRAMMATIK
       subst. III; -onis f.

BEDEUTUNG separatio, seiunctio -- (Ab-)Trennung, Abspaltung, Absonderung:
     U_BEDEUTUNG proprie:

         UU_BEDEUTUNG gener.:
             UUU_BEDEUTUNG in univ.:
                 * LUP. FERR. epist.; 93,2 p. 91,1 "bonorum societas profectum vobis {(sc. Karolo)} et laudem parit ...; #segregati.|o ... perversorum aeque laudabilis est."
                 * ELIS. SCHON. vis.; 2,30 "seorsum a ceteris ... Petrus et ... Paulus stare videbantur, et interrogavi angelum ..., quid significaret illa #segregati.|o duorum."
             UUU_BEDEUTUNG actio se avertendi, discedendi -- das Sich-Abwenden, Weggehen:
                 * AGOB.; 12,12 l. 36 "sicut ... duriores fiebant Iudęi adversus verbum Domini desinebantque credere, sic paulatim ostensa est et #segregati.|o predicatorum ab eis ((*{cf.} Vulg.; act. 19.9))."
         UU_BEDEUTUNG theol.; de iudicio extremo:
             * HONOR. AUGUST. phys.; 109 "nondum ... factum est iudicium, hoc est #segregati.|o reproborum ab electis."
         UU_BEDEUTUNG eccl.:
             * INNOC. III. registr.; 1,345 p. 516,7 "non ... in uniformitate corporis Christi, quod est ecclesia, sicut in humani corporis constitutione #segregati.|o partium generat sectionem; sectio ... mortem vel deformitatem inducit, immo #segregati.|o in obedientie virtute consistens unitatis est signum."
         UU_BEDEUTUNG canon.; de anathemate:
             * FLOD. hist.; 1,18 p. 104,5 "si ... incorrigibilis contumacie spiritum non deposuerit {princeps} ..., elogium #segregation.|is a corpore Christi ab omnibus ei porrigatur."
         UU_BEDEUTUNG iur.; de separatione bonorum:
             * CHART. Rhen. med.; II app. 12 p. 359,5 (s. XIII.^in.) "in rebus ęcclesię retrodictis a fratribus ita fuerit segregatus {(sc. praepositus)}, ut sua tercia pars ipsi et reliqui duę illis libere in cunctis usibus subservirent; quę #segregati.|o tam arta, ne quasi quedam alienatio prelato dedecorosa forte videretur in posterum."
         UU_BEDEUTUNG mus. i. q. caesura, incisio -- Einschnitt, Zäsur:
             * AUREL. REOM. mus.; 19,19 "non consideratur numerus syllabarum, utrum in prima an in secunda efficiatur melodia, sed #segregation.|es partium orationis et correptiones."
     
     U_BEDEUTUNG meton.:
         UU_BEDEUTUNG de monachis indisciplinatis; in lusu verborum:
             * RICHER. SENON. gest.; 2,17 p. 278,23 "tali ... congregacione, immo certe #segregation.|e, hec nostra Senoniensis ecclesia pluribus ... annis fuit frequentata."
         UU_BEDEUTUNG divisio (crinium) singulariter facta -- jeweils gezogener Scheitel:
             * GLOSS. Roger. I B; 2,10 p. 611,4 "post inunctionem per discrimina, id est #segregation.|es, capillorum pulverem istum superpone, qui sic fit: {eqs.}"

ABSATZ
BEDEUTUNG divisio, partitio, discretio -- (Auf-)Teilung, Aufspaltung, Scheidung:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG natur. et philos.:
             * HONOR. AUGUST. phys.; 53 l. 25 "sicut multorum luminarium lux simul est coniuncta, ut nulla in eis sit commixtio, nulla #segregati.|o."; 207 l. 13 "fallitur ... fractum in aqua remum ... seu aliud quid per divaricationem sensus, hoc est per radiorum, qui ex pupillis oculorum sparguntur, #segregation.|em geminatum existimans."
             * ALBERT. M. gener.; 2,3,13 p. 213,20 "ea, quae substantiae est 'generatio', non est determinata' secundum veritatem fieri 'congregatione' tali et '#segregation.|e' ((*$ARIST.; p. 317^a,18 "διακρίσει"))."; veget. 1,57 "alterationem quandam esse dicentes {(sc. philosophi)} corruptionem in #segregation.|e atomorum, sicut fecerunt Democritus et Leucippus."; metaph.; 1,3,8 p. 38,13 "dixit {Anaxagoras} 'generationem et corruptionem' fieri 'congregatione et #segregation.|e solum' harum materiarum, quae sunt similium partium."
             {ibid. saepe.}
         UU_BEDEUTUNG theol.; de iudicio extremo, in imag.:
                 * BERNH. CONST. can.; 41 p. 511,10 "ubi {(sc. infra retia)} pisces boni cum malis usque ad #segregation.|em, quae futura est in litore, hoc est in fine saeculi, aequo animo tolerentur
                  ((*{spectat ad} $VULG.; Matth. 13,47sq.))."

     U_BEDEUTUNG translate:
         UU_BEDEUTUNG controversia, lis -- Auseinandersetzung, Streit:
             * CHART. Eichsf.; 552 (a. 1275) "omnis summa #segregation.|is promissa per dominum Reinhardum prespositum ... est ... persoluta nobis {(sc. conventui)}."
         UU_BEDEUTUNG differentia -- Unterschied:
             * AUREL. REOM. mus.; 19,7 "non sursum tantum erigitur {(sc. modulatio authenti proti)}, quantum in autentu deutero nec iosum tantum deprimitur quantum in autentu tetrardo, et idcirco ibidem eorum cernitur #segregati.|o."

AUTORIN Weber