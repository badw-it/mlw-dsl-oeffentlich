LEMMA 1. securus

GRAMMATIK adi. I; -a, -um

SCHREIBWEISE
  script.:
    -cc-:  {=> seccurus}
    -uorus:  {=> secuorus}

GEBRAUCH
  usu praed.: passim, e. g. {=> securus_praed_1} {=> securus_praed_2} {=> securus_praed_3} {=> securus_praed_4}

BEDEUTUNG adi.:

U_BEDEUTUNG expers sollicitudinis -- frei von Sorge, unbesorgt, ruhig:

UU_BEDEUTUNG incuriosus, animaequus, (con)fidens --  sorglos, unbekümmert, zuversichtlich; {in malam partem i. q.} incautus -- unvorsichtig, leichtsinnig:

UUU_BEDEUTUNG in univ.:
* HUGEB. Willib.; 5 p. 104,20  "summus ... pontifex ... illum ... sine sollicitudinis ambiguitate #secur.|um cum suae iussionis licentia oboedientialiter pergere precipit."
* LIUTG. Greg.; 9  "qui {(fratres uterini)} ... iuvenili audacia amplius #secur.|i, quam oporteret, in quadam silva ... occisi sunt."
* WALAHFR. imag. Tetr.; 17  "mens #secur.|a, procul furibundae crapula curae."
* RADBERT. corp. Dom.; 21,233  "de pane illo et calice #secur.|us bibe."
* CONR. HIRS. mund. cont.; 715  "humilis mens ... in iniuriis #secur.|a, de pace sollicita."
* IORDAN. SAXO epist. II; p. 183,12  "ad aspera non remissus, ad adversa #secur.|us {(sc. frater)}."
{al.}

ANHÄNGER usu subst.:
* WIDUK. gest.; 1,11 p. 19,8  "ad #secur.|os ibimus, ad caedem tantum, non ad pugnam."
* ; 2,3 p. 69,23  "super inprovisos ac recenti victoria #secur.|os ... irruit {(sc. Bolizlavus)}."

UUU_BEDEUTUNG c. gen. vel abl. vel praep. "de" ((usu subst.: {=> subst})):
* LIUTG. Greg.; 15  "pater ... Gregorius ... de se ipso #secur.|us et de filio sollicitus."
* ANNAL. Fuld. II; a. 863 p. 57,5  "Karlmannus ... de obiectis sibi criminibus #secur.|us, quia innocens erat."
* WALTH. SPIR. Christoph. I; 4  "ipse ... viam est exultationis ingressus eundi ... labore #secur.|us."
* THIETM. chron.; 6,12  "nuncii ... rem ... pandunt Bolizlavo de tali periculo prius #secur.|o."
* EUPOLEMIUS; 1,586  "cunctis ... belli ... #secur.|is {@ subst}."
* EUPOLEMIUS; 2,325  "hostiles ... catervas qui {(rex)} poscat pugne #secur.|as."

ANHÄNGER remissius i. q. expers -- frei (von):
* HROTSV. Dulc.; 1,2  "esto #secur.|us curarum (({sim.} * Theoph.; 66))."


UU_BEDEUTUNG certus -- gewiss, überzeugt:

UUU_BEDEUTUNG c. praep. "de":
* LEX Ribv.; 32  "unde dominus eius {(servi)} de fiducia #secur.|us esse possit."
* ORD. Cas. I; 3  "nisi iubeat prior alicui doctiori minus doctis fratribus ... lectionem tradere, de cuius moribus #secur.|us sit."
* RUD. FULD. Leob.; 12 p. 127,31  "mater venerabilis de eius {(sc. Agathae)} puritate ...  #secur.|a."
* RIMB. Anscar.; 3 p. 23,35  "tristis factus, quia ad saeculum redire compellebar, sed de revertendi promissione #secur.|us."
* RUOTG. Brun.; 41 p. 44,5  "de cuius {(Godefridi ducis)} innocentia ... #secur.|us ... pater Bruno."
* CHART. Tirol. notar.; II 427^a  "ipse dominus {(sc. Ulricus)} dixit, quod de hoc {(sc. promissione Hainrici)} satis #seccurus {@ seccurus} esset."
{al.}

UUU_BEDEUTUNG c. enunt. vel inf.:
* CAPIT. reg. Franc.; 281,4 p. 356,22  "quomodo #secur.|i esse possimus, quousque {eqs.} (({sim.} * ; 281,5 "quomodo #secur.|i sumus, quatinus {eqs.}"))."
* CONSUET. Marb.; 206  "ut nec locus sit nec hora, in qua frater ullus #secur.|us esse possit ... non deprehendi et non publicari."


UU_BEDEUTUNG fortis, animosus, probus -- beherzt, mutig, tapfer:
* WALAHFR. carm.; 5,17,11  "si ... ad ventum licuisset flectere remum, #secur.|o eximerem pectore me pelago."
* HROTSV. Gall.; I 4,2  "perge #secur.|us {@ securus_praed_1}, Gallicane, ad bellum."
* WALTHARIUS; 162  "ad quaecumque iubes, #secur.|us et ibo paratus."
* OTTO FRISING. gest.; 1,20 p. 34,11  "Fridericus suos de turre adventare prospiciens iam #secur.|us factus Heinricum ducem exposcit."
{al.}

ANHÄNGER usu subst.:
* OTTO FRISING. gest.; 2,30 p. 138,9  "implorasti misera {(sc. urbs Roma)} felicem {(sc. Fridericum regem)}, debilis fortem, invalida validum, anxia #secur.|um."


UU_BEDEUTUNG fidus, fidelis, diligens -- zuverlässig, verlässlich, gewissenhaft:
* PRAECEPT. diaet.; III 19  "in mense ... Aprili mittis in hac potione ... absinthium ... et bibe #secur.|us {@ securus_praed_2} per totum mensem."
* THIETM. chron.; 4,1  "maxima pars procerum ... consensit, quod ... #secur.|a novo regi serviret."
* CONSUET. Marb.; 208  "eligenda est ... ex fratribus quędam #secur.|a et honesta persona, vitę probabilis frater."
* ROD. GLAB. hist.; 1,12  "iussit {(sc. Otto imperator)} comprehendere illum male #secur.|um pontificem."
* CHART. Dortm.; 77 p. 32,12  "si hospes ... non est multum #secur.|us et ydoneus ad conservandum prefata bona {eqs.}"
* CHART. Stir.; IV 261 p. 158,39  "nullus exeat extra septa claustri exceptis officialibus sine speciali licencia, et hoc cum fratre maturo et #secur.|o."
{al.}

ABSATZ

U_BEDEUTUNG expers periculi -- frei von Gefahr, gefahrlos, sicher:

UU_BEDEUTUNG tutus, praemunitus -- geschützt, gesichert:

UUU_BEDEUTUNG proprie:

UUUU_BEDEUTUNG in univ.:
* PASS. Quir. Teg.; 8 p. 14,13  "de Romanorum metu #secur.|i esse coeperunt {(sc. Adalpertus et Otkarius)}."
* FROUM. carm.; 11,31  "nos in tuta positi convalle ... #secur.|i."
* THIETM. chron.; 6,4  "qui {(hostes)} post tergum, ut opinabantur, #secur.|i."
* DIPL. Constantiae; 27 p. 85,9  "iubemus, ut, quicumque confugerit infra septa et terminos constitutos ... monasterii ..., #secur.|us sit, et nemo eum capere ... presumat (({sim.} * ; 57 p. 179,30))."
* CONST. imp. II; 418,2  "persona comitis et omnium supradictorum debent esse salve et #secur.|e per manus Romane ecclesie."

ANHÄNGER c. gen. obi.:
* PETR. DAM. epist.; 50 p. 97,50  "absentibus arbitris quanto repraehensionis #secur.|iores sunt {(sc. fratres)}, tanto, quicquid linguae confluxerit, liberius fundunt."

UUUU_BEDEUTUNG spectat ad meatum vel iter:
* CAPIT. reg. Franc.; 233,18  "ut legatarii ... #secur.|i ad propria redeant."
* THIETM. chron.; 6,6  "regi Heinrico #secur.|um fuga {(sc. Longobardorum)} patefecit ingressum."
* BERTH. chron. B; a. 1079 | p. 350,16  "qui {(legati)} legatos apostolicos ... in Teutonicam patriam #secur.|os ducere possent."
* OTTO SANBLAS. chron.; 40 p. 64,13  "pagani ... Ierusalem ... inexpugnabilem reddiderunt dato christianis #secur.|issimo conductu visendi sepulchrum Dominicum."
* CHART. Argent.; II 38  "liberum et #secur.|um accessum ad vos {(sc. civitatem Argentinensem)} nobis {(sc. Gotfrido abbati)} dignemini ... conferre."
{al.}

UUUU_BEDEUTUNG spectat ad loca:
* WIDUK. gest.; 1,10  "urbe ex pace promissa #secur.|iore reddita." {al.}
* CHART. Mog. A; II 433 p. 702,13  "ego Bonifacius ... iuro, quod ... personam domini archiepiscopi comiti Aldeprandino dabo in loco ei #secur.|o."
* CHART. Raitenh.; 67 p. 65,28  "quam {(domum xenodochii)} ... ordini ... ad ... #secur.|am ibidem mansionem ... M(eingotus) ... tradidit."
* CHART. Westph.; VI 781  "Mindam vel aliud opidum, quod michi ipsi {(sc. Ludolpho)} #secur.|um decreverit {(sc. Lothewicus)}, intrabo."

ANHÄNGER c. gen. obi.:
* HROTSV. prim.; 180  "ut terrenorum sit #secur.|um {(sc. coenobium)} dominorum."

UUU_BEDEUTUNG in imag.:
* WALAHFR. Mamm.; 12,11  "ipse ... mentis #secur.|us in arce."


UU_BEDEUTUNG ratus -- garantiert, verbürgt, (rechtlich) abgesichert:

UUU_BEDEUTUNG gener.:
* FRID. II. IMP. epist. C; 3 p. 102,12  "ut, quicumque ... ad regni nostri venire voluerint incolatum, ... libere veniant, eius #secur.|a fertilitate vescantur."

UUU_BEDEUTUNG publ. et iur.:

UUUU_BEDEUTUNG in univ.:
* LEX Sal. Merov.; 45,4  "si ... quis migraverit ..., ubi admigravit, #secur.|us, sicut et alii vicini <manent, ille> maneat."
* DIPL. Merov.; 113 p. 291,25  "decet regalem clementiam ... regimina ecclesiarum et monasteriorum ... sollerti cura providere, ut pax ecclesiarum et #secur.|a {!libertas} monachorum ... conservetur ((* CONC. Karol. A; 16 p. 101,11  "ut hi, qui in aecclesia libertatem conquirebant, ... in #secur.|a libertate permaneant."))."
* DIPL. Constantiae; 52 p. 168,6  "ad ampliorem ... fidem et #secur.|am confirmacionem ... hoc scriptum sigillavimus."

UUUU_BEDEUTUNG spectat ad possessionem:
* LEG. Burgund. const. I; 42,1  "ut mulier ... tertiam totius substantiae mariti ... #secur.|a possideat."
* TRAD. Frising.; 60  "ut #secur.|a permanere in perpetuum deberet {(sc. alodis)} sub huius ditioni tituli Sancti Martini."
* BERTH. chron. B; a. 1079 p. 356,13  "ut ... episcopis ... #secur.|um pacificumque ipsis {(rebus)} utendis et possidendis ingressum et arbitrium concederet {(sc. Heinricus rex)}."
* CHART. Tirol. notar.; I 327^d  "ut {Viola} esset #secur.|a de sua dote."
{al.}

UUUU_BEDEUTUNG iunctura "#secur.|um facere, reddere" i. q. indemnem facere, confirmatione afficere -- schadlos halten, (rechtlich) absichern, mit einer Garantie versehen:
* CAPIT. reg. Franc.; 264,5 (a. 865)  "mandat {(sc. Karolus rex)} vobis {(sc. Francis et Aquitanis)}, quia ... #secur.|os ... vos faciet ex sua parte, quod {eqs.}"
* EPIST. Reinh.; 13 p. 14,2  "ut ipse {(sc. frater Wi.)} in custodia pro fratre retentus exactores pecunie repetende #secur.|os redderet."
* CHART. Lux.; II 45 p. 57,28sqq.  "quod sororem meam {(sc. marchionis)} ... de tenuris suis salvandis bene #secur.|am facient ... comes et comitissa; ... coniugem meam bene #secur.|am facient de dotalicio suo bene salvando; ... Balduinum de Lobiis ... bene #secur.|um facient, quod {eqs.}"
* CHART. episc. Hild.; II 291  "ex parte canonicorum quesitum fuit in sententia, si #secur.|os eos deberet facere {(sc. Hoyerus)} de eo, quod ... ipse de hoc eis prestare deberet debitam warandiam."
{al.}


UU_BEDEUTUNG imperturbatus, inoffensus, liber, (ab)solutus -- ungestört, unbehelligt, ungehindert, frei:

UUU_BEDEUTUNG gener.:
* EIGIL Sturm.; 5  "ubi {(sc. in eremi habitatione)} ... possent {(sc. Sturmi cum duobus fratribus)} iugiter Domino #secur.|i {@ securus_praed_3} deservire."
* HRABAN. epist.; 30 p. 449,1  "nec ... licuit #secur.|ae lectioni insistere, quando ipse animus sentiebat se in multas partes esse divisum."
* DIPL. Otton. II.; 25  "ut homines ... necessaria servitia providentes ... #secur.|i inde discedant."
* DIPL. Constantiae; 11 p. 37,42  "#secur.|a tranquillitas et pax sancta fovetur in populis."
* OTTO FRISING. gest.; 2,25 p. 128,11  "non #secur.|i in lectulis, non in oratorio inperculsi animum ... colligere ... non valemus."
* CHART. Tirol. notar.; II 407  "ut ipsi {(sc. Otto et Rempretus)} sint ab eo {(sc. Eberlino)} #secur.|i in omnibus locis."
{al.}

UUU_BEDEUTUNG publ. et iur.:

UUUU_BEDEUTUNG spectat ad manumissionem:
* FORM. Sal. Bign.; 1  "ille, qui ... valead permanere {!bene ingenuus} adque #sec.|uorus {@ secuorus} ((* CHART. Sangall. A; 101  "b. ingenui atque #secur.|i." * DIPL. Ludow. Germ.; 121  "b. i. atque #secur.|us"))"
* CHART. Epternac.; 153  "trado {(sc. Buovo)} ... mancipia III ita, ut ... #secur.|i ... cum integra ingenuitate valeant permanere."
* CHART. Turic.; 219 p. 111,12  "Wicharius presbyter ... servum ... nomine Meginwardum tradidit ad altare sanctorum, ut et liber permaneat et #secur.|us, quo vellet, pergat."
{al.}

UUUU_BEDEUTUNG spectat ad vectigalia vel servitia ((c. praep. "ab" vel "de", c. gen.: {=> securus_gen})):
* DIPL. Ludow. Germ.; 124 p. 174,31  "ut #secur.|i essent {(sc. homines)} de illo censu."
* TRAD. Ratisb.; 44  "Gundbertum sibi accepit {(sc. Hludowicus rex)}, ut eum ... liberum atque ab opere servili #secur.|um ... efficeret."
* DIPL. Ludow. Iun.; 6  "ab omni vectigalium exquisitione #secur.|i ... permaneant {(sc. monasterii Werdenae fratres)}."
* TRAD. Ratisb.; 93  "tres filios meos ... liberos atque ab omni servitutis humanę vinculo #secur.|os reliqui {(sc. Ratheri)}."
* DIPL. Heinr. III.; 371  "ut eadem {(sc. Epternacensis)} abbatia ... semper libera et #secur.|a {@ securus_gen} tocius regalis servitii ... subsistat."
{al.}

UUUU_BEDEUTUNG spectat ad iudicium:
* LEG. Wisig.; 7,1,5  "si ... innocens adprobatur {(sc. quicumque in crimine accusatur)}, de iudicio #secur.|us {@ securus_praed_4} abscedat." {al.}
* FORM. Andec.; 50^a  "coniurare, quod ad morte sepedicto numquam consentissit ...; se hoc facere potest, diebus vite suae de ipsa causa #secur.|us permaneat; sin autem {eqs.}"
* WALAHFR. Mamm.; 11,4  "iura ... nihil esse nefandae artis apud Mammem, et ... #secur.|us abibis."

UUUU_BEDEUTUNG innocens -- unschuldig:
* REGINO syn. caus.; 2,19  "mater si iuxta focum infantem posuerit et alius homo aquam in caldariam miserit et ebullita aqua infans superfusus mortuus fuerit, pro negligentia mater poeniteat, et ille homo #secur.|us sit (({sim.} * CAPIT. reg. Franc.; 252,37. * DECRET. Burch.; 19,149))."
* CAPIT. reg. Franc.; 252,2 p. 214,12  "presbyter ... eiusdem criminis, propter quod caecus fuerit effectus, liberrimus, ut testatur episcopus, in cuius sancta synodo ... #secur.|us est factus."
* CHART. Babenb.; 183 p. 250,44  "si reus appareat, iudicetur ..., si vero #secur.|us appareat, liber sit a potestate iudicii."

ANHÄNGER usu subst. (abund.):
* CAPIT. reg. Franc.; 252,36  "grave peccatum est ... innocentem opprimere et #secur.|um crimine scienter criminosum habere."

UUU_BEDEUTUNG theol. et liturg.:

UUUU_BEDEUTUNG inculpatus -- unbescholten, schuldlos (spectat ad diem iudicii):
* WALAHFR. Wett.; 442  "licet intercessio purget crimina multorum, tamen hac #secur.|us haberi nemo potest, quia quo nescit sua pondere facta pensentur."
* MEGINH. FULD. Alex.; 5 p. 429,35  "mandans {(sc. Leo papa)} regi ... Hludhario missoque presenti praecipiens, ut illas {(reliquias)} ita honorofice in missarum celebrationibus ... procurarent, sicuti in die iudicii {(ci., "indicii" ed.)} #secur.|i esse voluissent."
* BRUNO QUERF. Adalb. A; 31  "ceteri sancti ... de hoc seculo #secur.|i non recedunt, sed circa extremam horam infinita angustia laborant."
* DIPL. Otton. III.; 291 p. 394,33  "ut ante regem regum #secur.|i veniamus in reddenda racione."

UUUU_BEDEUTUNG liberatus -- befreit (spectat ad exorcismum):
* THIETM. chron.; 8,25  "quem {(maligno arreptum spiritu)} ... facto sanctae crucis signo divina potestate #secur.|um exire fecit {(sc. Suithgerus praesul)}."


UU_BEDEUTUNG inviolatus, intemptatus, incolumis, salvus -- unversehrt, unangetastet, wohlbehalten, heil:

UUU_BEDEUTUNG de anim.:
* WALTH. SPIR. Christoph. II; 5,231  "frustra #secur.|as meditantur retia dammas, nam semel elapsum vitabit belua nodum."
* THIETM. chron.; 2,23  "tales insidiancium laqueos ... Otto ... #secur.|us evasit."
* ; 4,48  "Romani ... omnes inclusos emisere #secur.|os."
* ; 5,11  "Renum #secur.|us enavigat {(sc. Heinricus dux)}."
* VITA Heinr. IV.; 9 p. 32,14  "invisibilem ducem habebat {(sc. imperator)}, qui eum #secur.|um ... ducebat."
{al.}

UUU_BEDEUTUNG de rebus:
* WALAHFR. carm.; 16,9 "#secur.|a tua residere sagitta pharetra cerneris {(sc. Altgerus)}."
* DIPL. Otton. I.; 417 p. 569,18  "cum nostre exigat sublimitatis officium ... ob regnorum status #secur.|os ... evigilare {eqs.}"

ABSATZ

BEDEUTUNG subst.:

U_BEDEUTUNG masc. i. q. commendatus, cliens, satelles -- Anvertrauter, Schutzbefohlener, Gefährte, Begleiter:
* CHART. Friburg.; 235 (a. 1270)  "quod apud fratres recipiantur {(sc. fratres alii)} tamquam conventuales et ipsorum #secur.|i amicabiliter tractentur."

U_BEDEUTUNG fem. i. q. determinatio, circumscriptio -- Begrenzung, (Grenz-)Markierung:
* CHART. Sangall. A; 422 (a. 853)  "quicquid ... in loco, qui cognominatur 'Charbach', de utraque illius fluvioli parte concaptum legitimisque #secur.|arum adnotationibus habeo {(sc. presbyter)} circumdatum."

U_BEDEUTUNG neutr. i. q. assertio, promissio -- Zusicherung, Versprechung:
* ABBO SANGERM. bell.; 2,415  "nobis dederant {(sc. Dani)} tranquillum, Matrona flumen quidquid alit, solito '#secur.|um' quod vocitamus (hoc nostris violare Danos ingens erat horror)."
* ; 2,429  "#secur.|um frangunt {(sc. Dani)}, Senones tempnunt Matronamque aequoreo curru sulcant."


SUB_LEMMA secure VEL securiter

GRAMMATIK adv.

BEDEUTUNG absque sollicitudine -- ohne Sorge, unbesorgt:

U_BEDEUTUNG solute, incuriose -- (sorgen)frei, sorglos, unbekümmert:
* RIMB. Anscar.; 19 p. 3,23  "irruentes {(sc. Dani)} ... super quietos et #secur.|e habitantes improvise."
* WIDUK. gest.; 1,9 p. 14,18  "dux ... urbis circumdatur claustro nec ipsum caelum #secur.|e audet inspicere."
* VITA Burch. Worm.; 12 p. 837,40  "qui #securite.|r (#secur.|e {var. l.}) hodie in deliciis vivit, cras infeliciter morti succumbit."
* IOH. CODAGN. annal.; a. 1200 p. 28,1  "Cremonenses ... invenerunt nostros #securite.|r desarmatos stantes."
{al.}

U_BEDEUTUNG prompte, explorate -- ohne Zögern, freimütig, getrost:
* DIPL. Otton. III.; 405 p. 838,35  "presto eam {(commutationem)} habere et #secur.|e ostendere."
* CHART. cell. Paulin.; 54  "si necesse est idem sacramento confirmare, #secur.|e possumus {(sc. Conradus abbas et capitulum)}."
* IOH. CODAGN. trist.; p. 42,35  "Obertus archiepiscopus et Milus archipresbyter ... preceperunt populo ... et militibus omnibus, ut ... ad bellum procederent; quod #securite.|r facere debebant, quia illud castrum et eiusdem castri habitatores, iura ... et possessiones ad domnum archiepiscopum spectarunt."
{al.}

U_BEDEUTUNG fide, parate, deliberate -- zuverlässig, entschlossen, entschieden, bestimmt:
* GUNTH. PAR. hist.; 8,27  "Theutonicis ... hanc rem #secur.|ius (securiosius {var. l.}) et imperiosius iniungebat {(sc. Philippus rex)}."
* IOH. CODAGN. annal.; a. 1231 p. 107,39  "rogando {(sc. marchio)}, ut locum Clavasii ... firmiter et #securite.|r ... defendere et manutenere debeant {(sc. Papienses et alii)}."
* IOH. CODAGN. gest.; 9 p. 481,17  "ut pedites omnes ... bellum cum eis {(inimicis)} #securite.|r commisceri possent."
{al.}

BEDEUTUNG absque periculo -- ohne Gefahr, gefahrlos:

U_BEDEUTUNG tute, praemunite -- sicher, geschützt:

UU_BEDEUTUNG in univ.:
* PETR. TREV. David; 1 p. 33,10  "regularibus disciplinis instrui preelegit malens nescire #secur.|e quam cum periculo discere."

UU_BEDEUTUNG spectat ad meatum vel iter:
* DIPL. Ludow. Germ.; 24  "liceat, ea {(carra)} ... libere et #secur.|e ire et redire."
* EGBERT. fec. rat.; 1,48  "propter equum magis incedes #secur.|e, viator."
* DIPL. Heinr. III.; 356 p. 484,25  "ut liceat omnibus ... civibus #secur.|e ire et redire ad mercata omnia."
* CONST. Melf.; 2,4 "sub fida custodia bannitus ... #secur.|e redeat ad iustitiam faciendam."
* CHART. Tirol. notar.; I 64  "dederunt {(sc. Iordanus et Bertoldus)} fidanciam Castellanno absenti, ut ... veniat ... #secur.|e pro exeumdo de banno."

U_BEDEUTUNG rate, legitime -- (rechtlich) garantiert, gültig, unangefochten, rechtmäßig (usu iur.): 
* TRAD. Frising.; 605 p. 518,1 (a. 833)  "ut ipsa colonica ... ad opus Sanctae Mariae #securite.|r permansissent sine ulla contradictione."
* DIPL. Ludow. Germ.; 135 p. 188,24  "ut ipse {(comes)} et uxor eius ... diebus vitę illorum illud {(monasterium)} #securite.|r habeant in beneficium (({sim.} * ; 163 p. 227,36 * CHART. Lux.; I 275 p. 398,38  "ut ... bona ... libere et quiete et #secur.|issime perpetualiter habeas {(sc. confessor Symeon)}." {al.}))."
* DIPL. Heinr. III.; 351 p. 478,26  "omne mercatum Ytalicum absque qualibet exactione #secur.|e frequentent {(sc. Ferrarienses)}."
{al.}

U_BEDEUTUNG sine perturbatione, inoffense -- ungestört, unbehelligt, ungehindert:
* DIPL. Ludow. Germ.; 13 p. 15,37  "ut liceret eosdem monachos #secur.|e vivere absque alicuius infestatione."
* DIPL. Karoli III.; 77 p. 126,27  "unde homines ..., qui ... circuminhabitant, ... #securite.|r atque in pace ibi vivere queant."
{al.}

U_BEDEUTUNG inviolate, intacte -- unversehrt, unbeschadet, wohlbehalten:
* VITA Arnulfi; 20 p. 441,5  "nos ... #secur.|e de periculo subacto ... ad lectulos nostros remeavimus."
* BERTH. chron. B; a. 1079 p. 349,9  "ad quam {(synodum)} ... episcopi ... plerisque insidiarum et custodiariorum articulis ... #secur.|e pertransitis ... pervenere."
* HEINR. AUGUST. planct.; 961  "est {(sc. Christus)} adversa leo #securite.|r exuperando."
* STATUT. ord. Teut.; p. 95,16  "ut #secur.|e possit {(sc. electus)} in extremo iudicio consistere."
* IOH. CODAGN. annal.; a. 1213 p. 43,4  "sperantes {(sc. Cremonenses)} ... in ipsa {(planitie)} ... posse #securite.|r stare et permanere."
{al.}


AUTOR Berktold
