LEMMA scolopendria

GRAMMATIK
   subst. I; -ae f.

VEL scolopendrios (-us)

GRAMMATIK
   subst. II; -i m.

ETYMOLOGIE σκολοπένδριον

SCHREIBWEISE
   script. et form.:
     es-: * ANTIDOT. Glasg.; p. 138,32
     c-: * ALPHITA ; C 221
     scal-: {=> scolopendria_2}
     gen. sg. gr.:
        -diu: {=> scolopendria_3}

BEDEUTUNG
   ?de generibus filicis fere i. q. calcifraga, asplenos, adiantum -- etwa: Hirschzungenfarn (Asplenium scolopendrium L.), Milz-, Schriftfarn (Asplenium ceterach L.), Frauenhaarfarn (Adiantum Capillus-Veneris L.), Schwarzstieliger Streifenfarn (Asplenium adiantum nigrum L.):

   *ANON. herm.; p. 103,29 "#scolopendri.|us, id est splenion sive cervi lingua."

   * RECEPT. Lauresh.; 5,1,16 "#scolopen.|diu {@ scolopendria_3} drachmae VIII."

   * HILDEG. phys.; 1,30 "hirzeszunga (latine #scolopendri.|a {addit F}) calida existit ac humida est et iecori ac pulmoni et dolentibus visceribus valet."

   * BERNH. PROV. comm.; 1,1 p. 271,44 "capillus Veneris, epatica, politritum, adyanthos, ceterac, viola, #scolopendri.|a ... et alia iuxta aquas nascencia melius operantur, si in aqua decoquantur."

   * ALBERT. M.  veget.; 6,438 "#scolopendri.|a (#sca|.lopendria {@ scolopendria_2} {var. l.}) apud nos est ea, quae dicitur lingua cervina."

   * ALPHITA; S 89 "#scolopendri.|a, lingua cervina idem."

   {al. v. et {=> p. 229,3. 4.|-}}



AUTORIN
   Orth-Müller