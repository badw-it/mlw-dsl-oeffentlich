LEMMA scrutator

GRAMMATIK subst. III; -oris m.

BEDEUTUNG investigator, indagator, exquisitor, perscrutator -- Untersucher, Ergründer, Erforscher:

  U_BEDEUTUNG de hominibus:

    UU_BEDEUTUNG in univ.:

      UUU_BEDEUTUNG usu communi:
      * DUNGAL. (?) carm.; 20,7,1  "astrorum cupidus quisquis #scrutato.|r." 
      * HRABAN. hom. II; 137 p. 410^{A}  "sophistae mundi et #scrutator.|es siderum."
      * MUS. Enchir.; app. p. 226,78  "antiqua cithara ... a perspicacioribus #scrutatori.|bus in tantum redigeretur ad purum, ut VII forent discrimina vocum."
      * NADDA Cyriac. I; 10,7  "respondit {(sc. Marcellinus)} in Syra lingua unum verbum 'cyriona' fuisse, quod Latinis '#scrutato.|r' vel 'investigator rerum' interpretari congruum foret."
      * RADBOD. Mart.; 7 ((MGScript. XV; p. 1244,2))  "quae michi incerta erant, ... quae omnia nec affirmo nec abnego, sed #scrutatori.|bus importunis inquirenda relinquo."

      UUU_BEDEUTUNG spectat ad res divinas:
      * LIBRI Karol.; 3,6 p. 363,11  "per Danihelem, virum sanctissimum et archanorum Dei #scrutator.|em."
      * HRABAN. epist.; 12 p. 399,35  "esto {(sc. Frechulfus episcopus)} pius #scrutato.|r legis Dei et devotus executor preceptorum altissimi (({sim.} * CONR. EBERB. exord.; 5,9 p. 289,11  "mandatorum Dei #scrutato.|r sedulus et exsecutor devotus {(sc. Petrus)}"))."
      * RUOTG. Brun.; 33 p. 34,6  "sagax eorum, quę sunt Iesu Christi, #scrutato.|r."
      * HUGO HONAUG. (?) ignor.; 12  "caelestia divinitatis occulta ... in intelligendo admittunt #scrutator.|em et investigandi amatorem."
      {al.}

      UUU_BEDEUTUNG perquisitor, frequentator, rimator -- Besucher, Durchstöberer:
      * GODESC. GEMBL. gest.; 75  "bibliothecae assiduus #scrutato.|r {@ scrutato} erat {(sc. Anselmus)}."

    UU_BEDEUTUNG qui sciens est, peritus -- Kenner:
    * CAND. FULD. Eigil. II; 10,55  "Aeigil ... gravis veterum #scrutato.|r avorum."
    * VITA Galli II; 880  "sermonum et sanctorum operum #scrutato.|r opimus."
    * BERNO epist.; 13 p. 42,11  "Amalarius, divinorum officiorum #scrutato.|r non contempnendus."
    * METELL. exp. Hieros.; 6,33  "ait ... summus doctorum, #scrutato.|r et ipse locorum, hanc urbem {(sc. Ierusalem)} non esse Salem, sed eam viciose usu vulgari sic a nostris memorari."
    {al.}

    UU_BEDEUTUNG qui requirit, (prae)eligit, selector -- Aufsucher, Ausleser:
    * ALEX. MIN. apoc.; 21 p. 488,17  "#scrutator.|es malorum ({antea:} mala, quae vident in reprobis ...; bona in malum interpretantur {(sc. detractatores)})."

    UU_BEDEUTUNG qui explorat, observator, speculator -- Ausforscher, Beobachter, Kontrolleur, Kundschafter:
    * GESTA Font.; p. 99,1  "vitiorum per omnia fugax extitit {(sc. Ansegisus)}, aliorum uero neque acerbus reprehensor neque {!curiosus} #scrutato.|r ((* CHART. episc. Wirz. I; 326 p. 368,10  "nos {(sc. Innocentius papa)}, ... qui cunctos Christi fideles ... eo providentie studio gubernari cupimus, ut in eis c. etiam #scrutato.|r nichil inveniat nota dignum"))."
    * FUND. Consecr. Petri; p. 135,20  "rex exhylaratus est ... misitque #scrutatori.|bus {(leg. "-es")} per universos fines Hybernie."

        ANHÄNGER de inspectione aggeris (('Deichgraf', 'Deichrichter')) (({de re v.} F. Petri, Vorträge und Forschungen 18. 1975.; p. 696. 710)):
        * CHART. Traiect.; 740,2 (a. 1226)  "cum ... comes vo<luerit mittere ad perscrutandum predictas slusas> ..., <cum> ... #scruta.|<tores veniunt, si eas> adeo neglec<tas invenerint, quod secundum consuetudinem terre bannos> debeant solvere {eqs.}"

  U_BEDEUTUNG de Deo (({loci fere spectant ad} $VULG.; psalm. 7,10 et al.)):
  * LEO III. epist.; 2 p. 89,25  "est Deus in caelis #scrutato.|r corda et renes {(sic)}, qui scit, qualem amorem et sollicitudinem ... cottidie habemus."
  * OTTO FRISING. chron.; 8,19  "renum cordiumque #scrutator.|is acutissima provisio."
  * EPIST. Laens.; 10 "cum Deo teste, qui #scrutato.|r est cogitacionum cordis."

{saepius.}

AUTOR Berktold