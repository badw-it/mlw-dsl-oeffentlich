LEMMA semiduplus

GRAMMATIK
    adi. I-II; -a, -um

VEL *semiduplex

GRAMMATIK
    adi. III; -icis

GEBRAUCH
    c. praep.:
        ad: {=> semiduplus_1}

BEDEUTUNG t. t. liturg. de festo minus sollemni quam festum duplex, sed sollemniore quam festum simplex i. q. dimidius duplex -- Halbdoppel-, 'Semiduplex-' ((* {de re v.} L. Eisenhofer, Lehrbuch der Liturgik. I. 1932.; p. 589)) ((* LThK. ^{2}II.; p. 1019)):


    * ODO inton.; p. 117^b "sic {(sc. ut dictum est)} debent {psalmi} in duplicibus, #semiduplici.|bus diebus Dominicis, etiam simplicibus et similiter feriis ... decantari."

    * CHART. Argent.; I 271 p. 209,8 "omni festo #semidupl.|o {@ semifestum} et maiori cantabit {camerarius} 'venite' cum prebendario episcopi."

    * CONR. MUR. lib. ordin.; 53 "si in aliquo sabbato adventus Domini festum duplex evenerit, ut Dedicatio, vel #semiduple.|x, ut festum sancti Andree {eqs.} ((* {item} 1517))"; 1527 "duplex festum, immo #semiduple.|x."



BEDEUTUNG usu communi i. q. unus et dimidius, unam et dimidiam partem comprehendens -- anderthalbfach,anderthalbmal so groß (wie):

    * ALBERT. M. phys.; 8,3,4| p. 628,69 "quod linea CB sive BC, quod idem est, sit #semidupl.|a {@ semiduplus_1} ad lineam ZI."; p. 629,16 "quod ... 'ipsum D' citius 'veniet in $I' ex Z per duplam longitudinem, quam 'A' revertatur 'in $C' motum ex B termino finali #semidupl.|ae longitudinis CB."


AUTORIN Niederer