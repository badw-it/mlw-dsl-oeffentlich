 LEMMA *safranum (za-)
GRAMMATIK
       subst. II; -i n.
       -a, -ae f.: {=> safranum_4; safranum_5}

ETYMOLOGIE
       arab.
             za'farān: * {v.} Siggel, Arab.-dt. Wb. der Stoffe.; p. 40^a
             {cf. ital.} zafferano

SCHREIBWEISE
     script. et form.:
             zaphranym: {=> safranum_7}
             zaphir(um): * THADD. FLORENT. cons.; 37,59
             zafar-: * THADD. FLORENT. cons.; 140,7sqq.
             zaffar-: {=> safranum_8}
             zafir(um): * THADD. FLORENT. cons.; 44,139.
             zaframyn: {=> safranum_6}
             sophrana: {=> safranum_5}
             sofferana: {=> safranum_4}
             saffar-: {=> safranum_2}
             saffer-: {=> safranum_3}
             saffia-: {=> safranum_10}

BEDEUTUNG crocum -- Safran:
       U_BEDEUTUNG  proprie (de aromate ex croci sativi flore confecto):
       * CONSTANT. AFRIC. coit.; 16,98 "colora {(sc. mixturam)} cum #saffa|.ran.|o {@ safranum_2}."
       * WOLFGER. itin. comput.; 4,72 p. 94 "pro #safran.|o XLVII sol<idos>
       ((
             *{item}; 6,70 p. 104
       ))."
       * ARNOLD. SAXO flor.; 4,7| p. 83,47 "conficiatur #zaframyn {@ safranum_6} et suspendatur supra mulierem post partum."; p. 84,31 "suspendatur #zaphranym {@ safranum_7} super feminas equarum."
       * THADD. FLORENT. cons.; 32,80 "recipe ... #zaffa|.ran.|i{@ safranum_8}, nucis muscate ... ana drachmam I."
       * GEBER. clar.; 2,18 "accipe ... de #saffiano {@ safranum_10} partem unam." 
       * WILH. SALIC. chirurg.; 1,32 p. 314^D "recipe ... #z|.afran.|i drac<hmas> II."
       * ANNAL. Ceccan.; a. 1196 ((MGScript. XIX; p. 293,50)) "in apparatu ciborum ... media libri piperis et cinnamomi et #soffe|.ran.|ae {@ safranum_4}."; a. 1208| (p. 297,8) "in #sophrana {@ safranum_5}."
       {al.} {v. et {=> safraneus/safranum_11}}

       U_BEDEUTUNG translate :

             UU_BEDEUTUNG de aerugine; usu alch.; 'Eisensafran':
                   * PS. GERH. CREM. sal. I; 23 "si vis, ut remaneat eius {(Mercurii)} tinctura rubea fixa, tere ipsum cum aqua atramenti aut #z|.afran.|i de ferro et aqua salis amoniaci
                   ((
                     *{cf.}; II; 13 "croco ferri"
                   ))."

             UU_BEDEUTUNG color croceus -- goldgelbe Farbe:
                   * PS. CALID. lap.; p. 404 "de hoc phaulet potest fieri #saffe|.ran.|um{@ safranum_3}, qui est color phaulet ad tingendum in solem." 


AUTORIN Weber