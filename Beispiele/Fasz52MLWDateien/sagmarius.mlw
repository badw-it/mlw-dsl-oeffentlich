LEMMA sagmarius

VEL somarius

    (saum-, suma-, summa-, soum-, somma-)

GRAMMATIK 
    adi. I-II; -a, -um

SCHREIBWEISE 

    script. et form.:

        sang-: {=> sagmarius_8} * {adde} * VITA Corb.; 10^{capit.} {(var. l.)}

        saug-: {=> sagmarius_3a; sagmarius_3b; sagmarius_3c} {=> sagma/sagmarius_1a} {al.}

        sacm-: * DIPL. Loth. I.; 85 p. 208,43 {(var. l.)}

        -gum-: {=> sagma/sagmarius_5b} * {adde} CAPIT. reg. Franc.; 143 p. 294,42 {(var. l.)}

        sa(n)gina-: {=> sagmarius_9a; sagmarius_9b; sagmarius_9c} {=> sagmarius_17}  {al.}

        sauͦm-: {=> sagma/sagmarius_1f}
        
        seum-: {=> sagmarius_4}

        sommer-: {=> sagmarius_12}

        somna-: {=> sagmarius_13}

        -mer-: {=> sagmarius_15} {=> sagmarius_11a; sagmarius_11b}
                    * {adde} ANNAL. Ianuens.; III p. 7,12.
                    * ACTA imp. Winkelm.; I 766 p. 607,42.
                    {al.}



BEDEUTUNG adi. i. q. clitellarius, sarcinarius, ad sagma pertinens -- Saum-, Pack-:

    UNTER_BEDEUTUNG de equo: 

        * HIST. de exp. Frid. imp.; p. 70,13 "ubi {(sc. in civitate Rossae)} ... pars ... exercitus ... ob difficultatem viarum currus et plaustra relinquens equos #so|.mari.|os (#sorim|.arios {var. l.}) coepit onerare."
    
    UNTER_BEDEUTUNG de sella:

        * CHART. Argent.; I 616,110 (post 1129) "sellarii episcopo eunti ad curiam duas sellas #sou|.mari.|as (#somnarias {@ sagmarius_13} {cod.}) dabunt, ad expeditionem imperii quatuor."
        {v. et {=> sagma/sagmarius_1b; sagma/sagmarius_1c; sagma/sagmarius_1d}.}

    UNTER_BEDEUTUNG usu vario:

        * OTLOH. Wolfk.; 35 "unus ... de capellanis sub #saug|.mari.|o {@ sagmarius_3a} (#sag|.mario, #sang|.mario {@ sagmarius_8}, #sau|.mario, #sangin|.ario {@ sagmarius_17} {var. l.}) abscondens se filtro {eqs.}"

        * HEINR. TEG. advoc.; 3 p. 278,11 "ossa martiris cum scrinio {@ sagmascrinium} #saug|.mari.|o {@ sagmarius_3b}, ut translata olim fuerant ..., arę subterius inclusa non attigit {(sc. archiepiscopus)}."


BEDEUTUNG subst. masc. vel rarius {({=> sagmarius_18a; sagmarius_18b})} neutr. vel {({=> sagmarius_7} {=> sagma/sagmarius_1g})} fem.:

    UNTER_BEDEUTUNG equus clitellarius, iumentum sarcinarium -- Saum-, Last-, Packpferd, -tier; tam de equis quam de mulis, asinis:

        UU_BEDEUTUNG in univ.:

            UUU_BEDEUTUNG usu vario:

            * GERH. AUGUST. Udalr.; 2,21 "qui {(nuntii)} ... obtulerunt de caera, quantum unus fortis #sou|.mari.|us (#sau|.mari.|us {var. l.}) portare potuit."
   
            * RUODLIEB; V 561 "scutifer ... traxit #sag|.mari.|um variis opibus oneratum."
        
            * GESTA Frid. I. imp. B; p. 88,4 "nisi daret {imperator} ei {(Restagno)} centum #so|.mari.|os (#summ|.arios, #somer|.ios {@ sagmarius_11a} {var. l.}) auri et argenti honeratos ((* {inde} SICARD. chron.; p. 170,16 "#summ|.ari.|os [#summ|.ari.|as {@ sagmarius_7}, #so|.marios {var. l.}]." {al.}))."

            * WILH. RUBRUQU. itin.; 1,8 "utrum vellemus habere bigas cum bobus ad portandum res nostras vel equos pro #sagin|.ari.|is {@ sagmarius_9a} (#summ|.ari.|is {var. l.})." 

            {saepe. v. et {=> sagma/sagmarius_1a}.}

                ANHÄNGER per compar.:

                * VISIO Godesc. A; 2 p. 50,4 "qualibuscumque stipendiis preparatis et proprii dorsi #sag|.mari.|o ({ed. princ.}, #sagin|.ario {@ sagmarius_9b} {codd.}) inpositis iter ... arripuit."
            
            UUU_BEDEUTUNG spectat ad telonea pro quolibet iumento, qualibet sarcina danda:

            * DIPL. Pipp.; 19 "ut ... nullo telloneo ... neque ex navali remigio neque #sau|.mari.|is vel de carrali evectione solvere ... debeant {homines monasterii}."
            
            * DIPL. Ludow. Pii; 32^A p. 84,5 "teloneum de sex navibus ... necnon et de carris et #sag|.mari.|is ((32^B p. 84,32 "#sagin|.ariis {@ sagmarius_9c}")) necessaria ... deferentibus."; 184 p. 457,33 "quandocumque libuisset congregacione ... carros vel #sau|.mar.|ia {@ sagmarius_18a} seu naves ... apetere {eqs.}"

            * CHART. Rhen. med.; II 242 p. 281,22 "{(iura telonei)} de quolibet #sou|.mari.|o ({ci.}, #screiniario {@ sagmarius_16} {ed.}, s<...>imario {cod.}) ((* {cf.} F. Pfeiffer, Rheinische Transitzölle im Mittelalter. 1997.; p. 128 adn. 209)) IIII den. ((* {cf.} DIPL. Heinr. IV.; 487 p. 664,13 [spur. s. XII.^1] "de unoquoque #so|.mari.|o"))."

            {saepe.}


        UU_BEDEUTUNG spectat ad iumenta domino ad vecturam praestanda:
        
            UUU_BEDEUTUNG proprie:
        
            * REGISTR. abb. Werd.; 3,1 "dabit {villicus} ... #so|.mari.|um bonum cum viltro et sella aut pro #so|.mari.|o talentum unum." 

            * CHART. episc. Lub.; 20 p. 25,31 "quod ... a #so|.mari.|is procurandis ... liberi sint {coloni}."

            * ACTA imp. Winkelm.; I 257 p. 235,5 "ut homines ... monasteri cum plaustris et #su|.mari.|is ad usum monachorum ducant et afferant victualia et alias res."

            * CHART. Sangall. B; II 1414 p. 535,19 "dat quelibet colonia pro #sau|.mari.|is V solidos (({sim.} 1421 p. 544,11 "pro #seu|.mari.|o {@ sagmarius_4}." {ibid. al.}))."

            {saepe.}
            

            UUU_BEDEUTUNG meton. i. q. {@ sagmata_5} servitium dominicum cum iumentis sarcinariis praestandum, sagmas portandi -- Frondienst mit Saumtieren, Saumdienst; cf. {=> sagmata/sagmarius_19{sqq.}}:

            * CHART. Mekl.; 348 (a. 1228) "nos {(sc. comes)} villam ... ab exactione qualibet ... solitaque unius equi ad #so|.mari.|um amministratione ... liberam esse concedimus."

            * REGISTR. Mog.; p. 26 "qui {(mansi)} solvunt in festo Martini libras III, ... item ad #so|.mari.|um sol. IX minus tribus obolis {eqs.}" 

           
        UU_BEDEUTUNG spectat ad iumenta in exercitu ducta, impedimentis onerata:

        * BERTH. chron. B; a. 1080 p. 378,4 "ubi {(sc. in statione)} custodes ... #sau|.mari.|os {@ sagmarius_2a}, vehicula et sarcinas ... supellectuariorum ... observabant."

        * HIST. de exp. Frid. imp.; p. 72,3 "relictis bigis et quadrigis iter cum sagmis  et #somm|.ari.|is aggressi (({sim.} p. 84,23 "cum #somm|.ari.|is [#sau|.mari.|is {var. l.}] et sarcinis procedere"))." 

        * CONST. imp. II; 121 p. 161,24 "cum exercitus ... necessaria in #someriis {@ sagmarius_11b} per terram ... ferre nequivisset {eqs.} (({sim.} 122 p. 164,15 "cum #sommeriis {@ sagmarius_12}." * RYCCARD. chron.; a. 1229 p. 158,17 "cum  #sagmeriis {@ sagmarius_15}"))."

        {saepius.}

      
        
    UNTER_BEDEUTUNG sagma, sarcina (in clitellis deportata), gestamen -- (Saum-, Trag-)Sattel, (Saum-)Last, Ladung, Gepäck:

        UU_BEDEUTUNG in univ.:

        * EGBERT. fec. rat.; 1,474 "ęvo fractus equus #sau|.mari.|a {@ sagmarius_18b} ferre recusat."
        {v. et {=> sagma/sagmarius_1g}.}

               
        UU_BEDEUTUNG de magna copia:
                
            * SALIMB. chron.; p. 516,25 "expendit {(sc. papa)} multa milia Florentinorum aureorum in diversis annis, immo multos #so|.mari.|os denariorum aureorum."

        UU_BEDEUTUNG de mensura capacitatis; de re v. {=> sagma/sagmata_3}:

            * TRAD. Teg.; 102^b (a. 1078/91) "advocatus annualiter #saug|.mari.|um {@ sagmarius_3c} olei ad victum fratrum medium mediumque ad luminaria ... concinnanda ... condonavit."

    UNTER_BEDEUTUNG de hominibus:
    
        UU_BEDEUTUNG qui equum clitellarium, iumentum sarcinarium agit, ducit -- Führer eines Saumpferdes, -tieres, 'Säumer':
        
        * CONST. Melf.; 3,55 p. 426,21 "si samararii (somerarii, somererii, sumarerii, somanerii, someterii, #somerii {var. l.}) vel burdonarii vel alii custodes ... animalium ducentes animalia ... in terram alicuius ... diverterint." 

        UU_BEDEUTUNG ?cellerarius -- ?Kellermeister:

        * GISLEB. MONT. chron.; app. p. 341,13sqq. "heres Helvini #summ|.ari.|i et Gonterus Cabos portatores vini et cuiuslibet poculi; heres eiusdem Helvini #summ|.ari.|i panitarius." 


AUTORIN Niederer