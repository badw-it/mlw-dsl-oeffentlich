LEMMA  2. saltus

GRAMMATIK  subst. IV; -us m. vel {({=> saltus011})} f. vel {({=> saltus009})} n.

VEL  saltus

GRAMMATIK  subst. II; -i m.

SCHREIBWEISE

    script.:

        -im: {=> saltus010}

STRUKTUR

    form.:

        acc. pl. decl. III.:
            -ores: {=> saltus007}

        neutr.:
        
            -ora: {=> saltus003}{=> saltus005} {adde:} * TRAD. Frising.; 39 p. 67,33.; 46^a.; 66.
        {al.}
            -ura: {=> saltus008}

        fem. contam. ex decl. II. et III.:

            -oras: {=> saltus006}

BEDEUTUNG  silva (in montibus sita) -- (Berg-, Gebirgs-)Wald:

    U_BEDEUTUNG  strictius:

        UU_BEDEUTUNG  in univ.:

            UUU_BEDEUTUNG  def.:

                * CHART. Stir.; I 95 p. 112,11 (a. 1103) "#salt.|um {(sc. tradidit dux)} ..., qui vulgo vorst dicitur (({sim.} 99 p. 118,18. {ibid. al.} * DIPL. Conr. III.; 365,13))."

                    ANHÄNGER  alleg.:

                        * HRABAN. univ.; 13,5 p. 367^A "#salt.|us vel silva mystice significat sterilitatem gentium (({sim.} 19,5 p. 510^A))."; 19,5 p. 510^B "#salt.|us significat populum ecclesiae, de quo in propheta scriptum est: ...; rursus #salt.|us meridianus typum tenet populi Iudaeorum; unde in Ezechiel dicit: '{eqs.}'"

                        * RUP. TUIT. vict.; 11,13 p. 353,13 "perspice ... gentium, quas per condensa (condensas {var. l.}) #salt.|us intelligimus, tyrannos et duces."

            UUU_BEDEUTUNG  exempla:

                UUUU_BEDEUTUNG  gener.:

                    * COD. Karol.; 11 | p. 505,31 "ut ... territoria, etiam loca et #salt.|ora {@ saltus003} in integro ... restituaere praecipiatis (({sc. Pippinus rex}))(({sim.} p. 506,9. p. 506,27))."

                    * ARBEO Corb.; 20 "ut ad caenubium ire eum permisisset {papa} vel quaedam cellulae deputasset vel #salt.|i {@ saltus001} (#salt.|us, saltim {var. l.}) secretioris et operatione concederet agellum."

                    * VITA Desid. Cad.; 33 p. 590,21 "inter condensas #salt.|us {@ saltus011} residens {Lellus}."

                    * COD. Lauresh.; 92 "subdeterminata loca vallium, montium, #salt.|uum, rivulorum {eqs.}"

                    * CHART. Aquens.; 1 l. 42 "inter #salt.|us rivis aquarum calidarum ... repertis."

                    {persaepe.}

                        ANHÄNGER  abund.:

                            * ARBEO Corb.; 4 p. 191,14 "per latera montium et ima convallium atque silvarum #salt.|os {@ saltus002} (#salt.|us {var. l.})."

                            * WIPO gest.; 28 p. 46,2 "cum ... pervenisset in #salt.|us silvarum ad ... regionem Alamanniae."

                    UUUU_BEDEUTUNG  publ. et iur. (fere in formulis donationis et pertinentiae q. d. sim.):

                        * TRAD. Weiss.; 52 l. 92 (a. 742) "cum ... fareanariis, #salt.|is et subiunctis adiacenciis."

                        * TRAD. Frising.; 24^a p. 53,14 "donavi {(sc. Hroderi)} ... rures, #salt.|oras {@ saltus006}, silvas {eqs.}"; 39 p. 67,33 "tradedi {(sc. Oadalgaer)} ... silvas, #salt.|ores {@ saltus007}, planities {eqs.}"; 51 "condidimus {(sc. clericus)} ... aquarum ... decursos, paludes, #salt.|ora {@ saltus005}."; 65 p. 92,30 "donavi {(sc. Onolfus)} ... rura, #salt.|ura {@ saltus008}, prada {eqs.}"

                        * FORM. Sal. Merk.; 1 p. 241,13 "aquis aquarumve decursibus, mobile et inmobile, #salt.|is {@ saltus004} et subiunctis, cultis et incultis (({sim.} 9 p. 244,25 * CHART. Rhen. med.; I 42 p. 48,11))."

                        * DIPL. Conr. III.; 174.; 188 p. 341,32 "si quispiam ... unum mansum, unum mancipium, unum agrum vel #salt.|im {@ saltus010} ... aut tale aliquid ... abstulerit {eqs.}"

                        * CHART. Babenb.; 191 p. 266,5 "predium ... cum ... #salti.|bus, exitibus et reditibus."

                        * CHART. Burgenl.; I 380 p. 259,50 "usque quoddam #salt.|um {@ saltus009} filiorum ... comitis de Chak."

                            {persaepe.}

        UU_BEDEUTUNG  spectat ad venationem:

            * CHART. Stir.; I 649 p. 629,28 (a. 1185) "in venationibus liberam insecutionem canum ... feras de suis {(sc. abbatis)} #salti.|bus in nostros fuga aberrantes indulgemus {(sc. dux)}."

        UU_BEDEUTUNG  in nomine locorum:

            * ANNAL. Mett.; a. 803 p. 90,6 "imperator ... cum electis iter per Hircanum #salt.|um agens."

            * DIPL. Ludow. Germ.; 109 p. 157,43 "in locum, quem vocant Cidalaribah in #salt.|u Enisae fluvii."

            * REGISTR. abb. Werd.; 2 p. 11,23 "in #salt.|u, qui dicitur Vunnilo."; 2,31 p. 61,11 "in #salt.|u Sinithi in Hosanharth."

            * TRAD. Ratisb.; 208 "prope #salt.|um Nordwald."

            * WALTHARIUS; 490 "vir magnanimus ... venerat in #salt.|um ... Vosagum vocitatum."

            * THIETM. chron.; 6,3 "venimus ad #salt.|um Geronis (({sim.} 6,96 "Arnulfus antistes ad #salt.|um Geronis ... venit."; 7,3 p. 400,20))."

            * ADAM gest.; 1,2 "is {(sc. Wisara fluvius)} in Thuringiae #salt.|u fontem habet."

            * HELM. chron.; 80 p. 150,18 "archiepiscopus ... occurrit eis {(principibus)} in #salt.|u Boemico {(v. notam ed.)}."

            {al.}

    ABSATZ

    U_BEDEUTUNG  latius:
    
        UU_BEDEUTUNG  spectat ad montes:

            * EINH. Karol.; 9 p. 12,4 "#salt.|u Pyrinei superato (({sim.} * POETA SAXO; 1,377 "Pyrenei regressus ad intima #salt.|us"))."

        UU_BEDEUTUNG  spectat ad pascua:

            * CHART. Sangall. A; 680 p. 281,28 (a. 890) "grex porcorum de monasterio ad ... #salt.|um deducebatur ad pastum ({sim.} p. 282,18)."            

            * WALTH. SPIR. Christoph. II; 3,63 "ne veteris longinqua petat {ovile} divortia #salt.|us."; 4,154 "nonaginta novem per pascua nonne relictis in silvas #salt.|us_que redit {pastor} clamando vacantes, ... donec {eqs.}"; 5,100 "ut ... non ruat {(sc. agna lacteola)} in #salt.|um saciem factura luporum."

        UU_BEDEUTUNG  ?vinea -- ?Weinberg:

            * THIETM. chron.; 6,36 "de sua ... proprietate concessit {(sc. antecessor Thietmari)} in Uppusun VII mansos et #salt.|um, qui Pulcher Mons dicitur {(v. notam ed.)}."

AUTOR  Fiedler