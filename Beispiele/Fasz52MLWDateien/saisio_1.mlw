LEMMA 1. *saisio VEL *sacio 

GRAMMATIK 

    verbum III; -ivi, -ire


ETYMOLOGIE 

    theod. vet. sazzan, sezzan: [* {cf.} O. Schade, Altdt. Wörterb. 1882.; II. p. 746 s. v. 'satjan']
    

    {et} francog. vet. saisir:  [* {cf.} Stotz, Handb.; 1,IV § 74.4. 2,VI § 109.5
                                * Wartburg, Frz. etym. Wb.; XVII. p. 19^{b}sqq.]


SCHREIBWEISE

    script.:

        xaxi(o): {=> saisio_6}

        sei-: {=> saisio_1}

        sasi(o): {=> saisio_12b; saisio_4b} {saepius.}

        sassi(o): * ACTA imp. Stumpf; 193 p. 270,1 

        saxi(o): {=> saisio_5} {adde} * RICHER. SENON. gest.; 5,10 p. 336,18.

        saysi(o): {=> saisio_2}

        saixi(o):{=> saisio_7}

        saizi(o): {=> saisio_10}


FORM 

    form.:
        coniug. I.: {=> saisio_13}

STRUKTUR 

    struct. c. praep.:

        ad: {=> saisio_8a; saisio_8b; saisio_6}
        
        de: {=> saisio_9a; saisio_9b; saisio_15a; saisio_15b} 
            {adde} * FORM. Sal. Merk.; 27. 29 {al.}
        
        in {(c. acc.)}: {=> saisio_2} 

GEBRAUCH

    usu absol.: {=> saisio_13}
        
    [usu] mediopass.: {=> saisio_15a; saisio_15b}

    [usu] refl.: {=> saisio_9b}

    

BEDEUTUNG de terris, feodis, bonis i. q. occupare, usurpare, possīdere -- einnehmen, (sich) aneignen, in Besitz nehmen, Besitz ergreifen, in den Besitz eintreten (von): 

    UNTER_BEDEUTUNG in univ.; spectat ad possessionem illegitimam, violentam: {=> saisio_12b; saisio_2} {al.}: 

    * FORM. Marculfi; 1,36 l. 30 "ut ... advocatus ... licenciam habeat ... rem ...  partibus ecclesiae ... #saciendi (sancciendi, faciendi {var. l.})."; 2,41^{capit.} "si aliquis rem alterius ... ad proprietate #sacire {@ saisio_8a} (scire {var. l.}) vult ((* {sim.} FORM. Sal. Bign.; 21 "ad proprium #sacire {@ saisio_8b}."; 22))."

    * CHART. Lamb. Leod.; 36 p. 58,25 (a. 1128) "quicquid iniuste seu violenter ... in curte ..., que ... pertinet ad mensam fratrum ..., occupaveram aut #sasiveram {@ saisio_12b} {(sc. comes)} {eqs.}"

    * GESTA Ern. duc. I; 1,135 "municipia #saysivit {@ saisio_2} in ius imperatoris et suum."

    * RICHER. SENON. gest.; 4,5 p. 302,20 "patrimonium et munitiones beati Petri ... #saixivit {@ saisio_7} (#saxivit {@ saisio_5} {a. corr. 2}) et, quamdiu vixit, obtinuit {(sc. Fridericus imperator)}."

    {saepius.}


    UNTER_BEDEUTUNG spectat ad actum legalem in possessionem intrandi:

        UU_BEDEUTUNG in univ.:

        * FLOD. hist.; 4,41 p. 443,28 "ad quam {(sc. Gellani)} villam dum #saciendi (sanciendi {var. l.}) sibi causa proficisci fratres ... precepisset {praesul} {eqs}." 

        * CHRON. Andag.; 49 p. 122,2 "abbas ... vestituram legalem per manum ducis recipiens ... Caviniacum ... ut proprium suum ... sine ulla contradictione #saisivit."

        * GISLEB. MONT. chron.; 175 p. 259,18 "dum ... comes Hanoniensis ad #saisandum {@ saisio_13} Flandriam circuibat {eqs.}"

        * CHART. Rhen. med.; III 137 "qui {(episcopus)} castrum de Lievenberc propter forefactum ... per iudicium hominum suorum #saisiverat."

        * CHART. Laus.; 277 "#sasire {@ saisio_4b} potest et debet episcopus terram Rodulfi militis ..., quia obiit sine herede." 

        {al.}


        UU_BEDEUTUNG de bonis oppignoratis pigneratori commissis ((* {cf.} H. Planitz, Das dt. Grundpfandrecht. 1936.; p. 63sq.)):

        * CHART. Bern.; II 281 p. 305,16 (a. 1249) "ea, que ille {(sc. fideiussor seu debitor non-burgensis)} in villa habet, per iussum sculteti debet {(sc. burgensis)} #saisire (({sim.} III 58 p. 55,30 "adnotare sive #saisire"))." 

        * CHART. Helv. arb.; 58 p. 94,39 "quod comes, Radulfus et Henricus dictam guageriam cum aliis possessionibus et iuribus, si que #seisiverunt {@ saisio_1} de bonis dicti Petri ..., restituant ipsi Petro."

        * CHART. Lux.; IV 55 p. 64,3 "omnia bona nostra ... comiti {(sc. Flandrensi)} obligamus {(sc. comes Luxelburgensis)} ... ita, quod ... comes ea ... absque meffacere possit arrestare, #saisire, capere et expendere tanquam sua."

        {v. et {=> saisio_2/saisio_14}.}

        UU_BEDEUTUNG de beneficiis, feodis i. q. auferre, in proprium redigere, confiscare -- beschlagnahmen, wieder einziehen, 'konfiszieren':
 
        * WALTH. TER. Karol.; 38 p. 62,32 "domnus Willelmus hominium Alardi guerpivit et eo diffiduciato totum feodum eius #saisivit."

        * CHART. Lux.; II| 147,7 "quam {(sc. comiti Barrensi datam)} terram pro defectu servitii sui et pro pace fracta et interrupta ... comitissa Flandrie #sasiverat."; 341 "quod ... tale feudum, quod a me {(sc. Werico)} tenent ... Lodowicus et Petrus ..., #sasirem (#saisirem, #saizirem {@ saisio_10} {var. l.})"

        * RICHER. SENON. gest.; 4,23 p. 312,23 "terras et homines, que ... comes ... nomine feodi possederat ab eodem episcopo, ad ius et proprietatem Metensis episcopii resumspit et #xaxiit {@ saisio_6} {episcopus}."
        
        {al.}



BEDEUTUNG de hominibus i. q. in possessionem (im)mittere, investire -- in einen Besitz einweisen, einsetzen, 'investieren':

* ROB. TOR. chron.; a. 1169 p. 518,45 "de quibus {(firmitatibus)} #saisitus {@ saisio_15a} erat Herveus de Iven.";  a. 1171 p. 520,31 "de quibus {(terris)} rex Henricus ... fuerat #saisitus {@ saisio_15b}."

* INNOC. III. registr.; 2,243 p. 467,28 "qui {(princeps)} ... sic nepotem tuum {(sc. regis Armeniae)} de principatus successione #sasivit {@ saisio_9a}, ut sibi proprietatem et dominium, quoad viveret, retineret." 


    ANHÄNGER usu refl.:

    * CHART. Laus.; 189 (a. 1227) "coram sacerdote et multis aliis investivit et #saisivit {@ saisio_9b} {praepositus} se ad opus capituli de supradictis hominibus et terra."


AUTORIN Niederer