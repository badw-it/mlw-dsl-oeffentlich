LEMMA 1. *sagimen

GRAMMATIK
     subst. III; -inis n.

ETYMOLOGIE
      francog. vet.
            saïm: *{v.} Tobler-Lommatzsch, Altfrz. Wb. IX.; p. 62sq.* R. Marchionni, Der Sciendum-Kommentar. 2003.; p. LXVII     

VERWECHSELBAR
     confunditur c.:
           sanguis: {=> sagimen_1; sagimen_2}
                * {adde} HILDEG. phys.; 8,1 p. 367,9 {(var. l.)}


BEDEUTUNG pinguedo, omentum, adeps -- Fett, Speck, Schmalz:
      U_BEDEUTUNG gener.; fere de cibo:
           UU_BEDEUTUNG in univ.:
                * DECRET. Burch.; 19,15 "eo {(sc. quo paenitet frater)} die excepto vino, carne et #sagimin.|e sumat, quidquid velit."
                * CHRON. Mich.; 4 p. 7,13 "provisor panis et salis et #sagimin.|is."
                * GESTA Trud. cont. I; 13,3 "quod ... dictum sit ..., quot porcos ad #sagime.|n {(sc. fratres haberent)}."
                * VITA Meinw.; 178 p. 98,21 "ut ... omnes ... christiani ... in abstinentia sint carnis  et #sagimini.|is {@ sagimen_2} (sanguinis {var. l.})"
                * ALPHITA; S 171 "sumen vel #sagime.|n, omentum idem."
                {persaepe.}
           UU_BEDEUTUNG usu med.:
                * PLATEAR. pract.; 27,54 "dentur {(sc. asmate laboranti)} cum farina ordei cocte in #sagimin.|e {@ sagimen_1} (sanguine {var. l.})"
                * ROGER. SALERN. chirurg.; 1,463 "si locus fuerit rugosus, unge con veteri #sagimin.|e, deinde pilos extrahe."
                * TRACT. de chirurg.; 444 "de eisdem {(radicibus)} IX crispulas facias cum farina siliginis et ovis et #sagimin.|e facias."; 1059 "sagimen <abrasum> ... inponimus."
                * TRACT. de aegr. cur.; p. 107,30 "fiat eis {(lethargicis)} clistere de mercuriali et malvis et oleo, nitro et #sagimin.|e (({ci.,} sagimme {ed.}))"
                * THEOD. CERV. chirurg.; 2,20 p. 150^E "medicus habeat paratum #sagime.|n porcinum optime abrasum, et sic ex #sagimin.|e, quod ex parte cutis, et cum dicto #sagimin.|e totum membrum fractum inungatur."
                ANHÄNGER adde:
                * HILDEG. phys.; 7,5,9 p. 336,11 "pulverem ... #sagimin.|e, id est smalcz, de vitello ovorum paratum inmitte et sic unguentum fac."
           UU_BEDEUTUNG in tributo, traditione, commercio vectigalibus supposito (usu iur.):
                * CHART. Rhen. med.; $I 333 p. 387,27 (a. 1051) "quatinus ... fratres de predicta villa ... #sagime.|n interdum habeant."
                * CHART. episc. Hild.; $I 185 "cum ... cado #sagimin.|is et IV cadis mellis."
                * CHART. Tirol. I; 446 "tradidit {Bonel} ecclesie ... unum minale #sagimin.|is ad lumen cendendum omni anno."
                * CHART. Hans.; 277 "de vase #sagimin.|is allecis quatuor solidos dabunt {mercatores}, ... de vase #sagimin.|is porcorum duos solidos dabunt."
                * CHART. Rhen. med.; $III 932 p. 700,28 "venditor olei, sepi, #sagimin.|is (({ci.,} saginis {ed.}))."
                * REGISTR. Geisenf.; 11 "ad artocreas pertinet una olla parva #sagimin.|is (({germ. } smaltz)) ..., et ad vierwitzas debet dari #sagime.|n suficiens."
                {al.}

      U_BEDEUTUNG anat. (de corporis omento):
                * ALBERT. M. animal.; 1,24 "quaedam {(sc. membra)} ... sunt humida ... et siccantur ..., sicut sanguis, fleuma, sepum et pinguedo alia, quae #sagime.|n vocatur."; 3,138 "pinguedo #sagimin.|is est in hiis, quae habent dentes in utraque mandibula."
                ANHÄNGER de #sagimin.|e ceti ('Tran'):
                * ALBERT. M. animal.; 24,17 "undecim lagenas #sagimin.|is emisit {cetus}, ... hoc #sagime.|n et lagenas ego vidi, et est #sagime.|n valde lucidum et purum, postquam defecatum est; alter {(cetus)} captus fuit ..., cuius caput quadraginta reddidit #sagimin.|is lagenas."

      
    
 AUTORIN Weber