LEMMA   salictum

            (selec-, salli-, sallec-, salc-, salec-, saletum, -ittum, -cetum {sim.})
            
GRAMMATIK
    subst. II; -i n.

STRUKTUR
    form.:
 
        salecta {(decl. I.; ni subest usus verbalis sing. c. neutr. plur. secundum gr.)}: {=>salictum_1}

        abl.: 
            sg.:
                -u: * CHART. Drubic.; 25
            pl.:
                -ibus: * DIPL. Frid. I.; 881 p. 123,4

BEDEUTUNG   locus salicibus consitus, nemus salicum -- mit Weiden bewachsenes Grundstück, Weidengebüsch, Weidicht:

    UNTER_BEDEUTUNG in univ.:

        * YSENGRIMUS; 6,19 "cum sua villani pacare #salict.|a sinantur, cur requiem lucus regis habere nequit?"

        * CHART. Anhalt.; $I 764 "opus misericordie effectui mancipare volentes insulari #salict.|o ... adauximus {(sc. abbas)} (({sim.} * CHART. Port.; 108 "parvulam insulam #salict.|i citra Salam positam ... abbati ... vendidi {[sc. Conradus miles]}"))." 

        * CHART. civ. Misn.; 388 p. 291,29 "#salict.|um, quod in vulgari wert appellatur, cum montibus circumiacentibus {(sc. ecclesiae contulit marchio)} (({sim.} 391 p. 294,28))."

        * CHART. episc. Halb.; 1064 l. 17 "Iohannes de Gatersleve ... duo #salict.|a, unum vulgariter dictum magna breda, aliud parva breda, ... dedit ... ecclesie in restaurum (({sim.} 1065 l. 8 "#sall|.ictum"))." {al.}
    
    UNTER_BEDEUTUNG in formula pertinentiae q. d. chartarum:

        * DIPL. Heinr. III.; 271 p. 361,35 "cum ... #salcetis, olivetis, cannetis {eqs.} (({sim.} 394 p. 549,17 [spur.] "cum ... pascuis, #saletis, rupis." * CHART. Carniol.; $I 66 p. 72,15 "cum ... silvis, #selectis, sationibus." * CHART. march. Misn.; $II 428 "cum ... pascuis, vinetis, #sali.|cetis." * COD. Wang. Trident.; 55 p. 646,24 "cum ... stillicidiis, #sallectis et aqueductibus"))." 
     
        * DIPL. Frid. II.; 509 p. 164,9 "prata, silva, #sali.|tta {(sc. ecclesiae confirmavit rex)}." {saepe.} 

BEDEUTUNG   salix -- Weide:

    * CARM. Paul. Diac. app. I; 4,1,4 "qui {(praesul)} in ripis fluvii morat, at ubi multa #sal.|ecta {@salictum_1} nascitur et iuncus, pariter tegumenta corymbi {(v. notam ed.)}."

    * FROUM. carm.; 11,30 "#salict.|i foeturam pecudis securi pellimus {(sc. falsi pastores)} arcus {(v. notam ed. et cf. vol. $I. p. 910,42sqq.)}."

AUTORIN Clementi