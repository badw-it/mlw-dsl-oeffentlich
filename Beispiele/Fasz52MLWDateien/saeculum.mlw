LEMMA saeculum
    (se-, -clum)
GRAMMATIK subst. II; -i n.

SCHREIBWEISE  

    script.:
        -cc-: {=> saeculum3}
        -col(um): {=> saeculum4}{=> saeculum5} {al.}
        {?}seul(um): {=> saeculum6}

FORM  

    form.:
        masc.: {=> saeculum10}{=> saeculum11}
        fem.: {=> saeculum12}
       

BEDEUTUNG  aevum, tempus, spatium temporis -- Zeitlichkeit, Zeit(spanne, -dauer):

    UNTER_BEDEUTUNG  usu vario:

        UU_BEDEUTUNG  in univ.:

            * DIPL. Merov.; 166 p. 413,18 "quatenus de caduces rebus {!presente} #secoli {@ saeculum4} aeterna conquiretur ((* DIPL. Loth. II.; 13 p. 404,1 "in praesenti #se|.cul.|o." {al.}))."
            * ALCUIN. epist.; 163 p. 264,18sq. "#saecul.|um est ... mundi ordo decurrens, qui ad futura tendens praeterita deserit, et ideo #saecul.|a dicta esse putantur, quia in se iugiter revolvuntur tempora."
            * HRABAN. carm.; 39,58,6 "quem {(Christum)} non capiunt #saecul.|a."
            * COD. Odalb.; 57 p. 119,19 "locum ... perpetuo #saecul.|o possidere."
            * WALTH. SPIR. Christoph. II; 4,241 "omnia qui {(sc. Deus)} certo distinguens tempora fine disposuit secum volventem #se|.cul.|a cursum."; 5,80 "me {(sc. Aquilinam)} committe ... superanti #sęcla patrono." {saepius.} {v. et {=> saeculum98}}
            
            
        ANHÄNGER  iunctura "ultimum #saecul.|um" de die extremo:

             * NICET. ad Iust.; p. 119,22 "gaudere nos in ultimo #saecul.|o positos facias, quos lugere post laetitiam de perditione tua fecisti."

        UU_BEDEUTUNG  aetas (vitae humanae), spatium vitae -- (Zeit-, Menschen-)Alter, Lebenszeit, -jahre: 

            UUU_BEDEUTUNG  spectat ad vitam unius hominis:

                * FROUM. carm.; 21,23 "nutriat vos {(sc. amicos)} temporis etas #saecul.|a producens ... superisque recensens."
                * AMALAR. interpr.; 72 "eius {(sc. uniuscuiusque)} vita #se|.cul.|um dicitur {eqs.} ((*{sim.} ALBERT. M. summ. creat. I; 2,6,4 p. 394^{b},7))."
                * ALBERT. M. miss.; 2,10,2 p. 68^{a},24 "#saecul.|um unum est spatium uniuscuiusque durationis, quo durat creatura una."

            ANHÄNGER  spectat ad regenerationem (secundum Pythagoram):

                    * MANEG. c. Wolfh.; 1 p. 45,10 "quę {(anima)} ..., si in primo corpore male viveret, in presenti #se|.cul.|o vel in aliquo #se|.cul.|orum futurorum, quorum infinitam seriem promittebat, corpus deterius sortiretur."

            UUU_BEDEUTUNG  spectat ad generationem coaequalem ('Generation'):

                UUUU_BEDEUTUNG  proprie:

                    * BERTH. chron. B; a. 1054 p. 163^{b},11 "Herimannus ... in interiori ... ingenii vena pre cunctis <<sui #se|.cul.|i>> ((*{cf.} chron. A; p. 163^{a},7 "tunc temporis"))  viris mirabiliter dilatatus."
                    * ADALB. SAMAR. praec. dict.; p. 58,12 "reciprocationes et barbaras inusitationes sapientes nostri #se|.cul.|i spernunt, sed {eqs.}" {al.}

                ANHÄNGER  praevalente notione hominum coaequalis generationis:

                    * CARM. Teg.; 40,6,1 "omnia ..., Deus, ad te #se|.cul.|a ducis." 
                    
                UUUU_BEDEUTUNG  meton. i. q. mos, consuetudo (temporis) -- Zeitgeist, Mode:  

                    * EKKEH. IV. cas.; 45 "quibus {(viris)}, quia, ut nunc #se|.cul.|um est, diffidi putamus, tacere quam scribere maluimus."

            UUU_BEDEUTUNG  spectat ad multas generationes sequentes ('Epoche', 'Ära'):

                    UUUU_BEDEUTUNG  in univ.:

                        * WALAHFR. imag. Tetr.; 94 "{!aurea} quae prisci dixerunt #saecul.|a vates, ...venisse videmus 
                        ((* ADAM gest.; 3,39 p. 182,6. {saepius} ((* {cf.} VERG. ecl.; 4,5sqq. * Aen.; 6,792sq.))
                        ))."
                        * EPIST. var. II; 13 p. 317,23 "quicquid de primo #saecul.|o, quod ante generalem fuerat cataclismum, sive de secundo ... gentiles senserunt scriptores {eqs.} {(cf. {=> saeculum77})}."; 
                        14 p. 319,30 "decet ... te {(sc. imperatricem)} venerabilem unicum erudire filium, ... novi #sę|.cul.|i regem {eqs.}"
                        * CHRON. Erf. mod. I; a. 1208 p. 205,16 "a Teutonicis #se|.cul.|is {@ saeculum97} scelus inauditum exequitur." {saepe.}

                    UUUU_BEDEUTUNG  spectat ad regnum, culturam (unius aetatis):

                        * WILH. RUBRUQU. itin.; 1,14 "tertia die invenimus Tartaros, inter quos, cum intravi, visum fuit michi recte, quod ingrederer quoddam aliud #se|.cul.|um.";
                        36,7 "domino Francorum regi Lodovico et omnibus aliis dominis et ... magno #se|.cul.|o Francorum."

        UU_BEDEUTUNG  de certo annorum, temporis spatio:

            UUU_BEDEUTUNG  decennium -- Jahrzehnt:

                * EPIST. Hann.; 78^a p. 126,26 "cum tot {(sc. quinque)} #sę|.cul.|a numeretis {(sc. episcopus)} quot ecclesia vestra pręsules, cum tot annorum prudentia ... iuvenilem ... pręsentatis vigorem {(v. notam ed.)}."

            UUU_BEDEUTUNG  spatium centum annorum -- Jahrhundert:
                
                * EUGEN. VULG. calend.; 13 "septenis binis constat divisio #sęcli {@ saeculum98} in numeris rerum spaciis vertiginis ęvi, scilicet atomi molis necnonque momentis: inde ... temporis anni cyclorumque rotę ętatis, #sęcli quoque iuris {(v. notam ed.)}."
                * CONR. MUR. nov. grec.; 4,86 "#seclum centenne spatium ... dicitur."

            UUU_BEDEUTUNG  spatium mille annorum, millenarium -- Jahrtausend:

                * HONOR. AUGUST. imag.; 2,79 "#saecul.|um sunt mille anni."

            UUU_BEDEUTUNG  aeternitas -- Ewigkeit:

                UUUU_BEDEUTUNG  proprie:

                   * {v.} {=> saeculum96{sqq.}}
                

                UUUU_BEDEUTUNG  alleg. de Christo: 
                
                    * HYMN. abeced.; Z 2 "zona cuius {(Christi)} circumdedit cunctum mundum, lumen largum, sol super<num>, #se|.cul.|um summum."

    UNTER_BEDEUTUNG  locut. adv.:

        UU_BEDEUTUNG  "a #saecul.|o, #saecul.|is" i. q. ex, ab aevo, semper -- von Anbeginn (an), seit jeher, immer schon; {c. negat. i. q.} numquam adhuc -- noch nie:

            * WOLFHARD. Waldb.; 2,6^a p. 222,10 "Deus ... terrificum nobis ... est ostentare dignatus prodigium, ... a #se|.cul.|is {!inauditum} ((* BERNOLD. CONST. chron.; a. 1092 p. 497,1 "tonitru." {al.} {cf. et {=> saeculum97}}))."
            * DIPL. Conr. II.; 84 p. 114,31 "que {(concessiones)} supra dicte ecclesie a #se|.cul.|o usque nunc facte sunt a nostris predecessoribus."
            * CHART. archiep. Magd.; 360 "hec {(sc. inenarrabiles exsecrationes)} a #se|.cul.|is in christianos a christianis facta non sunt, ideoque {eqs.}" {al.}

            
        UU_BEDEUTUNG  "ante omnia #saecul.|a" i. q. ante omnia tempora -- vor aller Zeit, vor der Zeit: 

            * LIBER diurn.; 85 p. 107,5 "qui {(sc. Dei verbum)} natus est de patre ante omnia #se|.cul.|a."
            * MANEG. c. Wolfh.; 14 p. 74,4 "secundum predestinationem ante omnia #se|.cul.|a apud misericordis Dei consilium habitam." {saepe.}

        UU_BEDEUTUNG  "in {@ saeculum96}, per #saecul.|um, (omnia) #saecul.|a {sim.}" i. q. in aeternum, aevum, semper -- in alle Zeit, in Ewigkeit, auf ewig:

            UUU_BEDEUTUNG  in univ.:

                * TRAD. Frising.; 321 (a. 814) "ille bene {!possedit} rebus in #se|.cul.|a, qui {eqs.} ((* CHART. Sangall. A; 609 "a #saecul.|o in #saecul.|um possideant." {al.}))."
                * WALAHFR. carm.; 38,63 "quae {(praemia)} ... in omnia #sae.|cla redundent."; 38,100 "{!valeas} {(sc. Ruadbernus)} ... per #saecul.|a cuncta ((9,2,12. {al.}))."; 2,33 "per #saecul.|a pollens."
                * HRABAN. carm.; 39,13,1 "solus Deus in #saecul.|a, concludens cuncta tempora."
                * CONST. Constant.; 76 "qui {(Christus)} cum patre et spiritu sancto per infinita vivit et regnat #saecul.|a."
                * RHYTHM. ; 4,18,3 "'cum dolore ... lugens descendo in #se|.cul.|um (sepulcrum {ci. Vollmer; v. et notam ed.})', hoc modo Iacob plorabat Ioseph."
                {persaepe.}


            UUU_BEDEUTUNG  "(in) #saecul.|um #saecul.|i, (in) #saecul.|a #saecul.|orum {sim.}" ({de re v.} Stotz, Handb. 4,IX § 26,5. {v. et} Chr. Mohrman, Études sur le Latin des Chrétiens II. 1961. p. 127):

                UUUU_BEDEUTUNG  usu vario:

                    * TRAD. Patav.; 6 (ante 774) "acta sunt ... haec in civitate ... Reganesburo regnante Domino nostro in #se|.cul.|a #se|.cul.|orum, amen."
                    * TRAD. Frising.; 144 p. 150,26 "condemnabitur {(sc. contra agens)} in #se|.cul.|um #se|.cul.|i."
                    * WALAHFR. carm.; 75,17,4 "spiritu ... regente tempora #sae.|cli, amen."
                    * ANSELM. BIS. rhet.; 2,1 p. 139,11 (vs.) "cuius scelus ..., sunt illi quidem lacrime #se|.cul.|orum #se|.cul.|a." {persaepe.}

                UUUU_BEDEUTUNG  de formula musicali "#saecul.|orum (amen)" {(de re v. M. Appel, Terminologie in d. ma. Musiktraktaten. 1935. p. 58. n. 93)}:

                    * ODO inton.; p.117^{a} "ut antiphona cuius sit toni, statim cognoscitur et quot #se|.cul.|orum debeat."
                    * GUIDO AUGENS. mus.; 495 "plagalis ... elevari debet ... ad minus usque ad litteram illam, in qua incipit suum #se|.cul.|orum."
                    * GUIDO ARET. microl.; 8,27 "distinctiones ... dico eas, quae a plerisque differentiae vocantur (hoc est #saecul.|orum amen {add. V4})." {al.}

ABSATZ 

BEDEUTUNG  mundus, natura (creata) -- Welt, Schöpfung:

    UNTER_BEDEUTUNG  de mundo terreno (temporali, materiali):
    
        UU_BEDEUTUNG  in univ. {(usu plur.: {=> saeculum33})}:

            UUU_BEDEUTUNG  usu vario:

                * PAUL. DIAC. Lang.; 2,4 p. 74,17 "videres {(sc. lector)} #se|.cul.|um in antiquum redactum silentium."
                * RIMB. Anscar.; 3 p. 23,28 "quae {(vox suavissima)} mihi omne ... #saecul.|um visa est implevisse."; 35 p. 67,10 "vidit se quasi usque in caelum rapi et totum #saecul.|um acsi in teterrimam vallem collectum."
                * MARIAN. chron. a. m.; 2177 p. 477^{b},13 "secunda {@ saeculum77} aetas #se|.cul.|i a diluvio usque ad nativitatem Abrahae protenditur ({postea:} de tertia aetate mundi)."
                * VITA Heinr. II. A; 31 "caveant ... habitatores #se|.cul.|i domesticis Dei et civibus sanctorum detrahere. {saepe.}"  
                {v. et {=> saeculum55}}

            UUU_BEDEUTUNG  spectat ad creationem mundi:

                * WETT. Gall.; 29 p. 273,7 "plasmator #saecul.|orum {@ saeculum33}."
                * HYMN. Hraban.; 5,10,2 "os praeclarum conditoris, quod formavit #saecul.|um, en ... sugit matris ubera."
                * RATHER. epist.; 26 p. 150,24 "si ... #se|.cul.|um, quod nostri causa fecit Deus, minime diligimus." {al.}

            UUU_BEDEUTUNG  spectat ad inconstantiam, mutabilitatem mundi:

                * TRAD. Frising.; 586 (a. 829) "presentem #se|.cul.|um {@ saeculum12} ruinis ... casuram videbimus."
                * CAND. FULD. Eigil. II; 25,2 "#sae.|cli fluctivagi vana et peritura potestas."
                * HRABAN. carm.; 37,31 "#sae.|cli versatilis ordo."
                * EPIST. var.; 1,8 (ed. B. Bischoff, Anecdota. 1984. p. 127) "inter varias labilis #saecul.|i huius procellas." {saepe.}

            UUU_BEDEUTUNG  spectat ad finem mundi:  

                * WALAHFR. Wett.; 790 "terminus ut ... ponatur in ordine #sae.|cli."
                * HRABAN. epist.; 33 p. 467,6 "in vespere mundi, hoc est in fine #saecul.|i." {saepius.}


        UU_BEDEUTUNG  vita (mundana, temporalis, saecularis), conversatio (terrestris) -- (diesseitiges, irdisches) Leben, Lebenswandel, Diesseits:

            UUU_BEDEUTUNG  usu vario:

                * FORM. Andec.; 41 p. 18,9 "nec nus contingit ultimus dies inordinatus ... de huius #se|.cul.|i (#seuli {@ saeculum6} {cod.}) lucis discesseremus."
                * FORM. Marculfi; 1,12 p. 66,2 "dum pariter advixerint {coniuges} in hunc #se|.cul.|um {@ saeculum10}."
                * FORM. Senon. I; 1 "dum ... Deus nobis precepit in hunc #saecul.|um {@ saeculum11} sano corpore habere."
                * TRAD. Lunaelac.; 38 p. 137,17 "usque dum in monasterio vado {(sc. Hildiroh)} vel ad Romam aut de hoc #se|.cul.|o {!migraturus} sum ((* AGIUS vita Hath.; 10. * GERH. AUGUST. Udalr.; 1,27 l. 36. {persaepe}))."
                * TRAD. Frising.; 519 p. 442,39 "dum oportet unicuique in hoc #se<c>olo {@ saeculum5} vacare."
                * EPIST. Worm. I; 14 p. 31,2 "unus {(faber)}, quem optimum habui, #sę|.cul.|um mutavit."
                * CHART. Stir.; IV 319 "domino Ottone de medio huius #secculi {@ saeculum3} sublato." {persaepe.}

            UUU_BEDEUTUNG  spectat ad res terrenas:

                    * ARBEO Corb.; 14 "#se|.cul.|i (#saecul.|i {var. l.}) gloriam pertimescere."
                    * RIMB. Anscar.; 20 p. 45,5 "in #saecul.|i ... rebus dives erat {matrona}."
                    * RATHER. epist.; 16 p. 81,32 "ecce causa, ecce occasio, quam #saecul.|i praetendunt {!amatores} ((* THIETM. chron.; 1,26))."
                    * BERTH. chron. B; a. 1077 p. 304,7 "quanto ... magna fuit {imperatrix} in #se|.cul.|i magnalibus, tanto {eqs.}"
                    * ADAM gest.; 1,63 "respicite ... istum pauperem #se|.cul.|i et modicum, immo laudabilem ... sacerdotem Christi."
                    * GODESC. GEMBL. gest.; 48 p. 543,15 "cras forsitan dicet aliquis qualitatem perpendens praesentis temporis: 'falsum est, quod hic legitur, nunquam tali #se|.cul.|o usi sunt monachi illius temporis.'" {saepe.}



            UUU_BEDEUTUNG  vita monastica, religiosa sim. opposita: 

                UUUU_BEDEUTUNG  in univ.:

                    * WILLIB. Bonif.; 1 p. 7,11 "terrenis ... #saecul.|i {!renuntians} lucris ((* AGIUS vita Hath.; 7. {saepius}))."
                    * ARBEO Corb.; 1 "ut #se|.cul.|um a iuventutis {!relinqueret} flore ((10 p. 199,2. {saepius}))."
                    * NICOL. I. epist.; 38 p. 309,36 "cum militibus Christi sit Christo servire, militibus vero #saecul.|i #saecul.|o."
                    * EPIST. Teg. I; 63 "qui {(frater defunctus)} professionem regularem dimisit et ad #se|.cul.|um puplice rediit."
                    * DIPL. Heinr. IV.; 358 p. 476,46 "offertos ... monasterii nolumus in #se|.cul.|o vagari, sed ... liceat abbati ... ad monasterium revocare." {persaepe.}

                UUUU_BEDEUTUNG  de ordine clerici saecularis: 

                    * CHART. Eichst.; 90 p. 139,9 (a. 1272) "Lvdwico, sive fuerit in religione vel in #se|.cul.|o, ... duas dotes ..., quamdiu vixerit, deputamus {(sc. episcopus)}."


        UU_BEDEUTUNG  homines, communitas hominum terrestris, saecularis -- Menschheit, irdische Gemeinschaft, 'Welt':

            UUU_BEDEUTUNG  usu vario {(plerumque usu plur.)}:

                * HUGEB. Wynneb.; 11 "Willibaldus aecclesiam ... sancto consecravit #se|.cul.|orum omnium {!Salvatore} ((* Willib.; 5 p. 104,6. {al.}))."
                * WALAHFR. carm.; 6,27 "quamvis multa canas {(sc. episcopus)} #sae.|cli celebranda catervis."
                * WIDUK. gest.; 3,76 "defunctus est ... imperator ... multa ac gloriosa #saecul.|is (sedis {var. l.}) relinquens monimenta."
                * EPITAPH. var. II; 111,14 "gaudet ... omne #seclum."
                * DIPL. Heinr. III.; 305 p. 414,36 "falsum #saecul.|i blandientis honorem et inanem gloriam ... deficere cognoscimus." {saepius.}

            UUU_BEDEUTUNG  spectat ad aestimationem, iudicium hominum {(interdum aestimatione Dei opposita)}: 

                * LIUTG. Greg.; 7 p. 73,22 "praesules ... in hoc #se|.cul.|o decorati."
                * WANDALB. Goar.; 2,6 p. 49,13 "Vualtarius quidam homo {!secundum} #saecul.|um {!nobilis} ((* TRAD. Ratisb.; 603. * {sim.} MEGINH. FULD. Alex.; 4 p. 427,6 "s. #saecul.|i dignitatem ... n."))."                
                * DIPL. Ludow. Germ.; 130 "qui {(abbas eligendus)} secundum Deum et secundum #sę|.cu.|um ... dignus existat."
                * WIPO gest.; 1 p. 10,22 "Constantiensis ecclesiae praesul erat Heimo, vir sapiens in Deo, modestus et providus ad #se|.cul.|um." 
                * CHART. Westph.; IV 104 "potentes a #se|.cul.|o viri famosi."
                {saepe.}
            
            ANHÄNGER  de aestimatione imperitorum, idiotarum:

                * NOTULAE Wilh. Cong.; 11 "nomen eius {(Willehelmi Burgensis)} gloriosum erat aput omnes partes #se|.cul.|i."
            
            UUU_BEDEUTUNG spectat ad iurisdictionem saecularem: 

                * CHART. Fuld. B; 18 p. 36,18 (a. 752) "iuxta poenam #se|.cul.|i."
                * CHART. Rhen. inf.; I 23 "patrimonium ... mihi {(sc. Thangrimo)} ... secundum leges #se|.cul.|i potestati nostrae diiudicatum est."
                * DIPL. Otton. III.; 323 p. 751,1 "iudicio ecclesie et #se|.cul.|i."
                * CHART. Heinr. Leon.; 41 p. 61,22 "secundum iusticiam #se|.cul.|i." {al.}

            
  
    UNTER_BEDEUTUNG  iunctura "futurum, aliud #saecul.|um {sim.}" de mundo caelesti, (post vitam terrenam) venturo:

        UU_BEDEUTUNG  usu vario:

            * BONIF. epist.; 48 p. 77,15 "ut in gratia Dei semper hic et {!in futuro} #saecul.|o permaneatis {(sc. Gripho)} ((* HRABAN. epist.; 47 p. 502,33 "in presenti vita ... et in f. #se|.cul.|o." {saepius}))."
            * VITA Rimb.; 8 p. 86,24 "in alterius #se|.cul.|i vita."
            * IOH. PLAN. hist.; 3,9 "credunt {(sc. Tartari)} ..., quod post mortem in alio #se|.cul.|o vivant {eqs.}" {al.}
            
        UU_BEDEUTUNG  in visione:

            * CONC. Karol. B I; 41,7 p. 415,2 "sanctus Eucherius ... episcopus ... in oratione positus ad alterum est #saecul.|um raptus et vidit {eqs.}"
            * VISIO Godesc. B; 5,1 "omnia ..., que in illo #se|.cul.|o michi videre concessa sunt, longe ab hiis, que in #se|.cul.|o {@ saeculum55} {(sc. hoc)} videntur, et forma et specie distabant."


AUTOR Leithe-Jasper

UNTERDRÜCKE WARNUNG 516