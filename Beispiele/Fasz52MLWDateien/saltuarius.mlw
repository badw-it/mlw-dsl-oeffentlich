LEMMA  saltuarius

    (-ta-)

GRAMMATIK  substantivum II; -i m.

VEL {semel ({=> saltuarius001})} saltuaris

GRAMMATIK  substantivum III; -is m.

SCHREIBWEISE

    script.:

        {?}ps-: {=> saltuarius009}

        saluta-: {=> saltuarius010}
        
        -ter-: {=> saltuarius008}{=> saltuarius004} {al.}

        {?}-tair-: * CHART. Sangall. B; II 1412

        -terus: {=> saltuarius002}{=> saltuarius003}

STRUKTUR

    form.:

        abl. pl.:
        
            -ris: {=> saltuarius005}

BEDEUTUNG  custos saltuum, praefectus forestis, forestarius -- Forstaufseher, -verwalter, Flurhüter, Förster (({de re v.} * L. Söll, Die Bezeichnungen für den Wald in den roman. Sprachen. 1967.; p. 254)):

    U_BEDEUTUNG  in univ.:

        * LEG. Lang.; p. 126,18 "si in alia iudiciaria inventus fuerit {servus fugax}, tunc deganus aut #sal.|tarius (("#salutarius" {@ saltuarius010} {var. l.})), qui in loco est, conprehendere debeat."

        * CHART. Stir. I; 95 p. 112,13 "tradidit ... dux ... saltum ..., qui vulgo vorst dicitur, cum omni usu, quem habet ..., pellibus martonum et #saltuari.|bus {@ saltuarius001}, qui vorstere dicuntur (({inde} 99 p. 118,20.; 513 p. 479,36. * DIPL. Frid. I.; 562 p. 29,35))."

        * CHART. Altaerip.; add. D 1 p. 337,18 "testes: ... Giroldus #sal.|terius {@ saltuarius008}, Bonusfilius et Uldricus ministri (({sim.} * CHART. Babenb.; 125 p. 164,7 "confirmacionis testes sunt ... Diepoldus venator, Petrus #saltuari.|us." * CHART. Bern.; II 325 p. 351,15 "interfuerunt testes: ... Aymo ministralis, Iohannes Marchis, maior et #sal.|terus {@ saltuarius002} de Leucha {eqs.}"; 356 p. 381,34 "Petrus #sal.|terus {@ saltuarius003} de Leucha."; 417 "Petrus filius #sal.|terii {@ saltuarius004}." {al.}))."

        {persaepe.} {v. et vol. IV. p. 600,12.}

            ANHÄNGER  fort. huc spectat:

                * CONO LAUS. gest.; p. 776,20 "burgenses ad arma ferenda ydonei sequi debent ... vel senescalcum vel #psaltarium {@ saltuarius009}."
    
    U_BEDEUTUNG  spectat ad vineas ('Weinberghüter', 'Wengerter', 'Saltner'):

        * CHART. Ital. Ficker; 401 | p. 416,17 (a. 1246) "si aliquis postaverit lignamen vinearum Veronensium causa vendendi, emendet x soldos ..., nisi prius nunciaverit #saltuari.|is {eqs.}"; p. 417,4 "#saltuari.|i ... iuravere ad sancta Dei evangelia custodire ... dictam curtem." {ibid. al.}

BEDEUTUNG  praefectus (iudiciarius) -- (juristischer) Amtmann ((* {de re} v. D. Vitali, Mit dem Latein am Ende? 2007.; p. 600sq.)):

        * CHART. Helv. arb.; 54 p. 88,12 (a. 1247) "recepta sua debent sibi facere mansuarii cum IV equis et II #salt.|eris {@ saltuarius005} de villa in villam."

AUTOR  Fiedler