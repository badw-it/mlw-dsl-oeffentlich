LEMMA  sacellarius

    (sacce-)

GRAMMATIK  substantivum II; -i m.

SCHREIBWEISE  

    script.:
    
        sec-: * LIUTPR. hist.; 9 p. 166,6 {(var. l.)}

BEDEUTUNG  thesaurarius, custos fisci, dispensator, scriba, notarius -- Schatzmeister, Säckelmeister, Zahlmeister, Schreiber, ‘Notar’ ((* {de re v.} Bresslau, Urkundenlehre. II.; p. 202 et Th. Gross, R. Schieffer, Hinkmar von Reims. De ordine palatii. 1980.; p. 65)):

    U_BEDEUTUNG  eccl. et canon. de officialibus papae, episcoporum:

        * CONC. Karol. A; 5 | p. 38,21 "Theophanius notarius regionarius et #sacc|.ellari.|us relegit {(sc. epistulam)} {eqs.} (({item} p. 41,8 "#sacellari.|us."; p. 41,27 "#sacellari.|us"))."

        * ANNAL. regni Franc.; a. 801 p. 114,9 "huius {(sc. quae pontificem deposuerat)} factionis fuere principes Paschalis ... et Campulus #sacellari.|us (#sacc|.ellarius {var. l.}) (({sim.} * HIST. Lang. cont.; II [MGLang. p. 202,30] "Pascalis primicerius cum Campolo #sacc|.elari.|o ... ferino more eum {[papam]} comprehendentes"))."

        * ORD. Rom.; 5,92 "post omnes hos {(sc. fideles)} communicat ad sedem regionarios ... et #sacellari.|um (#sacc|.ellarium {var. l.}) et acolitum {eqs.} (({item} * PONTIF. Rom.-Germ.; 92,92))."

        * LEG. Lang.; p. 663,15 "#sacc|.ellari.|us debet habere curam monasteriorum ancillarum Dei et in festis debet introducere omnem honorem ante imperatorem."

        {persaepe.}

    U_BEDEUTUNG  publ. et iur. de officialibus regis, imperatoris sim.:

        * ANNAL. regni Franc.; a. 826 p. 170,13 "quem {(Georgium)} imperator Aquasgrani cum Thancolfo #sacellari.|o (#sacc|.ellario {var. l.}) misit."

        * HINCM. ord. pal.; 280 "sacrum palatium per hos ministros disponebatur: ...; quamvis ... ex latere eorum alii ministeriales fuissent ut ostiarius, #sacellari.|us (#sacc|.ellarius {var. l.}), dispensator {eqs.}"

        * ADAM gest.; 4,8 p. 236,3 "Heinricus apud Orchadas ... fuit episcopus isque in Anglia #sacellari.|us (#sacc|.ellarius {var. l.}) Chnud regis fuisse narratur."

        * LEG. Lang.; p. 664,21 "quartus {(sc. iudex est)} #sacc|.ellari.|us, qui stipendia erogat militibus et Romae sabbato scrutiniorum dat eleemosynam."

    U_BEDEUTUNG  usu vario:

        * ARNOLD. RATISB. Emm.; 2 p. 1033^C "marsupium secum portans ... #sacellari.|us eorum {(egenorum et peregrinorum)} appellari et esse non erubuit."

        * GESTA Camer.; 3,60 p. 488,37sq. (epist.) "sicut liberalitas vestra {(sc. Heinrici augusti)} #sacellari.|um habet, qui causis supervenientibus cotidianae expensas faciat, ita et ego {(sc. Gerardus episcopus)} #sacellari.|us eorum {(pagensium)} sum, ut {eqs.}"

AUTOR  Fiedler