LEMMA sanativus

GRAMMATIK
    adi. I; -a, -um

STRUKTUR
    usu subst: {=>sanativus_1{sqq.}} {=>sanativus_2} {=>sanativus_3}

BEDEUTUNG qui sanat, salutaris -- heilend, heilsam:

    UNTER_BEDEUTUNG proprie:
    
        * WILH. SALIC. chirurg.; 1,31 p. 314^D "sedato dolore applicetur medicamen #sanativ.|um (({sim.} * CONR. MEND. Attal.; p. 138,11 "multis ... medicaminibus magnis et parvis #sanativ.|is, curativis in vanum adhibitis ... gravitas morbi increvit"))." 
        
       * ALBERT. M. summ. theol. II; 8,31,2,2,1 p. 342^b,46 "impositio ... manus collationem virtutis curativae sive #sanativ.|ae significat." {al.}

    ANHÄNGER usu subst. {@sanativus_1}:

        * ABSAL. serm.; 17 p. 107^B "spiritualis ... languoris cura simile quiddam habet cum operibus curandis, quibus medici primo apponunt purgativa, secundo mitigativa, tertio #sanativ.|a, quarto conservativa."

        * ALBERT. M. top.; 1,4,5 p. 283^a,46 "#sanativ.|um ... est ... sanitatis effectivum, ut diaeta vel cibus."     

    UNTER_BEDEUTUNG in imag. et per compar.:

        * ALBERT. M. sent.; 2,11,6 p. 225^a,3 "custodia magis est medicina praeservativa quam #sanativ.|a; et ideo operabatur angelus ad hoc, quod non caderet homo."
        
        * CONR. SAXO spec.; 16 p. 486,13sqq. "lignum vitae est ... Iesus Christus, qui est et fructus vitae; huius autem folia #sanativ.|a sunt verba et documenta aedificativa; si autem #sanativ.|a sunt folia eius, quanto magis #sanativ.|us est ipse fructus {(v. notam ed.)}?" {al.}

BEDEUTUNG sanitatem indicans -- Gesundheit anzeigend:

    * ALBERT. M. top.; 1,4,5 p. 283^b,3 "illud ... alio modo dictum #sanativ.|um {@sanativus_2} dicitur sanitatis significativum, ut urina vel pulsus."

BEDEUTUNG ad sanitatem proclivis -- zur Gesundheit geneigt:

    * ALBERT. M. praedicam.; 5,4 | p. 111,53 "illi, qui ... operatione medicinae fiunt #sanativ.|i vel aegrotativi."; p. 111,61 "'dicuntur #sanativ.|i (sonativi {var. l.}) ((*ARIST.; p. 9^a,21 "ὑγιεινοί")), eo quod habeant potentiam naturalem, ut nihil' contra sanitatem 'a quibuslibet' levibus 'accidentibus patiantur'." {v. et {=>sanabilis/sanativus_1}}

SUB_LEMMA sanative

    GRAMMATIK adv.

    BEDEUTUNG   cum respectu sanitatis -- in Hinblick auf die Gesundheit:

        * ALBERT. M. top.; 1,4,5 | p. 283^a,43 "si sanativum {@sanativus_3} dicitur multipliciter, et #sanativ.|e"; p. 283^b,6 "casus, qui est #sanativ.|e, multipliciter dicetur etiam effective vel conservative vel significative sanitatis." 

AUTORIN Clementi