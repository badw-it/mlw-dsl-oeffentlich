LEMMA salsus

GRAMMATIK
      adi. I-II; -a, -um

FORM
      form.:
           superl.:
                -issimus: {=> salsus_1}

BEDEUTUNG sale, salis sapore affectus, salem continens, ἁλμυρός -- salzig, gesalzen, salzhaltig, Salz-:

      U_BEDEUTUNG adi.; usu subst.: {=> salsus_2; salsus_3; salsus_5}:
           UU_BEDEUTUNG proprie:
                UUU_BEDEUTUNG in univ.:
                     * BEDA temp. rat.; 29,6 "qui {(oceanus)} ... dulces fluminum occursus #sals.|is abunde commisceat ... undis."
                     * MAPPAE CLAVIC. ; 293 "sumes ... siliquas IV ... sandarace preparate ex pusca #sals.|a."
                     * THEOPH. sched.; 1,32 "viride #sals.|um non valet in libro."
                     * COMPOS. Matr.; 67,1 "sumis partem et mittis in {!aqua} #sals.|a diebus XXII
                     ((
                       * CIRCA INSTANS; p. 18a^v "fiat decoctio illius herbe {(sc. hellebori)} in a. #sals.|a."
                       {al.}
                     ))"
                     * REGIMEN san. Salern.; 159 "vigent {(sc. sapores)} tres: #sals.|us, amarus, acutus."
                     {persaepe}
                
                     ANHÄNGER in formula incantationis:
                          * RECEPT. Sangall. I; 138 "nera, nela, neria, nerella, que, lei, #sals.|a iacis, vivat filius illi, verba ista ter dicere debes."
       
                UUU_BEDEUTUNG de cibis, potibus:
                     * DIAETA Theod.; 306 "#sals.|um vinum caput scotomat, sitim incitat."
                     * IOH. S. PAUL. diaet.; 501 "pisces sicci et #sals.|i calidi sunt."
                     * CONST. imp. II; 439,13 "quod duo panes tocius generis secundum forum annone bene pixti, #sals.|i ac cribrati dentur pro I denario."
                     * ALBERT. M. animal.; 3,81 " meliorem lanam et molliorem faciunt {oves} ex pascuis tenuibus #sals.|is quam ex uberibus et bulcibus."
                     {al.}
                     ANHÄNGER usu subst.:
                          * AESCULAPIUS; 5 p. 10,14 "dimittat {(sc. melancholicus)} #sals.|um {@ salsus_2}, raphanum, cepulas, allium."
                          * CHRIST. cal. Mog.; 3 ((MGScript. XXV; p. 241,11)) "habebat ... calix duas ansas ..., sicut solent habere lapides mortarii, in quibus piperata et #sals.|a {@salsus_5} preparantur."     

                UUU_BEDEUTUNG de corporis umoribus:
                     * CARM. Walahfr. app.; 5,5,8 "luminum amborum #sals.|am profundite {(sc. omnes)} undam."
                     * WOLFHARD. Waldb.; 3,1 p. 260,5 "a tetris luminum cavernis #sals.|a lacrimarum flumina fundens."
                     * OLBERT. Veron.; 15 "sudor ... #sals.|us membra humectavit."
                     * PLATEAR. pract.; 3,8 "cotidiana {(sc. febris)} alia fit ex {!fleumate} naturali ..., alia ex #sals.|o
                     ((
                     *MAURUS phleb.; p. 17^a "laborans cotidiana {(sc. febre)} de flegmate #sals|o vel dulci."
                     {al.}))"
                          
                UUU_BEDEUTUNG de morbis:
                     * CONSTANT. AFRIC. eleph.; 2,29 "pustule sicce et #sals.|e sunt."
                     * ALBERT. M. animal.; 3,120 "quaedam {(melancholia)} ... separatur a subtili fleumate ..., et haec est #sals.|a."  
                     ; 23,80 "quando ... falco levato pede saepe percutit cum rostro crus suum, scias, quod habet guttam #sals.|am."
               
                UUU_BEDEUTUNG de agro infertili:
                     * ALBERT. M. veget.; 7,36 "efficitur ager pulverulentus et sabulosus et #sals.|us et pessimus."
                     ANHÄNGER fort. huc spectat (usu subst.):
                           * DIPL. Karoli M.; 234 p. 323,20 "omnem ripaticum, sive in #sals.|o {@ salsus_3} sive in dulci, ... perdonamus."
                UUU_BEDEUTUNG de aqua non potabili:
                     * VITA Hildulfi; 13 "turbis irruentibus reperta est aqua #sals.|issima {@ salsus_1}."
                     * CONSTANT. AFRIC. theor.; 5,99 p. 24a^r "aqua non sapida est #sals.|a et sulphurea et picea."
                                   
                UUU_BEDEUTUNG iuncturae "fons -a, fossa -a, aqua -a" sim.; 'Salzquelle', '-grube':
                     * COD. Lauresh.; 3335 "dono {(sc. Warinus)} ... II fontes #sals.|os ad salem faciendum."
                     * DIPL. Otton. I.; 232^a p. 318,25 "urbem ... Giuiconsten cum salsugine eius ceterasque urbes cum ... {!aquis} #sals.|is et insulsis, terris cultis et incultis {(sc. donamus)}
                     ((
                          *{sim.} DIPL. Otton. III.; 100 p. 512,7" confirmamus ... Lauretum, ut quantum #sals.|a aqua continet, eorum {[Veneticorum]} subiaceat potestati." {al.}
                     ))."
                     * DIPL. Heinr. III.; 317 p. 434,45 "curtem Sarmadas cum semenia et puteo #sals.|o."
                     * CHART. Bern.; $II 16 p. 25,9 "de Einge ... a fovea #sals.|a usque ad viam, que {eqs.}"
                     {saepe.}

                     ANHÄNGER in nomine:
                          * THIETM. chron.; 4,45 "commitens {(sc. caesar)} eundem {(archiepiscopatum)} ... Radimo eidemque subiiciens Reinbernum #S|.als.|ae Cholbergiensis aecclesiae episcopum."
                          * ACTA civ. Wism. A; 28^a "dominus Olricus e<mit> d<omum> Heynrici de Bocholte super #S|.als.|am Foveam {('Soltegrove')}."; 377 "Hinricus de #S|.als.|a Fossa."; B; 86 "Herbordus Turingus super #S|.als.|am Fossam impignoravit domum suam."

           UU_BEDEUTUNG translate:

                UUU_BEDEUTUNG in univ.:
                     * EKKEH. IV. bened. I; prol. 2,84 "si ... quis incęptum quis tale loquatur ineptum ..., dixeris insulsum (gloss.: insipidum) seu fellis (gloss.: invidię) acredine #sals.|um (gloss.: pro sallitum)."
                     * IOCALIS ; 407 "est melior #sals.|a quam sit dileccio falsa."        

           
                UUU_BEDEUTUNG argutus, facetus -- scharfsinnig, geistreich; in malam partem: {=> salsus_4}:
                     * SEDUL. SCOT. carm. I; app. 2,43 "carmina musidica pollentia famine #sals.|o, ludifero sensu aedificata colit."  
                     * EGBERT. fec. rat.; 1,216 "ingenio #sals.|o preceps violentia cedit."
                     * CARM. pro schola Wirz.; 138 "omnes devincis {(sc. sator irarum)} ... spernendo falsos suprave modum male #sals.|os{@ salsus_4}."
                     ANHÄNGER acc. sg. neutr. pro adv. i. q. prudenter -- klug:
                     * BENZO ad Heinr. IV.; 7,2 p. 588,1 "seniores Romani, licet hactenus sive #sals.|um sive insulsum elegistis, ... quęmcumque ... voluistis."
                   
      U_BEDEUTUNG subst:
           UU_BEDEUTUNG fem.:

                UUU_BEDEUTUNG ius -- Suppe:
                     * WILH. RUBRUQU. itin.; 3,2 "scindunt ... {(sc. Tartari carnem)} minutatim in scutella cum sale et aqua; aliam ... #sals.|am non faciunt."
                     * REIN. ALEM. phagifac.; 213 "quidam si piperis capiunt in flumine pisces aut in #sals|arum tingunt acrimine carnes, plus digitis quam pisce bibunt vel carne liquoris."
                    
                UUU_BEDEUTUNG   liquamen (salsum) -- salzige Sauce, Salzlake:
                      * THIETM. peregr.; 29,3 "sunt ibi {(sc. in terra Ierosolimis)} limones arbores, quarum fructus acer est et valet ad #sals.|am {(v. notam ed.)}"
                      ANHÄNGER fort. huc spectat:
                          * CHART. Argent.; $IV 35 p. 43,17 (a. 1224/28) "portarius ... dabit ..., quicquid necessarium est in coquina, scilicet in lignis et sale et pipere et oleo et acceto et #sals.|a et scutellis."
               
           UU_BEDEUTUNG neutr. i. q. mare -- Meer:
                * NARR. itin. nav.; p. 194,16 "ventorum iniurias iugiter experti diutius in #sals.|o fluctuavimus."      
              
AUTORIN Weber