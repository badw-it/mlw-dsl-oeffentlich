LEMMA  *sabelinus

    (cib-, cimb-, geb-, zab-, zeb-, zob-, -bil-, -bol-, -lli-)

GRAMMATIK  adi. I; -a, -um

BEDEUTUNG  ad sabelum pertinens -- Zobel-:

    U_BEDEUTUNG  adi.:

        * CHART. Westph.; I 87,25 (a. 1015) "dedit episcopus I marthrinum pelliz pro VI tal. et I #ze|.belin.|um roc pro VI tal. (({inde} * VITA Meinw.; 123 "unam #ze|.belin.|am tunicam"))."

        * LAND. MEDIOL. hist.; 2,18 p. 56,4 "cum ... archiepiscopus magno ducatu militum stipatus, quos pellibus martulinis aut #ci|.belin.|is aut renonibus variis et hermelinis ornaverat, ... super cicotergitronum sederet {eqs.}"

        * PETR. WILH. Aegid.; (MGScript. XII p. 320^a,48) "reduc {(sc. claudus)} in memoriam abbati, quia anno praeterito ... pelles #cim|.belin.|as ei optuli {(sc. vir nobilis)}."

        * ARNOLD. LUB. chron.; 1,5 p. 120,28 "addidit regina cuilibet militi pelles varias et pelliculam #zobi|.lin.|am (#zobo|.lin.|am {var. l.})."


    U_BEDEUTUNG  subst. masc. i. q. sabelus -- Zobel (({Martes Zibellina, de re v.} Grzimeks Tierleben. {@ Grzimek001} XII. 1979.; p. 58sqq.)):

        * PETR. DAM. epist.; 48 p. 58,1 "non ... constat episcopatus in turritis #ge|.belin.|orum transmarinarumve ferarum pilleis."; 97 p. 76,14 "ovium ... et agnorum despiciuntur exuviae, ermelini, #gebellini, martores exquiruntur et vulpes."

        * ALBERT. BEH. epist.; 135 "colleccio pellicularum cuiuslibet hermini vel #zabolini in vulgari per mercatores zimber appellatur."

            ANHÄNGER  in cognomine:

                * TRAD. Patav.; 974 (a. 1218/21) "rei testes sunt: ... Heinricus iunior #Zo|.belin.|us, Heinricus de Ekkenheim {eqs.}"; 1002 "magister Hainricus #Zo|.belin.|us."; 1003.



AUTOR  Fiedler