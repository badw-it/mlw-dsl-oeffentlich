LEMMA  saburra

GRAMMATIK  substantivum I; -ae f.

VEL  {semel ({=> saburra003})} *saurra

GRAMMATIK  substantivum I; -ae f.

SCHREIBWEISE

    script.:

        sub-: {=> saburra001; saburra002}

BEDEUTUNG  sabulum, onus sabuli in sentina navis positum -- (Schiff-)Sand, Ballast:

    U_BEDEUTUNG  proprie:

        * ENGELM. carm.; 3,176 ((MGPoet. III; p. 66;; s. IX.^med.)) "iam tempus adest funem religare carinae excursumque levem morsu stabilire #saburrae {(v. notam ed.)}."

        * ANNAL. Ianuens.; I p. 118,3 "cum tota #saurra {@ saburra003} et vianda ... naufragium passa fuit {(sc. navis)}."

            ANHÄNGER  per compar.:

                * PETR. DAM. epist.; 8 p. 120,2 "quasi #saburra quaedam lembum mentis meae praeponderat."

            ANHÄNGER  in imag.:

                * AMARC. serm. orat.; 5 "o sator, o soboles, o sancte spiritus, ... da, precor, ut superę compos sit Amarcius aulę apprendens fidei litus celeste #saburra {(v. notam ed.)}."

                * HUGO TRIMB. laur.; 266 "Rufe, Dei cultor, ... nobis succurras scelerum removendo #saburras."

    U_BEDEUTUNG  translate i. q. turba -- Menge:

        * ODO MAGDEB. Ern.; 4,139 "quam {(navem)} dum multa Ceres et rerum pulcra supellex implesset, tota pro portu stante #suburra {@ saburra001} (#sa|.burra {ed. princ.}) dux a rege dato ... sibi ... auro ... grates egit {eqs.}"

BEDEUTUNG  ancora -- Anker:

    * PASS. Ursulae; 10 "levantes #saburram (#su|.burram {@ saburra002} {var. l.}) felici remigio ... applicabant {iuvenis et puella} Coloniam."

    * CONR. MUR. nov. grec.; 4,254 "anchora sitque locus navis quandoque #saburra."

        ANHÄNGER  in imag.:

            * WALTH. SPIR. Christoph.; II 4,245 "corporis et mentis clavum ratione gubernans {summus rex} naufragio pelagi solidis navalibus acto annectit validam fluctu tollente #saburram, ne ruat in tumidas navis iactata procellas {(v. notam ed.)}."; 5,240 "simul capta dominantur {(sc. Nicea et sodalis)} in aethere palma pro reditu summo referentes vota patrono, in cuius portu validam fixere #saburram."

AUTOR  Fiedler