 LEMMA sacerdos
 GRAMMATIK subst. III; -otis m. vel raro {({=> sacerdos_1; sacerdos_2})} f.
 SCHREIBWEISE
     script.:
         -rto(s): {=> sacerdos_3}
         -dus: {=> sacerdos_4; sacerdos_5} {adde} * TRAD. Frising.; 29 p. 57,19.
          {al. (usque in s. VIII.^ex.; cf. Stotz, Handb. 3,VII; § 40.2)}

 BEDEUTUNG ἱερεύς, ecclesiasticus -- Priester, Geweihter:
 
     U_BEDEUTUNG apud christianos:
         UU_BEDEUTUNG de ordinibus inferioribus (fere sub ordine episcopi):
             UUU_BEDEUTUNG in univ.:
                 UUUU_BEDEUTUNG usu communi:
                     * CHART. Alsat. B; 66 (s. $VII.^ex.) "dux ... #sacerdot.|es ... et de Lotharingia convocari praecepit."; 88 p. 38,14 "ut ... #sacerdo.|s ad baptizandum vel missas celebrandum veniat."
                     * EIGIL Sturm.; 22 p. 158,17 "assumptis universis #sacerdoti.|bus, abbatibus, presbyteris."
                     * PONTIF. Rom.-Germ.; 85,5 "debent ... XII #sacerdot.|es episcopum circumstare."
                     * CHART. civ. Erf.; 22 p. 10,17 "quicunque #sacerdo.|s in ea {(ecclesia)} constituetur {eqs.}"
                     * LIBER ordin. Patav.; 2,12 p. 965,13 "#sacerdot.|em opportet offerre et benedicere, praeesse et praedicare et baptizare et communicare."
                     * CHART. Gosl.; I 473 "promotus est Gerhardus plebanus Gandersheimensis et canonus ecclesie ... in #sacerdot.|em." {persaepe. v. et {=> sacerdotalis/sacerdos_13; sacerdotium/sacerdos_14; sacerdotium/sacerdos_15; sacerdotalis/sacerdos_16}}
                    
                    ANHÄNGER in appos.:
                         * MIRAC. Genes. Hieros.; 2 p. 12,27 "Pippinus rex Langobardorum Ratodum tunc principem palatii sui, #sacerdot.|em, ... Darvisiam misit."
                         * CAND. FULD. Eigil. II; 17,97 "monachus ... #sacerdo.|s Ercanbertus."
                 
                 UUUU_BEDEUTUNG de certis #sacerdoti.|bus (exempla selecta afferuntur):
                     * RUOTG. Brun.; 47 p.50,14 "quo {(sc. ad corpus episcopi)} paucis nisi episcopis et quibusdam secundi ordinis #sacerdoti.|bus patebat accessus."
                     * CHART. Naumb. I; 124 p. 109,5 "abs ... assensu plebani #sacerdot.|is."
                     * TRAD. Teg.; 272 "Gotefridus #sacerdo.|s ministerialis."
                     * CHART. Westph.; VII 1481 "vos {(sc. conventus)} per secularem #sacerdot.|em eandem ecclesiam offitiabitis
                    ((
                      *{sim.} ACTA civ. Wism. A; 1131^b p. 87,25 "cuilibet #sacerdot.|i seculari ... do quatuor solidos"
                     ))."
                     * CONR. MUR. summ.; p. 38,19 "simplex #sacerdo.|s."
                     * CHART. Xant.; 251 p. 163,35 "qui #sacerdo.|s ... ad omnia et singula teneatur, ad que #sacerdot.|es, qui dicuntur elemosinarii, sunt astricti."
                     * CHART. Austr. sup. II; 508 p. 468,27 "que {(memoria)} habetur pro nostris confratribus #sacerdoti.|bus regularibus." 
                     {v. et {=> sacerdos_6} {=> sacerdotalis/sacerdos_7}. vol. $II. p. 1275,5. $IV. p. 379,49. p. 961,1.}
               
                 UUUU_BEDEUTUNG proverb.:
                     * WALAHFR. Wett.; 361 "plebis erit similis stultae peccando #sacerdo.|s
                      ((
                          {spectat ad} * $VULG.; Is. 24,2
                          ))."
                 UUUU_BEDEUTUNG in formula modestiae:
                     * AGIUS vita Hath.; 1 "Agius, quamvis indignus, gratia Christi #sacerdo.|s {@ sacerdos_8}."
                     * THIETM. chron.; 8,11 "heu mihi indigno #sacerdot.|i. {@ sacerdos_11}"
                     * HERM. IUD. conv.; 1 p. 70,16 "peccator ego et indignus #sacerdo.|s Hermannus, Iudas quondam dictus."
                 UUUU_BEDEUTUNG de abbate:
                     * WALAHFR. Wett.; 32 "sequitur {(sc. abbas Augiae)} binis laudandus Geba #sacerdo.|s (presbiter {var. l.})."
                     * CHART. Port.; 61 p. 82,8 (privil. papae) "licitum sit eidem abbati,
                    si tamen #sacerdo.|s fuerit, proprios novitios benedicere." {v. et {=> sacerdos_8}.}
                 UUUU_BEDEUTUNG in actione iur. (usque in s. $VIII.^ex., praeter {=> sacerdos_12}):
                     * LEX Alam.; 3,1 "#sacerdot.|em ecclesiae interpellet {(sc. qui servum fugitivum persequitur)} pro servo suo."; 6 "coram #sacerdot.|e vel ministros eius." 
                     * VITA Goar.; 127 p. 419,8  "accipe {(sc. episcopus)} duram et prolixam penitentiam iudicio #sacerdot.|um (#sacer.|totum {@sacerdos_3} {var. l.})."
                     * CHART. Fuld. B; 39 p. 68,15 "iram Dei incurrat et a manibus #sacerdot.|um et liminibus ecclesiarum excommunis appareat {(sc. qui contra donationem venire voluerit)}."
                     * BERTH. chron. B; a. 1077 p. 273,3 "dum quorundam #sacerdot.|um {@sacerdos_12} iudicia ... ab episcopo illo {(sc. Ulmae)} recipi solere comperissent {legati apostolici eqs.}" {al.}
                    
                     ANHÄNGER inter testes:
                     * CHART. episc. Hild.; I 358 (a. 1172) "testes sunt primi capitales Lamespringenses ęcclesię, videlicet #sacerdot.|es Heinricus, Adelbertus {eqs.}"
                     * TRAD. Ratisb.; 1042 "testes sunt Herman plebanus noster, Heinr<icus> #sacerdo.|s ipsius, Heinr<icus> decanus."
                     * CHART. civ. Halb.; 29 p. 38,19 "testes sunt: Gerardus #sacerdo.|s {@ sacerdos_6} de foro, Albertus #sacerdo.|s in Horthorp." {saepius.}
                 UUUU_BEDEUTUNG in iunctura "gradus -is, ordo #sacerdot.|um" sim.:
                     * WILLIB. Bonif.; epil. p. 58,37 "dum #sacerdot.|is gradum suscepit vinum et siceram non bibens, utriusque testamenti imitatus est patres."
                     * WALAHFR. Wett.; 320 "maior et alter in undis {!ordo} #sacerdot.|um praefixo stipite vinctus  ((carm. 77,16,1. {al.}))"
                     * RIMB. Anscar.; 28 capit. p. 16 "ut ibi {(sc. in vico Byrca)} #sacerdot.|is officio fungeretur {Erimbertus}."
                     
                 UUUU_BEDEUTUNG gen. pro adi. i. q. a sacerdote benedictus -- vom Priester geweiht:
                     * PS. OTHO med.; 355 "sale #sacerdot.|is capitis dolor et malus oris cedit odor."
                 UUUU_BEDEUTUNG locut. "non-#sacerdos":
                     * ALBERT. M. sacram.; 162| p. 106,40 "non-#sacerdoti explanare peccata non tenetur."; p. 106,44sq. "propter periculum sigilli confessionis, quod non-#sacerdotes nesciunt conservare; ex his patet, quod non-#sacerdotes in confessionem nequeunt absolvere, quia {eqs.}"; euch.; 6,4,2 p. 423^b,27 "si non-#sacerdos possit conficere {(sc. sacramentum)} {eqs.}"
             UUU_BEDEUTUNG monachus -- Mönch {(spectat ad utrumque sexum: {=> sacerdos_1})}:
                 * WIDUK. gest.; 2,37 "ut plures {(sc. monachi)} ... deposito habitu et relictis monasteriis grave onus #sacerdot.|um devitarent."; 3,46 p. 127,4 "utriusque sexus #sacerdot.|es {@ sacerdos_1} ictu fulminis interierunt."
             UUU_BEDEUTUNG fem. i. q. sanctimonialis -- Nonne:    
                 * YSENGRIMUS; 1,229 "Gręca salix posses {(sc. Reinardus)} prius esse aut Daca #sacerdo.|s {@ sacerdos_2} {(v. notam ed.)}."
                 {v. et {=> sacerdos_1}}
         
         UU_BEDEUTUNG de (archi)episcopo {(papa: {=> sacerdos_10; sacerdos_17}. al.)}:
              UUU_BEDEUTUNG in univ.:
                 * EIGIL Sturm.; 16 p. 150,13 "ad coenobium Fuldam sanctum #sacerdot.|is {(sc. Bonifatii)} deportaverunt cadaver."
                 * HRABAN. carm.; 7,1 "pontificalis apex primus et in orbe #sacerdo.|s{@ sacerdos_10}, Petri successor."
                 * HRABAN. inst. cler.; 1,5 p. 299,34 "#sacerdo.|s ... vocari potest, sive episcopus sit sive presbyter."
                 * RUOTG. Brun.; 25 p. 25,28 "pepercit ... in omnibus #sacerdot.|i suo ... Dominus."
                 * THIETM. chron.; 2,26 "hunc {(Guntherium custodem)} ... cum consilio cleri tociusque populi ad #sacerdot.|em constituit {(sc. imperator)}."; 6,40 p. 324,11 "in regis presentia crismate in #sacerdot.|em perunctus sum VII. kal. Mai."
                 {persaepe. v. et  {=> sacerdos_11}}
            
             UUU_BEDEUTUNG abund.:
                 * TRAD. Frising.; 1 p. 21,17 (a. 744) "in loco Frigisinga, ubi Ermbertus episcopus #sacerdo.|s praeesse dinoscitur."
                 * DIPL. Karoli III.; 17 p. 27,32 "cum omnibus habitantibus tam episcopis #sacerdoti.|bus
                 quam et primatibus seu et reliquo populo."

             UUU_BEDEUTUNG in iunctura "summus #sacerdo.|s":
                 * HUGEB. Willib.; 1 p. 88,7 "summi ... Dei #sacerdot.|is atque pontificis primordium ... decrevero ordinando ... texere."
                 * HRABAN. carm.; 2,1,3 "tu {(sc. Haistulfus)} decus es nostrum, doctor summusque #sacerdo.|s."
                 * ADALB. SAMAR. praec. dict.; p. 51,15 "ante tempora ... beati Gregorii alii {(sc. papae)} prime sedis pontifices, alii summi #sacerdot.|es {@ sacerdos_17}, alii universales se in epistolis appellabant."
                 {al.}

         UU_BEDEUTUNG de Deo, Christo:
             * CONC. Merov.; p. 166,4 "pascha ...., in quo {!summus} #sacerd.|us {@sacerdos_4} ac pontifex ... immolatus est
             ((
             *{sim.} EPIST. divort. Loth.; 13 p. 229,43 "ineluctabili sacerdotali {@ sacerdotalis_11} auctoritate, qua ligandi atque solvendi
             potestatem a summo #sacerdot.|e Christo Domino percepimus. {al.}"
             ))"
             * WALAHFR. exord.; 17 p. 490,7 "Christus #sacerdo.|s esse dicitur secundum ordinem Melchisedec."
             * LUP. FERR. epist.; 81 p. 73,6 "rex regum idemque #sacerdo.|s #sacerdot.|um, qui solus potuit ecclesiam regere, quam redemit."
             * LIBER ordin. Patav.; 82,12 p. 966,9 "#sacerdo.|s {(sc. fuit Christus)}, quando post caenam panem et vinum in corpus et sanguinem suum commutavit."
             * CHART. Basil. C; I 437 p. 321,1 "idem ipse {(sc. Christus)} libamen et hostium, sacrificium et #sacerdo.|s." {al.}
         UU_BEDEUTUNG de quolibet homine:
             * PETR. DAM. epist.; 145 p. 528,20 "constat ... quemlibet christianum esse per Christi gratiam #sacerdot.|em {(v. notam ed.)}."
             ANHÄNGER pro cognomine: * ACTA civ. Wism. B; 685 (a. 1278) "decretum inter uxorem Heylewigam et pueros ipsius et dominum Henricum #S|.acerdot.|em ..., quod dominus Henricus #S|.acerdo.|s et sui fratres habebunt 9 1/2 m<orgen>."; 896 "hanc hereditatem susceperunt dominus Iohannes #S|.acerdo.|s dictus sutor et Conradus famulus ipsius Henrici Albi ex parte eius."
    ABSATZ
     U_BEDEUTUNG apud paganos:
         UU_BEDEUTUNG in univ.:
             * CONST. Constant.; 85 "advenerunt #sacerdot.|es Capitolii."
             * WALTH. SPIR. Christoph. II; 5,69 "iacet augur Apollo cum Iovę, et incassum tenet intestina #sacerdo.|s."
             * OTTO FRISING. chron.; 7,3 p. 312,22 "cum de imperio debeat {(sc. Baldach, pars antiquae Babylonis)} esse Persarum, summo #sacerdot.|i suo, quem ipsi {(Persae)} Caliph dicunt, a regibus Persarum concessa, ut {eqs.}"
             * VOLQUIN. serm.; 220 ((StudMediev. $III/3. 1962. p. 329)) "habet ... Beelphegor #sacerdot.|es suos, Dagon suos et Baal suos."
             * CONST. imp. II; 153,3 "quod ... pacem inierint {populi} cum domino nostro Califa #sacerdot.|e imperatore fidelium {(v. notam ed.)}."
         UU_BEDEUTUNG apud Iudaeos:
             UUU_BEDEUTUNG ἀρχιερεύς -- Hohepriester:
                 * CHRON. Fred.; 2,7 "post quem {(Samsonem)} Heli #sacerd.|us {@sacerdos_5} {(sc. fuit)}."
                 * WALTH. SPIR. Christoph. II; 2,246 "divino pollens Aaron sub amore #sacerdo.|s."; 4,211 "Melchisedech primus veraxque in rege #sacerdo.|s."
                 ANHÄNGER in iunctura "summus #sacerdo.|s":
                 * OTTO FRISING. chron.; 2,47 p. 123,23 "populus ille {(sc. Iudaeus)} post illud {(sc. captivitatis Babylonicae)} tempus  non solum summos #sacerdot.|es, sed etiam reges habuit."
             UUU_BEDEUTUNG de propheta:
                 * WIDUK. gest.; 1,31 p. 44,3 "cum Samuelem sanctum et alios plures #sacerdot.|es ... legamus et iudices."
                 * VITA Adalb. Prag. A; 1 "ambulans sollicite iuxta precepta #sacerdot.|um
                 ((
                 {spectat ad} * $VULG.; Mich. 6,8
                 ))."    
 VERWEISE {cf.} consacerdos       

 AUTORIN Weber

UNTERDRÜCKE WARNUNG 516