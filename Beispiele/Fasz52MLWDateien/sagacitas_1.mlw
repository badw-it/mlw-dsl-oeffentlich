LEMMA sagacitas
GRAMMATIK
     subst. III; -atis f. 

SCHREIBWEISE
     script.:
           -gati-: {=> sagacitas_2; sagacitas_3; sagacitas_4} *{adde} WOLFHER. Godeh. I; praef. p. 169,39.
           -cetas: {=> sagacitas_1}

BEDEUTUNG sensus, vigilitas -- Spürsinn, Wachsamkeit:
      U_BEDEUTUNG animalium:
           * PETR. DAM. epist.; 40 p. 441,13 "timens {(sc. Hieremias)} ..., ne venationibus deditam canum suorum #sagacitat.|em hostile prestigium fassinaret."
           * ALBERT. M. animal.; 5,64 "polipus ... mirabili #sagacitat.|e pede anteriori complectitur lapidem."; 8,50 " est ... et alia animalium #sagacita.|s in praesentiendo nocumento."; 21,45^capit. "de #sagacitat.|e et perfectione anulosorum."
           * FRID. II. IMP. art. ven.; 1 p. 38,22 "sunt {(sc. aves fortiores)} ... tante #sagacitat.|is et industrie, quod {eqs.}"
           ANHÄNGER c. gen. explicativo:
                 * ALBERT. M. animal.; 21,30 "quae {(sc. perfectio piscium)} ex participatione est virium motivarum, quae sunt ... providentia et #sagacita.|s cuiusdam coniecturationis."
ABSATZ
      U_BEDEUTUNG hominum i. q. acies, argutiae, prudentia -- Geistesschärfe, Scharfsinn, Klugheit:
          UU_BEDEUTUNG strictius:
               UUU_BEDEUTUNG proprie:
                     UUUU_BEDEUTUNG in univ.:
                     * LIUTG. Greg.; 2 p. 68,4 "intelligens sanctus magister #sagacitat.|em pueri et facundiam."
                     * CAND. FULD. Eigil. I; 10 p. 227,52 "cum omni studio et #sagacitat.|e."; 20 "omni ... industria et #sagacitat.|e laborabat, ut {eqs.}"
                     * EKKEH. I. (?) Wibor.; 4 p. 36,3 "in bonis omnibus se studiosa #sagacitat.|e exercuit."
                     * OTTO FRISING. gest.; 2,13 p. 116,5 "quod indigenis per conubia iuncti filios ex materno sanguine ... aliquid Romanae mansuetudinis et #sagactitat.|is trahentes genuerint {(sc. Langobardi)}."
                     * CHART. Port.; 121 p. 145,17 "ne res dignas memoria successione temporum ... contingat ... obfuscari, convenit easdem presentium #sagacitat.|e et scripti testimoniis eternari."
                     {al.}
                        
                      ANHÄNGER usu plur.: * CHRON. Salern.; 90 p. 91,22 "de genealogia seu de #sagacitat.|bus Melfitanorum nos dicta sufficiant."         
                              
                     UUUU_BEDEUTUNG c. gen. explicativo:
                          * HUGEB. Willib.; 2 p. 89,23 "cum ... in eo iam simul annorum aetas {!mentisque} #sagacita.|s (#saga.|titas {@ sagacitas_2} {var. l.}) divinae dispensationis moderamine pululare ceperat
                          ((
                             * GERH. AUGUST. Udalr.; prol. l. 18 "non meis antecedentibus meritis confidens nec #sagacitat.|i mentis meae."
                             * HERM. AUGIENS. epist. comput.; 73 ((Dt. Arch. 40. 1984.; p. 476)) "in aliqua mentis #sagacitat.|e"
                          ))."
                          * HROTSV. Cal.; 9,6 "divini subtilitas iudicii longe praeterit humani #sagacitat.|em ingenii."
                          
                UUU_BEDEUTUNG meton. de homine (in allocutione honorifica):
                     UUUU_BEDEUTUNG in univ.:
                          * EPIST. Bonif.; 49 p. 79,15 "illud etiam scire desideramus {(sc. Denhardus, Lullus et Burchardus)} tuae #sagacitat.|is {(sc. abbatissae)} industriam, quod si {eqs.}"
                          * LULL. epist.; 103 p. 226,1 "rusticitatis mee litteras #sagacitat.|is (#saga.|titatis {@ sagacitas_4} {var. l.}) vestrę celsitudini {(sc. Bonifatio)} dirigere."
                          * HETTI epist.; 2 "meminisse volumus #sagacitat.|em vestram {(sc. episcopi)} monitionis."
                          * WALAHFR. Mamm.; prol. 1 "meminit vestra #sagacita.|s {(sc. Anselmus, Wulfingus et Lantwinus)}  {eqs.}"
                          * DIPL. Conr. II.; 276 "iubemus, ut, si quando principalis munificentia vel iudicalis #sagacita.|s praefatę antiquioris legis severitatem in his {(sc. capite damnatis)} temperare ... voluerit {eqs.}"
                     UUUU_BEDEUTUNG in promulgatione q. d. chartarum:
                          * DIPL. Pipp.; 8 p. 12,39 "cognuscat omnium fidelium ... #sagac.|etas{@ sagacitas_1}, quia {eqs.}"
                          * DIPL. Otton. III.; 182 "omnium ... fidelium noverit #sagacitat.|is industria."
                          * TRAD. Ratisb.; 916 "omnium ... fidelium tam presentium quam futurorum #sagacitat.|i innotescimus, quod {eqs.}"
                          {saepe.}

           UU_BEDEUTUNG latius i. q. studium -- Eifer:
                * CHRON. Fred.; 4,60 "si huius rei {(sc. largitionis)} #sagacita.|s cupiditates instincto non prepedisset." 
                ANHÄNGER in malam partem i. q. aviditas, avaritia -- Ehrgeiz, Gier:
                     * CHRON. Fred.; 4,27 "nimia #saga.|titate {@ sagacitas_3} vexatus ... cunctos in regno Burgundiae locratus est {(sc. Protadius maior domus)} inimicus."

 
AUTORIN Weber
  