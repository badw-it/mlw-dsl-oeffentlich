LEMMA salvus   

GRAMMATIK
   adi. I-II; -a, -um  

BEDEUTUNG
   adi.:

U_BEDEUTUNG
   integer, illaesus -- unversehrt, unverletzt:

UU_BEDEUTUNG
   de anim. eorumque actibus i. q.  sanus, incolumis -- gesund, wohlbehalten:

UUU_BEDEUTUNG
    in univ.:

    * ANTIDOT. Lond.; p. 20,37 "quicumque podagrosus usus fuerit istum medicamen per treginta dies, semper #salv.|us erit."

    * HUGEB. Wynneb.; 13 p. 117,15 "illa {(femina)} #salv.|a facta ... revixis artibus et sano corpore sacrum velamen adsumpsit."

    * EINH. Karol.; 9 p. 12,6 "#salv.|o et incolomi exercitu revertitur."

    * WIDUK. gest.; 3,69 p. 145,8 "fidem ... spondent {(sc. hostes)} #salv.|um eum {(Wichmannum)} domino suo presentari."

    * GERH. AUGUST. Udalr. ; 1,1 l. 18 "si #salv.|um illum esse cupiatis {(sc. parentes)}, caeleriter ablactetur."

    * CHART. Bund.; 592^b| p. 101,26 "persone de Clavenna ... debent esse #salv.|i et securi ((* {item}; p. 102,18))."

    * FRID. II. IMP. art. ven.; 2 p. 233,23 "ut penne et pedes #salv.|iores sint."
    {saepe.}


UUU_BEDEUTUNG
    securus -- sicher:

UUUU_BEDEUTUNG
   gener. de volatu falconum:

    * FRID. II. IMP. art. ven.; 4 p. 106,6 "talis ... volatus pulcrior et #salv.|ior, quia minus ledentur {falcones} a grue."

UUUU_BEDEUTUNG
    publ. et iur.:

    * LEX Raet. Cur.; 9,34 "quicumque culpabilis ad aeclesiam confugium fecerit, licead ei sive in aeclesia sive in portica aeclesiae ... #salv.|i esse debeant."

    * GISLEB. MONT. chron.; 144 p. 222,15 "comes ... absque #salv.|o conductu ad ... regem transire noluit."

    * CHART. civ. Erf.; 241 "duos mansos ... hospitali sancti Martini ... tradidi {(sc. Heinricus)} ... volens ipsum hospitale ...  ab omni impetitione #salv.|um reddere ... et indempne."


UUU_BEDEUTUNG
   in fide christiana i. q. saluber, redemptus, beatus -- heil, erlöst, selig:

   * AMALAR. ord. antiph.; 36,2 " antiphona 'o, Domine, #salv.|um fac me' ... postulat ..., ut Dominus abstinentem #salv.|um faciat a peccatis."

   * RUOTG. Brun.; 34 p. 35,29 "ut spiritus in die Domini #salv.|us esse possit."

   * CAES. HEIST. hom. exc.; 298 "Arsenio divinitus dictum est: 'si vis #salv.|us esse, fuga homines et tace'."
   {saepe.}


ANHÄNGER
    de exorcismo:

    * WETT. Gall.; 18 "exivit {(sc. daemon)} de ore eius quasi turpissima avis nigra; ... #salv.|a facta puella surrexit."


UU_BEDEUTUNG
   de rebus i. q. intactus, reservatus -- unvermindert, vollständig, unangetastet, unbeschadet {(saepe usu abl. abs. ['ohne Verletzung, unter Wahrung, Einhaltung, dem Vorbehalt (von)'])}:

UUU_BEDEUTUNG
    gener.:

UUUU_BEDEUTUNG
    in univ.:

    * ERMENR. Sval.; 3 "ast ego #salv.|a caeterorum estimatione dico eum melius monachum ... posse nuncupari, id est Solum, quam Solonem."

    * EPIST. Teg. I; 39 "ut ... hoc perficiatur #salv.|a fide inter utrosque."

    * IDUNG. PRUF. argum.; 817 "hoc ... #salv.|a honestate dicere possumus, quod {eqs}."

    * CHART. Wirt.; 1591 "litteras domini pape ... stilo, filo et bulla sanas et #salv.|as de verbo ad verbum legi fecimus {(sc. episcopus)}."

    * ANON. IV mus.; 7 p. 85,2 "iterato diversitas altera et maior: una longa nimia et tres mediocres ... et tres festinantes #salv.|a ultima, quae dicitur mediocris vel nimia."
    {saepe.}

UUUU_BEDEUTUNG
    purus, merus -- rein, unvermischt:

    * COMPOS. Luc.; S 28 "qui {(sc. stagnum)} ipsum metallum ad opera<m> #salv.|am {@ salvus_1} producit."


UUU_BEDEUTUNG
  publ. et iur.:

UUUU_BEDEUTUNG
  in univ.:

  * LEX Raet. Cur.; 3,19,4 "ubi tutores res parvulorum recipent ad gobernandum ..., scriptam #salv.|am ambe partes habere debent."

  * CHART. Rhen. med.; I 65 p. 73,37 "quicquid iuste et rationabiliter #salv.|o {! iure} ęcclesiastico de hac commutatione terrę utraque pars facere voluerit ((* DIPL. Loth. III.; 106 "#salv.|o ... i. nostro." {saepe}))."

  * TRAD. Ratisb.; 157 p. 122,6 "post ... obitum #salv.|a omnia et intemerata servitio supradictorum sanctorum cedent."

  * DIPL. Karoli III.; 17 p. 29,29 "ipsum pignus #salv.|um sit usque ad supra nominatas noctes."

  * DIPL. Otton. II.; 287 p. 334,36 "ut ... liceat ... abbati ... scriptiones  ... facere iuste et legaliter atque ... secundum voluntatem sui animi ordinare, #salv.|o videlicet censu ipsius ecclesię monasterii."

  * CHART. cell. Mar.; 9 p. 17,1 "#salv.|a sedis apostolice auctoritate."
  {persaepe.}

UUUU_BEDEUTUNG
   constitutus, ratus -- festgesetzt, vereinbart:

   * CHART. archiep. Magd.; 379 p. 496,26 (a. 1182) "remisimus {(sc. archiepiscopus)} nichilominus censum, qui ad nostram iusticiam spectabat, de #salv.|a aqua ad conficiendum sal in casa cimiterio adiuncta."

ANHÄNGER
   locut. {/#salv.|um facere} i. q. confirmare -- zusichern, bestätigen:

   * DIPL. Frid. I.; 533 "quod faciet {imperator} #salv.|a omnia iusta tenementa populi Romani ..., quod ea illis non auferet."


U_BEDEUTUNG
    exceptus -- ausgenommen, außer:

UU_BEDEUTUNG
    in univ.:

    * DIPL. Karoli M.; 191 p. 256,35 "concessimus Autlando abbati et monachis ..., ut ... in eorum proprias silvas licentiam haberent eorum homines venationem exercere ..., #salv.|as forestes nostras, quas ad opus nostrum constitutas habemus."

    * LAMB. TUIT. Herib.; 1,8 p. 164,13 "alter Helias #salv.|o Iohanne baptista ad nota pręsidia confugiens a Deo monuit esse quęrendum {(v. notam ed.)}."

UU_BEDEUTUNG
    locut. "#salv.|o (eo)" c. enunt.:

    * UFFING. Ida; 1,6 "nunquam extra ianuam ... pedem fertur amovere, #salv.|o eo, dum inopum stipendiis deserviret."

    * FRID. II. IMP. art. ven.; 3 p. 38,10 "minus molestantur {mutati} malo tractamento quam sauri {(sc. falcones)}, #salv.|o, si ipsi mutati id malum tractamentum habuerint, etiam quando erant sauri."

    * CHART. Bund.; 1127 p. 558,6 "#salv.|o, si alie coherencie invenirentur, in hac vendiccione permaneat."


ABSATZ
BEDEUTUNG
   subst. neutr.:

U_BEDEUTUNG
   depositum -- das Hinterlegte, 'Depositum': 

   * CHART. Tirol. notar.; II 551 (a. 1272) "#salv.|um Iacobi de Grigno."

U_BEDEUTUNG
   custodia, repositorium -- Verwahrung, 'Depot':

   * CHART. Tirol. notar.; II 552 (a. 1272) "Trentinus et eius uxor ... confessi fuerant se accepisse et habuisse in #salv.|o et deposito XL denarios."


SUB_LEMMA 1. salve  

GRAMMATIK
   adv.  

BEDEUTUNG
   incolumi modo, tuto -- unversehrt, beschützt:

   * ANNAL. Rom.; p. 479,47 "Heinricus cesar ... precepit, ... ut ... papam Gregorium cum tota curia, ubicumque voluisset, ducerent {(sc. consul et comes)} #salv.|e et secure per totum Romanum imperium."

   * FRID. II. IMP. art. ven.; 1 p. 113,14 "que {(aves)} ... in societate volant, quoniam #salv.|ius et securius sunt in societate."


SUB_LEMMA 2. *salvo


BEDEUTUNG
   incolumi modo, tuto -- unversehrt, beschützt:

   * CHART. Hans.; 281 p. 94,30sqq. (a. 1237) "quod ... mercatores ... in perpetuum #salv.|o et secure veniant in Angliam cum rebus et mercandisis suis ... et quod #salv.|o ibi morentur et quod #salv.|o inde recedant."

AUTORIN
   Orth-Müller