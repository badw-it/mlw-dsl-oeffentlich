LEMMA
  sacerdotium
GRAMMATIK
   subst. II; -i n.
SCHREIBWEISE
    script.:
         -duci-: {=> sacerdotium_2}
FORM 
     form.:
           gen. sg.:
                -ti: * LIBER diurn.; 78
METRIK
     metr.:
         -dŏ-: {=> sacerdotium_3; sacerdotium_4} {al.}
BEDEUTUNG officium sacerdotale, dignitas sacerdotis -- Priesteramt, -würde:
     U_BEDEUTUNG apud christianos:
        UU_BEDEUTUNG proprie:
            UUU_BEDEUTUNG in univ.:
                UUUU_BEDEUTUNG usu communi:
                     * DIPL. Karoli M.; 180 p. 243,6 "eius {(Asoarii abbatis)} considerantes #sacerdoti.|um ... per omnia verum esse credidimus."
                     * CARM. de Ansb.; 1,8 ((MGPoet. IV; p. 1004)) "nascitur Ansbertus virtutibus undique clarus atque #sacerdoti.|o {@sacerdotium_3} dignus."
                     * CARM. imag.; 23^g,5 "iura #sacerdoti.|i {@sacerdotium_4} Lucas sermone fatetur
                     ((
                        *{sim.} CARM. imag.; 23^h,1,5)) (({loci spectant ad} $SEDUL. carm. pasch.; 1,357
                        ))."
                     * HONOR. AUGUST. offend.; 15 "nec #sacerdoti.|o privetur, qui vitio aliquo deformetur."
                     * LIBER revel. Rich.; 100 p. 121,20 "erat {frater} ... dyaconus, set nec eius etatis nec talis vite,
                     ut #sacerdo.|cio dignus haberetur." {saepius. v. et {=> sacerdotalis/sacerdotium_1}}
                     ANHÄNGER c. gen. inhaerentiae:
                        * PONTIF. Rom.-Germ.; 11 "presbiterii #sacerdoti.|um poterit promereri."
                UUUU_BEDEUTUNG in iunctura "#sacerdoti.|i gradus, #sacerdoti.|i officium":
                     * CONC. Merov.; p. 33,12 "ut nullus in locum viventes ad ambiendum #sacerdoti.|i {!gradum} audeat aspirare
                     ((
                        *WETT. Gall.; 1 "#sacerdoti.|i g. adiit." {al.}
                      ))."
                     * LIBER diurn.; 46 p. 38,8 "ut sua te munitione circumtegat {(sc. Dominus)} et #sacerdoti.|i susceptum {!officium} operibus implere concedat 
                    ((
                       *EPIST. Froth.; 27 "ut ... ad o. #sacerdoti.|i ordinare eum {[Bertigangum clericum]} non dedignemini {[sc. episcopus]}." {al.}
                        ))."
            UUU_BEDEUTUNG de (archi)episcopatu, pontificatu:
                UUUU_BEDEUTUNG usu communi:
                     * EPIST. Desid. Cad.; 2,1 l. 6 "ut pontificalem #sacerdoti.|i vestri laudem insignem vel ... tenuem praesumam {(sc. Sulpicius)} disserere sermonem {(v. notam ed.)}."
                     * THEGAN. Ludow.; 44 p. 236,6 "Iesse a #sacerdo.|cio deposuisti {(sc. Ebo)}, nunc iterum revocasti eum in gradum pristinum."
                     * BERTH. chron. B; a. 1077 p. 270,19 "#sacerdoti.|o suo a legatis apostolicis privatus ... est {(sc. episcopus Augustensis.)}"
                     * CHART. Berg. Magd.; 118 p. 83,15 "ob reverentiam ... tam sanctissimi pontificis, qui, sicut Felix nomine, sic felicissimus virtutum operatione fungi dignus fuit  #sacerdoti.|o." {al.}
                UUUU_BEDEUTUNG in iunctura "summum #sacerdoti.|um":
                     * NOTKER. BALB. gest.; 1,18 p. 24,29 "optimus episcopus ille summo ... #sacerdoti.|o dignissimus est."
                     * DIPL. Conr. III.; 250 "removeri quidam a summi #sacerdoti.|i officio possunt, qui tamen alias ęcclesię ychonomias recte amministrare possunt." {al.}
                     ANHÄNGER adde:
                         * LIUTPR. hist.; 16 "Leonem ..., virum approbatum et ad {!summum} #sacerdoti.|i {!gradum} dignum, nobis in pastorem eligimus
                         {(sc. omnes Romani)}
                         ((
                         *COSMAS chron.; 3,7 p. 167,13 "quem ex eis {[clericis]} potissimum proveheret ad s. #sacerdoti.|i g."
                          ))."
            UUU_BEDEUTUNG in iunctura "regale #sacerdoti.|um" (({spectat ad} * $VULG.; $I Petr. 2,9))((*{de re v.}  J. Fried, DtArch. 19. 1973.; p. 508sqq.)):
                 * RUOTG. Brun.; 20 p. 19,19 "cum video {(sc. imperator)} ... nostro imperio regale #sacerdoti.|um accessisse {(v. notam ed.)}"
                 * CONST. imp. II; 460 "quamvis ... dux inter potentissimos principes habeatur, tamen sub eo non potest dici regale #sacerdoti.|um, sed ducale."
            UUU_BEDEUTUNG Christi:
                 * IDUNG. PRUF. argum.; 257 "habere in capite signum eius, per quod illi {(sc. Christo)} mancipatur {(sc. clericus)} et regis sui regnum et #sacerdotiu.|um figuratur."
        UU_BEDEUTUNG meton.:
            UUU_BEDEUTUNG de administratione ('Amtszeit'):
                 * ADAM gest.; 2,28 p. 89,3 "senex fidelis Adaldagus ... migravit ad Dominum anno #sacerdoti.|i nobiliter ministrati LIIII."
                 * CHART. Naumb. I; 368 "quę in diebus #sacero.|cii nostri {(sc. episcopi)} inter monasterium ... et ... domum hospitalem ... acta sunt, presentis scripture testimonio roborare dignum duxissimus {(sic)}."
            UUU_BEDEUTUNG potestas ecclesiastica -- kirchliche Macht:
                 * BONIZO ad amic.; 9 p.612,26 "diviso regno et #sacerdoti.|o."
                 * OTTO FRISING. gest.; 2,6 p. 106,29 "quo {(tempore)} ... de investitura episcoporum decisa fuit inter regnum et #sacerdoti.|um controversia." {al.}
            UUU_BEDEUTUNG de ipsa persona sacerdotis:
                 UUUU_BEDEUTUNG in univ.:
                    * LIUTPR. antap.; 2,6 p. 40,9 "si #sacerdoti.|i mei {(sc. Hattonis episcopi)} promissionibus minime credis {(sc. Adelbertus)}, iuramento saltem ne diffidas."
                    * CONST. imp. I; 388 "rex ... mittere decrevit ... ad componendam pacem inter ipsius regnum et #sacerdoti.|um vestrum {(sc. papam)} probabiles viros."
                    * DIPL. Loth. III.; 8 "abbas ... conquestus est quedam bona tempore discordie inter regnum et #sacerdoti.|um ecclesie sue fuisse ablata."
                    * DIPL. Conr. III.; 424 p. 426,9 "celsitudinem #sacerdoti.|i vestri {(sc. papae)} ... attente monendo rogamus, ut {eqs.}"
                 UUUU_BEDEUTUNG de sacerdotibus cunctis ('Priesterschaft'):
                    * HERIB. hymn.; 6,12,1 "nunc omne #sacerdoti.|um, primus ordo pontificum, clerum docendo subditum fletum tergite supplicum."
                    * LAND. MEDIOL. hist. II; 34 p. 34,10 (epist.) "ordinarii cardinales sancte Mediolanensis ecclesie necnon et primicerius cum universo #sacerdoti.|o et clero Mediolanensi ... omnibus sacerdotibus {@sacerdos_14} ... pacem et salutem {(sc. dat)}."
                    * CARM. Bur.; 37,3,1 "quod sanctum #sacerdoti.|um, quod unctio regalis se curvet ad imperium et vocem subiugalis {(cf. comm. ed.)}."
    U_BEDEUTUNG apud Iudaeos i. q. officium ἀρχιερέως -- Amt des Hohepriesters:
         * CHRON. Fred.; 2,33 p. 56,18 "post ann<o> uno interfecto {(sc. Aristobulo)} Ananelo reddit #sacerd.|ucium {@ sacerdotium_2}."
         * WALTH. SPIR. Christoph. I; 10 "illa legalis reverentia #sacerdoti.|i hunc {(Aaron)} quodammodo pręfigurabat portitorem evangelii."
         * RUP. TUIT. vict.; 8,23 p. 267,16 "Machabei fratres ... regio more perornati sunt, et posteri eorum regnum et #sacerdoti.|um non parva gloria recuperatum obtinuerunt
          ((
           *{sim.} trin.; 22,46     
           ))."
           ; 11,29 p. 370,17 "germinavit virga Aaron ..., atque ita cessavit superbia de #sacerdoti.|o conflictantium."
         * OTTO FRISING. chron.; 2, 46 capit. p. 17 "qui {(sc. Pompeius)} ... templum ... Domini polluit captoque et vincto Aristobolo fratri eius #sacerdoti.|um tradidit."
ABSATZ
BEDEUTUNG locus quo ara posita est -- Altarraum:
     * ALBERT. M. eccl. hier.; 7,6 p. 151,68 "'ad #sacerdoti.|um' ((* $DION. AR.; ((PG 3,;556^C)) "ἱερατεῖον")), alia translatio 'sanctuarium', id est ante locum, ubi stant sacerdotes{@ sacerdos_15}, id est in introitu chori, ubi stat populus {(sc. sacerdos mortuum recumbere facit)}."

  AUTORIN Weber              