LEMMA 1. sagino

GRAMMATIK
     verbum I; -avi, -atum, -are

SCHREIBWEISE
     script.:
         -irat(us): {=> sagino_1}

BEDEUTUNG pinguem reddere, satiare, nutrire -- mästen, sättigen, fett machen, nähren:

     U_BEDEUTUNG proprie:
             UU_BEDEUTUNG obi. anim. (fere in chartis):
                 * CAPIT. reg. Franc.; 9 p. 23,26 "unde porci debent #sagin.|are ({leg.} #sagin.|ari)."
                 * COD. Lauresh.; 3795 "donamus ... silvam ad {!porcos} L #sagin.|andos
                 ((
                 * CHART. Rhen. inf.; I 45 "ad #sagin.|andum p. XX"
                 ))."
                  * CHART. Rhen. med.; $I 120 p. 126,17 "#sagin.|ant verros II."
                  * DIPL. Otton. II.; 15 p. 23,21 "quicquid in ipsa marca enutrire aut #sagin.|are vel venatu conquirere homines eorum {(monachorum)} potuerint
                 ((
                     *{inde} * DIPL. Conr. II.; 148 p. 201,8. *DIPL. Heinr. III.; 58 p. 76,27. *DIPL. Heinr. IV.; 195 p. 252,25
                  ))."
                 * REGISTR. abb. Werd.; 3,38 p. 124,9 "in Ambreki XII porci perfecte #sagina.|bantur."
                 ANHÄNGER absol.:
                     * CAPIT. reg. Franc.; 32 p. 86,21 "si ... porcos ad #sagin.|andum in silvam nostram miserint {(sc. homines)}."
                     * DYNAMID. Hippocr.; 1,14 "volentes ... ipsum milium comedere, #sagin.|at quidem, sed non egeritur."

             UU_BEDEUTUNG obi. res i. q. tumefacere -- aufschwellen lassen:
                 * YSENGRIMUS ; 5,876 "scotigenum crustas mollisset {abbas} flamine terno atque #sagin.|asset (({v. notam ed.}))."

     U_BEDEUTUNG in imag. vel translate: 
         UU_BEDEUTUNG in bonam partem:
             * RADBERT. corp. Dom.; prol. ad Warinum 78 "quanta sit virtus vitam aeternam inpraesentiarum accipere et angelicis escis cotidie #sagin.|ari
             ((
                 *{item} prol. ad Karol. 60
             ))."
             * HEINR. AUGUST. planct.; 1572 "pane fidelis ali solet agni spirituali ..., unde #sagin.|atur."
             * OTTO FRISING. chron.; 7,35 p. 370,15 "ut in ipsa naturali refectione divinae scripturae iugiter intenti spiritum malint {(sc. monachi)} #sagin.|are quam corpus."
             * VISIO Godesc. A; 51 "omnes adeps frumenti cognitionis divine veritatis #sagin.|ando saciavit (({spectat ad} $VULG.; psalm. 147,17))."
             * CHART. Argent.; add. 251 p. 153,3 "superne pietatis dulcedine #sagin.|ari." 
             {al.}

         UU_BEDEUTUNG in malam partem:
             * OTLOH. Bonif.; 1,37 p. 152,20 "quibus {(praediis)} ... monachi inutiliter #sagin.|antur."
             * FUND. mon. Leonh.; ((MGScript. $XV; p. 1001,33)) "ecce quibus nostra servitus non, ut hostes garriunt, #sagin.|atur, sed utcunque sustentatur."  

SUB_LEMMA saginatus
 GRAMMATIK
         adi. I-II; -a, -um

STRUKTUR

      usu subst. vel ellipt.: {=> sagino_2}
BEDEUTUNG pinguis -- fett:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG de cibis i. q. adipe confectus -- mit Fett, Schmalz zubereitet:
             * AETHICUS; 67^b,11 "carnes recentissimas et #saginat.|as vitulorum ac pecudum super contignatia positae per itinera fuliginosa."
             * VITA Poppon.; 28 p. 312,33 "a #saginat.|is cibariis ... abstinebat."
             * TURBA phil.; 10 "donec brodium fiat #saginat.|o {@ sagino_2} simile."
             * COD. Eberh.; $I p. 151,8 "#saginat.|is uti cibis."
             * REGISTR. Pant. Col.; 17,6 p. 108,30 "#saginat.|um sorbicium dabitur sexaginta ovorum."
             * ARTEPH. secret.; p. 45 "in hac putrefactione in hac aqua primo apparet nigredo, sicut brodium #sagi.|ratum {@ sagino_1}."
                 
         UU_BEDEUTUNG de animalibus:
             UUU_BEDEUTUNG in univ.:
             * CHART. Sangall. A; 738 p. 341,36 (a. 892) "dentur {!porci} #saginat.|i tres
             (( 
                 ; app. 11 "unum porcum #saginat.|um." {al.}
                 *{sim.} TRAD. Salisb. II; 31 "viginti IIII^or mansi ... viginti IIII^or porcos #saginat.|os solvunt." {al.}
             ))."
             * ECBASIS capt.; 255 "si daret Heinricus ... porcos quingentos, vitulos totidem #saginat.|os."

             UUU_BEDEUTUNG de animali immolando (in imag. de Christo):
                 * RHYTHM.; 117,7,1 "ipse ... est credendus #saginat.|us vitulus, cuius caro immolata in crucis patibulo nos cruore suo tulit de mortis periculo."
          
          UU_BEDEUTUNG de hominibus i. q. satiatus -- satt:
              * CYPR. CORD. carm.; app. 3,5,3 ((MGPoet. $III; p. 148)) (c. s. X^1) "sic #saginat.|i (saginam {var. l.}), propinati gaudeant, nullus recuset farcinatum sumere, calicem vini gutta dum evibitur."

     U_BEDEUTUNG in imag. vel translate:
         UU_BEDEUTUNG in bonam partem:
             * GERH. AUGUST. Udalr.; 1,4 l. 108 "populo ... sacro Christi corpore #saginat.|o."
             * EPIST. Hann.; 105 p. 174,5 "talium litterarum opificibus ... de cęlestibus optimarum meditationum favis #saginat.|is."
             * OTTO FRISING. chron.; 8,34 p. 455,22 "ut ... in utroque, corpore scilicet ac spiritu, #saginat.|i utrisque Deum oculis videant {(sc. beati)}."

         UU_BEDEUTUNG in malam partem i. q. farcitus, repletus -- (voll)gestopft, angefüllt:
             * FRAGM. de Arnulfo; ((MGScript. $XVII; p. 570,22)) (c. 937) "his peccatis #saginat.|i et onerati ... exierunt {(sc. Ratisbonenses)} coacti."
             * PETR. DAM. epist.; 143 p. 523,25 "#saginat.|is ex aliena calamitate visceribus."
             * LAMB. WAT. annal.; a. 1153 p. 527,18 "ira #saginat.|us castellanus ... voce spumabat."

AUTORIN Weber