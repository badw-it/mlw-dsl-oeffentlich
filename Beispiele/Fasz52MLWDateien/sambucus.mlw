LEMMA sambucus

GRAMMATIK 
    subst. II; -i

VEL {raro} sambuca

GRAMMATIK
    subst. I; -ae f.

SCHREIBWEISE
    script.:
        cauc(us): {=>sambucus_4}
        sauc(us): {=>sambucus_1}
        sab-: {=>sambucus_2}
        sansu-: {=>sambucus_3} {adde} * ANTIDOT. Glasg.; p. 125,1
        

BEDEUTUNG ἀκτέα, ἀκτῆ -- Schwarzer Holunder (Sambucus nigra L.) ({cf. Marzell, Wb. dt. Pflanzennam. IV. p. 63sqq.}):

    UNTER_BEDEUTUNG gener. et natur.:
    
        * CHRON. Fred.; 3,49 "uvas in cauco {@sambucus_4} (pauco {p. corr. cod. 1}) nate sunt {(v. notam ed.)}."
    
        * ANON. herm.; p. 97,29 "actis, id est #sambuc.|us (({sim.} * MATTH. PLATEAR. [?] gloss.; p. 389^H "actis, id est #sambuc.|us, chamaeactis, id est ebulus quasi ... humilis actis, et dicitur a chamae, quod est infimum, ideo, qui dicit, ebulus est actis, #sambuc.|us {@sambucus_7} est chamaeactis, non mentitur." {al. v. et {=>sambucus_5} {=>sambucus_6}}))."  
    
        * IOH. AEGID. mus.; 17,44 "#sambuc.|a est genus ligni fragilis, cuius rami sunt concavi ..., unde tibiae componuntur et quaedam species symphoniae {(sim. {=>sambuca1/sambucus_1} {=>sambuca1/sambucus_2})}." {saepius.}

    ANHÄNGER in nomine loci iudicii: 

         * CHART. Westph.; III 902 (a. 1271) "coram iudicio, quod vulgo dicitur vrithing, in loco, qui dicitur malstath ad #sambuc.|um."

    UNTER_BEDEUTUNG medic.:

        * RECEPT. Lauresh.; 2,231 "#sauci {@sambucus_1} radicis cortices medianae cƚa II (({sim.} * PAUL. AEGIN. cur.; 211 p. 149,21 "#sambuc.|e {@sambucus_5} (($PAUL. AEG.; 3,48,3 p. 257,7 "τῆς ἀκτῆς")) radicis corticis sucus"))."; 4,71 "#sabuci {@sambucus_2} pulvis ℈ I."

        * THADD. FLORENT. cons.; 129,81 "in quo {(vino)} decoquantur ... bacce lauri, #sansucus {@sambucus_3} et piretrum."

BEDEUTUNG de altera specie #sambuc.|i i. q. ebulum, χαμαιάκτη -- Attich, Zwergholunder (Sambucus ebulus L.)({cf. Marzell, Wb. dt. Pflanzennam. IV. p. 57sqq.}): 

    * PS. ODO MAGD. herb.; 34 "duplex #sambuc.|i {@sambucus_6} dicunt genus esse periti; ... maior habet nomen actes, minor at chamaeactes; est ebulus, formam cui conscribunt breviorem." {v. et {=>sambucus_7}}

 AUTORIN Clementi