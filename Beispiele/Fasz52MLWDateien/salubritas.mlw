LEMMA salubritas

GRAMMATIK
   subst. III; -atis f. 

BEDEUTUNG
   proprie:

U_BEDEUTUNG
   valetudo -- Gesundheit:

   * RECEPT. Lauresh.; 2,128 capit. p. 83 "ad corporis #salubritat.|em servandam."

   * FORM. Augiens.; C 15 p. 372,6 " pro #salubritat.|e vestra {(sc. abbatis)} Domini misericordiam exorantes {(sc. monachus)}."

   * LUP. FERR. epist.; 109 (ed. Dümmler) "quo {(potu naturali)} #salubrita.|s animae corporisque nonnumquam adquiritur."

   * VITA Rust.; 27 (MGMer. IV p. 351,6) "quisque ... aegrotus ... pannum de ipsius {(sancti)} vestimentis suo corpori ... collocaverit, promeretur a Domino et recuperationem corporis et animae percipere #salubritat.|em."

   *TRANSL. Eug. Tolet.; add. 4,43 "dum a mentis duritia avaritiae detersit {aegrotus} maculam, corporis ... #salubritat.|em recepit."
   {v. et} {=> saluber_1/salubritas}

U_BEDEUTUNG
   salubris natura -- gesunde Beschaffenheit:

   * HARIULF. Arnulf.; 3,18 "animadversum est ... cum beati viri elevatione regioni Flandrensium advenisse frugum fecunditatem, corporum sanitatem, aurarum #salubritat.|em."

   * CONST. Melf.; 3,48 "#salubritat.|em aeris divino iudicio reservatam ... disponimus {(sc. imperator)} conservare."

BEDEUTUNG
   translate:

U_BEDEUTUNG
   salus, integritas -- Heil, Unversehrtheit:

UU_BEDEUTUNG
   de hominibus:

   * CAPIT. reg. Franc.; 293 p. 397,4 "ob ... regis ac regni #salubritat.|em {@ salubritas_1} atque stabilitatem."

   * VITA Greg. Porc. I; 1 "erat ... a pueritia ... in vestium apparatu ut laicus, cordis vero #salubritat.|e Deo vivus et seculo defunctus."
   {al.}

UU_BEDEUTUNG
   de rebus:

   * RATPERT. cas.; 8 p. 196,15 "utriusque loci rectores, episcopii videlicet et monasterii, ... perquirere cęperunt, qualiter #salubrita.|s pacis inter utrumque locum ita statueretur ..., ut {eqs}."

   * HILDEG. vit. mer.; 4,61 "per ipsum {(Deum)} ... solida #salubrita.|s sum {(sc. imago in visione)}."

   * ANNAL. Magd.; a. 328 p. 121,41 "concilii ... plurima pars ... inter alterutros unanimitatem et dogmatum #salubritat.|em libenter amplectebantur."

   {al. v. et} {=> salubritas_1}



U_BEDEUTUNG
   redemptio -- Erlösung (spectat ad Christum):

   * MANEG. c. Wolfh.; 4 p. 52,8 "ad quę {(genera hominum)} tantę #salubritat.|is noticia pervenire non potuit."


AUTORIN
    Orth-Müller