LEMMA sanguisuga
    (-ss-)

GRAMMATIK  subst. I; -ae f.

SCHREIBWEISE
    script.:
        sams-: {=> sanguisuga_03}
        -gsugg(a): * BERNH. PROV. comm.; 9,3 p. 322,2
        -ues-: {=> sanguisuga_02}
        -inis-: {=> sanguisuga_05}{=> sanguisuga_08}
        -uc(a): {=> sanguisuga_01}{=> sanguisuga_04}
        -usa: {=> sanguisuga_07}

METRIK  
    metr.:
        -sŭg(a): {=> sanguisuga_06}



BEDEUTUNG  qui sanguinem sugit, hirudo -- Blutegel:

    UNTER_BEDEUTUNG  sensu originario:

        UU_BEDEUTUNG  proprie:

             UUU_BEDEUTUNG  in univ.:

                * GLOSS. psalt. Lunaelac.; 9,13 "sanguinem nuncupat vivificationem animae, quam diabolus ante per fallaciam suggerebat sicut #sangu.|esugae {@ sanguisuga_02}, quae non saciantur."
                * BENZO ad Heinr. IV.; 2,18 p. 260,11 "non erat Normannis ulla spes fugę, resummebant spiritum viso sanguine sicut #sanguisug.|ę."
                * ALBERT. M. animal.; 7,151 "os aptavit {natura} quibusdam eorum {(animalium)} ad sugendum, sicut murenae et #sanguisug.|ae." {v. et {=> sansugium/sansugium_05}}

            UUU_BEDEUTUNG  spectat ad usum medicinalem:

                * AESCULAPIUS; 37 p. 59,14 "ad viscera dolentia vel extensa ... #sanguisug.|ae adhibendae."
                * ODO GLANN. Maur.; 3 p. 467,17 (ed. Holder-Egger) "ut ... uxor ... a scapulae ... plenius convalescere quaeat incommodo, #sangui.|ssugae (#sanguisug.|ę {var. l.}) sitibunda illi adhibeatur numerositas."
                * FRAGM. med. falc.; 31 "accipe de ipsa sugia de ipsis pennis et pulverem de #sangui.|ssuca {@ sanguisuga_04} {eqs.}"
                * TRACT. de aegr. cur.; p. 97,37 "accipe II #sanguisug.|as et pone in extremis narium {(sc. phrenetici)} vel in sumitate."
                * IORDAN. RUFF. equ.; p. 27,14 "quod accipiantur arundines ({leg.} hirudines), quae #sanguisug.|ae dicuntur, et ponantur circum circa inflationes crurium." {saepius.}

            UUU_BEDEUTUNG  spectat ad tributum:

                * REGISTR. Prum.; 96 "ex his {(sc. in Renbahc sitis)} mansis solvunt XVIIII #sam|.sug.|as {@ sanguisuga_03} unusquisque XXX."; 114 "debet ... #sanguisug.|as C."

        UU_BEDEUTUNG  in imag. de persona avara, vires et copiam exsugente:

            * ARNULF. delic.; 623 "natas #sanguisug.|e {@ sanguisuga_06} (gloss.: hirundinis {H}) geminas vehementer abhorre."
            * VOLQUIN. serm.; 236 ((StudMediev. III/3. 1962.; p. 329)) "mulierem mollem ... dicit ... luxurium carnis, ebriam de sanguine suorum scortatorum; haec est illa #sangui.|nissuga {@ sanguisuga_08} ({ed. [sic?],} #sangui.|ssuga {ed. in app. crit.}, #sanguis.|usa {@ sanguisuga_07} {var. l.}), quae sugit sanguinem ministrorum Dei."
            * ROB. ARR. fund.; ((MGScript. XV; p. 1124,12)) "quod ... insigne patrimonium huic sufficeret #sangui.|nisuge {@ sanguisuga_05} unius anni vel duorum alimenta prebere?" {al.}


    UNTER_BEDEUTUNG  fort. de sacco ad filtrandum q. d. apto:

        * GEBER. clar.; 1,63 "distilla acetum per #sanguisu.|cam {@ sanguisuga_01} {(v. comm. ed. p. 221)}."


AUTORIN  Leithe-Jasper