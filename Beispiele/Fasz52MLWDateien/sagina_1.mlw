LEMMA 1. sagina

GRAMMATIK
      subst. I; -ae f.

SCHREIBWEISE
     script.:
          -gen(a): {=> sagina_1; sagina_2}

BEDEUTUNG pabulum (pinguem faciens) -- (Mast-)Futter:

     U_BEDEUTUNG proprie:

         UU_BEDEUTUNG strictius:

             UUU_BEDEUTUNG in univ.: 
                 * WALAHFR. Gall.; 2,18 "dum sterilitas terrae fructus arborum ... porcis ... denegaret, ita ut in proximis heremis partibus nusquam #sagin.|a posset inveniri."
                 * FORM. Sangall.; $I 9 "utrum et caeteri cives in eodem {(sc. saltu)} ... pastum ... vel #sagin.|am animalium habere ... deberent; ... omnes illi pagenses ... habeant ... #sagin|am ... porcorum vel pastum peccorum."; $II 10 "ut omnia omnibus essent communia in ... #sagin.|a porcorum et pastu pecorum."
                 * CHART. Sangall. A; 742 "ut ... recipiam {(sc. Kerine)} ... porcis ... in ipso curtili enutritis #sagin.|am."

             UUU_BEDEUTUNG de iure saginandi:    
                 * TRAD. Frising.; 743 (a. 855) " tradidi {(sc. comes)} ... curtem cum ... silvis forestibus, venatione, #sagin.|a, exstirpatione."
                 * TRAD. Ratisb.; 257 " absque censu lignorum copiis, foeni etiam sectionibus ac porcorum inibi nutritorum #sagination.|is."
                 * DIPL. Heinr. II.; 21 "tradidimus ... civitatem ... cum ..., forestis, venationibus, #sagination.|is, aquis."
                 {al.}
    
         UU_BEDEUTUNG latius i. q. adeps -- Fett:
             * FRAGM. med. falc.; 14 "tolle oleum et diagridium ... et #sagin.|am."   
             * CONSUET. Trev.; 82 "multi a septuagesima usque in pascha abstinent a #sagin.|a"
             * FUND. Brunw.; 34 p. 141,14 "predium ... illud eos {(fratres)} enutrit pane, cerevisia, #sagin.|a et legumine."
             * CONSUET. Vird.; 6 "ova et #sag.|enam {@ sagina_1} et caseum fratres suspense vitabunt."
             {al.}

                 ANHÄNGER remissius i. q. cibus -- Nahrung:
                     * EPIST. Worm. I; 10 p. 27,20 "intimamus {(sc. frater cum discipulis)} celsitudini vestrę {(sc. episcopo)} ... nos laborare vini indigentia #sagin.|ę_que penuria."
              
     U_BEDEUTUNG in imag.:
         * WOLFHARD. Waldb.; 4,12^c p. 354,15 "eam {(ancillam)} ... divina #sagin.|a pascebat {(sc. Deus)}."
         * GERH. AUGUST. Udalr.; 1,1 l. 72 "congruo tempore duplici #sagin.|a scientiae ac religionis repletus."
         * COD. Udalr.; 261 "quatinus tum vestra {(sc. christianorum)} presentiali presentia, tum orationum et elemosinarum #sagin.|a in Christi nomine palmam victorie valeamus obtinere."


     U_BEDEUTUNG translate:
                 UU_BEDEUTUNG actio saginandi -- das Mästen, Mast:
                     * CHART. Sangall. A; 394 p. 15,10 (a. 845) "ut ... recipiam {(sc. Otpertus)} unum porcum IIII denarios valentem, quando #sag.|ena {@ sagina_2} fuerit"
                     * WANDALB. mens.; 358 " hoc {(sc. Decembri)} mense sues pasta iam glande madentes distento et plenam monstrantes ventre #sagin.|am."
                     * CARM. var. Walther; 1,30,4 "aucupes, coci, bubulci #sagin.|as incipiunt."

                 UU_BEDEUTUNG tempus saginationis -- Mastzeit:
                     * CHART. Turic.; 152 (a. 888) "ut ..., quando #sagin.|a porcorum (poncorum {ed.}) eveniat, XX porci de parte loci supra nominati mihi {(sc. Oͧdalgero)} saginentur."

                 UU_BEDEUTUNG pinguedo -- Fettleibigkeit:
                     * CHRON. Fred.; 3,56 "odium regis pre #sagin.|am incurrit {Marcytrudis}."; 4,28 "hoc tantum inpedimentum habebat {Claudius maior domus}, quod #sagin.|am esset corpore adgravatus."
                     ANHÄNGER fort. huc spectat:
                           * CONR. MUR. nov. grec.; 2,1043 "deridet sanna, fertur pinguedo #sagin.|a."

AUTORIN Weber