LEMMA saliva

GRAMMATIK
        subst. I; -ae f.

 SCHREIBWEISE
       script. et form.:
             -ib(a): {=> saliva_1; saliva_2; saliva_4; saliva_5; saliva_7} {al.}
             abl. pl.:
                   salibus: {=> saliva_6}
       decl. II.: {=> saliva_4; saliva_7}

GEBRAUCH
       usu plur.: {=> saliva_11; saliva_12; saliva_13; saliva_14; saliva_15; saliva_16} {al.}


BEDEUTUNG humor oris, sputum -- Speichel, Geifer, Spucke:
       U_BEDEUTUNG proprie:
             UU_BEDEUTUNG gener.:
                   UUU_BEDEUTUNG in univ.:
                         * LEX Alam.; 57,19 "subteriorem {(sc. si quis labium maculaverit)}, ut #saliv.|am continere non possit, cum XII solidis conponat."
                         * EPIST. var. I; 15 p. 519,17 "cum ... infuso #sali.|bo {@ saliva_7} dentibus."
                         * AMALAR. off.; 2,24,1 "sudario solemus tergere ... superfluam #saliv.|am decurrentem per labia."
                         * THEOPH. sched.; 2,18 "si ... vitrum durum fuerit, madefac illud digito tuo ex #saliv.|a."
                         * RUP. TUIT. vict.; 12,15 p. 388,6 "infantium est ... #saliv.|am emittere."
                         {al.} {v. et {=> saliualis/saliva_9; saliuaris/saliva_8}}
                         ANHÄNGER fort. huc spectat:
                               * DECRET. Frision. A; 8 p. 58,17 "pro nudati corporis rapina due marce {(sc. solvuntur)}, pro #saliv.|e excussione tantumdem."

                   UUU_BEDEUTUNG in miraculo (({spectat ad} * $VULG.; Ioh. 9,6)):
                         * IONAS BOB. Columb.; 1,15| p. 177,17 "ad ipsum {(Theudegisilum)} properat digitumque inlitum #saliv.|o (#saliv.|a {var. l.}) pristine statim sospitati reddit."; p. 178,10 "#sali.|bo {@ saliva_4} (#saliv.|a {var. l.}) inlitum sanum reddidit."; 2,3| "vir Dei suscipiens frustra pollicis suis #saliv.|is {@ saliva_11} (#sali.|bus {@ saliva_6}, #sali.|bis {@ saliva_5} {var. l.}) inlitum manui iunxit."
                         * VITA Magni Fauc. I; 16 p. 142,20 "lenivit oculos eius {(caeci)} cum #saliv.|a sua, nec mora ... aperti sunt."
                         * CARM. de Germ.; 1,63,2 ((MGPoet. $IV; p. 127)) "duae fiunt feminae de #saliv.|a oris sui sanatae invalidae."

                   UUU_BEDEUTUNG spectat ad detractionem:
                         * HROTSV. asc.; 42 "Iudaei spurcis hanc {(Christi faciem)} ... petiere #saliv.|is{@saliva_12}."

                   UUU_BEDEUTUNG spectat ad iram:
                         * WILH. APUL. gest.; 2,509 "ut duo cum certant productis dentibus apri, alter ab alterius perfunditur ore #saliv.|a dentes exacuens, ut acutos perferat ictus, ictibus et validis feriunt {(sc. hostes)} sua terga vicissim."
  
             UU_BEDEUTUNG medic. {(natur.: {=> saliva_10})}:
                   UUU_BEDEUTUNG in univ.:
                         * RECEPT. Lauresh.; 2,175 "favillam de foco cum #saliv.|a inducis; facit in initiis, si #saliv.|a ieiuna uteris."
                         * RECEPT. Bamb.; 5 "#saliv.|am ieiuni hominis in oculo mittit."
                         * CONSTANT. AFRIC. theor.; 3,17 p. 11b^v "in ligamentorum {(sc. linguae)} lateribus quedam vene sunt, que #saliv.|am {@ saliva_10} semper lingue subministrant."
                         * HILDEG. caus.; 272 p. 170,23sqq. "#saliv.|a ... munda et pura esset, nisi {eqs.}"
                         * ALBERT. M. animal.; 7,134 "quod sagitta intincta in #saliv.|am hominis ieiuni intoxicatur, quando vulnerat alium, et quod unicuique #saliv.|a propria est medicamen contra venenum."
                         {al.}

                   UUU_BEDEUTUNG in morbo:
                         * RECEPT. Lauresh.; 5,3 p. 354 "remediat ... eos, qui ... #saliv.|a habundant."
                         * PLATEAR. pract.; 3,11 "habundancia #saliv.|arum{@ saliva_13}."
                         * HILDEG. (?) caus.; 397 p. 233,22 "ut ... de #saliv.|a et de excreatione et emunctione homo purgetur."
             
             UU_BEDEUTUNG liturg. in actu symbolico (({spectat ad} * $VULG.; Ioh. 9,6)):
                   * ORD. Rom.; 15,115 "tangit {presbyter} de #sali.|ba {@ saliva_1} oris singolorum {(sc. infantium baptizandorum)} nares et aures."
                   * HRABAN. inst. cler.; 1,27 l. 29 "tanguntur ei {(catechizando)} nares et aures cum #saliv.|a."
                   * HONOR. AUGUST. gemm.; 3,63 "quod cum #saliv.|a aures et nares tangunt, hoc significat, ut {eqs.}"
                   {al.}       

       U_BEDEUTUNG in imag. (de locutione):
             * CARM. Paul. Diac. app. I; 8,1 "hausimus {(sc. Paulinus)} altifluo perfusas rore #saliv.|as {@saliva_14} ({corr. ex} #sali.|bas {@ saliva_2} {B}) ore fluenta tuo {(sc. Zachariae presbyteri)} labio stillante iocundo."
             * WALAHFR. carm.; 18,18 "te {(sc. Godescalcum)} ... sit certum ... me atque alios exemplorum verbique lucerna sapidis infundere posse #saliv.|is{@ saliva_15}."
             * RADBERT. Matth.; 5 prol. 238 "quia de #saliv.|a oris eius olim infans gustavi, verum loqui iam senex rogatus a vobis {(sc. fratribus)} non erubesco."
             * EPIST. Meginh.; 12 "numquidnam crepundia et infantiles #saliv.|as {@ saliva_16} homo quinquagenarius meditatur?"
             * EPIST. Praed.; 81 "iam IV super me {(sc. provincialem)} mutantur tempora, ex quo grandi pressus lapide officii incumbentis non datur respiratio vel, ut #saliv.|am meam degluciam, indulgetur."
             {al.}                         
ABSATZ                    
BEDEUTUNG spuma -- Schaum:
        * WALAHFR. hort.; 164 "qualis manibus quondam suspensa supinis lucet agens circum lomenti bulla #saliv.|am (gloss.: seiurun)."

AUTORIN Weber        