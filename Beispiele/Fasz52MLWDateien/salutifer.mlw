LEMMA salutifer

GRAMMATIK
   adi. I-II; -era, -erum   

SCHREIBWEISE
   script.:
           -ff-: {=> salutifer_2}

BEDEUTUNG
   proprie:

U_BEDEUTUNG
   qui valetudini prodest -- wohltuend, gesund(heitsfördernd):

   * VITA Bertini; 21 (MGMer. V p. 767,1) "nostro egrotanti amico ... #salutiferu.|um dabit {Dominus} potum."

   * ANTIDOT. Cantabr.; p. 164,14 "ordinamus, qualiter ... potio de herbis fit optima homini et #salutifer.|a ad bibendum."
 
   * {adde:} ODO MAGD. herb.; 1203 "est ... illius {(caulis Romanae)} ad multa #salutife.|r usus."


U_BEDEUTUNG
   sanus, potens -- gesund, stark:

   * WALAHFR. carm.; 84,10 "auge {(sc. Domine)} #salutifer.|am ... sanitatem, nec illum {(Hrabanum)} tangat Christo duce morbus iniquus."


BEDEUTUNG
   translate i. q. saluber, salutaris -- Heil, Rettung, Segen bringend, heilsam:

U_BEDEUTUNG
   de hominibus; de Christo: {=> salutifer_1} {=> salutifer_3}:

UU_BEDEUTUNG
   in univ.:

   * VITA Lamb. Traiect.; 1 (MGMer. VI p. 353,8) "cur nos christiani #salutifer.|i (#salutifer.|a {var. l.}) taciamus miracula Christi{@ salutifer_1}."

   * ERMENR. Sval.; 7 "ipse {(sc. Christus)} <<#salutifer.|ae {@ salutifer_3} medicus salutis>> (#salutife.|r medicus {var. l.})."

   * HILDEGAR. Far.; 118 (MGMer. V p. 198,11) "#salutife.|r visitator clementissimus Faro cubiculo adest."

ANHÄNGER
    de reliquiis:

   * VITA Paulin. Trev.; 16 "adest ... obviam #salutifer.|o corpori {(sc. Paulini)} ... innumerabilis populus."

UU_BEDEUTUNG
   de memoria mortui i. q. beatus -- selig:

   * DIPL. Heinr. IV.; 466 p. 632,14 " in Christi nomine ad #salutifer.|am memoriam Heinrici tercii Romanorum imperatoris augusti feliciter amen ((* {item} ; 489 p. 667,17. {ibid. al.}))."


U_BEDEUTUNG
   de rebus:

UU_BEDEUTUNG
   in univ.:

   * LULL. epist.; 92 p. 209,11 "Gregorio ... Lullus ... #salutifer.|am salutem."

   * FROUM. carm.; 27,6 "quando #salutifer.|as mihi protulit ars tua {(sc. Ellingeri)} cartas."

   * CHART. Raitenh.; 79 "nos {(sc. papa)} ..., qui #salutifer.|a commoda vestra {(sc. abbatis)} benigno favore prosequimur."
   {al. v. et {=>salutaris/salutifer}}

UU_BEDEUTUNG
   de praeceptis, admonitionibus, consiliis sim.:

   * LIBER diurn.; 84 p. 94,25 "#salutifer.|is sanctorum patrum doctrinis innitimur {(sc. papa)}."

   * COD. Karol.; 45 p. 562,2 "#eius {(Stephani)} #saluti.|fferis {@ salutifer_2} obtemperavit {Pippinus} monitis."

   * VITA Audom.; 6 (MGMer. V p. 756,15) "#salutifer.|a divini verbi precepta stolidis paganorum cordibus tradidit."

   * DIPL. Arnulfi; 96 p. 141,27 "nos ... #salutifer.|am petitionem libentissime audientes eorum {(sc. abbatis et fratrum)} arbitrio concessimus, ut {eqs}."

   * OTTO FRISING. gest.; 1,58 p. 82,11 "finita synodo #salutifer.|is_que ad innovationem ... antiquorum ibidem promulgatis decretorum capitulis."
   {saepe.}


UU_BEDEUTUNG
   de ritibus et caeremoniis christianis:

   * LIUTG. Greg.; 13 p. 77,37 "omnes scripturae sanctae novi testamenti et veteris hoc {(sc. elemosinarum)} opus #salutifer.|um laudant."

   * HRABAN. carm.; 39,49,4 "illic {(sc. in eremo)} tempus ieiunii peregit {Iesus} #salutifer.|i."

   * CAND. FULD. Eigil. I; 16 "sancta et #salutifer.|a sollemnitate peracta."

   * POETA SAXO; 1,350 "magna #salutifer.|um suscepit turba lavacrum."

   *; 2,11 "fonte #salutifer.|o Pippinum nomine natum abluit ... praesul."

   * DIPL. Otton. I.; 271 "ut ... liceat ... patriarce ... pro nostris ... beneficiis #salutifer.|as orationes ... ad Deum incessanter effundere."
   {al.}


ANHÄNGER
   de cruce:

   * HILDEGAR. Far.; 109 (MGMer. V p. 196,15) "quam {(basilicam)} ... fundavit in honore #salutifer.|ae ac vivificae crucis."

   * NOTKER. BALB. gest.; 1,23 p. 31,14 "signo #salutifer.|ę (#salutifer.|o {var. l.}) crucis ((* {sim.} * VITA Corb.; 21 "#salutifer.|o crucis Christi signo." * HIST. peregr.; p. 116,23 "sub vexillo #salutifer.|e crucis." {saepe}))."


SUB_LEMMA *salutifere

GRAMMATIK
   adv.   

BEDEUTUNG
   modo sanante -- heilend:

   * LAMB. TUIT. Herib.; 2,13 "paternam propitiationis dexteram extendit et, quod vix pręsumebant {(sc. infirmi)}, ut fieret, #salutifer.|e impendit."

BEDEUTUNG
   salubriter, modo ad salutem animae pertinente -- heilsam, im Hinblick auf das Seelenheil:

   * EPIST. pont. Rom. sel. I; 14 p. 76,2 "quę a sancta Romana et apostolica auctoritate iussa sunt, #salutifer.|e inpleantur."

   * HIST. Lang. cont.; II (MGLang. p. 200,27) "quem {(regem)} ... #salutifer.|e predicans {papa} Deo auctore valuit animum eius spiritali studio inclinare."

   * CONST. imp. II; 322,10 "ad obviandum ... #salutifer.|e his malis sepe ... direximus {(sc. rex)} sollempnes nuntios nostros."
   {al.}

AUTORIN
   Orth-Müller