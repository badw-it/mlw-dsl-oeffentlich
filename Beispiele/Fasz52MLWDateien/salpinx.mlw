LEMMA salpinx

GRAMMATIK
      subst. III; -ingis

VEL salpix
GRAMMATIK
      subst. III; -icis f.

SCHREIBWEISE
      script. et form.:
           ialpas: {=> salpinx_2}
           ialpes: {=> salpinx_3}
           -pax: {=> salpinx_1}


BEDEUTUNG tuba -- Trompete, Posaune:
      U_BEDEUTUNG proprie:
           * ERMOLD. Ludow.; 3,341 "#salpi.|cibus iam rura sonant, nemus omne resultat, et cava per campos bucina pulsa gemit."
           * CARM. Hraban. app.; 16,20,1 "viva tuba, #salpi.|nx sancta."
           * EPIST. var. II; suppl. 7 p. 627,17 "fistulae ... uno flatu ... unam ... armoniam velut #salp.|ix (#salp.|ax {@ salpinx_1} {var l.}) tubarum voces atque concentum dulci cantilena melos afflando ... auribus suaviter reddunt." 
           * EUGEN. VULG. syll.; app. 1,2,3 "intonet hac {(angelica turba)} #salpi.|nx pro Christi valde triumphis."

           ANHÄNGER de tuba in die extremo sonante:
                * CARM. Salisb. I; 5,13 "da, Domine clemens, huic {(sc. Adalrammi)} animae requiem, ut, cum vivificet #salp.|ix hunc ultima carne, integrum facias gaudia bina frui."

      U_BEDEUTUNG in imag.:
           * CARM. var. I; 27,1,33 "euangelica #salp.|ix typice intonat orbi."

      U_BEDEUTUNG translate:

           UU_BEDEUTUNG de hominibus:
                * ALCUIN. carm.; 15,3 "doctor in orbe pius, Christi clarissima #salp.|ix {(sc. Leo papa)}."
                * CARM. Paul. Diac. app. I; 8,7 "Zacharias frater ... Hiberniae ... decus, mundi gratissima #salp.|ix."
                
           UU_BEDEUTUNG de instrumento medico fort. i. q. genus fistulae -- viell.: eine Art Katheter:
                * ARS med.; 6,39 p. 33 (ed. Fischer) "#ialpas {@ salpinx_2} (ialpes {@ salpinx_3} {G}, κάλπυτον {B}) (({v. notam ed. p. 37}))."
                
AUTORIN Weber