LEMMA  saccellus

    (sace-)

GRAMMATIK  substantivum I; -i m.

SCHREIBWEISE

    script.:
    
        sache-: {=> saccellus001}

    metr.:
    
        să-: {=> saccellus002}

BEDEUTUNG  saccus parvus, sacculus -- Säckchen, Beutel:

    U_BEDEUTUNG  marsupium -- Geldbeutel:

        UU_BEDEUTUNG  in univ.:

            * IONAS BOB. Columb.; 1,22 p. 203,7 "his, qui #sacellum cum aureis furaverat."

            * VITA Mauric.; 162 "incidit interea medium delatus in agmen gaudentum livore suo spoliisque #sacellis, gaudentum, quorum volitabat ad ethera clamor."

            * VITA Rimb.; 14 "semper constitutos habebat, qui pecuniam in #saccell.|is ... gestarent."

            * HROTSV. Gong.; 191 "Gongolfi sensit {(sc. vir quidam)} pia facta #sacelli."

            * EGBERT. fec. rat.; 1,859 "omnes veniunt vacuis ad festa #sacellis {@ saccellus002}, qui nostris inhiant, cum nemo diaria portet."

            {al.}

        UU_BEDEUTUNG  fiscus -- (Staats-)Kasse, ‘Fiskus’:

            * DIPL. Merov.; 142 p. 359,6 (a. 695) "soledus ducentus, quod de #saccell.|o {!publico} annis singolis ... fuit consuetudo in alemunia ... dandi ((145 p. 366,37 "nisi quod ipsam {(redibitionem)} inferendam ... abbas ... annis singulis in #sacellum publicum reddere deberet." {al.}))."

            * DIPL. Karoli M.; 123 p. 173,10 "si quis fuit dux, <qui> ... bonitatem piorum ... violare presumserit, ... tercia parte ad fisci nostro #sacello multa conponat ({sim.} 141 p. 192,42. 195 p. 263,2)."

            {al.}

    U_BEDEUTUNG  usu medic.:

        UU_BEDEUTUNG  spectat ad homines:

            * AESCULAPIUS ; 3 p. 6,1 "inundato #saccell.|o calido et farina in vino cocta stomachum vaporari dicimus."

            * PAUL. AEGIN. cur.; 46 p. 23,10 "radentes caput calefacere oportet ex sale ... et similibus in #sacell.|is."

            * HILDEG. phys.; 1,70 p. 536,8 "semen lini ... expressa aqua in #saccell.|um fusum loco splenis ... cum #saccell.|o superponat."

            * CHIRURG. Sudhoff; II | p. 160 "omnia per #sacellum super aquam frigidam cola {eqs.}"; p. 179 "que {(sc. herbae)} relinquuntur in #sacello (sacculo {var. l.}) post factum claretum" {persaepe.}

        UU_BEDEUTUNG  spectat ad animalia:

            * MOSES PANORM. marisc.; 21 p. 190,18 "da ei {(equo)} annonam suam in quodam #sach|.ell.|o {@ saccellus001} capiti suo appenso."

    U_BEDEUTUNG  de sacculo reliquiis conservandis destinato:

        * HARIULF. chron.; 4,35 p. 272,11 "super pectus eius {(Gervini)} posito reliquiarum ... #saccell.|o (sacculo {@ saccellus003} {var. l.}) congregatio tota inchoavit litaniae supplicationes."

    U_BEDEUTUNG  usu communi:

        * CAES. HEIST. mirac. I; 3,15 p. 130,27 "si quid de tali opere vendere volueris {(sc. argentarius)}, in #saccell.|o pone ... et ... solus ... venias ad domum meam {(sc. clerici)}."

AUTOR  Fiedler