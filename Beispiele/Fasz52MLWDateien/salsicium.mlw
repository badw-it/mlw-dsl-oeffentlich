LEMMA salsicium

VEL salsucium

GRAMMATIK
       subst. II; -i n.
       -a, -ae f.: {=> salsicium_6; salsicium_10}

SCHREIBWEISE
       script.:
             {?}salmum: {=> salsicium_1}
             salcic-: {=> salsicium_7; salsicium_8; salsicium_9}
             salic-: {=> salsicium_2}
             -iti(a): {=> salsicium_6; salsicium_8; salsicium_9}
             -uti(um): {=> salsicium_3; salsicium_4; salsicium_5} {al.}

BEDEUTUNG strictius i. q. farcimen, caro sale conditus -- Wurst, Fleischsülze:
       U_BEDEUTUNG in univ.: 
       * QUERELA magistri; 403 "ut ... veruta ferant {pueri} puncto #sal.|cicia {@ salsicium_7} plena."
       * CHART. Bern.; $II 281 p. 307,36 "si salem aliquis vendere voluerit ..., #salicium {@ salsicium_2} non scindat, sed integrum vendat
             ((
               *{cf.} ; 720 p. 789,27 salmum {@ salsicium_1}
             ))."
       * FABULAR. MIN. ; 14,1 "trans rus portavit #sals.|ucia vir, properavit in spe nummorum mane subire forum."
       * CONST. imp. II; 439,11 "statuimus, quod duo bona #sals.|utia {@ salsicium_4} et magna ... dentur pro I denario."

       ANHÄNGER in tributo:
             * COD. Eberh.; $II p. 119,18 "in pascha {(sc. reddantur)} ... I hamma cum osse longo, #sals.|ucia V."
             * CHART. Naumb. I; 391 p. 350,27 "quicumque pernam fecerit ..., dabit dexteram scapulam preposito et I #sals.|utium {@ salsicium_3} habens longitudinem unius brachii."
             * REGISTR. Geisenf.; 2 "cum villici ... in nativitate afferunt redditus, ... eis dantur ... unam slaufbraten et IIII^or #sals.|ucia {('wuͤrst')}."
             {al.}
      
       U_BEDEUTUNG de singulis #salsici.|is:
             * REGISTR. Geisenf.; 9 "tertio {(sc. die in hebdomada)} #sals.|utium {@ salsicium_5} {!iecorinum} {('leberwůrst')} ... datur ... domicelle
             (( 
               *{sim.} *REGISTR. Xant.; p. 82,28 "decania habet speciale servicium de festis ...: in vigilia Victoris ... III #sals.|ucia iecorina" {ibid. al.}
              ))"
             * REGISTR. Xant.; p. 82,34 "in nativitate Domini ... I #sals.|ucium de stomacho."; p. 83,15sq. "cum de XII festis debeantur XII #sals.|ucia stomachalia, erunt simul LX #sals.|ucia, que faciunt XX carnes porcinas."
             {al.}
             * ALPHITA; M 115 "mecare, id est #sals.|ucie {@ salsicium_10} facte de carnibus intestinorum cutis."
   
       U_BEDEUTUNG pro (cog)nomine:
             * ACTA civ. Rost.; A $I 1,36 p. 42 (s. $XIII.^med.) "Radolfus filius Conradi cum #S|.als.|ucio deprehensus fuit cum veste et abiuravit civitatem."

BEDEUTUNG latius i. q. cena (salsa), cibus (salsus) -- (salziges) Essen, (salzige) Speise:
       * ALBERT. M. pol.; 3,7^y p. 261^b,40 "melius iudicat 'carpentario et #salsi.|tiam {@ salsicium_6} ((* $ARIST.; p. 1282^a,22 "θοίνην")) epulans, sed non coquus'."; 7,2^n p. 637^a,27sq. "'neque venari', supple oportet, 'ad #salcitiam' {@ salsicium_8} ((* $ARIST.; p. 1324^b,39 "θοίνην")), #salcitiae {@ salsicium_9} ... dicuntur salsae assaturae ..., 'vel hostiam' diis scilicet offerendam." 

   AUTORIN Weber