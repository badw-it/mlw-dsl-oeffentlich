LEMMA  *sabelus

    (zabilis, zebulus, thebalus, teb-, -erus)

GRAMMATIK  substantivum II; -i m.

ETYMOLOGIE  slav. sóbol': * {cf.} F. Kluge, Etym. Wörterb. der deutschen Sprache. ^{22}1989.; p. 815

STRUKTUR

    neutr. pl.:
        -lla: {=> sabelus001}

BEDEUTUNG  animal quoddam (({Martes Zibellina L., 'Zobel'; fere spectat ad pellem animalis ['Zobelfell']; de re v.} {=> sabelinus/Grzimek001})):

    * EGBERT. fec. rat.; 1,450 "migale, cuniculus, sophorus (gloss.: #zabilis) nigredine pulchra {(v. notam ed.)}."

    * ALBERT. STAD. Troil.; 4,755 "armellus, castor, martur, bever atque #sabe.|rus dat varium pannis convenienter opus."

    * ACTA civ. Rost.; C 620 "Iohannes Halshaghen et Iohannes Krampo impignoraverunt Hencen Rufo horrum, fenum et aream et IV #tebella {@ sabelus001} pro XVI 1/2 mr."

    * ALBERT. M. animal.; 7,49 "saffireon ... est #thebalus dictus latine, irsutam et nigram habens pellem pretiosam valde, qua utuntur ante pallia varia."

    * CARM. Bur.; 134,10 "nomina paucarum sunt ... socianda ferarum: ... martarus et magale, luter, castor #te|.bel.|us_que (#zebulus_que {var. l.}; gloss.: zobel)."

        VERWEISE {cf.} *sophorus

AUTOR  Fiedler