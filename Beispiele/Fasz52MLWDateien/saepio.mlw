LEMMA
saepio (se-)

GRAMMATIK
verbum IV; saepsi, saeptum, -ire


SCHREIBWEISE
    script.:
        ca-, c(o)e-: {=> ca_ce_coe_1; ca_ce_coe_2; saepes/ca_ce_coe_3; ca_ce_coe_4}
        sce-: {=> scepta}

FORM
    form.:
        perf.:
            -iv(i): {=> perfivi} {al.}
        partic. perf.:
            -tr(um): {=> septrum}

GEBRAUCH
    partic. perf. usu subst.: {=> ususubst}

BEDEUTUNG proprie:
    UNTER_BEDEUTUNG strictius:
        UNTER_UNTER_BEDEUTUNG saepe cingere, con-, circumsaepire -- mit einem Zaun versehen, einzäunen, -frieden, -hegen; absol.: {=> absol}: 
*FOLCUIN. Bert. ; 2,21 (chart. s. IX.^med.) "si non caballicant {homines}, #sepiunt virgas V et in monasterio inter omnes virgas IIII (({cf.} 2,20 "cludunt"))." {al.} 
*CHART. select. Keutgen ; 142,10 "si civis concivem edificando vel #sepiendo {@ absol} conturbaverit." 
*REGISTR. Maxim. ; p. 41,29 "#sepit mansus circa culturam nostram XV pedes." {saepius.} 
*CHART. Turg. III ; 330 p. 68,30 "prata ... debent intrante mense Maio #sepiri seu custodiri, quod vulgariter dicitur ‘gefridot’."
        UNTER_UNTER_BEDEUTUNG circumdare -- umgeben; praesertim c. sensu tuendi; in malam partem i. q. in-, circumcludere -- umzingeln, einschließen: {=> umzingeln_1; umzingeln_2}:
            UUU_BEDEUTUNG in univ.:
                UUUU_BEDEUTUNG spectat ad homines; latrunculos ludi: {=> umzingeln_1}:
*WALAHFR. Mamm. ; 19,5 "Mammem ... in media vidit {miles} fornace sedentem ... sancto agmine #septum." 
*NOTKER. BALB. gest. ; 1,18 p. 24,4 "sedebat {episcopus} ... ditissimorum militum cohortibus #septus." 
*HROTSV. Bas. ; 76 "illic {(sc. in concilio)} inventor sceleris ... consedit medius corvino milite #saep|tus." *Gall. I 12,3 "tribuni cum suis legionibus advenere meque euntem undiquesecus #saep|sere." 
*RHYTHMIMACH. ; 10,40 "soli primi et incompositi vagentur tuti, nisi ita sint adversariis #septi {@ umzingeln_1}, ut per legitimos cursus non possint evadere (({ex} ASILO rhythmimach. ; 45 "circumsaepti"))."
                ANHÄNGER in imag.: 
*PURCH. Witig. ; 122 "si vir queat unus haberi inventus ... virtutibus undique #septus {(v. notam ed.)}."
                UUUU_BEDEUTUNG spectat ad res: 
*FORM. Augiens. C ; 14 "mella geris tecum firmato glutine #septa dulcia." 
*RUODLIEB ; V 2 "curtis fuit amphiprehensa, que medio vacua, scenis foris undique #septa." 
*OTTO FRISING. gest. ; 1,21 p. 35,10 "dux ... in ... silvarum abditis locis #septis undique silvis ... castra posuit." * 2,21 p. 124,2 "Terdonenses artissimis #septi {@ umzingeln_2} claustris effugii locum non habentes."
    * 2,21 p. 125,31 "ut naturae presidiis #septos {@ ususubst} per inopiam cogeret {rex} potus" {v. et {=> saepes/ca_ce_coe_3}}
                ANHÄNGER involvere, tegere -- umschließen, einhüllen: 
*TRAD. Frising. ; 39 p. 67,8 (a. 770) "fragilis #septam parietis ahimam {(sic)} fictili tecumine subpositam contueor."
            UUU_BEDEUTUNG redimire, (circum)ornare -- bekränzen, (ringsum) schmücken:
                UUUU_BEDEUTUNG de hominibus: 
*WALAHFR. (?) carm. ; 60,1 "Hartperto superi #septo diademate regni ... devotus nuntiat N. {eqs.}" 
*CHRON. Salern. ; 103 "a papa ... oleo uncionis est {Lodoguicus} unctus coronaque suo prorsus capite #septus et ab omnibus imperator augustus est ... vocatus (({sim.} * 169 p. 172,21)) ({cf.} {=> saepto/saepio})."
                ANHÄNGER adde:
*CHRON. Salern. ; 113 p. 127,2 "Agarenus ... lorica ... indutus et capite calea #septus et sex lanceis propria manu gestans super eum {(Petrum)} irruit."
                UUUU_BEDEUTUNG de rebus: 
*WALTHARIUS ; 291 "ingreditur ... aulam velis rex undique #septam."
            UUU_BEDEUTUNG (de)finire, (de)terminare, (de)limitare -- um-, be-, abgrenzen:
                UUUU_BEDEUTUNG in univ.: 
*POETA SAXO ; 2,181 "quos {(sc. fines Saxoniae)} #sepserat (#seperat {a. corr. G}, #ceperat {B}) ad borealem Albia lata plagam iuxta confinia terrę Danorum." 
*OTTO FRISING. gest. ; 1,32 p. 49,25 "habet {(sc. Pannonia)} ... terminos ... non tam montium vel silvarum quam cursu maximorum fluviorum #septos." *2,13 p. 114,18 "haec {(sc. terra)} Pyreneo seu Apennino, altissimis et scopulosissimis alpibus in oblongum ductis, hinc inde #septa ... a Tyrreno mari usque ad Adriatici equoris horam protenditur."
                UUUU_BEDEUTUNG de lapidibus i. q. circum caedere -- ringsherum zuschneiden, behauen: 
*HUGO FARF. opusc. hist. ; p. 30,28 "pavimenta ... lapidibus quadratis et #septis (sectis {ed. princ.}) omnia strata erant."
        UNTER_UNTER_BEDEUTUNG claudere, includere -- einpferchen, einsperren; in imag.: 
*CAND. FULD. Eigil. II ; 8,6 "‘...’, dixit et introrsus oris spiramina #sepsit."
    UNTER_BEDEUTUNG latius i. q. complecti, replere -- erfassen, erfüllen: 
*ABBO SANGERM. bell. prol. ; 20 "ut ... #sepiat (gloss.: repleat) ethera palma volans."
BEDEUTUNG translate:
    UNTER_BEDEUTUNG munire -- schützen: 
*DIPL. Heinr. II. ; 345 "abbaciam ... precepto nostre emunitatis #sepivimus {@ perfivi}."
    UNTER_BEDEUTUNG de-, comprehendere, affligere -- umfangen, ergeifen, befallen: 
*CARM. de Nyn. ; 221 "fures vertigine clausit, et cunctos pariter tetra dementia #sepsit." 
*GERH. AUGUST. Udalr. ; 1,1 l. 151 "ille {(Rampertus)} ancipiti timore #septus viventis atque defuncti domini {(sc. Adalperonis)} coepit oculis pavens in eum conspicari." *1,26 l. 2 "moenia ... Augustae civitatis introgressus magna tristicia #septus est pro morte Adalperonis sui nepotis." *1,27 l. 120 "sanctus episcopus {(sc. Wolfgangus)} magna mesticia #septus est." *1,27 l. 152 "tu {(sc. episcopus)} es magna lassitudine #septus."
    ANHÄNGER fort. huc spectat:
*CHRON. Salern. ; 122 p. 136,6 "Christe, penitentes et dolentes suscipe et, qui sunt #septi peccatorum pondere ({an leg.} sopiti{?}) (({cf.} THANGM. Bernw. ; 41 p. 776,48 add. codd. 3. 4. 5))." 
*CHART. Eichsf. ; 385 "ne gestarum rerum memoria oblivionis caligine #sepiat ({vix recte; fort. leg.} #sepiatur {vel potius} sopiatur)."

SUB_LEMMA
saeptus

GRAMMATIK
adiectivum I-II; -a, -um

BEDEUTUNG conclusus -- (ab)geschlossen: 
*LEX Baiuv. ; 10,2 "si conclusa parietibus et pessulis cum clave munita fuerit {scuria} ...; si autem #septa (#septe {var. l.}) non fuerit, sed talis, quod Baiuuari scof dicunt, absque parietibus {eqs.}"

SUB_LEMMA saeptum

GRAMMATIK
subst. I; -i n.
-us, -i m.: {=> ca_ce_coe_1; ca_ce_coe_2}
-a, -ae f.: {=> fem} {al.}

GEBRAUCH

    fere usu plur.: {}

BEDEUTUNG ager, locus circumsaeptus, conclusus, definitus, fines -- eingefriedeter, umzäunter, abgegrenzter Bereich, (um-, abgeschlossenes) Landstück, Gebiet, Bezirk:
UNTER_BEDEUTUNG in univ.; saepe c. gen. explicativo, e. g. {=> explicativo_1}{sqq.} {=> explicativo_2}:
UNTER_UNTER_BEDEUTUNG gener.; in imag.: {=> imag}:
*PAUL. DIAC. Lang. ; 3,30 p. 110,9 "quod {(lignum)} ˻in regiis #septis˼ (gloss. L1: in clusura de ipso rege) situm erat." 
*CHART. Fuld. B ; 188 p. 284,7 "tradimus {(sc. Altfrid et Folcrat)}, quicquid ... proprietatis habemus ..., extra illum #caeptum {@ ca_ce_coe_1}, id est bifang, qui est in Giusungom; #coeptum {@ ca_ce_coe_2} illum foras dimitto." 
*DIPL. Karoli M. ; 247 p. 350,10 (spur.) "#septa apum {(sc. tradidit Tassilo)}." 
*TRAD. Salisb. I ; 40^b "Einhart ... duo #septa cum domo, quorum #septorum quinque iugera sunt exceptis XIII virgis mensuratis, ... tradidit" *44 "Eccha ... unum #septum mancipiaque duodecim ... tradidit."
* CHART. Wirt. ; 2813 "cum ... pertinentiis tam tactis quam intactis, cultis et non cultis, in #ceptro {@ septrum} et in cespite (({cf.} 3141. 3151))." {saepe.}
ANHÄNGER c. gen. explicativo {@ explicativo_1}: 
*IONAS BOB. Columb. ; 1,14 p. 174,13 "qui {(sc. gentes)} {!intra Alpium} #septa et Iurani saltus arva incolent ((*PRUD. TREC. annal. ; a. 838 p. 23,15 "imperatori sermo innotuit Hlodouuicum ... i|.ntra A|.lpium #septa colloquium expetisse." {sim.} *WALAHFR. carm. ; 38,19 "heu, quibus insidiis artissima #septa viarum alpibus in mediis sollers custodia cinxit"))." 
*SYLL. Sangall. app. ; 5,23 "inter quas {(Musas)} prima femina lucet anus ({in marg.:} GRAMMATICA) componens pueris labrorum #septa {@ imag} decenter." 
*SIGEBOTO Paulin. ; 19 "#septa silvarum domum effecit orationis." {al.}
UNTER_UNTER_BEDEUTUNG eccl.: 
*CONC. Merov. ; p. 126,14 "donec revertatur {monachus} ad #septa {@ explicativo_2} {!monasterii} ((* IONAS BOB. Columb. ; 1,20 p. 194,2 "milites ... peragrantur #septa m|.onasterii, virum Dei perquirunt." {saepe.} {sim.} *1,10 p. 170,4 "cernens ... unius caenubii #septa [#septo {var. l.}] tantum conversantum cohortem ... non teneri." *1,19 p. 191,11 "se dicit de caenubii #septa [#septis, #septo {var. l.}] non egressurum." {saepe}))." 
*IONAS BOB. Columb. ; 1,19 p. 190,8 "cur ... intra #septa (#scepta {@ scepta} {var. l.}) secretiora omnibus christianis aditus non pateret." 
*PAUL. DIAC. carm. ; 8,2 "Musae claustrorum #saept|is nec habitare volunt." 
*CARM. Paul. Diac. ; 36,29 ((ed. K. Neff. 1908.; p. 155)) "properasti {(sc. Paulus)} ... Christo subdita colla dare Benedicti ad #saept|a  {!beati}  ((* THEOD. TREV. transl. Celsi ; 8 "ad #septa b|.eati Eucharii"))." {saepe.}
UNTER_BEDEUTUNG σηκός -- Gehege, Pferch; in imag. vel per compar.: 
*IONAS BOB. Columb. ; 2,10 p. 253,26 "lupi rabię morbo detenti duos ex his, qui fautores erant ... adsertionis, ... intra #septam {@ fem} (#septum, #septa {var. l.}) inrumpentes ... laniarunt." 
*PAUL. DIAC. (?) carm. ; 5,3,2,1 "crux ... sit protectio #saept|is, ne lupus insidians possit adire gregem." 
*RUOTG. Brun. ; 33 p. 34,8 "si quos aut in suorum ovilium #septis aut extra repperit, qui ... solitariam vitam appeterent." 
*WALTH. SPIR. Christoph. II ; 5,86 "ego te dextris inducam, filia, #septis, qua lupus inbelli non sollicitatur ovili. 6,41 ianua tuta tuum claudas {(sc. Christus)} in sęcula #septum, cuius perpetuam pandit reseratio vitam." 
*CHRON. Pol. ; 3,21 p. 147,10 "Bolezlavus repertis hostibus ... quasi leo visa preda #septis conclusa stomachabatur." {al.}
UNTER_BEDEUTUNG cubiculum, camera, cella, tugurium — kleiner Raum, Kammer, Zelle, Hütte: 
*PAUL. DIAC. carm. ; 6,125 "rupea #saept|a (gloss.: speluncae illius {[sc. Benedicti]}) petens nancta est {(sc. mulier)} errore salutem." 
*EKKEH. IV. cas. ; 122 "ad quam {(fenestrellam)} #septo deforis aptato semet includere et finem vite ... devovit {Purchardus} exspectare; ... vir ille sanctus #septo iam illi tabulata per artifices praeparans {eqs.}" 
*YSENGRIMUS ; 4,113 "quod si pravus in hoc quis repserit advena sęptum."
ANHÄNGER spectat ad sepulcrum: 
*WANDALB. Goar. ; 2,1 p. 45,4 "quo {(latere)} levato et modico foramine patefacto debilem manum adhibuit et olosericam vestem, qua infra #septum (septem {var. l.}) latericio pariete structum sarcofagum corporis tegebatur, comprehensam traxit." 
*EPITAPH. var. I ; 17,24 "mausolei #septum nulla manus violet."
UNTER_BEDEUTUNG carcer -- Kerker: 
*IOH. SCOT. carm. ; 2,2,20 "#septa (gloss. R: claustra) subis {(sc. Christus)} Erebi."
UNTER_BEDEUTUNG tegumentum (carnale), involucrum -- (Körper-)Hülle: 
*AGIUS epic. Hath. ; 290 "bis sex #saept|a (serta {cod.}) modo sunt adoperta solo (({cf.} $CLAUD. Mam. anim. ; 3,14 p. 182,1))."
BEDEUTUNG saepimentum, saeptio(nis orbis) — Einfriedung, Einzäunung(sring):
UNTER_BEDEUTUNG strictius:
UNTER_UNTER_BEDEUTUNG in univ.: 
*IONAS BOB. Columb. ; 2,25 p. 293,19 "Leubardus cum Meroveo ... #septa vineae ... densabant atque muniebant." 
*FORMA mon. Sangall. ; 11,1 "haec ... #septa premunt discentis vota iuventae." 
*HIST. peregr. ; p. 169,30 "Turcorum agmina ... per portas se infra #septa murorum recipiunt fugiendo."
UNTER_UNTER_BEDEUTUNG spectat ad loca sacra:
UUU_BEDEUTUNG christianorum: 
*IONAS BOB. Columb. ; 2,5 p. 237,6 "#septa {!monastirii} densat, tegumenta renovat ((* 2,19 p. 272,4 "cum ... #septa monasterii per repagula scalae transilire conarentur {(sc. delinquentes)}." *STEPH. Wilfr. ; 47 p. 242,8 "quod ... #septa monasterii absque licentia regis non transmearet" *DIPL. Ludow. II. ; 53 p. 170,31 "ut ipse {(sc. imperator)} ... monasterii #septum muniret." {al.}))."
ANHÄNGER de muris ecclesiae:
*ADSO Frodob. ; 13 ((MGMer. V;    p. 78,25)) "intra #septa basilicae eiusdem monasterii nobilem sortiti sunt {(sc. episcopi benefactores)} sepulturam." 
*ARBEO Emm. ; 35 p. 78,6 "eiecto populo extra meneis (#septa {var. l.}) templi ostium ecclesiae sacerdotes ... serris munierunt." {al.}
UUU_BEDEUTUNG paganorum: 
*HELM. chron. ; 84 p. 160,23 "contrivit {episcopus} ... insignes portarum frontes, et ingressi atrium omnia #septa (#cepta {@ ca_ce_coe_4} {var. l.}) atrii congessimus circum sacras illas arbores et de strue lignorum iniecto igne fecimus pyram."
UNTER_BEDEUTUNG latius i. q. rete, nassa -- Fischwehr: 
*CHART. Wirt. II app. ; 1 p. 392,23 (s. XII.^med.) "infra ... silvam in Murga continetur #septum maiorum piscium captioni aptum." *p. 393,21 "quam {(traditionem)} quidam non minime libertatis fecit ... offerens ... dimidium aquaticum #septum." 
*DIPL. Frid. I. ; 981 p. 265,20 "licebit ipsis civibus et eorum piscatoribus piscari per omnia a supradicta villa Odislo usque in mare preter #septa comitis Adolfi." 
*CHART. Lub. I ; 124 p. 122,21 "concedimus civitati ... in aquis nostris ius piscandi exceptis nostris #septis, que war dicuntur."
BEDEUTUNG corrupt. in nomine loci; ‘Septizodium’, ‘Septasolium’; de re v. S. S. Lusnia, American Journal of Archaeology. 108. 2004. p. 517sqq.:
*CHART. Ioh. Halb. ; 11 p. 14,13 (epist. papae a. 1145) "ego Rodulfus diaconus cardinalis sancte Lucie in #Septa solis ss." 
*CHART. com. Mansf. VII ; 25 p. 403,8 (epist. papae) "ego Pelagius sancte Lucie ad #Septa solis diaconus ss."

AUTOR
Staub
