LEMMA sanctimonia

GRAMMATIK subst. I; -ae f.

 BEDEUTUNG sanctitas, pietas -- Heiligkeit, Frömmigkeit:

     U_BEDEUTUNG spectat ad homines:

         UU_BEDEUTUNG proprie:

             UUU_BEDEUTUNG in univ.:
                 * WILLIB. Bonif.; 8 |p. 47,16 "ut et locus et lingua relatam a nobis beati viri #sanctimoni.|am aequaliter prodat."; p. 53,3 "singularem #sanctimoni.|ae ac privatam castimoniae ... gerebat {Hadda} vitam
                 ((
                     *{sim.} * CAPIT. reg. Franc.; 33 p. 95,6 "qui castitatis et #sanctimoni.|ae emendatiores esse cupimus." {al.}                               
                 ))"
                 * UFFING. Ida; 2,7 p. 487,24 "aliqua gesta de beatae Idae #sanctimoni.|a populo succincte recitavit {episcopus}."
                 * DIPL. Otton. III.; 79^b p. 487,2 "ad ... augmentum ... #sanctimoni.|e beate congregationis."
                 * THANGM. Bernw.; 41 p. 776,29 "semet ipsum in #sanctimoni.|a solita ... exercuit."
                 {al.}

             UUU_BEDEUTUNG c. gen. explicativo:
                 * WILLIB. Bonif.; 1 p. 4,24 "divinae contemplationis eius #sanctimoni.|am relevare."
                 * ERMENR. Har.; 108 "ut ipse Grimoldus propter castiorem vitae #sanctimoni.|am cuidam proximo fonti se inmergeret
                 ((
                     *{sim.} DIPL. Conr. III.; 297 [spur.] "per #sanctimoni.|am vite innocentis"
                 ))"
                 * VITA Liutb.; 27 "veluti licenter accepto potu #sanctimoni.|am {!religionis} ... ad nichilum deputasti
                 ((
                     * CHART. Hall. I; 226 p. 215,39 "ecclesiis ... r. #sanctimoni.|am profitentibus pastoris non desit diligencia"
                 ))"
                 {al.}
                  
         UU_BEDEUTUNG meton.:

             UUU_BEDEUTUNG in allocutione honorifica:
                 * HADR. II. epist.; 39| p. 752,11 "apud #sanctimoni.|am tuam {(sc. Ignatii patriarchae)}"; p. 753,21 "adversus #sanctimoni.|am tuam {(sc. Ignatii patriarchae)}."                
                 * EPIST. Col.; 135 p. 247,15 "reverendam et praedicandam #sanctimomi.|am vestram {(sc. papae)} superna gratia semper inlustrare dignetur."
                 * FLOD. hist.; 4,1 p. 557,30 "tue {(sc. Fulconis praesulis)} iniungimus {(sc. papa)} #sanctimoni.|e, ut {eqs.}"
                 ANHÄNGER sensu collectivo:
                     * LEG. Wisig.; suppl. p. 481,32 "#sanctimoni.|ae vestrae {(sc. patrum in synodo residentium)} adminiculo fultus {(sc. Flavius Egica rex)}."
             UUU_BEDEUTUNG sanctimonialis status -- Nonnenstand, Nonnentum:
                 * WALAHFR. Gall.; 1,22 p. 300,35 "deposito ... terrenae dignitatis amictu #sanctimoni.|ae nitorem cum quodam sui secuta contemptu accessit {(sc. puella, quae regi nubere debuerat)} ad altare."
                 * CAPIT. reg. Franc.; 252 p. 228,6 "si sponte velamen ... sibi inposuerit ..., velit nolit #sanctimoni.|ae habitum ulterius habere debebit {vidua}."
                 * PONTIF. Rhen. sup.; 148 "sanctimonialis {@ sanctimonialis_7} virgo ... in talibus vestibus adplicetur, qualibus semper usitura est, professione ({leg.} professioni) et #sanctimoni.|ae aptis
                 ((
                     * {inde} PONTIF. Rom.-Germ.; 20,2
                 ))"
                 * GESTA Trev. cont. I; 2 p. 176,38 "eam {(puellam vitam canonicam profitentem)} ..., quae rem hanc {(sc. clericos decantare)} fecisset, ab ordine #sanctimoni.|ae proici dignum esse."
                 * IDUNG. PRUF. argum.; 471 "parabolice potest {(sc. sanctimonialis)} dici ... vas aureum propter virginalis #sanctimoni.|ae propositum."
                  
                             
         U_BEDEUTUNG spectat ad Deum:
                 * CHART. Heinr. Leon.; 4 p. 5,20 "in domo Dei #sanctimoni.|e magnificencieque celestis per ipsius sanctificationem {@ sanctificatio_4} nunquam deesse potest ubertas."
            
         U_BEDEUTUNG spectat ad res:
                 UU_BEDEUTUNG proprie:          
                         * ARNOLD. LUB. chron.; 2,14 p. 135,15 "nec multum ab hostibus querebatur ignis, eo quod propter #sanctimoni.|am (sanctimonium {@ sanctimonium_1} {var. l.}) loci parcerent civitati."
                         * STATUT. ord. Teut.; p. 64,16 "sacerdotes et clerici fratres debent venerari et in necessariis pre aliis procurari, quos commendat consecracionis #sanctimoni.|a."
                               
                 UU_BEDEUTUNG meton. i. q. sacrarium -- Heiligtum:
                         *TRANSL. sang. Dom. in Aug.; 10 p. 156,4 "Hunfridus Uualdonem ad observandam ... tanti thesauri #sanctimoni.|am {(sc. cruciculam sanctam)} relinquens."; 25 p. 160,23 "quatenus talem #sanctimoni.|am {(sc. cruciculam sanctam)} in aliqua sinat {domina} basilica nocte illa collocari."

ABSATZ

BEDEUTUNG sanctificatio, benedictio -- Heiligung, Segnung:
         * CONC. Karol. A; 44^B| p. 481,24 "isti non mediocriter erraverunt, qui ... #sanctimoni.|am ab eis {(imaginibus sanctorum)} se adipisci professi sunt."; p. 489,19 "nunc contra eos, qui ... #sanctimoni.|am se per eas {(imagines sanctorum)} adsequi fatentur."

AUTORIN Weber