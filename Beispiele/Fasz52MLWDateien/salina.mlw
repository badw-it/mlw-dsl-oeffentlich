LEMMA   salina

GRAMMATIK
    subst. I; -ae f.

VEL *salinum

GRAMMATIK
    subst. II; -i n.

SCHREIBWEISE
    script.:
        {?}sosina: * ACTA imp. Winkelm.; $I 783 p. 613,42 {(v. notam ed.)}

STRUKTUR
    
    form.: 
 
        abl.: 

            sg.:
                -e: * BENED. ANDR. chron.; p. 170,9 
                    * DIPL. Heinr. II.; 425 p. 540,23
            pl.:
                {?}salis: * DIPL. Heinr. III.; 198^a p. 252,6 (({v. notam ed. et cf.} * 198^b p. 251,35 "#salin.|is"))
    
BEDEUTUNG   proprie: 

    UNTER_BEDEUTUNG locus ubi sal foditur vel conficitur -- Salzgrube, Salzwerk, 'Saline':

        UNTER_UNTER_BEDEUTUNG in univ.:

            * TRAD. Ratisb.; 211^a (c. 980) "infra #salin.|am Bauvariensem, quam vulgo comprovinciales Hal solent nuncupare."

            * GESTA Mett.; praef. p. 534,19 "Mettis est civitas ... vicinarum venis #salin.|arum illustrior urbibus ceteris."

            * DIPL. Frid. I.; 562 p. 29,28 "cum ... #salin.|o et rudere, quod ariz dicitur (({sim.} * CHART. Babenb.; 409 p. 257,40 "in #salin.|a et rudere, quod aerz dicitur." {al.}))." 
            
            * CHART. Hall.; 133 p. 128,12sq. "dominus ... puteum #salin.|arum ... de suis sumptibus preparavit, cuius #salin.|e proventus ecclesie ... usufructuando ... condicit (({sim.} * CHART. Basil. A; $I 374 p. 552,17 "in ... puteis #salin.|orum." {al.} {cf. {=>salina_2}}))."

            * CHART. Babenb.; 176 p. 235,32sqq. "#salin.|am iam pene exhaustam et deficientem ... fratres renovare volentes et amplificare per manus opificum ... novam #salin.|am foderunt, in qua exuberantem salis venam invenerunt." {saepe. v. et {=>salinator/salina_1}} 

        ANHÄNGER    in nomine loci: 
    
            * DIPL. Karoli III.; 128 "de curtibus ... subter nominatis ..., id est ... de Salzburchhoue, de #S|.alin.|a, de Atilla {eqs.}"; 140 p. 226,37 "in pago Senonensi ... lebetes ..., que sunt in Grosono et in #S|.alin.|is {(sc. concedimus)}."

            * CHART. Landeshut.; 117 "ut de coccione patelle nostre salis nudi ac aridi apud #S|.alin.|am ... {@salina_1} monasterio karradas quadraginta ... tribuamus {(sc. abbas)}."
    
        UNTER_UNTER_BEDEUTUNG spectat ad cocturam salis quae apud -am fit:

            * CHART. Stir.; $I 178 p. 182,16 (a. 1139) "ius #salin.|e et eius focariorum ... nos {(sc. archiepiscopus)} ... confirmamus cenobio."

            * CHART. archiep. Magd.; 361 "contulimus {(sc. archiepiscopus)} ecclesie ... in Hallensi civitate tantam porcionem de puteo #salin.|arum {@salina_2}, ut ad coquendum salem quatuor inde panne instruantur."  

            * CHRON. Mont. Ser.; a. 1224 p. 212,22 "ut ... #salin.|a in sartagine buliente corpus meum ... fervoribus inicerem {(sc. homo officio coquendi salis fungens)} cruciandum." {v. et {=>salina_1} {=>salina_4} {=>salino/salina_1}}

    UNTER_BEDEUTUNG locus marinus, lacus ubi caloris causa ex vaporatione aquae salsae sal efficitur -- Salzgarten:

        * VITA Lamb. Font.; 3 ((MGMer. V; p. 611,5)) (paulo ante 811) "largitus est {(sc. Hildericus)} ... venerando patri ... terram super litus maris et areas #salin.|arum piscationumque, quae ibidem institutae erant."

        * GESTA Dagob.; 35 "cum #salin.|is supra mare (({inde} * DIPL. Karoli M.; 282 p. 421,40 [spur.]))."

        * HIST. peregr.; p. 155,17 "ingressi sunt {(sc. nostri)} terram horroris et salsuginis ... sicque ... gradientes nec invenientes gramen ad refocilationem animalium vix pervenerunt ad lacum #salin.|arum (({inde} * HIST. de exp. Frid. imp.; p. 76,9))."

        * IOH. S. PAUL. diaet.; 404 "vel si sit {(sc. marinum sal)} ex lacubus, quos #salin.|as vocamus, vel ex aqua marina igne cocta aestivo sole." {al. v. et {=>salinaster/salina_1}}

BEDEUTUNG   meton.:

    UNTER_BEDEUTUNG sal -- Salz:

        * CHART. Stir.; $I 604 p. 573,5 (c. 1180) "patellarii #salin.|e ... {@salina_4} #salin.|am decoquendam in saugmis vel carpentis ad ... sedes patellarum suarum de Halle usque ad terminos ... prediorum suorum devehebant."

        * CHART. Wirt.; 1986 "sanctimonialibus ... viginti urnas #salin.|e dedi {(sc. Heinricus scultetus)} pro centum libris Hallensium."

    UNTER_BEDEUTUNG genus obsonii, pulmenti -- eine Gerichtart:

        * CONSUET. Vird.; 3 "mox ..., ut aliquis sanguinem dempserit, in refectorium properabit et bucella panis accepta ponet desuper sal et tres #salin.|ae particulas et ... vorans sumet statim puram limpham {(v. notam ed.)}."


AUTORIN Clementi