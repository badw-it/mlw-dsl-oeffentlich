LEMMA 1. sacrifico

GRAMMATIK 
   verbum I; -avi, -atum, -are

STRUKTUR
   partic. perf. usu adi.: {=> sacrifico_1}

BEDEUTUNG 
   sacrificium facere, offerre -- opfern, (ein Opfer) darbringen:

   UNTER_BEDEUTUNG
   in cultu veterum, gentilium:

   * VITA Afrae I; 1 "cum ... omnes christiani ... conpellebantur #sacrific.|are ..., tunc et ... conprehensa est Afra."

   * RIMB. Anscar.; 20 p. 44,15 "cui {(matronae religiosae)} ... in quacumque necessitate positae, ut more eorum {(sc. Sclavorum)}
   idolis #sacrifica.|ret, suggestum est."

   * WALTH. SPIR. Christoph. I; 16 "templum Iovis sub specie #sacrific.|andi ingressę ... clamabant {Nicea et Aquilina:'eqs.'}"

   * ALBERT. M. elench.; 1,6,1 p. 642^a,31 "secundum legem ... erat opinabile, quod in pluribus ad cultum commune conferens esse 
   videbatur, ut aliquando filios #sacrific.|are et patres mactare, quod est contra rationem naturalem." {al.}

   UNTER_BEDEUTUNG in fide Iudaeorum (Veteris Testamenti):

   * HROTSV. Mar.; 92 "non licet incensum ... te {(sc. Ruben)} tangere sanctum, munera nec Domino praestat dare #sacrific.|ando, 
   te quia despexit, sobolis cum dona negavit."

   * CARM. de Frid. I. imp.; 1390 "sic rex Egipti Pharao dimittere Hebreos, #sacrific.|are Deo, quos acriter ipse premebat in regione sua, monitus bis terque negavit {(spectat ad Vulg. exod. 9,1)}."

   UNTER_BEDEUTUNG 
   in fide christianorum: 

      UNTER_UNTER_BEDEUTUNG 
      proprie:

         UUU_BEDEUTUNG
          {theol. et liturg. i. q.} missam celebrare -- die Messe feiern:

          * CONC. Karol. A; 61 p. 796,17 "qui {(sc. Ebo episcopus)} violenter triennio iam expulsus eram a debito #sacrific.|andi 
          loco."

          * GERH. AUGUST. Udalr.; 1,27 l. 150 "nobis {(sc. fratribus)} bonum videtur ..., ut ... 
          a {(var. l.,} ad {ed.)} te {(sc. episcopo)} salutaris hostiae munus nobis #sacrific.|antibus pro requie eius animae Deo offeratur (({sim.} * l. 168 "cunctis pro eius anima #sacrific.|antibus"))."

          * CONSUET. Trev.; 107 p. 351,8 "si ... anniversarium aut obitus alicuius fratris aut cari amici caelebratur, minimum signum 
          breviter, ut omnes ad #sacrific.|andum veniant, sonetur." {al.}

         UUU_BEDEUTUNG
          {publ., eccl., iur., canon. i. q.} donare, largiri -- schenken, spenden:

          * HERBORD. Ott.; 1,3 "in mendicis et pauperibus ... in nomine Domini #sacrific.|andum et ... eciam idoneis 
          et honestis hominibus indigentibus de re familiari imperciendum est."

          * TRAD. Frising.; 1556^b "eo tenore, quod ipse {(Ruodolfus)} super altare sancte Marie ... obtulisset et #sacrifica.|ret ita, 
          ut post mortem meam unusquisque tres nummos episcopo ... persolveret."

          * REGISTR. Baiuv. B; p. 398,20 "solvunt {homines} annuatim xxx denarios, qui #sacrific.|andi sunt super aram 
          beate Marie Nappurch in memoriam perpetuam advocatie hominum."

      UNTER_UNTER_BEDEUTUNG
        translate:

         UUU_BEDEUTUNG
            in univ.:

         * THANGM. Bernw.; 54 p. 781,5 "qui quoadusque vixit, Domino se sacrificium iusticiae #sacrifica.|vit."

         UUU_BEDEUTUNG
             dare -- geben:

            * EKKEH. IV. carm. var.; II 11,14 "omnibus his cantum pro defunctis vigilantum luctus solamen 
              #sacrifica.|bis {(sc. Gallus)}."

ABSATZ

BEDEUTUNG
   sacrum facere, consecrare -- heiligen, weihen:

UNTER_BEDEUTUNG
    apud paganos:

   * IONAS BOB. Ved.; 7 p. 315,17 "cum ... benedictionem cum crucis signo super vasa, quae gentile fuerant ritu #sacrifica.|ta
    (sacrata {var. l.}), premisisset, mox soluta legaminibus cunctum cervisae ligorem ... in pavimentum deiecerunt."

UNTER_BEDEUTUNG
    apud christianos:

    * CONC. Karol. A; 56 p. 712,9 "si ... infirmitate depressus fuerit, ne confessione atque oratione sacerdotali necnon 
    unctione #sacrifica.|ti {@ sacrifico_1} olei per eius neglegentiam careat."

AUTORIN
   Orth-Müller
