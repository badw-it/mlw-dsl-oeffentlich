LEMMA  sabbatizo

GRAMMATIK

    verbum I; -avi, -are

SCHREIBWEISE

    script.:

        -ixa-: {=> sabbatizo001}

STRUKTUR

    depon.: {=> sabbatizo004}

    partic. praes. usu adi.: {=> sabbatizo002}

BEDEUTUNG  sabbatum celebrare -- den Sabbat feiern {(fere de hominibus; de re: {=> sabbatizo002})}:

    U_BEDEUTUNG  strictius:

        UU_BEDEUTUNG  apud Iudaeos:

            UUU_BEDEUTUNG  in univ.:

                * EPIST. Alcuin.; 144 p. 230,19 "sabbatum ... non solum ob superstitionem Iudaeorum ..., sed ne cum Iudaeis #sabbatiz.|antur {@ sabbatizo004} (#sabbatiz.|entur, #sabbatiz.|etur {var. l.}), ieiunium solvere conantur {(sc. patres orthodoxi)}."

                * AGOB.; 11 l. 69 "ne femine christiane cum eis {(Iudaeis)} #sabbatiza.|rent."

            UUU_BEDEUTUNG  in imag.:

                * EPIST. Ratisb.; 5 p. 284,2 "‘quorum {(sc. otiosorum)} deus venter est’, cuius lacunam implere #sabbatiz.|are cum Iudeis est (({cf.} * $VULG. Phil.; 3,19))."

        UU_BEDEUTUNG  apud christianos i. q. observare otium dominicae -- die Sonntagsruhe einhalten:

            * CAPIT. episc.; III | p. 349,10 "diebus dominicis #sabbatiz.|are."; p. 366,5 "adnuntient presbiteri diebus dominicis per annum #sabbatiz.|andum."

            * ANON. algor. Salem.; p. 12,8 "ecclesia octavo die in misterio #sabbiz.|at."

    U_BEDEUTUNG  latius:

        UU_BEDEUTUNG  usu communi i. q. quiescere, ab opere vacare, otio frui -- ruhen, müßig sein:

            UUU_BEDEUTUNG  in univ.:

                * GOZECH. epist.; 22 l. 513 "sicut acceptat Deus, quod practice laboratur, ita non excluditur ab eius beneplacito, quod suo tempore theoretice #sabbiz.|atur (#sabbati.|xatur {@ sabbatizo001} {var. l.})."

                * EBO BAMB. Ott.; 3,22 p. 132,8 "non ... omni tempore #sabbatiz.|are possumus {(sc. rustici)}."

                * CAES. HEIST. hom. exc.; 230 "#sabbatiza.|vit Iudaeus in stercoribus ... usque in quartum diem."

                {v. et} {=> sabbatismus/sabbatismus01}

            UUU_BEDEUTUNG  quiete vivere -- in Ruhe leben:

                * OTTO FRISING. chron.; 7,35 p. 373,17 "tanto ... ardentius caelo suspensi #sabbatiz.|are creduntur {solitarii}, quanto amplius omnis humanae societatis extorres inveniuntur."

            UUU_BEDEUTUNG  spectat ad mortuum in sepulcro:

                * FUND. Torn.; 2 ((MGScript. XV; p. 1114,5)) (c. 1165) "cuius {(Crucifixi)} ossa in medio ecclesie beati Medardi #sabbatiz.|ant."

            UUU_BEDEUTUNG  spectat ad novalia ('brachliegen'):

                * ALBERT. M. animal.; 23,141 "turdus avis ... sedet in glebis agrorum #sabbatiz.|antium {@ sabbatizo002}."

        UU_BEDEUTUNG  celebrare -- feiern {(de die festo)}:

            * WOLFHER. Godeh.; I 26 p. 186,36 "pascha Parthepoli, pentecosten Gosleri #sabbatiza.|vit imperator."

            * ANNAL. Altah.; a. 1055 p. 51,5 "resurrectionem ... Domini apud Mantuam #sabbatiza.|vit imperator."

            * ANNAL. Ratisb.; a. 1085 (MGScript. XIII p. 49,26) "postquam pascha Domini Ratisponae #sabbatiza.|vit imperator."

AUTOR

Fiedler