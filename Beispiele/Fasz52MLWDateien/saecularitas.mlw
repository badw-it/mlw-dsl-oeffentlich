LEMMA saecularitas (se-)

GRAMMATIK subst. IV; -atis f.

BEDEUTUNG mundus, vita mundana -- weltliches Leben:

     U_BEDEUTUNG in bonam vel neutram partem:
          * HUGO FLAV. chron.; 2 p. 482,43 "condempnet {(sc. vita mea)} se totam iudicio suo ... non spe recuperandae #se|.cularitat.|is, sed {eqs.}"
          * GESTA Camer. cont. I D; 19 "sub specie ... #saecularitat.|is vir iste {(sc. episcopus)} militans inter saeculares {@ saecularis_10} omnibus se pro tempore conformans ... multitudine ... benefactorum ... delictorum suorum sarcinam in subditos suos misericors ... abiecisse credatur."
          * ABSAL. serm.; 29 p. 173^D "seculum ... appellatur #se|.cularita.|s, id est forma vivendi saeculariter {@ saecularitas_1}."
          * LAMB. ARD. hist.; 74 "Balduinus ... in omni #se|.cularitat.|is sapientia eloquentissimus."

     U_BEDEUTUNG in malam partem i. q. studium saeculare, saecularium condicio -- Weltzugewandtheit, Weltlichkeit {(usu plur.: { => saecularitas_2})}:
        * GESTA Camer.; 2,27 "quae {(sc. ecclesia in villa Hamatgia sita)} nunc per #saecularitat.|em multum delapsa vix paucos canonicos habet."
        * HUGO FLAV. chron.; 2 p. 495,13 "de ecclesia sancti Martini religionem exturbavit {Norgaudus} et #se|.cularitat.|em introduxit eum {(Gyrardum)} ibi ordinans."
        * CHART. Wirt.; 1638 p. 29,18 "in psallendo et legendo maturitatem, que religionem decet, studeant {(sc. sanctimoniales)} habere vanitates et #se|.cularitat.|es {@ saecularitas_2} cavendo."
        
AUTORIN Weber
