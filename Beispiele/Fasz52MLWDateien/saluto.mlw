LEMMA saluto  

GRAMMATIK
   verbum I; -avi, -atum, -are      

BEDEUTUNG
   salutem dicere, salute impertire -- (be)grüßen:

U_BEDEUTUNG
   proprie:

UU_BEDEUTUNG
   gener.:

UUU_BEDEUTUNG
   in univ.:

UUUU_BEDEUTUNG
    spectat ad homines:

    * WILLIB. Bonif.; 6 p. 28,11 "cum paucis ad invicem ac {!pacificis} se #salut.|assent {!verbis}, ... illum pontifex inquisivit ((* EIGIL Sturm.; 5 p. 135,36 "quos {(socios)} cum vidisset, ab episcopo v. p. illos #saluta.|vit." * WIDUK. gest.; 1,9 p. 15,9 "#salut.|ant {duces} Thiadricum v. p." {al.}))."

    * EIGIL Sturm.; 6 "#saluta.|tis fratribus, quod ibidem {(sc. Hersfelt)} reperit, praeceptum ... exposuit episcopi."

    * WALAHFR. carm.; 61,31 "terra ... te, care, #salut.|et."

    * FROUM. carm.; 21,18 "omnes presbiteros versu ... #salut.|o sub uno: {eqs}."
    {saepe. v. et {=> salutatorius/saluto_4}}

ANHÄNGER
    convenire -- treffen:

    * ERCHANB. gramm.; p. 34,3 "volo ire ad forum videre et #salut.|are amicum meum."


UUUU_BEDEUTUNG
    spectat ad reliquias, imagines sim.:

   * MEGINH. FULD. Alex.; 6 p. 430,17 "ut ... Alexandri martyris caput ... osculando #saluta.|ret {multitudo}."

   * PASS. Trudp.; 9 (ed. F. J. Mone, Quellens. d. bad. Landesgesch. I. 1848. p. 21) "ab episcopo ... caeterisque fidelibus ... visum atque #saluta.|tum corpus sancti Trudberti."

   * HUGO FLAV. chron.; 2 p. 404,50 "reliquias ... sibi afferri praecepit {(sc. abbas moribundus);} et #saluta.|tis eis atque deosculatis cum multa reverentia eas, quas collo ferre consueverat, exposuit."

   * ANNAL. Quedl.; a. 1008 p. 525,12 "amplectitur sancti Petri sepulchrum ... nec non aliorum sanctorum patrociniis #saluta.|tis ... revisit {Bertlalis} patriam."

ANHÄNGER
   in cultu paganorum:

   * THIETM. chron.; 7,69 "quod ... per omnes domos ... ductus {baculus} in primo introitu a portitore suo sic #saluta.|retur: 'Vigila, Hennil, vigila'."


UUUU_BEDEUTUNG
   spectat ad ver:

   * CARM. Bur. ; 81,4,4 "floridum alaudula tempus #salut.|atur."


UUU_BEDEUTUNG
   ante discessum i. q. vale dicere -- Lebewohl sagen:

   * ARBEO Emm.; 10 "accepta a principe licentia, #saluta.|tis a {(A1, om. rell.)} cunctis."

   * EIGIL Sturm.; 13 p. 145,15 "#saluta.|tis ipsis {(fratribus)} ... inde profectus est {Bonifatius}."


UUU_BEDEUTUNG
   visitare -- besuchen:

   * CAND. FULD. Eigil. II; 7,8 "vade age ... fratresque #salut.|a."

   * THIETM. chron.; 6,25 "hanc {(civitatem)} ad bellum properantes #salut.|ant {(sc. Slavi)}."

   * EKKEH. IV. bened. I; 1,54 "horam nemo putat, qua iudex sęcla #salut.|at (gloss.: visitat)."

   * ODBERT. Frid.; 12 "imperator ... timuit ... diutius cum prohibita manere femina, sed versus Galliam remeans #saluta.|vit eam."


UU_BEDEUTUNG
   publ. et eccl.:

UUU_BEDEUTUNG
   in univ.:

   * CAPIT. reg. Franc.; 14 p. 35,14 "in ecclesiam non debet {(sc. excommunicatus)} intrare ... nec in oratione iungere nec #salut.|are."

   * CONC. Karol. A; 14 p. 86,29 "universa generalitas populi huius Romanae urbis ad #salut.|andum eum {(pontificem)} sicut omnium dominum properare debent."

   * HINCM. ord. pal.; 592 "princeps reliquae multitudini in suscipiendis muneribus, #salut.|andis proceribus ... occupatus erat."

   * RAHEW. gest.; 3,12 p. 180,26 "directis ... uterque principum nunciis ... sese per illos mutuo #salut.|arunt (#saluta.|erunt {var. l.; sc. Fridericus imperator et Lodewicus rex})."

   * CONST. imp. II; 427,88 "si quis spoliaverit aliquem sine diffidatione ..., quem bene #saluta.|vit ..., manu mutiletur."
   {persaepe.}


UUU_BEDEUTUNG
   in formula salutationis:

   * CAPIT. reg. Franc.; 111 p. 225,16sqq. "#salut.|at vos {(sc. Hadrianum papam)} dominus noster filius vester Carolus rex ...; #salut.|ant vos cuncti sacerdotes."

   * FORM. Senon. II; add. 1,11 "homo, satis te presumo #salut.|are et rogo, ut pro nobis dignetis orare."


UUU_BEDEUTUNG
   in officio salutationis:

   * WILLIB. Bonif. ; 5 p. 22,9 "collecta numerosa reliquiarum multitudine ... Langobardorum regem Liodobrandum pacificis #saluta.|tum muneribus conpellavit."

   * LAMB. HERSF. annal.; a.1074 p. 173,11 "cum paucis et pene privato habitu ad #salut.|andum eum {(regem)} venerant {principes}."


UUU_BEDEUTUNG
   in coronatione i. q. acclamare -- laut ausrufen:

   * WIDUK. gest.; 1,26 "nomen novi regis cum clamore valido #salut.|antes frequentabant {(sc. universa multitudo)}."

   * BERTH. chron. B; a. 1077 p. 288,21 "quem {(Ruodolfum)} totis regie honorificentie insignibus et laudamentis gloriosissime salutis #saluta.|tum et glorificatum ... venerati sunt {(sc. Saxi)}."

   * HELM. chron.; 30 p. 57,27 "a quo {(episcopo sedis Ravennae)} ... benedictione percepta a populo Romano #saluta.|tus est {(sc. Heinricus rex)} imperator et augustus."


UU_BEDEUTUNG
   liturg.:

   * ORD. Rom.; 5,42 "#salut.|at episcopus populum dicens: 'Dominus vobiscum' ((* {sim.} ; 17,92 "nec #salut.|at presbiter, id est non dicit: 'Dominus vobiscum'"))."

   * AMALAR. off.; 3,9,1 "quos #salut.|amus, eis faciem praesentamus."

UU_BEDEUTUNG
   rhet.: {v.} {=> salutatio/saluto_1} {=> salutatio/saluto_2} {=> salutatio/saluto_2}

UU_BEDEUTUNG
   astron. (de coniunctione stellarum):

   * EPIST. Hann.; 78^a p. 126,31 "haud temere affirmaverim {(sc. decanus)} Saturnum Mercuriumque in vestra {(sc. episcopi)} se ... #salut.|asse genitura."

ABSATZ

U_BEDEUTUNG
    translate:

UU_BEDEUTUNG
   in univ.:

    * CARM. de cal. Erf.; 18 "regia ... domus, tenuis dum sibilus aure sero #salut.|at eam, funditus ipsa ruit."


UU_BEDEUTUNG
    (ap)probare -- gutheißen:

    * CHART. Ioh. Halb.; 15 p. 18,17 (a. 1153) "horum {(praedecessorum)} egregia facta et exempla ... #salut.|amus {(sc. episcopus)}, veneramur et laudamus."

    * CARM. Bur.; 195,12,1 "tunc #salut.|ant {(sc. consortes)} peccarium et laudant tabernarium."


UU_BEDEUTUNG
   coniungere -- verbinden:

   * CARM. de nat. animal.; 497 "linea ducta duos punctos ... #salut.|at; sunt duo puncta {(sc. initium vitae et mors)} tibi."


AUTORIN
   Orth-Müller