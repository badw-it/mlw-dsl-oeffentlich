LEMMA 1. sacramentum

GRAMMATIK
    subst. II; -i n.

SCHREIBWEISE
    script.:
        sagr-: {=> sacramentum_1a; sacramentum_1b}
            * {adde} LEX Alam.; 27,1 {(var. l.).} {al.}

FORM
    form.:
        masc.:
            nom.: * EDICT. Roth.; 362 p. 84,6. {ibid. al.}
            acc.: {=> sacramentum_2a; sacramentum_2b; sacramentum_2c} {al.}

METRIK  
    metr.:
        -răm-: {=> sacramentum_3a; sacramentum_3b}


BEDEUTUNG ius iurandum, iuramentum (sollemne, religiosum), obligatio, confirmatio per iuramentum facta -- (feierlicher, heiliger) Eid, Schwur, eidliche Verpflichtung, Versicherung:

    UNTER_BEDEUTUNG publ., eccl., iur., canon., theol.:
    
        UU_BEDEUTUNG in univ.:
        
            UUU_BEDEUTUNG usu vario:
            
                UUUU_BEDEUTUNG strictius:

                    * LEX Ribv.; 61,21 "si ... homo regius homini ... #sacrament.|o fide (fidem {var. l.}) fecerit." 
                    * DIPL. Merov.; I 157 p. 393,1 "homenis ... #sag|.rament.|um {@ sacramentum_1a} ... in omnibus vise fuerunt adimplisse." 
                    * DIPL. Ludow. Germ.; 66 p. 92,10 "fecit {rex} ... testes #sacrament.|um dare super sanctam ... crucem."
                    * CONST. imp. I; 176,10 "extorta #sacrament.|a nullius esse momenti iubemus."
                    * CHART. Babenb.; 198 "Ulricus se data dextera vice #sacrament.|i predictam pensionem ... annuatim solvere obligavit."
                    {persaepe.}
                    
                        ANHÄNGER de ipsis verbis iuramenti:

                        * CAPIT. reg. Franc.; 260,13 "istum #sacrament.|um iurabunt Franci homines: {'eqs.'} ((* {sim.} 260,14. {ibid. al.}))."
                        * BERTH. chron.; B a. 1077 p. 260,10 "qui {(sc. episcopi pro rege iurantes)}, ut summam #sacrament.|i memoremus, in hunc modum iurabant, ut {eqs.} {(v. notam ed.)}."

                UUUU_BEDEUTUNG latius i. q. votum -- Gelübde:

                * HROTSV. Gall.; 2,3 "nullis ... suppliciis umquam potero {(sc. Constantia)} compelli, quin inviolatum custodiam #sacrament.|um propositi (({antea:}  "servandam Deo virginitatem devovi"))."

            UUU_BEDEUTUNG iuncturae notabiliores:

                UUUU_BEDEUTUNG "#sacrament.|um calumniae"; 'Gefährdeeid', 'Kalumnieneid', 'Voreid' ((* {de re v.} Schröder-Künßberg, Rechtsgesch.; p. 392)) ((* HRG| II.; p. 566sqq. * HRG| V.; p. 1039sqq.)):

                * CONST. imp. I; 318,5 (a. 1186) "si quis ... cuiquam imposuerit, quod incendiarium receperit, et de hoc ipsum in iure voluerit convenire, hoc nequaquam ei liceat nisi primo praestito calumniae #sacrament.|o (({sim.} 318,12))."
                * ACTA imp. Winkelm.; I 52 p. 48,43 "ut ... nullo modo cogantur {monachi} #sacrament.|um calumpnie seu diffinitivum ... subire."
                * CHART. Ital. Ficker; 243 p. 290,21 "ego ... iudex recepto #sacrament.|o calumpnie ab utraque parte ... Iohannem et Pierum ... ad restitucionem ... condempno." 
                {al.}
                
                UUUU_BEDEUTUNG "#sacrament.|um corporale":

                * CHART. Argent.; I 181 p. 145,22 (a. 1220) "corporali #sacrament.|o interposito me fideliter ac firmiter astrinxi {(sc. episcopus)}, quod {eqs.}"
                * CONST. Melf.; 1,38,2 "corporali prestito #sacrament.|o." 
                {al. v. et {=> sacramentum_4}.}

                    ANHÄNGER fort. de #sacrament.|o corporali: 

                    * CHART. Burgenl.; II 37 p. 25,37 (a. 1272) "ut obligacioni ... non possit ... contraire, manifestum in ecclesia ... super altare ... prestitit {banus} #sacrament.|um." 


                UUUU_BEDEUTUNG "#sacrament.|um iurisiurandi" sim.; c. gen. inhaerentiae: 

                * DIPL. Ludow. Germ.; 170 p. 239,39 "cum #sacrament.|o iuris iurandi ... decimę ... confirmatę sunt."
                * TRAD. Patav.; 93 p. 81,26 "prestito iusiurationis #sacrament.|o."
                * CHART. Eichsf.; 87 p. 50,43 "traditio ... confirmata est sub #sacrament.|o iurisiurandi."
                {al.} 
                
                
                UUUU_BEDEUTUNG "#sacrament.|um terribile": 

                * NOTKER. BALB. gest.; 1,22 p. 30,16 "populo ... instante ... et se terribilibus #sacrament.|is astringente, quod {eqs.}"
                * WIDUK. gest.; 3,60 "#sacrament.|um terribile dedit {Wichmannus} se contra imperatorem ... numquam aliquid inique ... facturum."
                * THIETM. chron.; 6,92 "rimatur {Bolizlavus}, ... quoscumque potuit, ab eius {(regis)} gratia ... amovere; ... ita cluebat militis incliti firma fides et de #sacrament.|is terribilibus adeo curavit."
                {al.}


            UUU_BEDEUTUNG c. explicatione theod. vet. "ahteid" ((* {de re v.} Dt. Rechtswb. I.; p. 387 s. v. 'Achteid')):
            * CONC. Karol. A; 16^A,5 "qui ... se ... erigere ... contra querentem presumpserit, #sacrament.|um, quod ahteid dicunt, iurat."


        UU_BEDEUTUNG de iuramento a vassis fidelitatem, obsequium, militiam promittentibus praestando; 'Treue-, Gefolgschafts-, Lehnseid':

            UUU_BEDEUTUNG usu vario:

                UUUU_BEDEUTUNG in univ.: 

                * ANNAL. regni Franc.; a. 757 "#sacrament.|a iuravit {Tassilo} multa et innumerabilia reliquias sanctorum manus inponens et fidelitatem promisit regi Pippino."
                * RATHER. epist.; 18 "ut, si illi {(Bernardo)} aliquid adiutorii prębuero, periurus de #sacrament.|o <pręstito> domino nostro {(sc. imperatori)} timeam esse." 
                * OTTO FRISING. gest.; 2,43 p. 151,32 "proceres Baioariae hominio et #sacrament.|o sibi {(Heinrico duci)} obligantur."
                {persaepe.} 

                UUUU_BEDEUTUNG de iuramento quasi pro #sacrament.|o religioso existimato per contam. c. significatione vocis quae est signum divinum ((* {cf.} W. Affeldt, DtArch. 25. 1969.; p. 337sqq.)):
                
                * LIBER de unit. eccl.; 1,14| p. 205,46 "quod ipse {(papa)} habuerit solvere a #sacrament.|o principes regni, immo solvere in eis #sacrament.|um, quod fidem vel pactum promiserant regi suo."; p. 206,3sq. "#sacrament.|um dicitur quoque iusiurandum, et #sacrament.|um dictum est ab eo, quod sit sacrum signum, ut, dum visibiliter iusiurandum agitur, invisibilis rei sacrum signum intelligatur (({cf.} 1,4| p. 189,13))."

            UUU_BEDEUTUNG iuncturae notabiliores: 

                UUUU_BEDEUTUNG "#sacrament.|um fidelitatis, fidei":

                * TRAD. Frising.; 186 p. 178,37 (a. 802) "per #sacrament.|um {@ sacramentum_2a} fidelitatis, quem ... imperatori ... iuraverunt, adtestati sunt {missi} eos {(sc. testes)}, ut {eqs.}" 
                * EINH. Karol.; 10 p. 13,22 "ob #sacrament.|a fidelitatis a Beneventanis exigenda atque suscipienda." 
                * CHART. Babenb.; 181 p. 247,7 "sub #sacramenmt.|o fidei, quo nobis astricti erant {ministeriales} {eqs.}"
                {al.}

                UUUU_BEDEUTUNG "#sacrament.|um militare, militiae"; fort. de iuramento a milite ordinem equestrem intranti praestando ('Rittereid'): {=> sacramentum_5}:
                
                * WIDUK. gest.; 3,76 "imperatoris filio ... fidem pollicentes et operam suam contra omnes adversarios #sacrament.|is militaribus confirmantes {(sc. principes)}." 
                * THIOFR. Liutw.; 6 p. 19,27 "#sacrament.|is militaribus eum absolvit {rex}." 
                * WIBALD. epist. I; 339 p. 712,5 "ut ... militię #sacrament.|is acceptis expeditionem Ytalicam ... promoveret {imperator}." 
                * RICHER. SENON. gest.; 5,1 p. 329,26 "quorum {(filiorum)} duo militaribus #sacrament.|is {@ sacramentum_5} impliciti erant, alii vero duo ad hunc honorem nondum pervenerant." 
                {al.}
                
                    ANHÄNGER meton. de militia; usu plur.: {=> sacramentum_6}: 

                    * OTTO FRISING. chron.; 4,10 p. 195,33 "removendos eos {(episcopos)} a ludis, a #sacrament.|o militiae arcendos ... decernit {Iulianus} ((* {cf.} $RUFIN. hist.; 10,33 "militiae cingulum non dari ... iubet"))."
                    * CHRON. Reinh.; a. 1190 p. 546,28 "qui {(lantgravius)} ... post gloriosos #sacrament.|orum {@ sacramentum_6} militarium ... triumphos ... ad liberacionem terre sancte ... profectus {eqs.}"


        UU_BEDEUTUNG de iuramento praelatorum vel magistratuum munus intrantium, munere fungentium; 'Amtseid': 
        
            UUU_BEDEUTUNG spectat ad episcopum:

            * BONIF. epist.; 16 p. 29,18 "hoc ... indiculum #sacrament.|i ego Bonifatius ... episcopus manu propria scripsi ... et prestiti #sacrament.|um, quod et conservare promitto ((* {ex} LIBER diurn.; 75 p. 80,7sqq.; 76 p. 81,17sqq.))."


            UUU_BEDEUTUNG spectat ad magistratus iudiciales:

            * CONST. Melf.; 1,62,1|^{tit.} "de #sacrament.|is a baiulis et magistris camerariis prestandis."; p. 228,6 "postulamus ..., ut omnes camerarii et baiuli, priusquam ... baiulationes nostras administrandas susceperint, tactis ... evangeliis corporalia subeant #sacrament.|a {@ sacramentum_4}, quod pure et sine fraude ... iustitiam ministrabunt." 


            UUU_BEDEUTUNG spectat ad potestatem civitatis: 

            * IOH. CODAGN. annal.; a. 1226 p. 74,1 "dominus Pruinus ... fuit electus in potestatem Placentie; ... in plena contione iuravit #sacrament.|um {!regiminis} civitatis Placentie ((* CONST. imp. II; 394,17 "potestas civitatis Pisane et capitaneus populi in #sacrament.|o sui r. annuatim iurent servare omnia pacta habita ..., et populus Pisanus in #sacrament.|um populi illud idem iuret"))."

    ABSATZ

    UNTER_BEDEUTUNG philos. de iuramento deorum antiquorum apud Stygem praestato; usu alleg.:
    
    * ALBERT. M. metaph.; 1,3,4| p. 34,55sqq. "hunc ... conventum virtutis deorum caelestium {(sc. Oceani et Thetis quasi parentum generationis)} in medio aquae quidam vocant deorum synodum, quidam autem 'deorum #sacrament.|um' ((* $ARIST.; p. 983^b,31 "τὸν ὅρκον τῶν θεῶν")); synodum ... propter virtutum suarum congregationem ..., #sacrament.|um autem propter suorum sacrorum numinum influxum in aquam {eqs.}
         (({sim.} p. 34,73. p. 34,80)).";
    3,2,10| p. 128,40 "honor, quo gloriatur odium dissolvendo collam amicitiae, 'dissolvit #sacrament.|um' a diis caelestibus perfectum omnis eius, quod est mutabile.";
    11,1,8| p. 469,24 "forma ... formabit materiem et reiterabit simile illi, quod fuit in priori saeculo; hoc est ... iuramentum deorum, quod 'amplo rediit #sacrament.|o' {(v. notam ed.)}."

    ABSATZ

    UNTER_BEDEUTUNG usu communi:

        UU_BEDEUTUNG in univ.:

        * VITA Gang. I; 7 ((MGMer. VII; p. 162,9)) (s. $IX.^{ex.}/$X.^{in.}) "mulierem {(i. coniugem adulteram)} talibus {(sc. inquisitivis)} studuit vocibus compellari: '...'; illa more muliebri coepit #sacrament.|is multimodis adseverare se a cunctis detrahi iniuste."

            ANHÄNGER locut. "#sacrament.|is obtestari" i. q. obsecrare, adiurare -- (bei allem was heilig ist) anflehen, 'beschwören': 
        
            * ERMENR. Sval.; praef. 1 p. 154,3 "cur ... metenus perveneras {(sc. Gundrammus)} petens ac #sacrament.|is obtestans {eqs.}"

        UU_BEDEUTUNG de iuramento Hippocratis q. d.:
        
        * ARS med.; 3| p. 421,1 "coniurat {(sc. discipulus medicinae)} Hippocratis medicinale #sacrament.|um secundum eius praeceptum, ut {eqs.} (({cf.} p. 420,1 "Hippocraticum iuramentum"))."

ABSATZ

BEDEUTUNG mysterium, secretum, signum (sanctum, divinum, ad fidem pertinens), veritas divina (occulta, revelata), significatio mystica -- (heiliges, göttliches, Glaubens-) 'Mysterium', Geheimnis, Zeichen, (verborgene, offenbarte) göttliche Wahrheit, (mystische) Bedeutung; fere usu eccl. et theol.; usu communi: {=> sacramentum_7} ((* {de re v.} LThK. ^{3}VIII. 1999.; p. 1437sqq.)) ((* {et} A. M. Landgraf, Dogmengeschichte der Frühscholastik. III/1. 1954.; p. 11sqq. et passim)):

    UNTER_BEDEUTUNG in univ.: 

        UU_BEDEUTUNG strictius spectat ad arcana religionis: 

            UUU_BEDEUTUNG veteris testamenti: 

            * CONC. Karol. A;  19^D p. 133,34 "sicut scriptum est ... in prophetiae #sacrament.|o: {'eqs.'}  {(Vulg. gen. 1,26)}."
            * HRABAN. epist.; 12 p. 399,10 "ad Deuteronomium Moysi considerandum perveni, quatinus inde aliqua ... #sacrament.|a spiritaliter rimarer." 
            * ALBERT. M. Is.; 43,18 "antiqua dicit antiquata veteris testamenti #sacrament.|a et figuras, quae ... veritate apparente facta sunt mortifera."

            UUU_BEDEUTUNG novi testamenti, fidei christianae:

                UUUU_BEDEUTUNG usu vario: 

                * IONAS BOB. Columb.; 2,17 p. 269,3 "evangelii ... vitalia #sacrament.|a ... narrare." 
                * HRABAN. carm. euang.; 37 "Lucas depromit sancti #sacrament.|a Iohannis." 
                * CAES. HEIST. comm.; 3 "illam sequenciam, videlicet 'Ave, preclara maris stella', que propter varia, que in se continet, #sacrament.|a videtur obscurior, ... exponere studui." 
                * ALEX. MIN. apoc.; 2 p. 22,4 "#sacrament.|um dicitur, ubi aliud videtur et aliud intelligitur." 
                {saepius.}

                    ANHÄNGER iunctura "fidei #sacrament.|um": 

                    * LEG. Wisig.; 12,2,2 p. 413,4 "nullus contra omnem sanctum vere fidei #sacrament.|um {@ sacramentum_2b} aut cogitationes ruminet cordis aut verba ... proferat." 
                    * HROTSV. Mar.; 563 "ad fidei magnum quia perveniet {populus Iudaeorum} #sacrament.|um."
                    * DIPL. Heinr. III.; 391 p. 542,22 "catholicę fidei."
                    {al.}

                UUUU_BEDEUTUNG spectat ad mysteria Christi #sacrament.|a {@ sacramentum_11} ecclesiastica praefigurantia; cf. {=> sacramentum_8}:
                
                * WETT. Gall.; 25 p. 270,24 "euangelicis ... miraculis passionisque ac resurrectionis #sacrament.|is recitatis." 
                * WALTH. SPIR. Christoph. I; 5 p. 5 69,7 "Dominicę incarnationis et lavacri #sacrament.|um suęque misterium passionis ... sibi ... aperuit {angelus}." 
                * RUP. TUIT. trin.; 34,755 "huius {(sc. evangelicae)} doctrinae fluvius ... nobis distinguitur ... quattuor #sacrament.|is Christi principalibus, id est incarnatione, passione, resurrectione et ascensione eius." 
                {saepius.}

            UUU_BEDEUTUNG de imagine euangelistae allegorica:
            
            * CARM. imag.; 19,3 "Lucam votorum fingunt #sacrament.|a iuvencum."

            UUU_BEDEUTUNG de visionibus, miraculis sim.:

            * VITA Goar.; 1 "hic erat ... praescius futurorum visionum interpres; qui continuatis ieiuniis et orationibus instans praenoscere meruit #sacrament.|a."
            * TRANSL. sang. Dom. in Aug.; 21 "illa {(amatrix Dei)} demum miraculi #sacrament.|um cognoscens {eqs.}" 
            * SIGEW. Mein.; 19 (1,13) "invenit ... quoddam divinae revelationis memorabile #sacrament.|um: vidit enim prestanti corpore cervum {eqs.}"
            * VISIO Godesc. B; 25,11 "cui {(simplici)} talia {(sc. visionis)} #sacrament.|a revelata sunt."
            {al.}


            UUU_BEDEUTUNG de ecclesia a Deo constituta, instituta: 
            
            *EPIST. Col.; 6 p. 249,23 "archiepiscopale pallium ... ei {(archiepiscopo)} largiri ... imploramus {(sc. rex)}, ut vestra {(sc. papae)} auctoritate infulatus nobiscum exornet sanctae et individuae trinitatis #sacrament.|um."
            * HUGO FLOR. tract.; 2,1 ((MG Lib. Lit. II; p. 483,10)) "Deus duas ... potestates in aecclesia sua ... constituit, regiam videlicet et sacerdotalem, non absque magno ac saluberrimo #sacrament.|o."

            UUU_BEDEUTUNG de condicione vitae a Deo data: 
            
            * ALBERT. M. serm. I; 1 p. 18,5sqq. "#sacrament.|a dicit {(sc. Vulg. sap. 2,22)} passive, id est ea, quae Deus sacravit in seipso; hoc #sacrament.|um est paupertas; pauper enim {eqs.}" 

            UUU_BEDEUTUNG de nomine Dei vel numeris mystice explanatis: 
            
            * AMALAR. ord. antiph.; 33,4 "numeri #sacrament.|um quadraginta dierum et noctium (({sim.} 33,6))."
            * DHUODA lib. man.; 1,5,5 "ad nomen, qui dicitur Deus, duae continentur sillabae et quatuor litterae; cum has inveneris et legeris, quid aliud dicis nisi: 'Deus' hoc magnum admirabilem (et admirabile {var. l.}) continet #sacrament.|um {@ sacramentum_2c}."
            * OTLOH. Dion.; 11 p. 833,7 "centenarius {(numerus)}, qui, ut cetera eius #sacrament.|a pretermittam, ... virginitatis perfectionem designat."
            * ANON. algor. Salem.; p. 5,11 "quae {(unitas)} quidem potest dupplari, sed non dimidiari, in quo magnum latet #sacrament.|um." {ibid. al.}


        UU_BEDEUTUNG latius spectat ad secreta hominum; usu communi {@ sacramentum_7}: 

        * INVENT. Phil. Cell.; 2 "quibus {(apparitoribus)} praesentatis #sacrament.|um, quod archana mente cogitaverat {abbas}, occulte indicavit: {'eqs.'}"
        * EKKEH. IV. cas.; 131 p. 254,4 "cum ... ab eo {(Ekkehardo)}, si sui aput reges mentio aliqua fuerit, quererent {(sc. sui)}, 'magna', ait, 'si mihi dicere liceat; #sacrament.|um enim regis celare honorificum est'."

    ABSATZ

    UNTER_BEDEUTUNG signum rei sacrae, gratiae (visibile) -- (sichtbares) Heils-, Gnadenzeichen, 'Sakrament':

        UU_BEDEUTUNG praevalente notione caerimoniae, ritus ecclesiastici: 

            UUU_BEDEUTUNG in univ.: 

                UUUU_BEDEUTUNG usu vario:

                * HUGEB. Wynneb.; 5 p. 110,4 "canonice institutionis #sacrament.|a renovans multos ... iniuste copulationis coniugio nuptos ... a pravitate prohibuit."                
                * HRABAN. epist.; 55 p. 509,2 "ut aliquas questiones de #sacrament.|is divinis ... tibi {(sc. Thiotmaro)} exponendo absolverem {eqs.}"                
                * BERTH. chron. B; a. 1067 p. 204,12 "qui {(monachi)} ... #sacrament.|a, que ab ... symoniacis et uxoratis presbiteris conficerentur, nulla ... esse ... protestati sunt."                
                * CHART. Stir.; II 127 p. 193,33 (epist. papae) "pro ... quolibet ecclesiastico #sacrament.|o nullus ... quicquam audeat extorquere."                
                * ALBERT. M. sacram.; 7^{capit.} "de institutione #sacrament.|orum {eqs.}"                
                {persaepe.}

                    ANHÄNGER in iunctura "liber {sim.} #sacrament.|orum" spectat ad sacramentarium q. d.; ellipt.: {=> sacramentum_9a; sacramentum_9b}: 
                    
                    * VITA Hucberti; 18 ((MGMer. VI; p. 494,9)) "illi {(homines)} ... deposuerunt evangelium super altaris basilicae et #sacrament.|orum volumina {eqs.}"
                    * ORD. Rom.; 16,40 "benedicentur cerei a diacone ordine, quo in #sacrament.|orum {@ sacramentum_9a} habetur (({sim.} 16,41. 24,2. {ibid. saepe}))." 
                    * CATAL. biblioth. A; I 49 p. 248,18 "libri #sacrament.|orum LVIII." 
                    * LEO MARS. chron.; 3,63 p. 445,10sq. "#sacrament.|orum {@ sacramentum_9b} cum martirologio, #sacrament.|orum aliud."                    
                    {saepius. v. et {=> sacramentarius/sacramentum_10a; sacramentorium/sacramentum_10b}.}

                UUUU_BEDEUTUNG spectat ad distinctionem inter ipsum #sacrament.|um et rem, virtutem {sim.} #sacrament.|i:
                
                * AMALAR. off.; prooem. 7 "#sacrament.|a debent habere similitudinem aliquam earum rerum, quarum #sacrament.|a sunt."                  
                * CONC. Karol. A; 57,7 p. 781,10 "quid pertinet ad virtutem #sacrament.|i, quod pertinet ad visibile #sacrament.|um {eqs.}"                  
                * TRACT. de schism.; ((MG Lib. Lit. III; p. 127,23)) "sciendum ... est aliud esse #sacrament.|um, aliud rem #sacrament.|i atque aliud effectum #sacrament.|i {eqs.}"                  
                * CAES. HEIST. mirac. I; 9,1 p. 166,7sqq. "tria sunt in #sacrament.|o hoc {(sc. eucharistiae)} consideranda, unum quod tantum est #sacrament.|um, alterum, quod est #sacrament.|um et res, tertium, quod est res et non #sacrament.|um."                
                {al.}
        
                UUUU_BEDEUTUNG spectat ad #sacrament.|a in vetere testamento praefigurata: 
                
                * HRABAN. epist.; 9 p. 395,2 "in quo {(libro Exodi)} pene omnia #sacrament.|a, quibus presens ecclesia instituitur, ... figuraliter exprimuntur."; 33 p. 466,24 "in quo {(cantico Isaiae)} adventus salvatoris et baptismi #sacrament.|um praedicatur."                 
                * AMALAR. off.; 1,38,5 "'vinea facta est' {(i. lectio)}, scilicet #sacrament.|um Christi et ecclesiae, quod  pertinet ad allegoriam, praecedit."                
                {al.} 
                
                
                UUUU_BEDEUTUNG spectat ad #sacrament.|a {@ sacramentum_8} (canonica) a  Christo instituta; cf. {=> sacramentum_11}: 
                            
                * MANEG. c. Wolfh.; 18 capit. p. 43 "de duobus #sacrament.|is regenerationis et refectionis ante passionem in cęna institutis (({sim.} 18 p. 84,13))."                 
                * BERTH. RATISB. serm. exc.; p. 6,23 "dicunt {(sc. haeretici)}, quod septem #sacrament.|a, que Dominus hic reliquit nobis, non valeant {eqs.}"                 
                * STATUT. ord. Teut.; p. 36,9 "quando {(sc. in cena Domini)} primo hoc {(sc. eucharistiae)} #sacrament.|um Christus instituens corpus et sanguinem suum porrexit discipulis."
            
            UUU_BEDEUTUNG de certis #sacrament.|is canonicis:

                UUUU_BEDEUTUNG de baptismo:

                * LIBER diurn.; 6 p. 6,20 "{!baptismi} #sacrament.|um non nisi in paschali festivitate et pentecosten noverit {episcopus} esse prebendum ((* CONST. Constant.; 213. {saepius}))."
                * LEX Baiuv.; 8,21 "sine #sacrament.|o (#sag|.ramento {@ sacramentum_1b} {var. l.}) {!regenerationis} ... tradita est {anima} ad inferos ((* WILLIB. Bonif.; 6 p. 27,15. * NICOL. LEOD. Lamb.; 38 ((MGMer. VI; p. 414,20)) "ad suscipiendum divine r. #sacrament.|um." {al.}))."
                * HERM. IUD. conv.; 19 p. 118,21 "cum per divine institutionis salutare #sacrament.|um ab eius me rapi tirannide conspexit {hostis antiquus} (({sim.} 20 p. 121,6 "'domum' {(i. animam)} licet lavacro salutari 'mundatam' Christique #sacrament.|is 'ornatam' ... 'inveniens'"))."            
                {persaepe.}

                UUUU_BEDEUTUNG de eucharistia: 
                
                * RADBERT. corp. Dom.; prol. ad Karol. 12 "decrevi ... maiestati vestrae munus offerre, ... libellum ... de #sacrament.|is sacrae communionis."                 
                * OTTO FRISING. gest.; 2,28 p. 133,26 "de #sacrament.|o altaris, baptismo parvulorum non sane dicitur {(sc. Arnaldus)} sensisse."
                
                * ALBERT. M. euch.; 1,6,1 p. 213^a,1sq. "continet {(sc. #sacrament.|um eucharistiae)} ... eum, qui vas est omnis gratiae plenissimum: propter quod etiam a pluribus dicitur #sacrament.|um #sacrament.|orum."                 
                {persaepe.}

                    ANHÄNGER latius fere i. q. missarum sollemnia -- etwa: Messe; usu plur.:
            
                    * AMALAR. off.; prooem. 11 "iuvat multos ... seorsum #sacrament.|a celebrare de Dominicis diebus et seorsum de festivitatibus sanctorum."            
                    * WALDO Anscar.; praef. 105 "excessit cultura deorum polluta cruentis officiis pecudum ..., omnis in ecclesiae populus #sacrament.|a {@ sacramentum_3a} cucurrit."
                    * VITA Meinw.; 185 "ecclesiastica ornamenta et sacerdotalia indumenta, ... per quę ... divina celebrata erant #sacrament.|a, ... ecclesię proprietati ... confirmavit."            
                    * CONSUET. Vird.; 19 p. 402,6 "quod ordo ecclesiasticus de his diebus {(sc. triduo sacro)} in #sacrament.|is retinet, quid nos scribere debemus?"            
                    {al.}
    

                UUUU_BEDEUTUNG de confirmatione: 
                
                * HRABAN. epist.; 40| p. 478,25 "qualis ... fieri {(sc. debeat)} accessus post perceptam iam fidem ad accipiendam sacri baptismatis regenerationem et #sacrament.|orum divinorum sanctificationem (({sim.} p. 478,30 "quomodo oporteat ... iam convocatos catechumenos facere et baptismate Christi abluere #sacrament.|is_que divinis confirmare et in fidei veritate corroborare"))."
                

                UUUU_BEDEUTUNG de ordinatione clericorum: 
                
                * RUOTG. Brun.; 26 "attulit {legatus} pallium laudis pro spiritu meroris; spiritus namque Dei cordato homini et #sacrament.|i huius magnificę virtuti ... intento divinitus inspiravit, ut {eqs.}" 
                * ADALB. SAMAR. praec. dict.; p. 61,15 "pastoribus est obtemperandum, ... a quibus sancte ecclesie ac clericis #sacrament.|um {@ sacramentum_14} consecrationis ... conceditur."

                UUUU_BEDEUTUNG de unctione infirmorum vel morientium: 
                
                * HELM. chron.; 1,95 p. 187,21 "cum fratres consummationis finem inminere viderent, impenderunt ei {(Geroldo episcopo)} sacrae unctionis officium, sicque #sacrament.|is {!salutaribus} communitus ... carnis sarcinam deposuit ((* CHART. Bund.; 420 [epist. papae] "ut, si aliqui parrochianorum ... in extremis agentes ... ei {[presbytero]} voluerint confiteri, ... s. reficiens #sacrament.|is ecclesiastice tradat, si decesserint, sepulture"))."                 
                {cf. {=> sacramentum_12}.}

                UUUU_BEDEUTUNG de matrimonio:

                * EPIST. Hild.; 31 "quod cum nepte mea {(sc. comitis)} coniugii #sacrament.|um acciperet {rex}."
                {cf. {=> sacramentum_12}.}

                UUUU_BEDEUTUNG de paenitentia:
                
                * ALBERT. M. eccl. hier.; 1| p. 2,49 "in hac scientia non determinatur de #sacrament.|o {@ sacramentum_12} (subiecto {var. l.}) in communi et de omnibus partibus eius, quia nihil dicitur de matrimonio et extrema unctione et paenitentia."; p. 2,65 "cum in #sacrament.|o paenitentiae maxime exerceantur hierarchicae actiones, de ritu paenitentiae aliquid dicere deberet."; 2| p. 26,45sq. "quaedam #sacrament.|a non sunt tamen #sacrament.|a, sed officia personae ..., sicut paenitentia."


            UUU_BEDEUTUNG de caerimoniis, ritibus ecclesiasticis extra #sacrament.|a canonica positis, consecrationibus, sacramentalibus q. d. (de re v. {=> sacramentalis/sacramentum_16}):
            
            * IONAS BOB. Columb.; 2,7 p. 243,12 "religionis vestem (veste {var. l.}) per pontificem urbis ... induit {(sc. Eustahsius puellam)} sacravitque salutaribus #sacrament.|is."            
            * OTTO FRISING. gest.; 2,3 p. 104,28 "dum finito unctionis #sacrament.|o diadema sibi {(regi)} imponeretur {eqs.}"             
            * CHART. Tirol. I; 767^B p. 202,23 "liceat vobis {(sc. fratribus)} ... adire antistitem et ab eo consecracionum #sacrament.|a percipere."             
            * CHART. civ. Erf.; 90 "cum ... inter cetera #sacrament.|a dedicacionis ecclesie #sacrament.|um sive festum sollempnius reputetur {eqs.}"             
            {v. et {=> sacramentum_14}.}

            UUU_BEDEUTUNG de caerimoniis Iudaeorum: {v. {=> sacramentalis/sacramentum_15}.}


        UU_BEDEUTUNG praevalente notione rei corporeae:
        
            UUU_BEDEUTUNG de corpore et sanguine Christi:
            
                UUUU_BEDEUTUNG spectat ad panem et vinum in eucharistia consecratum: 
                
                * ARBEO Corb.; 24 p. 216,23 "tam corporis alimoniam quam animarum a viro Dei propinabantur #sacrament.|a."                
                * THEGAN. Ludow.; 7 p. 186,10 "ut ei {(imperatori)} #sacrament.|a Dominici corporis et sanguinis tribueret {pontifex}."                
                * GERH. AUGUST.; Udalr. 1,4 l. 150 "missa ... decantata cunctisque #sacrament.|a Christi accipientibus {eqs.}"                 
                * THIETM. chron.; 3,19 "corpus animamque caelesti #sacrament.|o muniunt {(sc. duces exercitus)}." 
                * IDUNG. PRUF. dial.; 1,503 "sacratissima Christi #sacrament.|a preciosissimo metallo honoramus." 
                {persaepe.}
                
                    ANHÄNGER alleg.: 
                    * RADBERT. corp. Dom.; 2,38 "spiritalia #sacrament.|a palato mentis et gustu fidei digne percipere."

                UUUU_BEDEUTUNG spectat ad sanguinem et aquam e Christi vulnere emanantes:
                
                * AMALAR. off.; 3,26,3 "exivit ab eius latere sanguis et aqua: sine his #sacrament.|is nemo intrat ad vitam aeternam."; 3,26,4 "ut #sacrament.|o sanguinis et aquae inficeretur gentilitas {eqs.}" 
                
                * HYMN. Hraban.; 14 (13),8,2 "hoc {(sc. crucis)} in ligno vitis vera pinguem botrum protulit, ex qua nobis manaverunt #sacrament.|a maxima: sanguis roseus et aquae, quae libantur mystice." 
                {al.}
            
                
                UUUU_BEDEUTUNG spectat ad miraculum sanguinis: 
                
                * CHART. Verd.; 177 p. 207,15 (a. 1191) "apparuit in bicario {(sc. aquam ablutionis continenti)} caruncula cruenta ...; aqua ... facta est sanguinolenta, immo sanguis; servatum est vas et contentum intra vas #sacrament.|um."

            UUU_BEDEUTUNG de oleo consecrato: 

            * AMALAR. off.; 1,12,26 "quando a populis offertur, simplex liquor est {oleum}, per benedictionem sacerdotum transfertur in #sacrament.|um ((* {sim.} PONTIF. Rom.-Germ.; 99,301 p. 83,1))."
            

            UUU_BEDEUTUNG de reliquiis sanctorum: 

            * DIPL. Loth. I.; 110 p. 258,36 "quod nobis aliquod evidens #sacrament.|um mittere dignemini {(sc. papa)}."            
            * LAMB. TUIT. carm.; 1,1 ((MGScript. rer. Germ. LXXIII; p. 267)) "circa reliquias: hęc clausura tuę formetur digna coronę, Laurenti ...; huc #sacrament.|a {@ sacramentum_3b} tuę carnis portantur adustę."             
            * NOTAE Bron.; a. 1154 ((MGScript. XXIV; p. 27,21)) "comes Namurcensis ... cartas et privilegia, #sacrament.|um et possessiones ville monasterii Broniensis ... roboravit."
            
            UUU_BEDEUTUNG de rebus profanis:
            
                UUUU_BEDEUTUNG per compar. de offis carnis: 
                
                * RUODLIEB; VII 10 "in plures offas quam {(suram)} concidendo minutas pro #sacrament.|is pueros partitur {hospes} in omnes."
                
                UUUU_BEDEUTUNG per blasphemiam de aqua balnei cuiusdam haeretici: 
                
                * COD. Udalr.; 309 p. 523,10 (c. 1115) "ut quidam in eo {(Tanchelmo haeretico)} divinitatem venerarentur in tantum, ut balnei sui aquam potandam stultissimo populo pro benedictione divideret velut sacratius et efficatius #sacrament.|um profuturum corporis et animae."

    ABSATZ

    UNTER_BEDEUTUNG gratia collata manifesta -- offensichtlicher Gnadenerweis; c. gen. explicativo:
    
    * VITA Mariae rhythm.; 3963 "ad Iesum ibat {(sc. mulier haemorroissa)} et eius vestimentum tetigit et sanitatis recepit #sacrament.|um."


AUTORIN Niederer

UNTERDRÜCKE WARNUNG 516