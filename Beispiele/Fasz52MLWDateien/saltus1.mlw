LEMMA  1. saltus

GRAMMATIK  subst. IV; -us m.

SCHREIBWEISE

    script.:

        ps-: {=> saltus001}{=> saltus002}
        {?}faltus: * FORM. Senon. II; add. 5,4 {(var. l.)}

STRUKTUR

    form.:
    
        abl. sg.:
        
            salu: * GODESC. SAXO gramm.; 2 p. 495,10 {(var. l.)}
            -o: {=> saltus003}

    struct.:

        c. gen. obi.: {=> saltus004}

BEDEUTUNG  actio saliendi -- das Springen, Hüpfen, Sprung ((de anima: {=> saltus005})):

    U_BEDEUTUNG  proprie:

        UU_BEDEUTUNG  in univ.:

            UUU_BEDEUTUNG  gener.:

                UUUU_BEDEUTUNG  de anim.:

                    * WALTHARIUS; 1280 "'aliquid memorabile faxo', dixit {Hagano} et a tergo #salt.|u se iecit equino."

                    * CARM. var. III; B 11,7 "qui tibi donato quicquam non gaudet honore, hunc medicina uri non solvat febribus ante, quam leporis ternos studeat comprendere #salt.|us."

                    * HILDEG. phys.; 7,5 "unicornus ... in eundo quasi #salt.|us habet."

                    * ALBERT. M. animal.; 2,2,2 p. 295^b,13 "constat ..., quod illorum {(bipedium)} motus non est nisi tribus modis, quorum unus est ambulatio et alter est #salt.|us et tertius cursus."; animal.; 20,29 p. 1287,28 "agonia vel tractus spiritus a vulgo ... vocatur #salt.|us {@ saltus005} animae."

                    {persaepe.}

                        ANHÄNGER  proverb.:

                            * EGBERT. fec. rat.; 1,487 "#salt.|um movisti, sed aves collegerat alter."

                UUUU_BEDEUTUNG  de rebus:

                    UUUUU_BEDEUTUNG  in univ.:

                        * TRANSL. Land.; 1,7 ((MGScript.; XV p. 609,41;; a. 982/90)) "accidit, ut ... feretrum, in quo sancta Landrada continebatur, #salt.|um ... daret."

                        * EKKEH. URAUG. Hieros.; 10,2 "stellam aliam ... in oriente locum suum longo interstitio #salti.|bus mutantem conspeximus."

                    UUUUU_BEDEUTUNG  spectat ad navim volantem ('Flug'):

                        * ADAM gest.; 4,11 "introitus ... tam strictus invenitur {(sc. in oceano)}, ut facili #salt.|u per noctem carbasa traiciantur."

                    UUUUU_BEDEUTUNG  spectat ad praecipitem lapsum aquae ('Wasserfall'):

                        * FUND. Aquic.; ((MGScript. XIV; p. 584,6)) ((s. XII.^2)) "responsum accepit {(sc. abbas)}, ut, si ... occurrenti aquae meatus efficeret, ita ut usque ad introitum insulae veniret, ibi esse possibile invenire #salt.|um, ubi efficeretur molendinum."

                        * DIPL. Frid. II.; 75 p. 150,25 "concedimus ... #salt.|um ... molendini, qui {eqs.}"

                UUUU_BEDEUTUNG  meton. in nomine loci:

                    * ROB. TOR. chron.; a. 1123 p. 487,29 "que {(sc. fenestra turris)} vocatur Conani #salt.|us, quia {eqs.}"

            UUU_BEDEUTUNG  anat. et medic. i. q. palpitatio -- das Zucken:

                * AESCULAPIUS; 25 p. 41,37 "tremor crebro, hoc est frequenti vibratione fit, {!membrorum} #salt.|u ((* ANTIDOT. Sangall.; p. 82,19 * ANTIDOT. Berolin.; 37 p. 74,28. {al.} ))."

                * GLOSS. med.; p. 51,5 "palmon: Greci vocant #salt.|um (#salt.|u {var. l.}) ventris ex parte palpitantis."
                
                * MAURUS progn.; 10 p. 29^b,19 "si ... in aliqua earum {(materiarum)} pulsus fuerit, id est #salt.|us {eqs.}"

                * BRUNUS LONG. chirurg.; 1,12 p. 110^E "significatio ..., quod sanguis manat ab arteriis, est, quando sanguis manat cum #salt.|u."

                * ALBERT. M. animal. quaest.; 13,12 p. 244,65 "pulsus ... potest esse in quolibet animali et dicitur #salt.|us membrorum vel singultatio."

                {persaepe.}
            
        UU_BEDEUTUNG  spectat ad coitum ('das Bespringen'):

            * ALBERT. M. nat. bon.; 349 p. 123,56 "quorundam ... animalium sic membra disposuit {natura}, quod #salti.|bus coeunt ut equi et canes."; animal.; 2,36 p. 238,10 "diversatur ... organum coeundi in hominibus ab organo, quo fit #salt.|us in aliis {(sc. animalibus)}."; 2,39 "quaedam {(sc. animalia)} ... habent virgas per follem ad ventrem applicatas, quia aliter non contingeret eis #salt.|us."

            {al.}

        UU_BEDEUTUNG  impetus, assultus -- Angriff, Attacke:

            * RHYTHM. ; 25,9,3 "Ionam missum in fluctibus alvo coeti conditum #salt.|o {@ saltus003} piscis inmani quis ad litus protulit?"

            * DIPL. Heinr. II.; 303 "neque aliqua superinposita ... predictis hominibus fiat scilicet de fodro, de adprehensione hominum vel #salt.|u {@ saltus004} domorum."

            * CONST. imp. I; 68,2 "incendium vel #salt.|um ad castella et ad domos non faciam {(sc. depraedator)}."

        UU_BEDEUTUNG  profluentia -- das Entspringen (meton. de fonte):

            * CHART. Eichsf.; 290 (c. 1240) "obtulimus {(sc. comes)} prefate ecclesie proprietatem aque, que vocatur #salt.|us Rume {('Rhumesprung')}."

    ABSATZ
    
    U_BEDEUTUNG  translate:

        UU_BEDEUTUNG  in univ.:

            UUU_BEDEUTUNG  gener.:

                * AMALAR. off.; 3,26,20 "conscientiam peccatorum nostrorum sacerdoti ostendimus; ... quo facto non ilico #salt.|um facimus in locum magistrorum, sed {eqs.}"

                * BOVO CORB. Boeth.; 208 [11] "nec solum sibi vicina et coherentia comparantur {(sc. elementa)}, sed eadem alternis #salti.|bus custoditur aequalitas."

                * CARM. Ratisb.; 34,13 "verba ... #salt.|um demonstrant pectoris altum."

                * CONR. HIRS. mund. cont.; 33 "si gressu prepeditiori annos eternitatis minus valeres {(sc. matricularius)} mentis #salt.|u pregustare."

                * CAES. HEIST. hom. exc.; 60 "rogatus est {daemon}, ut Dominicam oracionem diceret; obedivit, dixit, sed valde confuse, multos ibi #salt.|us faciens."

            UUU_BEDEUTUNG  medic. i. q. motus inaequalis, incertus -- Unregelmäßigkeit:

                * ALFAN. premn. phys.; 16,13 p. 95 "cordis ... motus secundum pulsus actus est, <<secundum #salt.|us>> (($NEMES.; {(PG 40,673^C)} κατὰ τοὺς παλμούς)) vero passio." {ibid. iterum.}

            UUU_BEDEUTUNG  mus. spectat ad intervalla ((* {de re v.} LexMusLat. vol. II. s. v.)):

                * GUIDO ARET. microl.; 14 "ut unus autenti deuteri fractis #salti.|bus delectetur."

                * IOH. AFFLIG. mus.; 16,5 "alii mimicos septimi {(sc. modi)} #salt.|us libenter audiunt."

                * GUIDO AUGENS. mus.; 362 "dispositionem fecit simplicium convenientium ordinatio; ... compositionem tarditas morarum, #salt.|uum levitas."; 567 "autentum ... sepe eleva #salt.|u vel hilari motu ad quintam."; 729 "per ... levitatem #salt.|uum, qua ad quintam ascendit {autentus}, plagales excludit."

                * ANON. ton. Schneider; p. 109 "tertius {(sc. tonus)} dicitur severus et incitabilis in cursu suo fractos habens #salt.|us."

                * HIER. MOR. mus.; 14,92 "quatuor sunt ... irregulares #salt.|us infra diapason, videlicet {eqs.}"

                {al.}

        UU_BEDEUTUNG  spectat ad seriem continuam:

            UUU_BEDEUTUNG  de circulo decemnovennali annorum q. d., in iunctura "-us lunae" {sim.} ('Mondsprung'):

                * BEDA temp. rat.; 2,18 "firmatum est, ut mensis XXX diebus putaretur, cum hoc nec solis nec lunae cursui conveniat; siquidem lunam duodecim horis minus salva ratione #salt.|us, solem vero decem horis et dimidia plus habere, qui solertius exquisiere, testantur."; 20,35 "ratio #salt.|us {!lunaris} ((* ALCUIN. epist.; 126 p. 185,15 * MEGINFR. (?) carm.; p. 648^C))."

                * COMPUT. Borst; 8,1 p. 680,3 "quintus est {annus}, dum luna per novemdecim annos, etiam #salt.|u transacto, ad easdem recurrit aetates."

                * ALCUIN. epist.; 126 p. 187,14 "#salt.|um (#p|.saltum {@ saltus001} {var. l.}) conputare."; 170 p. 280,9 "post impletam #salt.|us (#p|.saltus {@ saltus002} {var. l.}) supputationem, quae {eqs.}"
            
                {persaepe.}

            UUU_BEDEUTUNG  transgressio, omissio -- das Überspringen, Auslassen:

                UUUU_BEDEUTUNG  gener.:

                    * CONSUET. Vird.; 27 p. 413,9 "aliqua ex his, quae a senioribus nostris antiquitus novimus ... servata esse, quodam #salt.|u omisimus {eqs.}"

                    * CHRON. rhythm. Col.; 5,34 "antea discreto conscendebatur in altum, nunc penitus spreto faciebant ordine #salt.|um."

                    * CHART. Brem.; 314 p. 354,14 "pannicide in quolibet dimidio anno debent mutare tabernas et in ordine tabernas proximas sine #salt.|u intrare."

                UUUU_BEDEUTUNG  publ. et iur.:

                    * SUMMA dict. Saxon.; 7,9 | p. 237,8 "omnis appellacio gradatim fieri debet, ... nisi forte in casibus #salt.|us fiat (({sim.} p. 237,21 "appellacio ... facta per #salt.|um ad papam"))."


        
                UUUU_BEDEUTUNG  canon. de ordinationibus clericorum, quarum una alteram rite sequitur:

                    * FLOD. hist.; 3,11 p. 214,10 "iudicatum est ..., qui #salt.|u sine gradu diaconi ad sacerdotium prosilierit, in degradationem debitam resilire deberet."

                    * ALBERT. M. sacram.; 214 | p. 140,11 "videtur ..., quod unus sit disponens ad alterum, quia hi, qui faciunt #salt.|um, non habent exsecutionem officii, nisi suscipiant priorem."; p. 140,17 "unus ordo {(sc. ordo)} ab altero non accipit perfectionem, quod manifestum fit in his, qui faciunt #salt.|um, qui habent ordinem, quem suscipiunt, perfecte, etiam antequam priorem suscipiant."; 216 | p. 141,52

AUTOR  Fiedler