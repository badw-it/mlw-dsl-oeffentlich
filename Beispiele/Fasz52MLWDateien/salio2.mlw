LEMMA  2. salio (sallio)
GRAMMATIK  
    verbum III; -ivi, -itum, -ire

VEL  *salo (sallo)
GRAMMATIK
    verbum I; -atum, -are

STRUKTUR
    form. coniug. II.:    {=> salioB2}

METRIK
    metr.:
        sāl-: {=> salioB1}

BEDEUTUNG  sale conspergere, conficere, condere -- mit Salz bestreuen, salzen, in Salz einlegen:

    UNTER_BEDEUTUNG strictius: 

        UNTER_UNTER_BEDEUTUNG  proprie:

         * DIPL. Westph.; 14 p. 43,16 (a. 833) "ut ... locum provideremus {(sc. Hludowicus imperator)}, ubi sal fieri ad cibos monachorum ... #sali.|endos atque condiendos potuisset."
         * CARM. Sangall. I; 1,32 "ut ... nec solum carnes #sallirier unius armi {(sc. hirci ingentis)} ... ex hoc {(sc. multitudine salis)} quivissent."
         * ALBERT. M. animal.; 6,86 "dicunt hoc animal {(sc. medusam ['Qualle'])} diu durare, si #sal.|iatur ...; si ita est, tunc fit hoc ex salis desiccatione."; veget. 6,69 "nutrimentum ..., quod a fructu eius {(capparis)} accipi potest, valde parvum est, et praecipue, cum #sal.|itur."
         * CONR. MUR. nov. grec.; 1,2785sqq. "#sallio (#salleo{@salioB2}, #sal.|io {var. l.}), #sallo sale; salio {@salioA25} pede voceque psallo; #sallio #sallitum (#salitum {var. l.}) dat, salsum #sallo, supinum dat saltum salio{@salioA26}, privatur psallo supino; vel dic a salio distinguens #sal.|io {@salioB1}, salo; a sale sunt et habent L simplex #sal.|io, salo." 
         {al.}

        UNTER_UNTER_BEDEUTUNG  in imag. de sacerdotibus; spectat ad Vulg. Matth. 5,13:

         * PETR. DAM. serm.; 75,57 "cum sacerdotis cor ab amore supernae dulcedinis evanescit, amarescit ilico per salsuginem terrenae concupiscentiae ideoque iam non potest aliena corda #sallire."
     
        UNTER_UNTER_BEDEUTUNG translate; spectat ad stilum:

         * EPIST. Worm. I; 34 p. 64,30 (s. XI.^1) "stilum ... meum brevitate mediocri castigassem {(sc. discipulus)} nec tot sibi garrilitates permisissem, si te {(sc. magistrum)} ex illorum hominum numero ... censerem, quibus lectio queque cornupeta videtur, quę ... parvo sale, ne turpe feteat, #sallitur."
         
    UNTER_BEDEUTUNG  latius de quolibet pulvere i. q. conspergere -- bestreuen: 

     * FRAGM. mul. III; 30 "lardo ... mittis mel supra et #salas de pulvera de coriandri semen et absenti."
     * FRAGM. med. falc.; 70 "accipe limaturam de ferro subtilissimam et #sal.|a inde ipsas petias de lardo."; 124 "de ipsa pulvere {(sc. corticis iuniperi triti)} #sal.|a ipsam carnem, quam ad ipsum falconem dabis manducare."

SUB_LEMMA  salitus
VEL  salatus 
GRAMMATIK
    adi. I-II; -a, -um

BEDEUTUNG  saporem salis habens, sale pollens, conditus -- nach Salz schmeckend, gesalzen, salzig, salzreich, in Salz eingelegt:

    UNTER_BEDEUTUNG  gener.:

        UNTER_UNTER_BEDEUTUNG  proprie:

         * CHRON. Noval.; 2,5 p. 85,2 "de quo {(sc. monte)} oritur rivulus ..., in quo dicitur fontem #sal.|itam orire mixtimque cum eo currere."
         * PLATEAR. pract.; 25,49 "inungantur pectus et trachea arteria ex butiro non #sal.|ito (salso, soluto, salsato {var. l.})."
         * TRACT. de aegr. cur.; p. 379,21 "accipe carnem macram #sal.|atam de bacone."
         * ANON. secret.; p. 51,26 "arsenici et sulfuris est sublimatio et linguae tastatio, ut sint #sal.|ata." 
         * ACTA imp. Winkelm.; I 998 p. 756,43 "pro qualibet mezina de carnibus #sallitis gr. V {(sc. aestimentur)}."
         {al.}
     
        UNTER_UNTER_BEDEUTUNG translate; usu rhet.:

         * EKKEH. IV. bened. I; prol. 2,84 "si ... incęptum quis tale loquatur ineptum, quod paucos prorsus teneat benedictio versus, dixeris insulsum seu fellis acredine salsum (gloss.: pro #sallitum)." 
    
    UNTER_BEDEUTUNG  techn. de mitra sale rigida reddenda; 'stärken':

     * IOH. NEAP. Sever.; 6 p. 293,23 "quia caput tuum {(sc. episcopi capti)} multo #sal.|ito bombyce abundat, idcirco placidissimus te adhortor {(sc. victor)}, ut meis salutaribus monitis obedias et consulas tuae ... commoditati."

SUB_LEMMA saliens
GRAMMATIK
    subst. III; -entis m.

BEDEUTUNG  qui piscem allec q. d. sale condit -- einer der Hering einsalzt, pökelt: 

 * CHART. Lub.; I 27 p. 33,3 (a. 1224) "octo nummi dantur pro quolibet #sall|.ient.|e excepto gubernatore et cibaria faciente." 

AUTOR 
    Mandrin

UNTERDRÜCKE WARNUNG 516