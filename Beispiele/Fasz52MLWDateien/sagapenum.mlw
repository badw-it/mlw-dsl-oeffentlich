 LEMMA sagapenum VEL *serapinum

 GRAMMATIK
      subst. II; -i n.

 SCHREIBWEISE
      script. et form.:
           agapen: * ANTIDOT. Glasg.; p. 152,27
           saccapin(um): {=> sagapenum_1} *{adde} ANTIDOT. Bamb.; 19 p. 27,33
           sagopino: * ANTIDOT. Glasg.; p. 107,34
           sagapin(um): {=> sagapenum_2; sagapenum_3; sagapenum_6} {al.}
           sagapinon: * PAUL. AEGIN. cur.; 236 p. 173,13
           sagapinus: * GLOSS. Salern.; p. 10^b,4
           saga punic(um): {=> sagapenum_4}
           serra-: * ANON. secret.; p. 67,28
           serafi-: * IOH. IAMAT. chirurg.; 8,27 p. 69,12
           serapi(um): {=> sagapenum_5} *{adde} GLOSS. Salern.; p. 9^b,27
           serapinus: * GLOSS. Salern.; p. 10^b,4
           {?}saraptanos: * GLOSS. Salern.; p. 13^b,10

           gen.:
                -u: {=> sagapenum_1} *{adde} * ANTIDOT. Bamb.; 38

      BEDEUTUNG  cummi (ex Ferula persica confecta) -- Harz, Gummi (vom Steckenkraut):
           * ANTIDOT. Lond.; p. 17,26 "#sagapino {@ sagapenum_2} 𐅻 III."
           * COMPOS. Voss. I; 7 "accipies ... #saga punici  {@ sagapenum_4}
            ((
               * COMPOS. Matr.; 59,2 "sagapini {@sagapenum_3}"
               )) cretici."
           * ANTIDOT. Glasg.; p. 114,16 "#saccapinu {@ sagapenum_1} dr<achmas> IIII."
           * CIRCA INSTANS; p. 7a^v "ad ungues reparandos accipe #serapinum."
           * MATTH. PLATEAR. (?) gloss.; p. 371^D "gumma est arboris, quae ... dicitur #sagapenum."
           * TRACT. de aegr. cur.; p. 157,27 "recipe oleum, ... aloe #serapi{@ sagapenum_5}, id est picem Grecam."
           * ALPHITA; S 1 "#sagapinum {@ sagapenum_6} sive #serapinum gumma est."
           {persaepe.}

      BEDEUTUNG de planta fort. i. q. raphanus agria -- viell.: Wilder Rettich (Raphanus raphanistrum L.):
           * GLOSS. Roger. I B; 1,10 (1,12) p. 529,37 "recipe ... #serapinum, id est capistrum agreste, pro pleuresi."


AUTORIN Weber