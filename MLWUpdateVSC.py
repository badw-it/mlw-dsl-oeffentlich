"""MLWUpdateVSC.py - aktualisiert (falls nötig) die VSC-Erweiterung

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2021 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import subprocess, sys, os


def sync_and_share_verzeichnis():  # -> Tuple[str, str]:
    """Liefert den Pfad des gemeinsamen Syn & Share Verzeichnisses sowie
    den Verzeichnisnamen (d.h. den letzten Teil des Pfades):
    z.B. ('~/LRZ Sync+Share/MLW (Eckhart Arnold)', 'MLW (Eckhart Arnold)')
    """
    sync_n_share = os.path.join(os.path.expanduser('~'), 'LRZ Sync+Share')
    for mlwdir in ('MLW (Eckhart Arnold)', 'MLW', 'MLW-Demo (Eckhart Arnold)', 'MLW-Demo'):
        mlw = os.path.join(sync_n_share, mlwdir)
        if os.path.exists(mlw):
            return mlw, mlwdir
    print('Verzeichnis "%s" existiert nicht!? Wurde Sync&Share richtig installiert?' % mlw)
    return '', ''


def update_vscode_extension(mlw_file_path='', force_update=False):
    """Aktualisiert die VSC-Erweiterung, falls eine neue Version im
    MLW-DSL-Repositorium auf sync&share gefunden wurde."""
    import shutil
    extensions = ['mlwquelle']
    for vsdir in ('.vscode-oss', '.vscode', '.vscodium'):
        vscode_path = os.path.join(os.path.expanduser('~'), vsdir)
        if os.path.exists(vscode_path):
            break
    else:
        vscode_path = ''
        print('Konnte das .vscode-Verzeichnis zur Installation der '
              'MLW-Erweiterung nicht finden :-(')
    extensions_path = os.path.join(vscode_path, 'extensions')
    sync_n_share_path, mlwdir_name = sync_and_share_verzeichnis()
    if not sync_n_share_path:
        print('Konnte Sync&Share-Verzeichnis nicht finden!')
        return False
    extensions_src = os.path.join(sync_n_share_path, 'MLW-Software', 'VSCode')

    def ignore(directory, filelist):
        filelist = [filename for filename in filelist if filename.lower() not in ('.git', )]
        return directory, filelist

    def extract_version(json):
        i = json.find('"version"') + len('"version"')
        if i < 0:
            return -1, -1, -1
        i = json.find('"', i) + 1
        k = json.find('"', i)
        return tuple(int(part) for part in json[i:k].split('.'))

    def update_needed(src_path, dest_path):
        if not os.path.exists(os.path.join(dest_path, 'package.json')):  return True
        with open(os.path.join(src_path, 'package.json'), 'r', encoding='utf-8') as f:
            src = f.read()
        with open(os.path.join(dest_path, 'package.json'), 'r', encoding='utf-8') as f:
            dest = f.read()
        return extract_version(src) > extract_version(dest)

    update_counter = 0

    for extension in extensions:
        src_path = os.path.join(extensions_src, extension)
        if not os.path.exists(src_path):
            print('Kann Erweiterung "%s" nicht aktualisieren, da keine Quelle dafür gefunden'
                  % extension)
            continue

        dest_path = os.path.join(extensions_path, extension)
        veraltet_path = os.path.join(extensions_path, extension + '_VERALTET')
        if os.path.exists(dest_path):
            if force_update or update_needed(src_path, dest_path):
                print('  Neue Version von "%s" wird installiert.' % extension)
                update_counter += 1
            else:
                continue
            os.rename(dest_path, veraltet_path)
        else:
            update_counter += 1
        try:
            shutil.copytree(src_path, dest_path, ignore=ignore)
            if os.path.exists(veraltet_path):
                shutil.rmtree(veraltet_path)
            if sys.platform == "win32":
                print('Falls sich nichts tut, bitte mal kurz die ENTER-Taste drücken!')
                result = subprocess.run(['npm', 'install'], shell=True, cwd=dest_path)
            else:
                result = subprocess.run(['npm install'], shell=True, cwd=dest_path)
            if result.returncode != 0:
                if sys.platform == "win32":
                    install_script_path = os.path.join(dest_path, 'run_install.bat')
                    result = subprocess.run([install_script_path], shell=True, cwd=dest_path)
                else:
                    if sys.platform == "darwin":
                        npm_dir = os.path.join("nodejs_apple", "bin")
                    elif sys.platform == 'linux':
                        npm_dir = os.path.join("nodejs_linux", "bin")
                    else:
                        npm_dir = ''
                    npm_path = os.path.join(sync_n_share_path, npm_dir, 'npm')
                    result = subprocess.run([npm_path + ' install'], shell=True, cwd=dest_path)
                if result.returncode != 0:
                    print('Could not install all VSC-extensions properly!')
        except shutil.Error as error:
            print('Error %s while trying to install extension "%s".'% (error, extension))
            if os.path.exists(veraltet_path):
                print('Rolling back previous version of extension "%s".' % extension)
                os.rename(veraltet_path, dest_path)

    # lösche Dateien, die augenscheinlich die Aktualisierung von Erweiterungen blockieren
    if update_counter or force_update:
        for name in ['.init-default-profile-extensions', 'extensions.json', '.obsolete']:
            path = os.path.join(extensions_path, name)
            if os.path.exists(path):
                os.remove(path)

    # aktualisiere tasks.json ## settings.json wird nicht mehr aktualisiert (2.3.2023)

    if update_counter and mlw_file_path and not \
            (mlw_file_path.find('eckhart') >= 0 or mlw_file_path.find('PycharmProjects') >= 0):
        ur_path = os.path.abspath(mlw_file_path)
        if not os.path.isdir(ur_path):
            ur_path = os.path.dirname(ur_path)
        path = ur_path
        last_path = ''
        while path != last_path and not os.path.exists(os.path.join(path, '.vscode')):
            last_path = path
            path = os.path.dirname(path)
        if path == last_path:
            vsc_path = os.path.join(ur_path, '.vscode')
            print('Lege Verzeichnis "%s" an.' % vsc_path)
            os.mkdir(vsc_path)
        else:
            vsc_path = os.path.join(path, '.vscode')

        print('Lese: ' + os.path.join(extensions_src, 'tasks.json'))
        with open(os.path.join(extensions_src, 'tasks.json'), 'r', encoding='utf-8') as f:
            tasks_json = f.read()
            tasks_json = tasks_json.replace('MLW (Eckhart Arnold)', mlwdir_name)
            if sys.platform != "linux" and not sys.platform.lower().startswith('win'):
                tasks_json = tasks_json.replace('/home', '/Users')

                # Sonderfall Testsystem von Massimo Ce nicht mehr benötigt (2.3.2023)
                # if os.path.expanduser('~').lower().find('massimo') >= 0:
                #     tasks_json = tasks_json.replace('${env:USERNAME}', 'Massimo')
            # print('>>>>', mlwdir_name)
        # print('Lese: ' + os.path.join(extensions_src, 'settings.json'))
        # with open(os.path.join(extensions_src, 'settings.json'), 'r', encoding='utf-8') as f:
        #     settings_json = f.read()

        mlw_dir = os.path.basename(sync_n_share_path)
        tasks_json = tasks_json.replace('\\MLW\\', "\\" + mlw_dir + "\\", 1)
        tasks_target_path = os.path.join(vsc_path, 'tasks.json')
        print('Aktualisiere: ' + tasks_target_path)
        with open(tasks_target_path, 'w', encoding='utf-8') as f:
            f.write(tasks_json)
        # settings_target_path = os.path.join(vsc_path, 'settings.json')
        # print('Aktualisiere: ' + settings_target_path)
        # with open(settings_target_path, 'w', encoding='utf-8') as f:
        #     f.write(settings_json)

    else:
        pass
        # print('Keine Aktualisierung von settings.json und tasks.json in: ' + mlw_file_path)

    return True


if __name__ == '__main__':
    import tempfile
    mlwdir = sys.argv[1] if len(sys.argv) > 1 else tempfile.gettempdir()
    update_vscode_extension(mlwdir, force_update=True)
    print('Jetzt am besten mal Visual Studio Code neu starten!')
