#!/usr/bin/env python3

"""test_MLW_grammar.py - unit tests for the MLW grammar 

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2017 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys

scriptdir = os.path.dirname(os.path.realpath(__file__))
sys.path.extend([os.path.join(scriptdir, 'DHParser')])

import DHParser.log

from DHParser import configuration
from DHParser import dsl
from DHParser import testing

LOGGING = 'LOGS'
TEMPLATES = ['*_test_*.ini']
# TEMPLATES = ['SPIELWIESE.ini']
DIRECTORY = 'test_grammar'

if __name__ == "__main__":
    curdir = os.getcwd()
    os.chdir(scriptdir)
    if not dsl.recompile_grammar('MLW.ebnf', force=False,
                                 notify=lambda: print('recompiling MLW.ebnf')):  # recompiles grammar only if it has changed
        with open('MLW_ebnf_MESSAGES.txt', encoding="utf-8") as f:
            print(f.read())
        sys.exit(1)
    os.chdir(curdir)

    configuration.access_presets()
    configuration.set_preset_value('test_parallelization', True)
    configuration.set_preset_value('compact_sxpr_threshold', 5)
    configuration.set_preset_value('infinite_loop_warning', False)
    configuration.set_preset_value('history_tracking', True)

    from MLWCompiler import get_grammar, get_transformer, junctions

    if len(sys.argv) > 1:
        try:
            i = sys.argv.index('--xml')
            del sys.argv[i]
            configuration.set_preset_value('AST_serialization', 'XML')
        except ValueError:
            configuration.set_preset_value('AST_serialization', 'S-expression')

    # configuration.set_preset_value('AST_serialization'] = "S-expression"  # "XML"

    if len(sys.argv) > 1:
        if os.path.isdir(sys.argv[1]):
            DIRECTORY = sys.argv[1]
            if len(sys.argv) > 2:
                print("All arguments after '%s' will be ignored" % DIRECTORY)
        else:
            known_filetypes = set(testing.TEST_READERS.keys())
            valid_names = []
            DIRECTORY = None
            for name in sys.argv[1:]:
                directory, basename = os.path.split(name)
                if DIRECTORY is None:
                   DIRECTORY = directory
                if directory != DIRECTORY:
                    print("Cannot test files from different directories '%s' and '%s' at the same time"
                          % (directory, DIRECTORY))
                elif os.path.isfile(name) and os.path.splitext(basename)[1].lower() in known_filetypes:
                    valid_names.append(basename)
                else:
                    print("'%s' doesn't exist or isn't a test file in one of the known formats: %s"
                          % (basename, str(known_filetypes)))
            if valid_names:
                TEMPLATES = valid_names
                LOGGING = 'LOGS' if (len(TEMPLATES) == 1) else ''
    configuration.finalize_presets()

    print("\nRunning all tests matching %s in directory '%s' %s logging."
          % (TEMPLATES, os.path.basename(DIRECTORY), 'with' if LOGGING else 'without'))

    DHParser.log.start_logging(LOGGING)
    from DHParser import get_config_value
    error_report = testing.grammar_suite(DIRECTORY, get_grammar, get_transformer,
                                         fn_patterns=TEMPLATES, verbose=True,
                                         junctions=junctions, show={'AST'})  # xml, xml.ausgabe
    if error_report:
        print('\n\n')
        print(error_report)
        print(error_report[:error_report.find(':\n')] + '!')
