/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

 /* See the VSCode-API: https://code.visualstudio.com/api */


const DEBUG: boolean = false;


import * as vscode from 'vscode';
import * as net from 'net';
import { workspace, commands, env, Disposable, ExtensionContext, OutputChannel, Webview } from 'vscode';
import {
    LanguageClient,
    LanguageClientOptions,
    ServerOptions,
    // TransportKind,
    InitializeError
} from 'vscode-languageclient';
import { CodeLensResolveRequest, ResponseError } from 'vscode-languageserver-protocol';

// import get_css from './helper';

/* import * as child_process from "child_process";

const execPython = (cmd: string, args: string) =>
    new Promise<string>((resolve, reject) => {
        var cmdstr = 'python3 "' + cmd + '" "' + args + '"' 
        if (isLinux) {
            cmdstr = cmdstr.replace('\\', '/')
        } else {
            cmdstr = cmdstr.replace('/', '\\')
        }
        child_process.exec(cmdstr, (err, out) => {
            const terminals = <vscode.Terminal[]>(<any>vscode.window).terminals;
            if (err) {
                return reject(err);
            }
            return resolve(out);
        });
    }); */


let terminal = null;


const execPython = (cmd: string, args: string) =>
    new Promise<string>((resolve, reject) => {
        var interpreter = 'python "';
        if (isLinux||isMacOS) { interpreter = 'python3 "'; }
        var cmdstr = interpreter + cmd + '" "' + args + '"' ;
        if (isLinux||isMacOS) {
            cmdstr = cmdstr.replace('\\', '/');
        } else {
            cmdstr = cmdstr.replace('/', '\\');
        }
        if (!terminal) {
            terminal = vscode.window.createTerminal(`MLW`);
        }
        terminal.sendText(cmdstr + '\n');
        terminal.show();
        return resolve('');
    });

var fs = require('fs');
let client: LanguageClient;

const homedir = require('os').homedir();
const isLinux = process.platform === "linux";
const isMacOS = process.platform === "darwin";
const isWindows = process.platform === "win32";

// import * as path from 'path';

// function startLangServer(command: string): Disposable {
// const serverOptions: ServerOptions = {
//         command: command,
// };
//     const clientOptions: LanguageClientOptions = {
//         documentSelector: [{scheme: 'file', language: 'json'}],
//     };
//     return new LanguageClient(command, serverOptions, clientOptions).start();
// }


let defaultPort: number = 8888;


function startLangServerStream(command: string, args: string[]): Disposable {
    const serverOptions: ServerOptions = {
        command,
        args,
    };


    const clientOptions: LanguageClientOptions = {
        documentSelector: [{scheme: 'file', language: 'mlw'}],
        synchronize: {
            // Notify the server about file changes to '.clientrc files contained in the workspace
            fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
        },
        initializationFailedHandler: function(error: ResponseError<InitializeError> | Error | any): boolean {
            console.log('InitializationFailed');
            console.log(error.toString());
            return false;
        }
    };
    let client = new LanguageClient(command, `mlw stream lang server`, serverOptions, clientOptions);
    return client.start();
}


function startLangServerTCP(addr: number) : Disposable {
    const serverOptions: ServerOptions = function() {
        return new Promise((resolve, reject) => {
            var client = new net.Socket();
            client.connect(addr, "127.0.0.1", function() {
                resolve({
                    reader: client,
                    writer: client
                });
            });
            console.log('connection created');
        });
    };


    // Options to control the language client
    let clientOptions: LanguageClientOptions = {
        documentSelector: [{scheme: 'file', language: 'mlw'}],
        synchronize: {
            // Notify the server about file changes to '.clientrc files contained in the workspace
            fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
        },
        initializationFailedHandler: function(error: ResponseError<InitializeError> | Error | any): boolean {
            console.log('InitializationFailed');
            console.log(error.toString());
            return false;
        }
    };

    client = new LanguageClient(
        'MLWLanguageServer',
        `mlw tcp lang server (port ${addr})`,
        serverOptions,
        clientOptions);
    let disposable = client.start();
    return disposable;
}


function derive_path(uri: string): string[] {
    var separator: string = '/';
    var i = uri.lastIndexOf('/');
    if (i < 0)  { 
        i = uri.lastIndexOf('\\');
        if (i >= 0) separator = '\\';
    }
    var filename = uri.slice(i + 1, uri.length);
    var path = uri.slice(0, i + 1);
    return [separator, path, filename];
}


function getWebviewContent(webview: Webview, uri: string) {
    let separator: string, path: string, filename: string ;
    [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
    let html_path = path + 'Ausgabe' +  separator + 'HTML-Vorschau' + separator; 
    let text = fs.readFileSync(html_path + filename.replace('.mlw', '.html'), 'utf8');
    // text = text.replace('<link id="pagestyle" rel="stylesheet" href="MLW_kompakt.css" />', '<style>' + get_css() + '</style>');
    text = text.replace('"MLW_kompakt.css"', `"${webview.asWebviewUri(vscode.Uri.file(html_path + "MLW_kompakt.css"))}"`);
    text = text.replace("'MLW_kompakt.css'", `'${webview.asWebviewUri(vscode.Uri.file(html_path + "MLW_kompakt.css"))}'`)  ;
    text = text.replace("'MLW_disposition.css'", `'${webview.asWebviewUri(vscode.Uri.file(html_path + "MLW_disposition.css"))}'`) ;
    text = text.replace("'MLW.css'", `'${webview.asWebviewUri(vscode.Uri.file(html_path + "MLW.css"))}'`);
    text = text.replace('"MLW.js"', `"${webview.asWebviewUri(vscode.Uri.file(html_path + "MLW.js"))}"`);
    return text;
}


export function activate(context: ExtensionContext) {
    // console.log('activating language server connector...');
    // console.log(env.appName);
    // console.log(env.appRoot);
    // console.log(workspace.name);
    // console.log(workspace.workspaceFolders);
    // console.log(workspace.getConfiguration());
    // let disposable = startLangServerStream("python3", ["MLWServer.py", "--stream", "--logging"]);
    
    let HTMLVorschau = vscode.commands.registerCommand('mlwquelle.HTMLVorschau', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWCompiler.py', path + filename);
    });
    context.subscriptions.push(HTMLVorschau);

    let HTMLVorschauServer = vscode.commands.registerCommand('mlwquelle.HTMLVorschauServer', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWServer.py', path + filename);
    });
    context.subscriptions.push(HTMLVorschauServer);

    let HTMLVorschauFaszikel = vscode.commands.registerCommand('mlwquelle.HTMLVorschauFaszikel', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWCompiler.py', path);
    });
    context.subscriptions.push(HTMLVorschauFaszikel);   
    let DruckVorschau = vscode.commands.registerCommand('mlwquelle.DruckVorschau', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWPrint.py', path + filename);
    });
    context.subscriptions.push(DruckVorschau);
    let DruckVorschauFaszikel = vscode.commands.registerCommand('mlwquelle.DruckVorschauFaszikel', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWPrint.py', path);
    });
    context.subscriptions.push(DruckVorschauFaszikel);
    let Fahnenkorrektur = vscode.commands.registerCommand('mlwquelle.Fahnenkorrektur', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWPrint.py', "-e proof " + path + filename);
    });
    context.subscriptions.push(Fahnenkorrektur);
    let FahnenkorrekturFaszikel = vscode.commands.registerCommand('mlwquelle.FahnenkorrekturFaszikel', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWPrint.py', "-e proof " + path);
    });
    context.subscriptions.push(FahnenkorrekturFaszikel); 
    
    let Update = vscode.commands.registerCommand('mlwquelle.update', () => {
        let separator: string, path: string, filename: string;
        [separator, path, filename] = derive_path(vscode.window.activeTextEditor.document.uri.fsPath);
        execPython(homedir + '/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWUpdateVSC.py', "");
    });
    context.subscriptions.push(Update); 


    let VSCVorschau = vscode.commands.registerCommand('mlwquelle.VSCVorschau', () => {
        // Instance of the webview
        const panel = vscode.window.createWebviewPanel(
            'mlw-webview',
            'MLW-Webview',
            vscode.ViewColumn.One,
            { enableScripts: true }
        );
        // Set the HTML
        panel.webview.html = getWebviewContent(panel.webview, vscode.window.activeTextEditor.document.uri.fsPath);
    });
    context.subscriptions.push(VSCVorschau);

    // disposable = vscode.commands.registerCommand('')
    let disposable;
    if (DEBUG) {
        if (isLinux||isMacOS) {
           disposable = startLangServerStream("python3", [homedir + "/Entwicklung/MLW-DSL/MLWServer.py", "--stream", "--logging"]);
        } else {
           disposable = startLangServerStream("python", [homedir + "\\PyCharmProjects\\MLW-DSL\\MLWServer.py", "--stream", "--logging"]);
        }
    } else {
        if (isLinux||isMacOS) {
            disposable = startLangServerStream("python3", [homedir + "/LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software/MLWServer.py", "--stream"]);
         } else {
            disposable = startLangServerStream("python", [homedir + "\\LRZ Sync+Share\\MLW (Eckhart Arnold)\\MLW-Software\\MLWServer.py", "--stream"]);
         }
    }
    // let disposable = startLangServerTCP(defaultPort);
    context.subscriptions.push(disposable);
    console.log('MLWServer started...');        
}


export function deactivate(): Thenable<void> | undefined {
    if (!client) {
         return undefined;
     }
     // console.log('stop lsp client');
     return client.stop();
}
