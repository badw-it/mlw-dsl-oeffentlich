Visual Studio Code Unterstütztung
=================================

Zum installieren der VSCode-Unterstützung muss das
Unterverzeichnisse `mlwquelle` in das Konfigurationsverzeichnis von
VSCode ("~/.vscode") kopiert oder von dort verlinkt werden. Die
`tasks.json`-Datei gehört in das - davon zu unterscheidende -
`.vscode`-Verzeichnis unterhalb des Hauptverzeichnisses des Projekts,
also z.B. `MLW/.vscode`. Sie wird dann ausgewertet und die darin
angegebenen Aufgaben in Visual Studio Code verfügbar gemacht, wenn das
Hauptverzeichnis in VSCode (im Beispiel `MLW`) geöffnet wird.


Syntax-Highlighting
-------------------

<https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide>
<https://macromates.com/manual/en/language_grammars#naming-conventions>


Language Server
---------------

See:
- <https://code.visualstudio.com/api/language-extensions/language-server-extension-guide>
- <https://microsoft.github.io/language-server-protocol/>
- <https://langserver.org/>

## Functionality

Client-module for Visual Studio Code to connect MLWServer.py as a language-server for MLW

## Running the Sample

- Run `npm install` in this folder. This installs all necessary npm modules in the client folder
- NICHT MEHT NOTWENDIG: Run `python3 ../MLWServer.py --startserver` or `python3 ../MLWServer.py --startdaemon`
- Open VS Code on this folder.
- Press Ctrl+Shift+B to compile the client and server.
- Switch to the Debug viewlet.
- Select `Launch Client` from the drop down.
- Run the launch config.
- If you want to debug the server as well use the launch configuration `Attach to Server`
- In the [Extension Development Host] instance of VSCode, open a document in '.mlw'-language-mode.



TODOs
-----

1. Füge einen Task-Provider hinzu, der die "tasks.json" ersetzt:
   <https://code.visualstudio.com/api/extension-guides/task-provider>
   <https://github.com/microsoft/vscode-extension-samples/blob/master/task-provider-sample/src/customTaskProvider.ts>
