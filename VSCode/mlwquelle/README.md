# MLWQuelle

MLWQuelle definiert die syntaktische Hervorhebung und die Vorschlags-Schnipsel für
Wörterbuchartikel des Mittellateinischen Wöterbuches. Mit Hilfe der syntaktischen
Hervorhebung werden auch die Opera-Majora-Autornamen farblich kenntlich gemacht.

## Fähigkeiten

* Vorschlagsschnipsel für Literaturverweise
* Farbliche Hervorhebung von Schlüsselwörtern, Zitaten etc.
* Farbliche Hervorhebung von Opera-Majora-Autoren

## Autor

Autor: Eckhart Arnold <arnold@badw.de>

Quelle: <https://gitlab.lrz.de/badw-it/MLW-DSL>


Syntax-Highlighting
-------------------

<https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide>
<https://macromates.com/manual/en/language_grammars#naming-conventions>


Language Server
---------------

See:
- <https://code.visualstudio.com/api/language-extensions/language-server-extension-guide>
- <https://microsoft.github.io/language-server-protocol/>
- <https://langserver.org/>

## Functionality

Client-module for Visual Studio Code to connect MLWServer.py as a language-server for MLW
 
## Running the Sample

- Run `npm install` in this folder. This installs all necessary npm modules in the client folder
- Open VS Code on this folder.
- Press Ctrl+Shift+B to compile the client and server.
- Switch to the Debug viewlet.
- Select `Launch Client` from the drop down.
- Run the launch config.
- If you want to debug the server as well use the launch configuration `Attach to Server`
- In the [Extension Development Host] instance of VSCode, open a document in '.mlw'-language-mode.



TODOs
-----

1. Füge einen Task-Provider hinzu, der die "tasks.json" ersetzt:
   <https://code.visualstudio.com/api/extension-guides/task-provider>
   <https://github.com/microsoft/vscode-extension-samples/blob/master/task-provider-sample/src/customTaskProvider.ts>
