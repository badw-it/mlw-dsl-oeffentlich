#!/usr/bin/env python3

TIMESTAMP = "Mon Aug  5 15:40:03 2024"
VERSION = 'MLW-DSL Version: ' + TIMESTAMP

if __name__ == "__main__":
    print(VERSION)
