#!/usr/bin/env python3

"""MLWFehlerbericht.py - schickt Fehlermeldungen an digitalisierung@badw.de

OBSOLET!!!

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2017 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys


def Sende_Fehlerbericht_mit_SMTP(Quelltext_Dateiname, schnell=True):
    from email.message import EmailMessage
    import smtplib
    with open(Quelltext_Dateiname) as f:
        Quelltext = f.read()
    Nachricht = EmailMessage()
    Nachricht.set_content(Quelltext)
    Nachricht['Subject'] = f'Fehler beim Übersetzen von "%s".' % Quelltext_Dateiname
    Nachricht['From'] = 'digitalisierung@badw.de'
    Nachricht['To'] = 'arnold@badw.de'

    smtp = smtplib.SMTP('localhost')
    smtp.send_message(Nachricht)
    smtp.quit()


def Sende_Fehlerbericht_mit_Outlook(Anhang_Dateiname, quelltext_namen = [], schnell=True):
    if 'win' not in sys.platform.lower():
        print('Kann Fehlerberichte nur von Windows-Systemen aus versenden, nicht von: ' + sys.platform)
        return
    try:
        import win32com.client as win32
        if os.path.exists('pywin32installation.txt'):
            os.remove('pywin32installation.txt')
    except (ImportError, ModuleNotFoundError):
        if os.path.exists('pywin32installation.txt'):
            print('Kann keinen Fehlerbericht von diesem System versenden, '
                  'da die Installation von pywin32 fehlgeschlagen hat.')
            return
        import subprocess
        print('Auf diesem System ist pywin32 bisher noch nicht installiert.')
        print('Installiere pywin32 für Python, um Emails versenden zu können...\n')
        with open('pywin32installation.txt', 'w', encoding='utf-8') as f:
            f.write('Die Installation von pywin32 wurde schon einmal erfolglos versucht.')
        subprocess.run([sys.executable, '-m', 'pip', 'install', '--upgrade', 'pip', 'setuptools', 'wheel'])
        subprocess.run([sys.executable, '-m', 'pip', 'install', 'pywin32'])
        # for pth in sys.path:
        #     if pth.endswith('site-packages'):
        #         break
        # Starte das Script erneut, da sonst win32 beim import (noch) nicht gefunden wird.
        subprocess.run([sys.executable] + sys.argv)
        return
    print('Verschicke "%s"' % os.path.abspath(Anhang_Dateiname))
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = 'arnold@badw.de'
    mail.Subject = 'Fehler beim Übersetzen von: %s.' \
                   % ', '.join(os.path.base_name(name) for name in quelltext_namen)
    mail.Body = 'Die MLW-Datei im Anhang hat leider unverständliche Fehler produziert :-('
    # mail.HTMLBody = '<h2>HTML Message body</h2>' #this field is optional
    mail.Attachments.Add(os.path.abspath(Anhang_Dateiname))
    if schnell:
        mail.Send()
        print('Fehlerbericht gesendet. Fertig.\n')
    else:
        mail.Display(True)


def Sende_Fehlerbericht_mit_MailTo(Quelltext_Dateiname, schnell=True):
    import webbrowser
    with open(Quelltext_Dateiname) as f:
        Quelltext = f.read()
    webbrowser.open(('mailto:arnold@badw.de&subject=Fehler beim Übersetzen von %s.'
                     % (os.path.abspath(Quelltext_Dateiname))).replace(' ', '%20'))


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print('Aufruf:  python MLWFehlerbericht.py DATEINAME.mlw')
    else:
        if sys.argv[1] == '--schnell':
            Sende_Fehlerbericht_mit_Outlook(sys.argv[2], schnell=True)
            # Sende_Fehlerbericht_mit_MailTo(sys.argv[2], schnell=True)
        else:
            Sende_Fehlerbericht_mit_Outlook(sys.argv[1], schnell=False)
            # Sende_Fehlerbericht_mit_MailTo(sys.argv[1], schnell=False)
