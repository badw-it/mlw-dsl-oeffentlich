Mittellateinisches Wörterbuch
=============================

Dieses Verzeichnis enthält die domänenspezifische Sprache (DSL)
für das [Mittellateinische Wörterbuch](https://www.mlw.badw.de) (MLW) der
[Baayerischen Akademie der Wissenschaften](https://badw.de) (BAdW).

Die Quellen für die MLW-Software liegen auf dem GitLab-Server des Leibniz-
Rechenzentrums in Garching unter: 

* Öffentliches Repositorium: [gitlab.lrz.de/badw-it/mlw-dsl-oeffentlich](https://gitlab.lrz.de/badw-it/mlw-dsl-oeffentlich)

* Internes Repositorium mit Disussionen, Tickets etc: [gitlab.lrz.de/badw-it/MLW-DSL](https://gitlab.lrz.de/badw-it/MLW-DSL)


Autor und Copyright und Lizenz
------------------------------

Copyright 2018 by Eckhart Arnold <Eckhart.Arnold@badw.de>

Lizenz: [Apache 2.0 Open Source Lizenz](https://www.apache.org/licenses/LICENSE-2.0)



Stand
-----

Datum: 5.8.2024

Produktionsversion, die seit einigen Jahren im Einsatz ist und mit der
schon mehrere Faszikel fertiggestellt wurden. 

* Die Forschungsdaten (Quelletexte, XML etc.) liegen auf <https://daten.badw.de/mlw-data>.
* Die Druckfassung des MLW kann vom Publikationsserver der BAdW abgerufen werden:
  <https://publikationen.badw.de/de/meta/index#~1-is-Mittellateinisches%20W%C3%B6rterbuch>

* Die digitale Ausgabe wird vom Wörterbuchnetz Trier bereitgestellt:
  <https://www.woerterbuchnetz.de/?sigle=MLW>


Die Nutzer-Dokumentation findet sich in der Datei `MLW_Notation_Beschreibung.md`
im Ordner `Dokumentation`


Repository und Untermodule
--------------------------

Die DSL für das MLW setzt [DHParser](https://gitlab.lrz.de/badw-it/DHParser)
voraus. Um immer die aktuelle DHParser-Version aus dem gitlab zu verwenden,
empfiehlt es sich, das DHParser-Repositorium neben dem MLW-DSL repositorium
zu installieren und im Hauptverzeichnis des MLW-DSL-Repositoriums 
eine symbolische Verknüpfung mit dem Namen "DHParser" auf das 
DHParser-Unter(!)verzeichnis des DHParser-Repositorium anzulegen:

    # git clone https://gitlab.lrz.de/badw-it/MLW-DSL.git
    # git clone https://gitlab.lrz.de/badw-it/DHParser.git
    # cd MLW-DSL
    # ln -s ../DHParser/DHParser MLW-DSL

Hinweis: unter Windows kann der symbolische Link nicht über die grafische
Benutzeroberfläche angelegt werden, statt dessen muss das Kommando
"mklink /D" in der Eingabeaufforderung verwendet werden!

Es wären noch drei alternative Lösungen denkbar:

1. Statt eines symbolischen Links das DHParser/DHParser-Unterverzeichnis in ein
   DHParser-Unterverzeichnis von MLW-DSL kopieren, d.h. eine Kopie von DHParser
   im MLW-DSL-Repositorium mitzupflegen. -> Hauptnachteil: Änderungen an DHParser,
   die durch Anforderungen von MLW-DSL motiviert sind, müssen erst wieder ins
   DHParser-Repo zurückgespielt werden. Bei der Lösung mit symbolischem Link werden
   sie direkt dort durchgeführt.

2. DHParser mit pip vom [Python-PackageIndex](https://pypi.org) installieren.
   Das hat den Nachteil, das bei ggf. notwendige kurzfristige Änderungen und
   Fehlerkorrekturen in DHParser erst das ganze DHParser-Paket wieder auf
   pypi.org hochgeladen werden muss.

3. git-submodules. Das hat sich in der Praxis als sehr viel umständlicher erwiesen.


Verzeichnisstruktur
-------------------

Alle wichtigen Skripte, insbesondere das für die Installation 
(`Installiere-MLW.py`) und für die Übersetzung von MLW-Artikeln
(`MLWCompiler.py`) liegen im Hauptverzeichnis. Daneben gibt es folgende
Unterverzeichnisse:

1. Beispiele - enthält jeweils in eigenen Unterverzeichnissen Beispielartikel
               für die domänenspezifische Notation des Mittellateinischen
               Wörterbuchs (MLW-DSL). Mit dem Skript `verarbeite_Beispiele.py`
               im Hauptverzeichnis können diese Beispiele übersetzt werden. 
               
2. CSS -       Hier liegt die CSS-Datei (`MLW.css`) für die Vorschau von 
               MLW-Artikeln.
               
3. DHParser - Symbolischer Link auf das DHParser-Paket, auf dem der
               der Test- und Compiler-Code des MLW aufsetzt.

4. Dokumentation - Bedienungsanleitungen und Präsentationen zur MLW-Software

5. Javascript - Javascript code für die HTML-Vorschauseiten von MLW-Artikeln
                         
6. test_grammar - Die Einheiten-Tests für die Grammatik der MLW-DSL. 
               Unvollständig, aber trotzdem nützlich. Die Grammatiktests können
               mit dem Skript `tst_MLW_gramma.py` im Hauptverzeichnis gestartet
               werden. Die Test-Resultate werden in automatisch generierten
               Dateien im Markdown-Format im Unterverzeichnis REPORT zusammengefasst.

7. tests -     Einheiten-Tests (für [pytest](https://docs.pytest.org/)) 
               für andere Teile des Gesamtsystems, insbesondere für die
               die XML- und HTML-Ausgabe.

8. VSCode -    Alle Datein und ggf. Skripte für die Visual-Studio-Code-
               Integration des MLW. 


Grammatik
---------

Die Grammatik der MLW-DSL ist in der Datei `MLW.ebnf` spezifiziert. Aus dieser
Spezifikation wird automatisch der Python-Parser für die MLW-DSL in der Datei
`MLWCompiler.py` generiert. Um den Parser neu zu generieren, genügt es die
Grammatik-Tests zu starten (Skript `tst_MLW_grammar.py`). Dabei wird der Teil
(und nur der Teil) des Skripts `MLWParser.py` überschrieben, der den Parser enthält!


Skripte
-------

Folgende Skripte liegen im MWL-Hauptverzeichnis:

1. `MLWCompiler.py` - Mit diesem Skript kann man Dokumente im MLW-DSL-Format
   (Endung per Konvention `.mlw`)  übersetzen (vorerst nur nach HTML). In
   der Regel muss dieses Skript nicht direkt aufgerungen werden, sondern
   kann aus dem Visual-Studio-Code-Editor heraus als "Aufgabe" gestartet
   werden.
   
2. `MLWParser.py` - Dieses Skript wird von `MLWCompiler.py` aufgerufen. Es 
   bildet das Frontend für den MLWCompiler und enthält den Parser und
   erledigt die Transformation des konkreten in den abstrakten Syntax-Baum. 

3. `MLWServer.py` - Mit diesem Skript kann man ebenfalls Dokumente im 
    MLW-DSL-Format kompilieren. Anders als MLWCompiler.py (das von MLWServer.py
    importiert wird), started `MLWServer.py` einen TCP/IP-Server im Hintergrund,
    an den Quellcodes zum Kompilieren geschickt werden können. Das geht schneller
    als der Aufruf von MLWCompiler.py, da nicht jedes Mal das ganze 
    MLW-DSL/DHParser-Paket mitsamt der Übersetzung aller regulärer Ausdrücke 
    initialisiert werden muss. Bevor `MLWServer.py` MLWCompiler importiert, prüft
    es, ob schon ein Server läuft, und verwendet diesen.
    
    In Zukunft soll `MLWServer.py` zu einem vollwertigen 
    [Sprach-Server](https://microsoft.github.io/language-server-protocol/) ausgebaut werden. 

4. `MLWPrint.py` - Generiert eine PDF-Datei aus einem einzelnen Artikel oder
    allen Artikeln in einem Verzeichnis. Da die eigentlich Erzeugung der
    PDF-Datei auf einem Server der BAdW-abläuft, funktioniert dieses Skript
    nur, wenn man auch über die Zugangsdaten zum Server verfügt, d.h. nur
    bei Mitarbeitern des MLW.
   
5. `tst_MLW_grammar.py` - Lässt die Einheitentests für die Grammatik im 
   Unterverzeichnis "grammar_tests" laufen. Die Ergebnisse der Tests
   (in Form von Syntaxbäumen der getesteten MLW-Codeschnipsel) werden in 
   dem Unterverzeichnis "REPORT" des Verzeichnisses "grammar_tests"
   abgelegt. Falls bei einem Test Fehler aufgetreten sind, wird ein
   Protokoll des Parsing-Prozesses des entsprechenden Code-Schnipsels in 
   HTML-Form in einem weiteren Unterverzeichnis "grammar_tests/LOGS"
   abgelegt. 
 
6. `lies_Autoren_aus_MLW-DB.py` - Aktualisiert die Autoren-Vorschlagslisten
   für die VSCode-Erweiterung und für die Tippfehlerprüfung beim Übersetzen
   der Quelldateien. Die Daten werden aus der MLW-Datenbank gezogen

   
MLW-Software ausrollen:
-----------------------

Die MLW-Software wird über den Sync&Share-Ordner MLW (Eckhart Arnold)/MLW-Software
ausgerollt. In diesem Ordner kann die Software mit git pull aktualisiert
werden. Dazu sind im Heimverzeichnis folgende Befehle auszuführen:

    cd "LRZ Sync+Share/MLW (Eckhart Arnold)/MLW-Software"
    git pull origin master
    cd DHParser
    git pull origin master
    cd ..

Es empfiehlt sich, voher mit "git status" zu prüfen, ob man sich überhaupt auf
dem "master"-Zweig befindet. Alternativ, kann man auch in den Zweig 
"development" wechseln und dort die entsprechenden Befehle 
"git pull origin development" ausführen.

Danach sollte man zum Testen einmal "MLWCompiler.py" ausführen:

    python3 MLWCompiler.py
    
Wenn im Browser nach wenigen Sekunden ein Wörterbuchartikel angezeigt wird,
ist die Software erfolgreich aktualisiert worden. Die Visual Studio Code (VSC)
Erweiterungen werden aktualisiert, sobald die Bearbeiterin oder der Bearbeiter
aus Visual Studio Code heraus zum ersten Mal eine Datei übersetzt.
(z.B. mit Menü: Terminal->Aufgabe ausführen: MLW->HTML Vorschau). Nach einem
Neustart von Visual Studio Code ist die aktualisierte VSC-Erweiterung aktiv.


Weitere MLW-Software:
---------------------

**MLW-Datenbank**

Die opera majora und minora-Datenbank ist unter <https://dienste.badw.de:9999/>
erreichbar. Um die in der MLW-DSL-Software (einschließlich der Editor-Erweiterung)
hinterlegten Daten aus der Datenbank zu aktualisieren, muss das Skript
"lies_Autoren_aus_MLW-DB.py" ausgeführt werden. 
Auch hierfür werden Zugangsdaten benötigt.
Source-Code der Datenbank: <https://gitlab.lrz.de/badw-it/dmlw> (Autor: Alexander Häberlin)

**MLW-Druckausgabe**

Die Software für die Erzeugung von Druck-PDFs liegt auf: <https://gitlab.lrz.de/badw-it/mlw-export-website>

**DHParser**

Für den Betrieb der MLW-DSL wird das DHParser-Rahmenwerk benötigt: <https://gitlab.lrz.de/badw-it/DHParser>


*weitere Abhängigkeiten*

Außer DHParser ist die einzige weitere Abhängigkeit die requests-library für Python (benötigt von MLWPrint.py).
Empfehlenswert ist weiterhin die Installation von der (regex-Bibliothek)[https://pypi.org/project/regex/].
Ist sie vorhanden, wird sie automatisch gegenüber der re-Bibliothek aus der Python Standard Library bevorzugt.
Für die Entwicklung empfiehlt sich außerdem die Installation von (pytest)[https://docs.pytest.org/]

