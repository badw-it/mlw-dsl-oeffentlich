#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, re
import argparse
import cgi         # cgi sollte demnächst ersetzt werden, siehe: https://peps.python.org/pep-0594/#cgi
import datetime
from typing import List
import webbrowser


scriptpath = os.path.dirname(__file__)

REQUESTS_FLAG_FILE = 'requests_installation_status.txt'

TIMEOUT = 12000.0

try:
    import requests
    if os.path.exists(REQUESTS_FLAG_FILE):
        os.remove(REQUESTS_FLAG_FILE)
except (ImportError, ModuleNotFoundError):
    try:
        f = open(REQUESTS_FLAG_FILE, 'r')
        content = f.read()
        f.close()

        print('Konnte das "requests"-modul nicht installieren. Breche Verarbeitung ab...')
        sys.exit(1)
    except FileNotFoundError:
        pass

    import subprocess
    print('Auf diesem System ist "requests" bisher noch nicht installiert.')
    print('Installiere das "requests"-Module, um Kontakt mit dem '
          'Drucker-Server aufzunehmen.')
    subprocess.run([sys.executable, '-m', 'pip', 'install', '--upgrade', 'pip', 'setuptools', 'wheel'])
    subprocess.run([sys.executable, '-m', 'pip', 'install', 'requests'])
    subprocess.run([sys.executable, '-m', 'pip', 'install', 'chardet'])  # needed by requests and not always automatically installed
    with open(REQUESTS_FLAG_FILE, 'w', encoding='utf-8') as f:
        f.write('Installation des Python-Modules "requests" leider gescheitert.')
    subprocess.run([sys.executable] + sys.argv)
    sys.exit(0)


for mlwdir in ('MLW (Eckhart Arnold)', 'MLW', 'MLW-Demo (Eckhart Arnold)', 'MLW-Demo'):
    mlw_login_path = os.path.join(os.path.expanduser('~'), 'LRZ Sync+Share',
                                  mlwdir, 'Zugangsdaten')
    if os.path.exists(mlw_login_path):
        break
else:
    print('Drucken nicht möglich. Konnte den Pfad für die Zugansdaten nicht finden :-(')
    sys.exit(1)
# Achtung: Hardcodierter Passwort-Pfad! Falls das möglich ist, wäre es theoretisch
# denkbar, diesen Pfad in der tasks.json zu hinterlegen und als Parameter an das
# skript zu übergeben...

sys.path.append(mlw_login_path)
from mlw_login import mlw_login


SERVER_URL = 'badwver-itservice.srv.mwn.de:8080'

DRUCKVORSTUFE_UVZ = os.path.join("Ausgabe", "XML-Druckvorstufe")
DRUCK_UVZ = os.path.join("Ausgabe", "PDF-Druckvorschau")
# ACHTUNG: Diese Konstanten müssen immer den gleichen Wert haben wie die Konstante
#          desselben Namens in MLWCompiler.py!


def mlw_export(files,
               name='Export',
               input_type='xml',
               output_type='pdf',
               export_type='normal') -> str:
    """Upload files to MLW server, process them and download result.

    Result will usually be a PDF file. If output_type is set to
    'zip', output will contain the respective TeX-files, too.

    args:
    :files: a list paths of files to be processed
    :name: a name for the file to be generated, without extension (default: 'Export')
    :input_type: type of the input files: 'xml' (default) or 'tex'
    :output_type: type of the output file: 'pdf' (default) or 'zip'
    :export_type: 'normal' (default) or 'proof'

    returns: the path of the downloaded output file

    """
    client = requests.session()
    login(client)
    download_dir, name = os.path.split(name)
    timeout = datetime.timedelta(seconds=TIMEOUT)
    print('Schicke PDF-Auftrag an Server. Warte bis zu %s auf das Ergebnis.' % str(timeout))
    startzeit = datetime.datetime.now().replace(microsecond=0)
    response = process(client, files, name, input_type, output_type, export_type)
    stopzeit = datetime.datetime.now().replace(microsecond=0)
    dauer = stopzeit - startzeit
    print("Verarbeitungsdauer: ", dauer)
    if dauer > timeout:
        print("Höchstdauer von %s überschritten." % str(timeout))
    elif dauer > timeout - datetime.timedelta(seconds=TIMEOUT / 100.0):
        print("Höchstdauer von %s annährend überschritten." % str(timeout))
    if response is None:
        return ''
    if response.status_code != 200:
        with open('response_error.html', 'w', encoding='utf-8') as f:
            f.write('Fehler beim generieren der Druck-PDFs für folgende Dateien:\n')
            f.write('\n'.join(files))
            f.write('\n\n')
            f.write(response.text)
        print('Fehler beim Abruf des Druck-PDF:')
        print('Versuche ggf. die Druckausgabe direkt mit der Maske auf: '
              'http://badwver-itservice.srv.mwn.de:8080/mlw/export zu generieren.')
        # print(response.text)
        import sys
        if scriptpath not in sys.path:
            sys.path.append(scriptpath)
        # from MLWFehlerbericht import Sende_Fehlerbericht_mit_Outlook
        # Sende_Fehlerbericht_mit_Outlook('response_error.html', files)
        raise IOError('Druck-PDF wurde nicht vom Server zurückgeliefert. Fehler: ' + str(response.status_code))
    # download_dir = None
    download_path = write_file(response, download_dir, name)
    if download_path.endswith('error.html'):
        webbrowser.open(download_path)
        raise IOError('Datei konnte nicht auf dem Server verarbeitet werden!')
    return download_path


def login(client):
    url = 'http://' + SERVER_URL + '/accounts/login/'
    print('Logging into: ' + url)
    client.get(url)
    csrftoken = client.cookies['csrftoken']
    username = mlw_login['username']
    password = mlw_login['password']
    data = {
        'username': username,
        'password': password,
        'csrfmiddlewaretoken': csrftoken,
    }
    client.post(url, data=data)

    
def process(client, files, name, input_type, output_type, export_type):
    url = 'http://' + SERVER_URL + '/mlw/export'
    print('Sending %s\nto: %s    (input: %s, output: %s, export: %s)'
          % (str(files), url, input_type, output_type, export_type))
    client.get(url)
    csrftoken = client.cookies['csrftoken']
    data = {
        'name': name,
        'input_type': input_type,
        'output_type': output_type,
        'export_type': export_type,
        'csrfmiddlewaretoken': csrftoken,
    }
    files = [
        ('files', open(file_path, 'rb'))
        for file_path in files
    ]
    try:
        response = client.post(
            url,
            data=data,
            files=files,
            timeout=TIMEOUT,
        )
    except requests.exceptions.ConnectionError as e:
        print(e)
        response = None
    except requests.exceptions.ReadTimeout as e:
        print(e)
        response = None
    return response


def write_file(response, download_dir=None, filename=''):
    try:
        # cgi sollte demnächst ersetzt werden, siehe: https://peps.python.org/pep-0594/#cgi
        header_params = cgi.parse_header(response.headers['Content-Disposition'])
        if not filename:
            filename = header_params[1]['filename']
        # print(header_params)
    except KeyError:
        filename = "error.html"
    if download_dir:
        if not os.path.isdir(download_dir):
            os.mkdir(download_dir)
        download_path = os.path.join(download_dir, filename)
    else:
        download_path = filename
    try:
        with open(download_path, 'wb') as download_file:
            for chunk in response:
                download_file.write(chunk)
    except PermissionError:
        raise PermissionError(
            f'Die Datei "{download_path}" konnte nicht angelegt werden. '
            'Möglicherweise existiert eine Datei dieses Namens bereits und '
            'ist in einem anderen Program noch geöffnet.')
    return download_path



# Vorverarbeitung: Dateisuche etc.


def sammele_mlw_dateien(verzeichnis):
    """Gibt eine Liste aller zu Datein mit der Endung '.mlw' im angegebenen
    Verzeichnis zurück. """
    mlw_dateien = []
    for f in os.listdir(verzeichnis):
        if f.lower().endswith('.mlw'):
            mlw_dateien.append(os.path.join(verzeichnis, f))
    mlw_dateien.sort(key=lambda k: k.lower())
    return mlw_dateien


def split_all_exts(file_name: str) -> str:
    """Schneidet alle Erweiterungen eines Dateinamens ab, also bei
    iocus.ausgabe.xml wird .ausgabe.xml abgeschnitten und nicht bloß .xml.
    """
    dir_name, file_name = os.path.split(file_name)
    while True:
        file_name, ext = os.path.splitext(file_name)
        if not ext:
            break
    return os.path.join(dir_name, file_name)


def zu_aktualisierende(mlw_dateien: List[str], ziel: str=DRUCKVORSTUFE_UVZ) -> List[str]:
    """Prüft welche der in der Liste `mlw_dateien` mit ihrem Namen angegebenen Dateien
    aktualisierungsbedürftig sind und gibt eine Liste nur dieser Dateien zurück.

    `ziel` ist der Verzeichnisname desjenigen Zielverzeichnisses, das zur Prüfung heran
    gezogen werden soll.

    Wird als Ziel z.B. `DRUCK_UVZ` angegeben, dann werden
    alle Artikel neu übersetzt und(!) eine Druckvorschau generiert, von denen keine
    Druckvorschau existiert, deren Dateidatum jünger als das der Quelldatei ist.
    """
    # basis_verzeichnis = os.path.dirname(mlw_dateien[0])
    # if basis_verzeichnis.endswith(ziel):
    #     return []
    #
    # ziel = os.path.join(basis_verzeichnis, ziel)
    #
    # if not os.path.exists(ziel):
    #     from MLWCompiler import erzeuge_Ausgabe_Verzeichnisse
    #     erzeuge_Ausgabe_Verzeichnisse(os.path.dirname(mlw_dateien[0]),
    #                                   os.path.splitext(os.path.basename(mlw_dateien[0]))[0])
    #
    # ziel_dateien = {split_all_exts(entry): os.path.join(ziel, entry)
    #                 for entry in os.listdir(ziel) if os.path.isfile(os.path.join(ziel, entry))}
    #
    # veraltet = []
    # for quellen_pfad in mlw_dateien:
    #     quelle = split_all_exts(os.path.basename(quellen_pfad))
    #     print(quelle, quellen_pfad, ziel_dateien.get(quelle, '?'))
    #     if quelle not in ziel_dateien \
    #         or os.path.getmtime(ziel_dateien[quelle]) < os.path.getmtime(quellen_pfad):
    #             veraltet.append(quellen_pfad)
    #
    # return veraltet
    return mlw_dateien


def erzeuge_xml(mlw_dateien) -> List[str]:
    """Prüft, ob zu den übergebenen .mlw-Dateien aktuelle Ausgabe-XML-Dateien
    vorhanden sind. Wo nicht, werden sie erzeugt.
    """
    ohne_xml = zu_aktualisierende(mlw_dateien)
    lemmata = dict()

    if ohne_xml:
        from MLWCompiler import verarbeite_mlw_artikel, evaluiere
        from DHParser.configuration import set_config_value
        zusammenfassung = []
        for pfad in ohne_xml:
            set_config_value('xml_attribute_error_handling', 'lxml')
            status, meldungen, html_baum = verarbeite_mlw_artikel(pfad, 'LOGS', {'xml', 'ausgabe.xml'})
            neue_lemmata = html_baum.get_attr('lemmata', '').split(',')
            for lemma in neue_lemmata:
                if lemma in lemmata and pfad != lemmata[lemma]:
                    print(f"WARNUNG: Lemma {lemma} in {pfad} wurde bereits in {lemmata[lemma]} beschrieben!?")
                lemmata[lemma] = pfad
            zusammenfassung.append('{:<60} {}'.format(pfad[-60:], evaluiere(status, meldungen)))
            for m in meldungen:
                print(m)
        print('\nBilanz:')
        print('\n'.join(zusammenfassung))
    return list(lemmata.keys())


def pruefe_lxml_kompatibilitaet(xml_dateien):
    try:
        import lxml.etree as ET
    except ImportError:
        print('Could not import lxml to check xml-compatibility, trying ETree instead')
        import xml.etree.ElementTree as ET

    for pfad in xml_dateien:
        print('Prüfe:', pfad)
        try:
            tree = ET.parse(pfad)
            _ = tree.getroot()
            print('Ausgabe-XML lxml-Kompatibilität: OK')
        except ET.ParseError as err:
            print('    XML-ERROR:', err)


def hauptverzeichnis(verzeichnis) -> str:
    """Liefert das Verzeichnis mit den MLW-Datein zurück, falls Verzeichnis eins
    der Ausgabe-Unterverzeichnisse ist. Dadurch kann das Skript auch dann Ergebnisse
    """
    i = verzeichnis.find('Ausgabe/')
    if i < 0:
        i = verzeichnis.find('Ausgabe\\')
    if i >= 0:
        verzeichnis = verzeichnis[:i]
    return verzeichnis


def open_pdf(filename):
    if sys.platform == "darwin":
        import subprocess
        subprocess.run(["open", f'{filename}'])
    else:
        webbrowser.open(filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate Print-Preview for .mlw-files')
    parser.add_argument('files', nargs='*')
    parser.add_argument('-n', '--name',
                        dest='name',
                        help='name of the export',
                        default='Export',
    )
    parser.add_argument('-i', '--input',
                        dest='input_type',
                        help='input type: "xml" or "tex"',
                        choices=['xml', 'tex'],
                        default='xml',
    )
    parser.add_argument('-o', '--output',
                        dest='output_type',
                        help='output type: "pdf" or "zip"',
                        choices=['pdf', 'zip'],
                        default='pdf',
    )
    parser.add_argument('-e', '--export',
                        dest='export_type',
                        help='export type: "normal" or "proof"',
                        choices=['normal', 'proof'],
                        default='normal',
    )
    args = parser.parse_args()

    if len(args.files) == 0:
        args.files.append(os.path.join('Beispiele', 'gymnus', 'gymnus.mlw'))

    if len(args.files) == 1:
        if not os.path.exists(args.files[0]):
            print(f'Datei oder Verzeichnis "{args.files[0]}" existiert nicht!')
            sys.exit(0)
        if os.path.isdir(args.files[0]):
            verzeichnis = hauptverzeichnis(args.files[0])
            mlw_dateien = sammele_mlw_dateien(verzeichnis)
        else:
            verzeichnis = hauptverzeichnis(os.path.dirname(args.files[0]))
            mlw_dateien = args.files
            mlw_dateien.sort(key=lambda k: k.lower())
    else:
        # Es können mehrere Dateinamen, aber es darf nur höchstens ein Verzeichnis
        # und dann nur das Verzeichnis und keine Dateinamen angegeben werden
        mlw_dateien = args.files
        mlw_dateien.sort(key=lambda k: k.lower())
        assert all(os.path.isfile(pfad) for pfad in mlw_dateien), str(mlw_dateien)
        verzeichnis = hauptverzeichnis(os.path.dirname(mlw_dateien[0]))
        assert all(os.path.dirname(pfad) == verzeichnis for pfad in mlw_dateien[1:])

    if not mlw_dateien:
        print('Keine .mlw-Dateien im Verzeichnis "%s" gefunden!' % verzeichnis)
        sys.exit(1)

    xml_dateien = [os.path.join(verzeichnis, datei)
                   for datei in mlw_dateien if datei.endswith('.xml')]
    mlw_dateien = [datei for datei in mlw_dateien if datei.endswith('.mlw')]
    lemmata = erzeuge_xml(mlw_dateien)

    xml_dateien += [os.path.join(verzeichnis, DRUCKVORSTUFE_UVZ,
                                 os.path.basename(split_all_exts(pfad) + '.ausgabe.xml'))
                    for pfad in lemmata if pfad]

    if args.name == 'Export':
        # falls Vorgabe-name, finde einen sprechenderen Namen
        export_name = split_all_exts(os.path.basename(lemmata[0] if lemmata else xml_dateien[0]))
        if len(mlw_dateien) > 1:
            vz = os.path.basename(os.path.abspath(verzeichnis).rstrip('/'))
            if vz:
                export_name = vz
    else:
        export_name = args.name

    pruefe_lxml_kompatibilitaet(xml_dateien)

    export_pfad = os.path.join(verzeichnis, DRUCK_UVZ, export_name)
    if args.export_type == 'proof':
        export_pfad += "_Fahnenkorrektur.pdf"
    else:
        export_pfad += "_Druckvorlage.pdf"

    print("Erzeuge " + export_pfad)
    print("Das kann jetzt etwas dauern...")

    mlw_export(
        xml_dateien,
        export_pfad,
        args.input_type,
        args.output_type,
        args.export_type
    )

    print('Öffne ' + export_pfad)
    open_pdf(export_pfad)

    import MLWVersion
    print('\n' + str(MLWVersion.VERSION))
