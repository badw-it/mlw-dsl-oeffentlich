#!/usr/bin/sh

lowdown -s -T html MLW_Notation_Beschreibung.md >MLW_Notation_Beschreibung.html
wkhtmltopdf -B 15mm -T 15mm -L 10mm -R 10mm \
-q -s A4 MLW_Notation_Beschreibung.html MLW_Notation_Beschreibung.pdf
cp MLW_Notation_Beschreibung.* ~/LRZ\ Sync+Share/MLW\ \(Eckhart\ Arnold\)/
