#!/usr/bin/env python3

"""RealseTest.py - Release-Test für das MLW"""


import os, subprocess


def release_test():
    # MLWCompiler - Einzelartikel
    result = subprocess.run(['python3', 'MLWCompiler.py', os.path.join('Beispiele', 'sandix.mlw')])
    assert result.returncode == 0, str(result.returncode)
    # MLWCompiler - Artikelstrecke
    result = subprocess.run(['python3', 'MLWCompiler.py', os.path.join('Beispiele', 'Faszikel_52')])
    assert result.returncode == 0, str(result.returncode)

    # MLWPrint - Einzelartikel
    result = subprocess.run(['python3', 'MLWPrint.py', os.path.join('Beispiele', 'sandix.mlw')])
    assert result.returncode == 0, str(result.returncode)
    # MLWPrint - Artikelstrecke
    result = subprocess.run(['python3', 'MLWPrint.py', os.path.join('Beispiele', 'Faszikel_52')])
    assert result.returncode == 0, str(result.returncode)

    # MLWServer
    result = subprocess.run(['python3', 'MLWServer.py', os.path.join('Beispiele', 'sandix.mlw')])
    assert result.returncode == 0, str(result.returncode)
    result = subprocess.run(['python3', 'MLWServer.py', '--stopserver'])
    assert result.returncode == 0, str(result.returncode)

    print("Release-Test erfolgreich!")


if __name__ == '__main__':
    release_test()

