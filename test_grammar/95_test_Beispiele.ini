[match:LemmaPosition]
M1: """
    LEMMA *facitergula
      (fasc-, -iet-, -ist-, -rcu- {sim.})"""


[match:GrammatikPosition]
M1: """
    GRAMMATIK
        adiectivum I-II; -a, -um"""

M2: """
    GRAMMATIK
      subst. I; -ae f.
      -us, -i m.: {=> faceterculi}
      -um, -i n.: {=> facitergulum}"""


[match:EtymologiePosition]
M1: """
    ETYMOLOGIE
      gr. γυμνός"""


[match:SchreibweisenPosition]
M1: """
    SCHREIBWEISE
      script.:
        hym-: {=> hymperium_1} {=> hymperium_2}
        em-: CHRON. Fred.;  2,35sqq. capit.;  p. 43.;  2,36 p. 60,10.
        ym-: CHART. Sangall.;  A 194.
        impir-: {=> impirium_1}"""


M2: """
    SCHREIBWEISE
      script.:
        f- : BENED. ANDR. chron.; p. 113,3.

      superl.:
        him(us):  {=> himus_1}

        (h)ymm(us): DHUODA lib. man.; 4,1,61.

        ym(us): * DHUODA lib. man.; 3,7,10 {(var. l.)}
                * VITA Liutb.; 18 p. 23,2.

        imm(us): {=> immus_1} {=> immus_2}
                 * {adde}
                   * DHUODA lib. man.; 3,7,10.
                   * CARM. de Bened.; 517."""


[match:StrukturPosition]
M1: """
    STRUKTUR
      form. sing.:
        gen.:
          -ri: {=> imperator39m_1} * {adde} ANNAL. PLAC.; a. 1266 p. 516,21.
          -iae: CHRON. Fred.; 2,33.; p. 56,22. 2,35.
        abl.:
          -um: CHRON. Fred.; 2,15. 2,35sqq. capit.; p. 43."""


[match:GebrauchsPosition]
M1: """
    GEBRAUCH
         usu intrans.: {=> ziel10} {fort. add. {=> ziel11}}
         partic. perf. usu adi.: {=> ziel12}"""


[match:MetrikPosition]
M1: """
    METRIK
        metr.:  // TODO: Kein Doppelpunkt und kein Spatium wenn nur eine Variante angegeben
            ín-: {=> p. 10.18|ziel_3}"""


[match:VerwechselungsPosition]
M1: """
    VERWECHSELBAR
        confunditur c.:
            imperitus: {=> imperator80m_1}"""


[match:Verweise]
M1: "{=> hymperium_1}"
M2: "{=> imperium/hymperium_1}"
M3: "{=> Verweis noch offen | imperium/hymperium_1}"
M4: "{=> p. 12, 3.|-}"
M5: "{=> imperium/IMPERIUM}"
M6: "{=> imperium im TLL | https://tll.badw.de/wb/imperium}"

[match:Anker]
M1: "{@ hymperium_1}"


[match:Quellenangabe]
M1: "DHUODA lib. man.; 3,7,10 {(var. l.)}"


[match:Bedeutung]
M1: """
    BEDEUTUNG augere, amplificare -- vermehren, vergrößern:

      * RUD FULD. Leob.; 14 "tenebrarum ... densitas et continua
        corusactionum per fenestras irruptio maximum timidis
        horrorem #ingeminant."

      * THANGM. Bernw.; 28 p. 771,40 "horribilis strepitus #ingeminatur."

      * ALBERT. M. summ. theol. II; 7,28,3 p. 313^a,43 "multa
        mala #ingeminantur alicui, ut {eqs}." {al.}"""


[match:Bedeutungsangabe]
M1: "praematurus -- vorzeitig:"
M2: "inconditus -- ungereimt, unschön {(usu rhet.)}:"
M3: "spectat ad actus hominum:"
M4: """corripere, aggravare -- befallen, plagen, quälen;
       {mediopass. i. q.} laborare -- leiden (an);
       absol. vel ellipt.: {=> infesto_1}:"""
M5: """impedire -- hindern;
       {absol. vel ellipt. i. q.} oppositum esse --
       hinderlich sein, entgegenstehen:"""
M6: """corripere, aggravare -- befallen, plagen, quälen;
    {mediopass. i. q.} laborare -- leiden (an); absol. vel ellipt.: {=> infesto_1}:"""


[match:Belege]
M1: """* BERNOLD. CONST. chron.; p. 400,2 (ed. Pertz) "sedem apostolicam tirannice
         invasit eamque ... plures annos #infestavit {Guibertus} ((
           * {inde} ANNAL. Gottwic.; a. 1085 ((MGScript. IX; p. 601,38))
           * {sim.} BERNOLD. CONST. chron.; a. 1087 p. 463,16 ((ed. Robinson)) ))" """

M2: """* GLOSS. med.; p. 44,18sq. "qui (maniaci) ... taciti sunt, humore #infest|antur
         frigido et sicco"

       * {eqs.} TRACT. de aegr. cur.; p. 131,6 "dolor {(sc. capitis)} ... est
         continuus et ante prandium et post equaliter #infestat"; p. 163,23 "si
         aliquis dolor et passio ... aures #infest|averit" """

M3: """* WILH. SALIC. chirurg.; 2,2 p. 327^B "omnia hec {(sc. curationes)}
         facienda sunt in prima visitatione infirmi, nisi ...
         {!fluxus sanguinis} #infestaret (({sim.} 2,3 p. 328^B "nisi
         f. s. te #infestet"; 2,16 p. 337^C "in fluxu s., si te #infestet
         in prima visitatione."; 2,17 p. 337^D "nisi s. fluxu #infesteris.";
         {ibid. al. cf.} 2,4 p. 329^B "nisi ... te impediat."; 2,5 p. 329^G
         "nisi ... divertaris."; 2,9 p. 332^E "nisi ... molesteris." {sim.}))" """
M4: """* ALBERT. METT. div. temp.; 1,1 "cuius {(Wicmanni)} maiores magnam partem
    Germanie et maxime circa littora Oceani #imperio (#imperia {var. l.})
    tenebant." """

M5: """* ALBERT. M. mort.; 2,7 p. 363^a,25  "{!calor} modicus #intrane.|us
         ((
           * animal.; 16,115
             "'venae ... et ossa ex <<calore #intern.|o>>
             ((
               * ARIST. animal.; p. 743^a,17 "τῆς ἐντὸς θερμὸτητος"
              ))
             desiccantur'."

           * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intern.|um"
         ))"

       * mot. proc.; 1,1 p. 48,54 "tactus ... organum perfectum ... est #intern.|um et est cor
         ((
           * {sim.} animal.; 12,173 "in duobus ... sensibus, qui per medium #intern.|um
             accipiunt sensibilia {eqs.}"
         ))
         {ibid. al.}" """

M6: """* PETR. Dam. epist.;
       {=> 10 p. 131,10|http://mgh.de/dmgh/resolving/MGH_Briefe_d._dt._Kaiserzeit_4,1_S._131} """


[fail:Belege]
F1: """* GLOSS. med.; p. 44,18sq. "qui (maniaci) ... taciti sunt, humore #infest|antur
       frigido et sicco"; """
F2: """* GLOSS. med.; p. 44,18sq. "qui (maniaci) ... taciti sunt, humore #infest|antur
       frigido et sicco". """


[match:BelegLemma]
M1: """#inepto"""
M2: """#inep|to"""

[match:BelegText]
M1: '"‘{#ha! ha! he!}’"'
M2: '"#gh"'
M3: '"{#h}a"'
