#!/usr/bin/python3

"""
test_MLWOuput.py -- testet die Druck-XML und HTML-Ausgabe von MLWCompiler.py
"""

import copy
import sys
from collections.abc import Sequence

sys.path.extend(['../', '../DHParser'])

from MLWParser import WARNUNG_DATIERUNG_ALS_STELLENANGABE
from MLWCompiler import lemma_verdichtung, Verdichtungsfehler, \
    MLWCompiler, AusgabeTransformation, get_Ausgabe_Transformation, kompiliere_schnipsel, \
    HTMLTransformation, FEHLER_AUTOR_FEHLT, get_grammar, get_transformer, \
    get_HTML_Transformation, get_Druck_Transformation, \
    WARNUNG_EINSCHUB_ERWARTET, FEHLER_DOPPELTER_ANKER, WARNUNG_REDUNDANTER_STELLENABSCHNITT, \
    WARNUNG_FALSCHE_AUTORANGABE, FEHLER_STELLENANGABE_NICHT_ABGETRENNT, \
    WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE, TextÄnderungenMixin, KLAMMERN_FETT_SCHWELLE, \
    MLWLinter, WARNUNG_FALSCH_EINGEORDNETER_ZUSATZ, WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT, \
    HINWEIS_LEMMAWORT_OHNE_VERDICHTUNG, WARNUNG_EDITIONSANGABE_OHNE_DOPPELKLAMMERN, \
    FEHLER_STELLENANGABE_NICHT_ABGETRENNT, FEHLER_FOLGE_USU_OHNE_VORGÄNGER
from DHParser.error import is_error, ERROR, MANDATORY_CONTINUATION_AT_EOF, \
    MANDATORY_CONTINUATION_AT_EOF_NON_ROOT
from DHParser.compile import process_tree
from DHParser.nodetree import parse_sxpr, parse_xml, flatten_sxpr, RootNode, Node, EMPTY_NODE, \
    has_token, add_token, eq_tokens, LEAF_NODE, ANY_NODE, LEAF_PATH, ANY_PATH, \
    ContentMapping, has_class, pick_from_path
# from DHParser.transform import delimit_children, node_maker
from DHParser.toolkit import re, matching_brackets


class TestMLWHTML:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.mlw_verification = MLWLinter()
        self.mlw_output = get_Ausgabe_Transformation()
        self.mlw_preview = get_HTML_Transformation()
        # self.mlw_print = get_Druck_Transformation()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        self.mlw_compiler(st)
        return self.mlw_verification(st)

    def generate_html(self, src, root_parser) -> RootNode:
        st = self.compile(src, root_parser)
        st = self.mlw_output(st)
        st = self.mlw_preview(st)
        return st

    def test_grosse_Klammern_fett(self):
        belegtext = '''"virgam pastoralem et anulum per symoniacam hęresim tradidit {(sc. rex)};
    ... de eo #sacrilegi.|o ad te {(sc. patrem Romanae sedis)} scripsimus {(sc. episcopus Andegavensis)}
    (({sim.} * GREG. VII. registr.; 3,1 p. 244,4 "anathemate percutimus ... premaxime illos, qui ... ab eo
    {[sc. Herimanno episcopo]} aliquid in prediis vel in thesauris ecclesię acceperunt ad confovendum eum,
    ut in sua pertinacia perseveret, nisi resipuerint, videlicet ut condigna satisfactione, quod predicto
    #sacrilegi.|o peccaverunt, emendent."; 9,29 p. 613,18 "#sacrilegi.|a symoniacę heresis pestilentię
    ac proditiones"))."'''
        rn = self.generate_html(belegtext, 'BelegText')
        content = rn.content
        bracket_pairs = matching_brackets(content, '(', ')', []) \
                        + matching_brackets(content, '[', ']', [])
        cm = ContentMapping(rn)

        def ist_fett(ctx) -> bool:
            return ctx[-1].name == "Fett" or ctx[-1].get_attr('class', '').find('fett') >= 0

        for a, b in bracket_pairs:
            ctx_a, _ = cm.get_path_and_offset(a)
            ctx_b, _ = cm.get_path_and_offset(b)
            if b - a >= KLAMMERN_FETT_SCHWELLE:
                assert ist_fett(ctx_a)
                assert ist_fett(ctx_b)
            else:
                assert not ist_fett(ctx_a)
                assert not ist_fett(ctx_b)



class TestMLWPrint:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.mlw_verification = MLWLinter()
        self.mlw_output = get_Ausgabe_Transformation()
        self.mlw_preview = get_HTML_Transformation()
        self.mlw_print = get_Druck_Transformation()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        self.mlw_compiler(st)
        return self.mlw_verification(st)

    def generate_print_mlw(self, src, root_parser) -> RootNode:
        st = self.compile(src, root_parser)
        st = self.mlw_output(st)
        st = self.mlw_print(st)
        return st

    def generate_html(self, src, root_parser) -> RootNode:
        st = self.compile(src, root_parser)
        st = self.mlw_output(st)
        st = self.mlw_preview(st)
        return st

    def gen_print_and_html(self, src, root_parser) -> tuple[RootNode, RootNode, RootNode]:
        st = self.compile(src, root_parser)
        output_tree = self.mlw_output(st)
        html_tree = self.mlw_preview(copy.deepcopy(output_tree))
        print_tree = self.mlw_print(copy.deepcopy(output_tree))
        return print_tree, html_tree, output_tree

    def expect_warning(self, src: str, root_parser, warning_code, show_tree=False):
        st = self.compile(src, root_parser)
        if show_tree:  print(st.as_sxpr())
        if isinstance(warning_code, Sequence):
            error_codes = [e.code for e in st.errors_sorted]
            i = -1
            for code in warning_code:
                try:
                    k = error_codes.index(code)
                except IndexError:
                    assert False, f"Warnung oder Fehler {code} erwartet aber nicht erhalten!"
                assert k > i, f"Warnung oder Fehler {code} vor {error_codes[i]} erwartet, nicht danach!"
                i = k
        else:
            error_codes = {e.code for e in st.errors} - {1030, 1040}
            if warning_code == 0:
                assert not error_codes, f'Keine Warnungen oder Fehler erwartet, aber {error_codes} erhalten:' + str(st.errors)
            elif warning_code < 0:
                assert error_codes, 'Warnung oder Fehler erwartet, aber keine gefunden!'
            elif warning_code not in error_codes:
                assert False, f'Warnung oder Fehler {warning_code} erwartet, aber {error_codes} erhalten!'

    def test_Internet_Link(self):
        src = '''NACHTRAG post p. 443,59:
            LEMMA *celsor
            GRAMMATIK subst. III; -oris m.
            ETYMOLOGIE
                celsus, {cf.} ?celsuris:
                 * {v.} ThLL.; {=> s. v. | https://tll-1degruyter-1com-1001361hu06b3.emedia1.bsb-muenchen.de/article/3_0_4_celsuris_v2007?currentIndex=1&search_url=%252Fresults%253Ftype%253D0%2526field0%253D0%2526val0%253Dcelsuris%2526searchId%253Dc}
            BEDEUTUNG
                celsitas -- Höhe:
                * FORM. Salisb. II; 3,14 "#celsor.|e subruente poli (({v. comm. ed. p. 21}))."
            AUTORIN Pörnbacher'''
        print_tree, html, output = self.gen_print_and_html(src, 'Artikel')
        link = print_tree.pick(lambda nd: nd.has_attr('href'))
        assert link.content.strip() == 's. v.'
        assert link.name == 'Text'
        link = html.pick('a')
        assert link.content.strip() == 's. v.'

    def test_links_fette_Klammern(self):
        belege = '''* LEX Sal. Merov.; 2,16 "si quis maialem #sacriv.|um 
        (#sacri.|bo{@sacrivus_5}{=>Wolfenbüttel, Herzog August Bibliothek, Cod. Guelf. 97 Weiss., f. 7^r | http://diglib.hab.de/mss/97-weiss/start.htm?image=00017}, 
        votivo{=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 4404, f. 181^r | https://gallica.bnf.fr/ark:/12148/btv1b8426042t/f375.item}, 
        vodivo{=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 9653, f. 22^v | https://gallica.bnf.fr/ark:/12148/btv1b10545842k/f52.item} {var. l.}) 
        furaverit et hoc ..., quod #sacriv.|us (#sacriv.|um{=>München, Bayerische Staatsbibliothek, Clm 4115, f. 45^r | https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00060127/canvas/95/view}, 
        #sacri.|bus{@sacrivus_3}{=>Wolfenbüttel, Herzog August Bibliothek, Cod. Guelf. 97 Weiss., f. 7^r | http://diglib.hab.de/mss/97-weiss/start.htm?image=00017}, votivus{=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 4404, f. 181^r | https://gallica.bnf.fr/ark:/12148/btv1b8426042t/f375.item},
        {=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 9653, f. 22^v | https://gallica.bnf.fr/ark:/12148/btv1b10545842k/f52.item} {var. l.}) 
        fuit, potuerit adprobare {eqs.} (({inde} *LEX Sal. Pipp.; 2,6 {=>http://www.mgh.de/dmgh/resolving/MGH_LL_nat._Germ._4,2_S._31} *LEX Sal. Karol.; 2,14 {=>http://www.mgh.de/dmgh/resolving/MGH_LL_nat._Germ._4,1_S._27}))."; 
        2,17 "de alio maiale, qui #sacriv.|us 
        (#sacri.|fus{@sacrivus_4}{=>Wolfenbüttel, Herzog August Bibliothek, Cod. Guelf. 97 Weiss., f. 7^r | http://diglib.hab.de/mss/97-weiss/start.htm?image=00017}, 
        votivus{=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 4404, f. 181^r | https://gallica.bnf.fr/ark:/12148/btv1b8426042t/f375.item},
        {=>Paris, Bibliothèque Nationale de France, Département des Manuscrits, Latin 9653, f. 22^v | https://gallica.bnf.fr/ark:/12148/btv1b10545842k/f52.item} {var. l.}) 
        non fuit (({sim.} *LEX Sal. Pipp.; 2,7 
        ((rec. E{=>http://www.mgh.de/dmgh/resolving/MGH_LL_nat._Germ._4,1_S._X})) "maialem non #sag|.riv.|um {@sacrivus_1}." 
        
        * LEX Sal. Karol.; 2,15 {=>http://www.mgh.de/dmgh/resolving/MGH_LL_nat._Germ._4,1_S._27}))." '''
        print_tree, html, output = self.gen_print_and_html(belege, 'Belege')
        assert not print_tree.errors
        assert not html.errors
        assert not print_tree.pick(lambda nd: has_class(nd, 'fett'))
        assert html.pick(lambda nd: has_class(nd, 'fett'))

    def test_lange_AusgangslemmataListe(self):
        verweisartikel = '''LEMMA  *sculthacius, *scul(haisus), *sculthazus, *sculthedus, *sculthetus, *sculteitus {sim.} VIDE *scultetus
        
        AUTOR  Fiedler'''
        print_tree, html, output = self.gen_print_and_html(verweisartikel, 'VerweisArtikel')
        al = print_tree.pick('AusgangsLemmata')
        assert al.pick('P')

        verweisartikel = '''LEMMA  scumunico VIDE excommunico
        LEMMA  [scunarium VIDE sanctuarium]
        LEMMA  [scunia VIDE ischuria]
        LEMMA  *scuobassa, *scuopaza, * scuopusca {sim.} VIDE *sch(u)opoza
        LEMMA  *scupha VIDE *schuppa
        LEMMA  scuphiae, scuphium VIDE excubiae
        LEMMA  scupo VIDE excubo
        LEMMA  *scupoza VIDE *sch(u)opoza
        
        AUTOR  Fiedler'''
        print_tree, html, output = self.gen_print_and_html(verweisartikel, 'VerweisArtikel')
        p = list(print_tree.select_children('P'))
        assert len(p) >= 4

    def test_lange_Ausgangslemmaliste(self):
        artikel = '''LEMMA  *sculthacius, *scul(haisus), *sculthazus, *sculthedus, *sculthetus, *sculteitus {sim.} VIDE *scultetus
        LEMMA  scultile VIDE sculptilis
        LEMMA  sculto VIDE ausculto
        LEMMA  scultus VIDE sculpo
        LEMMA  *sculus VIDE scolex
        
        AUTOR  Fiedler'''
        print_tree, html, output = self.gen_print_and_html(artikel, 'Artikel')
        # print(print_tree.as_sxpr())

    def test_SpatienPosition(self):
        bedeutung = '''
        BEDEUTUNG
        de #sigill.|o adhibito ad claudendum, sigillandum, confirmandum varias res:
            AUFZÄHLUNG portas: 
                {=>sigillo/sigillum_02}
            AUFZÄHLUNG res pretiosas (monetam, aurum):
                * EINH. Karol.; 33 p. 39,8 ((testam.)) "sub #sigill.|o reconditis ({sc. partibus largitionis})."'''
        print_tree, html, output = self.gen_print_and_html(bedeutung, 'Bedeutung')
        pfad = print_tree.pick_path('Verweise')
        # print(pfad[-2].as_sxpr())
        assert pfad[-1].children[-1].name != 'L'
        i = pfad[-2].index(pfad[-1])
        # assert pfad[-2][i + 1].name == 'L'
        # geändert, da Text aus verweisen hinausgeschoben wird.
        assert pfad[-2][-2].name == 'TEXT'
        assert pfad[-2][-1].name == 'L'

    def test_Zusaetze_in_Verweisbloecken(self):
        belege = '''{=> sedeo_38{sqq.}}{=> sedeo_39}{=> sedeo_40}'''
        print_tree, html, output = self.gen_print_and_html(belege, 'Belege')
        # print(print_tree.as_sxpr())
        for verw in print_tree.select('Verweise'):
            assert verw.children
            assert 'Zusatz' not in verw
            assert verw.children[0].name != 'L'
            assert verw.children[-1].name != 'L'
        assert print_tree.pick('Zusatz')

    def test_usu_Sonderfall(self):
        # Ticket 474
        gebrauch = """
        GEBRAUCH
            usu absol.: {=> sequor04; sequor17; sequor26; sequor35; sequor43; sequor48; sequor49; sequor60} {adde} * CAND. FULD. Eigil. II; 17,16. {al.}
                 //ellipt.:  {=> sequor44} 
                [usu] pass.: {=> sequor09; sequor30} {adde} * LIOS MON.; 556 /*[p. 294](s. IX.)*/
                [usu] impers.: {=> sequor35; sequor43; sequor51; sequor58} {al.}
                in coniug. periphrastica: {=> sequor38; sequor45} {adde} * ANNAL. Ianuens.; I p. 82,15. III p. 153,30. {al.}
                partic.:
                    praes. usu subst.: {=> sequor67}
                    fut. usu adi.: {=> sequor61}
                    [usu] subst.: {=> sequor68}"""
        print_tree, html, output = self.gen_print_and_html(gebrauch, "GebrauchsPosition")
        assert not any(e.code == FEHLER_FOLGE_USU_OHNE_VORGÄNGER for e in output.errors)
        # beseitige Fehlermeldungen und Warnungen
        tree = Node(output.name, output.result)
        assert str(tree).find("partic.:    praes. usu subst.:  ⇒😯.    fut. usu adi.:  ⇒😯; subst.:  ⇒😯. ") >= 0


if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
