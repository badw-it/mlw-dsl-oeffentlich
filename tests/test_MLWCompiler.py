#!/usr/bin/python3

"""
test_MLWCompiler.py -- testet verschiedene Eigenschaften von
MLWCompiler.py, die nicht schon durch die Grammatik-Tests in
'grammar_test' abgedeckt sind. Dazu gehören insbesondere die
weiteren Verarbeitungsschritte nach der Erstellung des
Abstract-Syntax-Tree (AST), sowie bestimmte spezielle Algorithmen.
"""

import copy
import sys

sys.path.extend(['../', '../DHParser'])

from MLWCompiler import lemma_verdichtung, Verdichtungsfehler, \
    MLWCompiler, AusgabeTransformation, get_Ausgabe_Transformation, kompiliere_schnipsel, \
    HTMLTransformation, FEHLER_AUTOR_FEHLT, get_grammar, get_transformer, \
    WARNUNG_EINSCHUB_ERWARTET, FEHLER_DOPPELTER_ANKER, WARNUNG_REDUNDANTER_STELLENABSCHNITT, \
    WARNUNG_FALSCHE_AUTORANGABE, FEHLER_STELLENANGABE_NICHT_ABGETRENNT, \
    WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE, TextÄnderungenMixin, KLAMMERN_FETT_SCHWELLE, \
    WARNUNG_OP_CIT_NICHT_ZUZUORDNEN, HINWEIS_DISAMIGUIRUNG_DER_AUTORANGABE
from DHParser.error import is_error, ERROR, MANDATORY_CONTINUATION_AT_EOF, \
    MANDATORY_CONTINUATION_AT_EOF_NON_ROOT
from DHParser.compile import process_tree
from DHParser.nodetree import parse_sxpr, parse_xml, flatten_sxpr, RootNode, Node, EMPTY_NODE, \
    has_token, add_token, eq_tokens, LEAF_NODE, ANY_NODE, LEAF_PATH, ANY_PATH, \
    has_class, pick_from_path
# from DHParser.transform import delimit_children, node_maker
from DHParser.toolkit import re, matching_brackets
from DHParser import testing

POSSIBLE_ARTIFACTS = set(testing.POSSIBLE_ARTIFACTS) | {WARNUNG_OP_CIT_NICHT_ZUZUORDNEN}


class TestLemmaVerdichtung:
    # def test_automatische_verdichtung(self):
    #     tests = [('ineptus', 'inept|e', '-e'),
    #              ('ineptus', '|inepte', 'inepte'),
    #              ('ineptus', 'inepte', '-te'),
    #              ('ineptus', 'ineptus', 'ineptus')]
    #     for lemma, variante, erwartet in tests:
    #         ergebnis = lemma_verdichtung(lemma, variante)
    #         assert ergebnis == erwartet, \
    #             'Lemma "%s" mit Variante "%s" sollte "%s" liefern, nicht "%s"' \
    #             % (lemma, variante, erwartet, ergebnis)

    def test_manuelle_verdichtung(self):
        tests = [('ineptus', 'in|.ept.|e', 'in-e'),
                 ('ineptus', 'in.|ept|.us', '-ept-'),
                 ('ineptus', 'inep.|te', '-te'),
                 ('ineptus', 'il|.eptus', 'il-'),
                 ('ineptus', '.|inepte', 'inepte')]
        for lemma, variante, erwartet in tests:
            ergebnis = lemma_verdichtung(lemma, variante)
            assert ergebnis == erwartet, \
                'Lemma "%s" mit Variante "%s" sollte "%s" liefern, nicht "%s"' \
                % (lemma, variante, erwartet, ergebnis)

    def test_verdichtung_mehrteiliger_lemmata(self):
        # Ticket 467
        tests = [('sunt secuti', 'sunt secu.|ti', 'sunt -ti'),
                 ('sum sectus', 'sum secu.|tus', 'sum -tus'),
                 ('foret secuta', 'foret secu.|ta', 'foret -ta'),
                 ('secuta est', 'secu.|ta est', '-ta est'),
                 ('sectus est', 'secu.|tus est', '-tus est'),
                 ('sum sectus', 'sum secu.|tus', 'sum -tus')]
        for lemma, variante, erwartet in tests:
            ergebnis = lemma_verdichtung(lemma, variante)
            assert ergebnis == erwartet, \
                'Lemma "%s" mit Variante "%s" sollte "%s" liefern, nicht "%s"' \
                % (lemma, variante, erwartet, ergebnis)

    def test_Grenzfaelle(self):
        tests = [('inferior', '|Inferiores', 'Inferiores'),
                 # ('inferior', 'Inferiores', '-res')
                ]
        for lemma, variante, erwartet in tests:
            ergebnis = lemma_verdichtung(lemma, variante)
            assert ergebnis == erwartet, \
                'Lemma "%s" mit Variante "%s" sollte "%s" liefern, nicht "%s"' \
                % (lemma, variante, erwartet, ergebnis)

    def test_Fehlerkontrolle(self):
        tests = [('ineptus', 'in.|ept|.u.s', ''),
                 ('ineptus', 'in.|ept|u.s', ''),
                 ('ineptus', 'in.|ept|us', ''),
                 ('ineptus', 'in|ept|u|s', ''),
                 # ('ineptus', 'in|.ekt.|us', ''),  # Auskommentiert, da inzwischen erlaubt
                 ('ineptus', 'in|..|eptus', ''),
                 ('ineptus', 'im|ept|us', ''),
                 ('ineptus', 'im.|.|eptus', ''),
                 ('ineptus', 'im||eptus', ''),
                 ('ineptus', '|.ineptus', ''),
                 ('ineptus', 'ineptus.|', '')]
        for lemma, variante, erwartet in tests:
            try:
                ergebnis = lemma_verdichtung(lemma, variante)
                assert False, 'Fehlerhafte Lemmaverdichtung bei %s nicht gefunden! ' \
                              'Lieferte %s' % (variante, ergebnis)
            except Verdichtungsfehler:
                pass


class TestCSSClassHandlingHelperFunctions:
    def test_has_token(self):
        assert has_token('alpha beta gamma', 'alpha')
        assert has_token('alpha beta gamma', 'alpha beta')
        assert has_token('alpha beta gamma', 'beta')
        assert has_token('alpha beta gamma', 'gamma')
        assert not has_token('alpha beta gamma', 'delta')

    def test_eq_tokens(self):
        pass
        assert eq_tokens('alpha beta gamma', 'alpha beta gamma')
        assert eq_tokens('alpha   beta gamma', 'alpha beta gamma')
        assert eq_tokens('alpha beta gamma', 'gamma alpha beta')
        assert eq_tokens('', '')
        assert eq_tokens('', ' ')
        assert eq_tokens('  ', ' ')
        assert not eq_tokens('alpha beta gamma', 'gamma delta alpha beta')

    def test_add_token(self):
        assert eq_tokens(add_token('alpha beta', 'gamma'), 'alpha beta gamma')
        assert eq_tokens(add_token('alpha beta', 'gamma delta'), 'alpha beta gamma delta')
        assert eq_tokens(add_token('alpha beta delta', 'gamma delta'), 'alpha beta gamma delta')
        assert eq_tokens(add_token(add_token('alpha beta delta', 'gamma'), 'delta'),
                         'alpha beta gamma delta')


class TestMLWCompiler:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        return self.mlw_compiler(st)

    def test_finde_verankerbare_Einheit(self):
        node = parse_sxpr('(BelegText (BelegLemma "inter.|ticente") (Anker "interdico_1") (TEXT ", (") (BelegLemma "inter.|titente") (Anker "interdico_2") (L " ") (Zusatz "var. l.") (TEXT ") Deo."))')
        d = {'interdico_1' : 'inter.|ticente',
             'interdico_2' : 'inter.|titente'}
        for anker in node.select('Anker'):
            assert str(self.mlw_compiler.finde_letzte_verankerbare_Einheit(anker, node)) == d[str(anker)]

    def test_Stelle(self):
        try:
            rn = self.compile("app. A 6 p. 137,30 ", "Stelle")
        except ValueError:
            assert False, "Fehler in MLWCompiler.on_Stelle()"

    def test_Zeilenwechsel(self):
        belegtext = '"aetaernam \n percipiunt"'
        st = self.mlw_grammar(belegtext, 'BelegText')
        self.mlw_transformer(st)
        assert str(st) == "aetaernam percipiunt"

    def test_Anker_elimination(self):
        nd = parse_sxpr('(Beleg (BelegText (TEXT " (loca") (L " ") (Anker "iocus_8") (TEXT ", colloquia, colloqui)")))')
        rn = RootNode(nd)
        rn = self.mlw_compiler(rn)
        assert rn.content.find('loca,') >= 0, rn.as_sxpr()
        self.mlw_compiler.ordne_verweise.anker = {}

        nd = parse_sxpr('(Beleg (BelegText (TEXT " (loca ") (Anker "iocus_8") (TEXT ", colloquia, colloqui)")))')
        rn = RootNode(nd)
        rn = self.mlw_compiler(rn)
        assert rn.content.find('loca,') >= 0
        self.mlw_compiler.ordne_verweise.anker = {}

        nd = parse_sxpr('(BelegText (BelegLemma "curea") (L " ") (Anker "curia_2") (TEXT " ... est separata."))')
        rn = RootNode(nd)
        rn = self.mlw_compiler(rn)
        del rn['BelegLemma'].attr['globaleId']
        cmp = '(BelegText (BelegLemma `(verdichtung "curea") `(id "curia_2") `(lemma "curea") "curea") (TEXT " ... est separata."))'
        assert parse_sxpr(cmp).equals(rn), rn.as_sxpr()
        self.mlw_compiler.ordne_verweise.anker = {}

        nd = parse_sxpr('(Nebenbedeutungen (Klassifikation (Kategorienangabe (TEXT "mortifere")) '
                        '(L " ") (Anker "sacrilegium_1")) (L " ") (Klassifikation '
                        '(Kategorienangabe (TEXT "incorpor.")) (L " ") (Anker "sacrilegium_2")))')
        rn = RootNode(nd)
        rn = self.mlw_compiler(rn)
        assert len(list(rn.select('L'))) == 2
        assert rn.content.find('mortifere in') >= 0, rn.as_sxpr()

    def test_Anker_Elimination2(self):
        einschub = '((* {cf.} Dt. Rechtswb. XI. {@ sal_34}; p. 1451sqq.))'
        rn = self.compile(einschub, 'Einschub')
        stelle = rn.pick('Stelle', reverse=True)
        assert stelle.attr['werk'].find('sal_34') < 0

    def test_Anker_Elimination3(self):
        beleg_text = '''"videtur, quod insufficienter dividit rationem, quia sapientialia {@ sapientialis001} sunt in nobis, 
        ad quae etiam perficimur nobilissimo habitu; sed non sunt nisi in ratione; ergo debuit ponere cum duobus, 
        quae ponit, etiam #sapientific.|um."'''
        rn = self.compile(beleg_text, 'BelegText')
        assert rn.equals(parse_sxpr('''(BelegText
            (TEXT "videtur, quod insufficienter dividit rationem, quia ")
            (TEXT `(id "sapientialis001") `(globaleId "UNBEKANNTER-ARTIKELNAME.sapientialis001") "sapientialia ")
            (TEXT "sunt in nobis, ad quae etiam perficimur nobilissimo habitu; sed non sunt nisi in ratione; ergo debuit ponere cum duobus, quae ponit, etiam ")
            (BelegLemma `(verdichtung "-um") `(id "sapientificum") `(anker "automatisch generiert") `(globaleId "UNBEKANNTER-ARTIKELNAME.sapientificum") `(lemma "sapientificum") "sapientificum")
            (TEXT "."))'''))
        assert rn.content.find('quia sapientialia sunt') >= 0
        verankert = rn.pick(lambda nd: nd.content.find('sapientialia') >= 0)
        assert verankert.attr['id'] == 'sapientialis001'

    def test_Anker_Elimination4(self):
        # Ticket 466
        beleg_text = '''"eam agni #seque.|llam {@sequela02} pronuntiabis?"'''
        rn = self.compile(beleg_text, 'BelegText')
        assert rn.equals(parse_sxpr('''(BelegText
              (TEXT "eam agni ")
              (BelegLemma `(verdichtung "-llam") `(id "sequela02") `(globaleId "UNBEKANNTER-ARTIKELNAME.sequela02") `(lemma "sequellam") "sequellam")
              (TEXT " ")
              (TEXT "pronuntiabis?"))'''))
        beleg_text = '''"virginibus #seque.|llis {@sequela08} 'via constat et vita'."'''
        rn = self.compile(beleg_text, 'BelegText')
        assert rn.equals(parse_sxpr('''(BelegText
              (TEXT "virginibus ")
              (BelegLemma `(verdichtung "-llis") `(id "sequela08") `(globaleId "UNBEKANNTER-ARTIKELNAME.sequela08") `(lemma "sequellis") "sequellis")
              (TEXT " ")
              (ZITAT "‘via constat et vita’")
              (TEXT "."))'''))

    def test_Einschub_Warnung(self):
        rn = self.compile('"({sim.} 59,13 p. 556,9 #sabbat.|um ... cordis quietem in Deo {[sc. significat]})"',
                          'BelegText')
        assert rn.errors and rn.errors[0].code == WARNUNG_EINSCHUB_ERWARTET

    def test_Links(self):
        links = [
            '{=> 8,44|https://www.bibelwissenschaft.de/online-bibeln/'
            'biblia-sacra-vulgata/lesen-im-bibeltext/bibel/text/lesen/'
            'stelle/53/80001/89999/ch/915496889b6f8bf65a5b1e871a1d12e8/}',
            '{=> https://archive.org/details/culturamedioeval01roncuoft/page/312/mode/2up?q=sacrilegare}',
            '{=>https://epub.ub.uni-muenchen.de/17286/1/4Cod.ms.321_Sept.2013.pdf#page=36}',
            '{=>https://app.digitale-sammlungen.de/bookshelf/bsb00095232/view?view=ImageView'
            '&manifest=https%3A%2F%2Fapi.digitale-sammlungen.de%2Fiiif%2Fpresentation%2Fv2%2F'
            'bsb00095232%2Fmanifest&canvas=https%3A%2F%2Fapi.digitale-sammlungen.de%2Fiiif%2F'
            'presentation%2Fv2%2Fbsb00095232%2Fcanvas%2F290}'
            '{=> https://www.dmgh.de/mgh_capit_1/index.htm#page/189/mode/1up}'
            '{=> https://daten.digitale-sammlungen.de/bsb00000756/images/index.html?id=00000756&groesser=&fip=193.174.98.30&no=&seite=17}'
        ]
        for link in links:
            # wa_ergänzung statt EinzelVerweis, da Bearbeitung ein Eltern-Element voraussetzt
            rn = self.compile(link, 'wa_ergänzung')
            assert not rn.errors

    def test_alias(self):
        einzel_verweis = """{=>München, Universitätsbibliothek, ms. 4° 321, f. 17^v 
            | https://epub.ub.uni-muenchen.de/17286/1/4Cod.ms.321_Sept.2013.pdf#page=36}"""
        rn = self.compile(einzel_verweis, 'Verweise')
        expected = '(Verweise (a ' \
            '`(href "https://epub.ub.uni-muenchen.de/17286/1/4Cod.ms.321_Sept.2013.pdf#page=36")'\
            ' (TEXT "München, Universitätsbibliothek, ms. 4° 321, f. 17") (HOCHGESTELLT "v")))'
        assert flatten_sxpr(rn.as_sxpr()) == expected

    def test_Belege(self):
        belege = '''* PAUL. DIAC. Lang.; 5,27 "in ipso sacratissimo #sabbat.|o {!paschali}
        ((* AMALAR. off.; 1,35,3 "dies quinquaginta, qui secuntur a #sabbat.|o p|.aschali usque ad octavas pentecostes"
          * NOTKER. BALB. gest.; 2,21 p. 28,24))" '''
        rn = self.compile(belege, 'Belege')
        abk = rn.pick('Abkuerzung')
        assert abk is not None
        assert abk.has_attr('verdichtung')
        assert abk.attr['verdichtung'] == 'p.'

    # def test_Albert_Aristoteles_Ausnahme(self):
    #     # Gibt es die Alber M. Ausnahme doch nicht
    #     belege ='''* ALBERT. M. mort.; 2,7 p. 363^a,25 "{!calor} modicus #intrane.|us
    #     ((* animal.; 16,115 "'venae ... et ossa ex <<calore #intrane.|o>>
    #       (($ARIST.; p. 743^a,17 "τῆς ἐντὸς θερμότητος"))
    #       desiccantur'."
    #       * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intrane.|um"
    #     ))."'''
    #     rn = self.compile(belege, 'Belege')
    #     ctx = rn.pick_path(lambda ctx: ctx[-1].content == 'animal.')
    #     quellen_angabe = ctx[-3]
    #     assert quellen_angabe.name == 'Quellenangabe'
    #     assert quellen_angabe.attr['autor'] == "Arist."
    #
    #     belege ='''* NONALBERT. M. mort.; 2,7 p. 363^a,25 "{!calor} modicus #intrane.|us
    #     ((* animal.; 16,115 "'venae ... et ossa ex <<calore #intrane.|o>>
    #       (($ARIST.; p. 743^a,17 "τῆς ἐντὸς θερμότητος"))
    #       desiccantur'."
    #       * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intrane.|um"
    #     ))."'''
    #     rn = self.compile(belege, 'Belege')
    #     ctx = rn.pick_path(lambda ctx: ctx[-1].content == 'animal.')
    #     quellen_angabe = ctx[-3]
    #     assert quellen_angabe.name == 'Quellenangabe'
    #     assert quellen_angabe.attr['autor'] == "Nonalbert. M."

    def test_Albert_Aristoteles_2(self):
        belege = '''* ALBERT. M. metaph.; 7,1,2| p. 318,62 "'dubitavit aliquis' ..., 'utrum vadere et #san.|are 
        ((* $ARIST.; p. 1028^a,21 "τὸ ὑγιαίνειn")) et sedere'..., 'unumquodque' illorum 'sit ens aut non-ens'."; 
        p. 318,74 "'si aliquid' praeinductorum 'est' de numero 'entium' sicut 'vadens' aut 'sedens et #san.|ans{@ sano_10}' 
        ((* [[$ARIST. metaph.]]; p. 1028^a,25 "τὸ ὑγιαῖνον")) {eqs.}"'''
        rn = self.compile(belege, 'Belege')
        assert rn.pick('Stelle', reverse=True).get_attr('autor', '') == "Arist."
        rn = AusgabeTransformation()(rn)
        assert rn.pick('Stellenangabe', reverse=True).get_attr('class', '').find('kursiv') >= 0

    def test_ohne_Autor(self):
        belege = '''* PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ... 
        cum #scarifi.|catione ({B}, #scari.|fatione {A}) 
        ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως"))."; 
        71 p. 48,24 "ventosandum ... cum #scarifi.|catione ((* [[$PAUL. AEG.]]; 3,20,1 p. 168,23 "ἀμυχῶν"))."'''
        rn = self.compile(belege, 'Belege')
        assert rn.pick('Stelle', reverse=True).get_attr('autor', '') == "Paul. Aeg."
        rn = AusgabeTransformation()(rn)
        assert rn.pick('Stellenangabe', reverse=True).get_attr('class', '').find('kursiv') >= 0

    def test_Rueckwaertsverweis(self):
        st = parse_sxpr('(Verweise (Verweis (alias (TEXT "p. ") (TEXT "379,49.")) (L " ") (URL (pfad "forensis/"))))')
        # st = parse_sxpr('(Verweise (Verweis (alias (TEXT "p. ") (TEXT "379,49.")) (L " ") (URL (pfad "forensis/") (ziel "x"))))')
        self.mlw_transformer(st)
        rn = self.mlw_compiler(RootNode(st).with_pos(0))
        assert rn.as_sxpr() == '(Verweise (a `(href "forensis.xml") "p. 379,49."))'

    def test_Verweis_ohne_Ziel(self):
        st = parse_sxpr('(Verweise (Verweis (alias (TEXT "p. ") (TEXT "379,49.")) (L " ") (URL (ziel "-"))))')
        self.mlw_transformer(st)
        rn = self.mlw_compiler(RootNode(st).with_pos(0))
        assert rn.as_sxpr() == '(Verweise (a `(href "-") "p. 379,49."))'

    def test_Anker(self):
        sxpr = """(Beleg (Quellenangabe
            (Quelle (Autor "CONST.") (L " ") (Werk "Melf."))
            (L " ") (BelegStelle (Stellenangabe (Stelle "1,5"))
              (L " ") (BelegText
                (TEXT "multae leges ") (BelegLemma "sacrileg.|os")
                (TEXT " ") (Anker "sacrilegus_12")
                (TEXT " ") (Anker "sacrilegus_16")
                (TEXT " punierunt, sed poena moderanda est arbitrio iudicantis, nisi forte ... Dei templa destructa ... aut dona vel vasa sacra ... sublata sint.")))))"""
        ast = parse_sxpr(sxpr)
        rn = self.mlw_compiler(RootNode(ast).with_pos(0))
        assert len(rn.errors) == 1 and rn.errors[0].code == FEHLER_DOPPELTER_ANKER

    def test_Autorverkuerzung(self):
        schreibweise = """
            SCHREIBWEISE
                 script.:
                         za-: {=> safranum_1; safranum_9}
                         zaphir(um): * THADD. FLORENT. cons.; 37,59
                         zafar-: * THADD. FLORENT. cons.; 140,7sqq.
                         zaffar-: {=> safranum_8}
                         zafir(um): * THADD. FLORENT. cons.; 44,139.
                         zaframyn: {=> safranum_6}
                         sophrana: {=> safranum_5}
                         sofferana: {=> safranum_4}
                         saffar-: {=> safranum_2}
                         saffer-: {=> safranum_3}
                         saffia-: {=> safranum_10}        
        """
        rn = self.compile(schreibweise, 'ArtikelKopf')
        items = list(rn.select_if(lambda nd: nd.name == 'Autor'
                                             and nd.content == "Thadd. Florent."))
        assert len(items) == 3, "Autornamen sollten im Artikelkopf nicht gestrichen werden!"

    def test_angehängte_Zusätze(self):
        # Ticket 56
        u3bed = """
            UUU_BEDEUTUNG  usu vario:

                * PAUL. DIAC. Lang.; 2,4 p. 74,17 "videres {(sc. lector)} #se|.cul.|um 
                  in antiquum redactum silentium."
                * RIMB. Anscar.; 3 p. 23,28 "quae {(vox suavissima)} mihi omne ... #saecul.|um 
                  visa est implevisse."; 35 p. 67,10 "vidit se quasi usque in caelum rapi 
                  et totum #saecul.|um acsi in teterrimam vallem collectum."
                * MARIAN. chron. a. m.; 2177 p. 477^{b},13 "secunda {@ saeculum77} 
                  aetas #se|.cul.|i a diluvio usque ad nativitatem Abrahae protenditur 
                  ({postea:} de tertia aetate mundi)."
                * VITA Heinr. II. A; 31 "caveant ... habitatores #se|.cul.|i domesticis Dei et civibus sanctorum detrahere."
                {saepe.} {v. et {=> saeculum55}} """
        rn = self.compile(u3bed, 'U3Bedeutung')
        s = str(rn)
        i = s.rfind('saepe')
        k = s.rfind('v. et')
        assert k > i

    def test_Athetese(self):
        # Ticket 71
        beleg_text = '"uoluntatem, #[p]s|.alt.|im ut sub mensam illius"'
        rn = self.compile(beleg_text, 'BelegText')
        beleg_lemma = rn.pick('BelegLemma')
        assert beleg_lemma.content == "[p]saltim"
        assert beleg_lemma.attr['id'] == 'psaltim'
        assert beleg_lemma.attr['verdichtung'] == '[p]s-im'

    def test_Umwandlung_von_Varianten_in_Kategorien(self):
        form_pos = """
            FORM
                 form.:
                     dat. sg.:
                         -e: {=> saecularis_3}
                     abl.:
                     sg.:
                         -e: {=> saecularis_4}
                     pl.:
                         -is: {=> saecularis_5}
                     superl.:
                         -issim(us): {=> saecularis_6}
                     decl. II: {=> saecularis_7; saecularis_8; saecularis_9}
                               {al. }{(cf. Stotz, Handb. 4,VIII § 12.3)} """
        rn = self.compile(form_pos, "FormPosition")
        nd = rn.pick('Kategorie', reverse=True)
        assert nd['Beschreibung'].content == 'decl. II', nd['Beschreibung'].content
        form_pos = """
        FORM
             form.:
                 sacras-: {=> sacrosanctus_3; sacrosanctus_11}
                 sacras-a: {=> sacrosanctus_4; sacrosanctus_5; sacrosanctus_6}
                 sacris-is: {=> sacrosanctus_7}
                 dat. sg. fem.:
                     -i ({instar} -ae): {=> sacrosanctus_8}
                 separatim scribitur: {=> sacrosanctus_5}
                 usu subst.: {=> sacrosanctus_9; sacrosanctus_10}
        """
        rn = self.compile(form_pos, "FormPosition")
        nd = rn.pick(
            lambda nd: nd.get('Beschreibung', EMPTY_NODE).content == 'separatim scribitur',
            reverse=True)
        assert nd.name == 'Kategorie'

    def test_Umwandlung_Varianten_in_Kategorien(self):
        # Ticket 327
        scrib = """
        SCHREIBWEISE
            script. et form.:
                imper.:
                    -fac: {=> scarifico_1}
                inf.:
                    scariarti, scareartius {sim.}: {=> scarifico_4}
                partic. perf.:
                    curifacto {in loco corrupt.}: {=> scarifico_2}
        """
        rn = self.compile(scrib, "SchreibweisenPosition")
        for kategorie in  rn.select('Kategorie'):
            assert kategorie.pick('Variante')

    def test_Umwandlung_Varianten_in_Kategorien_Ausnahme(self):
        # Ticket 464
        scrib = """
            VERWECHSELBAR
            confunditur c.:
                2. serere: {=> sero_2/sero_99a; sero_2/sero_99b} /* "2. serere" wird kursiv angezeigt,  müsste aber gerade sein*/"""
        rn = self.compile(scrib, "VerwechselungsPosition")
        assert rn.pick('Kategorie') is None

    def test_redundante_Stellenangaben(self):
        belege = '''* CHART. Rhen. med.; III| 1222 "III 1222"; 1333 "III 1333"
                    * CHART. Rhen. med.; 1444 "III 1444"; II| 1555 "II 1555"; 1666 "II 1666"; II 1777 "II 1777"
                    * CHART. Rhen. theol.; IV 1888 "IV 1888" '''
        rn = self.compile(belege, 'Belege')
        assert rn.errors[0].code == WARNUNG_REDUNDANTER_STELLENABSCHNITT
        for beleg_stelle in rn.select('BelegStelle'):
            stelle = beleg_stelle.pick('Stelle')
            text = beleg_stelle.pick('BelegText')
            volle_stellenangabe = stelle.attr['stelle']
            assert volle_stellenangabe == text.content, str(beleg_stelle)
            assert volle_stellenangabe.endswith(stelle.content), str(stelle)
            if volle_stellenangabe == "II 1777":
                assert stelle.content == '1777'

    def test_redundante_Stellenangaben2(self):
        belege = '''* SABA MALASP. chron.; 6,7 p. 252,18 "illi ..., cui boves assignantur ad fructum,
            lex talis imponitur, ut ... ponat de sex #sal|.mat.|is bene scissorum et sulcatorum
            novalium rationem (({sim.} p. 253,8sq.; 6,8| p. 292,13
            "si magister massarie ... videbat aliquam #sal|.mat.|am bene sulcate novalis"))."'''
        rn = self.compile(belege, 'Belege')
        assert rn.errors
        belege = '''* SABA MALASP. chron.; 6,7| p. 252,18 "illi ..., cui boves assignantur ad fructum,
            lex talis imponitur, ut ... ponat de sex #sal|.mat.|is bene scissorum et sulcatorum
            novalium rationem (({sim.} p. 253,8sq.; 6,8| p. 292,13
            "si magister massarie ... videbat aliquam #sal|.mat.|am bene sulcate novalis"))."'''
        rn = self.compile(belege, 'Belege')
        assert not rn.errors
        stellen = [node.attr['stelle'] for node in rn.select('Stelle')]
        assert stellen == ["6,7 p. 252,18", "6,7 p. 253,8sq.", "6,8 p. 292,13"]

    def test_redundante_Stellenangaben3(self):
        belege = '''* IONAS BOB. Columb.; 1,15| p. 177,17 "ad ipsum"; 2,3| "vir Dei"'''
        rn = self.compile(belege, 'Belege')
        rn_str = rn.content
        assert rn_str == "Ionas Bob. Columb. 1,15 p. 177,17 ad ipsum 2,3 vir Dei"
        stelle = rn.pick('Stelle', reverse=True)
        assert stelle.attr['stelle'] == '2,3'
        belege = '''* IONAS BOB. Columb.; 1,15| p. 177,17 "ad ipsum"; 2,3|- "vir Dei"'''
        rn = self.compile(belege, 'Belege')
        assert rn.content == rn_str
        stelle = rn.pick('Stelle', reverse=True)
        assert stelle.attr['stelle'] == '2,3'

    def test_nicht_redundante_Stellenangaben1(self):
        belege = '''* CHART. Sangall. C; 14 (s. $VIII.^2) "inter #sali.|ka terra et hopas XL iurnales."; 
                      141 p. 129,14 "excepto ea {(hoba)}"'''
        rn = self.compile(belege, 'Belege')
        assert not rn.errors
        assert str(rn).find('141') >= 0

    def test_nicht_redundante_Stellenangaben2(self):
        belege = '''* ANNAL. Ceccan.; ((MGScript. XIX; p. 293,50))(a. 1196) 
                    "in apparatu ciborum ... media libri piperis et cinnamomi et #soffe|.ran.|ae {@ safranum_4}."; 
                    p. 297,8 (a. 1208) /* ((MGScript. XIX; p. 297,8))*/ "in #sophrana {@ safranum_5}."'''
        rn = self.compile(belege, 'Belege')
        for stelle in rn.select('Stelle'):
            assert stelle.attr['stelle'] == stelle.content

    def test_nicht_redundante_Stellenangaben3(self):
        belege = '''* ANNAL. Ceccan.; a. 1196 ((MGScript. XIX; p. 293,50)) 
                    "in apparatu ciborum ... media libri piperis et cinnamomi et #soffe|.ran.|ae {@ safranum_4}."; 
                    a. 1208| (p. 297,8) "in #sophrana {@ safranum_5}."'''
        rn = self.compile(belege, 'Belege')
        # print(rn.as_sxpr())
        # for stelle in rn.select('Stelle'):
        #     assert stelle.attr['stelle'] == stelle.content
        # TODO: No Test yet!

    def test_nicht_redundante_Stellenangaben4(self):
        belege = '''* ANNAL. Ceccan.; a. 1196 ((MGScript. XIX; p. 293,50)) 
                    "in apparatu ciborum ... media libri piperis et cinnamomi et #soffe|.ran.|ae {@ safranum_4}."; 
                    a. 1208| p. 297,8 "in #sophrana {@ safranum_5}."'''
        rn = self.compile(belege, 'Belege')
        print(rn.errors)
        # print(rn.as_sxpr())
        # TODO: No Test yet!

    def test_AutorWerk(self):
        autor_werk = "IOH. DE DEO chron."
        rn = self.compile(autor_werk, "AutorWerk")
        rn.errors = []
        assert str(rn) == "Ioh. De Deo chron.", str(rn)

    def test_FremdAutorFix(self):
        einschub = """((* {cf.} J.-P. Devroey, Revue du Nord. 61. 1979.; p. 545sq.))"""
        rn = self.compile(einschub, 'Einschub')
        autorangabe = rn.pick('Autorangabe')
        assert autorangabe and autorangabe.content == 'J.-P. Devroey'

    def test_eliminiere_leeres_Werk(self):
        einschub = '''(( *  {sim. ibid.}; p. 19a^{r} "#salvi.|a ...,"))'''
        rn = self.compile(einschub, "Einschub")
        rn.errors = []
        assert rn.pick('Werk') is None
        assert str(rn).find('ibid. ') >= 0, "Leerzeichen fehlt nach ibid."

    def test_uebertrage_Werk_und_Stelle(self):
        belege = '''* CIRCA INSTANS; p. 19a^{r} "eupatorium ... idem est quod #salvi.|a agrestis (( * {sim. ibid.} "#salvi.|a ..."))"'''
        rn = self.compile(belege, "Belege")
        stelle = rn.pick(lambda nd: nd.name == "Stelle" and nd.content == "sim. ibid.")
        assert stelle.attr['stelle'] == "p. 19a^{r}"

    def test_Werktitel_Validierung(self):
        beleg ='''* PAUL. AEGIN. cur.; 38 "ad emigraneam kroniam: ... <<olei #sabin.|i>> ((* Paul. Aeg.; 3,5,6 "ἐλαίου Σαβίνου"))"'''
        rn = self.compile(beleg, "Belege")
        assert rn.errors and rn.errors[0].code == WARNUNG_FALSCHE_AUTORANGABE
        beleg ='''* PAUL. AEGIN. cur.; 38 "ad emigraneam kroniam: ... <<olei #sabin.|i>> ((* PAUL. Aegin.; 3,5,6 "ἐλαίου Σαβίνου"))"'''
        rn = self.compile(beleg, "Belege")
        assert rn.errors and rn.errors[0].code == WARNUNG_FALSCHE_AUTORANGABE
        beleg = '''* PAUL. AEGIN. cur.; 38 "ad emigraneam kroniam: ... <<olei #sabin.|i>> ((* PAUL. Aegin. cur.; 3,5,6 "ἐλαίου Σαβίνου"))"'''
        rn = self.compile(beleg, "Belege")
        assert rn.errors and rn.errors[0].code == WARNUNG_FALSCHE_AUTORANGABE
        beleg = '''* NOTULAE Wilh. Cong.; 1376 "hec egritudo"'''
        rn = self.compile(beleg, "Belege")
        assert not rn.errors
        beleg = '''* WALAHFR. Mamm.; 15,10 "inclusi ..."; Wett.; 935 "#sacr.|as protendite"'''
        rn = self.compile(beleg, "Belege")
        assert not rn.errors

    def test_Werknamen_Abkuerzung(self):
        einschub = '''((HRG| II.; p. 566sqq. * HRG| V.; p. 1039sqq.))'''
        rn = self.compile(einschub, 'Einschub')
        assert str(rn) == "HRG II. p. 566sqq. V. p. 1039sqq."

    def test_Einschuebe_bei_Varianten_im_Artikelkopf(self):
        position = '''
    script.:
        sagina ((* {cf.} Battaglia, Dizionario. XVII.; p. 360sq.)): {=> saisina_1}'''
        rn = self.compile(position, 'Position')
        assert rn.pick('Kategorie') is None

    def test_opera_minora_Stellenangaben(self):
        belege = '''* VITA Austrig.; 7 ((MGMer. IV; p. 196,11)) (c. s. $IX.) "cum ... nullum custodem
            nec aliquem clericum ibidem {(sc. in ecclesiis castelli deserti)} reperisset
            nisi tantum <<patentibus hostiis et #sacrariol.|o cluso et seris obfirmato>>
            (patentia hostia et #sacrariol.|a clauso et seris obfirmato {1^b}, patentia hostia
            et #sacrariol.|um clausum et seris obfirmatum {2}), voluit missas in illa baselica
            celebrare (({cf.} p. 196,17 "regressi ... invenerunt {(sc. adminiculi)} hostia de
            sacrario {@ sacrarium_4a} illo aperta, que antea serata viderant, et cum introissent {eqs.}"))."'''
        rn = self.compile(belege, 'Belege')
        # print(rn.as_sxpr())
        for i, stelle in enumerate(rn.select('Stelle')):
            assert stelle.attr['autor'] == "Vita"
            assert stelle.attr['werk'] == "Austrig."
            assert stelle.attr['stelle'][:1] == '7'
            if i >= 1:
                assert stelle.attr['edition'] == 'MGMer. IV'

    def test_Literaturangaben_Verifikation(self):
        belege = '''{cf.} J. Ruska, Das Steinbuch des Aristoteles. p. 39. 75,14. 190,8'''
        rn = self.compile(belege, 'Belege')
        assert rn.errors[0].code == FEHLER_STELLENANGABE_NICHT_ABGETRENNT
        belege = '''HistVjSchr. 28. p. 530'''
        rn = self.compile(belege, 'Belege')
        assert rn.errors[0].code == FEHLER_STELLENANGABE_NICHT_ABGETRENNT
        belege = '''ArchDipl. 23.; 1977. p. 280'''
        rn = self.compile(belege, 'Belege')
        assert rn.errors[0].code == WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE

    def test_Anker_Junktur(self):
        belege = '* ALCUIN. carm.; 9,150 "qui {(Deus)} ferit <<atque #san.|at>> {@ sano_2}"'
        rn = self.compile(belege, 'Belege')
        l = list(rn.select('Junktur'))
        assert len(l) == 1

    def test_VerweisArtikel(self):
        vwa = """LEMMA salbia VIDE salvia
            LEMMA salbo VIDE salvo 
            LEMMA salcedo VIDE salsedo 
            LEMMA *salcesco VIDE *salsesco 
            LEMMA salcetum VIDE salictum 
            LEMMA salcicium VIDE salsicium
            
            AUTORIN Leithe-Jasper"""
        rn = self.compile(vwa, 'VerweisArtikel')
        vwi = rn.indices('VerweisBlock')
        diff = [vwi[i] - vwi[i - 1] for i in range(1, len(vwi))]
        assert all(d == 2 for d in diff)

    def test_tmesi(self):
        belegtext = '''"#sat- {@ satago_7} michi {(sc. Reinardo)} gallus ##-agit"'''
        rn = self.compile(belegtext, 'BelegText')
        for nd in rn.select('BelegLemma'):
            assert nd.attr['tmesi'] == 'sat-agit'
            assert nd.attr['lemma'] == 'satagit'
        belegtext = '''"vivat #a- hicks! #-rom- hicks! #-a vino gallico!"'''
        rn = self.compile(belegtext, 'BelegText')
        for nd in rn.select('BelegLemma'):
            assert nd.attr['tmesi'] == 'a-rom-a'
            assert nd.attr['lemma'] == 'aroma'

    def test_op_cit_Autor(self):
        artikel = '''
        LEMMA *saphena
        
        GRAMMATIK 
            subst. I; -ae f.
        
        ETYMOLOGIE arab. ṣāfin: * {cf.} J. Hyrtl, Das Arabische und Hebräische in der Anatomie. 1879.; p. 212sqq.
        
        BEDEUTUNG t. t. anat. de venis superficialibus incidendis i. q. Vena saphena magna, Vena saphena parva -- Große Rosenvene, Kleine Rosenvene ((* {de re v.} op. cit.)) ((* {et} Pschyrembel, Klinisches Wörterbuch. ^{255}1986.; p. 1769)):
        
           * CONSTANT. AFRIC. theor.; 2,12 p. 8a^r "tertia {(vena)} in cruris inferiora descendens usque ad eorum gibbosa atque cavillam, ista a medicis vocata est #so|.phen.|a {@ saphena_1a}."
        
        AUTORIN Niederer
        '''
        rn = self.compile(artikel, 'Artikel')
        op_cit = rn.pick(lambda node: not node.children and node.content == 'op. cit.', reverse=True)
        assert op_cit.get_attr('werk', '') != '' and op_cit.get_attr('autor', '') == 'J. Hyrtl'

    def test_Nachschlagewerg(self):
        einschub = "((* {cf.} Der Neue Pauly. XI.; p. 105 s. v. ‘Saton’))"
        rn = self.compile(einschub, "Einschub")
        # print(rn.as_sxpr())
        # for e in rn.errors: print(e)
        assert not rn.errors

# class Test AutorWerk

schnipsel_1="""
VERWECHSELBAR
    confunditur:
        compar. c.:
            inferre: {=> p. 5,31.|inferre/INFERRE}
            interior: {=> p. 12,50.|interior/INTERIOR}
        superl. c.:
            infirmus: * ARBEO Emm.; p. 54,24.
                      * OTTO FRISING. gest.; 2,30 p. 138,35 {var. l.}
            unus: p. 12,1.6.; {adde} p. 1632,47."""

schnipsel_2="""
VERWECHSELBAR
    confunditur:
        compar. c.:
            inferre: {=> p. 5,31.|inferre/INFERRE}
            interior: * OTTO FRISING. gest.; 2,30 p. 138,35 {var. l.}
        superl. c.:
            infirmus: p. 54,24."""

schnipsel_3="""
BEDEUTUNG  pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch,
  Tuch; usu liturg.; {de re v. {=> eintrag/ibi_X}}:

  * LIBRI confrat.; III app. A  6  p.  137,30 "pulpitum ...  #facitergula cocco
  imaginata circumdari iussit {pontifex}"

  * CATAL. thes. Germ.; 76,15 "#faciterulae II"; 40,5 VI "#vizregule"; 129^a,5
  "#facisterculas II."; 24,8  "#facitella X"; 114,8 VIII "#fezdreglę";  6,24
  "#fasciutercule VII"; 92,6 "#fascerculę tres."; 21,20 IIII "#festregelę"
  {saepe}"""

schnipsel_4="""
BEDEUTUNG  pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch,
  Tuch; usu liturg.; {de re v. {=> eintrag/ibi_X}}:

  * LIBRI confrat.; III app. A  6  p.  137,30 "pulpitum ...  #facitergula cocco
  imaginata circumdari iussit {pontifex}"

  * CATAL. thes. Germ.; 76,15 "#faciterulae II"; 40,5 VI "#vizregule"; 129^a,5
  "#facisterculas II."; 24,8  "#facitella X"
   
  * 114,8 VIII "#fezdreglę";  6,24 "#fasciutercule VII"; 92,6 "#fascerculę tres."; 
  21,20 IIII "#festregelę" {saepe}"""

schnipsel_5 = """
BEDEUTUNG  pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch,
  Tuch; usu liturg.; {de re v. {=> eintrag/ibi_X}}:

  * III app. A  6  p.  137,30 "pulpitum ...  #facitergula cocco
  imaginata circumdari iussit {pontifex}"

  * CATAL. thes. Germ.; 76,15 "#faciterulae II"; 40,5 VI "#vizregule"; 129^a,5
  "#facisterculas II."; 24,8  "#facitella X" """

class TestAutorWerk:
    def test_erhalteAutorWerk(self):
        """Zwischen verschiedenen Varianten im Artikelkopf sollten Autor und
        Werk erhalten bleiben"""
        tree = kompiliere_schnipsel(schnipsel_1, "VerwechselungsPosition")
        assert all(e.code < ERROR or e.code in
                   (MANDATORY_CONTINUATION_AT_EOF, MANDATORY_CONTINUATION_AT_EOF_NON_ROOT)
                   for e in tree.errors), str(tree.errors)

    def test_setzeAutorWerkzurueck(self):
        """Zwischen verschiedenen Kategorien im Artikelkopf und zwischen
        verschiedenen Belegstrecken sollten Autor und Werk zurückgesetzt werden"""
        tree = kompiliere_schnipsel(schnipsel_2, "VerwechselungsPosition")
        assert is_error(tree.error_flag)

        tree = kompiliere_schnipsel(schnipsel_3, "BedeutungsPosition")
        assert all(e.code < ERROR or e.code in
                   (MANDATORY_CONTINUATION_AT_EOF, MANDATORY_CONTINUATION_AT_EOF_NON_ROOT)
                   for e in tree.errors)

        tree = kompiliere_schnipsel(schnipsel_4, "BedeutungsPosition")
        assert not any(e.code == FEHLER_AUTOR_FEHLT for e in tree.errors)

        tree = kompiliere_schnipsel(schnipsel_5, "BedeutungsPosition")
        assert any(e.code == FEHLER_AUTOR_FEHLT for e in tree.errors)


class TestAnker:
    def test_Ankerzuordnung(self):
        """Anker werden nachgestellt und sollten dem letzten Wort zugeornet
        werden."""
        text = '"negotia saecularia: ... turpis verbi vel facti #ioculator.|em esse '\
               'vel iocum {@ iocus_2} saeculare diligere."'
        tree = kompiliere_schnipsel(text, "BelegText")
        assert next(tree.select_if(lambda nd: nd.get_attr('id', '') == 'iocus_2')).content.strip() == 'iocum'


class TestGrammatik:
    def test_wortart(self):
        grammatik = """substantivum II; -orum n."""
        tree = kompiliere_schnipsel(grammatik, 'Grammatik')
        assert tree['wortart'].has_attr('verdichtung')
        tree = process_tree(get_Ausgabe_Transformation(), tree)
        assert not tree['wortart'].content


class TestBesonderheiten:
    def test_sperrung(self):
        schnipsel = '''"si ... predictam civitatem nos {(sc. imperatorem) adire contigerit, 
        marscalcus sive camerarius ... in #curi.|a {!claustrali}} aliquem ex principibus 
        locandi potestatem habeat (( * CHART. Gosl.; II 26 p. 125,12. {al.}))."'''
        tree = kompiliere_schnipsel(schnipsel, 'BelegText')
        save = copy.deepcopy(tree)
        html_transform = HTMLTransformation()
        html_transform(tree)
        assert tree.pick('Sperrung').content == "claustrali"
        tree = save
        node = tree.pick(lambda nd: not nd.children and "aliquem" in nd.result)
        node.result = ',' + node.result
        html_transform(tree)
        assert tree.pick('Sperrung').content == "claustral"
        assert tree.content.find('claustrali') >= 0


class TestRegressionsFehler:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.at = AusgabeTransformation()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        return self.mlw_compiler(st)


if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
