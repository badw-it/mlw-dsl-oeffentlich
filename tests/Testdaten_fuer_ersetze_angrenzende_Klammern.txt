(BelegText
  (TEXT
    "editus "
  )
  (BelegLemma `(verdichtung "-o") `(id "sacerA15") `(globaleId "sacer1-test.sacerA15") `(lemma "sacro")
    "-o"
  )
  (TEXT
    " patris ore semper virginis casta voluit sub alvo arta naturae tolerare factor munia nostrae."
  )
)
(BelegText
  (TEXT
    "in sancta Sion, ubi "
  )
  (BelegLemma `(verdichtung "-r") `(id "sacer") `(globaleId "sacer1-test.sacer") `(lemma "sacer")
    "-r"
  )
  (TEXT
    " seculorum Salvator in cruce suspensus peremptus est."
  )
)
(BelegText
  (TEXT
    "illud "
  )
  (Zusatz
    "(oratorium)"
  )
  (TEXT
    " sublimatum in alto erectum "
  )
  (BelegLemma `(verdichtung "-o") `(lemma "sacro")
    "-o"
  )
  (TEXT
    " seculorum Satore seu omnium Salvatore sacratum ."
  )
)
(BelegText
  (TEXT
    "fratri ... "
  )
  (BelegLemma `(verdichtung "-r") `(lemma "sacer")
    "-r"
  )
  (TEXT
    " ... angelus ... "
  )
  (ZITAT
    "‘cras migrabis’"
  )
  (TEXT
    " ait."
  )
)
(BelegText
  (TEXT
    "itiner ... suum "
  )
  (BelegLemma `(verdichtung "-r") `(lemma "sacer")
    "-r"
  )
  (TEXT
    " angelus istis prosequitur dictis: "
  )
  (Zusatz
    (ZITAT
      "‘eqs.’"
    )
  )
)
(BelegText
  (TEXT
    "sunt cellule "
  )
  (Zusatz
    "(sc. cerebri)"
  )
  (TEXT
    " tres, in quibus nihil agitur absque miraculo, unde apud omnes naciones partes ille dicuntur "
  )
  (BelegLemma `(verdichtung "-e") `(id "sacre") `(globaleId "sacer1-test.sacre") `(lemma "sacre")
    "-e"
  )
  (TEXT
    " "
  )
  (Zusatz
    "eqs"
  )
  (TEXT
    "."
  )
)
(BelegText
  (TEXT
    "cum ... ille "
  )
  (BelegLemma `(verdichtung "-r") `(lemma "sacer") `(id "sacerA33") `(globaleId "sacer1-test.sacerA33")
    "-r"
  )
  (TEXT
    " sanctus viator Bonifatius ... salubria missarum commercia peregisset "
  )
  (Zusatz
    "eqs"
  )
  (TEXT
    "."
  )
)
(BelegText
  (TEXT
    "domine mi "
  )
  (Zusatz
    "(sc. archiepiscopi)"
  )
  (TEXT
    " gloriosissime, constans et "
  )
  (BelegLemma `(verdichtung "-r") `(lemma "sacer")
    "-r"
  )
  (TEXT
    " imperator."
  )
)
(BelegText
  (TEXT
    "patriarcharum "
  )
  (BelegLemma `(verdichtung "-r") `(lemma "sacer") `(id "sacerA35") `(globaleId "sacer1-test.sacerA35")
    "-r"
  )
  (TEXT
    " ordo."
  )
)
(BelegText
  (TEXT
    "per "
  )
  (BelegLemma `(verdichtung "-e") `(lemma "sacre") `(id "sacerA17") `(globaleId "sacer1-test.sacerA17")
    "-e"
  )
  (TEXT
    " nimium "
  )
  (BelegLemma `(verdichtung "-a") `(id "sacerA18") `(globaleId "sacer1-test.sacerA18") `(lemma "sacra")
    "-a"
  )
  (TEXT
    " verba Verenę."
  )
)
(BelegText
  (TEXT
    "inclusi ... ad plantas volvere "
  )
  (BelegLemma `(verdichtung "-as") `(id "sacras") `(globaleId "sacer1-test.sacras") `(lemma "sacras")
    "-as"
  )
  (TEXT
    " sua corpora."
  )
)
(BelegText
  (BelegLemma `(verdichtung "-as") `(lemma "sacras")
    "-as"
  )
  (TEXT
    " protendite "
  )
  (Zusatz
    "(sc. fratres)"
  )
  (TEXT
    " palmas."
  )
)
(BelegText
  (BelegLemma `(verdichtung "-is") `(id "sacris") `(globaleId "sacer1-test.sacris") `(lemma "sacris")
    "-is"
  )
  (TEXT
    " nostris obtutibus obtulit "
  )
  (Zusatz
    "Adelgisus"
  )
  (TEXT
    " verissimas auctoritates immunitatum ... predecessorum nostrorum imperatorum atque regum."
  )
)
(BelegText
  (BelegLemma `(verdichtung "-ae") `(id "sacrae") `(globaleId "sacer1-test.sacrae") `(lemma "sacrae")
    "-ae"
  )
  (TEXT
    " mos est conversationis, ut praeferre se ceteris pertimescat, qui subici aliis rennuerit."
  )
)
(BelegText
  (BelegLemma `(verdichtung "-as") `(lemma "sacras")
    "-as"
  )
  (TEXT
    " funde "
  )
  (Zusatz
    "[sc. Gallus]"
  )
  (TEXT
    " preces Domino."
  )
)
(BelegText
  (TEXT
    "lingua ... pia vitaque iure "
  )
  (BelegLemma `(verdichtung "-a") `(lemma "sacra")
    "-a"
  )
  (TEXT
    "."
  )
)
(BelegText
  (TEXT
    "ut ... nihil ex his, que antecessores mei et ego iuste et legaliter per "
  )
  (ROEMISCHE_ZAHL
    "XXX"
  )
  (TEXT
    " annos tenuimus, "
  )
  (BelegLemma `(verdichtung "-errimi") `(id "sacerA31") `(globaleId "sacer1-test.sacerA31") `(lemma "sacerrimi")
    "-errimi"
  )
  (TEXT
    " imperii vestri "
  )
  (Zusatz
    "[sc. imperatoris]"
  )
  (TEXT
    " perdat diebus."
  )
)
(BelegText
  (BelegKern
    (TEXT
      "ut pro opere tibimet expenso"
    )
    (L
      " "
    )
    (BelegLemma `(verdichtung "-is") `(lemma "sacris")
      "-is"
    )
    (L `(spatium "2")
      "  "
    )
    (Sperrung
      "orationibus"
    )
    (L `(spatium "2")
      "  "
    )
    (TEXT
      "me remuneres"
    )
    (L
      " "
    )
    (Zusatz
      "(sc. Frechulfus)"
    )
    (L
      " "
    )
    (MEHRZEILER)
    (Einschub
      (Belege
        (Beleg
          (Quellenangabe `(autor "Hraban.") `(werk "epist.") `(class "kein_rechter_rand")
            (BelegStelle
              (Stellenangabe
                (Stelle `(autor "Hraban.") `(werk "epist.") `(stelle "24 p. 431,8")
                  "(24 p. 431,8"
                )
              )
              (L
                " "
              )
              (BelegText
                (BelegLemma `(verdichtung "-as") `(lemma "sacras")
                  "-as"
                )
                (TEXT
                  " funde "
                )
                (Zusatz
                  "[sc. Gallus]"
                )
                (TEXT
                  " preces Domino."
                )
              )
            )
          )
        )
        (L
          " "
        )
        (Beleg
          (Quellenangabe `(autor "Agius") `(werk "epic. Hath.") `(class "kein_rechter_rand")
            (Quelle
              (Autor
                "Agius"
              )
              (L
                " "
              )
              (Werk
                "epic. Hath."
              )
            )
            (L
              " "
            )
            (BelegStelle
              (Stellenangabe
                (Stelle `(autor "Agius") `(werk "epic. Hath.") `(stelle "704")
                  "704"
                )
              )
              (L
                " "
              )
              (BelegText
                (TEXT
                  "lingua ... pia vitaque iure "
                )
                (BelegLemma `(verdichtung "-a") `(lemma "sacra")
                  "-a"
                )
                (TEXT
                  "."
                )
              )
            )
          )
        )
        (L
          " "
        )
        (Beleg
          (Quellenangabe `(autor "Rather.") `(werk "epist.") `(class "kein_rechter_rand")
            (Quelle
              (Autor
                "Rather."
              )
              (L
                " "
              )
              (Werk
                "epist."
              )
            )
            (L
              " "
            )
            (BelegStelle
              (Stellenangabe
                (Stelle `(autor "Rather.") `(werk "epist.") `(stelle "22 p. 117,9")
                  "22 p. 117,9"
                )
              )
              (L
                " "
              )
              (BelegText
                (TEXT
                  "ut ... nihil ex his, que antecessores mei et ego iuste et legaliter per "
                )
                (ROEMISCHE_ZAHL
                  "XXX"
                )
                (TEXT
                  " annos tenuimus, "
                )
                (BelegLemma `(verdichtung "-errimi") `(id "sacerA31") `(globaleId "sacer1-test.sacerA31") `(lemma "sacerrimi")
                  "-errimi"
                )
                (TEXT
                  " imperii vestri "
                )
                (Zusatz
                  "[sc. imperatoris]"
                )
                (TEXT
                  " perdat diebus."
                )
              )
            )
            (L
              " "
            )
            (BelegErgänzung
              (Zusätze
                (L
                  " "
                )
                (Zusatz
                  (TEXT
                    "persaepe. v. et"
                  )
                  (L
                    " "
                  )
                  (Verweise
                    (a `(href "#sacerA18") `(alias "⇒") `(globaleReferenz "sacer1-test.sacerA18"))
                  )
                  (TEXT
                    "."
                  )
                )
              )
            )
          )
        )
      )
      (ZOMBIE__
        ""
        "                    "
        "        UU_BEDEUTUNG de rebus:"
        ""
        "            UUU_BEDEUTUNG in univ.:"
        ""
        '                 * HRABAN. carm.; 4,1,7 /*ID 3324 [p. 166] [s. IX.^1]*/ "flatus ... #sace.|r iste tibi, numine claro, permanet aequalis."'
        '                 * WALAHFR. carm.; 77,11,2 /*ID 3520 [p. 416] [c. 825/49]*/ "#sacr.|o /*{ANKER LANG}*/ scapulas feretro ossa gesturus preciosa subdit {[sc. imperator]} martyris almi {[sc. Ianuarii]}."'
        '                 * EPITAPH. var. II; 22,1 /*ID 1762 [p. 293] [s. X./XI.^1]*/ "hic fratrum numero memorantur nomina #sacr.|o/*{ANKERLANG}*/, rite ferunt Domino qui laudes et quoque grates."'
        '                 * THIETM. chron.; 4,50 /*ID 3489 [p. 188] [a. 1012/18]*/ "abiit {archiepiscopus} ac #sacr.|am mox lanceam remisit." '
        '                 * MATTH. PLATEAR. [?] gloss.; add. p. 396^H /*ID 1793 [p. 396] [s. XII.] */ "hiera, id est, #sacr.|a."'
        "                  {persaepe. v. et {=> sacerA29; sacerA40}}"
        ""
        "            UUU_BEDEUTUNG in nomine loci:"
        ""
        '                UUUU_BEDEUTUNG "Mons S-r":'
        ""
        '                 * INNOC. III. registr.; 1,427 p. 637,15 /*ID 1786 [p. 637] [a. 1198]*/ "abbati et monachis Montis #S|.acr.|i {[v. notam ed.]}."'
        "                    "
        '                UUUU_BEDEUTUNG "-um palatium"; cf. /*ZITIERWEISE PRÜFEN*/ W. Falkowski, Wahrnehmung des königlichen palatium im westfränkischen Reich im 9. und 10. Jahrhundert. in: Biuletyn Polskiej Misji 5. 2009. p. 187sqq.:'
        ""
        '                 * CONC. Karol. A; 19 p. 131,3 /* ID 1725 Hinweis Marie-Luise PRÜFEN [a. 794]*/ "_____________________________."'
        '                 * CAPIT. reg. Franc.; 28,3 /* ?nicht Zettelmaterial MLW? Hinweis Marie-Luise PRÜFEN [a. 794]*/ "__________________________________" '
        '                 * HRABAN. epist.; 13 p. 401,32 /*ID 3352 [a. 826/29]*/ "domino ... Hilduino abbati et #sacr.|i palatii archicapellano Hrabanus exiguus servorum Dei servus ... optat salutem."'
        '                 * DIPL. Ludow. Germ.; 2 p. 2,37 /*ID 1745 [p. 2] [a. 830]*/ "Gauzbaldus #sacr.|i palatii nostri summus cappellanus ... adiens excellentiam nostram innotuit clementię nostrę, qualiter {eqs}."'
        '                 * CAPIT. reg. Franc.; 297,5 p. 431,3 /* ?nicht Zettelmaterial MLW? PRÜFEN Hinweis Marie-Luise [a. 858]*/ "palatium vestrum {[sc. imperatoris]} debet esse #sacr.|um et non sacrilegum."'
        '                /* * HINCM. epist.; 126 p. 63,4 /*ID 1779 [a. 859]*/ "scio vos {[sc. Karolum imperatorem]} dolere de istis malis, quae ... in regno vestro fiunt et, quod ... plus horrendum ... est, in palatio vestro, quod #sacr.|um appellari et esse debet."'
        '                 * DIPL. Karoli III.; 25 p. 41,23 /*ID 1742 [p. 41] [a. 880] "civitate Papia in #sacr.|o palacio."*/'
        "                    "
        '                UUUU_BEDEUTUNG "Via S-a" q. d.:'
        ""
        '                 * ALBERT. MIL. temp.; 63 /*ID 1817 [p. 400] [s. XIII.^ex.]*/ "hic {[sc. Felix papa]} fecit basilicam sanctorum /*{ANKER sanctus}*/ Cosme et Damiani in urbe Roma, qui appellatur Via #S|.acr.|a, iusta templum urbis Rome."'
        ""
        "            UUU_BEDEUTUNG iuncturae notabiliores:"
        ""
        '                UUUU_BEDEUTUNG  "aedes -ae" i. q. palatium imperiale -- Kaiserpalast; aliter: {=> sacerA37}: '
        ""
        '                 * DIPL. Conr. III.; 224 p. 397,12 /*ID 1739 [a. 1150]*/ "quanto fidei et humilitatis studio in #sacr.|is edibus gloriosi imperii tui susceperis {[sc. Emanuel imperator Conradum imperatorem]}."'
        "                "
        '                UUUU_BEDEUTUNG "ager -r" i. q. universitas fidelium, in Deum credentium -- Gesamtheit der an Gott Glaubenden, Christenheit:'
        "                "
        '                 * POETA SAXO; 3,198 /*ID 3450 [p. 35] [c. 888]*/ "dira venenifere conatus semina sectae, quae Felix ... sparserat, vellere de #sacr.|o {@sacerA19} Domini radicitus agro catholicus princeps synodum celebrare vocatos undique pontifices iam dictam fecit ad aulam."'
        "                    "
        '                UUUU_BEDEUTUNG "amor -r, dilectio -a" i. q. amor Dei, Christi -- Liebe zu Gott, Christus; usu abund.: {=> sacerA1}:'
        "                "
        '                 * HRABAN. epist.; 20 p. 425,25 /*ID 3358 [c. 835/40]*/"plurimi #sacr.|a dilectione incitati ad obsequium venerationis tuae {[sc. Otgarii archiepiscopi]} concurrunt."'
        '                 * CAND. FULD. Eigil. II; 21,2 /*ID 3277 [p. 113] [c. 840]*/ "Aegil, subiectis #sacr.|o /*{ANKERLANG}*/ connexus amore."'
        '                 * CARM. libr. II A; 1,23 /*ID 1708 [p. 168] [s. IX./X.]*/ "quem {[sc. sanguinem]} fundere Christi fecit amor #sacr.|us {@sacerA1}."'
        "                    "
        '                UUUU_BEDEUTUNG "apices -i, litterae -ae" i. q. litterae imperiales -- Kaiserbrief; aliter: {=> sacerA38}:'
        "                "
        '                 * GERB. epist.; 45 p. 75,3 /*ohne ID Einleitung: Gerbert durch Brief Theophanus an Spanienreise gehindert [a. 984/85]*/ "#sacr.|is {@sacerA36} litteris dominę nostrę Teuphanu imperatricis semper augustę ... prioribus divellimur ceptis."'
        '                 * CONST. imp. II; 249 p. 339,31 /*ID 1730 [a. 1244]*/ "hec igitur, que ad bonum pacis facta sunt ..., per #sacr.|os [sacrosanctos/*{ANKER sacrosanctus}*/ {var. l.}] apices in publicam deferimus notionem."'
        "                    "
        '                UUUU_BEDEUTUNG "carmina -a, laus -a" i. q. carmina ad laudem Dei prolata -- Gesang zum Lob Gottes, Lobpreis:'
        "                "
        '                 * HRABAN. carm.; 23,5 /*ID 3335 [p. 187] [a. 817/22]*/"carmina #sacr.|a /*{ANKERLANG}*/ tibi resonent, ... sit semper Christus carmen in ore tuo {[sc. Gerhohi]}."'
        '                 * WALAHFR. Mamm.; 26,6 /*ID 3543 [p. 92] [paulo ante 825]*/ "ad laudis cumulum libet addere quaedam, dona quibus domini, servi quoque facta fidelis narrentur mercesque humili sit plena poetae munere de proprio #sacr.|ae /*{ANKERLANG}*/ superaddere laudis."'
        "                 "
        '                UUUU_BEDEUTUNG "cena -a" {@sacerA45} i. q. convivium paradisiacum in remunerationem fidelium Christi datum -- Festmahl im Paradies zur Belohnung der gläubigen Christen; in imag.:'
        "                "
        '                 * WALTH. SPIR. Christoph. II; 3,230 /*ID 3577 [p. 39] [c. 982/83] ut, cum pro merito reprobos susceperit ordo, aecclesia natis illibans oscula patris in paradisiaci sistat pratensibus agri, qua per circuitum redolet fraglantia florum;"*/ "fulchraque precipuis adhibens lucentia gemmis invitat {[sc. ecclesia electos Dei]} #sacr.|ę secum ad mensalia cęnę et tandem rutilis exornans colla coronis pro duro gratam tribuet certamine palmam." {cf. {=> sacerA46}}'
        ""
        '                UUUU_BEDEUTUNG "diadema -um" i. q. corona imperialis -- Kaiserkrone:'
        "                "
        '                 * CARM. de Frid. I. imp.; 244 /*ID 1705 [p. 9] [a. 1162/66]*/ "superest regni caput olim visere Romam, ut sumat diadema #sacr.|um/*{ANKERKURZ}*/."'
        "                    "
        '                UUUU_BEDEUTUNG "disciplina[e] -a[e]" i. q. virtutes monachis discendae -- von Mönchen zu erlernende Disziplin:'
        "                "
        '                 * HUGEB. Willib.; 6 p. 105,21 /*ID 3406 [c. 778]*/ "ibidem {[sc. Eihstat]} #sacr.|am monasterialis vitae disciplinam in usum prioris vitae ... exercebat."'
        '                 * COZROH. praef.; p. 2,12 /*ID 3279 [p. 2] [c. 824]*/ "quem {[sc. Cozroh]} ... ipse {[sc. Hitto episcopus]} suis #sacr.|is disciplinis edocuit."'
        "                    "
        '                UUUU_BEDEUTUNG "divitiae -ae" i. q. divitiae perennes Paradisi -- unvergänglicher Reichtum im Paradies:'
        "                "
        '                 * HRABAN. epist.; 20 p. 425,29 /*ID 3359 [c. 835/40]*/ "#sacr.|is divitiis, quas habundanter habes {[sc. Otgarius]} in sapientia et scientia divinae legis."'
        "                    "
        '                UUUU_BEDEUTUNG "gradus, ordo -r, ordines -i" i. q. gradus ecclesiasticus -- Weihegrad; aliter: {=> sacerA35}:'
        "                "
        '                 * ARBEO Emm.; 11 p. 42,2 [rec. A] /*ID 3243 [c. 772]*/ "ut omnibus in <<#sacr.|is constitutis ordinibus>> [#sacr.|o ordine sublimatis rec. B] denuntiare debeas {[sc. Wolfleicus mandata]}."'
        '                 * LIUTG. Greg.; 8 /*ID 3433 [p. 73] [c. 790]*/ "Bonifatius Romae ... #sacr.|o gradu sublimatus est."'
        '                /* * THEGAN. Ludow.; 20 p. 208,15 ID 3480 [a. 837/38] "propinqui eorum {[sc. episcoporum humili genere natorum]} ... ad #sacr.|um ordinem pertrahuntur."'
        '                 * HRABAN. epist.; 40 p. 479,12 ID 3379 [a. 835/47] "ut, cum ordinati fuerint {[sc. ad divinum officium promovendi]} et #sacr.|is ordinibus sublimati, magis populo Dei prosint quam noceant."*/'
        '                 * FROUM. carm.; 10,10 /*ID 3300 [p. 37] [s. X.^ex.])*/ "cum benedixisset {(sc. Christus)} benedictos dextera summi presbiteros faceretque suos #sacr.|o ordine servos."'
        '                 * BERTH. chron. B; 1075 p. 222,16 (epist. papae) /*ID 3254 Brief Papst Gregors VII. an Bischof Otto I. von Konstanz (a. 1054/80)*/ "ut hi, qui ... interventu precii ad aliquem #sacr.|orum ordinum gradum vel officium promoti sunt {eqs}."'
        "                 {al.}"
        "                    "
        "                 ANHÄNGER meton. de diaconis, presbyteris sim. ordinatis:"
        ""
        '                  * EIGIL Sturm.; 14 p. 147,6 /*ID 3288 (a. 791/814)*/ "monasterium (#sace.|r ordo {EPB}) adventatione multorum crescebat."'
        '                  * GUNZO epist.; 1 p. 19,9 /*ID 1776 (c. 965)*/ "nuptię laicis concessę #sacr.|is ordinibus denegantur."'
        '                  * THIETM. chron.; 7,2 p. 398,19 /*ID 3496 (a. 1012/18)*/ "sanctorum instituta patrum in #sacr.|is ordinibus ... neglecta ... renovavit {imperator}."; 8,12 p. 508,10 /*ID 3500 (a. 1012/18)*/ "hii {(confratres spirituales)} sunt cooperatores #sacr.|i ordinis tui {(sc. successoris in episcopatum)} et futurae adiutores spei."'
        "                    "
        '                UUUU_BEDEUTUNG "imperium -um" ([cf. O. Hiltbrunner, op. cit.; {@sacerA41} p. 6sqq.]))'
      )
    )
    (Einschub
      (Beleg
        (Sekundärliteratur
          (Kursiv
            (Autorangabe
              "(W. Kienast, "
            )
            (L
              " "
            )
            (Werk
              (TEXT
                "Deutschland und Frankreich in der Kaiserzeit. III. "
              )
              (Erscheinungsjahr
                "1975"
              )
              (TEXT
                "."
              )
            )
            (L
              " "
            )
            (BelegStelle
              (Stellenangabe
                (Stelle `(autor "W. Kienast ") `(werk "Deutschland und Frankreich in der Kaiserzeit. III. 1975.") `(stelle "p. 675sq.")
                  "p. 675sq.)"
                )
              )
            )
          )
        )
      )
    )
    (TEXT
      "; sed cf. et comm. ed. ["
    )
    (Verweise
      (a `(href "#sacerA34") `(alias "⇒") `(globaleReferenz "sacer1-test.sacerA34"))
    )
    (TEXT
      "]; aliter:"
    )
    (L
      " "
    )
    (Verweise
      (a `(href "#sacerA31") `(alias "⇒") `(globaleReferenz "sacer1-test.sacerA31"))
    )
    (TEXT
      ":"
    )
  )
)


ut pro opere tibimet expenso -is  orationibus  me remuneres (sc. Frechulfus) (24 p. 431,8 -as funde [sc. Gallus] preces Domino. Agius epic. Hath. 704 lingua ... pia vitaque iure -a. Rather. epist. 22 p. 117,9 ut ... nihil ex his, que antecessores mei et ego iuste et legaliter per XXX annos tenuimus, -errimi imperii vestri [sc. imperatoris] perdat diebus.  persaepe. v. et .

        UU_BEDEUTUNG de rebus:

            UUU_BEDEUTUNG in univ.:

                 * HRABAN. carm.; 4,1,7 /*ID 3324 [p. 166] [s. IX.^1]*/ "flatus ... #sace.|r iste tibi, numine claro, permanet aequalis."
                 * WALAHFR. carm.; 77,11,2 /*ID 3520 [p. 416] [c. 825/49]*/ "#sacr.|o /*{ANKER LANG}*/ scapulas feretro ossa gesturus preciosa subdit {[sc. imperator]} martyris almi {[sc. Ianuarii]}."
                 * EPITAPH. var. II; 22,1 /*ID 1762 [p. 293] [s. X./XI.^1]*/ "hic fratrum numero memorantur nomina #sacr.|o/*{ANKERLANG}*/, rite ferunt Domino qui laudes et quoque grates."
                 * THIETM. chron.; 4,50 /*ID 3489 [p. 188] [a. 1012/18]*/ "abiit {archiepiscopus} ac #sacr.|am mox lanceam remisit."
                 * MATTH. PLATEAR. [?] gloss.; add. p. 396^H /*ID 1793 [p. 396] [s. XII.] */ "hiera, id est, #sacr.|a."
                  {persaepe. v. et {=> sacerA29; sacerA40}}

            UUU_BEDEUTUNG in nomine loci:

                UUUU_BEDEUTUNG "Mons S-r":

                 * INNOC. III. registr.; 1,427 p. 637,15 /*ID 1786 [p. 637] [a. 1198]*/ "abbati et monachis Montis #S|.acr.|i {[v. notam ed.]}."

                UUUU_BEDEUTUNG "-um palatium"; cf. /*ZITIERWEISE PRÜFEN*/ W. Falkowski, Wahrnehmung des königlichen palatium im westfränkischen Reich im 9. und 10. Jahrhundert. in: Biuletyn Polskiej Misji 5. 2009. p. 187sqq.:

                 * CONC. Karol. A; 19 p. 131,3 /* ID 1725 Hinweis Marie-Luise PRÜFEN [a. 794]*/ "_____________________________."
                 * CAPIT. reg. Franc.; 28,3 /* ?nicht Zettelmaterial MLW? Hinweis Marie-Luise PRÜFEN [a. 794]*/ "__________________________________"
                 * HRABAN. epist.; 13 p. 401,32 /*ID 3352 [a. 826/29]*/ "domino ... Hilduino abbati et #sacr.|i palatii archicapellano Hrabanus exiguus servorum Dei servus ... optat salutem."
                 * DIPL. Ludow. Germ.; 2 p. 2,37 /*ID 1745 [p. 2] [a. 830]*/ "Gauzbaldus #sacr.|i palatii nostri summus cappellanus ... adiens excellentiam nostram innotuit clementię nostrę, qualiter {eqs}."
                 * CAPIT. reg. Franc.; 297,5 p. 431,3 /* ?nicht Zettelmaterial MLW? PRÜFEN Hinweis Marie-Luise [a. 858]*/ "palatium vestrum {[sc. imperatoris]} debet esse #sacr.|um et non sacrilegum."
                /* * HINCM. epist.; 126 p. 63,4 /*ID 1779 [a. 859]*/ "scio vos {[sc. Karolum imperatorem]} dolere de istis malis, quae ... in regno vestro fiunt et, quod ... plus horrendum ... est, in palatio vestro, quod #sacr.|um appellari et esse debet."
                 * DIPL. Karoli III.; 25 p. 41,23 /*ID 1742 [p. 41] [a. 880] "civitate Papia in #sacr.|o palacio."*/

                UUUU_BEDEUTUNG "Via S-a" q. d.:

                 * ALBERT. MIL. temp.; 63 /*ID 1817 [p. 400] [s. XIII.^ex.]*/ "hic {[sc. Felix papa]} fecit basilicam sanctorum /*{ANKER sanctus}*/ Cosme et Damiani in urbe Roma, qui appellatur Via #S|.acr.|a, iusta templum urbis Rome."

            UUU_BEDEUTUNG iuncturae notabiliores:

                UUUU_BEDEUTUNG  "aedes -ae" i. q. palatium imperiale -- Kaiserpalast; aliter: {=> sacerA37}:

                 * DIPL. Conr. III.; 224 p. 397,12 /*ID 1739 [a. 1150]*/ "quanto fidei et humilitatis studio in #sacr.|is edibus gloriosi imperii tui susceperis {[sc. Emanuel imperator Conradum imperatorem]}."

                UUUU_BEDEUTUNG "ager -r" i. q. universitas fidelium, in Deum credentium -- Gesamtheit der an Gott Glaubenden, Christenheit:

                 * POETA SAXO; 3,198 /*ID 3450 [p. 35] [c. 888]*/ "dira venenifere conatus semina sectae, quae Felix ... sparserat, vellere de #sacr.|o {@sacerA19} Domini radicitus agro catholicus princeps synodum celebrare vocatos undique pontifices iam dictam fecit ad aulam."

                UUUU_BEDEUTUNG "amor -r, dilectio -a" i. q. amor Dei, Christi -- Liebe zu Gott, Christus; usu abund.: {=> sacerA1}:

                 * HRABAN. epist.; 20 p. 425,25 /*ID 3358 [c. 835/40]*/"plurimi #sacr.|a dilectione incitati ad obsequium venerationis tuae {[sc. Otgarii archiepiscopi]} concurrunt."
                 * CAND. FULD. Eigil. II; 21,2 /*ID 3277 [p. 113] [c. 840]*/ "Aegil, subiectis #sacr.|o /*{ANKERLANG}*/ connexus amore."
                 * CARM. libr. II A; 1,23 /*ID 1708 [p. 168] [s. IX./X.]*/ "quem {[sc. sanguinem]} fundere Christi fecit amor #sacr.|us {@sacerA1}."

                UUUU_BEDEUTUNG "apices -i, litterae -ae" i. q. litterae imperiales -- Kaiserbrief; aliter: {=> sacerA38}:

                 * GERB. epist.; 45 p. 75,3 /*ohne ID Einleitung: Gerbert durch Brief Theophanus an Spanienreise gehindert [a. 984/85]*/ "#sacr.|is {@sacerA36} litteris dominę nostrę Teuphanu imperatricis semper augustę ... prioribus divellimur ceptis."
                 * CONST. imp. II; 249 p. 339,31 /*ID 1730 [a. 1244]*/ "hec igitur, que ad bonum pacis facta sunt ..., per #sacr.|os [sacrosanctos/*{ANKER sacrosanctus}*/ {var. l.}] apices in publicam deferimus notionem."

                UUUU_BEDEUTUNG "carmina -a, laus -a" i. q. carmina ad laudem Dei prolata -- Gesang zum Lob Gottes, Lobpreis:

                 * HRABAN. carm.; 23,5 /*ID 3335 [p. 187] [a. 817/22]*/"carmina #sacr.|a /*{ANKERLANG}*/ tibi resonent, ... sit semper Christus carmen in ore tuo {[sc. Gerhohi]}."
                 * WALAHFR. Mamm.; 26,6 /*ID 3543 [p. 92] [paulo ante 825]*/ "ad laudis cumulum libet addere quaedam, dona quibus domini, servi quoque facta fidelis narrentur mercesque humili sit plena poetae munere de proprio #sacr.|ae /*{ANKERLANG}*/ superaddere laudis."

                UUUU_BEDEUTUNG "cena -a" {@sacerA45} i. q. convivium paradisiacum in remunerationem fidelium Christi datum -- Festmahl im Paradies zur Belohnung der gläubigen Christen; in imag.:

                 * WALTH. SPIR. Christoph. II; 3,230 /*ID 3577 [p. 39] [c. 982/83] ut, cum pro merito reprobos susceperit ordo, aecclesia natis illibans oscula patris in paradisiaci sistat pratensibus agri, qua per circuitum redolet fraglantia florum;"*/ "fulchraque precipuis adhibens lucentia gemmis invitat {[sc. ecclesia electos Dei]} #sacr.|ę secum ad mensalia cęnę et tandem rutilis exornans colla coronis pro duro gratam tribuet certamine palmam." {cf. {=> sacerA46}}

                UUUU_BEDEUTUNG "diadema -um" i. q. corona imperialis -- Kaiserkrone:

                 * CARM. de Frid. I. imp.; 244 /*ID 1705 [p. 9] [a. 1162/66]*/ "superest regni caput olim visere Romam, ut sumat diadema #sacr.|um/*{ANKERKURZ}*/."

                UUUU_BEDEUTUNG "disciplina[e] -a[e]" i. q. virtutes monachis discendae -- von Mönchen zu erlernende Disziplin:

                 * HUGEB. Willib.; 6 p. 105,21 /*ID 3406 [c. 778]*/ "ibidem {[sc. Eihstat]} #sacr.|am monasterialis vitae disciplinam in usum prioris vitae ... exercebat."
                 * COZROH. praef.; p. 2,12 /*ID 3279 [p. 2] [c. 824]*/ "quem {[sc. Cozroh]} ... ipse {[sc. Hitto episcopus]} suis #sacr.|is disciplinis edocuit."

                UUUU_BEDEUTUNG "divitiae -ae" i. q. divitiae perennes Paradisi -- unvergänglicher Reichtum im Paradies:

                 * HRABAN. epist.; 20 p. 425,29 /*ID 3359 [c. 835/40]*/ "#sacr.|is divitiis, quas habundanter habes {[sc. Otgarius]} in sapientia et scientia divinae legis."

                UUUU_BEDEUTUNG "gradus, ordo -r, ordines -i" i. q. gradus ecclesiasticus -- Weihegrad; aliter: {=> sacerA35}:

                 * ARBEO Emm.; 11 p. 42,2 [rec. A] /*ID 3243 [c. 772]*/ "ut omnibus in <<#sacr.|is constitutis ordinibus>> [#sacr.|o ordine sublimatis rec. B] denuntiare debeas {[sc. Wolfleicus mandata]}."
                 * LIUTG. Greg.; 8 /*ID 3433 [p. 73] [c. 790]*/ "Bonifatius Romae ... #sacr.|o gradu sublimatus est."
                /* * THEGAN. Ludow.; 20 p. 208,15 ID 3480 [a. 837/38] "propinqui eorum {[sc. episcoporum humili genere natorum]} ... ad #sacr.|um ordinem pertrahuntur."
                 * HRABAN. epist.; 40 p. 479,12 ID 3379 [a. 835/47] "ut, cum ordinati fuerint {[sc. ad divinum officium promovendi]} et #sacr.|is ordinibus sublimati, magis populo Dei prosint quam noceant."*/
                 * FROUM. carm.; 10,10 /*ID 3300 [p. 37] [s. X.^ex.])*/ "cum benedixisset {(sc. Christus)} benedictos dextera summi presbiteros faceretque suos #sacr.|o ordine servos."
                 * BERTH. chron. B; 1075 p. 222,16 (epist. papae) /*ID 3254 Brief Papst Gregors VII. an Bischof Otto I. von Konstanz (a. 1054/80)*/ "ut hi, qui ... interventu precii ad aliquem #sacr.|orum ordinum gradum vel officium promoti sunt {eqs}."
                 {al.}

                 ANHÄNGER meton. de diaconis, presbyteris sim. ordinatis:

                  * EIGIL Sturm.; 14 p. 147,6 /*ID 3288 (a. 791/814)*/ "monasterium (#sace.|r ordo {EPB}) adventatione multorum crescebat."
                  * GUNZO epist.; 1 p. 19,9 /*ID 1776 (c. 965)*/ "nuptię laicis concessę #sacr.|is ordinibus denegantur."
                  * THIETM. chron.; 7,2 p. 398,19 /*ID 3496 (a. 1012/18)*/ "sanctorum instituta patrum in #sacr.|is ordinibus ... neglecta ... renovavit {imperator}."; 8,12 p. 508,10 /*ID 3500 (a. 1012/18)*/ "hii {(confratres spirituales)} sunt cooperatores #sacr.|i ordinis tui {(sc. successoris in episcopatum)} et futurae adiutores spei."

                UUUU_BEDEUTUNG "imperium -um" ([cf. O. Hiltbrunner, op. cit.; {@sacerA41} p. 6sqq.]))(W. Kienast,  Deutschland und Frankreich in der Kaiserzeit. III. 1975. p. 675sq.); sed cf. et comm. ed. []; aliter: :
