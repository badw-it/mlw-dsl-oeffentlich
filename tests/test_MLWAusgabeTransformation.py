#!/usr/bin/python3

"""
test_MLWAusgabeTransformation.py -- testet die AusgabeTransformation
aus MLWCompiler.py
"""

import copy, re

from MLWCompiler import MLWCompiler, get_grammar, get_transformer, AusgabeTransformation, \
    WARNUNG_OP_CIT_NICHT_ZUZUORDNEN, TextÄnderungenMixin, HINWEIS_DISAMIGUIRUNG_DER_AUTORANGABE
from DHParser.nodetree import RootNode, pick_from_path, parse_sxpr, parse_xml, flatten_sxpr, \
    has_class, ContentMapping
from DHParser import testing


POSSIBLE_ARTIFACTS = set(testing.POSSIBLE_ARTIFACTS) | {WARNUNG_OP_CIT_NICHT_ZUZUORDNEN}
TAM = TextÄnderungenMixin()


class TestAusgabeTransformation:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.at = AusgabeTransformation()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        return self.mlw_compiler(st)

    def test_davor(self):
        tree = parse_sxpr('(A (TEXT hallo))')
        node = copy.deepcopy(tree)
        node = self.at.davor(node, "*")
        assert node.as_sxpr() == '(A (TEXT "*hallo"))'
        node = copy.deepcopy(tree)
        node = self.at.davor(node, "*", 'kursiv neu')
        assert node.as_sxpr() == '(A (TEXT `(class "kursiv neu") "*") (TEXT "hallo"))'
        node = self.at.davor(node, "#", 'kursiv')
        assert node.as_sxpr() == '(A (TEXT `(class "kursiv neu") "#*") (TEXT "hallo"))'
        tree = parse_sxpr('(A (B hallo))')
        node = copy.deepcopy(tree)
        node = self.at.davor(node, "Präfix")
        assert node.as_sxpr() == '(A (TEXT "Präfix") (B "hallo"))'
        tree = parse_sxpr('(A "Welt!")')
        node = copy.deepcopy(tree)
        node = self.at.davor(node, "Hallo ")
        assert node.as_sxpr() == '(A "Hallo Welt!")'
        tree = parse_sxpr('(A "*Hallo")')
        node = copy.deepcopy(tree)
        node = self.at.davor(node, "*")
        assert node.as_sxpr() == '(A "*Hallo")'
        node = self.at.davor(node, "*", allow_duplication=True)
        assert node.as_sxpr() == '(A "**Hallo")'

    def test_danach(self):
        tree = parse_sxpr('(A (TEXT hallo))')
        node = copy.deepcopy(tree)
        node = self.at.danach(node, "*")
        assert node.as_sxpr() == '(A (TEXT "hallo*"))'
        node = copy.deepcopy(tree)
        node = self.at.danach(node, "*", 'kursiv neu')
        assert node.as_sxpr() == '(A (TEXT "hallo") (TEXT `(class "kursiv neu") "*"))'
        node = self.at.danach(node, "#", 'kursiv')
        assert node.as_sxpr() == '(A (TEXT "hallo") (TEXT `(class "kursiv neu") "*") (TEXT `(class "kursiv") "#"))'
        tree = parse_sxpr('(A (B hallo))')
        node = copy.deepcopy(tree)
        node = self.at.danach(node, "Postfix")
        assert node.as_sxpr() == '(A (B "hallo") (TEXT "Postfix"))'
        tree = parse_sxpr('(A "hallo")')
        node = copy.deepcopy(tree)
        node = self.at.danach(node, " du da!")
        assert node.as_sxpr() == '(A "hallo du da!")'
        tree = parse_sxpr('(A "Hallo")')
        node = copy.deepcopy(tree)
        node = self.at.danach(node, "*")
        assert node.as_sxpr() == '(A "Hallo*")'
        node = self.at.danach(node, "*", allow_duplication=True)
        assert node.as_sxpr() == '(A "Hallo**")'
        tree = parse_sxpr('(TEXT "  hallo  ")')
        node = copy.deepcopy(tree)
        node = self.at.danach(node, '.')
        assert node.content == '  hallo.  '
        node = copy.deepcopy(tree)
        node = self.at.davor(node, '.')
        assert node.content == '  .hallo  '

    def test_unmittelbar_davor(self):
        tree = parse_sxpr('(A (B (C (D "1") (E "2"))) (C "3"))')
        node = copy.deepcopy(tree)
        self.at.unmittelbar_davor(node, '(')
        assert node.content == '(123'
        self.at.unmittelbar_davor(node, '(')
        assert node.content == '(123'
        self.at.unmittelbar_danach(node, ')')
        assert node.content == '(123)'
        assert node.as_sxpr() == '(A (B (C (D "(1") (E "2"))) (C "3)"))'

    def test_unmittelbar_danach(self):
        variante = parse_xml("""<Variante>
            <Beschreibung>-rto(s)</Beschreibung>
            <L> </L>
            <Beleg>
              <Verweise>
                <a href="#sacerdos_3" alias="⇒" globaleReferenz="sacerdos.sacerdos-3"></a>
              </Verweise>
            </Beleg>
          </Variante>""")
        tr = AusgabeTransformation()
        rn = tr(RootNode(variante))
        assert flatten_sxpr(rn.as_sxpr()) == '(Variante (Beschreibung "-rto(s): ") ' \
                                             '(L " ") (Beleg (Verweise (a `(href "#sacerdos_3") `(alias "⇒") ' \
                                             '`(globaleReferenz "sacerdos.sacerdos-3"))) (TEXT ".")))'

    def test_on_LemmaVarianten(self):
        node = RootNode(parse_sxpr('(LemmaVarianten (LemmaVariante (TEXT "fasc-")) '
                                   '(LemmaVariante (TEXT "-iet-")) (Zusatz "sim."))'))
        node = self.at(node)
        assert node.content == '(fasc-, -iet- sim.)'

    def test_on_GVariante(self):
        node = RootNode(parse_sxpr('(GVariante (flexion (FLEX "-um") (FLEX "-i")) (genus "neutrum"))'))
        node.pick('genus').attr['verdichtung'] = 'n.'
        node = self.at(node)
        assert node.content == "-um, -i n."

    def test_on_wortart(self):
        nd = parse_sxpr('(wortart (TEXT "littera") (L " ") (wa_ergänzung "octava alphabeti Latini"))')
        nd = self.at.on_wortart(nd)
        assert flatten_sxpr(nd.as_sxpr()) == \
               '(wortart `(wortart "littera octava alphabeti Latini") `(class "kursiv") ' \
               '(Kursiv (TEXT "littera") (L " ") ' \
               '(wa_ergänzung "octava alphabeti Latini")))'

    def test_on_Nebenbedeutungen(self):
        nd = parse_sxpr('(Nebenbedeutungen (TEXT "(") (Klassifikation "usu liturg.")'
                        '(L " ") (Zusatz (TEXT "saepe")) (TEXT ")"))').with_pos(0)
        nd = self.at.on_Nebenbedeutungen(nd)
        assert nd.content == '(usu liturg.; saepe)'

        nd = parse_sxpr('(Nebenbedeutungen (TEXT "(") (Klassifikation "usu liturg.") '
                        '(L " ") (Klassifikation "de re v.") (L " ") '
                        '(NBVerweise (Verweise (a `(href "eintrag_ibi_X") "eintrag_ibi_X"))) '
                        '(TEXT ")"))').with_pos(0)
        self.at.path = [nd]
        nd = self.at.on_Nebenbedeutungen(nd)
        assert nd.content == '(usu liturg.; de re v.: eintrag_ibi_X)'

    def test_Klammer_Kursivierung(self):
        st = parse_sxpr('(Testdaten (TEXT "(") (Zusatz "psalmis") (TEXT ")"))')
        self.at.Klammer_Kursivierung(st)
        assert st[0].get_attr('class', '').find('kursiv') >= 0
        assert st[-1].get_attr('class', '').find('kursiv') >= 0


    def test_Klammer_Kursivierung2(self):
        rn = self.compile('"... redegit {(sc. Herodes)}(({sim.} "ecclesiam ... profanissime"))."',
                          "BelegText")
        self.at(rn)
        opening = rn.pick(lambda nd: nd.content == '(')
        assert opening.has_attr('class') and opening.attr['class'].find('grade') >= 0

        rn = self.compile('''"'scientiam', id est spiritualem intellectum, '#sacrifcation.|um' '''
                          '''({PG,3,508^A} ἱερουργιῶν), id est sanctificationes, desiderantes, '''
                          '''scilicet perficiendos ad hoc tendentes. "''', "BelegText")
        self.at(rn)
        assert all(nd.get_attr('class', '').find('grade') <= 0 for nd in
                   rn.select_if(lambda n: not n.children))

        rn = self.compile('''"'diviniores', id est digniores, '#sacrification.|es' ({PG '''
                          '''3,505^C} ἁγιαστείαν), sicut est consecratio chrismatis et altaris'''
                          ''' {(sc. divina lex distribuit pontificali ordini)}. "''', "BelegText")
        self.at(rn)
        assert all(nd.get_attr('class', '').find('grade') <= 0 for nd in
                   rn.select_if(lambda n: not n.children))

        rn = self.compile('''"item candy [gloss.: #sacharum] in aqua frigida infusum '''
                          '''frequenter teneatur in ore"''', "BelegText")
        self.at(rn)  # sollte einfach keinen Fehler liefern...

        rn = self.compile('''"vas illud #sacra.|tum quasi simulacrum fecit {puerulus} demonum '''
                          '''(({spectat ad} $VULG.; 2 Tim. 2,21 sanctificatum))."''', "BelegText")
        self.at(rn)  # no further tests. This just should not raise any errors

        rn = self.compile('''"non videtur absurdum, si ... Christum vinum aqua mistum '''
                          '''({ci.,} mistam {ed.}) #sacramentalite.|r consecrasse intelligamus."''',
                          "BelegText")
        self.at(rn)
        for klammer in rn.select_if(lambda nd: nd.content.find('(') >= 0 or nd.content.find(')') >= 0):
            assert klammer.get_attr('class', '').find('kursiv') >= 0

        rn = self.compile('''BONIF. epist.; 48 p. 77,15 "ut in gratia Dei semper hic et 
                             {!in futuro} #saecul.|o permaneatis {(sc. Gripho)} 
                             ((* HRABAN. epist.; 47 p. 502,33 
                             "in presenti vita ... et in f. #se|.cul.|o." {saepius}))."''',
                          'Belege')
        self.at(rn)
        for klammer in rn.select_if(lambda nd: not nd.children and
                                               (nd.content.find('(') >= 0 or nd.content.find(')') >= 0)):
            assert klammer.get_attr('class', '').find('kursiv') >= 0 \
                   or klammer.name in ('Zusatz')

    def test_Klammerkursivierung3(self):
        rn = self.compile('fort. spectat ad inopiam, defectionem #sal.|is; '
                          'ni subest confusio c. {/sol}:', 'Bedeutungsangabe')
        rn = self.at(rn)
        for path in rn.select_path_if(lambda p: p[-1].content.strip() in ('(', ')', ':')):
            assert not self.at.ist_kursiv(path)
            # assert node.name == 'Grade' or node.get_attr('class', '').find('grade') >= 0

    def test_Klammerkursivierung4(self):
        def ist_kursiv(ctx):
            for nd in reversed(ctx):
                if nd.name in {'Kursiv', 'Zusatz'} or has_class(nd, 'kursiv'):
                    return True
                elif nd.name == 'Grade' or has_class(nd, 'grade'):
                    return False
            return False

        belege = '''* ANNAL. Plac.; a. 1284 p. 580,10 
          "in terra nostra oriuntur elephantes, dromidarii, camelli, ... #caxiaten.|is
          ((*{cf.} G. Oppert, Der Presbyter Johannes in Sage und Geschichte. ^{2}1870.;
            p. 169 l. 35 "cametennus [var. l.; om. pars codd.]"
            * {et} B. Wagner, Die Epistola presbiteri Johannis.; 2000. p. 433 l. 143 "cametecurnis"))."'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        ctx = rn.pick_path(lambda ctx: ctx[-1].content == '(')
        assert not ist_kursiv(ctx)
        ctx = rn.pick_path(lambda ctx: ctx[-1].content == ')', reverse=True)
        assert not ist_kursiv(ctx)

    def test_KlammerKursivierung5(self):
        grammatik = '''
            GRAMMATIK subst. II; -i m.
                f.: {=> sandalus_01}'''
        rn = self.compile(grammatik, 'GrammatikPosition')
        rn = self.at(rn)
        for nd in rn.select_if(lambda n: n.content in ('(', ')')):
            assert nd.name == "Kursiv"

    def test_KlammerKursivierung6(self):
        belege = '''* REGISTR. Austr. I; 240 p. 60 "in Michelhausen II villicationes, que debent #seignari 
        ({O}, #saginari {H})
  (({sim.} 261 p. 67 "curia debet #segnari [{O}, #seiginari {H}].";
     156 p. 113 "villicatio ... #seiginari [{O}, #sagenari {H}] debet.";
     157 p. 114 "villicatio debet #seignari [{O}, #saginari {H}]"))." {ibid. al.}'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        for ctx in rn.select_path_if(lambda ctx: not ctx[-1].children and
                                                 any(ctx[-1].content.find(bracket) >= 0
                                                     for bracket in ('(', ')', '[', ']'))):
            for nd in ctx[::-1]:
                assert nd.name != 'Grade' and not has_class(nd, 'grade')
                if nd.name == 'Kursiv' or has_class(nd, 'kursiv'):
                    break
            else:
                assert False, str([nd.name for nd in ctx])

    def test_Klammerkursivierung7(self):
        ep = """
            ETYMOLOGIE
                theod. vet. sazzan, sezzan: [* {cf.} O. Schade, Altdt. Wörterb. 1882.; II. p. 746 s. v. {/satjan}]"""
        rn = self.compile(ep, 'EtymologiePosition')
        rn = self.at(rn)
        assert all(nd.name == 'Grade' for nd in rn.select(lambda nd: nd.content.strip() in ('[', ']')))

    def test_Klammerkursivierung8(self):
        bt = '''"#sou|.mari.|o ({ci.}, #screiniario {@ sagmarius_16} {ed.}, s<...>imario {cod.}) 
                 ((* {cf.} F. Pfeiffer, Rheinische Transitzölle im Mittelalter. 1997.; p. 128 adn. 209)) IIII"'''
        rn = self.compile(bt, "BelegText")
        rn = self.at(rn)
        open_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find('(') >= 0)
        close_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find(')') >= 0)
        assert TAM.ist_kursiv(open_ctx)
        assert TAM.ist_kursiv(close_ctx)

    def test_Klammerkursivierung9(self):
        bt = '''"Christo pervium (pervia {var. l.}) {(v. notam ed.)}."'''
        rn = self.compile(bt, "BelegText")
        rn = self.at(rn)
        open_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find('(') >= 0)
        close_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find(')') >= 0)
        assert not TAM.ist_kursiv(open_ctx)
        assert not TAM.ist_kursiv(close_ctx)

    def test_Klammerkursivierung10(self):
        bt = '''"munditia (({sim.} 6,3 p. 91,83 "#sanct.|um ... a philosopho sic diffinitur: 'sanctitas est {eqs.}'"))."'''
        rn = self.compile(bt, "BelegText")
        rn = self.at(rn)
        open_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find('(') >= 0)
        close_ctx = rn.pick_path(
            lambda ctx: not ctx[-1].children and ctx[-1].result.find(')') >= 0)
        assert TAM.ist_kursiv(open_ctx)
        assert TAM.ist_kursiv(close_ctx)

    def test_Klammerkursivierung11(self):
        bl = '''* SERVITIA mensae; ((cod. Guelf. Gud. 131 f. 20^v)) {(a. corr.)}'''
        rn = self.compile(bl, 'Belege')
        rn = self.at(rn)
        schließende = rn.pick_path(lambda ctx: ctx[-1].content[-1:] == ')', reverse=True)
        assert not self.at.ist_kursiv(schließende)

    def test_grade_nach_grader_Klammer(self):
        tst = '''
        partic.:
            es- {(ni subest "exsculpo")}: {=> sculpo5} {=> sculpo12}'''
        rn = self.at(self.compile(tst, 'Position'))
        colon_path = rn.pick_path(lambda p: not p[-1].children and p[-1].content.strip() == ':')
        assert not self.at.ist_kursiv(colon_path)

    def test_grade_nach_Kursivierung(self):
        # Ticket 435
        belegtext = '''"... (suum {var. l.}) (id est 'suise' {add. 9}; gloss. 30: 'hessehunt' {[v. notam ed.]}) primum"'''
        rn = self.compile(belegtext, "BelegText")
        rn = self.at(rn)
        id_est = rn.pick_path(lambda path: path[-1].content.find('id est') >= 0)
        assert not self.at.ist_kursiv(id_est)

    def test_Einschub_Klammern_Bedeutungsangabe(self):
        bedeutungsangabe = '''aedificium sarciendum, pars aedificii vitiosa
             -- reparaturbedürftiges Gebäude, Gebäudeschaden
             ((* {spectat ad} $VULG. IV reg.; 12,6 ((* {ex} $LXX IV reg.; 12,6 "τό βεδεκ"))))
             ((* {cf.} E. A. Sophocles, Greek Lexicon of the Roman and Byzantine Periods. I. 1887.; p. 304)):'''
        rn = self.compile(bedeutungsangabe, 'Bedeutungsangabe')
        rn = self.at(rn)
        assert rn.content == '''aedificium sarciendum, pars aedificii vitiosa — ''' \
                             '''reparaturbedürftiges Gebäude, Gebäudeschaden ''' \
                             '''(spectat ad Vulg. IV reg. 12,6 [ex LXX IV reg. 12,6 τό βεδεκ]; ''' \
                             '''cf. E. A. Sophocles, Greek Lexicon of the Roman and Byzantine Periods. I. 1887. p. 304):'''
        assert not self.at.ist_kursiv(rn.pick_path(lambda ctx: not ctx[-1].children and ctx[-1].content.find(';') >= 0))

    def test_Einschub_Klammern_BelegText1(self):
        belege = '''* EKKEB. SCHON. opusc.; 11^I p. 336,32 "fac {(sc. Christus)} per eos {(archangelos)} sit
        ((* {spectat ad} $VULG. IV reg.; 12,6 ((* {ex} $LXX IV reg.; 12,6 "τό βεδεκ"))))
        ((* {cf.} E. A. Sophocles, Greek Lexicon of the Roman and Byzantine Periods. I. 1887.; p. 304))"'''
        rn = self.compile(belege, 'Belege')
        assert not rn.errors
        rn = self.at(rn)
        assert rn.content == '''Ekkeb. Schon. opusc. 11I p. 336,32 fac (sc. Christus) per eos''' \
                             ''' (archangelos) sit (spectat ad Vulg. IV reg. 12,6 [ex LXX IV reg. 12,6 τό βεδεκ]; ''' \
                             '''cf. E. A. Sophocles, Greek Lexicon of the Roman and Byzantine Periods. I. 1887. p. 304).'''

    def test_Einschub_Klammern_BelegText2(self):
        beleg_text = '''"periacent aliqua {(ossa)} per articulos, aliqua per commissuram, 
            aliqua per #sartur.|am (#sars|.uram, #sarsura {var. l.}), aliqua per gonfosim; 
            dentes caput quoque habet, #sartur.|as (arsuras {var. l.}) V {eqs.}"'''
        rn = self.compile(beleg_text, 'BelegText')
        rn = self.at(rn)
        assert rn.content == '''periacent aliqua (ossa) per articulos, aliqua per commissuram, ''' \
                             '''aliqua per -am (sars-, sarsura var. l.), aliqua per gonfosim; dentes caput ''' \
                             '''quoque habet, -as (arsuras var. l.) V eqs.'''

    def test_Klammernanpassung(self):
        beleg_text = '''"una clero advocato populoque #sanctuari.|o
        presentibus decanus ... accipiat virgam regiminis ... et ... tradat in manum ipsius {(abbatis)}
        ((* {inde} DIPL. Heinr. V.; 264 ((ed. M. Thiel, Die Urkunden Heinrichs V. und der Königin
        Mathilde. * MGDipl. reg. et imp. Germ. VII {(ed. sub prelo)} )) ))."'''
        rn = self.compile(beleg_text, "BelegText")
        rn = self.at(rn)
        assert str(rn) == ("una clero advocato populoque -o presentibus decanus ... accipiat virgam "
                           "regiminis ... et ... tradat in manum ipsius (abbatis; inde Dipl. Heinr. V. "
                           "264 [ed. M. Thiel, Die Urkunden Heinrichs V. und der Königin Mathilde.  "
                           "MGDipl. reg. et imp. Germ. VII (ed. sub prelo)]).")


    def test_DP_Kursivierung(self):
        vlz = '''excaldo. {adde ad vol. III. p. 1491,46}: * CONSUET. Trev.; 49 p. 305,17 
        "in hiis ... diebus quatuor pane #sa|.lda.|to ({ni leg.} #sc|.alda.|to) cum maiore copa medone utantur {fratres}."'''
        rn = self.compile(vlz, 'ZielLemmaBlock')
        rn = self.at(rn)
        node = rn.pick(lambda nd: nd.content.find(':') >= 0)
        assert node and node.name == 'Kursiv'

    def test_DP_Kursivierung2(self):
        bedeutungsangabe = '''de rebus ad cultum, religionem pertinentibus; 
        de insignibus regni: {=> sanctus_18{sqq.}}; spectat ad superstitionem: {=> sanctus_35}:'''
        rn = self.compile(bedeutungsangabe, 'Bedeutungsangabe')
        rn = self.at(rn)
        doppelpunkt_ctx = rn.pick_path(lambda ctx: not ctx[-1].children and ctx[-1].content.find(':') >= 0)
        assert self.at.ist_kursiv(doppelpunkt_ctx)

    def test_DP_Grade(self):
        ba = '''iunctura "omnes #sanct.|i" in nomine sollemnitatis; "cuncti #sanct.|i": {=> sanctus_26}:'''
        rn = self.compile(ba, 'Bedeutungsangabe')
        rn = self.at(rn)
        nd = rn.pick(lambda nd: nd.content == ':')
        assert nd.name == "Grade"

    def test_KlammerKursivierung_Druck_Nothilfe(self):
        # Ticket 89: Notabhilfe für fehlende Auswertung der "kursiv"-Klasse beim Druck
        bedeutungsangabe = '''fort. add. {(ni subest {/sac(c)ulus})}:'''
        rn = self.compile(bedeutungsangabe, 'Bedeutungsangabe')
        rn = self.at(rn)
        for nd in rn.select_if(lambda nd: nd.get_attr('class', '').find('grade') >= 0):
            assert nd.name == 'Grade' or nd.name != "TEXT", nd.as_sxpr()

    def test_Trennpunkt_zwischen_Belegen(self):
        st = parse_xml("""<Beleg>
              <Verweise>
                <a href="#sacerdos_4" alias="⇒" globaleReferenz="sacerdos.sacerdos-4"></a>
                <L> </L>
                <a href="#sacerdos_5" alias="⇒" globaleReferenz="sacerdos.sacerdos-5"></a>
              </Verweise>
            </Beleg>""")
        self.at.path = [st]
        self.at.abschließender_Punkt(st)
        assert st.content.endswith('.')

    def test_kein_Punkt_nach_Internetverweisen(self):
        verweise = '''"(sacrilgio{=>München, Bayerische Staatsbibliothek, Clm 903, f. 138^v 
            | https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00095232/canvas/290/view} 
            {codd.})"'''
        rn = self.compile(verweise, 'BelegText')
        rn.errors = []
        rn = self.at(rn)
        assert str(rn).find("138v.") < 0
        verweise = '''"(sacrilgio  {=>sacerlegio_5}{=>München, Bayerische Staatsbibliothek, Clm 903, f. 138^v 
            | https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00095232/canvas/290/view} 
            {codd.})"'''
        rn = self.compile(verweise, 'BelegText')
        rn.errors = []
        rn = self.at(rn)
        assert str(rn).find("138v.") >= 0

    def test_Einschub_Kursivierung(self):
        st = parse_xml("""<Einschub>
                    <Zusatz>spectat ad</Zusatz>
                    <L> </L>
                    <Beleg>
                      <Quellenangabe autor="Vulg." werk="Ioh.">
                        <Quelle>
                          <Autorangabe>Vulg.</Autorangabe>
                          <L> </L>
                          <Werk>Ioh.</Werk>
                        </Quelle>
                        <L> </L>
                        <BelegStelle>
                          <Stellenangabe>
                            <Stelle>8,44</Stelle>
                            <a href="https://www.bibelwissenschaft.de/online-bibeln/biblia-sacra-vulgata/lesen-im-bibeltext/bibel/text/lesen/stelle/53/80001/89999/ch/915496889b6f8bf65a5b1e871a1d12e8/" alias=" ⇒www.bibelwissenschaft.de"></a>
                          </Stellenangabe>
                        </BelegStelle>
                      </Quellenangabe>
                    </Beleg>
                  </Einschub>""")
        rn = self.at(RootNode(st))
        first = rn.pick_path(lambda ctx: not ctx[-1].children, reverse=False)
        assert self.at.ist_kursiv(first)
        last = rn.pick_path(lambda ctx: not ctx[-1].children, reverse=True)
        assert self.at.ist_kursiv(last)

    def test_Grammatik_littera_kursivierung(self):
        st = parse_sxpr('(Grammatik (wortart (TEXT "littera") (L " ")'
                        '(wa_ergänzung "duodevigesima alphabeti Latini")))')
        rn = self.at(RootNode(st))
        assert rn.pick('wortart').attr['class'].find('kursiv') >= 0

    def test_VerweisPosition1(self):
        st = parse_sxpr('(VerweisPosition (verwiesen_auf (verweis_stern "٭")'
                        '(LemmaWort "zuccarum")) (verwiesen_auf (verweis_stern "٭")'
                        '(LemmaWort "zacharias")))')
        rn = self.at(RootNode(st))
        assert str(rn) == "cf. ٭zuccarum, ٭zacharias.", rn.as_sxpr()

    def test_VerweisPosition2(self):
        vp = """
            VERWEISE    gemma, *salgemma."""
        rn = self.compile(vp, 'VerweisPosition')
        rn = self.at(rn)
        txt = str(rn)
        assert txt == "cf. gemma, ✲salgemma."

    def test_disambiguierte_autoren(self):
        # nach disambiguierten Autoren darf kein Komma gesetzt werden
        rn = self.compile('(({spectat ad} * $VULG.; psalm. 106,34))', 'Einschub')
        rn = self.at(rn)
        assert str(rn) == "(spectat ad Vulg. psalm. 106,34.)"  # kein Komma nach "Vulg."

    def test_Spatien_bei_Sperrungen(self):
        # vor und nach Sperrungen sollte jeweils nur ein Spatium stehen und dieses
        # von doppelter Größe sein
        rn = self.compile('"alpha {!beta} gamma"', 'BelegText')
        rn = self.at(rn)
        assert flatten_sxpr(rn.as_sxpr()) == '(BelegText (TEXT "alpha") ' + \
               '(L `(spatium "2") "  ") (Sperrung "beta") (L `(spatium "2") "  ") (TEXT "gamma"))'

    def test_Interpretamente(self):
        # keine überschüssigen Spatien vor und nach dem Trennstrich bei Interpretamenten
        rn = self.compile('capital, rica -- Kopftuch:', 'Bedeutungsangabe')
        rn = self.at(rn)
        assert flatten_sxpr(rn.as_sxpr()) == '(Bedeutungsangabe (Interpretamente ' + \
               '(LateinischeBedeutung (LateinischerAusdruck "capital") (TEXT ", ") ' + \
               '(LateinischerAusdruck "rica")) (TEXT " — ") (DeutscheBedeutung ' + \
               '(DeutscherAusdruck "Kopftuch"))) (TEXT ":"))'

    def test_Nebenbedeutungssonderfall(self):
        rn = self.compile('intrans. vel absol. i. q. sub-, insilire (in) -- hinein-, '
                          'daraufspringen in univ.; usu trans. i. q. transsilire -- '
                          'hindurchspringen (durch, über): {=> salioA3; salioA13}:',
                          'Bedeutungsangabe')
        rn = self.at(rn)
        rn.errors = []  # remove missing anchor complaint
        assert str(rn)[-3] != "."

        rn = self.compile("""
        GRAMMATIK
            subst. II; -i n.

            -a, -ae f.: {=>salictum_1}""", 'GrammatikPosition')
        rn = self.at(rn)
        rn.errors = []  # remove missing anchor complaint
        assert str(rn)[-2] != '.'

        rn = self.compile("liber sacramentorum -- 'Sakramentar'; "
                          "usu appos.: {=> sacramentorium_1}; "
                          "de re v. {=> sacramentarius/sacramentorium_2a; "
                          "sacramentarius/sacramentorium_2b}:",
                          'Bedeutungsangabe')
        rn = self.at(rn)
        rn.errors = []
        assert str(rn).replace('😯', '').find('.;') < 0

    def test_Grammatikvarianten(self):
        lemma_block = """LEMMA salictum

            (sale-, -cet-, -lcet-, -let-, -ll-, -lle-, -itt- {sim.})

        GRAMMATIK
            subst. II; -i n.

            -a, -ae f.: {=>salictum_1}"""
        rn = self.compile(lemma_block, 'LemmaPosition')
        rn = self.at(rn)
        rn.errors = []  # remove missing anchor complaint
        assert str(rn)[-1] != '.'

    def test_KlammerAufloesung(self):
        belegtext = '''"{(v. comm.)}((D. Goltz, op. cit.; p. 271))(({et} J. Ruska, QuellStudGeschNat. 6. 1937.; p. 48sq.))"'''
        rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        assert WARNUNG_OP_CIT_NICHT_ZUZUORDNEN in {e.code for e in rn.errors}
        assert rn.content == "(v. comm.; D. Goltz, op. cit. p. 271 et J. Ruska, QuellStudGeschNat. 6. 1937. p. 48sq.)"

    def test_op_cit_werk(self):
        belegtext = '''"((* {cf.} D. Goltz {@ sal_23}, 
            Stud. z. Gesch. d. Mineralnamen. 1972.; p.| 190.; 304)) ...
            ((D. Goltz, op. cit. ({=>sal_23}); p. 271))"'''
        rn = self.compile(belegtext, 'BelegText')
        werk = rn.pick('Werk', reverse=True)
        assert werk.content.strip() == 'op. cit.' and werk.attr['werk'] == "Stud. z. Gesch. d. Mineralnamen."

        belegtext = '''"((* {cf.} Dt. Rechtswb. {@ sal_34} XI.; p. 1451sqq.))
                        ...
                        ((* {cf.} op. cit. {(=> sal_34)}; p. 1451sqq.))"'''
        rn = self.compile(belegtext, 'BelegText')
        werk = rn.pick('Werk', reverse=True)
        assert werk.has_attr('nachschlagewerk')
        assert werk.get_attr('werk', '') == 'Dt. Rechtswb. XI.'

        belegtext = '''"((* {cf.} Dt. Rechtswb. {@ sal_34} XI.; p. 1451sqq.))
                        ...
                        ((* {cf.} op. cit. {(=> sal_34)}; p. 1451sqq.))
                        ((* {cf.} op. cit.; p. 154)) /* Verweis auf eine andere Seitenzahl des zitierten Werks */
                        ((* {cf.} op. cit.)) /* Verweis auf die schon zitierte Stelle des zitierten Werks */
                        "'''
        rn = self.compile(belegtext, 'BelegText')
        rekonstruierte_stelle = rn.pick('Stelle', reverse=True)
        assert rekonstruierte_stelle and rekonstruierte_stelle.content == '' \
               and rekonstruierte_stelle.get_attr('stelle', '') == 'p. 154'

    def test_KlammerAuflösung_JunkturAusnahme(self):
        belege = """* PACTUS Alam.; 30,1 [p. 32] (s. VII./VIII.^in.) "si cuiuscumque caballus
                      <<super sepe aliena #sallierit {@salioA2}
                      (psalliret {@salioA5}, #sal(l)iaverit {@salioA4} {var. l.})>>
                      (sepem alienam #sal.|ierit {@salioA3} {var. l.})
                      {eqs}." """
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        assert str(rn).find("(sepem alienam") >= 0

    def test_KlammerAuflösung_Fragezeichen(self):
        beleg_text = '''"pondere ({an leg.} sopiti{?}) (({cf.} THANGM. Bernw. ; 41 p. 776,48 add. codd. 3. 4. 5))."'''
        rn = self.compile(beleg_text, "BelegText")
        rn = self.at(rn)
        assert rn[rn.index(lambda nd: nd.content.strip().endswith('?')) + 1].name == 'L'
        s = str(rn)
        assert s.find('?;') < 0
        assert s.find('?cf.') < 0

    def test_Punkt_nach_Verweisen_ggf_eliminieren(self):
        grammatik = """
        GRAMMATIK
            subst. I; -i n.
            -us, -i m.: {=> ca_ce_coe_1; ca_ce_coe_2}
            -a, -ae f.: {=> fem} {al.}"""
        rn = self.compile(grammatik, 'GrammatikPosition')
        rn = self.at(rn)
        assert rn.content.find('.;') < 0

    def test_Punk_nach_Sublemma_ohne_Grammatik(self):
        # Ticket #73
        saeve_gekuerzt = """LEMMA saevus (se-)
            GRAMMATIK
               adi. I-II; -a, -um
            BEDEUTUNG
               furens -- wütend:
                  * WALAHFR. carm.; 6,22 "post mella gravis cape pocula myrrae, 
                    quae sit #saev.|a licet, pluribus auxilio est."
            SUB_LEMMA  1. saeve
            GRAMMATIK
               adv.
            BEDEUTUNG
               furenter -- wild:
                  * CARM. Bur.; 92,51,2 "forme fuit {equus} habilis, etatis 
                    primeve et respexit paululum tumide, non #seve."
            SUB_LEMMA  2. saeviter
            BEDEUTUNG
               crudeliter -- grausam, auf grausame Weise:
                  * PASS. Quint.; 241 (MGPoet. IV p. 985) "nescius infelix {(sc. praeses)}, quod
                    #saevite.|r ipse peribit cum cunctis, quos ille trahit per crimina vinctos."
            AUTORIN
               Orth-Müller
        """
        rn = self.compile(saeve_gekuerzt, 'Artikel')
        rn = self.at(rn)
        assert str(rn).find('saeviter.') >= 0

    def test_Etymologie_Einfuegung_von_Semikola(self):
        etymologie_position = """
            ETYMOLOGIE
               salvia:
               * {cf.} Meyer-Lübke, REW^3; nr. 7558"""
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        assert str(rn).find('salvia;') >= 0, str(rn)

        etymologie_position = '''
            ETYMOLOGIE
                bulga,
                ital. bolgia, bolgetta:
                    * {v.} Wartburg, Frz. etym. Wb. I.; p. 605sq. 
                    * {cf.} DuC.; s. v. "bulga, bulgia"'''
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        assert str \
            (rn) == "(bulga, ital. bolgia, bolgetta; v. Wartburg, Frz. etym. Wb. I. p. 605sq.; cf. DuC. s. v. bulga, bulgia) "

        etymologie_position = '''
        ETYMOLOGIE
        francog. boiac {['Herbergsrecht']}: *{v.} DuC.; s. v. "boiac"'''
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        assert str(rn) == "(francog. boiac [‘Herbergsrecht’]; v. DuC. s. v. boiac) "

        etymologie_position = """
    ETYMOLOGIE  byz. χελάνδιον; {fort. per contam. c.} *sando {formatum}: * {de re v.} J. H. Pryor, Crusades. VII. 2008.; p. 92sq."""
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        assert str \
            (rn) == '(byz. χελάνδιον; fort. per contam. c. ✲sando formatum; de re v. J. H. Pryor, Crusades. VII. 2008. p. 92sq.) '

    def test_Etymologie_Position_Kursivierung_von_Semikola(self):
        etymologie_position = """
            ETYMOLOGIE
                gr. byz. σκαραμάγγιον {[ex orig. persica]}: * {cf.} E. Trapp, Lexikon zur byz. Gräzität. II/2. 2017.; p. 1561
                * Reallexikon d. Byzantinistik. A I.; p. 387"""
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        semikolon = rn.pick(lambda nd: nd.content.strip() == ";")
        assert semikolon.name == "Kursiv" or has_class(semikolon, 'kursiv')

    def test_Etymologie_Unterdrueckung_von_Semikola(self):
        # Ticket 78
        etymologie_position = """
        ETYMOLOGIE 

            theod. vet. sazzan, sezzan: [* {cf.} O. Schade, Altdt. Wörterb. 1882.; 
                                         II. p. 746 s. v. satjan]

            {et} francog. vet. saisir:  [* {cf.} Stotz, Handb.; 1,IV § 74.4. 2,VI § 109.5
                                         * {et} Wartburg, Frz. etym. Wb.; XVII. p. 19^{b}sqq.] """
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        s = str(rn).strip()
        assert s.endswith(']')
        rn = self.at(rn)
        s = str(rn).strip()
        assert len(re.findall(r';\s*\[', s)) == 0
        assert len(re.findall(r'\[\s*;', s)) == 0

    def test_Etymologie_Beibehaltung_von_Semikola(self):
        # Ticket 78
        etymologie_position = """
        ETYMOLOGIE 

            theod. vet. musteren: {v.} Althochdt. Wb. III.; p. 248^b;
            {ni subest} anglosax. mustard: {cf.} DMLBS vol. II.; p. 725^a """
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn.errors = []
        rn = self.at(rn)
        txt = str(rn)
        assert txt.find('248b;') >= 0, txt

    def test_Etymologie_Trenner_Sonderfall(self):
        ep = """
        ETYMOLOGIE 

            theod. vet. sazzan, sezzan: [* {cf.} O. Schade, Altdt. Wörterb. 1882.; II. p. 746 s. v. "satjan"];

            {et} francog. vet. saisir:  [* {cf.} Stotz, Handb.; 1,IV § 74.4. 2,VI § 109.5
                                         * {et} Wartburg, Frz. etym. Wb.; XVII. p. 19^{b}sqq.]"""
        rn = self.compile(ep, 'EtymologiePosition')
        rn.errors =  []
        rn = self.at(rn)
        for nd in rn.children:
            if nd.content.strip() == ";":
                break
        assert nd.name != 'Kursiv' or nd.get_attr('class', '').find('kursiv') < 0
        # Semikolon zwischen beiden Blöcken muss kursiv sein
        # assert nd.name == 'Kursiv' or nd.get_attr('class', '').find('kursiv') >= 0

    def test_kein_Punkt_nach_Verweisen(self):
        bedeutungsangabe = """vox ficticia primis litteris nominum septem mortifera peccata 
            designantium effecta (({cf.} DMLBS; s.v. {/2. saglia}{=> https://logeion.uchicago.edu/saligia} {et} A. Watson, Journal of the Warburg Institute 10. 1947.; p. 148sqq. {=> https://www.jstor.org/stable/750401?read-now=1&seq=1#page_scan_tab_contents})):"""
        rn = self.compile(bedeutungsangabe, 'Bedeutungsangabe')
        rn = self.at(rn)
        assert not re.match(r'\.\s*et', str(rn))

    def test_Verschiebung_der_GrammatikAngabe_bei_SubLemma(self):
        artikel = """
        LEMMA   sacrilegus
        GRAMMATIK
            adi. I; -a, -um
        BEDEUTUNG   adi. i. q. impius, profanus -- frevelhaft, gotteslästerlich, irreligiös, 
                    'sakrilegisch' {(?usu subst.: {=>sacrilegus_1})}:
            * CONC. Merov.; p. 68,5 "si quis ... {!ausu} #sacr.|elego {@sacrilegus_1} auctoretatem
              divinae legis ac iura naturae perruperet {eqs.} ((*LEG. Wisig.; 3,5,3 p. 163,2 
              "qui {[fideles]} devotionem sanctam a. conprobantur #sacrileg.|o temerasse." 
              *BERTH. chron. B; a. 1076 p. 243,13 "#sacrileg.|o motus sui a. domnum 
              apostolicum ... satis excommunicabant {[sc. synodici]}"))."
        SUB_LEMMA   sacrilege
            GRAMMATIK   adv.
            BEDEUTUNG   impie, profane -- frevelhaft, irreligiös:
                * RATHER. epist.; 16 p. 86,28 "qui {(Milo episcopus)} me ... 
                  #sacrileg.|e iniuriavit."
        AUTORIN Clementi       
        """
        rn = self.compile(artikel, 'Artikel')
        rn = self.at(rn)
        sublemma = rn.pick('SubLemmaPosition')
        wort_anzeige = sublemma.pick('WortartAnzeige')
        assert wort_anzeige.content.startswith('adv')
        grammatik = sublemma.pick('Grammatik')
        wortart = grammatik.pick('wortart')
        assert wortart.content == ''

    def test_Verstecken_der_Wortangabe_bei_SubLemmata(self):
        saluo = '''
        LEMMA salvo 
        GRAMMATIK
           verbum I; -avi, -atum, -are
        BEDEUTUNG (con)servare -- retten, bewahren:
            * CHRON. Fred.; 2,57 p. 80,15 "#salva.|tus est Theudericus consilium Tholomei."
        SUB_LEMMA  salvandus
        GRAMMATIK
           adi. I-II; -a, -um  
        BEDEUTUNG qui bene conservatus est -- gut erhalten:
           * MAPPAE CLAVIC.; 292 "alumen rotundum et #salvand.|um, quod salis gemma vocatur, et calcantum ex aceto acerrimo teruntur in hereo mortario."
        SUB_LEMMA 1. salvans
        GRAMMATIK
            subst. III; -antis m.     
        BEDEUTUNG salvator -- Retter (usu attrib.):
           * WALAHFR. Mamm.; 18,26 "sub Christo #salvant.|e valet."
        SUB_LEMMA
           2. salvans
        GRAMMATIK
           subst. III; -antis n.
        BEDEUTUNG quod salutem fert -- das Heil-, Wohlbringende:
           * ALBERT. M. eth. I; 388 p. 328,48 "specialis est {iustitia} 'circa honores' et 'salutem' ((* $ARIST.; (p. 1130^b,2) "σωτηρίαν")), id est #salvant.|ia, et 'pecunias'."
        AUTORIN
           Orth-Müller               
        '''
        rn = self.compile(saluo, 'Artikel')
        rn = self.at(rn)
        wa = rn.pick('wortart', reverse=True)
        assert wa.content == ''

    def test_Artikelkopf_einzelne_Unterangaben(self):
        form = """
            FORM
                 form.:
                     dat.:
                         sg.:
                             -e: {=> saecularis_3}
                     abl.:
                         sg.:
                             -e: {=> saecularis_4}
                         pl.:
                             -is: {=> saecularis_5}
                     superl.:
                         -issim(us): {=> saecularis_6}
                     decl. II: {=> saecularis_7; saecularis_8; saecularis_9} 
                               {al. }{(cf. Stotz, Handb. 4,VIII § 12.3)} """
        rn = self.compile(form, 'FormPosition')
        rn.errors = []
        decl_ii = rn.pick(lambda nd: nd.content.startswith('decl. II'), reverse=True)
        assert decl_ii.name == "Kategorie"
        rn = self.at(rn)
        txt = str(rn)
        assert txt.find('superl. -issim(us)') >= 0
        assert txt.find('dat. sg. -e') >= 0

    def test_LemmaNr_und_vel(self):
        lemma = "LEMMA  saltem VEL 2. saltim"
        rn = self.compile(lemma, 'LemmaPosition')
        rn = self.at(rn)
        txt = str(rn)
        assert txt.find('vel 2. saltim') >= 0

    def test_Artikelkopf(self):
        form = """
        FORM
            in tmesi: {}"""
        rn = self.compile(form, "FormPosition")
        rn.errors = []
        rn = self.at(rn)
        assert str(rn).strip() == "in tmesi."

    def test_BelegLemmaAnhaengsel(self):
        # enklitische Partikel
        rn = self.compile('"#sanct.|ae_que"', 'BelegText')
        lemma = rn.pick('BelegLemma')
        assert lemma.attr['anhaengsel'] == 'que'
        assert lemma.attr['verdichtung'] == '-ae'
        assert lemma.attr['lemma'] == 'sanctae'
        assert rn.content == "sanctaeque"
        rn = self.at(rn)
        assert rn.content == "-aeque"

        rn = self.compile('"supra #|.scrip.|ta"', 'BelegText')
        rn = self.at(rn)

    def test_Punkt_nach_Beleg(self):
        belege = '''* AMALAR. off.; 1,1,9 "requies et liberatio perfecta in #sabbat.|issimo 
                 (#sabbatism.|o {var. l.}) datur, quando animae sanctorum requiem aeternam 
                 percipiunt"; 1,33,4 "post ... sex dies sequitur #sabbat.|issimus 
                 (#sabbatism.|us {var. l.}), qui eos {(neophytos)} introducit ad patriam 
                 repromissionis, ubi est requies sempiterna."'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        assert str(rn).find('percipiunt.') >= 0

    def test_Elimination_redundanter_Stellenangaben(self):
        einschub = '''((* {spectat ad} $VULG.; IV reg. 12,6 ((*$LXX; IV reg. 12,6 "τὸ βεδεκ"))))'''
        rn = self.compile(einschub, "Einschub")
        assert str(rn) == "spectat ad Vulg. IV reg. 12,6 LXX IV reg. 12,6 τὸ βεδεκ"

    def test_kein_Punkt_im_verschachtelten_Beleg(self):
        bedeutungsangabe = '''aedifica sarcienda, partes aedificiorum vitiosae -- reparaturbedürftige Gebäude, Gebäudeschäden ((* {spectat ad} $VULG.; IV reg. 12,6 ((*$LXX; IV reg. 12,6 "τὸ βεδεκ")))); 
                              per pravam interpr. vocis hebr., ut vid. ((* {cf.} E. A. Sophocles, Greek Lexicon of the Roman and Byzantine Periods. I. 1887.; p. 304)):'''
        rn = self.compile(bedeutungsangabe, 'Bedeutungsangabe')
        self.at(rn)
        assert rn.content.find('τὸ βεδεκ]') >= 0

    def test_Sonderfall_Runde_Klammern_in_Einschub(self):
        einschub = '''((* {sim.} HILDEG. (?) caus.; 444))'''
        rn = self.compile(einschub, 'Einschub')
        rn = self.at(rn)
        assert str(rn).find('[?]') >= 0

    def test_kein_Punkt_nach_adde(self):
        variante = '''-ll-: {=> salarius_02} {=> salarius_03} {adde} * CHART. Francof.; 276.'''
        rn = self.compile(variante, 'Variante')
        rn = self.at(rn)
        assert str(rn).find('adde ') >= 0
        variante = '''-ll-: {=> salarius_02} {=> salarius_03} * {adde} CHART. Francof.; 276.'''
        rn = self.compile(variante, 'Variante')
        rn = self.at(rn)
        assert str(rn).find('adde ') >= 0

    def test_Sekundaerliteratur_Uebergaenge(self):
        belegtext = '''"(({de re v.} Dt. Rechtswb. XI.; p. 1440sqq. s. v. 'Salmann'))
        (({keine Überleitung} K. O. Scherner, Salmannschaft, Servusgeschäft u. Venditio Iusta. 1971))"'''
        rn = self.compile(belegtext, "BelegText")
        rn = self.at(rn)
        assert str(rn).find('; keine Überleitung') >= 0

        belegtext = '''"(({de re v.} Dt. Rechtswb. XI.; p. 1440sqq. s. v. 'Salmann'))
        (({et} K. O. Scherner, Salmannschaft, Servusgeschäft u. Venditio Iusta. 1971))"'''
        rn = self.compile(belegtext, "BelegText")
        rn = self.at(rn)
        s = str(rn)
        assert s.find("‘Salmann’.") < 0
        assert not s[:s.find('et')].rstrip().endswith(';')

        belegtext = '''"(({de re v.} Dt. Rechtswb. XI.; p. 1440sqq. s. v. 'Salmann' {=> ziel_1}))
        (({et} K. O. Scherner, Salmannschaft, Servusgeschäft u. Venditio Iusta. 1971))"'''
        rn = self.compile(belegtext, "BelegText")
        rn = self.at(rn)
        s = str(rn)
        assert s[:s.find('et')].rstrip().endswith('.')

    def test_Ueberleitung(self):
        etymologie = '''
            ETYMOLOGIE
                {de formatione v.}: * Stotz, Handb.; 2/VI § 81,8'''
        rn = self.compile(etymologie, "EtymologiePosition")
        assert rn.content == "de formatione v. Stotz Handb. 2/VI § 81,8"

    def test_Semikola_bei_Sekundaeliteraturfolgen(self):
        bedeutungsangabe = """'Voreid' ((HRG II.; p. 566sqq.)) ((HRG V.; p. 1039sqq.)):"""
        rn = self.compile(bedeutungsangabe, "Bedeutungsangabe");  rn = self.at(rn)
        assert str(rn) == "‘Voreid’ (HRG II. p. 566sqq.; HRG V. p. 1039sqq.):", str(rn)

        bedeutungsangabe = """'Voreid' ((HRG II.; p. 566sqq. * HRG V.; p. 1039sqq.)):"""
        rn = self.compile(bedeutungsangabe, "Bedeutungsangabe");  rn = self.at(rn)
        assert str(rn) == "‘Voreid’ (HRG II. p. 566sqq.; HRG V. p. 1039sqq.):", str(rn)

    def test_Autorzuschreibung(self):
        belege = '''* PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ...
        cum #scarifi.|catione ({B}, #scari.|fatione {A})
        ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως")).";
        71 p. 48,24 "ventosandum ... cum #scarifi.|catione ((/* [[$PAUL. AEG.]]; */3,20,1 p. 168,23 "ἀμυχῶν"))."'''
        rn = self.compile(belege, "Belege")
        assert len(rn.errors) == 1 and rn.errors[0].code == HINWEIS_DISAMIGUIRUNG_DER_AUTORANGABE

        belege = '''* PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ...
        cum #scarifi.|catione ({B}, #scari.|fatione {A})
        ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως")).";
        71 p. 48,24 "ventosandum ... cum #scarifi.|catione ((* [[$PAUL. AEG.]]; 3,20,1 p. 168,23 "ἀμυχῶν"))."'''
        rn = self.compile(belege, "Belege")
        assert not rn.errors
        assert rn.pick('Quelle', reverse=True).has_attr('versteckt')


    def test_Anker_elimination(self):
        belegtext = '''"#silinis {@ sagma_10b} assignarent"'''
        rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        assert str(rn) == "silinis assignarent"

        belegtext = '''"#silinis {@ sagma_10b}assignarent"'''
        rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        assert str(rn) == "silinis assignarent"

        # belegtext = '''"#silinis{@ sagma_10b} assignarent"'''
        # rn = self.compile(belegtext, 'BelegText'); rn = self.at(rn)
        # assert str(rn) == "silinis assignarent"

        # kein Spatium vor Satzzeichen:
        belegtext = '''"#silinis {@ sagma_10b}. assignarent"'''
        rn = self.compile(belegtext, 'BelegText')
        rn = self.at(rn)
        assert str(rn) == "silinis. assignarent"

        # aber Spatium vor Abkürzungszeichen:
        belegtext = '''"#silinis {@ sagma_10b} ... assignarent"'''
        rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        assert str(rn) == "silinis ... assignarent"

        belegtext = '''"#silinis {@ sagma_10b}... assignarent"'''
        rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        assert str(rn) == "silinis ... assignarent"

        # belegtext = '''"#silinis{@ sagma_10b} ... assignarent"'''
        # rn = self.compile(belegtext, 'BelegText');  rn = self.at(rn)
        # assert str(rn) == "silinis ... assignarent"

    def test_Semikolon_Kursivierung(self):
        belegtext = '''"#ialpas {@ salpinx_2} (ialpes {@ salpinx_3} {G}, κάλπυτον {B}) (({v. notam ed. p. 37}))."'''
        rn = self.compile(belegtext, "BelegText");  rn = self.at(rn)
        delimiter = rn.locate(str(rn).find(';'))
        assert delimiter.name == 'Kursiv' or delimiter.get_attr('class', '').find('kursiv') >= 0

    def test_Semikolon_gerade(self):
        # Ticket # 460
        belegtext = '''"((* PLATEAR. pract.; 48,37 "venter percussus resonat ad modum u|tris #semiplen.|i."; 48,50 "u|tris #semiplen.|i (semiplani {var. l.})")) ((* {cf. [locos corrupt.]} AESCULAPIUS; 37| p. 57,25 "si palma pulsantur, in aures #{simul plenissimos} {@ semiplenus_2} sonos reddent."; p. 58,1 "sicut utres #{semine pleni} {@ semiplenus_1} sonitus reddunt"))"'''
        rn = self.compile(belegtext, "BelegText")
        rn = self.at(rn)
        # print(rn.as_sxpr())
        # print(rn.content)
        # print(' ' * rn.content.find(';') + '|')
        mapping = ContentMapping(rn)
        path, off = mapping.get_path_and_offset(mapping.content.find(';'))
        assert not self.at.ist_kursiv(path)

    def test_Semikolon_Position_usu(self):
        gebrauch = '''
        GEBRAUCH
            usu medial.: {=> satio1_2; satio1_7; satio1_8} {al.}
            [usu] refl.: {=> satio1_3; satio1_4; satio1_5} {al.}'''
        rn = self.compile(gebrauch, 'GebrauchsPosition')
        rn = self.at(rn)
        assert rn.content.find('. al.; refl.: ') >= 0

    def test_Komma_nach_Lemma(self):
        lemma_position = """
        LEMMA  1. sancio (-ccio, -ctio)
        GRAMMATIK
            verbum IV; -i(v)i, -itum, -ire"""
        rn = self.compile(lemma_position, "LemmaPosition")
        rn = self.at(rn)
        assert str(rn) == "1. sancio (-ccio, -ctio), -i(v)i, -itum, -ire."

        lemma_position = """
        LEMMA  1. sancio (-ccio, -ctio)
        GRAMMATIK
            verbum IV; sanxi {vel} -i(v)i, -itum, -ire"""
        rn = self.compile(lemma_position, "LemmaPosition")
        rn = self.at(rn)
        assert str(rn) == "1. sancio (-ccio, -ctio), sanxi vel -i(v)i, -itum, -ire."

    def test_Punkt_nach_fetten_Klammern_nicht_fett(self):
        belege = '''* LEG. Burgund. Rom.; 43 "legali #sanction.|e constat expressum: {eqs.}
        ((
        * {sim.}; const. I; 80 "quae minus statuta sunt, praesentis legis #sanction.|e (#sanction.|em {var. l.}) constitui."
        * RAHEW. gest.; 4,4 p. 236,19 "tam divinarum quam humanarum legum #sancti.|o {@ sanctio_6}." {al.}
        * RAHEW. gest.; 4,4 p. 236,19 "tam divinarum quam humanarum legum #sancti.|o {@ sanctio_6}." {al.}
        * RAHEW. gest.; 4,4 p. 236,19 "tam divinarum quam humanarum legum #sancti.|o {@ sanctio_6}." {al.}
        * RAHEW. gest.; 4,4 p. 236,19 "tam divinarum quam humanarum legum #sancti.|o {@ sanctio_6}." {al.}
        ))"
        * EPIST. var. I; 15 p. 517,13 "non ... iuris fas est nec potest quelibet ... rationalis formule #sancti.|o perspicue rata putari, nisi {eqs.}"'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        punkt = rn.pick(lambda nd: nd.content == '.', reverse=True)
        assert punkt and punkt.name == "TEXT"

    def test_spatium_vor_adde_in_Verweislemmazeile(self):
        vlz = 'excaldo. {adde ad vol. III. p. 1491,46}: * CONSUET. Trev.; 49 p. 305,17 "in hiis ..."'
        rn = self.compile(vlz, "ZielLemmaBlock")
        rn = self.at(rn)
        big_spatium = rn.pick(lambda nd: nd.get_attr('spatium', '') == '3+1')
        assert big_spatium is not None
        assert str(big_spatium) == '    '

    def test_nicht_gesichert_nicht_fett(self):
        lm = 'LEMMA ?saepto (se-)'
        rn = self.compile(lm, 'LemmaPosition')
        ng_ctx = rn.pick_path('nicht_gesichert')
        assert pick_from_path(ng_ctx, 'Lemma')
        rn = self.at(rn)
        ng_ctx = rn.pick_path('nicht_gesichert')
        assert not pick_from_path(ng_ctx, 'Lemma')

    def test_fettes_Komma_bei_Verweisartikeln(self):
        lemmata = '''saepefactus, saepefatus'''
        rn = self.compile(lemmata, 'AusgangsLemmata')
        rn = self.at(rn)
        komma = rn.pick(lambda nd: nd.content.strip() == ',')
        assert komma.name == 'Fett'

    def test_eliminate_identical_lemma_after_vel(self):
        lemma_position = """
           LEMMA  1. scabio
           GRAMMATIK  verbum I; -are
           VEL  scabio
           GRAMMATIK  verbum IV; -ire"""
        rn = self.compile(lemma_position, "LemmaPosition")
        rn = self.at(rn)
        assert str(rn) == '1. scabio, -are  vel -ire.'

    def test_Klammeranpassung_Etymologieposition(self):
        etymologie_position = '''
        ETYMOLOGIE *scara: * {de re v.} Stotz, Handb.; 2,VI § 109.5
                * A. de Sousa Costa, op. cit. {(CHECK)}; p. 274sq.
        '''
        rn = self.compile(etymologie_position, 'EtymologiePosition')
        rn = self.at(rn)
        assert WARNUNG_OP_CIT_NICHT_ZUZUORDNEN in {e.code for e in rn.errors}
        assert rn.content.find('[CHECK]') >= 0

    def test_Stellenangabe_grade(self):
        bedeutung = '''
        BEDEUTUNG alleg. ((* {loci spectant ad} $VULG. exod.; 28,17sqq.; apoc.; 4,3.; 21,20)):
        * HRABAN. univ.; 17,7| p. 467^D "#sardi.|us ... martyrum gloria significat ((* {sim.} p. 470^C "in #sard.|is reverendus martyrum cruor exprimitur." {al.}))."'''
        rn = self.compile(bedeutung, 'Bedeutung')
        stelle = rn.pick('Stelle', reverse=True)
        assert stelle.get_attr('autor', '') == "Hraban." and stelle.get_attr('werk', '') == 'univ.'
        rn = self.at(rn)
        stellenangabe = rn.pick('Stellenangabe', reverse=True)
        assert stellenangabe and stellenangabe.get_attr('class', '').find('kursiv') < 0

    def test_VerweisArtikel(self):
        va = '''
        LEMMA [2. sacramentum VIDE atramentum  ADDE ad vol. $I. p. 1132,30: * GEBER. clar.; 2,18 "pone simul de aqua de #sacramentis (#atrament.|is {ci. ed., v. notam})."]
        AUTORIN Niederer'''
        rn = self.compile(va, 'VerweisArtikel')
        rn = self.at(rn)
        assert rn.pick('unberechtigt')
        assert str \
            (rn) == '''[2. sacramentum v. atramentum.    adde ad vol. I. p. 1132,30: Geber. clar. 2,18 pone simul de aqua de sacramentis (-is ci. ed., v. notam).]Niederer'''
        va = '''
        LEMMA 2. sacramentum VIDE atramentum  ADDE ad vol. $I. p. 1132,30: * GEBER. clar.; 2,18 "pone simul de aqua de #sacramentis (#atrament.|is {ci. ed., v. notam})."
        AUTORIN Niederer'''
        rn = self.compile(va, 'VerweisArtikel')
        rn = self.at(rn)
        assert not rn.pick('unberechtigt')
        assert str \
            (rn) == '''[2. sacramentum v. atramentum.    adde ad vol. I. p. 1132,30: Geber. clar. 2,18 pone simul de aqua de sacramentis (-is ci. ed., v. notam).]Niederer'''

    def test_falsche_Kursivierung(self):
        # Ticket 350 weiter unten
        belege = '''* ALBERT. M. metaph.; 7,1,9| p. 331,62sq. "multipliciter dicimus esse dictum 'non-scibile' {@ scibilis_3a} 
                      ((* $ARIST.; p. 1030^a,33 "τὸ μὴ ἐπιστητὸν")): per additionem #scibilitat.|is, sicut prima causa est 
                      non-scibilis{@ scibilis_3b}, quia vincit #scibilitat.|e ... intellectum {eqs.}"; 
                      p. 331,68 "aliquid ... dicimus non-scitum, quia aufertur ei #scibilita.|s, sicut tempus et 
                      motus et materia prima, quae non sciuntur, nisi {eqs.}."; 
                      summ. theol.; I 19,76 p. 796^a,19 "possibilitas et #scibilita.|s et ... voluntabilitas ... 
                      secundum rationem et non secundum esse sunt in ipsa {(sc. potentia)}."'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        summa = rn.pick_path(lambda path: path[-1].name == 'Werk' and path[-1].content[:5] == 'summ.', reverse=True)
        assert not self.at.ist_kursiv(summa)

    def test_tit_Sonderfall(self):
        belege = '''* ALBERT. M. lin.; ^{tit.} "incipit #scienti.|a libri... "'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        assert rn.as_xml() == '''<Belege>
  <Beleg>
    <Quellenangabe autor="Albert. M." werk="lin." xml:space="preserve"><Quelle><Autor>Albert. M.</Autor><L> </L><Werk>lin.</Werk></Quelle><BelegStelle><Stellenangabe><Stelle autor="Albert. M." werk="lin." stelle="^{tit.}"><HOCHGESTELLT>tit.</HOCHGESTELLT></Stelle></Stellenangabe><L> </L><BelegText><TEXT>incipit </TEXT><BelegLemma verdichtung="-a" id="scientia" anker="automatisch generiert" globaleId="UNBEKANNTER-ARTIKELNAME.scientia" lemma="scientia">-a</BelegLemma><TEXT> libri...</TEXT></BelegText></BelegStelle></Quellenangabe>
  </Beleg>
  <L> </L>
</Belege>'''

    def test_tit_Sonderfall2(self):
        belege = '''* ALBERT. M. lin.; ^{tit.} "incipit #scienti.|a libri de lineis indivisibilibus, 
        quae facit ad #scienti.|am libri sexti Physicorum."; veget.; 4,1 "plantarum vires naturales ... 
        in hoc quarto libro huius #scienti.|ae dicere suscipimus."; animal.; 1,3 "#scienti.|am ... 
        de membris animalium dividemus secundum duplicem considerationem ipsorum {eqs.}"; 1,46 "diximus 
        in #scienti.|a de animae operibus ..., quod {eqs.}" {ibid. al.} /*lin. nach neuer Ed. zitiert! 
        in phys. zwischen 6. und 7. Buch! Helena hat das in die Zitierliste eingearbeitet*/'''
        rn = self.compile(belege, 'Belege')
        rn = self.at(rn)
        quellenangabe = rn.pick('Quellenangabe')
        belegstellen = quellenangabe.indices('BelegStelle')
        # Nur vor der ersten Belegstelle sollte das Leerzeichen beseitigt werden.
        assert quellenangabe[belegstellen[0] - 1].name != "L"
        for i in belegstellen[1:]:
            assert quellenangabe[i - 1].name == "L"

    def test_476_ueberschuessiger_Punkt(self):
        # Ticket 476
        position = """
        script.:
            sestaar-: * CATAL. mensur.; 22 ({cod.})"""
        rn = self.compile(position, 'Position')
        rn = self.at(rn)
        rn.errors = []
        assert str(rn).endswith('(cod.)')


if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
