//scaria = Maierhof: die Form scaria kommt ur ein einziges mal ausßerhalb von Südtirol vor. Sonst nur in Südtiroler Quellen, und immer die Bedeutung Maierhof, Sitz der Verwaltungssitz! ev. besser ein separates Lemma ansetzen? geht das überhaupt auf scara = Anteil an Genendeland zurück? oder auf scara = Schar?? Oder gar eine ander Herkunft? Falls es sich als semantische Splatug herausstellen sollte, dann wäre ein separater Ansatz besser. Sek.Lit. spärlich, aber ev.: Verhandlungen des deutschen Geographentags. 40. 1976 p. 369sq. und Archiv f. österr. GEschichte. 93/94. 1904. p. 370sq.
//NB: Dt. Rechtswb. hat eine eigenen Eintrag scaria = Meierhof, mit einem Beleg! Nicht bei Schar untergeordnet.

//Addenda-Verweis nötig: *esscaria v. *scaria/*scara: * CHART. Tirol. I; 1035 p. 80,29. Klären wie der Ansatz wird, dann Mechthild den Verweis geben. Im 1. Band steht *ascaria v. *scaria. Auf den Zettel war es ursprünglich als scaria lemmatisiert M-L hat es zu scara verschoben.


//Verhältis von scara <> scala klären! v.a. ital. Quellen haben scala als var. l., oder auch nur scala (z.B. ANNAL. Ianuens. I p. 108, wo in der alten MGH-Edition von Pertz durchgehend die Konjektur scara steht. dialektale r>l Verschiebung oder anderes Lemma?
//Achtung: afrz. Nebenform echele sim. neben esciere sim.: unter skara in FEW! XII. p. 95sq. Keine einzige Bedeutung Schar, Kohorte, Trupe oder so unter unter scala! scala = Truppe hat also wohl nciht mit scala = Leiter zu tun. Tobler-LOmmatzsch III p. 892: aemma-Amsatz eschiele, Nebenform eschiere = Schar, Heeresabteilung, Stalffel; Herkunft frk. skara; vreeist auf REW (dort ist es aber als Form von scala aufgefasst); Gamillscheg p. 344^a: èchelle von lat. scala, Bedeutung 1. Leiter, 2. Schar, in der zweiten bedeutung kontaminiert mit frk. skara
//Italienisch: In Battaglia unter scala keine Bedeutung Truppe gefunden, aber leider auch keine scala-Formen unter schiera. Ital. Dialektwörterbüche: nichts gefunden. Battisti-Alessio V ist abgängig.
//Fazit: m.E. steht hinter lat. scala bei ANNAL. Ianuens eine provenzalische Form von scara mit l (Belege bei Wartburg), die von fränk. skara stammt und dem frz. eschiere entspricht. Dagegen entspricht lat. scher(i)a der italienischen Form schièra, die aus frz./prov. eschiere stammt. Ob scala nur eine hyerkorrekte Latinisierung ist, oder ob tatsähnlich dialektal eine l-Form bestand, ist unklar.


LEMMA 1. *scara VEL *scaria


GRAMMATIK

    subst. I; ae f.

ETYMOLOGIE
    theod. vet. scara;
    {cf ital. schièra:    {de re v.} Stotz, Handb.; 1,III §28.9


//ahd. Wb. II p. 780^a; Lexer Wörterbuchnetz
//Grimm: zu scheren, Grundbedeutung "Abteilung, Abtrennung"
//ital. schièra < prov. esquiera, esqueira < afz. eschiere < fränkisch skara, cf. Battaglia, Grande Dizionario. XVII.; p. 1005sq.
//Battisti-Alessio; V p. 3388? Buch wo???
//Stotz, Handb.; 1,III § 28.3 Heeresteil und reihum von verschiedenen zu leistende Fronarbeit; Dienst in einer Heeresabteilung, Transport- oder Botendienst (Spandienst, Fuhrfronde), NUtzungsanteil an gemeinschaftlichem Gut (> engl. share); scaramannus und scararius = Unfreie, die bewaffnet Botendienste oder reisebegelitungen zu leisten hatten; verbale Ableitung scarire, cf. 2,VI § 109.5; zu den Formen mit prothetischem e- cf. 3,VII § 84.3 Adn. 91 

//Jean-Pierre Devroey, Les services de transports à l'abbaye de Prüm au IX`me siècle, Revue du Nord. 61. 1979. p. 543-569: Artikel lesen und ev. zitieren!
//zur Bedeutung: Dt. Rechtswb.; XII. p. 226sqq. s. v. 'Schar^1'


SCHREIBWEISE

    script.:
        ascaria: {=> scaria_4}  * {adde} CHART. Tirol. I; 1184 p. 231,3 /*"in #as|.car.|ia."*/
        esscaria: {=> scaria_3}
        schara: * CHRON. Ratisb.; ((MGScript. XXX; p. 1489,37))
        sclera {(cf. ital. "schièra")}: * IOH. CODAGN. annal.; p. 28,7. {ibid. al.}
        schera: * ANNAL. Ianuens.; III p. 129,18. * ACTA imp. Winkelm. I; 687 (2x).; ANNAL. Plac.; a. 1237 p. 477,21; a. 1268 p. 528,20. 
        * {adde} ALBERT. MIL. temp.; 16 p. 490,13.; 20 p. 498,7.
        scheria: * ANNAL. Ianuens.; IV p. 92,28. * ANNAL. Plac. a. 1157
        -rra: {=> scara_2} * {adde} HINCM. annal.; a. 876 p. 242,22 {(var. l.)}
        -ria: * CHART. Rhen. med.; II app. 10 p. 342,26.  /*persaepe in Südtiroler Quellen!*/       

//vorläufige Skripturen und Stellenangaben aus Lemmaliste, plus Neuzugänge aus den Zetteln; noch überarbeiten



/* Sek.Lit. konsultieren:
 A. de Sousa Costa, Studien zu volkssprachigen Wörtern in karoling. Kapitularien. 1993. p. 269
 H. Tiefenbach, Studien zu Wörtern volkssprachiger Herkunft in karoling. Königsurkunden. Index
 */


BEDEUTUNG agmen (exercitus), turma, manus (militum) -- (Heeres-)Abteilung, Zug, (Soldaten-)Truppe, Haufen, 'Schar'; acies bene disposita - geordnete Schlachtreihe: {=> scara_1}:

    UNTER_BEDEUTUNG spectat ad homines:

    * CHRON. Fred.; 4,74 "#scar.|am (sacram {var. l.}) de electis viris fortis ... cum ducebus et grafionebus secum habens {(sc. rex)}."

    * ANNAL. regni Franc.; a. 766 "in Bituricas Francorum #scar.|am conlocavit {Pippinus}."

    * COD. Karol.; p. 612,26 "ut firmissima vestra #scar.|a (sacra {var. l.}) partibus Beneventanis emittere vestra ... regalis excellentia niteat." /*787/88*/

    * EPIST. var. I; 20 p. 528,16 "illa #scar.|a nostra, que prius de Italia iussimus {(sc. Karolus)} pergere partibus Avariae ..., perrexerunt {eqs.} ((* {inde} FORM. Dion.; 25))." /*a. 791*/

    * ANNAL. Guelf.; a. 793 "Karolus resedit Reganespuruc; inde transmisit #scar.|a sua, ubi ecesse fuit ((* {sim.} ANNAL. Lauresh.; a. 803 "#scar.|as suas transmisit in circuitu, bi {eqs.}"))."


    * CHRON. Moiss.; a. 804 ((MGScript. I; p. 307,29)) "misit imperator #scar.|as suas in Wimodia (({sim.} a. 806.; 809.; 810.; 815))." /* a. 815 p. 311,39 "misit {imperator} #scar.|as suas, ubi necesse fuit, per marchas."*/

    * REGINO chron.; a. 773 "misit {rex} occulte unam #scar.|am (scalam, turmam {var. l.}) de electis viris per montana." /*a. 908*/

    * CHRON. Ved.; p. 691,42 "ubi {(loco statuto)} Theodericus cum decem #scar.|is militum affuit." /*s. X*/


    * CHRON. Cosmae cont. II; a. 1126 "dux Sobezlaus dividens suos in tres #scar.|as subito impetu in eos {(sc. Saxones)} proruit."

    * MIRAC. Genulfi; 6 ((MGScript. XV; p. 1206,40)) "hic {(comes)} ex illa nobilium #scar.|a Francorum, quam ... rex in urbe ... reliquerat." /*s. XI. med.*/

    * ANNAL. Plac.; a. 1157 p. 457,39 "peditibus et #scher.|iis militum ordinatis (({sim.} a. 1237 p. 477,21 "quidam miles imperatoris venit ... prope #scher.|am Placentinorum."; a. 1268 p. 528,20 "inceperunt {sc. milites} prelium cum duabus #scher.|is militum Karuli"))."

    * CHRON. Ratisb.; ((MGScript. XXX p. 1489,37)) "eum {(Tassilonem)} ... quatuor militaribus #scharis coercens circumvallavit {Karolus}."  /*s. XII.2*/


    * ACTA imp. Winkelm. I; 687 p. 542,32sqq. (a. 1242) "Pergamenses ... pervenerunt usque ad locum suum de Tayuno ibidem suas acies et #scheras tam milicie quam populli constituentes; nos {(sc. Brixienses)} vero ... depositis nostris #scheris super terras inimicorum propinqui ad minus dimidium miliare tota die prudenter extetimus."

    * CHRON. Albr.; a. 1212 p. 894,47 "fuit ... bellum maximum et prima #scar.|a (scala {codd.}) nostrorum {(sc. Francorum)} fuit contrita {eqs.}." /*s. XII.2*/

    * IOH. CODAGN. annal.; a. 1200 p. 28,7 "nostri ... eos {Cremonenses} in fugam miserunt usqe ad magnam #scleram Cremonensium et ... amicorum eorum."; a. 1229 p. 96,7 "gente Boninie per campum ... dispersa ..., sic quod vix ... potuit congregari et in turba et in #sclera {@ scara_1} reduci" /*s. XIII.1*/

    {al.}

    /* 
    * CAPIT. reg. Franc.; 203.; 281,7: 5x Heersabteilung
    * HINCM. annal: 7x milit. Schar, Heeresabteilung
    * LEG. Lang.; p. 221,40 "vos {(sc. Siginulfum principem)} vestrumque populum liceat per terram meam {(sc. Radelgisi principis)} transire contra illos hostiliter et cum #scar.|a." a. 851
    * CHRON. Salern.; 118 p. 132,15
    * ERCHEMB. CAS. hist.; 35.; 72 "superveniens scara theatralis a tergo et in medio circumsepti devicti sunt." cf. p. 257,34 mit nota: viri theatrales = die beim theater wohnen
    * COSMAS chron.: 5x milit. Schar, Heeresabteilung
    * IOH. CODAGN. trist.; p. 25,24.90.30.; p. 43,26.35.; p. 44,12sqq. al.
    
    */





    UNTER_BEDEUTUNG spectat ad naves i. q. classis, classicula - Flotte, Geschwader:

    * ANNAL. Ianuens.; III p. 129,18 "facta maxima et potens #schera galearum et taridarum in mari supra Devam {eqs.}."; IV p. 92,28 "invenit {admiratus} caravanam Veneticorum, in qua caravana erant galee XXXII armate et naves et ligna cum gabiis circa XXVIII, et barche circa XXVI, quibus visis a dicto admirato, se cum suis galeis in #scheria recolegit." /*ACHTUNG: von DuC wird ANNAl. Ianuens. IV p. 92,28 als identisch mit schere (Schäre) interpretiert, im Sinn eines sicheren Rückzugsortes, natürlichen Hafens! Aber schere selbst versteht er als gefährliche Stelle! Das hat wohl  nichts miteinander zu tun (schere ist ein Lübecker Beleg: mhd. schere = Felsen, Klippe, Schäre); in scheria recolegit = sammelte sich mit seinen galeis zu einer (geschlossenen) Flotte? Oder vereinigte sich samt seinen galeis mit seiner Flotte? MGH_Edition Script. XVIII p. 285,5 hat als nota = acies*/







BEDEUTUNG de servitio dominicali per ordinem praestando:


    UNTER_BEDEUTUNG ?servitium militare (in turma praestandum) -- (in einer Schar zu leistender) Heeres-, Soldatendienst: /*so nach Niermeyer*/

    * CAPIT. reg. Franc.; 74,2 "ut ... nec wacta nec de #scar.|a nec de warda ... heribannum comis exactare presumat." /*a. 811; im Index unklar ob zu Schar, oder ob "servitium itinera faciendi"; könnte aber auch "im Heer zu leistender Schardienst, d.h. Transport- oder Botendienst" sein. Der Ursprung des Wortes scheint ja schon im militärischen Kontext zu liegen - die frondienstlichen Bedeutungen kommen später. Die einzelnen militärischen Tätigkeiten sind hier recht genau definiert: wacta, scara, warda, heribergum; Dt. Rechtswb. hat diesen Beleg für Schar(dienst), Truppe(ndienst) reklamiert*/


    UNTER_BEDEUTUNG servitium nuntii, comitandi, vehendi -- Boten-, Kurier-, Begleit-, Transportdienst, 'Schardienst, -werk':

    * CAPIT. reg. Franc.; 128,8 "unusquisque {(mansus)} ... #scar.|am facit; ... #scar.|am facit ad vinum ducendum; ... #scar.|am facit." /*a. 810; nota ed: servitium, schaarwerk*/

    * DIPL. Loth. I.; 45 p. 136,17 "de itinere exercitale seu #scar.|as {(v. notam ed.)}." /*a. 840*/

    * FORM. imp.; 38 "ut ... neque #scar.|am facere neque heribannum ... ab eis {(negotiatoribus)} requirere aut exactare praesumatis {(sc. episcopis et alii)}." /*s. IX.1*/

    * CHART. Rhen. med.; I 120 p. 126,23 (a. 882) "solvit unusquisque {(sc. mancipiorum)} ... #scar.|as III per noctes XIIII de lignis carra V."

    * REGISTR. Prum.; 2 "que {(mansa)} ... #sar.|am faciunt; Ello habet mansum I, qui similiter #scar.|am facit sicut ceteri scararii {@ scararius_1a}."; 5 "que {(mansa)} similiter servire debuissent ut superiores et modo #scar.|am faciunt ((* {cf.} CAES. PRUM. gloss.; p. 171^1 "scararios {@ scararius_1b} ... ministeriales appellamus; #scar.|am facere est domino abbati, quando (???) ipse iuserit {(sic)}, servire, et uncium eius seu litteras ad locum sibi deterinatum deferre"))."; 6; 15; 55 "facit {(sc. mansum)} #scar.|am sive cum eco seu cum pedibus (({sim.} 67 "#scar.|am pedestriam."; 69 "#scar.|am cum nave."; 71 "#scar.|am facit cum nave")).";  /* (a. 893); CHART. Rhen. med. 135 = REGISTR. Prum. neu Edition Schwab*/

    * REGISTR. Weiss.; 6 "debent {servi} ... suam #scar.|am, quando opus est, secundum ordinem facere." /*neue Ed.: s. IX.med.; ibid. persaepe, 41 Hits in Wordsuche, immer dieselben Formulierungen*/

    * COD. Eberh.; I p. 314,9 "colonus ... #scharam facit." /*s. XII.med.; = TRAD. Fuld. I 10*/

    * REGISTR. Mediol.; 6 ((ZGeschSaarland 15. 9165. p. 120)) "solvit {Liedolt} enarios VIII aut #scar.|iam inter tres mansos ad monastertium." /*s. IX.ed/XII.ed; hir form scaria außerhalb von Norditalien! und Bedutung Frondienst*/




BEDEUTUNG xxx - Zahlung anstelle eines Boten-, Transportdienstes: /*oder exigere allgemiener, und scara doch ein Dienst? auffällig, dass frühere Beleg als die anderen Diplomata, wo es der Dienst ist.*/

    * DIPL. Karoli M.; 108 p. 153,32 "ad ...freda ... exigere ... nec #scar.|as vel coniectos ... 
        ((* {inde} DIPL. Ludow. Pii; 53 p. 138,15 "ad ... #scarr.|as {@ scara_2} vel coniectos tam de carris quam etiam de parafredis exactandis (({inde} 251| p. 625,7. p. 625,27 "ad ... #scar.|as vel mansionaticos vel coniectos ... exigendos"))." * DIPL. Loth. I.; 56 p. 160,13. * DIPL. Loth. II.; 3 p. 386,33. * DIPL. Arnulfi; 28 p. 43,13.35. * DIPL. LUdow. Germ.; 134 p. 127,1.21.))."

    * CHART. Rhen. med.; I 95 p. 99,30 (a. 860) "ut nullus iudex ... audeat ... #scar.|as vel mansionaticos vel coniectos ... exquirere."




BEDEUTUNG pars agri communis -- Anteil an Gemeindeland:

* CHART. Rhen. inf.; I 7 (a. 796) "qualiter ego Theganbaldus ... tradidi partem hęreditatis meę Liudgero abbati ..., id est ... hovam integram ... cum pascuis et perviis et aquarum decursibus et #scar.|a in silva." /*könnte hierher gehören; cf. Dt. Rechtswb.; Sek.Lit genauer ansehen! lat. oder deutscher Beleg? ahd. ist auch scara! nach dem Prinzip wenn es ahd. sein könnte, dann wollen wir es so auffassen, fällt dieser Beleg weg!*/

* REGISTR. abb. Werd.; 2,2 p. 12,7sq. "hec sunt nomina locorum ... hereditatis: ... in silva, que dicitur Puthem, #scar.|as XXVIII {eqs.}"; 2,6 p. 27,16 "tradidit ... unum dominicalem mansum ... et quindecim #scar.|as" /* CHART. Rhen. inf.; I 65 p. 31,8 (a. 855)= REGISTR. abb. Werd. 2,2 p. 12,7sq.; Beleg in Dt. Rechstwb. für diese Bedeutung zitiert; ibid. persaepe. (23x)*/



BEDEUTUNG curtis vilicalis q. d., ambitus curtis vilicalis q. d., sedes vilicationis -- Maierhof(bezirk), Sitz der Gutsverwaltung: /*laut Tiroler UBB der Hauptsitz der bischfölichen Verwaltung; nicht dasselbe wie der Anteil an Gemeindeland im 8./9. Jh.!*/

* CHART. Tirol. I; 435 p. 232,38 (a. 1188) "pastores ... pro suo feudo habebant pecias duas panni ..., qus terras Armanus posuit in #scar.|iam; et terra #scarie, que fuit {eqs.} {(v. ind. ed. p. 387)}."; 484 p. 272,39 "comes ... investivit ... episcopum ... de scaria sua de Nano, quam dicebat esse suum alodium, et de alia sua curia {eqs.}"; 619 p. 93,17 "de nemore ... pertinente ad #scar.|iam de Domo." /*noch ca. 20x in diser Form jnd Bedeutung im Band II*/

* COD. Wang. Trident.; 1 p. 524,9 "comes ... iure ... pignoris investivit ... episcopum de #scar.|ia sua de Nano, quam dicebat esse suum allodium."; 191 p. 946,21 "de #scar.|ia illa {(sc. de Domo)} solvit ... vilicus ... XX modios blave maioris (({sim.} p. 950,7 "de curia #scar.|ie"))."; 52* p. 1217,7sqq. "carta #scar.|ie de pertinentiis {eqs.}" /*1 ist a. 1194; mind. 10 Belege Index: ambito amministrativo e sede dello scarius*/


* REGISTR. Trident.; I 10 /*a.1220*/ "frumentum et faba debet esse #scari.|e ... canonicorum."; 55 p. 58,26 "debet {Millus} omni anno ... trahere unum carrum de lignis ... ad #scar.|iam canonicorum."; 63 "quod {(vinum)} trahitur ... ad #schariam ... canonicorum"; 86 p. 78,3 "illi de Bevedeno tenentur solvere omni anno #scar.|ie duos amesseros, scilicet {eqs.}" /*scaria: persaepe! auch scarius oder scario = der Verwalter der scaria kommt oft vor!*/

* CHART. Tirol. I; 1035 p. 80,29 "in plebatu Piani ante #ess|.car.|iam {@ scaria_3} dominorum canonicorum Tridentinorum."; 1037 p. 83,14 "in curia #as|.car.|ie {@ scaria_4} canonicorum."

* CHART. Tirol. notar.; I| 71^a p. 33,37 /*a.1236*/ "in domo #scar.|ie ... abbatis (({sim.} 86^a.; 105.; 201 p. 98,1 "in curte #scar.|ie." {ibid. al.}))." /* Regest zu 71: scaria=Maierhof, scarius=Maier*/

{persaepe.}

AUTORIN Niederer