#!/usr/bin/python3

"""
test_MLWCompiler.py -- testet verschiedene Eigenschaften von
MLW-Compiler.py, die nicht schon durch die Grammatik-Tests in
'grammar_test' abgedeckt sind. Dazu gehören insbesondere die
weiteren Verabteitungsschritte nach der Erstellung des
Abstract-Syntax-Tree (AST), sowie bestimmte spezielle Algorithmen.
"""

import copy
import os
import sys

sys.path.extend(['../', '../DHParser'])

from DHParser.lsp import shortlist
from DHParser.stringview import TextBuffer

from MLWUpdateVSC import update_vscode_extension
from MLWServer import gather_symbols, anker_sortierung

class TestVSCode:
    def test_update_vscode(self):
        success = update_vscode_extension()
        assert success

TEST_ARTIKEL_sagax = """
LEMMA sagax
GRAMMATIK
     adi. III; -acis

ETYMOLOGIE
    gr. byz.

SCHREIBWEISE
     script. et form.:
         saca-: {=> sagax_7}
         abl. sg.:
             -ati: {=> sagax_2}
             -cae: {=> sagax_7}
             -ce: {=> sagax_19; sagax_5; sagax_15} *{adde} CHRON. Fred. cont.; 19. {al.}
         nom. pl.:
             -ases: {=> sagax_4}
         compar.: 
             -ati(or): {=> sagax_1; sagax_16} 
         decl. $II.: {=> sagax_6}

STRUKTUR
     struct.:
         c. gen.: {=> sagax_8; sagax_13}
         c. abl.: {=> sagax_9; sagax_10; sagax_11; sagax_12}
         c. praep.:
             ad: {=> sagax_14; sagax_17}
             in c. abl.: {=> sagax_18}
    
BEDEUTUNG facile sentiens, attentus, vigil -- leicht spürend, aufmerksam, wachsam:
     U_BEDEUTUNG proprie:
         UU_BEDEUTUNG in univ.:
             UUU_BEDEUTUNG de animalibus:
                 * RHYTHM.; 79,27,2 "conradunt {(sc. cupidi)}, quaerunt, inhiant velut #sagac.|es {(sc. canes)} avidi."
                 * TRANSL. Eug. Tolet.; 39 "ut nec homines nec canes venationis #sagac.|es {@ sagax_13} timuit {cervula}."
                 * FRID. II. IMP. art. ven.; 3 p. 59,3 "sit {(sc. canis)} #saga.|x ad intendendum {@ sagax_14} id, quod docetur"
                 * CARM. Bur.; 58,4,10 "anser #saga.|x, vultur edax."
                 * CONR. MUR. nov. grec.; 4 prol. p. 209,21 "cum canibus nare #sagac.|i."
             UUU_BEDEUTUNG de hominibus:
                 * EIGIL Sturm.; 7 "avidus locorum explorator ubique #sagac.|i obtutu montuosa atque plana perlustrans loca."
                 * CAND. FULD. Eigil. II; 2,30 "haud secus {(sc. ac apis)} hic iuvenis ... munere Christi proficiens aetate simul sensuque #sagac.|i pasci promeruit umbrati nectaris haustu."
                 * GERH. AUGUST. Udalr.; 1,1 l. 141 "#sagac.|i oculorum acie intus et forinsecus positiones aecclesiae caute perlustrans."

         UU_BEDEUTUNG sollers -- geschickt:
             * WALAHFR. carm.; 47,1,1 "pinxit imaginibus rerum studiosa coruscis hanc ill. pallam arte manuque #saga.|x {@ sagax_12}."
             * ARNOLD. SAXO flor.; 2,1 p. 53,37 "manus breves et exigue furtis opportune et #saga.|ses {@ sagax_4} sunt."

     U_BEDEUTUNG translate:
         UU_BEDEUTUNG de hominibus: 
             UUU_BEDEUTUNG mente acer, prudens -- scharfsinnig, klug:
                 UUUU_BEDEUTUNG in univ.:
                     UUUUU_BEDEUTUNG usu communi:
                         * ARBEO Corb.; prol. p. 188,9 "domino Virgilio ... #sagac.|issimo heres exiguus in Domino salutem {(sc. dat)}."
                         * WALAHFR. Wett.; 407 "episcopus ... mente piger nec corde #saga.|x."
                         * GODESC. SAXO theol.; 1 p. 83,2 "o mi lector, ... capax, #saga.|x, perspicax, sicut oportet, inspector."
                         * THANGM. Bernw.; 54 "totius rei publicae #sagac.|issimum provisorem tam nobilium dignitas quam plebium humilitas ... subtractum esse deplorat."
                         * HELM. chron.; 59 p. 114,22 "cepit ... #sagac.|issimus {(sc. comes)} intentare sollerter, si {eqs.}"
                         {saepius.} {v. et {=> sagax_3}}

                     ANHÄNGER usu subst:
                         * HUGEB. Willib.; 6 p. 105,13 "transacto ... prolixa iteneris meatu Willibaldi, quam ille #saga.|x in VII annorum indutia lustrando adiebat."; Wynneb.; 2 p. 107,19 "cum #saga.|x ille votiva voluntatum adimplere optabat desideria."
                         * RUOTG. Brun.; 2 p. 3,23 "quantum a #saga.|tioribus {@ sagax_1} discerni potuit."
                 
             
                     UUUUU_BEDEUTUNG c. abl. vel gen.:
                         * ARBEO Corb.; 18 p. 208,22 "vir natantissimus, ... alacer ingenio, locutione #saga.|x {@ sagax_9}."
                         * WALAHFR. Wett.; 57 "iustus in arbitrio, arte #saga.|x {@sagax_10} (gloss.: cleini)."
                         * ERMENR. Sval.; 10 p. 162, 25 "o nimiae prudentiae #sagac.|issime {@ sagax_8} (#sagac.|issimae {var l.}) frater."
                         * TRACT. de loc. terr. sanct.; ((SBMünch. 1865/$II. p. 147,5)) "Pisani, Ianuenses et Venetici ... mercimoniorum ingenio #sagac.|es {@ sagax_11}."

                     UUUUU_BEDEUTUNG c. praep.:
                         * ARBEO Emm.; 2 p. 28,21 "erat ... ad eradicanda vitia #saga.|x {@ sagax_17}."
                         * EIGIL Sturm.; 2 p. 133,4 "#saga.|x {@ sagax_18} in cogitatione."                     
                         {v. et {=> sagax_14}}

                     UUUUU_BEDEUTUNG usu praed. pro adv.:
                         * WALAHFR. imag. Tetr.; 240 "cura mihi fuerat tales cognoscere fasces, rimabarque #saga.|x tantarum munia rerum, dum {eqs.}"

                 UUUU_BEDEUTUNG iuncturae "mens, ingenium" {sim.} "#saga.|x":
                     * WALAHFR. Blaithm.; 61 "animo ... #sagac.|i."; 116 "mente #sagac.|i
                     ((
                      * CARM. var. III; 9,14 "m. #saga.|ti {@ sagax_2}"
                     ))."
                     * THANGM. Bernw.; 11 "#sagac.|is ingenii vir
                     ((
                     *{sim.} RUOTG. Brun; 4 p. 5,5 "ingenio #sagac.|i."
                     {al.}
                     ))."
                     * EPIST. Hild.; 134,2 p. 195,2 "hanc traditiunculam ... #sagac.|i intellectu capite {(sc. socii)}."
                     ANHÄNGER adde:
                         * PAUL. DIAC. carm.; 32,16 "illud vestra {(sc. regis)} #saga.|x nimium sapientia purget."
                         * RADBERT. corp. Dom.; prol. ad Karol. 67 "ut haec diligentius vestra perlegat #saga.|x intellegentia."
            
                  UUUU_BEDEUTUNG de sermone:
                      * AETHICUS; 81,10 "arte medicos gnaros, quorum ab initio illius generationis multorum eruditionem #sagac.|issimo argumento claruit {Thessalia}."
                      * ARBEO Emm.; 7 p. 37,16 "aliis vitia #sacacae {@ sagax_7} (#sagac.|i {var. l.}) sermone radicitus ex corpore abscedebat
                      ((
                          *{inde} ANTIPH. de Emm.; ((MGMer. $IV; p. 525,21)) "#sagac.|is {@ sagax_6} sermonibus"
                      ))."
                      ; 29 p. 69,16 " unusquisque iustorum invisibili gladio praecinctus est <<#sagac.|issimo sermone>> (#sagac.|issimi sermonis {var. l.}) Dei."; 38 p. 86,30.

             UUU_BEDEUTUNG studiosus, intentus -- beflissen, eifrig:
                 * AETHICUS; 1 "philosophorum scidolas #sagac.|e {@ sagax_19} {!indagatione} investigans
                 ((
                     * {item}; 104))
                     (( *{sim.}; 93 "indagationem #sagac.|issimam"
                     ))."
                 * WALAHFR. Wett.; 795 "ammonuit divinis laudibus omnes conatu servire bono studioque #sagac.|i."
                 * EINH. Karol.; 325 p. 30,17 "discebat artem computandi et intentione #sagac.|i siderum cursum curiosissime rimabatur."
                 * DIPL. Heinr. II.; 244 p. 281,20 "si ... aurem ... prebemus petitionibus nostrorum fidelium, ... eos ... circa nos #saga.|tiores {@ sagax_16} serviendo efficimus ac devotiores."

                 ANHÄNGER in malam partem i. q. avidus -- gierig:
                     * CHRON. Fred.; 4,60 "cum ... #sagac.|e {@ sagax_5} desiderio vellit {Dagobertus} omnibus undique expoliis novos implere thinsauros."

             UUU_BEDEUTUNG fortis, comprobatus -- stark, verlässlich:
                 * POETA SAXO ; 4,168 "quos {(Bohemos)} contra missus multis cum milibus eius {(sc. Karoli)} natus et aequivocus bellum virtute #sagac.|i  commisit"
                 * THIETM. chron.; 1,2 "quia antiquorum #sagac.|i (#saga.|ti {a. corr. A.})  memoria certum indagare nequeo nec per scripta invenio."

             UUU_BEDEUTUNG   diligens -- umsichtig:    
                 * CONSUET. Marb.; 341 "debet ... prelatus ... #sagac.|i sollicitudine unumquemque probare studio corrigendi."

             UUU_BEDEUTUNG astutus -- schlau, verschlagen (usu subst.):
                 * WALAHFR. Wett.; 218 "ille {(sc. diabolus)} feroxque rapaxque, minax mendaxque #saga.|x_que."

         UU_BEDEUTUNG de natura i. q. sapiens -- weise:
             * ALBERT. M. sens.; 1,13 p. 49,59 "#saga.|x natura humori crystallino in talibus {(sc. sanguinem habentibus)} animalibus circumposuit abundanter, ut {eqs.}"

         UU_BEDEUTUNG de angelis i. q. velox -- schnell:
             * AETHICUS; 13 "ubi voluerint {angeli} ..., #sagac.|e {@ sagax_15} praepropere volatum discurrere et fieri ab eis, quod iussum est, in momento."

         UU_BEDEUTUNG de rebus i. q. commodus -- angemessen:        
             * WOLFHER. Godeh. I; 12 p. 177,2 "si quid fratribus subtractum erat in vestitu vel stipendio, #sagac.|i totum adimplevit reinpendio."


SUB_LEMMA sagaciter

GRAMMATIK adv.
            
     BEDEUTUNG diligenter, prudenter -- sorgfältig, klug:

         U_BEDEUTUNG in univ.:
             * CHRON. Fred. cont.; 2 p. 169,20 "regem Theudericum in regno restituto ipse {(Ebroinus)} suum principatum #sagacite.|r restauravit."
             * AUREL. REOM. mus.; 19,45 "#sagax {@ sagax_3} cantor, #sagacite.|r intende, ut ... duobus in locis ... facias celerum ictum."
             * DIPL. Loth. II.; 34 p. 441,26 "providere #sagacite.|r oportet."
             * EPITAPH. var. II; 42,19 "callens {(sc. Hathawiga abbatissa)} ... #saga.|cius una, quid deceat, quid non, quo virtus, quo ferat error."
             * THIETM. chron.; 2,28 p. 74,15 "singula ... #sagacite.|r perquirens et de susceptione ducis ... perdidicit {imperator}."
             * GESTA Halb.; p. 103,7 "ut sub regula beati Benedicti viverent {sanctimoniales}, #sagacite.|r ordinavit {episcopus}."
             {al.}
    
         U_BEDEUTUNG attente -- aufmerksam:
             * WALAHFR. carm.; 14,8 "haec attende {(sc. homo)} #sagacite.|r."

     BEDEUTUNG studiose -- beflissen, eifrig:
             * ROD. GLAB. Wilh.; 5 "illa {(anus)} ... religiosis quibusque #sagacite.|r intimare curavit, quoniam {eqs.}"
     BEDEUTUNG fortiter, audaciter -- tapfer, mutig:
         * LIBER hist. Franc.; 5 p. 245,6 "venientes ... #sagacite.|r in finibus Toringorum ibique resederunt {(sc. Franci)}."; 9 "Chlodovecus ... regnum Francorum viriliter (#sagacite.|r {rec. B}) suscepit."
         * CHRON. Fred. cont.; 14 "egregius bellator Carlus princeps regionem Burgundie #sagacite.|r penetravit."
         ANHÄNGER  in malam partem i. q. procaciter -- frech, dreist:
             * AETHICUS; 102,17 "consurrexit ... Romulus super avum, Numitorem interfecit, regnum #sagacite.|r et adroganter usurpavit."

     BEDEUTUNG de nave i. q. velociter -- schnell:
             * AETHICUS; 51 "classem lignis levigatis factam, velocem, onera maris vel fluviorum #sagacite.|r adferentem navale commercio {(sc. Aethicus ait)}."
             
AUTORIN Weber
"""


TEST_STRUKTUR_sagax = """LEMMA sagax
    GRAMMATIK 
    ETYMOLOGIE 
    SCHREIBWEISE 
    STRUKTUR 
    BEDEUTUNG facile sentiens, attentus, vigil -- leicht spürend, aufmerksam, wachsam:
        U1 proprie:
            U2 in univ.:
                U3 de animalibus:
                    @ sagax_13 #sagac.|es
                    @ sagax_14 intendendum
                U3 de hominibus:
            U2 sollers -- geschickt:
                @ sagax_12 #saga.|x
                @ sagax_4 #saga.|ses
        U1 translate:
            U2 de hominibus:
                U3 mente acer, prudens -- scharfsinnig, klug:
                    U4 in univ.:
                        U5 usu communi:
                        AHG usu subst:
                            @ sagax_1 #saga.|tioribus
                        U5 c. abl. vel gen.:
                            @ sagax_9 #saga.|x
                            @sagax_10 #saga.|x
                            @ sagax_8 #sagac.|issime
                            @ sagax_11 #sagac.|es
                        U5 c. praep.:
                            @ sagax_17 #saga.|x
                            @ sagax_18 #saga.|x
                        U5 usu praed. pro adv.:
                    U4 iuncturae "mens, ingenium" {sim.} "#saga.|x":
                        @ sagax_2 #saga.|ti
                    AHG adde:
                    U4 de sermone:
                        @ sagax_7 #sacacae
                        @ sagax_6 #sagac.|is
                U3 studiosus, intentus -- beflissen, eifrig:
                    @ sagax_19 #sagac.|e
                    @ sagax_16 #saga.|tiores
                AHG in malam partem i. q. avidus -- gierig:
                    @ sagax_5 #sagac.|e
                U3 fortis, comprobatus -- stark, verlässlich:
                U3 diligens -- umsichtig:
                U3 astutus -- schlau, verschlagen (usu subst.):
            U2 de natura i. q. sapiens -- weise:
            U2 de angelis i. q. velox -- schnell:
                @ sagax_15 #sagac.|e
            U2 de rebus i. q. commodus -- angemessen:
SUB_LEMMA sagaciter
    GRAMMATIK 
    BEDEUTUNG diligenter, prudenter -- sorgfältig, klug:
        U1 in univ.:
            @ sagax_3 #sagax
        U1 attente -- aufmerksam:
    BEDEUTUNG studiose -- beflissen, eifrig:
    BEDEUTUNG fortiter, audaciter -- tapfer, mutig:
    AHG in malam partem i. q. procaciter -- frech, dreist:
    BEDEUTUNG de nave i. q. velociter -- schnell:
    AUTORIN  Weber"""


def as_tree(docsyms):
    l = []
    for sym in docsyms:
        l.append(' '.join([sym['name'], sym['detail']]))
        l.extend('    ' + s for s in as_tree(sym['children']))
    return l


class TestServerFunctions:
    def test_gather_symbols(self):
        buffer = TextBuffer(TEST_ARTIKEL_sagax)
        symbole, anker, verweise = gather_symbols(buffer.buffer)
        # import json
        # print(json.dumps(symbole[0], indent=2))
        tree = as_tree(symbole)
        # print('\n'.join(tree))
        assert '\n'.join(tree) == TEST_STRUKTUR_sagax
        # print(anker)
        # print(verweise)

    def test_gather_symbols_fault_tolerance(self):
        with open('satis.mlw', 'r', encoding='utf-8') as f:
            scara = f.read()
        buffer = TextBuffer(scara)
        try:
            symbole, anker, verweise = gather_symbols(buffer.buffer)
            # print(symbole)
            # print(anker)
            # print(verweise)
            # print('\n'.join(as_tree(symbole)))
        except Exception:
            assert False, "Fehler sollten abgefangen werden!"

    def test_anker_sortierung(self):
        anker_liste = ["@anker_3", "@anker_10", "@anker_200", "@anker_4000", "@anker_15000"]
        anker_liste.sort()
        assert anker_liste == ['@anker_10', '@anker_15000', '@anker_200', '@anker_3', '@anker_4000']
        anker_liste.sort(key=anker_sortierung)
        assert anker_liste == ['@anker_3', '@anker_10', '@anker_200', '@anker_15000', '@anker_4000']

    def test_Vorschlagsliste(self):
        import MLWSchnipsel
        completion_labels = [item['label'].upper() for item in MLWSchnipsel.completion_items]
        a, b = shortlist(completion_labels, '* GLOSS. med.'.upper())
        assert completion_labels[a:b] == [
            '* GLOSS. MED. COD. MON.; P. 24,43',
            '* GLOSS. MED. COD. PETROPOL.; P. 273,19',
            '* GLOSS. MED. COD. TREV.; P.',
            '* GLOSS. MED.; P. 96,19']
        a, b = shortlist(completion_labels, '* GLOSS. med. cod.'.upper())
        assert completion_labels[a:b] == [
            '* GLOSS. MED. COD. MON.; P. 24,43',
            '* GLOSS. MED. COD. PETROPOL.; P. 273,19',
            '* GLOSS. MED. COD. TREV.; P.']
        a, b = shortlist(completion_labels, '* GLOSS. med. cod. P'.upper())
        assert completion_labels[a:b] == ['* GLOSS. MED. COD. PETROPOL.; P. 273,19']



if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
