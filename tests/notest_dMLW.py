#!/usr/bin/env python3

"""test_dMLW.py - Einheitentests für das dMLW-modul zum
                  herunterlaten von Daten aus der MLW-DB.

Author: Alexander Häberlin <alexander.haeberlin@mlw.badw.de>

Copyright 2020 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import configparser
import json
import os
import re
import sys


skript_pfad = os.path.dirname(__file__)

# Pfad zu den Zugangsdaten im Heimverzeichnis des Test-Nutzers

zugangsdaten_pfad = os.path.abspath(
    os.path.join('H:', 'Geheim', 'dMLW.ini'))
if not os.path.exists(zugangsdaten_pfad):
    zugangsdaten_pfad = os.path.abspath(os.path.join(
        os.path.expanduser('~'), 'Dokumente', 'Zugangsdaten', 'dMLW.ini'))
    # os.path.join(os.path.expanduser('~'), 'Geheim', 'dMLW.ini'))
assert os.path.exists(zugangsdaten_pfad) and os.path.isfile(zugangsdaten_pfad)

# Pfad zum zu testenden dMLW.py-Skript

sys.path.append(os.path.abspath(
    os.path.join(skript_pfad, '..', 'Hilfsskripte')))
print(sys.path)

import dMLW


class TestdMLW:
    def setup_method(self):
        config = configparser.ConfigParser()
        config.read(zugangsdaten_pfad)
        credentials = config['dMLW']
        self.reader = dMLW.dMLW(credentials['user'],
                                credentials['password'],
                                credentials['URL'])

    def teardown_method(self):
        pass

    def test_opera(self):
        view = 'opera_view'

        # Abruf der Version

        version = self.reader.version(view)
        assert re.match(r'\d\d\d\d-\d\d-\d\d\s+\d\d:\d\d:\d\d', version), \
            "Datums- und Zeitangabe erwartet, aber nicht: " + version

        # Ein einfacher Normalfall

        j_str, j_dump = self.reader.load(view, {'in_use': 1}, ['id', 'example'],
                                         ['author_abbr', 'work_abbr_sort'])
        assert json.loads(j_str) == j_dump, "Rückgabewerte sollte in beiden " \
                "Formen (string und json-object) gleich sein"

        # Ein komplexerer Normalfall: mit allen wichtgen Tabellenfeldern

        j_str, j_dump = self.reader.load(
            view, {'in_use': 1},
            ['id', 'in_use', 'opus', 'example', 'work_abbr', 'work_abbr_sort',
             'work_full', 'author_id', 'author_abbr', 'author_full',
             'date_display', 'date_sort', 'date_type', 'txt_info',
             'bibliography', 'citation', 'is_maior', 'editions'])
        assert json.loads(j_str) == j_dump, "Rückgabewerte sollte in beiden " \
                "Formen (string und json-object) gleich sein"

        # Fehlerbehandlung: fehlende Eingabewerte

        try:
            j_str, j_dump = self.reader.load()
            assert False, "Ein ValueError hätte ausgeworfen werden müssen!"
        except ValueError:
            pass

