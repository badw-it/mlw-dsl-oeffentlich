#!/usr/bin/python3

"""
test_MLWLinting.py -- testet die MLWLinter-Klasse
aus dem MLWCompiler.py-skript
"""

import copy
import sys
from collections.abc import Sequence

sys.path.extend(['../', '../DHParser'])

from MLWParser import WARNUNG_DATIERUNG_ALS_STELLENANGABE
from MLWCompiler import lemma_verdichtung, Verdichtungsfehler, \
    MLWCompiler, AusgabeTransformation, get_Ausgabe_Transformation, kompiliere_schnipsel, \
    HTMLTransformation, FEHLER_AUTOR_FEHLT, get_grammar, get_transformer, \
    WARNUNG_EINSCHUB_ERWARTET, FEHLER_DOPPELTER_ANKER, WARNUNG_REDUNDANTER_STELLENABSCHNITT, \
    WARNUNG_FALSCHE_AUTORANGABE, FEHLER_STELLENANGABE_NICHT_ABGETRENNT, \
    WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE, TextÄnderungenMixin, KLAMMERN_FETT_SCHWELLE, \
    MLWLinter, WARNUNG_FALSCH_EINGEORDNETER_ZUSATZ, WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT, \
    HINWEIS_LEMMAWORT_OHNE_VERDICHTUNG, WARNUNG_EDITIONSANGABE_OHNE_DOPPELKLAMMERN, \
    FEHLER_STELLENANGABE_NICHT_ABGETRENNT, FEHLER_ADDE_FORMATIERUNG
from DHParser.error import is_error, ERROR, MANDATORY_CONTINUATION_AT_EOF, \
    MANDATORY_CONTINUATION_AT_EOF_NON_ROOT
from DHParser.compile import process_tree
from DHParser.nodetree import parse_sxpr, parse_xml, flatten_sxpr, RootNode, Node, EMPTY_NODE, \
    has_token, add_token, eq_tokens, LEAF_NODE, ANY_NODE, LEAF_PATH, ANY_PATH, \
    has_class, pick_from_path
# from DHParser.transform import delimit_children, node_maker
from DHParser.toolkit import re, matching_brackets


class TestMLWLinter:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.mlw_verification = MLWLinter()

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        self.mlw_compiler(st)
        return self.mlw_verification(st)

    def expect_warning(self, src: str, root_parser, warning_code, show_tree=False):
        st = self.compile(src, root_parser)
        if show_tree:  print(st.as_sxpr())
        if isinstance(warning_code, Sequence):
            error_codes = [e.code for e in st.errors_sorted]
            i = -1
            for code in warning_code:
                try:
                    k = error_codes.index(code)
                except IndexError:
                    assert False, f"Warnung oder Fehler {code} erwartet aber nicht erhalten!"
                assert k > i, f"Warnung oder Fehler {code} vor {error_codes[i]} erwartet, nicht danach!"
                i = k
        else:
            error_codes = {e.code for e in st.errors} - {1030, 1040}
            if warning_code == 0:
                assert not error_codes, f'Keine Warnungen oder Fehler erwartet, aber {error_codes} erhalten:' + str(st.errors)
            elif warning_code < 0:
                assert error_codes, 'Warnung oder Fehler erwartet, aber keine gefunden!'
            elif warning_code not in error_codes:
                assert False, f'Warnung oder Fehler {warning_code} erwartet, aber {error_codes} erhalten!'

    def test_LemmaZusatz(self):
        src = """LEMMA *facitergula (fasc-, -iet-, -ist-, -rcu- {sim.})"""
        self.expect_warning(src, 'LemmaPosition', 0)
        src = """LEMMA *facitergula {sim.}"""
        self.expect_warning(src, 'LemmaPosition', 0)
        src = """LEMMA *facitergula (fasc-, -iet-, -ist-, -rcu-) {sim.}"""
        self.expect_warning(src, 'LemmaPosition', WARNUNG_FALSCH_EINGEORDNETER_ZUSATZ)

    # def test_Zitat_nummerierter_Lemmata(self):
    #     src = '''
    #     ETYMOLOGIE
    #         fere in chartis Italiae septemtrionalis:
    #              * {cf.} Wartburg, Frz. etym. Wb. IV.; p. 728sqq.
    #              * Battisti-Alessio, Diz. etim. ital. III.; p. 2111 s. v. 2. "ischia"
    #              * F. Giger et al., Dicziunari Rumantsch Grischun X.; p. 130sq. s. v. 1. "isla"
    #     '''
    #     self.expect_warning(src, 'EtymologiePosition', 0, show_tree=True)

    def test_Etymologie_ungesichert_als_Zusatz(self):
        src = '''
            ETYMOLOGIE
                 ?franc. inf. vet. +fillo:
                  * {v.} A. de Sousa Costa, Studien zu volkssprachigen
                    Wörtern in karolingischen Kapitularien. 1993.; p. 334sqq.
        '''
        self.expect_warning(src, 'EtymologiePosition', 0)
        src = '''
            ETYMOLOGIE
                 {?}franc. inf. vet. +fillo:
                  * {v.} A. de Sousa Costa, Studien zu volkssprachigen
                    Wörtern in karolingischen Kapitularien. 1993.; p. 334sqq.
        '''
        self.expect_warning(src, 'EtymologiePosition', WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT)

    # def test_Literatur_in_Bedeutungsangaben(self):
    #     src = """in iunctura "regale #sacerdoti.|um" (({spectat ad} $VULG.;
    #               I Petr. 2,9 * {de re v.} J. Fried, DtArch. 19. 1973.; p. 508sqq.)):"""
    #     self.expect_warning(src, 'Bedeutungsangabe', 0, show_tree=True)

    def test_Lemmawort_ohne_Verdichtung(self):
        src="#{ha! ha! he!}"
        self.expect_warning(src, 'Lemmawort', 0)
        src="#inept.|ae"
        self.expect_warning(src, 'Lemmawort', 0)
        src="#ineptae"
        self.expect_warning(src, 'Lemmawort', HINWEIS_LEMMAWORT_OHNE_VERDICHTUNG)

    # def test_Datierung_als_Stellenangabe(self):
    #     self.expect_warning('* ANNAL. Plac.; a. 1266; p. 516,21', 'Belege',
    #                         WARNUNG_DATIERUNG_ALS_STELLENANGABE)
    #     # Ausnahme ANNAL. Frising, #Ticket 375
    #     self.expect_warning('* ANNAL. Frising.; a. 1264 "tonitruum"', 'Belege', 0)


    def test_unmarkierte_Editionsangaben(self):
        src = """* BERNOLD. CONST. chron.; p. 400,2 (ed. Pertz) "sedem apostolicam
    tirannice invasit eamque ... plures annos -vit {Guibertus}
    ((* {inde} ANNAL. Gottwic.; a. 1085 ((MGScript. IX p. 601,38))
      * {sim.} BERNOLD. CONST. chron.; a. 1087 p. 463,16
      ((ed. Robinson)) ))." """
        self.expect_warning(src, 'Belege',
            [WARNUNG_EDITIONSANGABE_OHNE_DOPPELKLAMMERN,
             FEHLER_STELLENANGABE_NICHT_ABGETRENNT],
            show_tree=False)

    def test_falscher_Doppelpunkt_bei_VIDE(self):
        src = '''LEMMA [*sconficio VIDE *disconficio: {adde ad vol. III. p. 726,70:} *NOTAE Pis. ; a. 1154 ((MGScript. XIX; p. 266,38)) "fuit {(sc. Conradus rex)} ibi #sconficto per fame."]'''
        self.expect_warning(src, '_VerweisBlöcke', FEHLER_ADDE_FORMATIERUNG)


if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
