#!/usr/bin/python3

"""
test_MLWAusgabeKurztests.py -- testet die Ausgabe auf UTF-8 Ebene,
wodurch sich zahlreiche typografische Fehler auf sehr einfache
Weise abfangen lassen.
"""

from MLWCompiler import MLWCompiler, get_grammar, get_transformer, AusgabeTransformation, \
    WARNUNG_OP_CIT_NICHT_ZUZUORDNEN
from DHParser.nodetree import RootNode
from DHParser.error import is_error
from DHParser import testing

POSSIBLE_ARTIFACTS = set(testing.POSSIBLE_ARTIFACTS) | {WARNUNG_OP_CIT_NICHT_ZUZUORDNEN}


class TestAusgabeKurztest:
    def setup_method(self):
        self.mlw_grammar = get_grammar()
        self.mlw_transformer = get_transformer()
        self.mlw_compiler = MLWCompiler()
        self.at = AusgabeTransformation()
        # raise NotImplementedError

    def compile(self, src: str, root_parser) -> RootNode:
        st = self.mlw_grammar(src, root_parser)
        self.mlw_transformer(st)
        return self.mlw_compiler(st)

    def probe(self, parser, quelltext, erwartet):
        rn = self.compile(quelltext, parser)
        rn.errors = [err for err in rn.errors if is_error(err)]
        print("\nDaten-XML:")
        print("----------")
        print(rn.as_sxpr())
        rn = self.at(rn)
        print("\nAusgabe-XML:")
        print("------------")
        print(rn.as_sxpr())
        rn.errors = []
        gefunden = str(rn).strip()
        # print("\nErgebnis-Zeichenkette: " + gefunden + "\n\n")
        if erwartet:
            # assert gefunden == erwartet, f"\nErwartet: {erwartet}\nGefunden: {gefunden}\n"
            print('\n')
            print(erwartet)
            print(gefunden)
            pointer = []
            for i in range(max(len(erwartet), len(gefunden))):
                if i < len(erwartet) and i < len(gefunden) and erwartet[i] == gefunden[i]:
                    pointer.append(' ')
                else:
                    pointer.append('^')
            print(''.join(pointer))
        else:
            print(gefunden)

    def pruefe(self, parser, quelltext, erwartet, *, ignore_errors=False):
        rn = self.compile(quelltext, parser)
        rn.errors = [err for err in rn.errors
                     if is_error(err) and err.code not in POSSIBLE_ARTIFACTS]
        assert ignore_errors or not rn.errors
        rn = self.at(rn)
        rn.errors = []
        gefunden = str(rn).strip()
        if erwartet:
            # print(rn.as_sxpr())
            assert gefunden == erwartet, str(erwartet) + ' != ' + str(gefunden)
        else:
            print(gefunden)

    def test_indcl(self):
        self.pruefe('Grammatik', 'subst. unbestimmt; indecl. m.', 'subst. indecl. m.')

    def test_mehrfachgenus(self):
        self.pruefe(
            'GrammatikPosition', """
            GRAMMATIK subst. II; -i m.
                f.: {=> sandalus_01}""",
            "-i m. (f.: ⇒😯)")

    def test_runde_und_eckige_Klammern(self):
        self.pruefe('BelegText',
                    '''"capillis ((* {cf.} J. Ruska, op. cit. ({=> sal_10}); p. 53))."''',
                    'capillis (cf. J. Ruska, op. cit. [⇒😯] p. 53).')

    def test_dreifach_geschachtelte_Klammern(self):
        # Ticket 398
        self.pruefe('Belege',
                    '''* EKKEH. IV. cas. ; 25 p. 200,13 "Augensis {(sc. abbatia)} ... est 
                    ({vide notam ed.}) (({cf. et} * NON. ; p. 251,13 "satullem pro saturem 
                    [satulem {pars codd.} (({cf.} * VARRO Men. ; 401))]"))."''',
                    'Ekkeh. IV. cas. 25 p. 200,13 Augensis (sc. abbatia) ... est (vide notam ed.; '
                    'cf. et Non. p. 251,13 satullem pro saturem [satulem pars codd. (cf. Varro '
                    'Men. 401)]).')

    def test_Punkt_am_Ende(self):
        self.pruefe('Belege',
                    '''* TRAD. Frising.; 532 "quedam #sanctimonial.|is f."''',
                    'Trad. Frising. 532 quedam -is f.')
        self.pruefe('Belege',
                    '''* TRAD. Frising.; 224 (a. 806) "Deo sacrata et #sanctimonial.|is femina
                        ((
                         ;532 "quedam #sanctimonial.|is f."))
                           ((* {sim.} DIPL. Ludow. Germ.; 67 p. 94,9 "ut ... #sanctimonial.|ium feminarum..."
                           {saepe}))"''',
                    'Trad. Frising. 224 (a. 806) Deo sacrata et -is femina (532 quedam -is f.; sim. Dipl. Ludow. Germ. 67 p. 94,9 ut ... -ium feminarum... saepe).')

    def test_kein_Punkt_am_Ende(self):
        self.pruefe('MetrikPosition',
                    '''
                    METRIK
            
                    metr.:
                        satāgunt ((* {cf.} Stotz, Handb.; VII § 97.2)): {=> satago_3a; satago_3b}''',
                    'metr. satāgunt (cf. Stotz, Handb. VII § 97.2):  ⇒😯 ⇒😯.')
        pass

    def test_Verweise_Punkt_nach(self):
        self.pruefe('Bedeutungsangabe',
                    '''"#sal nitri, nitrum";
                       usu antiquo de spuma nitri ('Soda', 'Alkali'), e. g. {=> sal_18a; sal_18b};
                       usu recentiori de borace ('Salpeter'), e. g. {=> sal_19a; sal_19b}
                       ((* {cf.} D. Goltz, op. cit. ({=> sal_23}); p. 165sqq.))
                       ((* C. Prisner, K. Figala, op. cit. ({=> sal_17}); p. 318sq.)):''',
                    "sal nitri, nitrum (usu antiquo de spuma nitri [‘Soda’, ‘Alkali’], e. g. ⇒😯 ⇒😯; usu recentiori de borace [‘Salpeter’], e. g. ⇒😯 ⇒😯; cf. D. Goltz, op. cit. [⇒😯] p. 165sqq.; C. Prisner, K. Figala, op. cit. [⇒😯] p. 318sq.):")

    def test_Klammer_Position(self):
        self.pruefe('Belege',
                    '''* AETHICUS; 6 "{terra} hemitica, carpaica, #sata.|yca (#sataic.|a {var. l. ed. Prinz}) 
                    {(cf. comm. ed. Herren p. 60; aliter comm. ed. Prinz p. 92 adn. 33)}."''',
                    'Aethicus 6 terra hemitica, carpaica, -yca (-a var. l. ed. Prinz; cf. comm. ed. Herren p. 60; aliter comm. ed. Prinz p. 92 adn. 33).')

    def test_BelegLemma_in_Zitaten(self):
        self.pruefe('BelegText',
                    '''"in 'summo #sacerdotal.|issimo' {@sacerdotalis_6}"''',
                    '''in ‘summo -issimo’''')
        self.pruefe('BelegText',
                    '''"in 'summo #sacerdotal.|issimo {@sacerdotalis_6}'"''',
                    '''in ‘summo -issimo’''')

    def test_VerweisArtikel(self):
        vwa = '''LEMMA [2. iniustus VIDE in ET iustus]
    LEMMA inkoo VIDE incoho
    AUTORIN Weber'''
        self.pruefe('Artikel', vwa, '[2. iniustus v. in et iustus.]        inkoo v. incoho.Weber')

    def test_Abkuerzungspunkt_beibehalten(self):
        self.pruefe('GebrauchsPosition',
                    '''
                    GEBRAUCH
                            partic. perf. usu adi.: {=> saucio_8} {al.}
                            [usu] subst.: {=> saucio_4; saucio_6} {al.}''',
                    'partic. perf. usu adi.: ⇒😯. al.; subst.: ⇒😯 ⇒😯. al.')

    def test_Überflüssiger_Punkt(self):
        # Ticket 362
        self.pruefe('VerweisArtikel',
                    '''LEMMA [sicrenatus VIDE sic ET renatus: *CHART. Ticin.; 194 p. 27,33.]
                    AUTORIN Pörnbacher''',
                    '[sicrenatus v. sic et renatus: Chart. Ticin. 194 p. 27,33.]Pörnbacher')

    def test_fehlender_Punkt(self):
        # Ticket 361
        self.pruefe('SchreibweisenPosition',
                    '''
                    SCHREIBWEISE
                          script.:
                            ce-:  {=> scelus_3; scelus_9}
                                  * {adde} HERM. IUD. conv.; 2 p. 75,4 {(var. l.)}
                                  * CONR. MUR. nov. grec.; 7,67 {(var. l.)}''',
                    'script. ce-:  ⇒😯 ⇒😯. adde Herm. Iud. conv. 2 p. 75,4 (var. l.). Conr. Mur. nov. grec. 7,67 (var. l.).')

    def test_praeter(self):
        # Ticket 359
        self.pruefe('VerweisArtikel',
                    '''LEMMA scol- VIDE schol- PRAETER
                       AUTOR Mandrin''',
                    'scol- v. schol- praeter:Mandrin')

    def test_AlternativeGrammatik(self):
        self.pruefe('GrammatikVarianten',
                    '({?}m.: {=> scelus_1})',
                    '(?m.: ⇒😯)')

    def test_Fehler_redundante_Stellenangabe(self):
        # Ticket 363
        self.pruefe('SchreibweisenPosition', '''
            SCHREIBWEISE  
                script.:
                    ca-: * THADD. FLORENT. cons.; 47,76
                    scarumcell(a): * ROGER. SALERN. chirurg.; 3,217
                    sor-: {=> sarcocolla8}
                    sacc-: * ANTIDOT. Bamb.; app. p. 36,26
                    saro-: * GLOSS. Roger. I B; p. 131,1 ((ed. Nalesso))
                    sarto-: {=> sarcocolla2}
                    sarcolla:   {=> sarcocolla7} * {adde} GLOSS. Roger. I B; p. 49,8sqq. ((ed. Nalesso)) {ibid. al.}
                    -chollam: * GLOSS. Roger. I B; p. 131,1 ((ed. Nalesso))
                    -cocla:  {=> sarcocolla3}
                    -culla: {=> sarcocolla8; sarcocolla9}
                    -olam:  {=> sarcocolla5}''',
                    'script.:    ca-:  Thadd. Florent. cons. 47,76.    scarumcell(a):  Roger. Salern. chirurg. 3,217.    sor-:  ⇒😯.    sacc-:  Antidot. Bamb. app. p. 36,26.    saro-:  Gloss. Roger. I B p. 131,1 (ed. Nalesso).    sarto-:  ⇒😯.    sarcolla:  ⇒😯. adde Gloss. Roger. I B p. 49,8sqq. (ed. Nalesso). ibid. al.    -chollam:  Gloss. Roger. I B p. 131,1 (ed. Nalesso).    -cocla:  ⇒😯.    -culla:  ⇒😯 ⇒😯.    -olam:  ⇒😯.')

    def test_EtymologiePosition(self):  # issue 370
        self.pruefe('EtymologiePosition', '''
            ETYMOLOGIE ital. scavino, {cf. {=> scavignatio/scavignator_1}}''',
                    '(ital. scavino, cf. )')  # kein zweiter Punkt nach den (hier unsichtbaren) Verweisen vor der Klammer!

    def test_VerweisBlock(self):
        self.pruefe('_VerweisBlöcke', '''LEMMA [scomelo VIDE *excommeio {(add.)}]''',
                    '[scomelo v. ✲excommeio (add.).]')
        self.pruefe('_VerweisBlöcke',
                    '''LEMMA [ideum VIDE antidotum. {adde ad vol. I. p. 709,27}: ANTIDOT. Glasg.; p. 106,36 "cousque <<conpleas antidotum>> ({cod.,} conplectant ideum {ed.})."]''',
                    '[ideum v. antidotum.    adde ad vol. I. p. 709,27: Antidot. Glasg. p. 106,36 cousque conpleas antidotum (cod., conplectant ideum ed.).]')
        self.pruefe('_VerweisBlöcke',
                    '''LEMMA [ideum VIDE antidotum {adde ad vol. I. p. 709,27}: ANTIDOT. Glasg.; p. 106,36 "cousque <<conpleas antidotum>> ({cod.,} conplectant ideum {ed.})."]''',
                    '[ideum v. antidotum.    adde ad vol. I. p. 709,27: Antidot. Glasg. p. 106,36 cousque conpleas antidotum (cod., conplectant ideum ed.).]')

    def test_Etymon_Klammern(self):
        self.pruefe('EtymologiePosition', '''
                    ETYMOLOGIE  *scac(c)us {et angl.} exchequer''', '(✲scac(c)us et angl. exchequer)')

    def test_Lemmaangabe_Punkt_auch_ohne_Flexion(self):
        self.pruefe('SubLemmaPosition', '''SUB_LEMMA  scaeve (seve, sceve)\n    GRAMMATIK  adverbium''',
                    'adv. scaeve (seve, sceve).')
        pass

    def test_Verweis_adde_Doppelpunkt(self):
        self.pruefe('VerweisArtikel',
                    '''LEMMA [2. secundus VIDE fecundus. {adde ad vol. IV. p. 114,57sq.}: * AEDILV. carm.; 21,36 "cuius {(Christi)}"]
 
                    AUTORIN Niederer
                    ''',
                    '[2. secundus v. fecundus.    adde ad vol. IV. p. 114,57sq.: Aedilv. carm. 21,36 cuius (Christi).]Niederer')
        self.pruefe('VerweisArtikel',
                    '''LEMMA [2. secundus VIDE fecundus. {adde ad vol. IV. p. 114,57sq.}: * AEDILV. carm.; 21,36 "cuius {(Christi)}" 
                                                                                          * EPIST. Hann.; 18 p. 42,11 "si secundissimam"]
 
                    AUTORIN Niederer
                    ''',
                    '[2. secundus v. fecundus.    adde ad vol. IV. p. 114,57sq.: Aedilv. carm. 21,36 cuius (Christi). Epist. Hann. 18 p. 42,11 si secundissimam.]Niederer')
        pass

    def test_Klammer_Semikolon_Ersetzung(self):
        self.pruefe('Belege',
                    ''' * INSCR. Germ.; LXX 79 B 5 (a. 1035/48?) ((vs.)) "#altipeten.|s vates ... ille Iohannes {(sc. evangelista)}." ''',
                    'Inscr. Germ. LXX 79 B 5 (a. 1035/48?; vs.) -s vates ... ille Iohannes (sc. evangelista).')
        self.pruefe('Belege',
                    '''* PS. BEDA arithm.; 1 l. 48 ((ed. 1972; rec. β)) "quoties novem #concepto.|r numeri se habere responderit, toties duo a divinante colligendi sunt." ''',
                    'Ps. Beda arithm. 1 l. 48 (ed. 1972; rec. β) quoties novem -r numeri se habere responderit, toties duo a divinante colligendi sunt.')
        self.pruefe('Belege',
                    '''* PS. BEDA arithm.; 1 l. 48 ((ed. 1972)) ((rec. β)) "quoties novem #concepto.|r numeri se habere responderit, toties duo a divinante colligendi sunt." ''',
                    'Ps. Beda arithm. 1 l. 48 (ed. 1972; rec. β) quoties novem -r numeri se habere responderit, toties duo a divinante colligendi sunt.')

    def test_kein_Punkt_nach_GrammatikVarianten(self):
        # Ticket 412
        self.pruefe('LemmaPosition', '''
        LEMMA sia
        GRAMMATIK
            subst. I; -ae f. ({an} -um, -i n.{?})''',
                    'sia, -ae f. (an -um, -i n.?)')

    def test_kein_Punk_vor_Klammer(self):

        # Ticket 428
        self.pruefe('Bedeutungsangabe',
                    '''nota musica -- 'Semibrevis' ((* A. Riethmüller, Handwörterb. d. musikal. Terminologie. V. 1971.; s. v. "semibrevis")):''',
                    'nota musica — ‘Semibrevis’ (A. Riethmüller, Handwörterb. d. musikal. Terminologie. V. 1971. s. v. semibrevis):')

    def test_pruefe_wa_ergaenzungs_verdoppelung(self):
        # Ticket 406
        self.pruefe('GrammatikPosition', '''
                     GRAMMATIK coni. vel adv.''',
                    'coni. vel adv.')

    def test_Abstand_hochgestellte_Stellenangabe(self):
        # Ticket #379
        self.pruefe('Beleg', '* AETHICUS;^{tit.} p. 2 "incipit"', 'Aethicustit. p. 2 incipit.')

    def test_eckige_Klammern(self):
        # Ticket #442
        self.pruefe('Beleg', '* TRACT. de chirurg.; 325 "brionie libras VIII, #sici.|dis {@sicyos13} '
                             '(gloss. $I: id est cucumeres agrestes) (#sicludis {@sicyos12} (#sic.|hidis {ind.}) {$II}) l. IIII."',
                    'Tract. de chirurg. 325 brionie libras VIII, -dis (gloss. I: id est cucumeres agrestes; sicludis [-hidis ind.] II) l. IIII.')

    def test_erzwungene_Klammern(self):
        # Ticket #445
        self.pruefe('Beleg', '* COMPOS. Luc.; U 25 "ter\[r\]es calcu"', 'Compos. Luc. U 25 ter[r]es calcu.')

    def test_athetesen_Klammern(self):
        # athetese, durch eckige Klammern markiert
        self.pruefe('BelegText', '"perfecta, ... [et] {(delendum, ut vid.)} salus"',
                    'perfecta, ... [et] (delendum, ut vid.) salus')
        # keine athetese
        self.pruefe('BelegText', '"[satulem {pars codd.} (({cf.} * VARRO Men. ; 401))]"',
                    '(satulem pars codd. [cf. Varro Men. 401])')

    def test_falsche_eckige_Klammer(self):
        self.pruefe('EtymologiePosition', '''
    ETYMOLOGIE {cf.} ital. secchiaio, secchiaro {ex} ital. secchia [{ex} sicla {pro} situla]: 
        * {v.} Battaglia, Dizionario. XVIII.; p. 395
        * D. Piccini, Lessico Latino medievale in Friuli. 2006.; p. 431''',
                    '(cf. ital. secchiaio, secchiaro ex ital. secchia [ex sicla pro situla]; v. Battaglia, Dizionario. XVIII. p. 395; D. Piccini, Lessico Latino medievale in Friuli. 2006. p. 431)')

    def test_Leerschlagbeseitigung_vor_Anker(self):
        self.pruefe('BelegText',
                    '"({corr. ex} #scrupo|.lositate {@ scrupulositas_1}) ((ed. PL 101,644^D  "dubitatione"))"',
                    '(corr. ex scrupo-; ed. PL 101,644D dubitatione)', ignore_errors=True)

    def test_falscher_Punk(self):
        # Ticket 458
        self.pruefe('Belege',
                   '* HRABAN. epist.; 2^a p. 382,35 ({var. l.})',
                   'Hraban. epist. 2a p. 382,35. (var. l.)')
        self.pruefe('Belege',
                   '* HRABAN. epist.; 2^a p. 382,35 {(var. l.)}',
                   'Hraban. epist. 2a p. 382,35 (var. l.).')


if __name__ == "__main__":
    from DHParser.testing import runner
    runner("", globals())
