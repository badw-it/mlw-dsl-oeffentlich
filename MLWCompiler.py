#!/usr/bin/env python3

"""MLWCompiler.py - compiles MLW (medieval latin dictionary) files to XML

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2017 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

#######################################################################
#
# SYMBOLS SECTION - Can be edited. Changes will be preserved.
#
#######################################################################

import copy
import os
import shutil
import sys
import webbrowser
from functools import partial
from itertools import chain
from typing import Container, Tuple, Set, List, Optional, Dict, Iterable, \
    Union, Callable, cast, Any

assert sys.version_info >= (3, 5, 3)

scriptdir = os.path.dirname(os.path.realpath(__file__))
sys.path.extend([os.path.join(scriptdir, 'DHParser')])

try:
    import regex as re
except ImportError:
    import re

from DHParser import is_filename, Grammar, Compiler, Lookahead, Alternative, Text, Synonym, \
    Option, OneOrMore, RegExp, Series, RE, ZeroOrMore, Forward, NegativeLookahead,\
    Required, Whitespace, mixin_comment, compile_source, PreprocessorFunc, Node, \
    TransformationDict, transformation_factory, traverse, reduce_single_child,\
    move_fringes, replace_by_single_child, change_name, name_matches, \
    normalize_whitespace, replace_or_reduce, remove_whitespace, remove_tokens, flatten, \
    collapse, collapse_children_if, traverse_locally, content_matches, prev_path, \
    remove_content, keep_nodes, remove_anonymous_empty, remove_anonymous_tokens, \
    has_parent, is_token, apply_if, apply_unless, remove_children_if, is_anonymous, \
    lstrip, rstrip, strip, is_one_of, not_one_of, linebreaks, line_col, EMPTY_NODE, \
    parse_sxpr, remove_brackets, is_empty, WHITESPACE_PTYPE, ErrorCode, TOKEN_PTYPE,\
    has_errors, Error, recompile_grammar, remove_empty, error_on, THREAD_LOCALS, \
    set_config_value, replace_by_children, RootNode, access_presets, finalize_presets, \
    access_thread_locals, process_tree, ZOMBIE_TAG, merge_adjacent, remove_if, \
    update_attr, get_config_value, Lookbehind, has_descendant, neg, resume_notices_on, \
    set_tracer, trace_history, is_warning, add_error, delimit_children, insert, node_maker, \
    positions_of, any_of, all_of, add_attributes, as_list, first, last, Parser, StringView, \
    flatten_sxpr, set_tracer, NOTICE, WARNING, ERROR, FATAL, ANY_NODE, \
    path_sanity_check, next_path, as_list, ContentMapping, strlen_of, \
    matching_brackets, NOPE, load_if_file, has_ancestor, is_error, select_from_path, \
    select_path_if, select_path, pick_from_path, LEAF_NODE, LEAF_PATH, \
    leaf_path, LAST_CHILD, FIRST_CHILD, next_leaf_path, prev_leaf_path, ensuing_str, \
    set_preset_value, has_class, add_class, remove_class, pick_path, ThreadLocalSingletonFactory, \
    add_token_to_attr, has_token_on_attr, ANY_PATH, content_of, find_common_ancestor, \
    split_node, create_junction, Junction, create_match_function, pick_from_path_if
#  from DHParser.testing import unit_from_file, grammar_suite, TEST_ARTIFACT
from DHParser.log import start_logging

from MLWParser import get_preprocessor, get_grammar, get_transformer, TEXT, WSP, is_wsp, \
    unter_artikel, ERROR_SYMBOL, WARN_SYMBOL, SEARCH_SYMBOL, ASTTransformation
from MLWDaten import opera, daten_vom
from MLWUpdateVSC import update_vscode_extension

import MLWVersion

DEBUG = False


# Pfade für die Ausgabe-Unterverzeichnisse

DATEN_UVZ = os.path.join("Ausgabe", "XML-Daten")
DRUCKVORSTUFE_UVZ = os.path.join("Ausgabe", "XML-Druckvorstufe")
VORSCHAU_UVZ = os.path.join("Ausgabe", "HTML-Vorschau")
DRUCK_UVZ = os.path.join("Ausgabe", "PDF-Druckvorschau")
FEHLERSUCHE_UVZ = os.path.join("Ausgabe", "Wanzensuche_fuer_die_Programmierer")
FEHLERMELDUNGEN_UVZ = os.path.join("Ausgabe", "Fehlermeldungen_fuer_die_Autoren")


#######################################################################
#
# COMPILER SECTION - Can be edited. Changes will be preserved.
#
#######################################################################


GATTUNGSNAME = {'ANON.', 'ACTA.', 'CHART.', 'COD.', 'DIPL.', 'REGISTR.', 'TRAD.', 'CATAL.',
                'ANNAL.', 'CASUS', 'CHRON.', 'GESTA', 'FUND.', 'GENEAL.', 'HIST.', 'ITIN.',
                'NARR.', 'ORIGO', 'RELAT.', 'BREVIAR.', 'COLLECT.', 'DEDIC.', 'DEFENS.',
                'EXCERPT.', 'FRAGM.', 'LIBELL.', 'LIBER', 'LIBRI', 'NOTA', 'NOTAE', 'NOTIT.',
                'NOTULAE', 'TESTAM.', 'CALEND.', 'KALEND.', 'COMPUT.', '(MARTYR.)',
                '(MEMORIALE)', '(NECROL.)', 'MAPPA', 'MAPPAE', 'TAB.', 'CAPIT.', 'CONC.',
                'CONST.', 'DECRET.', '(EDICT.)', 'LEG.', 'LEX', '(PACTUS)', '(PAX)',
                'CONSUET.', 'MISS.', 'ORD.', 'ORDO', 'PONTIF.', 'RITUALE', 'SACRAM.',
                'STATUT.', 'VITA', 'MIRAC.', 'PASS.', 'TRANSL.', 'CONVERS.', 'INVENT.',
                'TRIUMPH.', 'VISIO', 'OFFIC.', 'ORAC.', 'PRAGM.', 'SEQ.', 'VATIC.', 'ALLEG.',
                'CARM.', 'EPIST.', '(DIAL.)', 'FORM.', 'EPITAPH.', 'HYMN.', 'INSCR.', 'RHYTHM.',
                'TIT.', 'TRACT.', 'GLOSS.', 'SCHOL.', 'SUBSCR.', '(LUDUS)', 'SERMO', 'SPEC.',
                'SUMMA', 'SYLL.', 'ANTIDOT.', 'BOTAN.', 'COMPOS.', '(DIAETA)', 'RECEPT.',
                '(MEDIC.)', '(PRACT.)', '(PRAECEPT.)'}


OHNE_WERKTITEL = {'AESCULAPIUS', 'AETHICUS', 'ALIA MUSICA', 'ALPHITA', 'ANNALISTA SAXO', 'ARCHIPOETA',
              'ASINARIUS', 'AURELIUS', 'BREV. NOTIT.', 'CIRCA INSTANS', 'CYMBALA',
              'DIXIT ALGORIZMI', 'DYASC.', 'EUPOLEMIUS', 'FABULAR. MINOR', 'HERACLIUS', 'IOCALIS',
              'LIGURINUS', 'LIOS MON.', 'MAPPAE CLAVIC.', 'MUS. ENCHIR.', 'NECROL.', 'PEREGRINUS',
              'POETA SAXO', 'RAPULARIUS', 'RHYTHMIMACH.', 'RHYTHM.', 'RUODLIEB', 'SALUTARIS POETA',
              'SOLIMARIUS', 'TROTULA', 'UNIBOS', 'WALTHARIUS', 'YSENGRIMUS'}


class Verdichtungsfehler(Exception):
    def __init__(self, fehler):
        super().__init__()
        self.fehler = fehler


def lemmawort_verdichtung(lemma: str, variante: str) -> str:
    """
    VERALTET: (Halb-)automatische Verdichtung wird nicht mehr verwendet,
    da zu häufige Fehler.

    ##### Halbautomatische und manuelle Verdichtung

    In der Regel sollten die in einem Beleg vorkommenden Lemmaworte als
    solche gekennzeichnet werden. Dies geschieht dadurch, dass das
    Lemmawort im Belegtext durch ein vorangestelltes Gitterzeichen "#"
    gekennzeichnet wird, z.B.:

        "Otto rex Galliam #inferiorem petit."

    Im Quelltext sollte immer das vollständige Lemmawort stehen, auch wenn
    in der Druckausgabe später möglicherweise nur eine verdichtete Form
    erscheint, also z.B. "-em" für "inferiorem". Die Verdichtung wird im
    Quelltext durch einen senkrechten Strich kenntlich gemacht:

        "Otto rex Galliam #inferior|em petit."

    Der Computer vergleicht den Teil vor oder hinter dem senkrechten
    Strich mit dem im Artikelkopf (bzw. Sub-Lemma-Kopf) angegebenen Lemma.
    Der Teil, der übereinstimmt,
    wird dann durch einen Verdichtungsstrich "-" ersetzt. In diesem Fall
    lautet das Unterlemma "inferior", so dass als verdichtete Fassung
    "-em" generiert wird.

    Dabei handelt es sich um einen halb-automatischen Vorgang, bei der die
    Verfasserin oder der Verfasser des Artikels durch einen senkrechten
    Strich die Trennstelle(n) für die Verdichtung angibt, der Computer
    aber bestimmt, welche Wortteile verdichtet werden.

    Da Fälle auftreten können, in denen die vom Computer vorgenommene
    Verdichtung nicht den Intentionen der Artikelautorin oder des
    Artikelautors entspricht, besteht auch die Möglichkeit, die
    Verdichtung vollständig manuell festzulegen. Dazu setzt man neben den
    senkrechten Strich auf der Seite desjenigen Segments, das gestrichen
    werden soll einen Punkt, z.B.:

        "ne ... sompnolenti {(sic)} oblivionis #inh|.erti.|a depravata subtrahantur"

    Im Druck wird nun `#inh|.erti.|a` durch die verdichtete Form "inh-a"
    ersetzt werden. Eine halbautomatische Verdichtung wäre deshalb
    schwierig, weil nicht nur das Segment "erti", sondern auch das "a" am
    Schluss mit dem Lemmawort "inertia" übereinstimmen, so dass der
    Computer nach der Regel, Übereinstimmendes wegzulassen, auch das "a"
    weglassen müsste, was aber nicht erwünscht ist.
    """

    def prüfe(rückgabe: str) -> str:
        """Prüft den Rückgabewert noch einmal auf Plausibilität. Keine nur aus '-',
        ' ' oder nichts bestehenden Rückgabewerte erlaubt."""
        if rückgabe.replace('-', '').replace(' ', ''):
            return rückgabe
        else:
            raise Verdichtungsfehler('Bei dieser Verdichtung bleibt nichts mehr vom Wort übrig!')

    lemma = lemma.lower()

    if lemma == variante.lower():
        return prüfe(variante)

    teile = variante.split('|')
    if len(teile) > 3:
        raise Verdichtungsfehler('Fehler bei Lemmaangabe: Mehr als zwei Trennstriche gefunden!')

    punkte = variante.count('.')
    if punkte > 0:
        if len(teile)-1 != punkte:
            raise Verdichtungsfehler('Mehrdeutige manuelle Verdichtung: '
                                     'Jeder | muss mit genau einem . versehen werden!')
        if punkte != variante.count('|.') + variante.count('.|'):
            raise Verdichtungsfehler('Punkte . müssen unmittelbar neben | vorkommen!')
        if variante.find('..') >= 0:
            raise Verdichtungsfehler('Leere Verdichtung bei ' + variante)
        verdichtungsliste = []
        for teil in teile:
            if teil.count('.'):
                if len(teil) > 1:
                    verdichtungsliste.append('-')
                teil = teil.replace('.', '')
                # AUSKOMMENTIERT, da zu viele flasche Fehler geworfen werden
                # if lemma.find(teil) < 0:
                #     raise Verdichtungsfehler(('Gestrichener Teil "%s" ist nicht im lemma ' +\
                #                               '"%s" enthalten') % (teil, lemma))
            else:
                verdichtungsliste.append(teil)
        verdichtung = ''.join(verdichtungsliste)
        return prüfe(verdichtung)

    if len(teile) == 3:
        if not lemma.startswith(teile[0].lower()):
            raise Verdichtungsfehler('Anfang des Lemmaworts stimmt nicht mit Lemma überein!')
        if not lemma.endswith(teile[2].lower()):
            raise Verdichtungsfehler('Ende des Lemmaworts stimmt nicht mit Lemma überein!')
        return prüfe('-' + teile[1] + '-')
    elif len(teile) == 2:
        if teile[0] and teile[1]:
            if lemma.startswith(teile[0].lower()):
                return prüfe('-' + teile[1])
            else:
                if not lemma.endswith(teile[1].lower()):
                    raise Verdichtungsfehler(
                        'Weder Anfang noch Ende des Lemmaworts "%s" stimmen mit dem Lemma "%s" '
                        'überein! Bitte durch einen Punkt neben dem senkrechten Strich die Seite '
                        'markieren, die gestrichen werden soll. Beispiel: "irrationalitat.|em"'
                        % (variante, lemma))
                return teile[0] + '-'
        else:
            return prüfe(teile[0] if teile[0] else teile[1])

    return variante  # keine automatische Verdichtung mehr, da zu unzuverlässig

def lemma_verdichtung(lemma: str, variante: str) -> str:
    if variante.find(' ') >= 0:
        varianten = [v for v in variante.split(' ') if v]
        worte = [lemma if v.find('|') >= 0 else v for v in varianten]
        verdichtete = [lemmawort_verdichtung(w, v) for w, v in zip(worte, varianten)]
        return ' '.join(verdichtete)
    else:
        return lemmawort_verdichtung(lemma, variante)

def kehre_um(verdichtung: Dict) -> Dict:
    return {v: k for k, v in verdichtung.items() if v not in verdichtung}


WortartVerdichtung = {
    "adverbium": "adv.",
    "adiectivum": "adi.",
    "comparativum": "compar.",
    "interiectio": "interi.",
    "coniunctio": "coni.",
    "littera": "littera",
    "numerale": "numer.",
    "particula": "particula",
    "praepositio": "praep.",
    "pronomen": "pron.",
    "substantivum": "subst.",
    "superlativum": "superl.",
    "verbum": "verbum"
}

WortartExpansion = kehre_um(WortartVerdichtung)

GenusVerdichtung = {
    "maskulinum": "m.",
    "femininum": "f.",
    "neutrum": "n.",
}
GenusExpansion = kehre_um(GenusVerdichtung)

CasusVerdichtung = {
    "nominativ": "nom.",
    "genitiv": "gen.",
    "dativ": "dat.",
    "akkusativ": "akk.",
    "ablativ": "abl.",
    "vokativ": "vok."
}
CasusExpansion = kehre_um(CasusVerdichtung)

NumerusVerdichtung = {
    "singular": "sg.",
    "plural": "pl."
}
NumerusExpansion = kehre_um(NumerusVerdichtung)


EtymologieSprachListe = {"anglosax.", "arab.", "bohem.", "byz.", "catal.", "finnice", "franc.",
                         "francog.", "francoprov.", "frisic.", "gall.", "germ.", "gr.", "hebr.",
                         "hibern.", "hisp.", "hung.", "ital.",
                         # "lat-erschlossen", "lat-klass.", "lat-mlw.", "lat.",
                         "occ.", "pers."
                         "polon.", "port.", "raetoroman.", "saxon", "sard.", "sicil.", "slav.",
                         "theod.", "turc.", "val."}


HINWEIS_VERWEISZIEL_LEER = ErrorCode(50)
HINWEIS_REDUNDANTE_STELLENANGABE_ELIMINIERT = ErrorCode(55)
HINWEIS_DISAMIGUIRUNG_DER_AUTORANGABE = ErrorCode(60)

WARNUNG_AUTOR_POSITION = ErrorCode(501)
WARNUNG_ANKER_FEHLT = ErrorCode(503)
WARNUNG_ÜBERFLÜSSIGE_KLAMMERN = ErrorCode(504)
WARNUNG_UNBEKANNTE_ETYMOLOGIE_SPRACHE = ErrorCode(505)
WARNUNG_STELLENANGABE_ALS_VARIA_LECTIO_MARKIERT = ErrorCode(506)
WARNUNG_EINSCHUB_ERWARTET = ErrorCode(507)
WARNUNG_ZUSATZ_ERWARTET = ErrorCode(508)
WARNUNG_UNAUSGEWOGENE_KLAMMER = ErrorCode(509)
WARNUNG_WIEDERHOLTE_STELLENAUFTEILUNG = ErrorCode(510)
WARNUNG_REDUNDANTER_STELLENABSCHNITT = ErrorCode(511)
WARNUNG_FALSCHE_AUTORANGABE = ErrorCode(512)
WARNUNG_OP_CIT_ERWARTET = ErrorCode(513)
WARNUNG_NICHT_GEKENNZEICHNETER_BELEGTEXT = ErrorCode(514)
WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE = ErrorCode(515)
WARNUNG_MEHRGLIEDRIGES_LEMMA = ErrorCode(516)
WARNUNG_ENKLITISCHER_PARTIKEL = ErrorCode(517)
WARNUNG_FEHLENDE_ANFÜHRUNGSSTRICHE = ErrorCode(518)
WARNUNG_ABGEKÜRZTE_STELLENANGABE = ErrorCode(519)
WARNUNG_GEÄNDERTE_KAPITELNUMMER = ErrorCode(520)
WARNUNG_FEHLENDER_EINSCHUB = ErrorCode(521)
WARNUNG_ANNO_IN_STELLENANGABE = ErrorCode(522)
WARNUNG_REDUNDANTE_STELLENANGABE = ErrorCode(523)
WARNUNG_EDITIONSANGABE_OHNE_DOPPELKLAMMERN = ErrorCode(524)
WARNUNG_OP_CIT_NICHT_ZUZUORDNEN = ErrorCode(525)
WARNUNG_VERALTETES_KONSTRUKT = ErrorCode(530)
WARNUNG_ZU_VIELE_EDITIONSANGABEN = ErrorCode(535)
WARNUNG_TRENNUNG_WERK_STELLENANGABE = ErrorCode(540)

FEHLER_ZIEL_VERGEBEN = ErrorCode(5001)
FEHLER_AUTOR_FEHLT   = ErrorCode(5010)
FEHLER_WERK_FEHLT    = ErrorCode(5011)
FEHLER_NUR_EINE_UNTERBEDEUTUNG = ErrorCode(5020)
FEHLER_QUELLENANGABE_OHNE_AUTOR = ErrorCode(5030)
FEHLER_DOPPELTER_ANKER = ErrorCode(5040)
FEHLER_ABKUERZUNG_FEHLERHAFT = ErrorCode(5050)
# FEHLER_KEIN_ZIEL_FUER_ANKER = ErrorCode(5050)
FEHLER_ABWEICHENDE_STELLENAUFTEILUNG = ErrorCode(5060)
FEHLER_STELLENANGABE_NICHT_ABGETRENNT = ErrorCode(5070)
FEHLER_FOLGE_USU_OHNE_VORGÄNGER = ErrorCode(5080)
FEHLER_UNKLARE_TMESIS = ErrorCode(5090)
FEHLER_UNBEKANNTES_WERK = ErrorCode(5100)
FEHLER_ADDE_FORMATIERUNG = ErrorCode(5110)


RE_ROEM_ZAHL = r'M*(C[MD]|D?C*)(X[CL]|L?X*)(I[XV]|V?I*)\.?$'

VERWEISZIEL_FEHLT = "VERWEISZIEL_FEHLT"


def ASCII(wort: str) -> str:
    """Ersetzt nicht ASCI-Zeichen durch einen Unterstrich."""
    return re.sub('[^a-zA-Z0-9#]', '_', wort.replace('[', '').replace(']', ''))


def absolutes_verweis_ziel(artikel_name, relatives_ziel):
    """Erzeugt aus dem namen des Artikels und dem Verweisziel
    eine global (d.h. über alle Lemmata hinweg) eindeutige Zielmarke,
    deren Namenskennung LaTeX-kompatibel ist (d.h. keine
    LaTeX-Steuerzeichen wie Unterstriche, Kaufmannsund etc enthält).
    """
    # TODO: sorgfältigere LaTeX-string-Filterung!
    return ASCII(artikel_name).replace('_', '-') + '.' + \
           ASCII(relatives_ziel).replace('_', '-')


class OrdneVerweise:
    """
    Diese Klasse kapselt die Prozeduren, mit denen die Verweise innerhalb
    einer Strecke aufeinanderfolgender Verweise in einem Artikel nach
    ihrem Ziel geordnet werden können.
    """
    def __init__(self, compiler: Compiler):
        self.compiler = compiler
        self.anker = {}  # type: Dict[str, Node]
        self.pseudo_anker = set()  # type: Set[str]
        self.verweis_strecken = []  # type: List[List[Tuple[Node, Node]]]
        self.last_parent = None

    def melde_anker(self, anker_name, anker: Node, ziel: Node):
        """
        Registriert gefundene Anker in der Reihenfolge ihrer Meldung.
        """
        if anker_name in self.anker and anker_name not in self.pseudo_anker:
            if self.compiler.lbreaks is None:
                self.compiler.lbreaks = linebreaks(self.compiler.tree.source)
            self.compiler.tree.new_error(
                anker, 'Ziel "%s" bereits vergeben in Zeile %s Spalte %i !'
                       % (anker.content, *line_col(self.compiler.lbreaks, self.anker[anker_name].pos)),
                FEHLER_ZIEL_VERGEBEN)
        else:
            try:
                self.pseudo_anker.remove(anker_name)
            except KeyError:
                pass
            self.anker[anker_name] = ziel

    def melde_beleglemma(self, node: Node, lemma: str):
        if 'id' not in node.attr and lemma not in self.anker:
            self.pseudo_anker.add(lemma)
            self.anker[lemma] = node
            node.attr['id'] = lemma
            node.attr['anker'] = 'automatisch generiert'
            if not node.has_attr('globaleId'):  # is this check necessary?
                node.attr['globaleId'] = absolutes_verweis_ziel(self.compiler.artikel_name, lemma)

    def melde_verweis(self, node, link, parent):
        """
        Registriert einen Verweis und ordnet ihn seiner Verweisstrecke zu.
        """
        if self.last_parent == parent:
            last_node = self.verweis_strecken[-1][-1][0]
            surrounding = tuple(child for child in parent.children if child.name != 'L')
            if surrounding.index(last_node) == surrounding.index(node) - 1:
                self.verweis_strecken[-1].append((node, link))
            else:
                self.verweis_strecken.append([(node, link)])
        else:
            self.last_parent = parent
            self.verweis_strecken.append([(node, link)])

    def prüfe_verweise(self, root: RootNode):
        """
        Prüft bei internen Verweisen, d.h. Verweisen auf Ziele innerhalb
        desselben Artikels, ob das Verweisziel existiert, d.h. ob auch ein
        Anker gesetzt wurde.
        """
        anker_set = frozenset(self.anker)
        for strecke in self.verweis_strecken:
            for _, link in strecke:
                ziel = link.attr['href']
                if ziel.startswith('#'):
                    if ziel[1:] not in anker_set:
                        root.new_error(link,
                                       'Anker für Verweis auf Ziel "%s" fehlt!' % ziel[1:],
                                       WARNUNG_ANKER_FEHLT)
                        link.result = '⇒' + SEARCH_SYMBOL

    def ordne_verweise(self):
        """
        Ordnet aufeinanderfolgende Verweise nach der Reihenfolge ihrer Ziele.
        """
        pass  # TODO: Verweisordnungsalgorithmus implementieren.


# def eliminate_tag(path: List[Node], tag_name: str):
#     """
#     Removes all nodes with the given tag_name from the tree
#     originating in path[-1].
#     """
#     node = path[-1]
#     if len(path) > 1:
#         if node.name == tag_name:
#             parent = path[-2]
#             parent.result = tuple(child for child in parent.children if child != node)
#     for child in node.children:
#         path.append(child)
#         eliminate_tag(path, tag_name)
#         path.pop()

def mergeable(path: List[Node]) -> bool:
    """Returns True, if nodes can safely be merged. This is the case for
    Whitespace ("L") and Text-Nodes ("TEXT") if neither have any attributes."""
    node = path[-1]
    return node.name in {'L', 'TEXT'} and not node.has_attr()


def eliminate_nodes(parent: Node, deletables: Container[Node]):
    parent.result = tuple(child for child in parent.children if child not in deletables)
    merge_adjacent([parent], mergeable, 'TEXT')
    # merge_adjacent([parent], lambda ctx: ctx[-1].name in {'L', 'TEXT'}, 'TEXT')


def add_nodes(parent: Node, nodes: Iterable[Node], index):
    parent.result = tuple(parent.result[:index] + tuple(nodes) + parent.result[index:])


def ist_Bedeutung(node: Node) -> bool:
    return node.name in ('Bedeutung', 'U1Bedeutung', 'U2Bedeutung', 'U3Bedeutung',
                             'U4Bedeutung', 'U5Bedeutung')


def replace_unprintables(msg) -> str:
    L = len(msg)
    i, k = 0, 0
    compat_msg = []
    while i < L:
        if ord(msg[i]) > 127:
            compat_msg.append(msg[k:i])
            compat_msg.append('?')
            k = i + 1
        # elif msg[i] in ('«', '»'):
        #     compat_msg.append(msg[k:i])
        #     compat_msg.append("'")
        #     k = i + 1
        i += 1
    compat_msg.append(msg[k:i])
    return ''.join(compat_msg)


def add_errors_to_titleattr(node: Node, errors: List[Error]):
    if not errors:
        return
    if is_warning(max(err.code for err in errors)):
        add_class(node, 'warning')
    else:
        add_class(node, 'error')
    node.attr['title'] = ';  '.join(
        err.severity + ": " + err.message.replace('"', "'").replace('&', '+')
        for err in errors)


RX_STELLENANGABE = re.compile(r'[ .]p\.\s*\d+(?:,\s*\d+)?')
RX_ERSCHEINUNGSJAHR = re.compile(r'(?:(?:\^\{|\{\^)[1-9][0-9]?[0-9]?\})?(?:1[8-9]|20)[0-9][0-9](?=[ .\n])')
RX_ANNO = re.compile(r'a. +1[0-4][0-9][0-9]')

ZÄHLER = (
    ('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV',
     'XVI', 'XVII', 'XVIII', 'XIX', 'XX', 'XXI', 'XXII', 'XXIII', 'XIV'),
    'ABCDEFGHIKLMNOPQRSTUVWXY',
    ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18',
     '19', '20', '21', '22', '23', '24'),
    'abcdefghiklmnopqrstuvwxy',
    'αβγδεζηθικλμνξοπρστυφχψω',
    ('i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'vii', 'ix', 'x', 'xi', 'xii', 'xiii', 'xiv', 'xv',
     'xvi', 'xvii', 'xviii', 'xix', 'xx', 'xxi', 'xxii', 'xxiii', 'xiv')
)


VULGATA_ODER_SEPTUAGINTA = frozenset({"Vulg.", "LXX"})


def Vulgata_Septuaginta_Songerfall(autor1, autor2):
    return autor1 in VULGATA_ODER_SEPTUAGINTA \
           and autor2 in VULGATA_ODER_SEPTUAGINTA


def artikel_name(artikel: Node) -> str:
    """Liefert einen automatisch generierten Artikelnamen zurück. Dieser wird nur
    verwendet, wenn der Dateiname nicht verwendet werden kann, was dann der Fall ist,
    wenn in einer Datei mehrere Artikel stehen.
    """
    assert artikel.name == "Artikel"
    lemma_node = artikel.pick('LemmaBlock') or artikel.pick('AusgangsLemmata')
    if lemma_node:
        lemma_nr = ' ' + (lemma_node.pick('LemmaNr') or EMPTY_NODE).content.strip().rstrip('.')
        lemma = (lemma_node.pick('LemmaWort').content.strip() + lemma_nr).strip().replace(' ', '_')
        prefix = 'Nachtrag_' if "NachtragsPosition" in artikel else ''
        return prefix + lemma
    return ""


def wa_content(wa_node: Node) -> str:
    """Liefert den Inhalt einer wortart-Angabe ohne die Wortart-Ergänzung."""
    return ''.join(nd.content for nd in
                   wa_node.select(lambda nd: nd.name != 'wa_ergänzung' and not nd._children,
                                  include_root=True, skip_subtree="wa_ergänzung")).strip()


class MLWCompiler(Compiler):
    """Kompiliert den abstrakten Syntax-Baum eines MLW-Artikels in eine
    "reiche" Baumstruktur (semantischer MLW-Baum). Das beinhaltet:
    1. semantische Prüfungen
    2. Anreicherung mit redundanten Informationen
       (z.B. Zähler, Autor-Werk-Informationen)
    """

    def __init__(self):
        super(MLWCompiler, self).__init__()
        self.zähler_abstand = 2
        self.artikel_name = "UNBEKANNTER_ARTIKELNAME"

    def reset(self):
        super().reset()
        self.lbreaks = None
        self.lemmawort = ""
        self.mehrfachansatz = []
        self.unecht = False
        self.aktueller_autor = ''
        self.aktuelles_werk = ''
        self.aktuelle_edition = ''
        self.aktuelles_jahr = ''
        self.aktuelle_stelle = ''  # nur erster Abschnitt der aktuellen Stelle!
        self.aktuelle_stelle_folge = ''  # Folge-Abschnitte der aktuellen Stelle!
        self.op_cit = {}  # Autor -> Werkname
        self.letzter_werkname = ''
        self.ordne_verweise = OrdneVerweise(self)
        self.gr_lemma_string = ''   # jeweils letztes lemmawort mit eigener GrammatiPosition
        self.unterdrücke_warnungen: Set[ErrorCode] = set()
        self.Angaben_letzter_Einschub: Optional[Tuple[str, str, str, str, str]] = None
        self.op_cit = dict()  # Autor -> zuletzt genanntes Werk

    def prepare(self, root: RootNode) -> None:
        # assert root.stage == "AST", f"Source stage `AST` expected, `but `{root.stage}` found."
        root.stage = "xml"

    def finalize(self, result: Any) -> RootNode:
        self.tree.string_tags = set()  # {WHITESPACE_PTYPE, 'L'}
        self.ordne_verweise.prüfe_verweise(self.tree)
        for path in self.tree.select_path('BelegText'):
            merge_adjacent(
                path,
                lambda p: p[-1].name == TEXT and not p[-1].has_attr())
        if isinstance(result, RootNode):
            return cast(RootNode, result)
        else:
            raise AssertionError(f'Compilation-result should be of type RootNode, not {type(result)}')


    def on_Artikel(self, node):
        if 'stadium' in node.attr:
            raise AssertionError('Übersetzer erwartet einen abstrakten Syntaxbaum, '
                'nicht das Stadium "{}"!'.format(node.attr['stadium']))
        if self.path[0].name != 'Artikel':
            self.artikel_name = artikel_name(node) or self.artikel_name
        node = self.fallback_compiler(node)
        node.attr['stadium'] = "semantischer Baum"
        node.attr['generator'] = MLWVersion.VERSION
        node.attr['name'] = self.artikel_name
        self.path[0].stage = 'xml'
        return node

    def formatiere_Verweisartikel(self, node, bias=0):
        children = node.children
        i = bias
        for k, child in enumerate(children):
            if child.name == 'L':  #  and child.get_attr('spatium', '') in ('4', '8'):
                i += 1
                if k >= 1:
                    if children[k - 1].name in ('TRENNE', 'Ueberschrift'):
                        if i % 2 != 0:
                            i += 1
                    elif children[k - 1].name == 'VERBINDE':
                        if i % 2 == 0:
                            i -= 1
                    elif children[k - 1].pick('Beleg'):
                        if i % 2 != 0:
                            i += 1
                if i % 2 == 0 and not (k >= 1 and children[k - 1].name == "PRAETER"):
                    child.name = 'P'
                    child.result = '\n'
                    if child.has_attr('spatium'):  del child.attr['spatium']

        # remove markers
        node.result = tuple(nd for nd in node.children
                            if nd.name not in ('VERBINDE', 'TRENNE'))

    def on_VerweisArtikel(self, node):
        node = self.fallback_compiler(node)
        # i = 0
        # for child in node.children:
        #     # Verwandle jedes zweite große spatium in einen Zeilenwechsel
        #     # (Bei Verweisartikeln immer nur max zwei Verweise in einer Zeile)
        #     if child.name ==  'L' and child.get_attr('spatium', '') == '8':
        #         i += 1
        #         if i % 2 == 0:
        #             child.name = 'P'
        #             child.result = '\n'
        self.formatiere_Verweisartikel(node, bias=0)
        return node

    def on_PraeterBlock(self, node):
        node = self.fallback_compiler(node)
        i = node.index('PRAETER')
        self.formatiere_Verweisartikel(node, bias=0)
        return node

    def on_LemmaNr(self, node):
        node = self.fallback_compiler(node)
        node.attr['nr'] = int(node.content.strip('.'))
        return node

    def on_Verweis(self, node):
        moved_ws = ''
        try:
            url = first(node['URL'])
            externer_verweis = 'pfad' in url or 'protokoll' in url
            if externer_verweis:
                protokoll = url.get('protokoll', EMPTY_NODE).content
                ziel = url.get('ziel', EMPTY_NODE).content
                if not protokoll:
                    pfad = ''.join(pf.content for pf in as_list(url['pfad']))
                    artikel_name = os.path.basename(pfad.rstrip('/'))
                    if not pfad.startswith('/'):
                        # pfad = '../' + pfad
                        pfad = ''
                    urlstr = protokoll + pfad + artikel_name
                    if ziel:
                        urlstr += '.xml#' + ziel
                    else:
                        urlstr += '.xml'
                else:
                    artikel_name = (as_list(url['pfad']) + [Node('', '')])[0].content.strip('/')
                    urlstr = url.content
                    # vorhergehender Leerraum wird in den Inhalt des Link-Tags
                    # hinein verschoben, damit er in der Druckausgabe, wo
                    # Links auf Internetquellen unterdrückt werden, nicht als
                    # störende Lücke sichtbar wird.
                    ctx = prev_path(self.path)
                    if ctx and ctx[-1].name == 'L':
                        self.finalizers.append((eliminate_nodes, (ctx[-2], (ctx[-1],))))
                        moved_ws = ' '
            else:
                Z = url['ziel']
                ziel = Z.content
                if ziel in ('-', ''):
                    artikel_name = ''
                    url.result = ''
                    urlstr = '-'
                else:
                    artikel_name = self.artikel_name
                    Z.result = "#" + ziel
                    urlstr = ASCII(url.content)
        except KeyError:
            url = EMPTY_NODE
            urlstr = VERWEISZIEL_FEHLT
            artikel_name = VERWEISZIEL_FEHLT
            ziel = ""
            externer_verweis = False

        try:
            alias = first(node['alias'])
            if alias.children:
                if alias.children[0].name == 'TEXT':
                    alias.children[0].result = moved_ws + alias.children[0].result
                    text = alias.children
                else:
                    text = (Node('TEXT', moved_ws),) + alias.children
            else:
                text = moved_ws + alias.content
            alias_attr = ''
        except KeyError:
            text = ''
            alias_attr = moved_ws + ('⇒' + artikel_name) if externer_verweis else '⇒'

        link = Node('a', text)
        link.name = 'a'
        link.attr['href'] = urlstr.replace('&', '%38')
        if alias_attr:
            link.attr['alias'] = alias_attr
        if 'protokoll' not in url and 'ziel' in url:
            link.attr['globaleReferenz'] = absolutes_verweis_ziel(artikel_name, ziel)
        link.with_pos(node.pos)
        # if urlstr == VERWEISZIEL_FEHLT:
        #     link.attr['title'] = "Verweisziel fehlt noch!"
        #     self.tree.new_error(link, "Verweisziel fehlt!", HINWEIS_VERWEISZIEL_LEER)
        try:
            self.ordne_verweise.melde_verweis(node, link, self.path[-2])
        except IndexError:
            # Kann in einem Einheitentest vorkommen, wobei node die root-node ist
            # Die RootNode darf nicht ersetzt werden, daher das Folgende
            node.result = link.result
            node.name = link.name
            node.attr = link.attr
            link = node
        return link


    def ergänze_Zähler(self, node, tiefe):
        def ändere_spatium(spatium):
            if spatium.name == 'L' and spatium.get_attr('spatium', '') == '4':
                spatium.attr['spatium'] = '3+1'
        i = 0
        bedeutungen = tuple(node.select_children(ist_Bedeutung))
        if len(bedeutungen) > 1:  # len(node.children) > 1:
            for nd in bedeutungen:
                try:
                    zähler = Node("Zähler", ZÄHLER[tiefe + self.zähler_abstand][i])
                    zähler.with_pos(nd.pos)
                except IndexError:
                    zähler = Node("Zähler", '😵').with_pos(nd.pos)
                    self.tree.new_error(zähler,
                        "Zu große Zahl an Unterbedeutungen (bisher nur %i erlaubt)"
                        % len(ZÄHLER[tiefe + self.zähler_abstand]))
                i += 1
                nd.result = (zähler, ) + nd.children
        elif len(bedeutungen) == 1:
            i = node.index(bedeutungen[0])
            if i > 0:
                # Fall 1: node ist eine Bedeutung oder Unter(...unter...)Bedeutung
                ändere_spatium(node[-1])
            # else:
            #     # Fall 2: node ist eine Bedeutungsposition
            #     parent = self.path[-2]
            #     i = parent.index[node]
            #     if i > 0:
            #         ändere_spatium(parent[i - 1])


    def nur_eine_Unterbedeutung(self, node):
        """
        Gibt wahr zurück, wenn der Knoten genau einen
        (Unter(Unter...))Bedeutungskonten hat, oder falsch, falls der Knoten
        nur Belege oder mehr als eine Unterbedeutung hat.
        """
        i = 0
        for child in node.children:
            if ist_Bedeutung(child):
                i += 1
                if i > 1:
                    break
        return i == 1

    def prüfe_Anzahl_Unterbedeutungen(self, node):
        if self.nur_eine_Unterbedeutung(node):
            ub_node = node.pick(ist_Bedeutung)
            # Die folgende if-Klausel dient dazu, Fehlermeldungen in Fällen zu unterdrücken,
            # wo der Fehler nur ein Folgefehler einer nicht erkannten Bedeutung ist
            if all(nd.content.find('BEDEUTUNG') < 0 for nd in node.select(ZOMBIE_TAG)):
                self.tree.new_error(ub_node,
                                    "Einzelne Unterbedeutungen sind nicht möglich bzw. sinnvoll.",
                                    FEHLER_NUR_EINE_UNTERBEDEUTUNG)

    def usu_Sonderfall(self, node):
        ebene = node.name  # "Besonderheit", "Kategorie" oder "Unterkategorie"
        beschreibung = node['Beschreibung']
        if beschreibung.content[0:5] == '[usu]':
            beschreibung.result = 'usu' + beschreibung.content[5:]
            ctx = pick_path(self.path[-2:], ebene,
                            include_root=False, reverse=True)
            if not ctx or \
                    not ctx[-1].get('Beschreibung', EMPTY_NODE).content.find('usu') >= 0:
                self.tree.new_error(
                    beschreibung, f'Die eingeklammerte Form "[usu]..." muss '
                                  f'unmittelbar auf ein anderes "usu" oder "[usu]" auf derselben '
                                  f'Ebene folgen.', FEHLER_FOLGE_USU_OHNE_VORGÄNGER)
            node.attr['usu'] = '[usu]'
        return node

    def on_Besonderheit(self, node):
        self.fallback_compiler(node)
        node = self.usu_Sonderfall(node)
        return node


    def on_Kategorie(self, node):
        self.aktueller_autor = ''
        self.aktuelles_werk = ''
        self.aktuelles_jahr = ''
        self.aktuelle_stelle = ''
        self.aktuelle_stelle_folge = ''
        self.fallback_compiler(node)
        node = self.usu_Sonderfall(node)
        return node

    def on_Unterkategorie(self, node):
        self.fallback_compiler(node)
        node = self.usu_Sonderfall(node)
        return node

    def on_Variante(self, node):
        node = self.fallback_compiler(node)
        beschreibung = node['Beschreibung']
        # beschr_text = beschreibung.content.rstrip()
        beschr_text = ''.join(nd.content for nd in
                              beschreibung.select(
                                  lambda node: not node.children \
                                               and node.name not in {'Zusatz', 'FesterZusatz'},
                                  include_root=True,
                                  skip_subtree=lambda nd: nd.name == "Einschub")
                              ).rstrip()
        if beschr_text.endswith('.') or beschr_text.endswith(' scribitur') \
                or (beschr_text.split(' ')[0].endswith('.')
                    and not beschr_text.split(' ')[0].rstrip('.').isnumeric()):
            if pick_from_path(self.path, 'Kategorie', reverse=True):
                node.name = 'Unterkategorie'
            else:
                node.name = 'Kategorie'
        node = self.usu_Sonderfall(node)
        return node

    def on_EtymologieBeispiel(self, node):
        node = self.fallback_compiler(node)
        if node.children:
            first = node.children[0]
            if first.name == "TEXT":
                content = first.content.strip()
                if content.endswith('.') and ' ' not in content \
                        and not content in EtymologieSprachListe:
                    self.tree.new_error(node, '"%s" ist keine bekannte Sprachangabe!' % content,
                                        WARNUNG_UNBEKANNTE_ETYMOLOGIE_SPRACHE)
        return node

    def on_BedeutungsPosition(self, node):
        if len(self.path) < 2:
            # tritt nur bei Testcode auf, der lediglich einen Ausschnitt des Baumes betrachtet
            return self.fallback_compiler(node)
        tiefe = int(unter_artikel(self.path).attr.setdefault('unterbedeutungstiefe', '0'))
        if self.nur_eine_Unterbedeutung(node):
            self.zähler_abstand = -1 if tiefe > 3 else 1
            parent = self.path[-2]
            spatium = parent[parent.index(node) - 1]
            if spatium.name == 'L' and spatium.get_attr('spatium', '') == '4':
                spatium.attr['spatium'] = '3+1'
        else:
            self.zähler_abstand = 0 if tiefe >= 3 else 2
            self.ergänze_Zähler(node, 0)
        # self.aktueller_autor = ''
        # self.aktuelles_werk = ''
        self.aktuelle_stelle = ''
        self.aktuelles_jahr = ''
        self.aktuelle_stelle_folge = ''
        return self.fallback_compiler(node)

    def on_Bedeutungsangabe(self, node):
        # for zusatz in node.select('Zusatz'):
        #     if zusatz.content[:1] == '(' or zusatz.content[-1:] == ')':
        #         self.tree.new_error(
        #             node, 'Zusätze zu Bedeutungsangaben müssen nicht in runde Klammern '
        #             'eingeschlossen werden. Die Klammern werden später automatisch '
        #             'hinzugefügt', WARNUNG_ÜBERFLÜSSIGE_KLAMMERN)
        node = self.fallback_compiler(node)
        self.Angaben_letzter_Einschub = None
        return node

    def warne_vor_expliziter_Gradestellung(self, node):
        idx = node.indices('Kursiv')
        for i in idx:
            self.tree.new_error(node[i], 'Wörtlich vorkommenden Ausdrücke oder Belege sollten '
               'besser in Anführungsstriche "..." als explizit grade {/...} gesetzt werden!',
               WARNUNG_NICHT_GEKENNZEICHNETER_BELEGTEXT)

    def on_LateinischeBedeutung(self, node):
        self.fallback_compiler(node)
        self.warne_vor_expliziter_Gradestellung(node)
        return node

    def on_DeutscheBedeutung(self, node):
        self.fallback_compiler(node)
        self.warne_vor_expliziter_Gradestellung(node)
        return node

    def on_Klassifikation(self, node):
        self.fallback_compiler(node)
        self.warne_vor_expliziter_Gradestellung(node)
        return node

    def on_LateinischesWort(self, node):
        self.fallback_compiler(node)
        return node

    def verschiebe_letzten_Zusatz(self, node):
        """Wenn am Ende einer Folge von Belegen ein Zusatz steht, der nicht durch das Wort
        "ibid." eingeleitet wird, so bezieht sich der Zusatz auf alle Belege und sollte daher
        vom Ende des letzten Beleges hinter alle Belege innerhalb der Bedeutungsposition
        verschoben werden."""
        assert node.name.find('Bedeutung') >= 0 or node.name.find('Anhänger') >= 0
        if node.pick(lambda nd: nd.name.endswith('Bedeutung')) is not None:
            # Falls tiefere Unterbedeutungsebenen im Bedeutungsblock enthalten sind, kehre zurück
            return
        try:
            letztes_Blatt_path = next(node.select_path_if(
                lambda ctx: not ctx[-1].children or ctx[-1].name.endswith('Zusatz'), reverse=True))
            letztes_Blatt = letztes_Blatt_path[-1]
            # Verschiebe, wenn Belegfolge mit einem Zusatz endet, der nicht innerhalb
            # eines, i.e. des letzten BelegTexts steht
            if letztes_Blatt.name.endswith("Zusatz") and \
                    not any(nd.name == 'BelegText' for nd in letztes_Blatt_path):
                eltern_knoten = next(node.select_if(
                    lambda nd: nd.children and nd.children[-1] == letztes_Blatt, reverse=True))

                # sammele alle Zusätze am Schluss des Blocks ein:
                anhang = [letztes_Blatt]
                for i in range(len(eltern_knoten.children) - 2, -1, -1):
                    knoten = eltern_knoten.children[i]
                    tag_name = eltern_knoten.children[i].name
                    if tag_name.endswith('Zusatz') or tag_name == 'L':
                        anhang.append(knoten)
                    else:
                        while anhang and anhang[-1].name == 'L':
                            anhang.pop()
                        anhang.reverse()
                        break
                else:
                    anhang.reverse()

                if all(knoten.content.find('ibid.') < 0 for knoten in anhang):
                    it = node.select('Beleg')
                    _ = next(it)
                    _ = next(it)  # es muss mindestens zwei Belege geben
                    if eltern_knoten.name.find('Bedeutung') < 0:
                        for einschub in node.select('Einschub', reverse=True):
                            if einschub.pick(lambda nd: nd == letztes_Blatt):
                                # Verhindere das Verschieben von Zusätzen aus Einschüben heraus!
                                raise StopIteration

                        eltern_knoten.result = eltern_knoten.result[:-len(anhang)]
                        node.result = node.result + tuple(anhang)
        except StopIteration:
            pass

    def initialisiere_Bedeutung(self, node):
        # self.aktueller_autor = ''
        # self.aktuelles_werk = ''
        self.aktuelle_stelle = ''
        self.aktuelles_jahr = ''
        self.aktuelle_stelle_folge = ''
        self.verschiebe_letzten_Zusatz(node)
        return self.fallback_compiler(node)

    def on_Bedeutung(self, node):
        self.prüfe_Anzahl_Unterbedeutungen(node)
        self.ergänze_Zähler(node, 1)
        return self.initialisiere_Bedeutung(node)

    def on_U1Bedeutung(self, node):
        self.prüfe_Anzahl_Unterbedeutungen(node)
        self.ergänze_Zähler(node, 2)
        return self.initialisiere_Bedeutung(node)

    def on_U2Bedeutung(self, node):
        self.prüfe_Anzahl_Unterbedeutungen(node)
        self.ergänze_Zähler(node, 3)
        return self.initialisiere_Bedeutung(node)

    def on_U3Bedeutung(self, node):
        self.prüfe_Anzahl_Unterbedeutungen(node)
        self.ergänze_Zähler(node, 4)
        return self.initialisiere_Bedeutung(node)

    def on_U4Bedeutung(self, node):
        self.prüfe_Anzahl_Unterbedeutungen(node)
        self.ergänze_Zähler(node, 5)
        return self.initialisiere_Bedeutung(node)

    # def on_U5Bedeutung(self, node):
    #     self.ergänze_Zähler(node, 6)
    #     node = self.intialisiere_Bedeutung(node)
    #     return node

    def on_Anhänger(self, node):
        return self.initialisiere_Bedeutung(node)

    def lies_lemmawort(self, node):
        if 'unecht' in node:
            self.unecht = True
            del node['unecht']
        self.fallback_compiler(node)
        lemmaworte = [nd.content for nd in node.select('LemmaWort', False)]
        if lemmaworte:
            self.lemmawort = lemmaworte[0]   # Inhalt des ersten Lemmawortes
            self.mehrfachansatz = lemmaworte[1:]
        else:
            self.tree.new_error(node, "Lemmawort nicht gefunden. "
                                      "Vermutlich Folgefehler eines anderen Fehlers.")
        return node

    def on_Lemma(self, node):
        self.fallback_compiler(node)
        lemmawort = node.pick('LemmaWort')
        assert 'id' not in node.attr
        LW = lemmawort.content.upper()
        # if LW in self.ordne_verweise.anker:
        #     raise AssertionError('Ziel {L} ist bereits vergeben in Knoten: {K}'
        #                          .format(L=LW, K=node.as_sxpr()))
        while LW in self.ordne_verweise.anker:
            # Disambiguierung beim Mehrfachansatz
            LW = "VEL_" + LW
        node.attr['id'] = LW.replace('-', '_')  # Ersetzung für elliptische Verweilemmata, z.B. "edif- vide aedif-"
        if self.unecht:
            add_token_to_attr(node, 'unecht', 'hinweise')
            ua = pick_from_path(self.path, 'UnterArtikel')
            if ua is None:  ua = pick_from_path(self.path, 'Artikel')
            if ua:  add_token_to_attr(ua, 'unecht', 'hinweise')
        self.ordne_verweise.melde_anker(LW, node, node)
        return node

    def on_LemmaPosition(self, node):
        return self.lies_lemmawort(node)

    def on_SubLemmaPosition(self, node):
        node = self.lies_lemmawort(node)
        if not node.pick('GrammatikPosition'):
            lemma_block = node.pick('LemmaBlock')
            lemma_block.attr['grammatik_wie_bei'] = self.gr_lemma_string
        return node

    # def on_LemmaVariante(self, node):
    #     self.fallback_compiler(node)
    #     # assert not node.children, node.as_sxpr()
    #     # node.xml_attr['verdichtung'] = lemma_verdichtung(self.lemmawort, node.result)
    #     return node

    def on_LemmaBlock(self, node):
        self.fallback_compiler(node)
        if node.pick('GrammatikPosition'):
            try:
                self.gr_lemma_string = node.pick('LemmaWort').content
            except AttributeError:
                pass  # Fehler wird an anderer Stelle bereits moniert.
        elif len(self.path) >= 2:
            parent = self.path[-2]
            passed = False
            for child in parent.children:
                if passed:
                    if child.children and 'GrammatikPosition' in child:
                        assert child.name == 'LemmaBlock'
                        self.gr_lemma_string = child.pick('LemmaWort').content
                        node.attr['grammatik_wie_bei'] = self.gr_lemma_string
                        break
                elif child == node:
                    passed = True
        try:
            if self.path[-2].pick('LemmaBlock') != node:
                # except for the very first lemma block, move etymology-block to the front
                lemma_pos = node.index('Lemma')
                etym_pos = node.index('EtymologiePosition')
                children = list(node.children)
                etym = children[etym_pos]
                del children[etym_pos]
                children.insert(lemma_pos, etym)
                node.result = tuple(children)
        except (ValueError, IndexError):
            pass
        return node

    def verdichte_und_normalisiere(self, node, verdichtung, expansion):
        self.fallback_compiler(node)
        assert not node.children
        inhalt = node.content
        node.attr['verdichtung'] = verdichtung.get(inhalt, inhalt).replace('<', '&lt;')
        node.result = expansion.get(inhalt, inhalt)
        return node

    def on_flexion(self, node):
        self.fallback_compiler(node)
        return node

    def on_casus(self, node):
        return self.verdichte_und_normalisiere(node, CasusVerdichtung, CasusExpansion)

    def on_numerus(self, node):
        return self.verdichte_und_normalisiere(node, NumerusVerdichtung, NumerusExpansion)

    def on_genus(self, node):
        return self.verdichte_und_normalisiere(node, GenusVerdichtung, GenusExpansion)

    def on_wortart(self, node):
        """
        1. Überführt die Wortart-Klasse vom entsprechenden Kind-Knoten in ein
           Attribut.
        2. Ergänzt ein Attribut mit der verdichteten Form
        3. Schreibt die kanonische, unabgekürzte Bezeichnung in den Inhalt des Knotens
        """
        self.fallback_compiler(node)
        if node.children and node.children[-1].name.endswith('klasse'):
            klasse = node.children[-1]
            node.attr['klasse'] = klasse.content
            assert len(node.children) == 3 and node.children[1].name == WSP, node.as_sxpr()
            node.result = node.children[0].result
            assert not node.children
        wa = wa_content(node)
        node.attr['verdichtung'] = WortartVerdichtung.get(wa, wa).replace('<', '&lt;')
        if node.children:
            typ = node[0]
            typ.result = WortartExpansion.get(typ.content, typ.content)
        else:
            node.result = WortartExpansion.get(node.result, node.result)
        return node

    def on_vel(self, node):
        node = self.fallback_compiler(node)
        for nd in node.select(LEAF_NODE, include_root=True):
            nd.result = nd.result.replace('VEL', 'vel')
        return node

    def on_Beleg(self, node):
        self.fallback_compiler(node)
        if any(nd.name == "BedeutungsPosition" for nd in self.path) \
                and not 'Einschub' in (nd.name for nd in self.path):
            # Nur innerhalb der BelegPosition, aber nicht im Artikelkopf
            # erfordert jeder neuer Beleg eine erneute Autor- und Werkangabe.
            # Innerhalb des Belegs dürfen dann aber mehrere Stellenangaben
            # folgen, ohne dass Autor- und Werk erneut angegeben werden müssen.
            # self.aktueller_autor = ''
            self.aktuelles_werk = ''
            self.aktuelle_stelle = ''
            self.aktuelles_jahr = ''
            self.aktuelle_stelle_folge = ''
            self.Angaben_letzter_Einschub = None
        return node

    def on_BelegStelle(self, node):
        node = self.fallback_compiler(node)
        try:
            i = node.index('Werk')
            if not node[i].content:
                # lösche leere Werkangaben
                del node[i]
        except ValueError:
            pass
        # self.Angaben_letzter_Einschub = None
        return node

    def on_BelegText(self, node):
        self.fallback_compiler(node)
        for ctx in node.select_path('TEXT'):
            text = ctx[-1].content
            if text[-1:] == '(':
                succ = next_path(ctx)
                has_number = False
                nctx = succ
                while nctx:  #  and nctx[-1].name != 'BelegLemma':
                    if nctx[-1].content.find(')') >= 0:
                        nctx = None
                    else:
                        has_number = has_number \
                                or (nctx[-1].name == 'TEXT'
                                    and re.findall(r'\d', nctx[-1].content))
                        nctx = next_path(nctx)
                if has_number:
                    self.tree.new_error(succ[-1],
                                        "Wahrscheinlich sollte hier ein echter Einschub "
                                        "(( ... )) stehen!",
                                        WARNUNG_EINSCHUB_ERWARTET)
        bt = node.indices('BelegLemma')
        for i in range(len(bt) - 1):
            a, b = bt[i], bt[i + 1]
            if not content_of(node.children[a + 1:b]).strip():
                moegliches_lemma = content_of(node.children[a:b + 1])
                self.tree.new_error(node, f'Falls es sich bei {moegliches_lemma} um ein einzelnes'
                                    ' mehrgliedriges Lemma handelt, sollte es zusammenhängend'
                                    f' notiert werden: #{{{moegliches_lemma}}}',
                                    WARNUNG_MEHRGLIEDRIGES_LEMMA)
        return node

    def on_Lemmawort(self, node):  # Nicht zu verwechseln mit LemmaWort mit großem "W"!
        def Warnung_enklitischer_Partikel(urlemma, partikel: str) -> bool:
            try:
                N = len(partikel)
                if urlemma.endswith(partikel) \
                        and not self.lemmawort.endswith(partikel) \
                        and urlemma[-N - 1] not in ('|', '.', '_'):
                    self.tree.new_error(
                        node, 'Falls es sich bei der Endung um ein enklitisches "%s" handelt, '
                        % partikel + 'sollte es durch einen vorangestellten Unterstrich "_" ' +
                        'gekennzeichnet werden: "%s_%s" ' % (urlemma[:-N], urlemma[-N:]),
                        WARNUNG_ENKLITISCHER_PARTIKEL)
                    return True
            except IndexError:
                return False

        urlemma = node.content
        lemma = urlemma
        anhaengsel = ''
        a = urlemma.rfind('_')
        if a >= 0:
            anhaengsel = urlemma[a + 1:]
            lemma = urlemma[:a]
            node.attr['anhaengsel'] = anhaengsel
        else:
            for partikel in ('que', 've'):
                if Warnung_enklitischer_Partikel(urlemma, partikel):
                    break
        try:
            verdichtung = lemma_verdichtung(self.lemmawort, lemma)
        except Verdichtungsfehler as err:
            fehlerliste = [err.fehler]
            for ansatz in self.mehrfachansatz:
                try:
                    verdichtung = lemma_verdichtung(ansatz, lemma)
                    break
                except Verdichtungsfehler as err:
                    fehlerliste.append(err.fehler)
            else:
                for fehler in fehlerliste:
                    self.tree.new_error(node, fehler)
                verdichtung = ''
        lemma = lemma.replace('|', '').replace('.', '')
        node.result = lemma + anhaengsel
        if verdichtung:
            node.attr['verdichtung'] = verdichtung.replace('<', '&lt;')
        self.ordne_verweise.melde_beleglemma(node, ASCII(lemma + anhaengsel))
        if not node.has_attr('lemma'):
            node.attr['lemma'] = lemma

        # if node.has_attr('tmesi') and not node.attr['tmesi']:
        #     beleg_text = pick_from_path(self.path, 'BelegText', reverse=True)
        #     if beleg_text:
        #         lw = list(beleg_text.select({'Lemmawort', 'BelegLemma'}, skip_subtree='Einschub'))
        #         i = lw.index(node)
        #         a, b = i, i
        #         while a > 0 and lw[a].has_attr('tmesi'):
        #             a -= 1
        #         while b < len(lw) and lw[b].has_attr('tmesi'):
        #             b += 1
        #         tmesi = (''.join(nd.content for nd in lw[a:b])).replace('--', '-')
        #         volles_lemma = tmesi.replace('-', '')
        #         for nd in lw[a:b]:
        #             nd.attr['tmesi'] = tmesi
        #             nd.attr['lemma'] = volles_lemma

        # if node.has_attr('tmesi') and not node.attr['tmesi']:

        if not node.has_attr('tmesi') and node.content[0:1] == '-' or node.content[-1:] == '-':
            beleg_text = pick_from_path(self.path, 'BelegText', reverse=True)
            if beleg_text:
                lw = list(beleg_text.select({'Lemmawort', 'BelegLemma'}, skip_subtree='Einschub'))
                i = lw.index(node)
                a, b = i, i
                while a > 0 and lw[a].content[0:1] == '-' and lw[a - 1].content[-1:] == '-':
                    a -= 1
                while b < len(lw) - 1 and lw[b].content[-1:] == '-' \
                        and lw[b + 1].content[0:1] == '-':
                    b += 1
                if lw[a].content[0:1] == '-':
                    if a == 0:
                        self.tree.new_error(lw[a], f'Tmesis-Fehler: Kein Bindestrich am Anfang '
                                            f'von "{lw[a]}" erwartet, da kein vorhergehender '
                                            f'Lemmabestanteil im Belegtext gefunden.',
                                            FEHLER_UNKLARE_TMESIS)
                    else:
                        self.tree.new_error(lw[a], f'Tmesis-Fehler: Entweder Bindestrich am Ende '
                                                   f'von "{lw[a]}" oder kein Bindestrich am Anfang des '
                                                   f'folgenden Lemmawortes "{lw[a + 1]}" erwartet',
                                            FEHLER_UNKLARE_TMESIS)
                if lw[b].content[-1:] == '-':
                    if b == len(lw):
                        self.tree.new_error(lw[b], f'Tmesis-Fehler: Kein Bindestrich am Ende '
                                            f'von "{lw[b]}" erwartet, da kein '
                                            f'nachfolgender Lemmabestanteil im Belegtext gefunden',
                                            FEHLER_UNKLARE_TMESIS)
                    else:
                        self.tree.new_error(lw[b], f'Tmesis-Fehler: Entweder Bindestrich am Anfang '
                                            f'von "{lw[b]}" oder kein Bindestrich am Ende des '
                                            f'vorhergehenden Lemmawortes "{lw[b - 1]}" erwartet',
                                            FEHLER_UNKLARE_TMESIS)
                tmesi = (''.join(nd.content for nd in lw[a:b + 1])).replace('--', '-')
                volles_lemma = tmesi.replace('-', '')
                for nd in lw[a:b + 1]:
                    nd.attr['tmesi'] = tmesi
                    nd.attr['lemma'] = volles_lemma

        return node

    def on_Abkuerzung(self, node):
        content = node.content
        i = content.find('|')
        dots = content.count('.')
        if i <= 0 or i >= len(content)-1 or content.count('|') > 1 or dots > 1 \
                or (dots == 1 and content[i+1] != '.'):
            self.tree.new_error(node,
                "Abkürzungen dürfen nur die Markierungen |. oder | enthalten und, anders "
                "als mit # eingeleitete Lemmaworte, diese auch nur einmal!",
                FEHLER_ABKUERZUNG_FEHLERHAFT)
        node.attr['verdichtung'] = content[:i].replace('<', '&lt;') + '.'
        node.result = content.replace('|', '').replace('.', '')
        return node

    def on_BelegLemma(self, node):
        return self.on_Lemmawort(node)


    def finde_letzte_verankerbare_Einheit(self, node, parent):
        """Findet das letzte BelegLemma für den Anker-Knoten `node`,
        das noch keine id hat. Gibt `parent` zurück, falls kein entsprechendes
        BelegLemma gefunden wurde."""
        einheiten = []
        k = parent.index(node)
        for sibling in reversed(parent.children[0:k]):
            if sibling.name in {"BelegLemma", "Autor", "Werk", "Autorangabe", "Werktitel",
                                    "Nachschlagewerk", "Werkname", "Quelle", "Junktur"}\
                    or sibling.name == 'TEXT' and sibling.content.strip():
                einheiten.append(sibling)
            else:
                descendant = sibling.pick({"BelegLemma", "Autor", "Werk", "Autorangabe", "Werktitel",
                                    "Nachschlagewerk", "Werkname", "Quelle", "Junktur"})
                if descendant:  einheiten.append(sibling)
        # for sibling in parent.children[:k]:    # [k+1:]
        #         beleg_lemmata.append(sibling)
        if not einheiten:
            # falls keine Beleg-Lemma vorhanden wird der Elternknoten des Ankers das Sprungziel
            einheiten.append(parent)
        for einheit in einheiten:
            if not (einheit.has_attr('id')
                    and einheit.attr['id'] not in self.ordne_verweise.pseudo_anker):
                return einheit
        return None

    def on_Anker(self, node):
        """Anker beziehen sich immer auf das vorhergehende Wort.
        """
        assert self.path[-1] == node
        try:
            parent = self.path[-2]
        except IndexError:
            # Kann nur in einem Einheiten-Test vorkommen
            return node

        ziel = self.finde_letzte_verankerbare_Einheit(node, parent)

        # Suche das vorhergehende Wort, sofern nicht das BelegLemma dem
        # Anker unmittelbar bzw. nur durch Leerzeichen oder Satzzeichen
        # getrennt vorhergeht.
        ni = parent.index(node)
        try:
            zi = parent.index(ziel)
        except (ValueError, TypeError):
            zi = -1
        between = parent.children[zi + 1: ni]
        if not between and ziel.name == 'TEXT':  between = (ziel,)

        reconstruct_task = ()
        if not any(nd.name in ('AutorWerk', 'Sekundärliteratur') for nd in self.path[-3:]):
            # knüpfe Anker an das vorhergehende Wort im Text
            s = ' '.join(nd.content for nd in between if nd.pick('BelegLemma') is None)
            last_word = (re.findall(r'\w+', s)[-1:] + [''])[0]
            if last_word:
                ni -= 1
                ziel = parent.children[ni]
                while not last_word in ziel.content:
                    ni -= 1
                    ziel = parent.children[ni]
                if len(re.findall(r'\w+', ziel.content)) > 1:
                    i = ziel.content.rfind(last_word)
                    # TODO: What about the case where `ziel` is not a leaf-node???
                    remainder = ziel.content[i:] # .rstrip()
                    ziel.result = ziel.content[:i]
                    ziel = Node(ziel.name, remainder).with_pos(ziel.pos + i)
                    # parent.result = parent.children[:ni + 1] + (ziel,) + parent.children[ni + 1:]
                    reconstruct_task = (add_nodes, (parent, (ziel,), ni + 1))
                    #  add_nodes(parent, (ziel,), ni + 1)
            elif s.strip(' ') == '':
                pass
                # eliminate superfluous blanks
                # parent.result = parent.children[:zi + 1] + parent.children[ni:]

        def doppelte_Anker(parent: node) -> List:
            anker_indices = parent.indices('Anker')
            doppelte = set()
            if len(anker_indices) > 1:
                a = anker_indices[0]
                for b in anker_indices[1:]:
                    dazwischen = ''.join(nd.content for nd in parent[a + 1: b]).strip()
                    if not dazwischen:
                        doppelte.add(parent[a])
                        doppelte.add(parent[b])
            return list(doppelte)

        doppelte = doppelte_Anker(parent)
        if doppelte:
            anker_liste = ", ".join(nd.content for nd in doppelte)
            for nd in doppelte:
                if nd.has_attr('fehler'):  # Fehler wurde schon gemeldet
                    return node
                nd.attr['fehler'] = 'doppelte Anker: ' + anker_liste
            error = Error(f'Mehrere Anker für dasselbe Ziel sind nicht möglich: '
                          f'{anker_liste}',
                          node.pos, FEHLER_DOPPELTER_ANKER)
            self.tree.add_error(node, error)
            return node
        if ziel is None:
            error = Error('Alle in Frage kommenden Ziele haben bereits einen Anker! '
                          '"%s" kann nicht zugewiesen werden!' % node.content,
                          node.pos, FEHLER_DOPPELTER_ANKER)
            self.tree.add_error(node, error)
            return node
        else:
            anker_name = node.content
            self.ordne_verweise.melde_anker(anker_name, node, ziel)
            ziel.attr['id'] = anker_name
            if ziel.has_attr('anker'):  del ziel.attr['anker']  # Anker wurde nicht automatisch generiert!
            ziel.attr['globaleId'] = absolutes_verweis_ziel(self.artikel_name, anker_name)
            # if node != ziel and anker_name:
            #     node.attr['name'] = anker_name
            #     node.attr['globaleId'] = absolutes_verweis_ziel(self.artikel_name, anker_name)
        zombies = tuple(node.select(ZOMBIE_TAG)) if node.children else ()
        # node.result = ''

        # beseitige überflüssige Spatien: Wenn
        # a) vor und nach dem Anker ein Spatium steht, oder
        # b) vor dem Anker ein Spatium und danach ein Satzzeichen steht,
        # dann beseitige das Spatium vor dem Anker
        deletables = (node,)
        i = parent.index(node)
        pred = next(parent[i - 1].select_if(lambda nd: not nd.children, include_root=True, reverse=True))
        ancestor = parent
        k = -3
        while abs(k) <= len(self.path) and i + 1 >= len(ancestor.children):
            i = self.path[k].index(ancestor)
            ancestor = self.path[k]
            k -= 1
        try:
            succ = ancestor[i + 1]
            if succ.name not in ('ZITAT'):
                tailchr = succ.content[:1]
                if tailchr and (tailchr in " ;:,!?'´‘’`)]"
                                or (tailchr == "." and succ.content[:2] != "..")):
                    if pred.name == 'L' or (pred.name == 'TEXT' and re.fullmatch(r' *', pred.content)):
                        deletables = (pred, node)
                    elif not reconstruct_task and pred.result.endswith(' '):
                        # this has already been done, if reconstruct_task exists, see above
                        pred.result = pred.result.rstrip()
        except IndexError:
            pass
        parent.result = tuple(nd for nd in parent.result if nd not in deletables)
        if zombies:
            self.finalizers.append((add_nodes, (parent, zombies, i + 1 - len(deletables))))
        if reconstruct_task:
            self.finalizers.append(reconstruct_task)
        return node

    def on_Text(self, node):
        # node.parser = MockParser('Text')
        node.name = 'Text'
        return node

    def on_Einschub(self, node):
        # Einschübe können wiederum Quellenangaben enthalten,
        # die aber aktueller_autor und aktuelles_werk nicht
        # überschreiben sollten, da diese variablen, später
        # von der übergeordneten Quellenangabe noch ausgelesen
        # werden.
        stapel = (self.aktueller_autor, self.aktuelles_werk, self.aktuelle_edition,
                  self.aktuelle_stelle, self.aktuelles_jahr, self.aktuelle_stelle_folge)
        if self.Angaben_letzter_Einschub and not self.aktueller_autor and not self.aktuelles_werk:
            (self.aktueller_autor, self.aktuelles_werk, self.aktuelle_edition,
             self.aktuelle_stelle, self.aktuelles_jahr, self.aktuelle_stelle_folge) = self.Angaben_letzter_Einschub
        nd = self.fallback_compiler(node)
        self.Angaben_letzter_Einschub = (
            self.aktueller_autor, self.aktuelles_werk, self.aktuelle_edition,
            self.aktuelle_stelle, self.aktuelles_jahr, self.aktuelle_stelle_folge)
        (self.aktueller_autor, self.aktuelles_werk, self.aktuelle_edition,
         self.aktuelle_stelle, self.aktuelles_jahr, self.aktuelle_stelle_folge) = stapel
        curr = nd
        while curr.name != 'Stelle' and len(curr.children) == 1:
            curr = curr.children[0]
        if curr.name == 'Stelle':
            self.tree.new_error(nd, 'Sonderbar: Einschub besteht lediglich aus einer einzelnen '
                                'Stellenangabe!? Fehlen möglicherweise die Anführungsstriche '
                                'beim Belegtext?', WARNUNG_FEHLENDE_ANFÜHRUNGSSTRICHE)
        return nd

    def on_opus_minus(self, node):
        # Opera minora können wiederum Werkangaben enthalten,
        # die aber aktuelles_werk nicht überschreiben sollten,
        # da diese Variablen, später von der übergeordneten
        # Quellenangabe noch ausgelesen werden.
        nd = self.fallback_compiler(node)
        return nd

    def on_Autor(self, node):
        def geordnet_nach_teilstring_inklusion(namen: List[str]) -> List[str]:
            namen = list(set(namen))  # beseitige Doubletten
            for i in range(len(namen) - 1):
                for k in range(i + 1, len(namen)):
                    if namen[k].find(namen[i]) >= 0:
                        namen[i], namen[k] = namen[k], namen[i]
            return namen

        autor = node.content
        namen = [name for name in re.findall(r'[A-ZÄÖÜ.\-]+', autor)
                 if not re.match(RE_ROEM_ZAHL + '$', name)]  # LXX = Septuaginta nicht in Kapitälchen
        namen = geordnet_nach_teilstring_inklusion(namen)
        for name in namen:
            autor = autor.replace(name, name[0] + name[1:].lower())
        if autor == self.aktueller_autor and autor.strip().upper() not in GATTUNGSNAME \
                and not has_ancestor(self.path, 'ArtikelKopf'):
            node.result = ''
        else:
            node.result = autor
            if autor != self.aktueller_autor \
                    and not Vulgata_Septuaginta_Songerfall(autor, self.aktueller_autor):
                self.aktueller_autor = autor
                self.aktuelles_werk = ''
                self.aktuelle_stelle = ''
                self.aktuelles_jahr = ''
                self.aktuelle_stelle_folge = ''
        return node


    def on_Sekundärliteratur(self, node):
        # Reichere "op. cit."-Angaben um die jeweils gültigen
        # Informationen zu Autor und Werk an.
        autor, werk = '', ''
        autor_nd = node.pick('Autorangabe')
        werk_nd = node.pick('Werk')

        if autor_nd:
            autor = ''.join(nd.content for nd in autor_nd.select_if(
                lambda n: n.name != 'Anker' and not n.children, include_root=True))
        else:
            autor = self.aktueller_autor
            if not autor and "Nachschlagewerk" in self.op_cit:
                autor = "Nachschlagewerk"
        if werk_nd:
            werk = ''.join(nd.content for nd in werk_nd.select_if(
                lambda n: n.name != 'Anker' and not n.children, include_root=True))
            if werk_nd.has_attr('nachschlagewerk'):
                autor = "Nachschlagewerk"
            elif autor == "Nachschlagewerk":
                werk_nd.attr['nachschlagewerk'] = "1"
        # if werk.find("op. cit.") >= 0:
        #     werk = self.op_cit.get(autor, '')
        #     if werk:
        #         werk_nd.attr['werk'] = werk
        # elif autor:
        #     if werk == self.op_cit.get(autor, ''):
        #         pass  # Warnung wieder abgeschaltet, da es oft sinnvoll auf "op. cit." zu
        #         # verzichten, auch wenn es möglich wäre.
        #         # self.tree.new_error(node,
        #         #     f'Hier könnte auch "op. cit." geschrieben werden, denn der Autor '
        #         #     f'oder die Autorin "{autor}" wurde unlängst schon einmal zitiert.',
        #         #     WARNUNG_OP_CIT_ERWARTET)
        #     else:
        #         self.op_cit[autor] = werk

        # self.fallback_compiler() darf erst hier aufgerufen werden,
        # da on_Anker die Struktur verändert und erst mit
        # "reconstruct-tasks" später wieder ins reine bringt!
        node = self.fallback_compiler(node)
        if not node.pick('BelegStelle', reverse=True) \
                and werk_nd and werk_nd.content.strip() == 'op. cit.':
            werk = werk_nd.get_attr('werk', 'GibtsNicht')
            autor = werk_nd.get_attr('autor', 'GibtsNicht')
            for ctx in select_path(self.path, 'Sekundärliteratur', reverse=True):
                if ctx[-1] != node:
                    werk_nd = ctx[-1].pick('Werk')
                    if werk_nd.get_attr('werk', '') == werk \
                            and werk_nd.get_attr('autor', '') == autor:
                        # Erzeuge eine leere BelegStelle mit den letzten Stellenangaben
                        # zu demselben Autor und Werk
                        bs_nd = ctx[-1].pick('BelegStelle')
                        if bs_nd:
                            stelle_nd = bs_nd.pick('Stelle')
                            if stelle_nd:
                                stelle_copy = Node('Stelle', '').with_attr(stelle_nd.attr)
                                bs_copy = Node('BelegStelle', Node('Stellenangabe', stelle_copy))
                                bs_copy.attr['info'] = "rekonstruierte Stellenangabe"
                                bs_copy.with_pos(node.pick(LEAF_NODE, reverse=True).pos)
                                node.result = (*node.result, bs_copy)
                        break
        return node

    def on_Autorangabe(self, node):
        node = self.fallback_compiler(node)
        autor = node.content.strip()
        if autor != self.aktueller_autor \
                and not Vulgata_Septuaginta_Songerfall(autor, self.aktueller_autor):
            self.aktueller_autor = autor
            self.aktuelles_werk = ''
            self.aktuelle_stelle = ''
            self.aktuelles_jahr = ''
            self.aktuelle_stelle_folge = ''
        for ancestor in reversed(self.path):
            if ancestor.name == 'Quellenangabe':
                break
                node.attr['fremdautor'] = '1'
            elif ancestor.name == 'Einschub':
                break
        return node

    def validiere_Werktitel(self, path: List[Node], node: Node, werk: str):
        """Prüft Werkangaben auf Fehler. Fügt ggf. eine Warnung ein."""
        if any(nd.name == 'opus_minus' for nd in self.path):
            return
        if not werk or not (65 <= ord(werk[0]) <= 90):
            # Werktitel startet nicht mit Großbuchstaben, keine Verwechselungsgefahr
            return
        wupper = werk.upper()
        wuhead = wupper.split(' ')[0]  # if wupper.find(' ') >= 0 else ''
        parent = path[-2] if len(path) >= 2 else EMPTY_NODE
        if ((('Autor' not in parent and 'Autorangabe' not in parent)
              and (wuhead != wupper)  # opear = Dict: Autor -> Werke
              and (wuhead in opera or wupper in opera))   # or wupper in opera
            or ((self.aktueller_autor.upper() + ' ' + wuhead) in opera)):
                # or self.aktueller_autor.upper() + ' ' + wupper in opera:
                self.tree.new_error(
                    node, f'Falls {wuhead} ein Autorenname oder Teil eines Autorennamens ist, '
                    'sollte der gesamte Autorenname GROSSGESCHRIEBEN werden!',
                    WARNUNG_FALSCHE_AUTORANGABE)

    def on_Werk(self, node):
        node = self.fallback_compiler(node)
        if not self.aktueller_autor:
            if node.has_attr('nachschlagewerk'):
                self.aktueller_autor = "Nachschlagewerk"
            else:
                if len(self.path) >= 2 and 'Verweise' in self.path[-2]:
                    self.tree.new_error(node, 'Fehlende Funktion: MLW-DSL führt noch keine Daten aus anderen Artikeln nach...', NOTICE)
                    self.aktueller_autor = "Autor nicht gefunden!"
                    self.aktuelles_werk = "Werk nicht gefunden!"
                else:
                    self.tree.new_error(node, 'Werk "%s" ohne vorherige Autorangabe!'
                                        % node.content, FEHLER_AUTOR_FEHLT)

        werk_attr = node.get_attr('werk', '')
        if werk_attr == 'op_cit_Werk':
            try:
                werk = self.op_cit[self.aktueller_autor]
            except KeyError:
                self.tree.new_error(node, f'Autor "{self.aktueller_autor}" wurde op. cit. zitiert'
                    f' ohne im selben Artikel zuvor schon mit irgendeinem Werktitel zitiert worden'
                    f' zu sein!', WARNUNG_OP_CIT_NICHT_ZUZUORDNEN)
                werk = node.content.strip()
            node.attr['werk'] = werk
        elif werk_attr == 'op_cit_AutorWerk':
            node.attr['autor'] = self.aktueller_autor
            werk = self.aktuelles_werk
            node.attr['werk'] = werk
        else:
            werk = node.content
            if self.aktueller_autor:
                self.op_cit[self.aktueller_autor] = werk
        nicht_abgetrennte_Stellenangaben = RX_STELLENANGABE.findall(werk)
        if nicht_abgetrennte_Stellenangaben:
            s1 = nicht_abgetrennte_Stellenangaben[0]
            self.tree.new_error(node, f'Stellenangabe "{s1}" nicht durch Semikolon von '
                                      'der Werk- bzw. Editionsangabe abgetrennt!',
                                FEHLER_STELLENANGABE_NICHT_ABGETRENNT)

        wn = node.pick('Werkname')
        if wn and not wn.content and wn.has_attr('werkname'):
            werk = wn.attr['werkname'] + ' ' + werk
        self.validiere_Werktitel(self.path, node, werk)
        opus_minus = pick_from_path(self.path, 'opus_minus')
        if opus_minus:
            self.aktuelle_edition = werk
            if self.aktuelle_stelle_folge != '?':
                self.aktuelle_stelle_folge = ''
            opus_minus.attr['editionsangabe'] = werk
            # TODO: eigentlich sollte das Tag besser Editionsangabe heißen
        elif werk and werk != self.aktuelles_werk:
            if werk[0:3] == "ed.":
                self.aktuelle_edition = werk
            else:
                self.aktuelles_werk = werk
                self.aktuelle_edition = ''
            self.aktuelle_stelle = ''
            self.aktuelles_jahr = ''
            self.aktuelle_stelle_folge = ''
        # if self.path[-2].name != "AutorWerk":
        #     node.attr['autor'] = self.aktueller_autor
        return node

    def on_Werkname(self, node):
        node = self.fallback_compiler(node)
        content = node.content
        if content[-1:] == '|':
            name = content[:-1]
            if name == self.letzter_werkname:
                node.result = ''
                node.attr['werkname'] = self.letzter_werkname
                try:
                    parent = self.path[-2]
                    i = parent.index(node)
                    if len(parent.children) > i + 1:
                        if parent[i + 1].name == 'L':
                            del parent[i + 1]
                except IndexError:
                    pass
            else:
                if node.children:
                    nd = node.pick(lambda nd: not nd.children, reverse=True)
                else:
                    nd = node
                nd.result = nd.content[:-1]
                self.letzter_werkname = name
        else:
            self.letzter_werkname = content
        return node

    def on_Erscheinungsjahr(self, node):
        node = self.fallback_compiler(node)
        self.aktuelles_jahr = node.content.strip()
        return node

    def on_Quellenangabe(self, node):
        node = self.fallback_compiler(node)  # lies zuerst AutorWerk ein, falls vorhanden
        assert not 'autor' in node.attr
        assert not 'werk' in node.attr
        if not self.aktueller_autor:
            self.tree.new_error(node, 'Quellenangabe kann kein Autor und Werk '
                                'zugeordnet werden!', FEHLER_QUELLENANGABE_OHNE_AUTOR)
            node.attr['autor'] = "?"
            node.attr['werk'] = "?"
        else:
            node.attr['autor'] = self.aktueller_autor
            # # Albert M. Ausnahme: Gibt es sie doch nicht?
            # if self.aktueller_autor == "Albert. M.":
            #     autor = None
            #     quelle = node.pick_child('Quelle')
            #     if quelle:
            #         autor = quelle.pick_child('Autor') or quelle.pick_child('Autorangabe')
            #     if autor is None:
            #         # Aristoteles-Ausnahme: Bei Albertus Magnuns werden die Aristoteles-Texte
            #         # ohne Autorangabe nur nach ihrem Werk zitiert.
            #         node.attr['autor'] = "Arist."
            node.attr['werk'] = self.aktuelles_werk
            if self.aktuelle_edition:  node.attr['edition'] = self.aktuelle_edition

        # Ticket #379: Hochgestellte Stellenangaben schließen sich ohne Leerzeichen an
        new_result = []
        for i, nd in enumerate(node.children):
            if nd.name == 'BelegStelle' \
                    and pick_from_path(nd.pick_path(LEAF_PATH), 'HOCHGESTELLT') \
                    and i > 0 and node.children[i - 1].name == 'L':
                new_result.pop()
                node.attr['xml:space'] = 'preserve'
            new_result.append(nd)
        node.result = tuple(new_result)

        return node

    def on_Stelle(self, node):
        def als_text(nd) -> str:
            if nd.children:
                return ''.join(als_text(child) for child in nd.children)
            elif nd.name == 'HOCHGESTELLT':
                return '^{' + nd.content + '}'
            elif nd.name == 'TIEFGESTELLT':
                return '_{' + nd.content + '}'
            else:
                return nd.content

        node = self.fallback_compiler(node)

        content = node.content
        if RX_ERSCHEINUNGSJAHR.match(content):
            self.tree.new_error(node, f'Stellenangabe {content} scheint mit einer Jahreszahl zu '
                'beginnen. Falls es sich um ein Erscheinungsjahr hanadelt, sollte es mit der '
                'Werkangabe vor dem Semikolon stehen.', WARNUNG_ERSCHEINUNGSJAHR_IN_STELLENANGABE)

        if not self.aktueller_autor:
            try:
                c = next(select_path(self.path, {'Werk', 'Nachschlagewerk'}, reverse=True))
                nd = c[-1]
            except StopIteration:
                nd = EMPTY_NODE
            if nd.name != 'Nachschlagewerk':
                if any(nd.name == "ArtikelKopf" for nd in self.path):
                    self.tree.new_error(
                        node, f'Stellenangabe "{node.content}" ohne vorherige Autorangabe! '
                        'Oder wurde im Artikelkopf nach der Angabe einer Besonderheit '
                        'der Doppelpunkt vergessen?', FEHLER_AUTOR_FEHLT)
                else:
                    self.tree.new_error(node, 'Stellenangabe "%s" ohne vorherige Autorangabe!'
                                        % node.content, FEHLER_AUTOR_FEHLT)
        if 'Stellenabschnitt' in node:
            try:
                _, i2 = node.indices('Stellenabschnitt')
                abschnitt_1 = ''.join(als_text(nd) for nd in node.children[:i2])
                folge = ''.join(als_text(nd) for nd in node.children[i2:])
                if not folge:  # 2. Stellenabschnitt ist leer
                    # lösche leeren 2. Stellenabschnitt, verschiebe ggf. Leerzeichen in die
                    # nächst hähere Ebene
                    if node.children[i2 - 1].name == 'L':
                        i2 -= 1
                        if len(self.path) >= 2:
                            i = self.path[-2].index(node)
                            self.path[-2].insert(i + 1, node.children[i2])
                    node.result = node.result[:i2]
            except ValueError:
                abschnitt_1 = als_text(node)
                folge = als_text(node)
            if self.aktuelle_stelle != '' and self.aktuelle_stelle == abschnitt_1:
                node.result = folge
            if self.aktuelle_stelle.startswith(abschnitt_1) \
                    and self.aktuelle_stelle != abschnitt_1:
                self.tree.new_error(node,
                    "Redundante Angabe des ersten Stellenabschnitts: '%s'" % \
                    abschnitt_1, WARNUNG_REDUNDANTER_STELLENABSCHNITT)
            self.aktuelle_stelle = abschnitt_1
            self.aktuelle_stelle_folge = folge
        elif 'FesterZusatz' not in node:
            stelle = als_text(node)
            if self.aktuelle_stelle:
                if self.aktuelle_stelle_folge and stelle.startswith(self.aktuelle_stelle) \
                        and self.aktuelle_stelle.split(' ')[0] == stelle.split(' ')[0]:
                    self.aktuelle_stelle_folge = stelle[len(self.aktuelle_stelle):]
                    node.result = self.aktuelle_stelle_folge
                    self.tree.new_error(node,
                        "Redundante Angabe des ersten Stellenabschnitts: '%s'" % \
                        self.aktuelle_stelle, WARNUNG_REDUNDANTER_STELLENABSCHNITT)
                else:
                    if not self.aktuelle_stelle_folge:
                        if self.aktuelle_stelle == stelle \
                                and pick_from_path(self.path, 'BedeutungsPosition'):
                            # self.tree.new_error(node, f"Redundante Stellenangabe: {stelle}",
                            #                     WARNUNG_REDUNDANTE_STELLENANGABE)
                            self.tree.new_error(node,
                                f"Redundante Stellenangabe: {stelle} wurde stillschweigend gestrichen!",
                                HINWEIS_REDUNDANTE_STELLENANGABE_ELIMINIERT)
                            node.result = ''
                        elif self.aktuelle_stelle and stelle.lstrip()[0:2] == 'p.' \
                                and not self.aktuelle_stelle.lstrip()[0:2] == 'p.' \
                                and self.aktuelle_stelle.find('p.') >= 0:
                            self.tree.new_error(node, 'Falls »%s« eine abgekürzte Stellenangabe '
                                'ist, sollte bei der vorhergehenden Stellenangabe der '
                                'beizubehaltende Abschnitt durch »|« gekennzeichnet werden'
                                % stelle, WARNUNG_ABGEKÜRZTE_STELLENANGABE)
                        self.aktuelle_stelle = stelle
                    else:
                        if self.aktuelle_stelle_folge.lstrip()[0:2] == 'p.' \
                                and stelle.lstrip()[0:2] != 'p.':
                            self.tree.new_error(
                                node, 'Angabe der Folgestelle beginnt diesmal nicht mit »p.«!? Falls '
                                'sich die Kapitelnummer geändert hat, bitte erneut mit »|« kennzeichnen.!',
                                WARNUNG_GEÄNDERTE_KAPITELNUMMER)
                        self.aktuelle_stelle_folge = stelle
            else:
                if not pick_from_path(self.path, 'opus_minus'):
                    self.aktuelle_stelle = stelle

        def fmt_stelle(stelle, folge) -> str:
            def clean(folde: str):
                if folge[:1] == '(' and folge [-1:] == ')':
                    return folge.lstrip('(').rstrip(')')
                return folge

            if folge == '?':  return stelle
            if stelle and folge and stelle[-1:] not in ';:,. ' and folge[:1] != ' ':
                return stelle + ' ' + clean(folge)
            return (stelle + clean(folge)).strip()

        einschub = pick_from_path(self.path, 'Einschub')
        if einschub and self.Angaben_letzter_Einschub \
                and self.aktueller_autor != self.Angaben_letzter_Einschub[0] \
                and not einschub.pick('Werk') \
                and not (einschub.pick('Autor') or einschub.pick('Autorangabe')):
            self.tree.new_error(node, f'Diese Stelle wird dem Autor "{self.aktueller_autor}", '
                f'zugeschrieben. Falls das nicht stimmt, '
                f'bitte den richtigen Autor, ggf. mit Werk(!), in doppelten eckigen Klammern angeben, '
                f'e.g. "[[{self.Angaben_letzter_Einschub[0]}]]".',
                HINWEIS_DISAMIGUIRUNG_DER_AUTORANGABE)
        node.attr['autor'] = self.aktueller_autor
        node.attr['werk'] = self.aktuelles_werk
        if self.aktuelles_jahr:  node.attr['jahr'] = self.aktuelles_jahr
        if self.aktuelle_edition:  node.attr['edition'] = self.aktuelle_edition
        node.attr['stelle'] = fmt_stelle(self.aktuelle_stelle or content, self.aktuelle_stelle_folge)
        return node

    def on_Stellenangabe(self, node):
        if 'opus_minus' in node:
            om_flag = True
            try:
                if node['opus_minus'].pick('Stelle'):
                    self.aktuelle_stelle_folge = '?'
            except AttributeError as e:
                om_list = node['opus_minus']
                self.tree.new_error(node, f'Mehr als eine Editionsangabe? Handelt es sich bei '
                    f'»{om_list[1]}« wirklich um eine Editionsangabe? Datumsangaben sollten nur '
                    f'in einzelne Klammern gesetzt werden oder durch ein DOPPELTES SEMIKOLON '
                    f'getrennt innerhalb einer Editionsangabe angehängt werden. Orginalstellen ' 
                    f'können durch ein EINZELNES SEMIKOLON getrennt angehängt werden.',
                    WARNUNG_ZU_VIELE_EDITIONSANGABEN)
                if om_list[0].pick('Stelle'):
                    self.aktuelle_stelle_folge = '?'
        else:
            om_flag = False
        node = self.fallback_compiler(node)
        if om_flag:
            stelle = node.pick('Stelle')
            if stelle and self.aktuelle_edition:
                stelle.attr['edition'] = self.aktuelle_edition
        fz = node.pick('FesterZusatz')
        if fz and fz.content == "var. l." and not re.findall(r'\d', node.content):
            self.tree.new_error(
                node, '"%s" is als Stellenangabe ausgezeichnet und zugleich als varia lectio '
                'markiert! Möglicherweise handelt es sich gar nicht um eine Stellenangabe. '
                'Dann sollte es entweder in Anführungsstrichen (Zitat) oder in geschweiften '
                'Klammern (Zusatz) stehen.' % node.content.replace('var. l.', '').strip(),
                WARNUNG_STELLENANGABE_ALS_VARIA_LECTIO_MARKIERT)
        if node.children and node.children[-1].name == 'L' and len(self.path) >= 2:
            # move trailing whitespace upwards. This is merely cosmetic, but cannot be done
            # in AST-Transformation, because empty Stellenabschnitte still signal a divided
            # Stellenangabe, and cann thus only be dropped during compilation stage.
            parent = self.path[-2]
            i = parent.index(node) + 1
            if i >= len(parent.children) or parent.children[i].name != 'L':
                parent.insert(i, node.children[-1])
            node.result = node.result[:-1]

        return node


HINWEIS_LEMMAWORT_OHNE_VERDICHTUNG = ErrorCode(55)
WARNUNG_FALSCH_EINGEORDNETER_ZUSATZ = ErrorCode(405)
WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT = ErrorCode(410)


class MLWLinter(Compiler):
    """Liefert Warnungen für Konstrukte, die syntaktisch erlaubt sind, aber
    möglicherweise nicht den semantisch gewollten Sinn ausdrücken."""

    def reset(self):
        super().reset()
        self.unterdrücke_warnungen = set()

    def verarbeite_Direktiven(self, node):
        direktiven = node.pick('Direktiven', reverse=True)
        if not direktiven:  return
        lösche_direktiven = True
        if "Unterdrücke_Warnungen" in direktiven:
            for nummer in direktiven['Unterdrücke_Warnungen'].select('NUMMER'):
                nr = ErrorCode(int(nummer.content))
                if nr >= ERROR:
                    self.tree.new_error(nummer, f'Mit UNTERDRÜCKE WARNUNGEN können nur Warnungen, '
                                        f'aber keine Fehler wie {nummer} unterdrückt werden!')
                    lösche_direktiven = False
                self.unterdrücke_warnungen.add(nr)
        if lösche_direktiven:
            path = node.pick_path('Direktiven', reverse=True)
            if path and len(path) >= 2:  del path[-2]['Direktiven']

    def on_Artikel(self, node):
        self.verarbeite_Direktiven(node)
        node = self.fallback_compiler(node)
        if self.unterdrücke_warnungen:
            # assert isinstance(node, RootNode)
            root = self.tree
            root.errors = [e for e in root.errors if e.code not in self.unterdrücke_warnungen]
            epositions = dict()
            for position, nids in root.error_positions.items():
                for nid in nids:
                    epositions[nid] = position
            for nid, elist in list(root.error_nodes.items()):
                for error in elist:
                    if error.code in self.unterdrücke_warnungen:
                        try:
                            del root.error_nodes[nid]
                        except KeyError:
                            pass
                        try:
                            del root.error_positions[epositions[nid]]
                        except KeyError:
                            pass
        return node

    def on_LemmaBlock(self, node):
        node = self.fallback_compiler(node)
        try:
            if node[-1].name in {'Zusatz', 'FesterZusatz'} \
                    and node[-2].name == 'LemmaVarianten' \
                    and node[-2][-1].name not in {'Zusatz', 'FesterZusatz'}:
                self.tree.new_error(node, "Zusatz sollte innerhalb der Variantenklammer stehen, nicht danach.",
                                    WARNUNG_FALSCH_EINGEORDNETER_ZUSATZ)
        except IndexError:
            pass
        return node

    def on_EtymologieAngabe(self, node):
        node = self.fallback_compiler(node)
        if node.children and node[0].name == 'TLLVerweis':
            node.attr['tllverweis'] = '1'
            node.result = node[0].result
        try:
            if node[0].name == "Zusatz" and node[0].content.strip() == '?':
                self.tree.new_error(
                    node,
                    'Fragezeichen-Markierung ("ungesichert") sollte ohne geschweifte'
                    'Klammern notiert werden!',
                    WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT)
        except IndexError:
            pass
        return node

    def on_Lemmawort(self, node):
        if node.get_attr('verdichtung', '') == node.content \
                and node.content.find(' ') < 0 \
                and not pick_from_path(self.path, lambda nd: nd.name.endswith('Position')):
            self.tree.new_error(node, 'Keine Verdichtung beim Lemmawort angegeben',
                                HINWEIS_LEMMAWORT_OHNE_VERDICHTUNG)
        return node

    def on_Stelle(self, node):
        node = self.fallback_compiler(node)
        content = node.content
        if re.match(r' *a[.] *\d\d\d\d *$', content) \
                and not 'opus_minus' in self.path[-2] \
                and not (node.get_attr('autor', '') == 'Annal.')\
                and not (node.get_attr('werk', '') == 'chron.'):
            pass
            # # Deactiviert. Ticket # 376
            # self.tree.new_error(node,
            #     'Stellenangabe besteht nur aus einer Datierung!? Ist das beabsichtigt?',
            #     WARNUNG_DATIERUNG_ALS_STELLENANGABE)
        else:
            editionen = re.findall(r'\(.+\)', content)
            mlw_grammar = get_grammar()
            for ed in editionen:
                result = mlw_grammar.fullmatch('om_kern', ed[1:-1])
                if result is not None and not ed.startswith('(rec.'):
                    self.tree.new_error(node,
                        f'Editions- oder opus-minus-Angabe "{ed}" '
                        f'nicht in doppelten runden Klammern!',
                        WARNUNG_EDITIONSANGABE_OHNE_DOPPELKLAMMERN)
        return node

    def on_Zusatz(self, node):
        node = self.fallback_compiler(node)
        if node.content.find('op. cit.') >= 0:
            self.tree.new_error(node, '"op. cit." sollte nicht in einem Zusatz versteckt werden!',
                                WARNUNG_ANGABE_ALS_ZUSATZ_MASKIERT)
        return node

    def on_AddeAngabe(self, node):
        parent = self.path[-2] if len(self.path) >= 2 else None
        if parent and not (parent.pick('Bedeutungsangabe') or parent.pick('Beleg')):
            self.tree.new_error(node, 'ADDE AD sollte nur noch im Ausnahmefall verwendet werden, '
                'bei nachzutragenden Bedeutungsangaben und Belgen. '
                'Schreibe ansonsten bitte einen Zusatz: {adde ...}:', WARNUNG_VERALTETES_KONSTRUKT)
        node = self.fallback_compiler(node)
        return node

    def on_ZielLemmaBlock(self, node):
        node = self.fallback_compiler(node)
        belege = node.pick('Belege')
        if belege and belege.content[:5] == "adde ":
            self.tree.new_error(node, 'adde-Angabe in dieser Form nicht erlaubt! Vor der '
                'adde-Angabe sollte kein Doppelpunkt, der Doppelpunkt nach der adde-Angabe '
                'außerhalb der geschweiften Klammern stehen! '
                'Formbeispiel: veni VIDE vidi. {adde ...}: VICI. Caesar; ...',
                FEHLER_ADDE_FORMATIERUNG)
        return node

    def on_Quelle(self, node):
        """Gleicht die Werknamen mit den Werklisten aus der Datenbank ab. Ist ein Werkname
        dort nicht enthalten, gibt es eine Fehlermeldung.
        """
        def werk_hinweis(autor, alle_werke) -> str:
            werk_liste = [w for w in alle_werke if w[:3] == werk[:3]] or list(alle_werke)
            werk_liste.sort()
            if werk_liste:
                ua = ' u.a.' if len(werk_liste) > len(alle_werke) else ''
                return f'Bekannte Werke von »{autor}« sind{ua}: {str(werk_liste)[1:-1]}'
            else:
                return f'»{autor}« wird ohne Werk zitiert.'

        def prüfe_werk(werk, alle_werke):
            if werk in alle_werke:
                return True
            for w in alle_werke:
                l = w.split(' ')
                eintrag = l[0] if l[0][:1] != '[' else l[1]
                if werk == eintrag:
                     return True
                if eintrag == werk.split(' ')[0]:
                    return True
                if eintrag.find('[') >= 0:
                    if werk == eintrag.replace('[', '').replace(']', ''):
                        return True
                    i = eintrag.find('[')
                    k = eintrag.find(']')
                    if werk == eintrag[:i] + eintrag[k + 1:]:
                        return True
            return False

        node = self.fallback_compiler(node)
        try:
            autor = node['Autor'].content.replace('(?)', '').replace('[?]', '').strip().upper()
        except KeyError:  # nur "Autorangabe" bzw. disambiguiert
            # print(node.as_sxpr())
            return node
        try:
            werk = node['Werk'].content.replace('(?)', '').replace('[?]', '').strip()
        except KeyError:
            werk = ''

        alle_werke = opera[autor]
        if werk:
            if not prüfe_werk(werk, alle_werke):
                w_kurz = werk.split(' ')[0]
                if any(w.startswith(werk) for w in alle_werke):
                    self.tree.new_error(
                        node, f'Bei »{werk}« fehlt ein Teil der Werkangabe oder wurde in die '
                        f'Stellenangabe verschoben. ' +
                        werk_hinweis(autor, alle_werke), WARNUNG_TRENNUNG_WERK_STELLENANGABE)
                elif any(werk.startswith(w) for w in alle_werke) or any(w.startswith(w_kurz) for w in alle_werke):
                    self.tree.new_error(
                        node, f'Das Werk »{werk}« ist so nicht in der Datenbank verzeichnet. '
                        + werk_hinweis(autor, alle_werke), WARNUNG_TRENNUNG_WERK_STELLENANGABE)
                else:
                    if autor == "VITA":
                        self.tree.new_error(
                            node, f'Unbekannter Name »{werk}« für »{autor}« angegeben! ' \
                            + werk_hinweis(autor, alle_werke), FEHLER_UNBEKANNTES_WERK)
                    else:
                        self.tree.new_error(
                            node, f'Unbekanntes Werk »{werk}« von Autor »{autor}« angegeben! ' \
                                  + werk_hinweis(autor, alle_werke), FEHLER_UNBEKANNTES_WERK)
        elif alle_werke and not '' in alle_werke \
                and not all(w[:1] == "[" and w[-1:] == "]" for w in alle_werke):
            self.tree.new_error(
                node, f'Kein Werk für Autor »{autor}« angegeben! ' + werk_hinweis(autor, alle_werke),
                FEHLER_UNBEKANNTES_WERK)
        return node


get_compiler = ThreadLocalSingletonFactory(MLWCompiler)
get_verification = ThreadLocalSingletonFactory(MLWLinter)

def compile_MLW(ast):
    return get_verification()(get_compiler()(ast))

compiler: Junction = create_junction(
    MLWCompiler, "AST", "xml")



#######################################################################
#
# END OF DHPARSER-SECTIONS
#
#######################################################################


# MLW-Kompilierung ####################################################


def tracer(p: Parser, text: StringView) -> Tuple[Optional[Node], StringView]:
    node, rest = p._parse(text)  # <===== call to the actual parser!
    print(p.pname + p.ptype, flatten_sxpr(node.as_sxpr()))
    return node, rest


def kompiliere_mlw(source, log_dir='', source_data=NOPE, *, preserve_ast=False):
    """Kompiliert ``source`` and liefert (result, errors, ast) zurück.
    """
    datei_name = 'UNBEKANNTER_ARTIKEL'
    if '\n' not in source:
        datei_name = os.path.splitext(os.path.basename(source))[0]

    def compile_and_verify(ast):
        nonlocal datei_name

        ist_verfasser = create_match_function('ArtikelVerfasser')
        verfasser = None
        for artikel in ast.select('Artikel', include_root=True, reverse=True, skip_subtree='Artikel'):
            verf = artikel.pick_if(ist_verfasser, reverse=True)
            if verf:
                verfasser = verf
            elif verfasser:
                artikel.result = artikel.result + (copy.deepcopy(verfasser),)
        if verfasser is None:
            lastNode = ast[-1]
            nd = Node(ZOMBIE_TAG, '').with_pos(lastNode.pos + lastNode.strlen())
            ast.result = ast.result + (nd, )
            ast.new_error(nd, 'Autor oder Autorinnenangabe fehlt!')

        compiler = get_compiler()
        compiler.artikel_name = datei_name
        # sofern eine Datei mehrere Artikel enthält, werden anstelle des
        # Quelldateinamens Dateinamen aus den Lemmata dieser Artikel generiert.
        verifier = get_verification()
        return verifier(compiler(ast))

    start_logging(log_dir)
    print('Kompiliere: ', os.path.abspath(os.path.join(os.getcwd(), source)), '\n')
    preprocessor = get_preprocessor()
    grammar = get_grammar()  # grammar, source = test_filter(source)
    # watch_list = [grammar['Lemma'], grammar['_Lemma'], grammar['_lemma_text'], grammar['LemmaWort']]
    # set_tracer(watch_list, tracer)
    transformer = get_transformer()
    result = compile_source(source, preprocessor, grammar, transformer,
                            compile_and_verify, preserve_AST=preserve_ast)
    # set_tracer(watch_list, None)
    return result


def kompiliere_schnipsel(schnipsel: str, parser_name: str) -> Node:
    """kompiliert einen MLW-Schnipsel für einen Einheiten-Test."""
    st = get_grammar()(schnipsel, parser_name)  # -> CST
    get_transformer()(st)                       # -> AST
    tree = compile_MLW(st)
    return tree


DATEN_VORLAGE = """<Datenbaum><pre><code>
%s
</code></pre></Datenbaum>
<br/>
"""

def schnipsel_zu_HTML(schnipsel: str, parser_name: str) -> Tuple[str, str]:
    baum = kompiliere_schnipsel(schnipsel, parser_name)
    daten_schnipsel = baum.serialize("indented")
    # daten_schnipsel = re.sub(r'L\n\s*"', r'L "', daten_schnipsel)
    process_tree(get_Ausgabe_Transformation(), baum)
    process_tree(get_HTML_Transformation(), baum)
    html_schnipsel = baum.as_xml()
    return DATEN_VORLAGE % daten_schnipsel + html_schnipsel


KOPF_VORLAGE = """<h3>%s:%s</h3>
<Quellcode><pre><code>
%s
</code></pre></Quellcode>
<br/>
"""

AUSGABE_VORLAGE = """<Ausgabe>
%s
</Ausgabe><br />
"""

def ausgabe_probe(datei_name: str, test_unit: Dict) -> str:
    sammlung = ['<Artikel>']
    for parser in test_unit:
        for test, test_schnipsel in test_unit[parser].get('match', dict()).items():
            kopf = KOPF_VORLAGE % (parser, test, test_schnipsel)
            ausgabe = AUSGABE_VORLAGE % schnipsel_zu_HTML(test_schnipsel, parser)
            sammlung.extend([kopf, ausgabe])
    sammlung.append('</Artikel>')
    return schreibe_html_seite(datei_name, '\n'.join(sammlung), mit_css=True)



# Ausgabe-Anreicherung ################################################


INLINE_ELEMENTS = {'Nachtrag',
                   'LemmaBlock',
                   'Lemma',
                   'LemmaVarianten',
                   'ZielLemma',
                   'flexion',
                   'EtymologieAngabe',
                   'EtymologiePosition',
                   'GrammatikVariante',
                   'GrammatikVarianten',
                   'Beschreibung',
                   'Zusatz',
                   'Zusätze',
                   'Bedeutungsangabe',
                   'LateinischeBedeutung',
                   'DeutscheBedeutung',
                   'unsicher',
                   'wortart',
                   'wortarten',
                   'genera',
                   'Stelle',
                   'Stellenangabe',
                   'Autor',
                   'Werk',
                   'BelegStelle',
                   'BelegText',
                   'Einschub',
                   'Verweise',
                   'PseudoVerweise',
                   'unberechtigt',
                   'VerweisBlock',
                   'verwiesen_auf',
                   'a',
                   'GebrauchsPosition'}
OMITTED_TAGS = {'TEXT', ':Whitespace', 'L'}


#RX_FULLSTOP = re.compile(r'[.!?][)\]´’\'"]')
RX_FULLSTOP = re.compile(r'[.!?][´’\'"]')  # nach Klammern wird doch ein Punkt gesetzt!
RX_ADJACENT_BRACKTES = re.compile(r'\)\s*\(|\]\s*\[')
RX_KLAMMER =  re.compile(r'[\[\]()]')


KOPF_SCHWELLE = 120  # Zeichen, ab denen nach der Lemma-Position oder dem Artikelkopf
                     # ein Absatz eingefügt wird.
# KÖRPER_SCHWELLE = 2000  # Zeichen, nach denen bei Bedeutungsblöcken ein Absatz eingefügt wird

KLAMMERN_FETT_SCHWELLE = 220  # Anzahl Zeichen innerhalb von Klammern, ab der die Klammern fett gedruckt werden


SATZZEICHEN = ('.', '!', '?', ';', ':', ',')
SATZZEICHEN_ERWEITERT = SATZZEICHEN + (')', ']')


def ist_Klammer(zeichen: str) -> bool:
    "Liefert wahr, falls `zeichen` eine öffnende oder schließende Klammer ist."
    return zeichen.strip() in ('(', ')', '[', ']')


class TextÄnderungenMixin:
    """Eine Mixin-Klasse, die Funktionen für die Ergänzungen auf der Zeichenkettenebene
    bereit hält."""

    def davor(self, node: Node, zeichen: str, classes: str = '', allow_duplication=False) -> Node:
        """Setzt das oder die gegebenen Zeichen vor den Inhalt von `node`.
        Gibt `node` wieder zurück."""
        def prepend(c: str, s: str) -> str:
            """Prepend character c to string s, skipping whitespace."""
            t = s.lstrip()
            n = len(s) - len(t)
            return ' ' * n + c + t
        if not zeichen or (not allow_duplication and node.content.startswith(zeichen)):
            return node
        if node.children:
            nd = node[0]
            tn = nd.name
            if ((tn == 'TEXT'
                 or (tn == 'Zusatz' and not nd.children and not ist_Klammer(zeichen)))
                    and has_class(nd, classes)):
                nd.result = prepend(zeichen, nd.result)
            else:
                tag_name = 'L' if not zeichen.strip() else 'TEXT'
                z = Node(tag_name, zeichen, True)
                add_class(z, classes)
                node.result = (z, *node.result)
        elif has_class(node, classes):
            node.result = prepend(zeichen, cast(str, node.result))
            if node.name == 'L' and zeichen.strip():  node.name = 'TEXT'
        else:
            raise AssertionError('Kann "' + zeichen + '" nicht vor einem '
                                 + node.name + '-Knoten einfügen!')
        return node

    def danach(self, node: Node, zeichen: str, classes: str = '', allow_duplication=False) -> Node:
        """Setzt das oder die gegebenen Zeichen hinter den Inhalt von `node`.
        Gibt `node` wieder zurück."""
        def append(s: str, c: str) -> str:
            """Append character c to string s, skipping whitespace."""
            t = s.rstrip()
            n = len(s) - len(t)
            return t + c + ' ' * n
        if not zeichen or (not allow_duplication and node.content.endswith(zeichen)):
            return node
        if node.children:
            kursiv = self.ist_kursiv(node.pick_path(LEAF_PATH, reverse=True))
            tag_name = 'L' if not zeichen.strip() else 'TEXT'
            z = Node(tag_name, zeichen, True)
            add_class(z, classes)
            node.result = (*node.result, z)
            # Klammerkursivierung wird gesondert behandelt!
            if tag_name == 'TEXT' \
                    and zeichen.strip() not in ('(', ')', '[', ']') \
                    and classes.find('kursiv') < 0 and classes.find('grade') < 0 and \
                    (self.ist_kursiv(node.pick_path(LEAF_PATH, reverse=True)) != kursiv):
                z.name = "Kursiv" if kursiv else "Grade"
                add_class(z, 'kursiv' if kursiv else 'grade')
            if node[-1].name == node[-2].name and node[-1].has_equal_attr(node[-2]):
                node[-2].result = append(node[-2].result, node[-1].result)
                node.result = node.result[:-1]
        elif has_class(node, classes):
            node.result = append(node.result, zeichen)
            if node.name == 'L' and zeichen.strip():  node.name = 'TEXT'
        else:
            raise AssertionError('Kann "' + zeichen + '" nicht nach einem '
                                 + node.name + '-Knoten einfügen!')
        return node

    def ergänze(self, node, davor=None, danach=None, classes='', allow_duplication=False):
        """Fügt vor und/oder nach dem Knoten Trennzeichen ein."""
        node = self.fallback_compiler(node)
        node = self.davor(node, davor, classes, allow_duplication)
        node = self.danach(node, danach, classes, allow_duplication)
        return node

    def trenne(self, node, tag_name, trennzeichen):
        """Fügt zwischen Kind-Knoten mit `tag_name` das `trennzeichen` ein, indem
        das Trennzeichen an den Inhalt der jeweiligen Kind-Knoten angehängt wird.

            >>> TextÄnderungenMixin().trenne(parse_sxpr('(flexion (FLEX "-um") (FLEX "-i"))'), 'FLEX', ', ').as_sxpr()
            '(flexion (FLEX "-um, ") (FLEX "-i"))'
        """
        flag = node.children[0].name == tag_name
        for i in range(len(node.children)-1):
            if node.children[i + 1].name == tag_name:
                if flag:
                    self.danach(node.children[i], trennzeichen)
                else:
                    flag = True
            else:
                flag = False
        return node

    def trenne_mit_tag(self, node, tag_name, trennzeichen):
        """Fügt zwischen Kind-Knoten mit `tag_name` ein TEXT-tag mit dem
        `trennzeichen` ein. Falls `trennzeichen` aus Leerzeichen besteht,
        wird statt eines TEXT- ein L-tag eingefügt.

            >>> TextÄnderungenMixin().trenne_mit_tag(parse_sxpr('(flexion (FLEX "-um") (FLEX "-i"))'), 'FLEX', ', ').as_sxpr()
            '(flexion (FLEX "-um") (TEXT ", ") (FLEX "-i"))'

        Bei Knoten ohne Kinder ('Blatt-Knoten') geschieht nichts.
        """
        if node.children:
            flag = node.children[0].name == tag_name
            collect_result = [node.children[0]]
            trenn_tag = 'TEXT' if trennzeichen.strip() else 'L'
            for child in node.children[1:]:
                if child.name == tag_name:
                    if flag:
                        collect_result.append(Node(trenn_tag, trennzeichen))
                    else:
                        flag = True
                else:
                    flag = False
                collect_result.append(child)
            node.result = tuple(collect_result)
        return node

    def ist_kursiv(self, path: List[Node]) -> Optional[bool]:
        """Prüft, ob das letzte Element des Kontexts grade order kursiv gesetzt wird."""
        assert isinstance(path, list), '%s ist kein Kontext, d.h. keine Liste von Knoten' \
                                          % str(path)
        GRADE_TAGS = {'GRI_WORT', 'Grade', 'BelegText'}
        KURSIVE_TAGS = {'vel', 'genus', 'WortartAnzeige', 'Bedeutungsangabe',
                        'Derselbe', 'Sonderbelege', 'Zusatz', 'FesterZusatz',
                        'VariaLectioZusatz', 'ArtikelVerfasser', 'Kursiv',
                        'EtymologieSprache'}
        KURSIV_SPEZIAL = {'Besonderheit', 'Kategorie'}  # wenn Vorfahre von 'Beschreibung'
        GRADE_SPEZIAL = {'Variante'}  # wenn Vorfahre von 'Beschreibung'
        for i in range(len(path) - 1, -1, -1):
            nd = path[i]
            cls = nd.get_attr('class', '')
            if cls.find('grade') >= 0:
                return False
            if cls.find('kursiv') >= 0:
                return True
            if nd.name in GRADE_TAGS:
                return False
            if nd.name in KURSIVE_TAGS:
                return True
            # if nd.name == "Kursiv":
            #     return not self.ist_kursiv(path[:-1]) if len(path) > 1 else True
            if nd.name == 'Beschreibung' and i > 0:
                parent = path[i - 1]
                if parent.name in GRADE_SPEZIAL:
                    return False
                if parent.name in KURSIV_SPEZIAL:
                    return True
        return None

    def baue_text_knoten(self, inhalt, soll_kursiv, umgebung_kursiv):
        if soll_kursiv == umgebung_kursiv:
            knoten = Node('TEXT', inhalt)
        elif soll_kursiv:
            knoten = Node('Kursiv', inhalt)
            add_class(knoten, 'kursiv')
        else:
            knoten = Node('Grade', inhalt)
            add_class(knoten, 'grade')
        return knoten

    def ist_unsichtbar(self, node_or_path: Union[Node, List[Node]]):
        """Liefert wahr, wenn der Knoten und alle Kindknoten entweder
        im Attribut Klasse die Klasse 'versteckt' enthalten order nur
        aus Leerzeichen bestehen."""
        def unsichtbar(node):
            return (is_wsp([node]) or
                    (has_class(node, 'versteckt')) or
                    (all(unsichtbar(child) for child in node.children)
                     if node.children
                     else (node.name != 'a' and not node.content.strip())))
        if isinstance(node_or_path, Node):
            return unsichtbar(cast(Node, node_or_path))
        else:
            for ancestor in cast(List, node_or_path):
                if unsichtbar(ancestor):
                    return True
        return False


    def letzter_sichtbarer_Zweig(self, node: Node) -> Optional[Node]:
        """Returns the last sub-branch where the last child node is still visible."""
        try:
            path = next(node.select_path_if(
                lambda ctx: not ctx[-1].children and not self.ist_unsichtbar(ctx),
                include_root=True, reverse=True))
            if len(path) <= 1:
                return path[-1]
            for i in range(len(path) - 1, 0, -1):
                if path[i - 1].children[-1] != path[i]:
                    return path[i]
            return path[0]
        except StopIteration:
            return None

    def unmittelbar_davor(self, teilbaum, was) -> Node:
        """
        Setzt vor das erste 'Blatt' des Teilbaums eine weitere Zeichnkette
        sofern das Blatt nicht bereits mit dieser Zeichenkette beginnt.
        Im unterschied zur einfachen davor()-Methode, wird soweit als
        möglich auf die Einfügung eines TEXT-Knoten verzichtet.
        Gibt den ergänzten oder veränderten Teilbaum zurück.
        """
        def setze_davor(element, was):
            return self.davor(element, was)
        try:
            element = next(teilbaum.select_if(self.selector, reverse=False))
            if element.name in OMITTED_TAGS or element.name == 'a':
                parent = teilbaum
                while parent.children and element not in parent.children:
                    parent = parent.children[1]
                if parent:
                    return setze_davor(parent, was)
                else:
                    print("WARNUNG: %s vor %s bleibt unsichtbar" % (was, element.content))
            else:
                return setze_davor(element, was)
        except StopIteration:
            return setze_davor(teilbaum, was)

    def selector(self, nd: Node) -> bool:
        return (not self.ist_unsichtbar(nd)
                and ((not nd.children
                      or (nd.name in INLINE_ELEMENTS
                          and nd.children[-1].name not in (WHITESPACE_PTYPE, 'L')))
                     and nd.name not in (WHITESPACE_PTYPE, 'L')))

    def selector_innermost(self, nd: Node) -> bool:
        return (not self.ist_unsichtbar(nd)
                and ((not nd.children
                      or (nd.name in INLINE_ELEMENTS
                          and nd.children[-1].name not in (WHITESPACE_PTYPE, 'L')
                          and not nd.children[-1].children))
                     and nd.name not in (WHITESPACE_PTYPE, 'L')))

    def unmittelbar_danach(self, teilbaum: Node, was: str) -> Node:
        """
        Hängt an das letzte 'Blatt' des Teilbaums eine weitere Zeichnkette
        an, sofern das Blatt nicht bereits mit dieser Zeichenkette endet.
        Im unterschied zur einfachen danach()-Methode, wird soweit als
        möglich auf die Einfügung eines TEXT-Knoten verzichtet.
        Gibt den ergänzten oder veränderten Teilbaum zurück.
        """
        def hänge_an(element, was):
            return self.danach(element, was)
        try:
            element = next(teilbaum.select_if(self.selector_innermost, reverse=True))
            if element.name == "Verweise":
                # Ausnahme für Verweise zum Zweck leichterer Nachverarbeitung für die Druckausgabe
                try:
                    i = teilbaum.index(element)
                    if i + 1 < len(teilbaum.children) and teilbaum[i + 1].name == 'TEXT':
                        teilbaum[i + 1].result = was + teilbaum[i + 1].result
                    else:
                        element.result = element.children + (Node('TEXT', was, True),)
                        # teilbaum.result = teilbaum.result[:i + 1] + (Node('TEXT', was, True),) \
                        #                + teilbaum.result[i + 1:]
                        return teilbaum
                except ValueError:
                    if element.children and element.children[-1].name == 'TEXT':
                        element.children[-1].result += was
                    else:
                        element.result = element.children + (Node('TEXT', was, True),)
                    return teilbaum
                    # return hänge_an(teilbaum, was)
            elif element.name in OMITTED_TAGS  or element.name == 'a' \
                    or has_class(element, 'fett'):
                parent = teilbaum
                while parent.children and element not in parent.children:
                    parent = parent.children[-1]
                if parent:
                    return hänge_an(parent, was)
                else:
                    print("WARNUNG: %s nach %s bleibt unsichtbar" % (was, element.content))
            else:
                return hänge_an(element, was)
        except (StopIteration, ValueError):
            return hänge_an(teilbaum, was)

    def abschließender_Punkt(self, node):
        """Hängt an das letzte Kind-Element des gegebenen Knotens einen
        abschließenden Punkt an, sofern der Text
        des Knotens nicht schon mit einem Punkt endet."""
        def next_char():
            next_ctx = next_path(self.path)
            content = next_ctx[-1].content if next_ctx else ' '
            while not content:
                next_ctx = next_path(next_ctx)
                content = next_ctx[-1].content if next_ctx else ' '
            return content[:1]

        ende = node.content.rstrip()[-2:]
        if ende[-1:] not in SATZZEICHEN and not RX_FULLSTOP.match(ende) \
                and next_char() not in SATZZEICHEN_ERWEITERT:
            self.unmittelbar_danach(node, '.')

    def trenner_nach(self, path: List[Node], index, trenner=";"):
        """Füge `trenner` nach dem Element mit index `index` hinzu."""
        node = path[-1]
        assert node.children
        assert index < len(node.children)
        try:
            element = node.children[index]
            nachfolger = node.children[index + 1]
            if element.content.rstrip().endswith(trenner) \
                    or nachfolger.content.lstrip().startswith(trenner):
                return  # verdoppele nicht möglicherweise schon vorhandenen trenner
            if nachfolger.content[:1] != " " and trenner[-1:] != " ":
                trenner += " "
            k0 = self.ist_kursiv(path)
            k1 = self.ist_kursiv(path + leaf_path([element], LAST_CHILD))
            k2 = self.ist_kursiv(path + [nachfolger])
            if k1 == k2 and nachfolger.name in ('L', 'TEXT'):
                nachfolger.name = 'TEXT'
                nachfolger.result = trenner + nachfolger.result
            else:
                tr_node = Node('TEXT', trenner)
                if k0 != k1:
                    add_class(tr_node, 'kursiv' if k1 else 'grade')
                node.result = node.result[:index + 1] + (tr_node,) + node.result[index + 1:]
        except IndexError:
            if trenner[-1:] != " ":  trenner += " "
            node.result = node.result + Node('TEXT', trenner)

    def ueberleitung(self, kopf: Union[Node, str], ende: Union[Node, str]) -> bool:
        """Liefert wahr zurück, wenn die beiden Bestandteile durch eine
        verbale Überleitung verbunden sind, so dass es keines Satzzeichens
        mehr bedarf."""
        KOPULA_LINKS = ('et', 'adde', 'v.')
        KOPULA_RECHTS = ('et', 'adde')
        if isinstance(kopf, Node):  kopf = cast(Node, kopf).content
        if isinstance(ende, Node):  ende = cast(Node, ende).content
        kopf = kopf.rstrip()
        ende = ende.lstrip()
        if kopf[-1:] in (';', '?') or ende[:1] == ';':
            return True
        # for kopulum in KOPULA:
        #     L = len(kopulum) + 1
        #     if kopf[-L:] == ' ' + kopulum or ende[:L] == kopulum + ' ':
        #         return True
        for kopulum in KOPULA_LINKS:
            L = len(kopulum) + 1
            if kopf[-L:] == ' ' + kopulum:
                return True
        for kopulum in KOPULA_RECHTS:
            L = len(kopulum) + 1
            if ende[:L] == kopulum + ' ':
                return True
        return False


    def Klammer_Kursivierung(self, node, ctx: List[Node] = []):
        """Kursiviert Klammern nach der Regel: Wenn das erste und das letzte Element
        innerhalb der Klammer beide kursiv sind, dann sind auch beide Klammern kursiv.
        Anderenfalls bleiben beide(!) Klammern gerade.

        Diese Funktion bearbeitet nur solche Knoten, bei denen die öffnende und
        schließende Klammer am Anfang bzw. Ende des ersten und letzten Kind-Knoten stehen,
        z.B.:
        (TEXT "(") (Zusatz "psalmis") (TEXT ")")
        """
        def bestimme_stil(klammer: List[Node], vorgänger_nachfolger: List[Node]) -> Tuple[bool, bool]:
            ist_stil = bool(self.ist_kursiv(klammer))
            soll_stil = self.ist_kursiv(vorgänger_nachfolger)
            if soll_stil is None:
                soll_stil = ist_stil
            return soll_stil, ist_stil

        try:
            iterator = node.select_path_if(lambda ctx: not ctx[-1].children)
            öffnende = next(iterator)
            if öffnende[-1].name != TEXT or öffnende[-1].content[:1] not in ('(', '['):
                return
            nachfolger = next(iterator) if len(öffnende[-1].content) == 1 else öffnende
            iterator = node.select_path_if(lambda ctx: not ctx[-1].children, reverse=True)
            schließende = next(iterator)
            if schließende[-1].name not in (TEXT, 'Zusatz') \
                    or schließende[-1].content[-1:] not in (')', ']'):
                return
            vorgänger = next(iterator) if len(schließende[-1].content) == 1 else schließende
        except StopIteration:
            return

        if not ctx:  ctx = self.path
        öffnende = ctx + öffnende
        schließende = ctx + schließende
        nachfolger = ctx + nachfolger
        vorgänger = ctx + vorgänger

        soll_öffnende, ist_öffnende = bestimme_stil(öffnende, nachfolger)
        soll_schließende, ist_schließende = bestimme_stil(schließende, vorgänger)
        if soll_öffnende != soll_schließende:
            soll_öffnende = soll_schließende = False
        if ist_öffnende != soll_öffnende:
            if len(öffnende[-1].content) > 1:
                klammer_auf = öffnende[-1].content[:1]
                öffnende[-1].result = öffnende[-1].content[1:]
                öffnende[-1] = Node('TEXT', klammer_auf)
                öffnende[-2].result = (öffnende[-1],) + öffnende[-2].children
            add_class(öffnende[-1], 'kursiv' if soll_öffnende else 'grade')
            # Workaround für Druckausgabe
            if öffnende[-1].name == 'TEXT':
                öffnende[-1].name = 'Kursiv' if soll_öffnende else 'Grade'
        if ist_schließende != soll_schließende:
            if len(schließende[-1].content) > 1:
                klammer_zu = schließende[-1].content[-1:]
                schließende[-1].result = schließende[-1].content[:-1]
                schließende[-1] = Node('TEXT', klammer_zu)
                schließende[-2].result = schließende[-2].children + (schließende[-1],)
            add_class(schließende[-1], 'kursiv' if soll_schließende else 'grade')
            # Workaround für Druckausgabe
            if schließende[-1].name == 'TEXT':
                schließende[-1].name = 'Kursiv' if soll_schließende else 'Grade'


    def Klammer_Kursivierung_BelegText(self, node):
        """Dasselbe wie die Funktion 'Klammer_Kursivierung' jedoch im BelegText
        und unter weniger einschränkenden Bedingungen, aber unter der Annahme,
        dass die öffnende Klammer am Ende eines Elements steht und die schließende
        am Anfang steht, oder - umgekehrt - die öffnende Klammer am Anfang eines
        Elements und die schließende am Ende.
        """
        def set_state(node: Node, state: str):
            if state == 'kursiv':
                remove_class(node, 'grade')
                add_class(node, 'kursiv')
                if node.name == 'TEXT':  node.name = 'Kursiv'
            elif state == 'grade':
                remove_class(node, 'kursiv')
                add_class(node, 'grade')
                if node.name == 'TEXT':  node.name = "Grade"
            else:
                raise AssertionError('Illegalder Kursivierungsstatus: ' + state)

        def gleiche_Randklammern_an(anfang_kursiv, ende_kursiv, ctx_open, ctx_close):
            nd_open = ctx_open[-1]
            nd_close = ctx_close[-1]
            if anfang_kursiv == ende_kursiv == True:
                if len(nd_open.content.lstrip().rstrip("‘")) == 1:
                    set_state(ctx_open[-1], 'kursiv')
                if len(nd_close.content.rstrip().lstrip("’")) == 1:
                    set_state(ctx_close[-1], 'kursiv')
            elif anfang_kursiv == ende_kursiv == False:
                if len(nd_open.content.lstrip().rstrip("‘")) == 1:
                    set_state(ctx_open[-1], 'grade')
                if len(nd_close.content.rstrip().lstrip("’")) == 1:
                    set_state(ctx_close[-1], 'grade')

        content = node.content
        unmatched_round, unmatched_square = [], []
        round_brackets = matching_brackets(content, '(', ')', unmatched_round)
        square_brackets = matching_brackets(content, '[', ']', unmatched_square)

        # bemängele unausgewogene Klammern
        for unmatched_pos in chain(unmatched_round, unmatched_square):
            if content[unmatched_pos] in ('(', '['):
                message = "Schließende Klammer fehlt!"
            else:
                assert content[unmatched_pos] in (')', ']')
                message = "Öffnende Klammer fehlt!"
            warnung = Error(message, node.pos + unmatched_pos, WARNUNG_UNAUSGEWOGENE_KLAMMER)
            self.tree.add_error(node, warnung)

        # Sortiere Klammern nach Größe des umfassten bereiches. Kleinere zuerst.
        # Dadurch ist sichergestellt, dass innere Klammern vor äußeren kursiviert werden.

        all_brackets = [(pair, '[', ']') for pair in square_brackets] + \
                       [(pair, '(', ')') for pair in round_brackets]
        all_brackets.sort(key=lambda it: it[0][1] - it[0][0])

        for pair, opening, closing in all_brackets:
            cm = ContentMapping(node)
            ctx_open, _ = cm.get_path_and_offset(pair[0])
            ctx_close, _ = cm.get_path_and_offset(pair[1])
            nd_open = ctx_open[-1]
            nd_close = ctx_close[-1]

            # Fall 1: Anfang( Mitte )Ende

            if nd_open.content.endswith(opening) \
                    and nd_close.content.startswith(closing):
                if self.ist_kursiv(next_leaf_path(ctx_open)) \
                        and self.ist_kursiv(prev_leaf_path(ctx_close)):
                    kursiv = True
                    klasse = 'kursiv'
                else:
                    kursiv = False
                    klasse = 'grade'

                if self.ist_kursiv(ctx_open) != kursiv:
                    if re.fullmatch(r' *[(\[]', nd_open.content):
                        set_state(nd_open, klasse)
                    else:
                        opening_node = Node('TEXT', opening).with_pos(
                            nd_open.pos + nd_open.strlen() - 1)
                        set_state(opening_node, klasse)
                        nd_open.result = nd_open.content[:-1]
                        i = ctx_open[-2].index(nd_open)
                        ctx_open[-2].result = ctx_open[-2].children[:i + 1] + (opening_node,) \
                                              + ctx_open[-2].children[i + 1:]

                if self.ist_kursiv(ctx_close) != kursiv:
                    if re.fullmatch(r'[)\]] *', nd_close.content):
                        set_state(nd_close, klasse)
                    else:
                        closing_node = Node('TEXT', closing).with_pos(nd_close.pos)
                        set_state(closing_node, klasse)
                        nd_close.result = nd_close.content[1:]
                        k = ctx_close[-2].index(nd_close)
                        ctx_close[-2].result = ctx_close[-2].children[:k] + (closing_node,) \
                                               + ctx_close[-2].children[k:]

            # Fall 2: (Anfang Mitte Ende)

            elif nd_open.content.lstrip().startswith(opening) \
                and nd_close.content.rstrip().endswith(closing):
                anfang_kursiv = self.ist_kursiv(ctx_open) \
                    or (len(nd_open.content.lstrip().rstrip("‘")) == 1
                        and self.ist_kursiv(next_leaf_path(ctx_open)))
                ende_kursiv = self.ist_kursiv(ctx_close) \
                    or (len(nd_close.content.rstrip().lstrip("’")) == 1
                        and self.ist_kursiv(prev_leaf_path(ctx_close)))

                if anfang_kursiv != ende_kursiv:
                    if anfang_kursiv:
                        if re.fullmatch(r' *[(\[] *', nd_open.content):
                            set_state(nd_open, 'grade')
                        else:
                            opening_node = Node('TEXT', opening).with_pos(nd_open.pos)
                            set_state(opening_node, 'grade')
                            nd_open.result = nd_open.content[1:]
                            i = ctx_open[-2].index(nd_open)
                            ctx_open[-2].result = ctx_open[-2].children[:i] \
                                + (opening_node, ) + ctx_open[-2].children[i:]

                    if ende_kursiv:
                        if re.fullmatch(r' *[)\]] *', nd_close.content):
                            # opening_node = Node('TEXT', opening).with_pos(nd_open.pos)
                            # set_state(opening_node, 'grade')  # nd_open?
                            set_state(nd_close, 'grade')
                        else:
                            closing_node = Node('TEXT', closing).with_pos(
                                nd_close.pos + nd_close.strlen() - 1)
                            set_state(closing_node, 'grade')
                            nd_close.result = nd_close.content[:-1]
                            k = ctx_close[-2].index(nd_close)
                            ctx_close[-2].result = ctx_close[-2].children[:k + 1] \
                                + (closing_node,) + ctx_close[-2].children[k + 1:]
                else:
                    gleiche_Randklammern_an(anfang_kursiv, ende_kursiv, ctx_open, ctx_close)
                # elif anfang_kursiv:  # anfang_kursiv == ende_kursiv == True
                #     if len(nd_open.content.lstrip().rstrip("‘")) == 1:
                #         set_state(ctx_open[-1], 'kursiv')
                #     if len(nd_close.content.rstrip().lstrip("’")) == 1:
                #         set_state(ctx_close[-1], 'kursiv')
                # else:  # anfang_kursiv == ende_kursiv == False
                #     if len(nd_open.content.lstrip().rstrip("‘")) == 1:
                #         set_state(ctx_open[-1], 'grade')
                #     if len(nd_close.content.rstrip().lstrip("’")) == 1:
                #         set_state(ctx_close[-1], 'grade')

            # Fall 3: Anf(ang Mitte En)de

            else:
                if nd_open.content.lstrip().startswith(opening):
                    anfang_kursiv = self.ist_kursiv(ctx_open) \
                        or (len(nd_open.content.lstrip()) == 1
                            and self.ist_kursiv(next_leaf_path(ctx_open)))
                else:
                    anfang_kursiv = self.ist_kursiv(ctx_open)
                if nd_close.content.rstrip().endswith(closing):
                    ende_kursiv = self.ist_kursiv(ctx_close) \
                    or (len(nd_close.content.rstrip()) == 1
                        and self.ist_kursiv(prev_leaf_path(ctx_close)))
                else:
                    ende_kursiv = self.ist_kursiv(ctx_close)

                if anfang_kursiv != ende_kursiv:
                    if anfang_kursiv:
                        i = nd_open.content.find(opening)
                        opening_node = Node('TEXT', opening).with_pos(nd_open.pos + i)
                        set_state(opening_node, 'grade')
                        splitted = [
                            Node(nd_open.name, nd_open.content[:i]).with_pos(nd_open.pos),
                            opening_node,
                            Node(nd_open.name,
                                 nd_open.content[i + 1:]).with_pos(nd_close.pos + i + 1)]
                        splitted = tuple(nd for nd in splitted if nd.strlen() > 0)
                        i = ctx_open[-2].index(nd_open)
                        ctx_open[-2].result = ctx_open[-2].children[:i] \
                            + splitted + ctx_open[-2].children[i + 1:]

                    if ende_kursiv:
                        i = nd_close.content.find(closing)
                        closing_node = Node('TEXT', closing).with_pos(nd_close.pos + i)
                        set_state(closing_node, 'grade')
                        splitted = [
                            Node(nd_close.name, nd_close.content[:i]).with_pos(nd_close.pos),
                            closing_node,
                            Node(nd_close.name,
                                 nd_close.content[i + 1:]).with_pos(nd_close.pos + i + 1)]
                        splitted = tuple(nd for nd in splitted if nd.strlen() > 0)
                        i = ctx_close[-2].index(nd_close)
                        ctx_close[-2].result = ctx_close[-2].children[:i] \
                            + splitted + ctx_close[-2].children[i + 1:]
                else:
                    gleiche_Randklammern_an(anfang_kursiv, ende_kursiv, ctx_open, ctx_close)
                # elif anfang_kursiv:  # anfang_kursiv == ende_kursiv == True
                #     if len(nd_open.content.lstrip()) == 1:  set_state(ctx_open[-1], 'kursiv')
                #     if len(nd_close.content.rstrip()) == 1:  set_state(ctx_close[-1], 'kursiv')
                # else:  # anfang_kursiv == ende_kursiv == False
                #     if len(nd_open.content.lstrip()) == 1:  set_state(ctx_open[-1], 'grade')
                #     if len(nd_close.content.rstrip()) == 1:  set_state(ctx_close[-1], 'grade')


    def Klammern_fett(self, node):
        """Setzt Klammern fett, sofern öffnende und schließende Klammer mehr als
        125 Zeichen voneinander entfernt liegen. Diese Funktion sollte nach anderen
        Klammer-bearbeitungsfunktionen, die Klammern hinzufügen oder aneinanderstoßende
        Klammern auflösen und durch Semikolon ersetzen, aufgerufen werden."""
        content = node.content
        unmatched_round, unmatched_square = [], []
        round_brackets = matching_brackets(content, '(', ')', unmatched_round)
        square_brackets = matching_brackets(content, '[', ']', unmatched_square)
        brackets = round_brackets + square_brackets

        # if node.pick('a'):
        #     no_links = copy.deepcopy(node)
        #     for a in no_links.select('a'):
        #         a.result = ''
        #     no_links_content = no_links.content
        #     round_no_links = matching_brackets(no_links_content, '(', ')', [])
        #     square_no_links = matching_brackets(no_links_content, '[', ']', [])
        #     no_links_brackets = round_no_links + square_no_links

        cm = None
        for i in range(len(brackets)):
            a, b = brackets[i]
            if b - a < KLAMMERN_FETT_SCHWELLE:  continue
            if cm is None:  cm = ContentMapping(node)
            ctx_a, rel_a = cm.get_path_and_offset(a)
            ctx_b, rel_b = cm.get_path_and_offset(b)
            for ctx, r in ((ctx_a, rel_a), (ctx_b, rel_b)):
                nd = ctx[-1]
                if nd.strlen() == 1:
                    # if nd.name == 'TEXT':
                    #     nd.name = 'Fett'
                    # else:
                    #     nd.result = Node('Fett', nd.content)
                    # add_class(nd, 'fett')
                    nd.result = Node('Fett', nd.content).with_attr({'class': 'fett'})
                    if self.ist_kursiv(ctx):
                        add_class(nd, 'kursiv')
                else:
                    assert not nd.children
                    content = nd.content
                    davor = content[:r]
                    danach = content[r + 1:]
                    res = []
                    if davor:  res.append(Node('TEXT', davor).with_pos(nd.pos))
                    res.append(Node('Fett', content[r]).with_pos(nd.pos + len(davor)).with_attr({'class': 'fett'}))
                    if self.ist_kursiv(ctx):
                        add_class(res[-1], 'kursiv')
                    if danach:  res.append(Node('TEXT', danach).with_pos(nd.pos + len(davor) + 1))
                    nd.result = tuple(res)

    def EinschubKlammern(self, node):
        try:
            first_leaf_ctx = self.path[:-1]
            first_leaf_ctx.extend(next(node.select_path_if(
                lambda ctx: not ctx[-1].children, include_root=True)))
            first_leaf = first_leaf_ctx[-1]
            last_leaf_ctx = self.path[:-1]
            last_leaf_ctx.extend(next(node.select_path_if(
                lambda ctx: not ctx[-1].children, include_root=True, reverse=True)))
            last_leaf = last_leaf_ctx[-1]

            öffne, schließe = self.Einschub_Klammern()
            # # wird jetzt regelmäßig in on_BelegText() geprüft
            # if not pick_from_path(self.path[:-1], 'Einschub'):
            #     self.passe_innere_Klammern_an(node, öffne, schließe)

            if öffne == '(':
                for ctx in node.select_path('Autor', include_root=False):
                    an = ctx[-1].content
                    if an.find('(?)') >= 0 and not any(c.name == 'Einschub' for c in ctx[1:]):
                        ctx[-1].result = an.replace('(?)', '[?]')

            kursiv = (self.ist_kursiv(first_leaf_ctx), self.ist_kursiv(last_leaf_ctx))
            result = node.result


            def add_opening_bracket(kursiv: Tuple[bool, bool]):
                nonlocal result, öffne, schließe
                if first_leaf.name in {'a', 'HOCHGESTELLT', 'ROEMISCHE_ZAHL'} \
                        or pick_from_path(first_leaf_ctx, 'Sperrung'):
                    kursivierung = {'class': 'kursiv'} if kursiv[0] else {'class': 'grade'}
                    result = (Node('TEXT', öffne).with_attr(kursivierung),) + result
                else:
                    first_leaf.result = öffne + first_leaf.result

            def add_closing_bracket(kursiv: Tuple[bool, bool]):
                nonlocal result, öffne, schließe
                if last_leaf.name in {'a', 'HOCHGESTELLT', 'ROEMISCHE_ZAHL', 'GRI_WORT'} \
                        or pick_from_path(last_leaf_ctx, 'Sperrung'):
                    kursivierung = {'class': 'kursiv'} if kursiv[1] else {'class': 'grade'}
                    result = result + (Node('TEXT', schließe).with_attr(kursivierung),)
                else:
                    last_leaf.result = last_leaf.result + schließe

            if kursiv == (True, True) or kursiv == (False, False):
                add_opening_bracket(kursiv)
                add_closing_bracket(kursiv)
            else:
                # falls Anfang und Ende unterschiedlich kursiviert, verwende an beiden Enden grade Klammern
                if kursiv[0]:
                    result = (Node('TEXT', öffne).with_attr({'class': 'grade'}),) + result
                else:
                    add_opening_bracket((False, False))
                if kursiv[1]:
                    result = result + (Node('TEXT', schließe).with_attr({'class': 'grade'}),)
                else:
                    add_closing_bracket((False, False))
            if result != node.result:
                node.result = result
        except StopIteration:
            pass


def to_8bit_ascii(s: str) -> str:
    if s.isascii():
        return s
    return ''.join(ch if ord(ch) <= 255 else '?' for ch in s)


def deutsche_Zitate_ein(node: Node):
    """Umschließt alle ZITAT-Knoten innerhalb der unmittelbaren
    Kinder von `node` mit einem <DeutscherAusdruck>-tag.
    """
    def Zitat_Deutsch_markieren(nd: Node) -> Node:
        return Node('DeutscherAusdruck', nd) if nd.name == "ZITAT" else nd

    if node.children:
       node.result = tuple(Zitat_Deutsch_markieren(nd) for nd in node.children)
    return node


class AusgabeTransformation(Compiler, TextÄnderungenMixin):
    """Übersetzt die "reiche" MLW-Baumstruktur in eine Baumstruktur, die
    um relevante Informationen für die Druck- und Bildschirmausgabe, wie
    z.B. Trennzeichen, angereichert ist. Es handelt sich hierbei um eine
    "Ausgabe-Abstraktionsschicht", d.h. es werden keine Spezifika
    berücksichtigt, die nur für die Druck- oder nur für die Bildschirm-
    Ausgabe relevant sind.

    Sinnvollerweise sollte diese Transformation auf einer Kopie des
    semantischen MLW-Baums ausgeführt werden, da die Transformation am
    Ort ausgeführt wird und der übergebene Baum dabei verändert wird.
    """
    def __init__(self):
        super().__init__()

    def reset(self):
        super().reset()
        # self.aktueller_autor = ''
        self.ist_Fremdautor = False
        # self.aktuelles_werk = ''
        self.letzte_wortartanzeige = ''
        self.Angaben_letzter_Einschub: Optional[Tuple[bool]] = None

    def prepare(self, root: RootNode) -> None:
        # assert root.stage == "xml", f"Source stage `xml` expected, `but `{root.stage}` found."
        root.stage = "ausgabe.xml"

    def finalize(self, result: Any) -> RootNode:
        if isinstance(result, RootNode):
            return cast(RootNode, result)
        else:
            raise AssertionError(f'Compilation-result should be of type RootNode, not {type(result)}')

    def Absätze(self, node):
        """Wandelt Spatien zwischen LemmaPosition, ArtikelKopf und
        BedeutungsPosition in Absätze um, wenn die (vorhergehenden) Blöcke
        eine bestimte Länge überschreiten.
        Regeln:
        1. Wenn die Gesamtlänge der Lemma-Position und des Artikel-Kopfes 144
           zusammen Zeichen überschreiten, beginnt nach dem Artikelkopf ein
           neuer Absatz, andernfalls wird der Bedeutungsblock lediglich
           durch ein 4-fach Spatium abgetrennt.
        2. Für Sub-Lemmata gilt dieselbe Regel
        """
        def verwandle_spatium_in_Absatz(L: Node):
            assert L.name in ('L', 'LB', 'P')
            if L.name != 'P':
                L.name = 'LB'
                if L.has_attr('spatium'):
                    del L.attr['spatium']
        assert 'Artikel' in node.name
        if 'LemmaPosition' in node:
            lp = node.index('LemmaPosition')
        elif 'SubLemmaPosition' in node:
            lp = node.index('SubLemmaPosition')
        else:
            lp = None
        len_lp = node[lp].strlen() if lp is not None else 0
        if 'ArtikelKopf' in node:
            ak = node['ArtikelKopf']
            len_ak = ak.strlen() + len(list(ak.select('a'))) * 6
        else:
            len_ak = 0
        if 'EtymologiePosition' in node:
            etym = node['EtymologiePosition']
            len_etym = etym.strlen() + len(list(etym.select('a'))) * 6
        else:
            len_etym = 0
        if 'BedeutungsPosition' in node:
            bd = node.index('BedeutungsPosition')
        else:
            bd = None
        # if len_lp + len_ak + len_etym >= ABSATZ_SCHWELLE and bd is not None:
        # Etymologie wird laut Helena hier nicht mitgezählt, oder doch
        if len_lp + len_ak >= KOPF_SCHWELLE and bd is not None:
            verwandle_spatium_in_Absatz(node[bd - 1])

    # TODO: die attr_funktionen sollten besser in eine eigene DruckAusgabe-Transformation
    #       verschoben werden, da HTML anders als lxml mit Unicode-Attributwerten keine Probleme hat
    def attr_autor(self, node: Node, attr_value: str) -> Optional[Node]:
         node.attr['autor'] = to_8bit_ascii(attr_value)


    def attr_werk(self, node: Node, attr_value: str) -> Optional[Node]:
        node.attr['werk'] = to_8bit_ascii(attr_value)


    def attr_stelle(self, node: Node, attr_value: str) -> Optional[Node]:
        node.attr['stelle'] = to_8bit_ascii(attr_value)


    def on_Artikel(self, node):
        if not node.get_attr('stadium', 'abstrakter Syntaxbaum').endswith("semantischer Baum"):
            raise AssertionError('Ausgabe-Transformation erwartet das Stadium "semantischer Baum"'
                                 ' nicht das Stadium "{}"!'.format(node.attr['stadium']))
        # Präprozessor
        # Zu versteckende Elemente müssen vorher versteckt werden, da der Baum von der Wurzel
        # aufwärts abgearbeitet wird und die 'versteckt'-Eigenschaft der Kind-Elemente an einigen
        # Stellen abgefragt wird
        for sublemma in node.select('SubLemmaPosition'):
            for wortart in sublemma.select({'wortarten', 'wortart'}):
                add_class(wortart, 'versteckt')

        # Compiler
        node = self.fallback_compiler(node)

        # ggf. Absätze einfügen
        self.Absätze(node)

        if has_token_on_attr(node, 'unecht', 'hinweise'):
            lb = node.pick('LemmaBlock')
            self.davor(lb, '[')
            try:
                end = node.index('UnterArtikel')
            except ValueError:
                end = node.index('ArtikelVerfasser')
            while node[end - 1].name in ('L', 'p'):
                end -= 1
            self.unmittelbar_danach(node[end - 1], ']')
        node.attr['stadium'] += " -> Ausgabebaum"
        return node

    def on_UnterArtikel(self, node):
        node = self.fallback_compiler(node)
        self.Absätze(node)
        if has_token_on_attr(node, 'unecht', 'hinweise'):
            lb = node.pick('LemmaBlock')
            self.davor(node, '[')
            self.unmittelbar_danach(node, ']')
        return node

    def on_NachtragsPosition(self, node):
        node = self.fallback_compiler(node)
        inner_node = Node('Kursiv', node.result).with_pos(node.pos).with_attr({'class': 'kursiv'})
        node.result = (inner_node,)
        return node

    def on_NachtragsLemma(self, node):
        node = self.fallback_compiler(node)
        add_class(node, 'fett')
        add_class(node, 'grade')
        inner_node = Node('Grade', Node('Fett', node.result).with_pos(node.pos)).with_pos(node.pos)
        node.result = (inner_node,)
        return node

    def on_a(self, node):
        if node.content in ('⇒' + SEARCH_SYMBOL, '⇒x', '⇒?', '⇒xxx'):
            add_class(node, 'warning')
            node.attr['title'] = "Verweisziel nicht gefunden. Fehlender Anker?"
        node.attr['href'] = node.attr['href'].replace('.xml#', '.html#')
        node = self.fallback_compiler(node)
        href = node.attr['href']
        content = node.content
        if (not node.has_attr('alias')
            and (href == "-"
                 or (len(content) < 40
                     and not (content[0:4] == 'www.'
                              or content[0:5] == "http:"
                              or content[0:6] == "https:")))):
            node.attr['druck'] = "ja"
            # node.name = 'TEXT'
        return node

    def on_nicht_klassisch(self, node):
        node = self.fallback_compiler(node)
        node.attr['title'] = "sechszackig fett"
        return node

    def on_nicht_klassisch_verweis(self, node):
        node = self.fallback_compiler(node)
        node.attr['title'] = "sechszackig dünn"
        return node

    def on_nicht_gesichert(self, node):
        node = self.fallback_compiler(node)
        node.result = Node('Kursiv', node.result).with_attr({'class': 'kursiv'})
        return node

    def on_erschlossen(self, node):
        node = self.fallback_compiler(node)
        node.attr['title'] = "fünfzackig, Umriss"
        return node

    def add_vel_and_drop_dubplicate_lemma(self, node):
        """Füge das Schlüsselwort "vel." zwischen Lemmaalternativen ein."""
        assert node.name in {'LemmaPosition', 'SubLemmaPosition'}
        assert node.children
        result = []
        first_block_skipped = False
        last_child = EMPTY_NODE
        last_lemmastr = ''
        for child in node.result:
            if child.name == "LemmaBlock":
                if first_block_skipped:
                    vel_str = 'vel ' if last_child.content[-1:] == ' ' else ' vel '
                    result.append(Node('vel', vel_str))
                    li = child.index('Lemma')
                    lemmastr = child[li].content
                    if lemmastr == last_lemmastr:
                        k = (li + 2) if len(child.children) > li + 1 and child[li + 1].name == 'L' else li + 1
                        del child[li:k]
                    else:  last_lemmastr = lemmastr
                else:
                    first_block_skipped = True
                    last_lemmastr = child['Lemma'].content
            result.append(child)
            last_child = child
        node.result = tuple(result)
        return node

    def punkt_nach_LemmaPosition(self, node):
        visible_part = self.letzter_sichtbarer_Zweig(node)
        ende = visible_part.content.rstrip()[-2:]
        if ende[-1:] not in ('.', ',') and ende not in ['.)', '.]']:
            gp = visible_part.pick('GrammatikPosition')
            if (((gp and gp.content.strip()) or visible_part.pick('LemmaVarianten')
                 or node.name in ("Lemma", "SubLemmaPosition"))  # and visible_part[-1].name != "LemmaWort"
                    and not visible_part.pick('Beleg')):
                self.abschließender_Punkt(visible_part)
                return
        vp_path = self.path[:-1] + node.pick_path(lambda ctx: ctx[-1] is visible_part,
                                                           include_root=True)
        next_ctx = next_path(vp_path)
        if next_ctx and not next_ctx[-1].content.startswith(' '):
            self.unmittelbar_danach(visible_part, ' ')

    def on_AusgangsLemmata(self, node):
        node = self.fallback_compiler(node)
        for txt in node.select_children('TEXT'):
            if txt.content.strip() == ',':
                txt.name = 'Fett'
                add_class(txt, 'fett')
        return node

    def on_LemmaPosition(self, node):
        node = self.fallback_compiler(node)
        self.add_vel_and_drop_dubplicate_lemma(node)
        last_leafp = node.pick_path(LEAF_PATH, reverse=True)
        # if not pick_from_path(last_leafp, 'AlternativeGrammatik'):
        if node.content[-1:] != ')' and not node.pick('GrammatikPosition'):
            self.punkt_nach_LemmaPosition(node)
        return node

    def on_SubLemmaPosition(self, node):
        node = self.fallback_compiler(node)
        self.add_vel_and_drop_dubplicate_lemma(node)
        wortart = node.pick('wortarten') or node.pick('wortart')
        if wortart:
            if wortart.content != self.letzte_wortartanzeige:
                self.letzte_wortartanzeige = wortart.content
                former_result = node.result[1:] if node[0].name == 'L' else node.result
                node.result = (Node('WortartAnzeige', wortart.content).with_pos(node.pos),
                               Node('L', ' ').with_pos(node.pos + wortart.strlen()),
                               *former_result)
                # if node.get_attr('class', '').find('versteckt') >= 0:
                # assert not node.children
                wortart.result = ''
            elif has_class(wortart, 'versteckt'):
                wortart.result =''
        self.punkt_nach_LemmaPosition(node)
        return node

    def on_Lemma(self, node):
        node = self.fallback_compiler(node)
        if 'nicht_gesichert' in node and len(self.path) > 1:
            parent = self.path[-2]
            nicht_gesichert = node['nicht_gesichert']
            del node['nicht_gesichert']
            i = parent.index(node)
            parent.insert(i, nicht_gesichert)
        return node

    def on_LemmaBlock(self, node):
        node = self.fallback_compiler(node)
        gp = node.get('GrammatikPosition', None)
        if not gp:
            parent = self.path[-2]
            last_lb = next(parent.select_children('LemmaBlock', reverse=True))
            if node == last_lb:
                gp = parent.get('GrammatikPosition', None)
        if gp and not all(self.ist_unsichtbar(nd) for nd in gp.children):
            try:
                L = first(node['LemmaVarianten'])
            except KeyError:
                try:
                    L = first(node['Lemma'])
                except KeyError:
                    self.tree.new_error(node, "Lemma nicht gefunden. "
                                              "Vermutlich Folgefehler eines anderen Fehlers.")
                    L = Node('__dummy__', '')
            try:
                grammatik = gp['Grammatik']
                flx = grammatik.pick('flexion')
                if flx and 'indeclinabile' not in flx:  # 'flexion' in grammatik
                    self.danach(L, ',')
                else:
                    self.danach(L, ' ')  # "."?
            except KeyError:
                pass
        return node

    def on_unberechtigt(self, node):
        node = self.fallback_compiler(node)
        letzte_quellenangabe = node.pick('Quellenangabe', reverse=True)
        if letzte_quellenangabe:
            add_class(letzte_quellenangabe, 'kein_rechter_rand')
        add_class(node.pick_child(lambda nd: nd.content == '['), 'fett')
        add_class(node.pick_child(lambda nd: nd.content == ']', reverse=True), 'fett')
        verweis_block = node[-2]
        if verweis_block.name != ZOMBIE_TAG and not verweis_block.content[-1:] == '.':
            self.abschließender_Punkt(verweis_block)
            # verweis_block.result = (*verweis_block.children, Node('TEXT', '.'))
        return node

    def on_unsicher(self, node):
        node.result = (Node('Kursiv', node.content).with_attr({'class': 'kursiv'}).with_pos(node.pos),)
        return node

    def on_VerweisBlock(self, node):
        node = self.fallback_compiler(node)
        if len(self.path) >= 2:
            siblings = self.path[-2].children
            pos = 1
            while siblings[pos - 1] != node:
                assert pos <= len(siblings)
                pos += 1
            assert 0 <= pos <= len(siblings)
            while (pos < len(siblings)
                   and siblings[pos].name not in ('PraeterBlock', 'VerweisBlock')):
                pos += 1
            if pos < len(siblings) and siblings[pos].name == 'PraeterBlock':
                # kein abschließender Punkt bei nachfolgendem PraeterBlock
                return node
        self.abschließender_Punkt(node)
        if node.pick('BelegText') and not pick_from_path(self.path, 'unberechtigt'):
            node.result = (Node('TEXT', '['), *node.result, Node('TEXT', ']'))
            letzte_quellenangabe = node.pick('Quellenangabe', reverse=True)
            if letzte_quellenangabe:
                add_class(letzte_quellenangabe, 'kein_rechter_rand')
        return node

    def on_ZielLemmaBlock(self, node):
        node = self.fallback_compiler(node)
        try:
            beleg = node.get('Beleg', node.get('Belege', None))
            predecessor = as_list(node['ZielLemma'])[-1]
            i = node.index(predecessor)
            ensuing = content_of(node[i + 1:])
            if ensuing[0:1] != '.' and (not beleg or ensuing.lstrip()[:4] == 'adde') \
                    and node[-1].name != 'Zusatz' \
                    and content_of(node[i + 1:i + 2])[-1:] != '.'\
                    and node[i + 1].content[-1:] != '-':
                node.result = (*node.result[:i + 1], Node('TEXT', '.'), *node.result[i + 1:])
            non_L_pred = predecessor
            for child in node.children:
                if child == beleg:
                    break
                elif child.name in {"Zusatz", "VerweisErgänzung"} and predecessor.name == 'L' \
                        and child.content.startswith('adde'):
                    predecessor.attr['spatium'] = '3+1'
                    predecessor.result = '    '
                    predecessor = child
                    non_L_pred = child
                elif child.content:
                    predecessor = child
                    if child.name != 'L':
                        non_L_pred = child
            if beleg: self.unmittelbar_danach(predecessor, ':')
            if predecessor.name == 'TEXT' and predecessor.content.strip() == ':' \
                    and self.ist_kursiv([non_L_pred]):
                predecessor.name = 'Kursiv'
        except (KeyError, IndexError):
            pass
        return node

    def on_Vide(self, node):
        node = self.fallback_compiler(node)
        verweis = node.pick('a')
        if verweis and 'vol' in verweis.content:
            add_class(verweis, 'kursiv')
        return node

    def on_LemmaVarianten(self, node):
        node = self.fallback_compiler(node)
        # for i in range(len(node.children)-1):
        #     if node.children[i+1].name == "LemmaVariante":
        #         self.danach(node.children[i], ', ')
        #     else:
        #         self.danach(node.children[i], ' ')
        collect_result = [node.children[0]]
        for successor in node.children[1:]:
            if successor.name == "LemmaVariante":
                collect_result.append(Node('TEXT', ', '))
            else:
                collect_result.append(Node('L', ' '))
            collect_result.append(successor)
        node.result = tuple(collect_result)
        self.davor(node, '(')
        self.danach(node, ')')
        self.Klammer_Kursivierung(node)
        return node

    def verdichte_grammatikangabe(self, node):
        node = self.fallback_compiler(node)
        node.attr[node.name] = node.content
        node.result = node.attr['verdichtung'].replace('&lt;', '<')
        return node

    def on_casus(self, node):
        self.verdichte_grammatikangabe(node)
        add_class(node, 'kursiv')
        node.result = Node('Kursiv', node.result)
        return node

    def on_numerus(self, node):
        self.verdichte_grammatikangabe(node)
        add_class(node, 'kursiv')
        node.result = Node('Kursiv', node.result)
        return node

    def on_genus(self, node):
        return self.verdichte_grammatikangabe(node)

    def on_wortart(self, node):
        node = self.fallback_compiler(node)

        klasse = node.get_attr('klasse', "")
        if klasse:
            node.attr['title'] = "Klasse: " + klasse

        node.attr['wortart'] = node.content
        if node.has_attr('verdichtung') \
                and not tuple(nd for nd in node.select_if(lambda n: self.tree.node_errors(n),
                                                          include_root=True)):
                # don't delete node.result if there have been errors, so that errors will
                # still be shown in the HTML-preview
            if any(nd.name == "SubLemmaPosition" for nd in reversed(self.path[:-1])):
                node.result = node.attr['verdichtung'].replace('&lt;', '<')
            else:
                grammatik = None
                i = -2
                while grammatik is None and len(self.path) >= abs(i):
                    grammatik = self.path[i]
                    i -= 1
                flex = grammatik.pick('flexion') if grammatik else None
                if (not grammatik or grammatik.pick('numerus')
                    or (flex and not flex.pick('indeclinabile'))) \
                        and (not node.children or not node.pick('wa_ergänzung')):
                    # die Angabe einer Flexion macht die Ausgabe der Wortart überflüssig.
                    node.result = ''
                else:
                    # rette die Vollform in ein Attribut und ersetze für die
                    # Ausgabe die Vollform durch die verdichtete From im Inhalt des Knotens
                    node.attr['vollform'] = wa_content(node)
                    verdichtung = node.attr['verdichtung'].replace('&lt;', '<')
                    if node.children:
                        node[0].result = verdichtung
                    else:
                        try:
                            parent = self.path[-2]
                            i = parent.index(node)
                            if len(parent.children) - 1 > i \
                                    and content_of(parent[i + 1:])[:1] != ' ':
                                parent.insert(i + 1, Node('L', ' ').with_pos(parent[i + 1].pos))
                        except IndexError:
                            pass  # offensichtlich Test-Code ohne Elternelement
                        node.result = verdichtung
        # if content.startswith('littera'):
        if node.content:
            add_class(node, 'kursiv')
            node.result = Node('Kursiv', node.result)
            node.result[0]._pos = node._pos
        return node

    def on_indeclinabile(self, node):
        node = self.fallback_compiler(node)
        node.result = Node('Kursiv', node.result).with_attr({'class': 'kursiv'})
        return node

    def on_hic_non_tractatur(self, node):
        node = self.fallback_compiler(node)
        if not node.content.rstrip().endswith('.'):
            node.result = node.content + '.'
        add_class(node, 'fett')
        add_class(node, 'kursiv')
        return node

    def on_SchreibweisenPosition(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ' ')
        return node

    def on_StrukturPosition(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ' ')
        return node

    def on_GebrauchsPosition(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ' ')
        return node

    def on_MetrikPosition(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ' ')
        return node

    def on_VerwechselungsPosition(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ' ')
        return node

    def on_VerweisPosition(self, node):
        node = self.fallback_compiler(node)
        content = node.content
        # Trenne Verweise durch Komma
        # for child in node.children[:-1]:
        #     if child.name in ('Verweise', 'verwiesen_auf'):
        #         assert child.children
        #         child.result = (*child.children, Node('TEXT', ', '))
        for i in range(len(node.children) - 1):
            child = node.children[i]
            if child.name in ('Verweise', 'verwiesen_auf') \
                    and any(succ.name in ('Verweise', 'verwiesen_auf')
                            for succ in node.children[i + 1:]):
                assert child.children
                tr = ',' if node.children[i + 1].content[:1] == ' ' else ', '
                child.result = (*child.children, Node('TEXT', tr))
        # Füge abschließenden Punkt hinzu (wenn nicht schon vorhanden)
        if not content.endswith('.'):
            last_child = node.children[-1]
            if last_child.children:
                last_child.result = (*last_child.children, Node('TEXT', '.'))
            else:
                last_child.result = last_child.result + '.'
        # Füge Verweismarker "cf." hinzu (wenn nicht schon vorhanden)
        if not content.startswith('cf.'):
            node.result = (Node('Kursiv', 'cf. ').with_attr({'class': 'kursiv'}), *node.children)
        return node

    def on_BedeutungsPosition(self, node):
        node = self.fallback_compiler(node)
        erste_bedeutung = first(node['Bedeutung'])
        add_class(erste_bedeutung, 'kein_linker_rand')
        try:
            zähler = node.pick('Zähler').content
        except AttributeError:
            zähler = ''
        # if zähler == 'I':
        #     self.entzerre_große_Bedeutungsblöcke(node, 'I')
        # elif zähler == 'A':
        #     self.entzerre_große_Bedeutungsblöcke(node, 'A')
        self.unmittelbar_danach(node, ' ')
        return node

    def on_Autor(self, node):
        node = self.fallback_compiler(node)
        self.ist_Fremdautor = False
        return node

    def on_Autorangabe(self, node):
        # if not node.content.rstrip().endswith('.'):  # TODO: Is this necessary??
        if not node.has_attr('disambiguiert'):
            self.unmittelbar_danach(node, ',')
        if node.content.strip() == 'Lxx':  # Septuaginta-Sonderfall
            node.result = node.content.replace('Lxx', 'LXX')
        for ancestor in reversed(self.path):
            if ancestor.name == 'Quellenangabe':
                # Setze Quellenangabe kursiv, aber nicht den BelegText, der Teil
                # der Quellenangabe ist.
                self.ist_Fremdautor = True
                node.attr['fremdautor'] = '1'
                quelle = ancestor.pick_child('Quelle')
                if quelle:  add_class(quelle, 'kursiv')
                belegstelle = ancestor.pick_child('BelegStelle')
                if belegstelle:
                    stellenangabe = belegstelle.pick_child('Stellenangabe')
                    if stellenangabe:  add_class(stellenangabe, 'kursiv')
                break
            elif ancestor.name == "Einschub":
                break
        return node

    def Semikolon_vor_Ueberliergungstraeger(self, node):
        if len(self.path) > 1:
            parent = self.path[-2]
            i = parent.index(node)
            try:
                L = parent[i + 1]
                if L.name == 'L' and parent[i + 2].name == 'Ueberlieferungstraeger':
                    L.result = ';' + L.content
                    L.name = 'TEXT'
            except IndexError:
                pass

    def on_Werk(self, node):
        node = self.fallback_compiler(node)
        if self.ist_Fremdautor:
            for ancestor in reversed(self.path):
                if ancestor.name == 'Quellenangabe':
                    # Setze Werk kursiv, aber nicht den BelegText, der Teil
                    # der Quellenangabe ist.
                    add_class(node, 'kursiv')
                    belegstelle = ancestor.pick_child('BelegStelle')
                    if belegstelle:
                        stellenangabe = belegstelle.pick_child('Stellenangabe')
                        if stellenangabe:  add_class(stellenangabe, 'kursiv')
                    break
        self.Semikolon_vor_Ueberliergungstraeger(node)
        return node

    def on_Erscheinungsjahr(self, node):
        node = self.fallback_compiler(node)
        self.Semikolon_vor_Ueberliergungstraeger(node)
        return node

    def on_EtymologiePosition(self, node):
        #print(node.as_sxpr())
        node = self.fallback_compiler(node)
        self.passe_innere_Klammern_an(node, öffnende='(', schließende=')')
        for belegtext in node.select("BelegText"):
            # add_class(belegtext, "grade")
            if len(belegtext.children) == 1 and belegtext.children[0].name == "TEXT":
                belegtext.children[0].name = 'Grade'
            else:
                belegtext.result = Node('Grade', belegtext.result).with_attr({'class': 'grade'})
        node.result = (Node('TEXT', '('),) + node.result + (Node('TEXT', ')'),)
        self.Klammer_Kursivierung(node)
        self.unmittelbar_danach(node, ' ')
        etymologie_angaben = list(node.select_children('EtymologieAngabe'))
        # for angabe in etymologie_angaben[:-1]:
        if not etymologie_angaben:  return node   # Folgefehler vermeiden
        angabe = etymologie_angaben[0]
        for folge_angabe in etymologie_angaben[1:]:
            if 'Sekundärliteratur' in angabe \
                    and not self.ueberleitung('', folge_angabe):
                    # and not folge_angabe.content.startswith('et'):
                    # and not folge_angabe.children[0].name == "Zusatz":
                self.trenner_nach(self.path, node.index(angabe), ';')
            else:  # Sonderfall: Trenner schon da. Kursivierung zu prüfen
                i = node.index(angabe)
                succ = node.children[i + 1]
                if succ.content[:1] == ';' and succ.name == 'TEXT' \
                        and self.ist_kursiv(angabe.pick_path(LEAF_PATH, reverse=True)):
                    succ.name = "Kursiv"
            angabe = folge_angabe
        return node

    def on_EtymologieDetail(self, node):
        node = self.fallback_compiler(node)
        return node

    def on_SekundärliteraturBlock(self, node):
        node = self.fallback_compiler(node)
        try:
            k = node.index('Sekundärliteratur')
        except ValueError:
            k = -1
        while k >= 0:
            try:
                l = node.index('Sekundärliteratur', k + 1)
                j = l
                while node.children[j - 1].name in ('L', 'Zusatz'):
                    j -= 1
                spatium = node.children[j]
                if spatium.name == 'L':
                    first_leaf = node.children[l].pick(LEAF_NODE)
                    if first_leaf.name != "Zusatz" \
                            or first_leaf.content not in ('et', 'adde'):  # TODO: use self.ueberleitung()
                        spatium.name = 'TEXT'
                        spatium.result = '; '
                        ctx = self.path + node.children[k].pick_path(
                            LEAF_PATH, include_root=True, reverse=True)
                        if self.ist_kursiv(ctx):
                            add_class(spatium, 'kursiv')
                        else:
                            add_class(spatium, 'gerade')
                k = l
            except ValueError:
                k = -1
        return node

    def on_EtymologieAngabe(self, node):
        node = self.fallback_compiler(node)
        if node.has_attr('tllverweis'):
            del node.attr['tllverweis']
            return node
        try:
            ctx = node.pick_path('SekundärliteraturBlock')
            if not ctx:
                self.on_SekundärliteraturBlock(node)
                ctx = node.pick_path('Sekundärliteratur')
            k = node.index(ctx[1])
            try:
                i = node.index('Etymon')
                # etymon_flag = True
            except ValueError:
                try:
                    i = node.index('EtymologieSprache')
                except ValueError:
                    i = 0
                    assert node.children[0].name in {"Zusatz", "FesterZusatz"}, node.as_sxpr()
            tail = ''.join(child.content for child in node.children[i + 1:k]).lstrip()
            kopf = ''.join(child.content for child in node.children[:k])
            if not self.ueberleitung(kopf, node.children[k].content) \
                    and tail.rstrip()[-1:] not in ('[', '('):
                while node.children[k - 1].name == 'L':
                    k -= 1
                spatium = node.children[k]
                if spatium.name == 'L':
                    spatium.result = '; '
                    if self.ist_kursiv(prev_leaf_path(self.path + [node.children[k]])):
                        spatium.name = 'Kursiv'
                    else:
                        spatium.name = 'TEXT'
                else:
                    self.trenner_nach(self.path, k - 1, ";")

            # Klammerkursivierung bzw. Grade-Stellung
            ctx = self.path[:-1] + ctx
            i = ctx[-2].index('SekundärliteraturBlock')
            if 0 < i < len(ctx[-2].children) -1:
                klammer_auf = ctx[-2][i - 1]
                klammer_zu = ctx[-2][i + 1]
                if klammer_auf.content.strip() == "[":
                    assert klammer_zu.content.strip() == "]"
                anfang = pick_path(ctx, LEAF_PATH, include_root=True)
                ende = pick_path(ctx, LEAF_PATH, include_root=True, reverse=True)
                if not (self.ist_kursiv(anfang) and (self.ist_kursiv(ende))):
                    klammer_auf.name = "Grade"
                    remove_class(klammer_auf, 'kursiv')
                    add_class(klammer_auf, 'grade')
                    klammer_zu.name = "Grade"
                    remove_class(klammer_zu, 'kursiv')
                    add_class(klammer_zu, 'grade')

        except (ValueError, IndexError):
            # 'Sekundärliteratur' not in node!
            pass
        return node

    def Sekundaerliteratur_Semikola(self, node):
        """Setze Semikola zwischen aufeinanderfolgende Sekundärliteraturangaben,
        falls nicht schon vorhanden und falls keine Überleitung ("et")."""
        bi = node.indices('Beleg')
        L = len(node.children)
        for i in bi[:-1]:
            beleg_1 = node[i]
            if 'Sekundärliteratur' in beleg_1:
                k = i + 1
                while k < L and node[k].name == 'L':
                    k += 1
                if k >= L:
                    continue
                beleg_2 = node[k]
                if beleg_2.name == 'Beleg' and 'Sekundärliteratur' in beleg_2 \
                    and not self.ueberleitung(beleg_1.content, beleg_2.content):
                    self.unmittelbar_danach(beleg_1, ';')

    # def on_BelegStelle(self, node):
    #     stapel = self.ist_Fremdautor
    #     node = self.fallback_compiler(node)
    #     self.ist_Fremdautor = stapel
    #     return node

    def on_Belege(self, node):
        node = self.fallback_compiler(node)
        if any(nd.name == "Einschub" for nd in self.path):
            for quellenangabe in node.select('Quellenangabe'):
                add_class(quellenangabe, 'kein_rechter_rand')
            letzter_Beleg = node.pick_child('Beleg', reverse=True)
            for child in node.children:
                if child.name == "Beleg" and child != letzter_Beleg and \
                        not self.ueberleitung('', ensuing_str(self.path + [child], 40)):
                        # not ensuing_str(self.path + [child], 40).lstrip().startswith('et'):
                    self.unmittelbar_danach(child, '.')  # ';')
        self.Sekundaerliteratur_Semikola(node)
        return node

    def on_Beleg(self, node):
        def ergänze_Punkt():
            nonlocal self, node

            follow_up = next_path(self.path)
            while follow_up and follow_up[-1].name == 'L':
                follow_up = next_path(follow_up)
            # wenn Belege (nach einem Doppelpunkt) folgen,
            # sollte kein abschließender Punkt gesetzt werden

            keine_BelegStellen = True
            neue_quellenangabe = follow_up and follow_up[-1].pick('Quellenangabe')
            if not follow_up or follow_up[-1].name not in ('Beleg', 'Belege')\
                    or neue_quellenangabe:
                for bs_path in node.select_path('BelegStelle', skip_subtree='Einschub'):
                    bs = bs_path[-1]
                    # # suche nach ggf. vorhandener BelegErgänzung und setze den Punkt dahinter
                    # if len(bs_path) > 1:
                    #     parent = bs_path[-2]
                    #     i = parent.index(bs) + 1
                    #     while (i < len(parent.children)
                    #            and parent[i].name in ('L', 'BelegErgänzung')):
                    #         if parent[i].name == 'BelegErgänzung':  bs = parent[i]
                    #         i += 1
                    keine_BelegStellen = False
                    self.abschließender_Punkt(bs)
                if keine_BelegStellen:
                    self.abschließender_Punkt(node)

        def anknüpfung():
            nonlocal self, node
            if self.ueberleitung('', ensuing_str(self.path, 40)):
                # ensuing[:2] == 'et' or ensuing[:4] == 'adde':
                return True
            try:
                parent = self.path[-2]
                # content[-5:] == ' adde' or content[-3:] == ' et' \
                if self.ueberleitung(node, '') \
                        and parent.pick_child('Beleg', reverse=True) != self:
                    return True
            except IndexError:
                pass
            return False

        node = self.fallback_compiler(node)
        if not any(nd.name in ("Beleg", "GrammatikVarianten", "Bedeutungsangabe", "AlternativeGrammatik")
                   for nd in self.path[:-1]):
            einschub = pick_from_path(self.path, 'Einschub', reverse=True)
            if einschub:
                nd = einschub.children[-1]
                while nd != node and nd.children:
                    nd = nd.children[-1]
                if nd == node or not anknüpfung():
                    # Knoten steht am Ende eines Einschubs oder wird nicht von copula 'et' gefolgt!
                    ergänze_Punkt()
            else:
                if not anknüpfung():  ergänze_Punkt()
                self.Angaben_letzter_Einschub = None
        return node

    def on_Lemmawort(self, node):
        if 'verdichtung' in node.attr:
            node.result = node.attr['verdichtung'].replace('&lt;', '<') + node.get_attr('anhaengsel','')
        return node

    def on_Abkuerzung(self, node):
        node.attr['wort'] = node.content
        node.result = node.attr['verdichtung'].replace('&lt;', '<')
        return node

    def on_Stellenangabe(self, node):
        node = self.fallback_compiler(node)
        self.ersetze_angrenzende_Klammern(node)
        self.Klammer_Kursivierung(node)
        if self.ist_Fremdautor:
            node.attr['fremdautor'] = '1'
            if not has_class(node, 'kursiv'):
                for ancestor in reversed(self.path):
                    if ancestor.name == 'Quellenangabe':
                        add_class(node, 'kursiv')

        # Sonderfall 'tit.' für Titel, siehe Ticket: 379
        if node[0].name == 'Stelle' and node[0].children and node[0][0].name == 'HOCHGESTELLT' \
                and node[0][0].content == 'tit.':
            p = pick_path(self.path, lambda path: not path[-1].children and path[-1].name != 'L',
                          reverse=True)
            if pick_from_path(p, 'Werk', reverse=True):
                for q in select_path(p, lambda path: path[-1].name == 'L' or path[-1] == node):
                    if q[-1].name == 'L':
                        self.finalizers.append((eliminate_nodes, (q[-2], (q[-1],))))
                    if node in q:  break
                ca, i = find_common_ancestor(p, self.path)
                ca.attr['xml:space'] = 'preserve'  # xml will be inlined on output
            # node.result = node.result + (Node('L', ' ').with_pos(node.pos + node.strlen()),)

        return node

    def on_BelegLemma(self, node):
        return self.on_Lemmawort(node)

    def on_GrammatikPosition(self, node):
        node = self.fallback_compiler(node)
        for nd in reversed(self.path):
            if nd.name == 'LemmaPosition':
                break
        else:
            return node
        try:
            lemma_block = next(nd.select('LemmaBlock', reverse=True))
            if lemma_block.pos <= node.pos \
                    and not node.pick('GrammatikVarianten'):  # keine Punkte nach eingeklammerten Grammatik-Varianten!
                self.abschließender_Punkt(node)
        except StopIteration:
            pass
        # self.unmittelbar_danach(node, ' ')  # Leerzeichen wird schon nach LemmaBlock angehängt.
        return node

    def on_GrammatikVarianten(self, node):
        node = self.fallback_compiler(node)
        gv_num = [child.name for child in node.children].count('GrammatikVariante')
        gv_counter = 0
        new_result = []
        for child in node.children:
            if child.name == 'GrammatikVariante':
                first_leaf = child.pick(LEAF_NODE)
                last_leaf = child.pick(LEAF_NODE, reverse=True)
                if first_leaf.name == 'genus' and last_leaf.name in ('a', 'genus'):
                    tname = 'Kursiv'
                else:
                    tname = 'TEXT'
                if gv_counter == 0:
                    new_result.append(Node(tname, '('))
                new_result.append(child)
                gv_counter += 1
                if gv_counter < gv_num:
                    last_leaf = new_result[-1].pick(LEAF_NODE, reverse=True)
                    if last_leaf.content[-1:] == '.':
                        last_leaf.result = last_leaf.content[:-1] + ';'
                    else:
                        new_result.append(Node(tname, ';'))
                else:
                    new_result.append(Node(tname, ')'))
            else:
                new_result.append(child)
        node.result = tuple(new_result)
        # self.trenne_mit_tag(node, 'GrammatikVariante', ';')
        # varianten = [gv for gv in node.children if gv.name == "GrammatikVariante"]
        # if varianten:
        #     self.davor(varianten[0], '(')
        #     self.danach(varianten[-1], ').')
        self.Klammer_Kursivierung(node)
        return node

    def on_AlternativeGrammatik(self, node):
        node = self.fallback_compiler(node)
        assert node.children
        if self.ist_kursiv(self.path + [node.result[0]]) and \
                self.ist_kursiv(self.path + [node.result[-1]]):
            node.result = (Node('TEXT', '(').with_pos(node.pos).with_attr({'class': 'kursiv'}),
                           *node.result,
                           Node('TEXT', ')').with_pos(node.pos + node.strlen() + 1).with_attr({'class': 'kursiv'}))
        else:
            node.result = (Node('TEXT', '(').with_pos(node.pos),
                           *node.result,
                           Node('TEXT', ')').with_pos(node.pos + node.strlen() + 1))
        return node

    def on_GVariante(self, node):
        node = self.fallback_compiler(node)
        if len(self.path) >= 2:
            parent = self.path[-2]
            if 'Beleg' in parent or 'Belege' in parent:
                self.unmittelbar_danach(node, ': ')
        return node

    def on_flexion(self, node):
        node = self.fallback_compiler(node)
        node = self.trenne_mit_tag(node, 'FLEX', ', ')
        if node.content[-1:] != ' ':
            try:
                parent = self.path[-2]
                i = parent.index(node)
                if i < len(parent.children) - 1 and not parent.children[i+1].content.startswith(' '):
                    self.danach(node, ' ')
            except IndexError:
                pass  # happens only when debugging isolated flex-elements
        return node

    def ersetze_angrenzende_Klammern(self, common_ancestor):
        """Ersetze Fälle wie (usu liturg.)(de re v.) durch (usu liturg; de re v.)"""
        def geschütztes_Element(node: Node) -> bool:
            return node.name in ('Junktur', 'Erzwungene_Klammer')

        if common_ancestor.pick(ZOMBIE_TAG):
            # avoid endless loops
            return
        while True:
            content = common_ancestor.content
            try:
                m = next(RX_ADJACENT_BRACKTES.finditer(content))
            except StopIteration:
                break
            a, b = m.span()
            closing_ctx = common_ancestor.locate_path(a)[1:]
            opening_ctx = common_ancestor.locate_path(b - 1)[1:]
            if pick_from_path_if(opening_ctx, geschütztes_Element) \
                    or pick_from_path(closing_ctx, geschütztes_Element) \
                    or not closing_ctx or not opening_ctx:
                return
            closing_nd = closing_ctx[-1]
            opening_nd = opening_ctx[-1]
            if closing_nd is opening_nd:
                ctx = [common_ancestor] + closing_ctx
                i = split_node(closing_nd, ctx[-2], closing_nd.content.find(')') + 1)
                closing_nd = ctx[-2][i - 1]
                closing_ctx[-1] = closing_nd
                opening_nd = ctx[-2][i]
                opening_ctx[-1] = opening_nd
            # ersetze schließende Klammer durch Semikolon ";"
            s = closing_nd.result
            i = s.rfind(')')
            # if self.ueberleitung(closing_ctx[0].content.rstrip(' ').rstrip(')'),
            #                      opening_ctx[0].content.lstrip(' ').lstrip('(')):
            if self.ueberleitung(content[:a].rstrip(' ').rstrip(')'),
                                 content[b:].lstrip(' ').lstrip('(')) \
                    and not closing_nd.name == 'Datierung':  # Sonderfall Ticket #407
                delimiter = " " if b - a == 2 else ""
            else:
                delimiter = "; " if b - a == 2 else ";"
            k = i
            if not pick_from_path(closing_ctx, 'Verweise', reverse=True):
                # Beseitige vorhergehende Satzzeichen außer Fragezeichen und Punkt nach Verweisen.
                if delimiter.strip():  streichmenge = (',', ';')
                else:  streichmenge = ('.', ',', ';')
                while s[k-1:k] in streichmenge:
                    k -= 1
            closing_nd.result = s[0:k] + delimiter + s[i+1:]
            # Kursivierung nachführen:  (ACHTUNG: deckt noch nicht alle möglichen Fälle ab!!!!)
            ctx = [common_ancestor] + closing_ctx
            vorgaenger = ctx[-2][ctx[-2].index(closing_nd) - 1]
            vorgaenger_ctx = ctx[:-1] + [vorgaenger]
            if closing_nd.content.strip() == '':  closing_nd.name = 'L'
            if closing_nd.content[0:1] == ";":  # andernfalls ist die Kursivierung schon geklärt
                if self.ist_kursiv(vorgaenger_ctx):
                    if not self.ist_kursiv(ctx) and closing_nd.name == 'TEXT':
                        closing_nd.name = 'Kursiv'
                elif self.ist_kursiv(ctx) and closing_nd.name == 'TEXT':
                    closing_nd.name = 'Grade'
            # beseitige öffnende Klammer
            if opening_nd.result == "(":
                parent = common_ancestor.find_parent(opening_nd)
                parent.result = tuple(nd for nd in parent.children if nd != opening_nd)
            else:
                s = opening_nd.result
                i = s.find('(')
                opening_nd.result = s[0:i] + s[i + 1:]

    def überarbeite_Klammerung(self, node):
        """Setzt den Inhalt des Knotens in runde Klammern. Verwandelt runde
        Klammern innerhalb des Knotens in eckige. (ACHTUNG: Alle Klammern
        werden eckig, d.h. tiefere Klammerebenen werden nicht berücksichtigt.)
        """
        content = node.content.strip()
        if not (content.startswith('(') and content.endswith(')')):
            # Ersetze innere runde Klammern durch eckige Klammern
            for nd in node.select('TEXT'):
                nd.result = nd.result.replace('(', '[').replace(')', ']')
            # ergänze äußere runde Klammern
            node = self.ergänze(node, davor='(', danach=')')
            self.Klammer_Kursivierung(node)
        else:
            node = self.fallback_compiler(node)
        return node

    def on_Ergänzung(self, node):
        return self.überarbeite_Klammerung(node)

    def on_Beschreibung(self, node):
        if self.path[-2].name == "Besonderheit" and \
                (has_parent(self.path, {'Kategorie'})
                 or self.path[-2] != self.path[-3][0]):
            node.attr['class'] = "followup"
        node = self.fallback_compiler(node)
        self.ersetze_angrenzende_Klammern(node)
        return node

    def on_Sekundärliteratur(self, node):
        node = self.fallback_compiler(node)
        inner_node = Node('Kursiv', node.result).with_attr({'class': 'kursiv'})
        node.result = (inner_node,)
        return node

    def usu_Sonderfall(self, node):
        beschreibung = node.children[0]
        assert beschreibung.name == "Beschreibung"
        if node.has_attr('usu'):
            # usu-Sonderfall: Ersetze Spatium durch Semikolon
            assert beschreibung.content[0:4] == 'usu '
            beschreibung.result = beschreibung.content[4:]
            ctx = pick_path(self.path[-2:], 'L', include_root=False, reverse=True)
            assert ctx is not None and ctx[-1].name == 'L' and ctx[-1].has_attr('spatium')
            del ctx[-2][ctx[-2].index(ctx[-1])]
            ctx = pick_path(self.path[-2:], 'Verweise', include_root=False, reverse=True)
            # i = ctx[-2].index(ctx[-1]) + 1
            i = len(ctx[-2].children) -1
            if len(ctx[-2].children) > i \
                    and not ctx[-2][i].name.endswith('Zusatz') \
                    and ctx[-2][i].content[-1:] == '.':
                nd = ctx[-2][i]
                while nd.children:
                    nd = nd.children[-1]
                nd.result = nd.content[:-1] + '; '
                add_class(nd, 'kursiv')
                if nd.name == 'TEXT':  nd.name = 'Kursiv'
            else:
                self.trenner_nach(self.path[:-1], self.path[-2].index(node) - 1, ";")
        return node

    def on_Besonderheit(self, node):
        node = self.fallback_compiler(node)
        beschreibung = node.children[0]
        if len(node.children) > 1:
            i = 0
            for child in node.children[1:]:
                if child.name in ('Variante', 'Kategorie'):
                    i += 1
                if i > 1:
                    break
            if i != 1:  # or len(list(node.select('Unterkategorie'))) > 1:
                trenner = ':' if len(node.children) > 1 and node.children[1].name == 'L' else ': '
                self.danach(beschreibung, trenner)
            else:
                add_class(child, 'kein_linker_platz')
        else:
            self.abschließender_Punkt(node)
        node = self.usu_Sonderfall(node)
        return node

    def on_Kategorie(self, node):
        node = self.fallback_compiler(node)
        if len(node.children) > 1:
            beschreibung = node.children[0]
            i = 0
            for child in node.children[1:]:
                if child.name in ('Variante', 'Unterkategorie'):
                    i += 1
                    if i > 1:
                        break
            if i != 1:
                self.danach(beschreibung, ': ')
                # beschreibung.attr['danach'] = ': '
            else:
                add_class(child, 'kein_linker_platz')
        else:
            self.abschließender_Punkt(node)
        node = self.usu_Sonderfall(node)
        return node

    def on_Unterkategorie(self, node):
        return self.on_Kategorie(node)

    def on_Variante(self, node):
        node = self.fallback_compiler(node)
        # beschreibung = node['Beschreibung']
        # beschr_text = beschreibung.content.rstrip()
        # if beschr_text.endswith('.') or beschr_text.endswith('scribitur'):
        #     add_class(beschreibung, 'kursiv')
        quellenangaben = [qa for qa in node.select('Quellenangabe')]
        if quellenangaben:
            # Quellenangaben haben im CSS einen rechten Rand, der bei Einschüben stört!
            add_class(quellenangaben[0], 'kein_rechter_rand')
        beschreibung = node.result[0]
        assert beschreibung.name == 'Beschreibung', node.as_sxpr()
        if len(node.children) > 1:
            self.unmittelbar_danach(beschreibung, ': ')
        if node.content[-2:] not in ('.)', '.]'):
            self.abschließender_Punkt(node)
        node = self.usu_Sonderfall(node)
        return node

    def on_Nebenbedeutungen(self, node):
        node = self.überarbeite_Klammerung(node)
        # Füge Abgrenzungszeichen ";" zwischen mehreren Nebenbedeutungen ein,
        # sowie Abgrenzungszeichen ":" zwischen Nebenbedeutung und Verweisen
        # (falls vorhanden).
        trenn_regeln = {'Interpretamente': ';',
                        'Klassifikation': ';',
                        'Zusatz': ';',
                        'Einschub': ';',
                        'Belege': ';',
                        'Beleg': ';',
                        'NBVerweise': ':'}
        trenner = list(filter(lambda it: bool(it),
                              (trenn_regeln.get(child.name, '')
                               for child in node.children[::-1])))[:-1]
        result = []
        for i, child in enumerate(node.children):
            result.append(child)
            if child.name in trenn_regeln.keys():
                if trenner:
                    trennzeichen = trenner.pop()
                    if node.children[i + 1].content[:1] != ' ':  trennzeichen += ' '
                    pos = result[-1].pos + result[-1].strlen()
                    soll_kursiv =  self.ist_kursiv(
                        self.path + result[-1].pick_path(LEAF_PATH, reverse=True))
                    umgebung_kursiv = self.ist_kursiv(self.path)
                    result.append(self.baue_text_knoten(
                        trennzeichen, soll_kursiv, umgebung_kursiv).with_pos(pos))
        node.result = tuple(result)

        children = node.children
        for i, nd in enumerate(children):
            if nd.name == "TEXT" and i > 0 and nd.content.rstrip()[0:1] == ':':
                if not self.ist_kursiv(
                        self.path + children[i - 1].pick_path(LEAF_PATH, reverse=True)):
                    if nd.content.strip() == ':':
                        nd.name = "Grade"
                    else:
                        nd.result = nd.content[1:]
                        node.result = node.result[:i] + (Node('Grade', ':'),) + node.result[i:]
        return node

    def on_NBEinschub(self, node):
        node = self.fallback_compiler(node)
        self.EinschubKlammern(node)
        return node

    def on_Bedeutungsangabe(self, node):
        node = self.fallback_compiler(node)

        for ctx in node.select_path(LEAF_PATH):
            if ctx[-1].content.find('op. cit.') >= 0 \
                    and not any(nd.name == 'Sekundärliteratur' for nd in ctx)\
                    and (len(ctx) <= 1 or not ctx[-2].pick('Verweise')):
                if pick_from_path(ctx, 'Nebenbedeutungen'):
                    self.tree.new_error(ctx[-1], '"op. cit." wurde nicht als Literaturangabe erkannt! '
                        'Zur Unterscheidung von der Angabe einer Nebenbedeutung bitte einen Stern '
                        'an den Anfang des Einschubs setzen.',
                                        WARNUNG_FEHLENDER_EINSCHUB)
                else:
                    self.tree.new_error(ctx[-1], '"op. cit." wurde nicht als Literaturangabe erkannt! '
                        'Möglicherweise sollte hier ein Einschub stehen, wie z.B.: '
                        "(('Salzgraf', cf. op. cit. ({=> sal_22}); p. 1469sq.))",
                                        WARNUNG_FEHLENDER_EINSCHUB)

        self.ersetze_angrenzende_Klammern(node)
        nebenbedeutungen = node.get('Nebenbedeutungen', EMPTY_NODE)
        if nebenbedeutungen != EMPTY_NODE:
            self.Klammer_Kursivierung(nebenbedeutungen, self.path + [node])
        last_leaf_ctx = self.path + node.pick_path(
            lambda ctx: not ctx[-1].children, reverse=True)

        self.Angaben_letzter_Einschub = None
        if self.ist_kursiv(last_leaf_ctx):
            return self.danach(node, ':')
        else:
            # return self.danach(node, ':', 'grade')
            # Workaround: Druckausgabe:
            node = self.danach(node, ':', 'grade')
            last = node.pick(lambda nd: not nd.children and nd.content.rstrip()[-1:] == ":",
                             reverse=True)
            if last.name == "TEXT":  last.name = 'Grade'
            return node

    def on_Bedeutung(self, node):
        self.fallback_compiler(node)
        return node

    def on_U1Bedeutung(self, node):
        self.fallback_compiler(node)
        return node

    def on_U2Bedeutung(self, node):
        self.fallback_compiler(node)
        return node

    def on_U3Bedeutung(self, node):
        self.fallback_compiler(node)
        return node

    def on_U4Bedeutung(self, node):
        self.fallback_compiler(node)
        return node

    def on_U5Bedeutung(self, node):
        self.fallback_compiler(node)
        zähler = node.pick('Zähler')
        if zähler is not None and zähler.content.strip() in ZÄHLER[-1]:  zähler.result = ''
        return node

    def on_Klassifikation(self, node):
        node = self.fallback_compiler(node)
        deutsche_Zitate_ein(node)
        return node

    def on_Interpretamente(self, node):
        node = self.fallback_compiler(node)
        lat = node.pick('LateinischeBedeutung')
        deu = node.pick('DeutscheBedeutung')
        if lat is not None and deu is not None:
            i = node.index(deu)
            node.result = node.result[:i] + (Node('TEXT', ' — '),) + node.result[i:]
        # for ctx in node.select_path(
        #     lambda ctx: not ctx[-1].children and ctx[-1].content.find('i. q.') >= 0):
        #     if pick_from_path(ctx, 'Zusatz') is None:
        #         self.tree.new_error(
        #             ctx[-1], 'Zusätzliche Angaben ("... i. q.") sollten besser als Zusatz in '
        #             'geschweiften Klammern {...} geschrieben werden!', WARNING)
        return node

    def on_LateinischeBedeutung(self, node):
        node = self.fallback_compiler(node)
        return self.trenne(node, 'LateinischerAusdruck', ', ')

    def on_DeutscheBedeutung(self, node):
        node = self.fallback_compiler(node)
        return self.trenne(node, 'DeutscherAusdruck', ', ')

    def on_Exemplar(self, node):
        node = self.fallback_compiler(node)
        self.unmittelbar_danach(node, ':')
        return node

    def on_Zusatz(self, node):
        node = self.fallback_compiler(node)
        if '(' in node.content:
            öffnende, schließende = self.Einschub_Klammern()
            for nd in node.select_if(lambda nd: not nd.children and '(' in nd.content,
                                     include_root=True):
                nd.result = nd.content.replace('(', öffnende)
            for nd in node.select_if(lambda nd: not nd.children and ')' in nd.content,
                                     include_root=True):
                nd.result = nd.content.replace(')', schließende)
            deutsche_Zitate_ein(node)  # Annahme: Die meisten Zitate in Zusätzen sind deutsch
        self.Klammer_Kursivierung(node)

        # # Belegtexte auch im Druck gerade setzen
        # for bt in node.select('BelegText', skip_subtree='Zusatz'):
        #     bt.result = Node('Grade', bt.result).with_attr({'class': 'grade'}).with_pos(bt.pos)

        return node

    def on_Zusätze(self, node):
        node = self.fallback_compiler(node)
        self.Klammer_Kursivierung(node)
        return node

    def on_BelegText(self, node):
        node = self.fallback_compiler(node)
        in_Einschub, in_Bedeutungsangabe, in_Sekundärliteratur, in_BelegText = \
            False, False, False, False
        einschub_schachtelungen = 0
        for nd in self.path[:-1]:
            tname = nd.name
            if tname == 'Einschub':
                in_Einschub = True
                einschub_schachtelungen += 1
                einschub = nd
            in_Bedeutungsangabe = in_Bedeutungsangabe or tname == "Bedeutungsangabe"
            in_Sekundärliteratur = in_Sekundärliteratur or tname == "Sekundärliteratur"
            in_BelegText = in_BelegText or tname == "BelegText"

        self.ersetze_angrenzende_Klammern(node)
        if (in_Einschub, in_Bedeutungsangabe, in_Sekundärliteratur, in_BelegText) \
                == (False, False, False, False):
            self.passe_innere_Klammern_an(node, '[', ']')
        if in_Bedeutungsangabe or in_Sekundärliteratur:
            add_class(node, "grade")
            node.result = Node('Grade', node.result)
        if not in_BelegText:
            self.Klammer_Kursivierung_BelegText(node)
        if in_Bedeutungsangabe:
            add_class(node, 'kein_linker_rand')
        # self.Klammern_fett(node)
        return node

    def Einschub_Klammern(self):
        """Liefert für potentiell verschachtelte Einschübe ein passendes paar
        Klammern zurück, d.h. '(', ')' oder '[', ']'."""
        schachtelung = 0
        for nd in self.path[:-1]:
            if schachtelung == 0 and nd.name == 'BelegText': break
            if nd.name in ('Einschub', 'opus_minus'):
                schachtelung += 1
        return [('(', ')'), ('[', ']')][schachtelung % 2]
        # return [('(', ')'), ('[', ']'), ('<', '>')][schachtelung % 3]

    def passe_innere_Klammern_an(self, node: Node, öffnende: str, schließende: str):
        """Passt Klammern, z.B. innerhalb eines Einschubs, so an, dass immer wechselweise
        eckige und runde ineinander geschachtelt sind."""
        flip = {'[':'(', ']':')', '(':'[', ')':']'}
        ebene = 0

        cm = ContentMapping(node)
        i = 0
        m = RX_KLAMMER.search(cm.content, i)
        if not m: return ''
        s0, s2 = '', ''
        i0, i2 = -1, -1
        s = m.group(0)
        i = m.start(0)
        while m:
            m2 = RX_KLAMMER.search(cm.content, i + 1)
            if m2:
                s2 = m2.group(0)
                i2 = m2.start(0)
            else:
                s2 = ''
                i2 = -1
            if s in ('(', '['):
                ebene += 1
                if (s, s2) != ('[', ']'):
                    if ((s, s2) != ('(', ')')
                         or ((i == 0 or not re.match(r'(?!\d)\w', cm.content[i - 1]) and
                             (i2 == len(cm.content) - 1 or not re.match(r'(?!\d)\w', cm.content[i2 + 1])))
                         )):
                        s = öffnende if ebene % 2 == 0 else flip[öffnende]
            else:
                if (s0, s) != ('[', ']'):
                    if ((s0, s) != ('(', ')')
                         or ((i0 == 0 or not re.match(r'(?!\d)\w', cm.content[i0 - 1]) and
                             (i == len(cm.content) - 1 or not re.match(r'(?!\d)\w', cm.content[i + 1])))
                         )):
                        s = schließende if ebene % 2 == 0 else flip[schließende]
                ebene -= 1
                assert ebene >= 0, '\n' + cm.content + '\n' + ' ' * i + '^' + ' ' * (len(cm.content) - i - 1)

            path, k = cm.get_path_and_offset(i)
            if path[-1].name != 'Erzwungene_Klammer':
                txt = path[-1].content
                path[-1].result = txt[:k] + s + txt[k + 1:]

            s0 = s
            i0 = i
            m = m2
            s = s2
            i = i2

        assert ebene == 0, '\n' + cm.content + '\n' + ' ' * i + '^' + ' ' * (len(cm.content) - i - 1)

    def on_Einschub(self, node):
        stapel = self.ist_Fremdautor
        # if self.Angaben_letzter_Einschub:
        #     self.ist_Fremdautor = self.Angaben_letzter_Einschub[0]
        node = self.fallback_compiler(node)
        self.Angaben_letzter_Einschub = (self.ist_Fremdautor,)
        self.ist_Fremdautor = stapel
        # bestimme, a) ob Klammern rund, eckig oder spitz  und b) ob Klammern kursiv oder gerade

        # Quellenangaben haben im CSS einen rechten Rand, der bei Einschüben stört!
        letzte_quellenangabe = node.pick('Quellenangabe', reverse=True)
        if letzte_quellenangabe:
            add_class(letzte_quellenangabe, 'kein_rechter_rand')

        if pick_from_path(self.path, 'Bedeutungsangabe') \
                and not pick_from_path(self.path[:-1], 'Einschub'):
            # keine Klammern bei Einschüben erster Ebene innerhalb von Bedeutungsangaben!
            return node

        self.EinschubKlammern(node)
        return node

    def on_opus_minus(self, node):
        node = self.ergänze(node, *self.Einschub_Klammern(), allow_duplication=True)
        self.Klammer_Kursivierung(node, self.path)
        return node

    def on_Verweise(self, node):
        def Nebenbedeutungssonderfall(ctx: List[Node]) -> bool:
            """Falls Verweise am Ende von Nebenbedeutungen stehen, sollte kein Punkt
            gesetzt werden, da (später!) noch eine schließende Klammer gesetzt wird."""
            child = ctx[-1]
            i = -2
            while abs(i) <= len(ctx) and child == ctx[i].result[-1]:
                child = ctx[i]
                if child.name in ["Nebenbedeutungen", "NBVerweise", "GrammatikVarianten",
                                  "Einschub", "Klassifikation", "AlternativeGrammatik"]:
                    return True
                i -= 1
            return False

        def ausschließlich_Internetverweise(node: Node) -> bool:
            """Liefert `True`, falls unter den Verweisen keine Verweise auf andere
            Stellen im MLW vorkommen, sondern nur Internetverweise, die in der
            Druckausgabe nicht erscheinen."""
            for a in node.select('a'):
                if a.has_attr('globaleReferenz'):
                    return False
            return True

        node = self.fallback_compiler(node)
        if not ausschließlich_Internetverweise(node) \
                and not re.match(r'\w|\.|\)|\]|;|\s*et', ensuing_str(self.path, 40)) \
                and not Nebenbedeutungssonderfall(self.path)\
                and not pick_path(self.path, 'EtymologiePosition'): # prüfe: Knoten am Ende des Astes?
            self.abschließender_Punkt(node)  # Punkt nach Ende jeder Verweisgruppe

        # Verschiebe Punkt in das Eltern-Element:
        # Erforderlich, da Punkt sonst in der Druckausgabe verschwindet
        def verschiebe_Punkt(ctx):
            node = ctx[-1]
            punkt = node.children[-1]
            if punkt.name == "TEXT" and punkt.content[:1] == '.' and len(ctx) >= 2:
                parent = ctx[-2]
                i = parent.index(node)
                node.result = node.result[:-1]
                if (len(parent.result) <= i + 1) \
                        or (parent.result[i + 1].content[:1] not in SATZZEICHEN_ERWEITERT):
                    parent.result = parent.result[:i + 1] + (punkt,) + parent.result[i + 1:]

        verschiebe_Punkt(self.path)

        if pick_from_path(self.path, 'EtymologiePosition'):
            node.result = Node('Kursiv', node.result).with_pos(node.pos)
        # Kleiner Trick, um die Verweise im Druck erscheinen zu lassen:
        if any(a.get_attr('druck', '') for a in node.select('a')):
            node.name = "PseudoVerweise"
            for a in node.select('a'):
                a.name = 'TEXT'
                del a.attr['href']

        if pick_from_path(self.path, 'Besonderheit', reverse=True) \
                and not pick_from_path(self.path, 'Variante', reverse=True):
            add_class(node, 'kursiv')
        return node

    # def on_Sonderbelege(self, node):
    #     return self.ergänze(node, '(', ')')

    def on_Sperrung(self, node):
        """Vor und nach jeder Sperrung sollte ein festes doppeltes Spatium stehen."""
        # gutes Testbeispiel: saeculum.mlw
        node = self.fallback_compiler(node)
        prev_ctx = leaf_path(prev_path(self.path), LAST_CHILD)
        if prev_ctx:
            top = prev_ctx[-1]
            if top.get_attr('spatium', '1') == '1':
                top.result = top.result.rstrip()
                if top.result.rstrip():
                    # previous leaf contains text, and possibly trailing whitespace, e.g.
                    # (TEXT "quatenus de caduces rebus ") (Sperrung "presente") (TEXT " ")
                    parent = prev_ctx[-2]
                    i = parent.index(top)
                    parent.result = parent.result[:i + 1] \
                                    + (Node('L', '  ').with_attr(spatium='2'), ) \
                                    + parent.result[i + 1:]
                else:
                    # previous leaf contains only whitespace, e.g.
                    # (TEXT " ") (Sperrung "inauditum") (TEXT " ")
                    top.result = '  '
                    top.name = 'L'
                    top.attr['spatium'] = '2'
        next_ctx = leaf_path(next_path(self.path), FIRST_CHILD)
        if next_ctx:
            top = next_ctx[-1]
            if top.content[0:1] not in SATZZEICHEN_ERWEITERT and top.get_attr('spatium', '1') == '1':
                top.result = top.result.lstrip()
                if top.result.rstrip():
                    # following leaf contains trailing whitespace, e.g.
                    parent = next_ctx[-2]
                    i = parent.index(top)
                    parent.result = parent.result[:i] \
                                    + (Node('L', '  ').with_attr(spatium='2'),) \
                                    + parent.result[i:]
                else:
                    # following leaf contains only whitespace, e.g.
                    top.result = '  '
                    top.name = 'L'
                    top.attr['spatium'] = '2'
        return node

    def on_PraeterBlock(self, node):
        node = self.fallback_compiler(node)
        if node.content.strip() == 'praeter':
            # leerer Präter-Block
            praeter = node.pick(lambda nd: not nd.children and nd.content == 'praeter')
            praeter.result = praeter.content + ':'
        return node

    def VIDE_ET_PRAETER(self, node):
        node = self.fallback_compiler(node)
        node.name = 'Kursiv'
        return node

    def on_VIDE(self, node):
        return self.VIDE_ET_PRAETER(node)

    def on_ET(self, node):
        return self.VIDE_ET_PRAETER(node)

    def on_PRAETER(self, node):
        return self.VIDE_ET_PRAETER(node)

    def on_Kursiv(self, node):
        node = self.fallback_compiler(node)
        vorfahren_namen = tuple(nd.name for nd in self.path)
        kursive_tags =  ('Kursiv', 'Zusatz', 'Bedeutungsangabe', 'Sekundärliteratur')
        kursivierung = sum(vorfahren_namen.count(tag) for tag in kursive_tags)
        if 'Bedeutungsangabe' in vorfahren_namen and 'Zusatz' in vorfahren_namen:
            kursivierung -= 1
        if kursivierung % 2 == 0:
            node.name = "Grade"
        return node

    def on_MEHRZEILER(self, node):
        node.result = node.content.replace('<', '&lt;').replace('>', '&gt')
        return node

    def on_GRI_WORT(self, node):
        node = self.fallback_compiler(node)
        if self.ist_kursiv(self.path[:-1]) and any(nd.name == 'Kursiv' for nd in self.path[:2:-1]):
            # erlaube Kursivierung nur, wenn unmittelbar zuvor explizit erzwungen
            add_class(node, 'kursiv')
        else:
            # ansonsten stelle explizit grade!
            node.result = Node('Grade', node.result).with_pos(node.pos).with_attr({'class': 'grade'})
        return node

    def on_ZOMBIE__(self, node):
        node = self.fallback_compiler(node)
        return node


get_Ausgabe_Transformation = ThreadLocalSingletonFactory(AusgabeTransformation)


# def generiere_Ausgabe(semantischer_MLW_Baum: Node) -> Node:
#     # for nd in semantischer_MLW_Baum.select_if(lambda node: True, include_root=True):
#     #     if nd.attr:
#     #         print(nd.attr)
#     MLW_Baum_Kopie = copy.deepcopy(semantischer_MLW_Baum)
#     ausgabe_transformation = get_Ausgabe_Transformation()
#     ausgabe_baum = ausgabe_transformation(MLW_Baum_Kopie)
#     return ausgabe_baum

ausgabe: Junction = create_junction(
    AusgabeTransformation, "xml", "ausgabe.xml")


#######################################################################
#
# HTML-Transformation
#
#######################################################################

class HTMLTransformation(Compiler, TextÄnderungenMixin):
    """Übersetzt die "reiche" MLW-Baumstruktur nach HTML. Sinnvollerweise
    sollte diese Transformation auf einer Kopie des semantischen MLW-Baums
    ausgeführt werden, da der übergebene Baum durch die Transformation
    verändert wird. Der HTML-Code kann nach der Transformation
    mit result.ax_xml() ausgelesen werden.
    """
    def __init__(self):
        super().__init__()
        self.inline_tags = INLINE_ELEMENTS
        self.empty_tags = set()  # {'Anker', 'NullVerweis', 'klassisch', 'gesichert'}
        self.string_tags = OMITTED_TAGS

    def __call__(self, root):
        result = super().__call__(root)
        self.tree.inline_tags = self.inline_tags
        self.tree.empty_tags = self.empty_tags
        self.tree.string_tags = self.string_tags
        return result

    def reset(self):
        super().reset()

    def prepare(self, root: RootNode) -> None:
        # assert root.stage == "ausgabe.xml", f"Source stage `xml` expected, `but `{root.stage}` found."
        root.stage = "html"

    def finalize(self, result: Any) -> RootNode:
        if self.tree.errors:
            for node in self.tree.select(lambda nd: True, include_root=True):
                ident = id(node)
                if ident in self.tree.error_nodes \
                        and not (has_class(node, 'warning') or has_class(node, 'error')):
                    error_list = self.tree.error_nodes[ident]
                    if has_errors(error_list, ERROR):
                        node.attr['davor'] = ' ' + ERROR_SYMBOL + ' '
                        add_class(node, 'error')
                    elif has_errors(error_list, WARNING):
                        node.attr['davor'] = ' ' + WARN_SYMBOL + ' '
                        add_class(node, 'warning')
                    elif has_errors(error_list, NOTICE):
                        add_class(node, 'notice')
                    add_errors_to_titleattr(node, error_list)
        if isinstance(result, RootNode):
            return cast(RootNode, result)
        else:
            raise AssertionError(f'Compilation-result should be of type RootNode, not {type(result)}')

    def on_Artikel(self, node):
        if not node.get_attr('stadium', 'abstrakter Syntaxbaum').endswith("Ausgabebaum"):
            raise AssertionError('HTML-Transformation erwartet das Stadium "Ausgabebaum" '
                                 'nicht das Stadium "{}"!'.format(node.attr['stadium']))
        node = self.fallback_compiler(node)
        node.attr['stadium'] += " -> HTML-Baum"
        root = self.path[0]
        if root.errors:
            ecolor = lambda error: 'color:Red' if is_error(error) else 'color:DarkOrange'
            err_list = Node('ol', tuple(Node('li', str(error)).with_attr(style=ecolor(error))
                                        for error in root.errors_sorted))
            # err_list.attr['style'] = 'color:red;'
            if has_errors(root.errors):
                err_heading = Node('h3', 'Fehler').with_attr(style='color:red;')
            else:
                err_heading = Node('h3', 'Warnungen und Hinweise').with_attr(style='color:DarkOrange;')
            node.result = node.result + (Node('Errors', (err_heading, err_list)),)
        return node

    def on_L(self, node):
        if node.has_attr('spatium'):
            add_class(node, 'spatium_' + node.attr['spatium'].replace('+', '_'))
        return node

    def on_VerweisBlock(self, node):
        node = self.fallback_compiler(node)
        if "VERBORGEN" in node:
            del node['VERBORGEN']
            add_class(node, 'verborgen')
        return node

    def on_Verweise(self, node):
        node = self.fallback_compiler(node)

        try:
            parent = self.path[-2]
        except IndexError:
            # es handelt sich um Testcode, der mit einem "Verweise"-Knoten
            # ohne Eltern startet
            return node

        i = parent.index(node)
        tail = parent.children[i + 1:]
        if tail and not tail[0].children and tail[0].result and tail[0].result[0] in ",;.:?":
            satzzeichen = tail[0].result[0]
            tail[0].result = tail[0].result[1:]
            if not tail[0].result:
                parent.result = parent.result[:i + 1] + tail[1:]
            node.result = node.result + (Node('TEXT', satzzeichen, True),)
        return node

    def on_a(self, node):
        node = self.fallback_compiler(node)
        node.attr['href'] = node.attr['href'].replace('%38', '&')
        if node.has_attr('alias') and not node.content:
            node.result = node.attr['alias']
        if node.content.find(ERROR_SYMBOL) >= 0:
            add_class(node, 'warning')
        return node

    def on_Quellenangabe(self, node):
        node = self.fallback_compiler(node)
        # FEHLERHAFTER ALGORITHMUS:
        # autorwerk = node.attr['autor'] + ' ' + node.attr['werk']
        # for stelle in node.select('Stelle'):
        #     stelle.attr['title'] = autorwerk
        return node

    def on_BelegLemma(self, node):
        node = self.fallback_compiler(node)
        if node.has_attr('verdichtung') and node.has_attr('lemma') and not node.has_attr('title'):
            node.attr['title'] = node.attr['lemma']
        return node

    def on_ZOMBIE__(self, node):
        if node == self.path[0]:
            node.name = "ROOT"
            return node
        content = re.sub(r'\s*\n\s*\n', '\n', node.content)
        content = re.sub(r'^\s*\n', '', content)
        content = re.sub(r'\n\s*$', '', content).strip(' ')
        node.result = content
        if content.strip():
            # self.davor(node, ERROR_SYMBOL + ' ')
            # self.danach(node, ' ' + ERROR_SYMBOL)
            node.attr['davor'] = ERROR_SYMBOL + ' '
            node.attr['danach'] = ' ' + ERROR_SYMBOL
            errors = self.tree.node_errors(node)
            errors.sort(key=lambda err: -err.code)
            if errors:
                add_errors_to_titleattr(node, errors)
            else:
                node.attr['title'] = "Aufgrund eines Fehlers überprungener Abschnitt"
        return node

    def on_Sperrung(self, node):
        node = self.fallback_compiler(node)
        if DEBUG and len(self.path) <= 1:
            print(self.path[0].as_sxpr())

        level, succ = -1, None
        while succ is None:
            level -= 1
            try:
                i = self.path[level].index(self.path[level + 1])
            except IndexError:
                return node  # # already at root level (should never happen outside tests)
            if len(self.path[level].children) - 1 > i:
                succ = next(self.path[level][i + 1].select_if(lambda nd: not nd.children,
                                                                 include_root=True))
            elif abs(level) >= len(self.path):
                return node  # already at root level (should never happen outside tests)

        last = next(node.select_if(lambda nd: not nd.children, include_root=True, reverse=True))
        if succ.content[0:1] in ",.;:":
            last_letter = last.result[-1:]
            last.result = last.result[:-1]
            succ.result = last_letter + succ.result
        return node

    def on_Abkuerzung(self, node):
        if not node.has_attr('title'):
            node.attr['title'] = node.attr['wort']
        return node

    def on_Junktur(self, node):
        return self.ergänze(node, '˻', '˼')

    def on_Stelle(self, node):
        node = self.fallback_compiler(node)
        angabe = ' '.join(node.get_attr(attr, '') for attr in ('autor', 'werk', 'jahr', 'edition', 'stelle')
                          if node.has_attr(attr))
        if angabe.strip():
            node.attr['title'] = angabe
        return node

    def on_Lemmawort(self, node):
        anhaengsel = node.get_attr('anhaengsel', '')
        if anhaengsel:
            if 'verdichtung' in node.attr:
                lemma = node.attr['verdichtung'].replace('&lt;', '<')
            else:
                lemma = node.attr['lemma']
            node.result = (Node('Lemmawort', lemma), Node('span', anhaengsel).with_attr(style="color:dimgrey;"))
        node.name = "BelegLemma"
        return node

    def on_BelegLemma(self, node):
        return self.on_Lemmawort(node)

    def on_BelegText(self, node):
        node = self.fallback_compiler(node)
        self.Klammern_fett(node)
        return node



get_HTML_Transformation = ThreadLocalSingletonFactory(HTMLTransformation)

html_ausgabe: Junction = create_junction(
    HTMLTransformation, "ausgabe.xml", "html")


# def generiere_HTML(ausgabebaum: Node) -> Node:
#     # for nd in semantischer_MLW_Baum.select_if(lambda node: True, include_root=True):
#     #     if nd.attr:
#     #         print(nd.attr)
#     MLW_Baum_Kopie = copy.deepcopy(ausgabebaum)
#     html_transformation = get_HTML_Transformation()
#     html_baum = html_transformation(MLW_Baum_Kopie)
#     return html_baum


HTML_LEAD_IN = """<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>{title}</title>
<style>
{style}
</style>
<link id="pagestyle" rel="stylesheet" href="MLW_kompakt.css" />
</head>

<body>
<p>
<input type="button" value="Übersichtlich" onClick="document.getElementById('pagestyle').setAttribute('href', 'MLW.css');"/>
<input type="button" value="Kompakt" onClick="document.getElementById('pagestyle').setAttribute('href', 'MLW_kompakt.css');" />
<input type="button" value="Disposition" onClick="document.getElementById('pagestyle').setAttribute('href', 'MLW_disposition.css');" />
<span style="font-weight:bold;">""" + str(MLWVersion.VERSION) + '</span> Opera-Daten vom: ' + daten_vom + """
</p><hr/>
"""

HTML_LEAD_OUT = """
<script src="MLW.js" type="text/javascript" async></script>

<br/>

<p style="font-size:80%">Acknowledgement: This webpage uses the 
<a href="https://fonts.google.com/specimen/Cardo">Cardo-Font</a> 
for classicists and medievalists by 
<a href="https://fonts.adobe.com/designers/david-j-perry">David J. Perry</a>, 
licensed unter the <a href="OFL.txt">Open Font License</a>.</p>
</body>
</html>
"""


def schreibe_html_seite(file_name, html_snipped, show=False, mit_css=False) -> str:
    """Schreibt für die .mlw-Datei `file_name`, die in den HTML-Schnipsel
    `html_snipped` übersetzt wurde eine vollständige HTML-Datei, die
    im Browser angezeigt werden kann. Der Pfad der Ausgabedatei wird
    zurück gegeben."""
    dir_name = os.path.dirname(file_name)
    base_name = os.path.basename(file_name)
    if file_name.endswith('.ini'):
        out_dir = os.path.join(dir_name, "AUSGABE_TEST")
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
    else:
        out_dir = os.path.join(dir_name, VORSCHAU_UVZ)

    def copy_files(src_dir: str, files: List[str], copy_if_exists=False):
        for file in files:
            if copy_if_exists or not os.path.exists(os.path.join(out_dir, file)):
                shutil.copy(os.path.join(scriptdir, src_dir, file), out_dir or '.')

    copy_files('CSS', ['MLW.css', 'MLW_kompakt.css', 'MLW_disposition.css'], copy_if_exists=True)
    copy_files('Javascript', ['MLW.js'], copy_if_exists=True)
    copy_files('CSS', ['Cardo-Bold.ttf', 'Cardo-Italic.ttf', 'Cardo-Regular.ttf',
                       'linbiolinum_r-w.woff2', 'linbiolinum_rb-w.woff2',
                       'linbiolinum_ri-w.woff2', 'OFL.txt'], copy_if_exists=False)

    file_path = os.path.join(out_dir, base_name)
    out_name = os.path.splitext(file_path)[0] + '.html'
    if mit_css:
        with open(os.path.join(scriptdir, 'CSS', 'MLW_kompakt.css'), 'r') as f:
            css = f.read()
    else:
        css = ""
    with open(out_name, 'w', encoding="utf-8") as f:
        f.write(HTML_LEAD_IN.format(title=os.path.splitext(base_name)[0], style=css))
        f.write(html_snipped)
        f.write(HTML_LEAD_OUT)
    return out_name


def erzeuge_artikel_strecke(verzeichnis: str, html_bäume: List[RootNode]) -> str:
    """Vereint die HTML-Schnipsel einer ganzen Artikel-Strecke zu einer Datei
    und gibt deren Dateinamen zurück.
    """
    datei_name = os.path.basename(verzeichnis.rstrip('/').rstrip('\\')) + 'Strecke.html'
    datei_pfad = os.path.join(verzeichnis, VORSCHAU_UVZ, datei_name)
    html = [HTML_LEAD_IN.format(title=os.path.splitext(datei_name)[0], style='')]
    ziele: Dict[str, List[Node]] = {}
    verweise: Dict[str, List[str]] = {}
    for baum in html_bäume:
        for ctx in baum.select_path(ANY_PATH, include_root=True):
            node = ctx[-1]
            if node.has_attr('globaleId'):
                globaleId = node.attr['globaleId']
                node.attr['id'] = globaleId
                ziele[globaleId] = ctx[-2:]
            if node.has_attr('globaleReferenz'):
                globaleReferenz = node.attr['globaleReferenz']
                node.attr['href'] = '#' + globaleReferenz
                verweis_liste = verweise.setdefault(globaleReferenz, [])
                zielmarke = node.get_attr('id', globaleReferenz + '_' + str(len(verweis_liste) + 1))
                verweis_liste.append(zielmarke)
                if not node.has_attr('id'):  node.attr['id'] = zielmarke
        html.append(baum.as_xml())
    html.append('<hr/>')
    html.append('<h3>Alle Anker (TEST)</h3>')
    html.append('<ol>')
    for anker, umgebung_ctx in ziele.items():
        if not umgebung_ctx[-1].has_attr('anker'):
            umgebung = umgebung_ctx[-2]
            umgebung_ctx[-1].attr['style'] = 'background-color: yellow;'
            umgebung_str = umgebung.as_xml(indentation=0, inline_tags={umgebung.name}, string_tags={'L', 'TEXT'})
            umgebung_ctx[-1].attr['style'] = ''
            unbenutzt = "UNBENUTZT! " if anker not in verweise else ''
            html.append(f'<li>{unbenutzt}<a href="#{anker}">{anker}</a>: {umgebung_str}</li>')
    html.append('</ol><br />')
    html.append('<h3>Alle Verweise (TEST)</h3>')
    html.append('<ol>')
    for ziel, verweis_liste in verweise.items():
        alist = []
        for verweis in verweis_liste:
            alist.append(f'<a href="#{verweis}">{verweis}</a>')
        verweisangabe = ', '.join(alist) # if alist else '<span>KEINE VERWEISE ZU DIESEM ZIEL</span>'
        html.append(f'<li><a href="#{ziel}">{ziel}</a> &lt;== {verweisangabe}</li>')
    html.append('</ol><br />')
    html.append(HTML_LEAD_OUT)
    with open(datei_pfad, 'w', encoding='utf-8') as f:
        f.write('\n'.join(html))
    return datei_pfad


#######################################################################
#
# Druck-Transformation
#
#######################################################################

class DruckTransformation(Compiler, TextÄnderungenMixin):
    """Ausgabe-Transformationen, die spezifisch für die Druckausgabe
    sind, aber nicht ins HTML einfließen sollten."""

    def reset(self):
        super().reset()

    def prepare(self, root: RootNode) -> None:
        assert root.stage == "ausgabe.xml", f"Source stage `xml` expected, `but `{root.stage}` found."
        root.stage = "ausgabe.druck.xml"

    def on_Kursiv(self, node):
        node = self.fallback_compiler(node)
        add_class(node, 'kursiv')
        return node

    def on_Grade(self, node):
        node = self.fallback_compiler(node)
        add_class(node, 'grade')
        return node

    def on_TEXT(self, node):
        if has_class(node, 'grade'):
            node.name = 'Grade'
        elif has_class(node, 'kursiv'):
            node.name = 'Kursiv'
        return node

    def on_Beleg(self, node):
        node = self.fallback_compiler(node)
        # Ticket 334: Korrigiere optische Täuschung
        ending_path = node.pick_path(LEAF_PATH, reverse=True)
        ending = ending_path[-1]
        full_path = self.path + ending_path[1:]
        ensuing = pick_path(full_path, LEAF_PATH)
        seq = ensuing[-1].content[0:1] if ensuing else ''
        if ending and ending.content[-1:] == "’" and seq not in ('.', ',', ';', ':', ')', ']'):
            ending.result = ending.content + ' '
        return node

    def on_wortart(self, node):
        node = self.fallback_compiler(node)
        if has_class(node, 'versteckt'):
            node.attr['versteckt'] = node.content
            node.result = ''
        return node

    def on_hic_non_tractatur(self, node):
        node = self.fallback_compiler(node)
        node.result = Node('Fett', node.result).with_attr({'class': 'fett'})
        node.result = Node('Kursiv', node.result).with_attr({'class': 'kursiv'})
        return node

    def attr_versteckt(self, node, value):
        self.path[-2].result = tuple(nd for nd in self.path[-2].result if nd != node)
        return None

    def ist_internet_verweis(self, a):
        href = a.get_attr('href', '')
        return href.startswith('https://') or href.startswith('http://') or href.startswith('www.')

    def on_a(self, node):
        # TODO: Entfernen sobald Links im Druck erscheinen sollen!
        node = self.fallback_compiler(node)
        # if node.content[:4] in ('http', 'www.'):
        #     # del node.attr['href']
        #     node.name = 'TEXT'
        # # print(node.as_sxpr())
        if self.ist_internet_verweis(node):
            artikel = pick_from_path(self.path, 'Artikel')
            if artikel and 'NachtragsPosition' in artikel:
                # Zeige Internet-Links vorerst nur in der NachtragsPosition an!
                node.name = 'Text'
            else:
                node.result = ''
        return node

    def on_Verweise(self, node):
        if len(self.path) <= 1: # just in case a shallow element is being debugged
            return self.fallback_compiler(node)

        # Fremde Elemente wie z.B. Zusätze sollten für die Druckausgabe nicht in einem
        # "Verweise"-Block stehen:

        children = node.children
        siblings = []
        nd = node
        i = 0
        L = len(children)
        while i < L:
            package = []
            while i < L and children[i].name == 'L':  # leading whitespace
                package.append(children[i])
                i += 1
            if package:
                siblings.extend(package)
                package = []
            while i < L and children[i].name in ('a', 'L'):
                package.append(children[i])
                i += 1
            if package:
                while package[-1].name == 'L':  # trailing whitespace
                    siblings.append(package.pop())
                nd.result = tuple(package)
                siblings.append(nd)
                package = []
            while i < L and children[i].name not in ('a', 'L'):
                package.append(children[i])
                i += 1
            if package:
                siblings.extend(package)
            if i < L:
                nd = Node('Verweise', ())
        if len(siblings) > 1:
            parent = self.path[-2]
            i = parent.index(node)
            parent.result = parent.result[:i] + tuple(siblings) + parent.result[i + 1:]

        if all(self.ist_internet_verweis(a) for a in node.select('a')):
            # Da Internet-Verweise in der Druck-Ausgabe nicht angezeigt werden,
            # sollte das vorhergehende Leerzeichen gelöscht werden
            i = self.path[-2].index(node)
            if i > 0:
                pred = self.path[-2][i - 1].pick(LEAF_NODE, include_root=True, reverse=True)
                pred.result = pred.content.rstrip()
            # self.path[-2].result = tuple(nd for nd in self.path[-2].result if nd != node)
            # merge_adjacent(self.path[:-1], is_one_of('TEXT'))
        self.fallback_compiler(node)
        if node.children and node.children[-1].name == 'L' and len(self.path) > 1:
            parent = self.path[-2]
            i = parent.index(node)
            parent.result = parent.result[:i + 1] + (node.children[-1],) + parent.result[i + 1:]
            node.result = node.children[:-1]
        return node

    def on_PseudoVerweise(self, node):
        node = self.fallback_compiler(node)
        for text in node.select('TEXT'):
            text.name = "Kursiv"
            add_class(text, 'kursiv')
        return node

    def on_BelegText(self, node):
        node = self.fallback_compiler(node)
        self.Klammern_fett(node)
        return node

    def erzwinge_Kursivierung(self, node):
        """Erzwinge, wenn nötig, die Kursivierung."""
        node = self.fallback_compiler(node)
        if self.ist_kursiv(self.path):
            add_class(node, 'kursiv')
            if len(node.children) != 1 or node[0].name != 'Kursiv':
                node.result = (Node('Kursiv', node.result).with_pos(node.pos),)
        return node

    def on_ROEMISCHE_ZAHL(self, node):
        return self.erzwinge_Kursivierung(node)

    def on_HOCHGESTELLT(self, node):
        return self.erzwinge_Kursivierung(node)

    def on_AUFLAGE(self, node):
        node.name = "HOCHGESTELLT"
        return self.erzwinge_Kursivierung(node)

    def on_TIEFGESTELLT(self, node):
        return self.erzwinge_Kursivierung(node)

    def on_Zusatz(self, node):
        node = self.fallback_compiler(node)
        for pth in node.select_path('TEXT'):
            if self.ist_kursiv(self.path[:-1] + pth):
                nd = pth[-1]
                nd.name = 'Kursiv'
                add_class(nd, 'kursiv')
        return node

    def on_AddeAngabe(self, node):
        node = self.fallback_compiler(node)
        node.result = Node('Kursiv', node.result).with_pos(node.pos)
        return node

    def on_VerweisArtikel(self, node):
        node = self.fallback_compiler(node)

        drop = set()
        for i in range(len(node.children)):
            if (node[i].name == 'VerweisBlock' and 'VERBORGEN' in node[i]) \
                    or (node[i].name == 'unberechtigt'
                        and 'VERBORGEN' in node[i]['VerweisBlock']):
                drop.add(i)
                if node[i + 1].name == 'P':  drop.add(i + 1)
        node.result = tuple(node.children[i] for i in range(len(node.children))
                            if i not in drop)

        L = len(node.children)
        for i, nd in enumerate(node.children):
            if nd.name == 'L':
                succ = node.children[i + 1] if L > i + 1 else EMPTY_NODE
                pred = node.children[i - 1] if i > 0 else EMPTY_NODE
                if pred.strlen() >= 40 or succ.strlen() >= 40:
                    nd.name = 'P'
                    nd.result = '\n'
                    if nd.has_attr('spatium'):  del nd.attr['spatium']
        return node

    def on_AusgangsLemmata(self, node):
        node = self.fallback_compiler(node)
        lemmata = list(node.select('Lemma'))
        if len(lemmata) > 3:
            left = []
            for nd in node.children:
                if nd.name == 'L':
                    if strlen_of(left) >= 40:
                        nd.name = 'P'
                        nd.result = '\n'
                        left = []
                        continue
                left.append(nd)
        return node


    # def on_Quelle(self, node):
    #     node = self.fallback_compiler(node)
    #     if has_class(node, 'kursiv'):
    #         for ctx in node.select_path(LEAF_PATH):
    #             if not pick_from_path(ctx, 'Kursiv'):
    #                 if ctx[-1].tag_name == 'TEXT':
    #                     ctx[-1].name = "Kursiv"
    #                 # elif ctx[-1].tag_name == "ROEMISCHE_ZAHL":
    #                 #     ctx[-1].result = (Node('ROEMISCHE_ZAHL', ctx[-1].result), )
    #                 #     ctx[-1].tag_name = 'Kursiv'
    #     return node


get_Druck_Transformation = ThreadLocalSingletonFactory(DruckTransformation)

druck_ausgabe: Junction = create_junction(
    DruckTransformation, "ausgabe.xml", "ausgabe.druck.xml")

junctions = {ASTTransformation, compiler, ausgabe, html_ausgabe, druck_ausgabe}


#######################################################################
#
# Service-Funktionen
#
#######################################################################


#######################################################################
#
#  Hauptfunktion(en) zum verarbeiten von MLW-Artikeln
#
#######################################################################


def relative_path(file_path: str) -> str:
    """Gibt den relativen Pfad vom gegenwärtigen Arbeitsverzeichnis zu dem
    Pfad `file_path` zurück.
    """
    cwd = os.getcwd()
    if 'win' in sys.platform.lower():
        if file_path.upper().startswith(cwd.upper()):
            rel_path = file_path[len(cwd):]
            if rel_path.startswith('\\'):
                # rel_path = '.' + rel_path
                rel_path = rel_path[1:]
        else:
            rel_path = file_path
    else:
        if file_path.startswith(cwd):
            rel_path = file_path[len(cwd):]
            if rel_path.startswith('/'):
                # rel_path = '.' + rel_path
                rel_path = rel_path[1:]
        else:
            rel_path = file_path
    return rel_path


def erzeuge_Ausgabe_Verzeichnisse(lemma_dir, lemma_name):
    def lösche_oder_erzeuge(pfad):
        nonlocal lemma_name
        if os.path.exists(pfad):
            assert os.path.isdir(pfad)
            # lösche alte, nicht mehr aktuelle Ausgabe-Dateien
            files = os.listdir(pfad)
            for name in files:
                if name.startswith(lemma_name + '.') or os.path.split(lemma_name)[1] in ('.css', '.js'):
                    try:
                        os.remove(os.path.join(pfad, name))
                    except PermissionError:
                        print('Die Datei "%s" konnte nicht gelöscht werden. ' 
                              'Möglicherweise ist sie noch in einem anderen Programm geöffnet! '
                              % os.path.join(pfad, name))
                        print('Es könnte sein, dass dadurch die weitere Bearbeitung scheitert.')
        else:
            try:
                os.makedirs(pfad)
            except FileExistsError:
                pass
    lösche_oder_erzeuge(os.path.join(lemma_dir, DATEN_UVZ))
    lösche_oder_erzeuge(os.path.join(lemma_dir, DRUCKVORSTUFE_UVZ))
    lösche_oder_erzeuge(os.path.join(lemma_dir, VORSCHAU_UVZ))
    lösche_oder_erzeuge(os.path.join(lemma_dir, DRUCK_UVZ))
    lösche_oder_erzeuge(os.path.join(lemma_dir, FEHLERSUCHE_UVZ))
    lösche_oder_erzeuge(os.path.join(lemma_dir, FEHLERMELDUNGEN_UVZ))


def verarbeite_mlw_artikel(file_name, log_dir='', output=set(), show_html=False) \
        -> Tuple[int, List[str], RootNode]:
    """Verarbeitet einen MLW-Artikel.
        file_name: der Dateiname
        log_dir:  das Log-Verzeichnis oder eine leere Zeichenkette,
                  wenn keine Log-Dateien angelegt werden sollen.
        output: eine Menge von Zwischenstufen, die in serialisierter
                Form neben dem Ausgabe-HTML mitabgelegt werden.
                Mögliche Zwischenstufen:
                    'CST':  Concrete Syntax Tree (CST) als S-Expression
                    'AST'   Abstract Syntax Tree (AST) als S-Expression
                    'xml',  Das aus dem AST generierte Daten-XML des Artikels
                    'ausgabe.xml', Das u.a. um zusätzliche Trennzeichen
                            angereicherte Ausgabe-XML, zugleich die Vorstufe
                            für a) die HTML-Ausgabe und b) die Druckausgabe

    Rückgabe;
        int:  Ein Erfolgswert:
            0 = erfolgreich übersetzt (wenn auch möglicherweise mit Fehlermeldungen)
            1 = Datei konnte auf Grund fundamentaler Fehler (z.B. Datei nicht gefunden)
                nicht übersetzt werden.
        List[str]:  Eine Liste der vom Übersetzer zurückgemeldeten Fehler.
        str:  HTML-Schnipsel. Sofern die Verarbeitung erfolgreich war,
            die letzte Verarbeitungsstufe als HTML-Schnipsel der in eine HTML-Seite
            eingebaut werden kann, sonst eine leere Zeichenkette.
    """
    if get_config_value('xml_attribute_error_handling') != 'lxml':
        set_config_value('xml_attribute_error_handling', 'fix')
    msg = []
    if DEBUG:
        print("Ausgabeformate: ", output)

    if not file_name.lower().endswith('.mlw'):
        print(f'Dateiname "{file_name}" hat nicht die Endung ".mlw". Breche ab...')
        return 1, msg, None

    set_config_value('log_syntax_trees', output & {'AST', 'CST'})  # will be logged, anyway

    source_data = []
    baum, errors, ast = kompiliere_mlw(file_name, log_dir, source_data, preserve_ast=bool(output))
    if baum is not None:
        source, source_mapping = baum.source, baum.source_mapping

    # TODO: don't use srciptdir if file_name is not in a directory below scriptdir
    lemma_path = os.path.splitext(file_name)[0]
    lemma_name = os.path.basename(lemma_path)
    lemma_dir = os.path.dirname(lemma_path)

    erzeuge_Ausgabe_Verzeichnisse(lemma_dir, lemma_name)
    rel_path = relative_path(file_name)

    def errors_to_msgs(errors):
        # if any(err.code >= FATAL for err in errors):
            # if there are any fatal errors, only print the fatal errors,
            # because otherwise there might be too many.
            # errors = [err for err in errors if err.code >= FATAL]
        delta = len(file_name) - len(rel_path)
        for error in errors:
            error_str = str(error)[delta:]
            msg.append(error_str)
            if msg:
                with open(os.path.join(lemma_dir, FEHLERMELDUNGEN_UVZ, lemma_name + '.txt'),
                          'w', encoding='utf-8') as f:
                    f.write('\n'.join(msg))

    def speichere_lemmata_einzeln_als_xml(baum: RootNode, UVZ: str, extension: str):
        lemmata = []
        for artikel in baum.select('Artikel', include_root=True, skip_subtree=lambda nd: nd.name.endswith('Position')):
            # outname = lemma_name + '.xml'
            lemma = artikel.attr['name']   # artikel_name(artikel)
            outname = lemma + extension
            if DEBUG:
                print('Schreibe: ', os.path.abspath(os.path.join(os.getcwd(), UVZ, outname)))
            with open(os.path.join(lemma_dir, UVZ, outname), 'w', encoding='utf-8') as f:
                f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
                f.write(artikel.as_xml(inline_tags = baum.inline_tags,
                                       string_tags = baum.string_tags,
                                       empty_tags = baum.empty_tags))
            lemmata.append(lemma)
        baum.attr['lemmata'] = ','.join(lemmata)

    if errors:
        error_level = max(err.code for err in errors)
        if error_level >= FATAL:
            errors_to_msgs(errors)
            msg.append('\nLeider hat es so verhängnisvolle Fehler gegeben, '
                  'dass keine Vorschau erzeugt werden konnte :-(\n')
            return 1, msg, None
        elif error_level >= ERROR:
            if DEBUG:
                msg.append('Es gab Fehler. Die Vorschau könnte Lücken enthalten.')
        else:
            if DEBUG:
                msg.append('\nEs gab ein paar Warnungen...')

    workdir = os.getcwd()

    if os.path.exists(log_dir) and os.path.isdir(log_dir):
        for fname in os.listdir(log_dir):
            if fname.startswith(os.path.basename(lemma_name) + '_') \
                    and fname.endswith('.log.html'):
                basename = os.path.basename(fname)
                logsrc = os.path.join(workdir, log_dir, basename)
                shutil.move(logsrc, os.path.join(lemma_dir, FEHLERSUCHE_UVZ, basename))

    if 'CST' in output and log_dir:
        cstname = lemma_name + '.cst'
        if log_dir:
            cstsrc = os.path.join(workdir, log_dir, cstname)
            shutil.move(cstsrc, os.path.join(lemma_dir, FEHLERSUCHE_UVZ, cstname))

    if 'AST' in output and log_dir:
        astname = lemma_name + '.ast'
        if log_dir:
            astsrc = os.path.join(workdir, log_dir, os.path.basename(astname))
            shutil.move(astsrc, os.path.join(lemma_dir, FEHLERSUCHE_UVZ, astname))
        else:
            with open(os.path.join(lemma_dir, FEHLERSUCHE_UVZ, astname), 'w',
                      encoding='utf-8') as f:
                f.write(ast.as_sxpr())

    if 'xml' in output:
        speichere_lemmata_einzeln_als_xml(baum, DATEN_UVZ, '.xml')
        # for artikel in baum.select('Artikel', include_root=True, skip_subtree=lambda nd: nd.name.endswith('Position')):
        #     # outname = lemma_name + '.xml'
        #     outname = artikel.pick('Lemma').pick('LemmaWort').content.strip() + '.xml'
        #     if DEBUG:
        #         print('Schreibe: ', os.path.abspath(os.path.join(os.getcwd(), DATEN_UVZ, outname)))
        #     with open(os.path.join(lemma_dir, DATEN_UVZ, outname), 'w', encoding='utf-8') as f:
        #         f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        #         f.write(artikel.as_xml(inline_tags = baum.inline_tags,
        #                                string_tags = baum.string_tags,
        #                                empty_tags = baum.empty_tags))

    process_tree(get_Ausgabe_Transformation(), baum)
    if baum.error_flag >= FATAL:
        msg.append('Sehr schwere Fehler aufgetreten: Nur noch eine Not-Vorschau mit groben '
                   'Formatierungsfehlern möglich!')
    baum_druck = copy.deepcopy(baum)

    process_tree(get_HTML_Transformation(), baum)
    if baum.error_flag >= FATAL:
        msg.append('Sehr schwere Fehler aufgetreten: Nur noch eine Not-Vorschau mit groben '
                   'Formatierungsfehlern möglich!')
        baum.result = (Node('Fehlermeldung', 'Not-Vorschau mit falschen oder fehlenden '
                                                  'Formatierungen, da Strukturerkennung durch '
                                                  'bestimmte Fehler durcheinander geraten...'),
                            *baum.result)

    html_schnipsel = baum.as_xml()
    # # HTML-Schnipsel ohne Header, Knöpfe etc. wird in das Datenverzeichnis geschrieben
    # with open(os.path.join(lemma_dir, DATEN_UVZ, lemma_name + '.htm'),
    #           'w', encoding='utf-8') as f:
    #     f.write(html_schnipsel)

    html_file_name = schreibe_html_seite(file_name, html_schnipsel)
    if show_html:
        webbrowser.open('file://' + os.path.abspath(html_file_name) if sys.platform == "darwin"
                        else html_file_name)

    process_tree(get_Druck_Transformation(), baum_druck)
    if 'ausgabe.xml' in output:
        speichere_lemmata_einzeln_als_xml(baum_druck, DRUCKVORSTUFE_UVZ, '.ausgabe.xml')
        # with open(os.path.join(lemma_dir, DRUCKVORSTUFE_UVZ, lemma_name + '.ausgabe.xml'),
        #           'w', encoding='utf-8') as f:
        #     f.write('<?xml version="1.0" encoding="UTF-8"?>\n')  # entfernt wg. lxml-Inkompatibilität
        #     f.write(baum_druck.as_xml())

    errors_to_msgs(baum.errors_sorted)

    if baum.errors:
        if DEBUG:
            msg.append("Die Datei wurde vollständig verarbeitet. "
                       "Leider wurden Fehler und/oder Warnungen gefunden :-(\n")
    else:
        msg.append("Das Einlesen war erfolgreich! Keine Fehler :-)\n")

    for node in baum.select(ANY_NODE, include_root=True):
        if node.has_attr('globaleId'):
            node.attr['id'] = node.attr['globaleId']
        if node.has_attr('globaleReferenz'):
            node.attr['href'] = '#' + node.attr['globaleReferenz']

    return 0, msg, baum



#######################################################################
#
# Rumpf-Programm
#
#######################################################################


def evaluiere(result, messages):
    """Prüft die Rückgabe eines Aufrufs von `verarbeite_mlw_artikel` und
    liefert eine Bewertung als Zeichnkette mit den möglichen Werten
    "GESCHEITERT", "mit Fehlern", "gelungen" zurück."""
    if result >= 1:
        return "GESSCHEITERT"
    if messages:
        for m in messages:
            if m.find('Error') >= 0:
                return "mit Fehlern"
    return "gelungen"


def cpu_profile(func):
    import cProfile as profile
    import pstats
    pr = profile.Profile()
    pr.enable()
    ret = func()
    pr.disable()
    st = pstats.Stats(pr)
    st.strip_dirs()
    st.sort_stats('time').print_stats(40)
    return ret


if __name__ == "__main__":
    print(MLWVersion.VERSION)
    access_presets()
    set_preset_value('test_parallelization', True)
    set_preset_value('AST_serialization', 'S-expression')
    set_preset_value('xml_attribute_error_handling', 'fix')
    set_preset_value('infinite_loop_warning', False)
    homedir = os.path.expanduser('~')
    # if 'di68kap' in homedir or 'eckhart' in homedir:
    #    set_preset_value('resume_notices'] = True
    finalize_presets()
    # tst = get_config_value('resume_notices')

##############################################################################
##############################################################################
    BEISPIEL = os.path.join('Fasz52MLWDateien/saligia.mlw')
##############################################################################
##############################################################################

    grammar_path = os.path.abspath(__file__).replace('Compiler.py', '.ebnf')
    parser_update = False

    def notify():
        global parser_update
        parser_update = True
        print('Kompiliere veränderte Grammatik: ' + grammar_path)
    if not recompile_grammar(grammar_path, force=False, notify=notify):
        error_file_name = os.path.basename(__file__).replace('Compiler.py', '_ebnf_MESSAGES.txt')
        with open(error_file_name, encoding="utf-8") as f:
            print(f.read())
        sys.exit(1)
    elif parser_update:
        print(os.path.basename(__file__) + ' hat sich geändert. '
              'Bitte noch einmal starten, um die Änderungen anzuwenden.')
        sys.exit(0)

    silent = False
    if '--silent' in sys.argv:
        sys.argv.remove('--silent')
        silent = True
    if '-s' in sys.argv:
        sys.argv.remove('-s')
        silent = True

    profile = False
    if '--profile' in sys.argv:
        sys.argv.remove('--profile')
        profile = True
    if '-p' in sys.argv:
        sys.argv.remove('-p')
        profile = True

    log_dir = 'LOGS'
    if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1] in ['-d', '--debug']):
        if BEISPIEL.endswith('.ini'):
            sys.argv.append(os.path.join('test_grammar', BEISPIEL))
        else:
            sys.argv.append('-t')
            # sys.argv.append('-d')
            beispiel_path = os.path.join('Beispiele', BEISPIEL)
            if beispiel_path.endswith('.mlw') or os.path.isdir(beispiel_path):
                sys.argv.append(beispiel_path)

    MULTIPROCESS = True
    if len(sys.argv) > 1 and sys.argv[1] not in ['--help', '-h']:
        output = {'xml', 'ausgabe.xml'}
        for i in range(1, len(sys.argv)):
            if sys.argv[i] in ['-d', '--debug']:
                DEBUG = True
                print('Volle Debugging-Ausgabe')
                print(sys.argv)
                set_config_value('history_tracking', True)
                set_config_value('resume_notices', True)
                start_logging(log_dir)
                output = {'CST', 'AST', 'xml', 'ausgabe.xml'}
            elif sys.argv[i] in ['-t', '--alltrees']:
                # print('Gebe alle Zwischenstufenbäume ab AST aus.')
                output |= {'AST', 'xml', 'ausgabe.xml'}
            elif sys.argv[i] in ['-s', '--singlethread']:
                print("single threading only")
                MULTIPROCESS = False

        if DEBUG:
            print('Ausgabeformate', output)

        file_or_dir = sys.argv[-1]

        print(file_or_dir)

        if os.path.isdir(file_or_dir):
            dir_name = file_or_dir

            # führe alle Tests im Verzeichnis aus
            # Merke: Eigentlich sollte ein Verzeichnis entweder nur .mlw-Dateien
            #        oder Tests enthalten
            files = os.listdir(dir_name)
            if any((f.lower().endswith('.ini') and f.find('_test_') >= 0) for f in files):
                from DHParser.testing import grammar_suite
                error_report = grammar_suite(dir_name, get_grammar, get_transformer,
                                              fn_patterns=['*_test_*.ini'], verbose=True)
                if error_report:
                    print('\n\n')
                    print(error_report)
                    print(error_report[:error_report.find(':\n')] + '!')

            # sammele .mlw-Dateien
            mlw_files = []
            for path, dirs, files in os.walk(dir_name):
                for f in files:
                    if f.lower().endswith('.mlw'):
                        mlw_files.append(os.path.join(path, f))
            mlw_files.sort(key=lambda k: k.lower())

            # verabreite alle gefundenen .mlw-Dateien am Stück
            exit_status = 0
            html_bäume = []
            from concurrent.futures import ProcessPoolExecutor
            from DHParser.toolkit import instantiate_executor, SingleThreadExecutor
            executor = ProcessPoolExecutor if MULTIPROCESS else SingleThreadExecutor
            with instantiate_executor(len(mlw_files) > 3, executor) as pool:
                futures = []
                for file_path in mlw_files:
                    # alternativ:  in zu_aktualisierende(mlw_files, os.path.join(dir_name, VORSCHAU_UVZ)):
                    # result, messages, html_baum = verarbeite_mlw_artikel(file_path, log_dir, output,
                    #                                                      len(mlw_files) <= 1)
                    futures.append(pool.submit(verarbeite_mlw_artikel,
                                               file_path, log_dir, output, len(mlw_files) <= 1))
                for future in futures:
                    result, messages, html_baum = future.result()
                    if result == 0:
                        html_bäume.append(html_baum)
                    for m in messages:
                        print(m)
            if len(mlw_files) > 1:
                dateiname = erzeuge_artikel_strecke(os.path.dirname(mlw_files[0]), html_bäume)
                dateiurl = 'file://' + os.path.abspath(dateiname) if sys.platform == "darwin" else dateiname
                print(f'Öffne "{dateiurl}" im Internet-Browser.')
                webbrowser.open(dateiurl)

            update_vscode_extension(os.path.dirname(os.path.abspath(dir_name)))

            if exit_status > 0:
                sys.exit(1)

        elif os.path.isfile(file_or_dir):
            file_name = file_or_dir
            if file_name.lower().endswith('.ini') and 'test_grammar' in file_name:
                path, fname = os.path.split(file_name)
                start_logging(log_dir)
                from DHParser.testing import grammar_suite, unit_from_file
                error_report = grammar_suite(path, get_grammar, get_transformer,
                                             fn_patterns=[fname], verbose=True)
                test_unit = unit_from_file(file_name)
                dateiname = os.path.abspath(ausgabe_probe(file_name, test_unit))
                webbrowser.open('file://' + dateiname if sys.platform == "darwin" else dateiname)
                sys.exit(1 if error_report else 0)

            if profile:
                exit_status, msgs, _ = cpu_profile(lambda: verarbeite_mlw_artikel(file_name, log_dir, output, not silent))
            else:
                exit_status, msgs, _ = verarbeite_mlw_artikel(file_name, log_dir, output, not silent)
            for m in msgs:
                print(m)

            update_vscode_extension(os.path.dirname(os.path.abspath(file_name)))

            if exit_status > 0:
                sys.exit(exit_status)
        else:
            if os.path.exists(file_or_dir):
                print(file_or_dir + ' ist weder eine Datei noch ein Verzeichnis!?')
            else:
                print(file_or_dir + ' existiert nicht in ' + os.getcwd())
            sys.exit(1)
    else:
        print("Aufruf: MLWCompiler.py [--slient] [--debug] FILENAME")
