# MLW Notation

Stand: 17. November 2022

Im Folgenden wird die vereinfachte Notation für Artikel des Mittellateinischen
Wörterbuchs (MLW) beschrieben. Die vereinfachte Notation erlaubt es, einen
Artikel mit einem Text-Editor einzugeben und dann automatisch nach XML zu
übersetzen.

Die aktuellste Version der Notationsbeschreibung ist immer auf:
<https://gitlab.lrz.de/badw-it/MLW-DSL/-/blob/development/Dokumentation/MLW_Notation_Beschreibung.md>
abrufbar!

## 1 Vorbereitung

### 1.1 Installation der notwendigen Software

Um Artikel in der vereinfachten Notation eingeben und die Vorschau anzeigen zu
können, müssen mehrere Voraussetzungen erfüllt sein:

1. Ein "Python-Interpreter" ab Version 3.7 sollte auf dem Rechner
   installiert sein.

   *Ob das der Fall ist, kann so überprüft werden: Windows-Taste drücken oder mit
   der Maus auf das Windows-Symbol (normalerweise links unten auf dem
   Bildschirm) klicken. Dann entweder "python" oder "idle" tippen. Dann müsste
   als Erstes etwas wie "IDLE (Python ...)" erscheinen. Falls nicht, ist Python
   wahrscheinlich nicht richtig installiert. Falls doch, kann man mit einem
   Klick darauf eine Python-Shell starten. Die Python Versionsnummer sollte in
   dem Fenster, das dann erscheint, ausgegeben werden. Das Fenster kann danach
   wieder geschlossen werden.*

2. Das Python-Modul "requests" muss installiert sein, damit die Druckvorschau
   generiert werden kann. Das kann man überprüfen, indem man eine sogenannte
   Python-Shell wie z.B. "IDLE" aufruft und dann das Kommando:

       import requests

   eingibt. Liefert es einen "ImportError", dann ist
   das entsprechende Modul leider noch nicht installiert.

   *Für Expertinnen und Experten:* Sollte das Modul nicht vorhanden
   sein kann man es in der Windows-Eingabeaufforderung nachinstallieren
   mit den Befehlen:

        python -m pip install --upgrade pip
        python -m pip install requests  

3. Visual Studio Code ab Version 1.32 sollte installiert sein.

   *Das lässt sich prüfen, indem man - genauso wie oben - die
   Windows-Taste drückt, dann "visual" eintippt und, falls "Visual
   Studio Code" erscheint, das Programm mit einem Maus-Klick startet.
   Die Versionsnummer kann man im Menu "Hilfe->Info" bzw. "Help->info"
   erfragen.*

4. Sync&Share sollte installiert sein und laufen.

   *Sync&Share ist auf jedem unserer Rechner vorinstalliert. (Für private
   Laptops kann es von <https://syncandshare.lrz.de> heruntergeladen werden.) Es
   muss allerdings noch gestartet und so konfiguriert werden, dass es nach jedem
   Neustart des Computers automatisch wieder startet. Wenn es läuft, erkennt man
   das an dem kleinen runden weißen Symbol in der Taskleiste, in dem entweder
   ein Häkchen, ein Ausrufezeichen oder eine Sanduhr angezeigt werden.*

Wenn entweder der Python-Interpreter oder Visual Studio Code nicht
installiert sind, oder eine zu alte Version aufweisen, oder wenn
Sync&Share nicht funktioniert, dann hilft das Digital Humanities-Referat
der BAdW weiter.

**Für Fortgeschrittene**: Es ist auch möglich, "Python"
(<https://www.python.org>) und "Visual Studio Code"
(<https://code.visualstudio.com>) von den entsprechenden Websites herunter zu
laden und ohne Administrator-Rechte zu installieren, sofern bei der Installation
angegeben wird, dass es *nicht* für alle Benutzer installiert werden soll.

Im Installations-Dialog von Python sollte auf die richtige Wahl der
Installations-Optionen geachtet werden. Das geht so:

1. Wähle im Installationsprogramm „Custom-Installation“

2. schalte im nächsten Schritt unter „Optional Features“ die Option
   „for all users“ AUS und „py launcher“ AN,

3. und schalte im darauf folgenden Schritt „Advanced Options“ die Option
   „add python to environment variables“ AN

Um Sync&Share erstmalig zu starten und einzurichten, kann man wiederum die
Windows-Taste drücken, "sync" eingeben und dann "LRZ Sync+Share"
starten. Jetzt sollte sich ein Fenster öffnen. Falls nicht, müsste
mindestens das Sync&Share-Symbol im Symbolabschnitt der Taskleiste
erscheinen (ggf. auf den "Winkel" klicken, um die "versteckten" Symbole
anzuzeigen). Mit der Menütaste (rechte Maustaste bei Rechtshändern)
kann man zu dem Symbol das Kontext-Menü öffnen. Der Menü-Punkt
"Anzeigen" öffnet das Sync&Share-Fenster. Dort müsst Ihr die
"Einstellungen..."-Dialogbox öffnen und in den Einstellungen, ziemlich
in der Mitte, bei "Startverhalten" das Häkchen vor "Starte nach dem
Windows Login" setzen und mit "OK" die Dialogbox schließen. Jetzt müsste
in Eurem Heimatverzeichnis ein Ordner "LRZ Sync+Share" erscheinen. Der
Ordner wird praktischerweise auch im "Schnellzugriff" des
Windows-Dateimanagers ("Explorer") angezeigt.


### 1.2 Prüfen der Softwareinstallation

Um zu überprüfen, ob die Installation erfolgreich war, sind folgende
Schritte notwendig:

1. Starte Visual Studio Code.

2. In Visual Studio Code wähle im Menü "**Datei->Ordner öffnen...**"
   oder drücke die Tastenkombination: STRG+K STRG+O. In der
   Datei-Auswahlbox, wähle den Ordner "LRZ
   Sync+Share\MLW\MLW-Testartikel". Auf der linken Seite baut sich der
   Dateibaum auf. 

   Eigene Wörterbuchartikel, die nicht bloß zum Ausprobieren und Testen
   geschrieben werden, sondern für die spätere Publikation bestimmt sind, sollten
   nicht im gemeinsamen Testartikelordner abgelegt werden! Vielmehr sollte dafür
   ein eigener Ordner angelegt werden. Als Ort eignet sich Sync&Share oder das
   vom Leibniz-Rechenzentrum (LRZ) bereitgestellte Netzwerklaufwerk. (Nicht zu
   empfehlen ist die lokale Festplatte, da von der lokalen Festplatte keine
   Sicherungskopien durch das LRZ angelegt werden.) 
   
   Um zu Artikeln, die im eigenen Bearbeitungsordner liegen, eine Vorschau im
   Internet-Browser (auch "HTML-Vorschau" genannt) erzeugen zu können, ist es
   notwendig, den kompletten ".vscode"-Unterordner mit der darin enthaltenen
   "tasks.json"-Datei aus dem "MLW-Testartikel"-Verzeichnis in das eigene
   Arbeitsverzeichnis zu kopieren. Das sollte am besten gleich als Erstes nach
   dem Anlegen eines neuen Arbeitsverzeichnisses geschehen. Nur dann findet
   Visual Studio Code nämlich die "MLW -> HTML-Vorschau" und "MLW ->
   Druckvorschau" und weitere sogenannte "Aufgaben", mit denen man die HTML-
   oder Druckvorschau eines Artikels oder einer Artikelstrecke generieren kann.

3. Im Dateibaum auf der linken Seite im Visual Studio Code-Fenster gibt es, wenn
   Ihr Euch im MLW-Testartikelordner befindet, einige Unterverzeichnisse mit den
   Artikeln. (Da es sich um Testartikel handelt, ist nicht davon auszugehen,
   dass die Artikel in diesem Ordner frei von Fehlern sind!) Öffnet eine dieser
   Dateien, z.B. "iocularis.mlw" im Ordner "iocularis". Wenn die Datei farblich
   eingefärbt wird ("syntaktische Hervorhebung" oder auch "syntax-highlighting"
   genannt), so ist das ein erstes gutes Zeichen. Falls nicht, stimmt etwas
   mit der Installation nicht. Wenn Ihr nun im Menü "Terminal->Aufgabe ausführen"
   wählt, sollten nach kurzer Zeit mehrere "Aufgaben" für MLW-Artikel (das sind
   für den Computer alle Dateien mit der Endung ".mlw"), vorgeschlagen werden,
   unter anderem auch die Aufgabe "MLW -> HTML-Vorschau (Einzelartikel)". Ein
   Klick darauf startet die Übersetzung des MLW-Artikels nach XML und generiert die
   HTML-Vorschau, die sich - sofern der Artikel keine allzu groben syntaktischen
   Fehler enthält - nach einigen Sekunden im Internet-Browser öffnen müsste
   (ggf. musst Du zum Browserfenster wechseln, falls der Browser schon offen
   war).

*Hinweis: Es empfiehlt sich sehr, in den Einstellungen von Visual Studio
Code (Menü Datei->Einstellungen->Einstellungen) den Zeilenumbruch ('Word
Wrap') auf "on" zu schalten. Dann werden lange Zeilen optisch
umgebrochen, d.h. es handelt sich immer noch um eine Zeile, die aber in
mehreren Zeilen dargestellt wird, wie man an der Zeilennummerierung
erkennen kann.*

*Eine Alternative dazu bildet die "rewrap"-Erweiterung für Visual Studio Code!
Zum Installieren klickt man in der Symbolleiste links auf das Symbol mit den
Kästchen ("Extensions"), gibt im Suchfeld oben "rewrap" und fügt die Erweiterung
mit einem Klick auf "Installieren" dem Editor hinzufügen. Die Rewrap-Erweiterung
erlaubt es, überlange Zeilen mit der Tastenkombination "ALT+Q" in mehrere echte
Zeilen zu zerlegen.*

*Auf dieselbe Weise wie die Rewrap-Erweiterung kann man auch das
"German-Language-Pack for Visual Studio Code" installieren. Dann erscheinen alle
Menü-Punkte in Deutsch. Für andere Sprachen, etwa Italienisch, gibt es das
natürlich auch.*

### 1.3 Fehlerbehebung bei Visual Studio Code

Leider kann es gerade nach den automatischen Updates von Visual Studio Code (VSC) manchmal
zu Fehlern, insbesondere zum Fehlen bestimmter Funktionen kommen.

1. **Die Vorschau lässt sich nicht erzeugen, weil VSC die entsprechenden Befehle nicht anzeigt.**

   Entweder fehlt in dem aktuell geöffneten Arbeitsverzeichnis noch das Unterverzeichnis `.vscode`
   oder es existiert, enthält aber noch nicht die `tasks.json`-Datei, oder es wurde nicht das
   richtige Arbeitsverzeichnis geöffnet, sondern - was leicht versehentlich geschieht - ein Ordner
   darüber oder darunter.

    *Alternative Notlösung*: Ctrl+Shift+P drücken und "MLW" tippen. Dann sollten die Befehle ebenfalls erscheinen.
    Das funktionier allerdings nur, wenn die MLW-Erweiterung richtig installiert ist.

2. **Es fehlt die farbliche Hervorhebung von Schlüsselwörtern bei .mlw-Dateien**
  
   a) Möglicherweise wurde versehentlich eine VSC-Erweiterung aus dem "Marktplatz" über die MLW-Erweiterung
   installiert, weil VSC diese manchmal explizit für Dateien mit der Endung ".mlw" anbietet. Dann muss diese
   Erweiterung wieder deinstalliert und VSC neu gestartet werden. Die Liste der Erweiterungen kann man mit
   dem Knopf, der aussieht wie mehrere Quadrate, von denen eins etwas herausgerückt ist, auf der linken
   Seite aufrufen.

   b) Um zu prüfen, ob die (richtige) MLW-Erweiterung schon installiert ist, muss man die Liste der Erweiterungen 
   mit dem entsprechenden Knopf auf der linken Seite öffnen. In der sich dann auf der linken Seite öffnenden Liste
   sollte irgendwo "mlw-language-server" stehen. Ist das nicht der Fall, dann hilft es, für eine beliebige  
   .mlw-Datei eine Vorschau zu generieren, dann wird die Erweiterung automatisch installiert. Danach muss VSC neu
   gestartet werden.

   c) Falls auch das nicht hilft, kann die Ursache auf einem Fehler bei der Registrierung der Erweiterungen
   durch VSC beruhen. Um VSC dazu zu bringen, dass es die installierten Erweiterungen erneut zusammensucht 
   und registriert muss man alle Dateien (aber nicht die Unterverzeichnisse!) 
   im `C:\Users\LRZ-Kennung\.vscode\extensions` Verzeichnis löschen.
   Nach dem nächsten Neustart sollte VSC die MLW-Erweiterung wieder gefunden haben.

   d) Schließlich kann man noch überprüfen, ob rechts unten auf dem Rahmen des MLW-Fensters "Nur Text"
   steht. Ist dies der Fall, sollte man darauf klicken und in der Auswahlbox "MLW (mlw): konfigurierte Sprache"
   wählen. Wird dort keine Auswahl "MLW" angeboten, so ist die Erweiterung noch nicht richtig installiert.
   Dann hilft evtl. einer der vorherigen Schritte weiter.

3. **Eine bestimmte VSC-Version bereitet immer wieder Probleme**

   Wenn sich Probleme, die mit VSC auftreten partout nicht beheben lassen, dann kann es
   helfen, die automatischen Updates in den Einstellungen von VSC abzuschalten und 
   danach eine ältere Version zu installieren. (Achtung, die Reihenfolge ist wichtig: Erst 
   automatische Updates abschalten, dann die alte Version installieren. Andernfalls 
   lädt die alte Version nach dem ersten Start automatische gleich wieder die neueste und 
   potentiell problematische Version von VSC herunter und installiert sie im Hintergrun.)

   Probleme gab es bisher einmal auf einzelnen Rechnern mit der Version 1.75. Eine Version, 
   die tadellos funktioniert, ist die Version 1.72.2.

   *Automatische Updates ausschalten:* Im Menü "Datei->Einstellungen->Einstellungen" aufrufen
   (alternativ STRG und die Komma-Taste "," drücken) und dann nach "Update: Mode" suche. 
   (Achtung: Die Einstellung "Update: Mode" zum Update des Hauptprogramms von Visual Studio
   Code bitte nicht mit der zum automatischen Update der Erweiterungen verwechseln!) Dort 
   muss die Einstellung "none" gewählt werden, um die Updates abzuschalten.

   Ältere Versionen von VSC findet man auf: <https://visual-studio-code.de.uptodown.com/windows/versions>

   Vor der Installation empfiehlt es sich, die Erweiterungen im C:\Users\NUTZERNAME\.vscode\extensions 
   des Nutzerverzeichnisses zu löschen und später über die VSCode-Oberfläche neu zu installieren.
   Alternativ kann man auch im Verzeichnis jeder Erweiterung einzeln die "package.json"-Datei löschen.
   (Damit werden die Erweiterungen zurückgesetzt und beim nächsten Start von VSCode neu für die 
   entsprechende Version von VSCode initialisiert.)

## 2 Einen neuen Artikel anlegen

1. Startet Visual Studio Code und öffnet Euren Artikel-Arbeitsordner. Ordner
   können in Visual Studio Code mit dem Menü "Datei->Ordner öffnen..." geöffnet
   werden.

2. Falls dies der erste Artikel in einer neuen Artikelstrecke ist, an der Ihr
   arbeiten möchtet, empfiehlt es sich, falls noch nicht geschehen, zunächst
   einen neuen Ordner für die Artikelstrecke anzulegen. Das kann entweder wie
   üblich mit dem Dateimanager (bei Windows "Explorer" genannt und bei Apple
   "Finder") geschehen oder auch innerhalb von Visual Studio Code. 

   Für Letzteres
   fahrt Ihr mit der Maus auf die Leiste oberhalb des Dateibaums auf der linken
   Seite des VSC-Fensters. Es erscheinen mehrere Symbole: Ein Klick auf das
   zweite Symbol von links ermöglicht das Anlegen eines neuen Ordners. Dem
   Ordner solltet Ihr einen passenden Namen für die Artikelstrecke geben. Es
   darf auch gerne der Name eines der Lemmata der Artikelstrecke sein.

   Vergesst auf jeden Fall nicht, den ".vscode"-Unterordner mit der
   "tasks.json"-Datei in den neu angelegten Arbeitsordner zu kopieren!

3. Als Nächstes legt Ihr auf dieselbe Weise innerhalb des neu erstellten
   Unterverzeichnisses eine Datei mit dem Namen des ersten Artikels, den
   Ihr schreiben wollt und der Endung ".mlw" an. (Falls Visual Studio
   Code vorschlägt, auf dem "Marktplatz" nach Erweiterungen für Dateien
   des Typs ".mlw" zu suchen, kann die Meldung einfach weggeklickt
   werden.)

Zur Einarbeitung in den Visual-Studio-Code-Editor sei diese Einführung empfohlen:
Teil 1: <https://www.heise.de/hintergrund/Visual-Studio-Code-Ein-Code-Editor-fuer-alle-s-6128966.html?seite=all>
Teil 2: <https://www.heise.de/hintergrund/Visual-Studio-Code-Teil-2-Den-Code-Editor-durch-Erweiterungen-ausbauen-6147512.html?seite=all>


## 3 Einen Artikel eingeben und übersetzen

### 3.1 Einzelne Artikel übersetzen

Wenn Ihr die Datei mit der Endung ".mlw" angelegt habt, dann könnt Ihr
sie im Editor entsprechend der unten ausführlich beschriebenen Syntax
eingeben und bearbeiten. Um den Artikel zu übersetzen, müsst Ihr im Menü
"Terminal" den Menüpunkt "Aufgabe ausführen..." wählen. Euch sollten dann
mehrere Aufgaben angezeigt werden, und zwar:

    MLW -> HTML-Vorschau (Einzelartikel)
    MLW -> HTML-Vorschau (Ganze Artikelstrecke)
    MLW -> Fahnenkorrektur (Einzelartikel)
    MLW -> Fahnenkorrektur (Ganze Artikelstrecke)
    MLW -> Druckvorschau (Einzelartikel)
    MLW -> Druckvorschau (Ganze Artikelstrecke)

Es ist gut möglich, und sollte nicht weiter beunruhigen, dass diese Liste von
Aufgaben in einer anderen Reihenfolge angezeigt wird! Die am häufigsten genutzte
Aufgabe dürfte die HTML-Vorschau eines einzelnen Artikels sein. Nach dem
Aufrufen von "MLW -> HTML-Vorschau (Einzelartikel)" wird der gerade bearbeitete 
Artikel nach XML (und HTML) übersetzt und im Internet-Browser eine Vorschau angezeigt.

Bei der Übersetzung öffnet sich in Visual Studio Code ein weiteres Fenster, das
ggf. Fehlermeldungen enthält. Wenn Ihr auf einen Fehler klickt, springt der
Editor zu der Stelle im Quelltext, wo der Fehler aufgetaucht ist. Oft, aber
nicht immer ist dies die Stelle, die den Fehler verursacht hat. Die als
fehlerhaft identifizierte Stelle wird zusätzlich vom Editor rot unterkringelt.

Manchmal identifiziert der Computer die fehlerhafte Stelle falsch, dann findet
sich der echte Fehler fast immer etwas weiter oben im Text. Das betrifft dann
fast immer Syntax-Fehler, d.h. Stellen, an denen etwas geschrieben worden ist,
was nicht den formalen Vorgaben der MLW-Notation entspricht.

**Tipp**: Diese Art der Fehlersuche kann etwas frustrierend sein.
Verschwendet nicht zu viel Zeit damit! Wenn Ihr das Gefühl habt, bei der
Fehlersuche nicht weiterzukommen, dann legt dafür ein Ticket auf
<https://gitlab.lrz.de/badw-it/MLW-DSL/-/issues> an.

**Hinweis:** *Wenn die Aufgabe "MLW -> HTML-Vorschau" nicht gefunden
wird (sollte normalerweise beim Aufruf des Menüs "Terminal->Aufgabe
ausführen..." nach kurzer Zeit erscheinen), dann arbeitet Visual Studio
Code nicht auf dem richtigen Ordner. Mit dem Menü "Datei->Ordner
öffnen..." muss zunächst ein Arbeitsordner geöffnet werden, der ein Unterverzeichnis mit
dem Namen ".vscode" (der Punkt am Anfang ist wichtig!) enthält, in welchem
die "tasks.json"-Datei abgelegt ist, in der die MLW-Aufgaben beschrieben sind.*

*Der Grund für dieses Verhalten ist folgender: Für Visual Studio Code
sind die Aufgaben anders als die farbliche syntaktische Hervorhebung
immer mit einem spezifischen Projekt verknüpft, nicht mit einem
spezifischen Dateityp (wie z.B. Dateien, die mit ".mlw" enden). Ein
Projekt wiederum besteht aus einem Ordner mit einem
".vscode"-Unterordner, der projektspezifische Konfigurationsdateien
enthält. Öffnet man nur eine Datei, ohne vorher mit "Datei->Ordner
öffnen..." einen Projektordner geöffnet zu haben, dann findet Visual
Studio Code die zugehörige Aufgabe nicht.*

*Dasselbe passiert, wenn man im "falschen" Ordner arbeitet bzw. den "falschen"
Ordner öffnet, z.B. einen Unterordner für einen bestimmten Artikel wie
"ischion". Dort fehlt das ".vscode"-Unterverzeichnis und Visual Studio Code
findet die Aufgabe nicht, obwohl es die geöffnete mlw-Datei syntaktisch richtig
einfärbt.

### 3.2 Artikelstrecken übersetzen

Wenn Ihr schon mehrere Artikel in demselben Unterverzeichnis angelegt
habt, dann könnt Ihr auch gleich die ganze Artikelstrecke übersetzen.
Dazu wählt Ihr den Menüpunkt "Terminal->Aufgabe ausführen..." die Aufgabe
"MLW -> HTML-Vorschau (Ganze Artikelstrecke)". Dann werden alle auf
'.mlw' endenden Dateien in dem Verzeichnis, in dem der gerade zu
bearbeitende Artikel liegt, in einem Rutsch übersetzt und eine Vorschau
der gesamten Artikelstrecke angezeigt.

In der html-Vorschau gibt es drei Ansichten zur Auswahl: übersichtlich, 
kompakt und Disposition. "Kompakt" bietet den Artikel als Lauftext, 
"übersichtlich" hebt die einzelnen Positionen voneinander ab, 
"Disposition" zeigt nur das Gerüst des Artikels ohne Artikelkopf und Belege.

**Übrigens**: Ihr könnt die Artikel-Vorschau im Browser auch ausdrucken, in dem
Ihr die Druck-Funktion Eures Browsers verwendet. Der Ausdruck der HTML-Vorschau
erfolgt immer einspaltig mit breitem Korrekturrand. Das sieht nicht so schick
aus wie die ein- oder zweispaltige Druckvorschau könnte aber auf die Schnelle
für einen ersten Korrekturdurchgang ein halbwegs passabler Ersatz sein. (Das ist
oft dann hilfreich, wenn aus irgendwelchen Gründen, z.B. Verbindungs- oder
andere Netzwerkprobleme, die Erzeugung der Druckausgabe gerade nicht
funktioniert.)


### 3.3 Die Verzeichnis-Organisation

Alle Dateien, die beim Übersetzen von MLW-Artikeln erzeugt werden,
werden in einem automatisch angelegten Unterverzeichnis "Ausgabe" im
Verzeichnis der jeweiligen Artikelstrecke abgelegt. Dort werden die
erzeugten Dateien in weitere Unterverzeichnisse einsortiert:

| Unterverzeichnis  | enthält                                 |
|-------------------|-----------------------------------------|
| Fehlermeldungen   | Fehlermeldungen bei der Übersetzung     |
| HTML-Vorschau     | Dateien der Browser-Vorschau            |
| PDF-Druckvorschau | vorerst noch leer                       |
| Wanzensuche       | nur für die Softwareentwicklung wichtig |
| XML-Daten         | Generierte XML-Fassung der Artikel      |
| XML-Druckvorstufe | Die Eingabe-Daten für die Druckvorschau |

Vor allem die Dateien der "XML-Druckvorstufe" sind wichtig. Denn diese
Dateien müssen ausgewählt werden, wenn die Druckvorschau nicht aus
Visual Studio Code heraus, sondern mit der Web-Eingabemaske für
die Druckvorschau erzeugt werden soll.

Nützlich könnte ggf. auch das Unterverzeichnis "PDF-Druckvorschau"
sein, in dem übrigens auch die einspaltig gesetzten Fahnenkorrekturen
landen. Dort findet man die Druck-PDFs wieder, falls man sie sich
noch einmal anschauen möchte, ohne sie neu zu erzeugen - was bei der
Druckvorschau etwas zeitaufwändiger ist als bei der HTML-Vorschau.

### 3.4 Druckvorschau und Fahnenkorrektur selbst erzeugen

#### 3.4.1 Druckvorschau in Visual Studio Code erzeugen

Der einfachste Weg, um eine Druckvorschau zu erzeugen, besteht darin, in
Visual Studio Code aus dem Menü "Terminal -> Aufgabe ausführen..."
eine der Aufgaben "MLW -> Vorschau Druckbild" oder "MLW -> Vorschau
Fahnenkorrektur" auszuwählen. Visual Studio Code übersetzt dann die
MLW-Datei im Editor nach XML, sowie alle weiteren auf ".mlw" endenden
Dateien in demselben Verzeichnis, und es schickt sämtliche XML-Dateien an
Olivers Druckvorschau-Server. Nach einer Weile (es dauert etwas
länger als die HTML-Vorschau) kommt das Druck-PDF zurück und wird dann -
je nachdem, wie Euer Computer konfiguriert ist - im Browser oder mit
einem anderen PDF-Anzeigeprogramm angezeigt. Von dort könnt Ihr es auch
speichern oder ausdrucken, wenn Ihr das möchtet.

Der Unterschied zwischen der Druckvorschau und der Fahnenkorrektur
besteht darin, dass die Druckvorschau zweispaltig ist und das ganze
Blatt füllt, während die Fahnenkorrektur einspaltig ist, und einen
breiten Korrekturrand lässt. (Beim Ausdruck der HTML-Vorschau im Browser
ist übrigens immer ein Korrekturrand vorhanden, da davon ausgegangen
wird, dass dieser Ausdruck sowieso nur zu Korrekturzwecken dient.)


#### 3.4.2 Druckvorschau mit der Web-Eingabemaske erzeugen

Die Druckvorschau und die einspaltige "Fahnenkorrektur" einer Artikelstrecke
kann alternativ auch ohne Visual Studio Code direkt über die Web-Oberfläche des
Druckvorschau-Servers erzeugt werden. Das ist ein klein wenig komplizierter,
funktioniert dafür aber auch dann, wenn mit den "Aufgaben" in Visual Studio Code
mal etwas klemmt, oder das Programm die "tasks.json"-Datei nicht findet.

Die Weboberfläche des Druckvorschau-Servers ist unter
<http://badwver-itservice.srv.mwn.de:8080/mlw/export> zu erreichen. Es
erscheint ein Login-Fenster, in das Ihr die für jede Mitarbeiterin und
jeden Mitarbeiter (aber hoffentlich niemanden sonst) im 
Sync & Share-Verzeichnis "MLW (Eckhart Arnold)\Zugangsdaten" in der Textdatei 
"Zugangsdaten.txt" abgelegten Login-Daten eingebt.

Ist das geschehen, so könnt Ihr auf der dann erscheinenden Seite mit dem
"Browse..."-Knopf die Druckvorstufen-Dateien zum Zusammenstellen einer
Druckvorschau auswählen. Diese Dateien findet Ihr im Verzeichnis
"Ausgabe/XML-Druckvorstufe" innerhalb des Verzeichnisses Eurer
Artikelstrecke. Die Dateien enden alle mit ".ausgabe.xml". Sind dort
keine Dateien vorhanden oder fehlt das ganze Verzeichnis, dann müsst Ihr
erst über das Menü "Terminal->Aufgabe ausführen..." die Aufgabe "MLW ->
HTML-Vorschau (Ganze Artikelstrecke)" ausführen. Dann wird das
Verzeichnis angelegt und darin die Druckvorstufen-Dateien erzeugt.

Ist es Euch gelungen, die Dateien in die Web-Anwendung hochzuladen, dann
könnt Ihr noch den Export-Typ "Normal" (zweispaltig ohne Korrekturrand)
oder "Korrekturfahne" (einspaltig mit Korrekturrand) auswählen und mit dem Knopf
"Los!" wird die Erzeugung des Druck-PDFs angestoßen. Bis die PDF-Datei
zurück geliefert und im Browser angezeigt wird, kann das je nach Größe
der Artikelstrecke einige Sekunden bis Minuten, oder bei ganzen Faszikeln
auch mal Stunden dauern. Also: Geduld!

### 3.5 Fehler finden und Fehler mitteilen

Es können mehrere Arten von Fehlern auftreten:

1. Das Übersetzungsprogramm "MLW -> HTML-Vorschau" meldet Fehler.

2. Das Übersetzungsprogramm kann den eingegebenen Text oder größere
   Bereiche gar nicht übersetzen.

3. Der Text wurde ohne Fehler übersetzt, aber die Vorschau stimmt nicht
   oder entspricht nicht den Regeln und Gepflogenheiten für einen
   MLW-Artikel.

Bei den Punkten 1 und 2 ist die Ursache entweder ein Fehler bei der Eingabe, bei
dem Teile der Eingabe nicht der in dieser Anleitung beschriebenen Notation
entsprechen, oder ein Fehler im Übersetzungsprogramm. Bei Punkt 3 handelt es
sich meistens um einen Fehler im Übersetzungsprogramm.

Insbesondere bei den Fehlern der ersten beiden Kategorien besteht der
erste Schritt zur Fehlersuche darin, anhand der Fehlermeldungen im
Ausgabe-Fenster und anhand dieser Beschreibung zu versuchen, den
Quelltext des Artikels zu korrigieren oder umzuarbeiten.

Wenn das nicht zum Erfolg führt, oder Ihr die Ursache für eine
Fehlermeldung oder fehlerhafte Ausgabe nicht finden könnt, oder Ihr
Anlass zu der Vermutung habt, dass es sich um einen Programm-Fehler
handelt, dann solltet Ihr den Fehler über das Ticket-System 
der gitlab-Plattform melden: <https://gitlab.lrz.de/badw-it/MLW-DSL/-/issues>
(Siehe auch Kapitel 6 "Fehler und Probleme mitteilen" in dieser Anleitung.)


### 3.6 Warnungen

Die vom Übersetzungsprogramm gemeldeten Fehler fallen in zwei unterschiedliche
Kategorien: Fehler und Warnungen, z.B.:

    93:41: Warning (516): Falls es sich bei sacerdos sacerdotum um ein einzelnes mehrgliedriges Lemma handelt, sollte es zusammenhängend notiert werden: #{sacerdos sacerdotum}
    106:33: Error (5070): Stellenangabe " p. 329" nicht durch Semikolon von der Werk- bzw. Editionsangabe abgetrennt!

Bei Fehlern konnten Teile des Artikels nicht oder nicht richtig übersetzt
werden, so dass auch die Vorschau oder Druckvorlage, selbst wenn sie weitgehend
vollständig erstellt werden konnte, an bestimmten Stellen fehlerhaft ist. Fehler
sollten unbedingt behoben werden, bevor ein Artikel in den Druck geht.

Warnungen zeigen an, dass Teile des Artikels möglicherweise(!) semantisch falsch
verstanden wurden, auch dann, wenn die Druckausgabe korrekt ist. Anders als
Fehler können Warnungen auch falscher Alarm sein. Bei einer
Warnung sollte man sich überlegen, ob sie berechtigt ist, bevor der
Artikel-Quelltext korrigiert wird, um die Warnung zu vermeiden.

Hat man sich davon überzeugt, dass eine Warnung unberechtigt ist, so kann man
die störende Warnung unterdrücken, was derzeit allerdings nur für
alle Warnungen eines bestimmten Typs möglich ist. Dazu muss man
ganz am Ende des Artikel (nach der Verfasserangabe) den Befehl
`UNTERDRÜCKE WARNUNG` einfügen und die Kenn-Nummern der
Warnungen, die immer in Klammern hinter "WARNING" stehen, angeben:

    UNTERDRÜCKE WARNUNG 514, 516

Bei diesem Beispiel werden Warnungen des Typs 514 und des Typs 516 beim nächsten
Übersetzen nicht mehr angezeigt. Andere Warnungen können immer noch erscheinen.


### 3.7 Ursachen für bestimmte unverständliche Fehler (Tabulatoren)

Der Artikel kann nur dann richtig übersetzt werden, wenn der Quellcode
a) im UTF-8 Format gespeichert ist und b) KEINE Tabulatoren enthält.
Tabulatoren sehen aus wie Leerzeichen, haben aber einen anderen
Zeichencode, den das Übersetzungsprogramm nicht verarbeitet.

Falls eine der beiden Bedingungen nicht gegeben ist, kann man wie im Folgenden
beschrieben, mit Visual Studio Code Abhilfe schaffen.

#### 3.7.1 Textkodierung auf UTF-8 umstellen

Standardmäßig speichert Visual Studio Code Textdateien im Unicode-Format UTF-8.
Sollte es trotzdem einmal vorkommen, dass eine .mlw-Datei in einer anderen
Kodierung vorliegt, dann muss man sie ins UTF-8-Format konvertieren.

Die Textcodierung einer Datei wird von Visual Studio Code rechts unten in der
Info-Leiste angezeigt:

![Grafik: Textkodierungsanzeige](Grafiken/Textkodierung.png)

Wenn dort etwas eine Andere Kodierung als "UTF-8" steht, z.B. "Windows 1252",
dann kann man die Kodierung leicht auf UTF-8 ändern, indem man auf diese
Kodierungsangabe in der Info-Leiste mit der Maus klickt. Oben im Fenster
öffnet sich dann ein Menü, das einen zur Auswahl einer Aktion auffordert.
Hier sollte man zunächst "Save with Encoding" wählen und dann in dem sich
nun öffnenden Auswahlfenster "UTF-8" (ohne BOM). Das Dokument liegt nun
in UTF-8 vor.

#### 3.7.2 Tabulatoren entfernen

Das Übersetzungsprogramm verweigert die Übersetzung von .mlw-Dateien, die
Tabulator-Zeichen enthalten, da dies sonst zu obskuren Fehlermeldungen führt.
Erhält man bei einem Übersetzungsversuch die Fehlermeldung, dass Tabulatoren in
der Datei gefunden wurden, kann man sie durch Suchen und Ersetzen beseitigen.

Dazu muss man im Menü Bearbeiten->Ersetzen wählen oder Strg+H drücken. In dem
sich öffnenden Suchen&Ersetzen-Fenster muss man nun zunächst die Suche auf
reguläre Ausdrücke umstellen. Dazu muss das Symbol `.*` ganz rechts in der
Suchleiste anklicken:

![Grafik: Tabulatoren ersetzen](Grafiken/Tabulatoren_ersetzen.png)

Nun muss man im Suchfeld `\t` eingeben. (Das ist der reguläre Ausdruck für das Tabulator-Zeichen.)
In das Ersetzen-Feld sollte man die Anzahl von Leerzeichen eintippen, die der Breite des Tabulators
entspricht, was normalerweise vier Leerzeichen sind. Anschließend kann man getrost auf das Symbol
für "Alle Ersetzen" klicken. Schließlich sollte der so geänderte Text noch einmal gespeichert werden.
Die Übersetzung sollte danach funktionieren.

Sollte dieses Problem einmal aufgetreten sein, dann ist es sinnvoll, in den Einstellungen von
Visual Studio Code die Einträge für die Behandlung der Tabulatoren zu überprüfen. Die 
Einstellungen sollten so gewählt werden, dass Tabulatoren immer durch Leerzeichen ersetzt werden,
so dass auch durch das Drücken der Tabulator-Taste kein Tabulator-Code, sondern eine bestimmte Anzahl
Leerzeichen in den Text eingefügt wird:

    "editor.detectIndentation": false,
    "editor.insertSpaces": true,
    "editor.tabSize": 4,

### 3.8 Eingabe kombinierter Unicode-Sonderzeichen

*Zwei Tipps von Marie-Luise Weber zur Eingabe kombinierter Sonderzeichen:*

1. Methode: Als erste Anlaufstelle gibt es die von Tanja zusammengestellte
    Sonderzeichendatei (LRZ Sync+Share\MLW (Eckhart
    Arnold)\MLW-Testartikel\Sonderzeichen.vscode), die man in VSc direkt öffnen und
    verwenden kann. Dort sind auch einige kombinierte Zeichen drin. Dann gibt es die
    von Johannes zusammengestellte Worddatei (Sync&Share >> MLW >> MLW
    Sonderzeichenliste). Nur scheinen manche der dort aufgelisteten Zeichen nicht
    mehr zu funktionieren, resp. auf die falschen Unicode-Zeichen zu führen. In der
    VSC- und HTML-Fassung fällt das nicht unbedingt auf, aber im pdf dann schon. Um
    wirkliche hochgestellte Buchstaben zu erreichen, geht es so: Zuerst den unten
    stehenden Buchstaben tippen, dann in Unicode den Code für den hochgestellten
    Buchstaben ("kombinierendes diakritisches Zeichen") eingeben und das Zeichen mit
    dem angebotenen Button kopieren und dann in VSC einfügen: dann setzt sich der
    kombinierte Buchstabe von selbst zusammen. Link zu der Unicode-Seite, ganz unten
    unter "Mittelalterliche hochgestellte diakritische Buchstaben":
    https://unicode-table.com/de/blocks/combining-diacritical-marks/

2. Methode: Wenn es mit der obigen Methode auch nicht klappt, empfiehlt es sich,
    die Zeichen nicht selbst zusammenzusetzen, sondern als fertiges Zeichen aus
    Unicode zu holen. Dazu hat Helena eine sehr gut brauchbare Liste inlusive
    Unicode-Nummern und Dezimalcode (s. dazu auch Ticket #111 (closed)) gefunden:
    https://www.utf8-zeichentabelle.de/unicode-utf8-table.pl?start=7552&number=512&names=-&utf8=dec
    Viele der Zeichen, die wir brauchen, dürften da drauf stehen.

## 4 Die Notation

### 4.1 Grobstruktur

Jeder Artikel des Mittellateinischen Wörterbuchs besteht aus einer Folge 
von Beschreibungsblöcken, sogenannten "Positionen", die jeweils durch ein 
vollständig großgeschriebenes Schlüsselwort, wie z.B. `BEDEUTUNG`, eingeleitet
werden. Ein Block endet immer dort, wo der nächste Artikelteil anfängt.
(Eine Ausnahme bilden Unterbedeutungsblöcke, die ineinander
verschachtelt sein können und immer dort aufhören, wo ein neuer
(Unter-)Bedeutungsblock derselben oder einer niedrigeren Ebene oder ein
ganz neuer Artikelteil, z.B. die Autor(innen)angabe, beginnt.)

Die Abfolge der Blöcke sieht so aus:

    LEMMA
    GRAMMATIK

    ETYMOLOGIE

    SCHREIBWEISE
    FORM
    STRUKTUR
    GEBRAUCH
    METRIK
    DEPONENS
    VERWECHSELBAR

    BEDEUTUNG
      UNTER_BEDEUTUNG
       .
       .
       .
    .
    .
    .

    SUB_LEMMA...

    VERWEISE
    AUTORIN

Die Blöcke von `SCHREIBWEISE` bis `VERWECHSELBAR` bilden den Artikelkopf.
Sie sind optional, d.h. sie dürfen vorkommen, müssen es aber nicht, und
ihre Reihenfolge ist beliebig. Ebenso ist der Block `VERWEISE` am Ende
optional und auch der Block `SUB_LEMMA`.

Die Blöcke im Artikelkopf haben abgesehen von `GRAMMATIK` und `ETYMOLOGIE`
alle ein- und dieselbe Feinstruktur. `GRAMMATIK` und `ETYMOLOGIE` zählen 
hier nicht zum Artikelkopf im engeren Sinne,
da beide eng mit der Lemmaangabe verknüpft sind und bei mehreren Lemmavarianten
anders als die Positionen `SCHREIBWEISE`  bis `VERWECHSELBAR` mehrfach angegeben
werden können. Zudem haben die Grammatik- und Etymologieposition eine andere
Feinstruktur als die Artikelkopfpositionen im engeren Sinne.

**_Hinweis zu Einrückungen_**

In den hier vorgestellten Beispielen wird der Text an vielen Stellen
um zwei Zeichen eingerückt, z.B.:

      UNTER_UNTER_BEDEUTUNG  voluntas, arbitrium --
        Wille, (willkürlicher) Beschluss:

        * VITA EUCH. VAL. MAT.; 22 "quamvis esset Maternus magnae vir
        auctoritatis ..., nihil tamen ex proprio imperio quasi libera
        utens potestate exercuit"

Hier ist alles, was auf das Schlüsselwort `UNTER_UNTER_BEDEUTUNG` folgt, um zwei
Zeichen eingerückt. Diese Einrückungen sind nicht verpflichtend und können auch
weggelassen werden. Sie haben aber den Vorteil, dass einige Texteditoren für
eingerückte Bereiche die Funktion "Einklappen" anbieten. Bei Visual Studio Code,
beispielsweise, fährt man dazu mit dem Mauszeiger auf die linke Leiste (wo die
Zeilenzahlen stehen). Dort erscheint dann ein kleines Minuszeichen. Klickt man
darauf, so wird der ganze Text unterhalb von `UNTER_UNTER_BEDEUTUNG`
"eingeklappt" und das Minuszeichen verwandelt sich in ein Pluszeichen. Durch
einen Klick auf das Pluszeichen wird der Text wieder ausgeklappt. Diese Funktion
kann bei großen Artikeln helfen, den Überblick zu behalten - als zusätzliche
Hilfe zu der Gliederungsübersicht auf der linken Seite.

**WICHTIG: Im Artikelkopf ist die richtige Einrückungstiefe für die Strukturerkennung
relevant!** D.h. im Artikelkopf hat die Einrückungstiefe eine unmittelbare 
semantische Bedeutung. Außerhalb des Artikelkopfes orientiert sich die Strukturerkennung an
anderen Merkmalen und Fehler oder Abweichungen bei der Einrückungstiefe fallen
nicht ins Gewicht. Die Einrückung dient dort nur der Optik.


### 4.2 Feinstruktur

Im Folgenden wird die Feinstruktur der einzelnen Positionen beschrieben. Dabei
wird immer zunächst der Standardfall erklärt und danach auf seltener vorkommende
Fälle eingegangen (wie z.B. den Mehrfachansatz bei einer Lemma-Position).


#### 4.2.1 LEMMA-Position

Die Lemmaposition wird durch das Schlüsselwort LEMMA eingeleitet,
gefolgt von dem (ggf. "gesternten") Lemmawort. Wie bei der
Druckausgabe können darauf in runden Klammern eingeschlossen ein oder
mehrere "Lemmavarianten" folgen. In der Regel werden hier nur
Abkürzungen zur Bezeichnung bestimmter sprachlicher Phänomene
angegeben. Am Ende kann noch ein Zusatzhinweis des Bearbeiters
oder der Bearbeiterin wie z.B. `{sim.}` folgen. Zusätze werden als
solche dadurch kenntlich gemacht, dass man sie in geschweifte Klammern
einschließt.

(Hinweis: Im Druck und in der Vorschau werden die geschweiften
Klammern nicht mit angezeigt und stattdessen das, was innerhalb der
geschweiften Klammern steht, kursiv gesetzt.)

Beispiel:

    LEMMA *facitergula

      (fasc-, -iet-, -ist-, -rcu- {sim.})

Die zusätzliche Leerzeile zur deutlicheren optischen Trennung von
Lemmawort und Lemmavarianten ist nicht zwingend und kann auch
weggelassen werden, ebenso wie die Einrückung der Lemmavarianten und
des Zusatzes.

Das allgemeine Format für die LEMMA-Angabe sieht so aus:

    LEMMA [*] Lemmawort

      (Lemmavariante 1, Lemmavariante 2, ... {Zusatz})

Die Angabe von Lemmavarianten und/oder Zusatz ist optional.

**Unechte Lemmata**: Bei "unechten" Lemmata (nur in den Addenda) 
wird in der Druckausgabe traditionell der ganze Artikel in 
eckige Klammern eingefasst. In der Notation müssen dafür 
unechte Lemmata durch das Schlüsselwort `UNECHT` markiert werden:

    LEMMA UNECHT caxiatenis


##### 4.2.1.1 Doppel- oder Mehrfachansatz (mit Schlüsselwort VEL)

Kommt eine Lemmavariante so häufig vor, dass sie als verselbständigte 
Nebenform angesehen werden kann, kann sie statt in Klammern nach dem 
Lemmawort mit dem Schlüsselwort VEL (immer groß geschrieben) 
angeschlossen werden. Das ist ein sogenannter Doppel- oder Mehrfachansatz.

Im Gegensatz zu den in Klammern angegebenen Lemmavarianten zeigt die
vel-Form eine Lemma-Alternative an, die über eine eigene Grammatikposition
oder eine eigene Etymologieposition verfügen darf, z.B.:

    LEMMA infesto

    GRAMMATIK
        verbum I; -avi, -are

    VEL {semel {=> l.30|-}} infestor

    GRAMMATIK
        verbum I; -ari

oder als Beispiel für einen Mehrfachansatz mit eigener Etymologieposition:

    LEMMA
    
    insula (-sola)
    
    VEL {raro} iscla (ys-)
    
    ETYMOLOGIE
        fere in chartis Italiae septemtrionalis:
             * {cf.} Wartburg, Frz. etym. Wb. IV.; p. 728sqq.
             * Battisti-Alessio, Diz. etim. ital. III.; p. 2111 s. v. "2. ischia"
             * F. Giger et al., Dicziunari Rumantsch Grischun X.; p. 130sq. s. v. "1. isla"
    
    GRAMMATIK
    
    subst. I; -ae f.

Im Falle einer eigenen Etymologieposition wird die Etymologieangabe bei der
Ausgabe der Lemmavariante übrigens vorangestellt:  **insula** (-sola) *vel raro (fere in chartis Italiae septemtrionalis; cf. Wartburg, ...)* **iscla** (ys-), -ae *f.*

Verfügt die Lemmaalternative nicht über eigene Grammatikangaben, dann können das
Lemma und die mit `VEL` angeschlossene Variante der Kürze halber auch alles in
eine Zeile geschrieben werden. Das `GRAMMATIK`-Schlüsselwort muss jedoch immer
in eine neue Zeile gesetzt werden:

    SUB_LEMMA infimus VEL imus
    GRAMMATIK  superl. I-II; -a, -um

Gelten ein- und dieselben Grammatikangaben für mehrere Lemmaalternativen, dann
wird die Grammatikposition nur am Ende angegeben. Sie gilt automatisch für alle
vorhergehenden Lemmaalternativen ohne eigene Grammatikposition.

Übrigens: In zwei der obigen Beispiele kommen die Zusätze `{semel}` und `{raro}` vor. 
Diese und andere Indikatoren für die Häufigkeit (semel, bis, raro, rarius etc.) 
können nach dem Schlüsselwort `VEL` eingefügt werden. Gegebenenfalls kann im 
Zusatz auch ein Verweis enthalten sein, z.B. `{semel {=> ankername}}` oder 
`{bis {=> ankername1; ankername2}}`.


##### 4.2.1.2 Unterartikel (SUB_LEMMA)

Nach sämtlichen Bedeutungen und Belegen, aber noch vor der Verweisposition und
der Autor- bzw. Autorinnen-Angabe können Unterartikel angegeben werden.
Unterartikel haben denselben Aufbau wie Hauptartikel, nur dass sie keine eigene
Verweis- und Autor/innen-Position enthalten. Außerdem werden sie nicht mit
`LEMMA`, sondern dem Schlüsselwort `SUB_LEMMA` eingeleitet.

#### 4.2.2 GRAMMATIK-Position

Die Grammatik-Position wird durch das Schlüsselwort `GRAMMATIK` eingeleitet.
Darauf folgt die Wortart, die Angabe der Flexion und - bei Nomina - das Genus.
Die Wortart muss immer angegeben werden. Je nach Wortart, *muss* an die
Wortartangabe eine Nomen-Klasse oder Verb-Klasse angeschlossen werden, oder es
kann direkt eine Wortartergänzung folgen. Siehe dazu die Liste der zulässigen
Wortartbezeichnungen im Anhang. 

Alle folgenden Angaben wie Flexionsformen, genera und ggf. auch casus und numerus
können weggelassen werden. In diesem Fall wird davon ausgegangen, dass
die entsprechenden Angaben unbestimmt sind. (Das bedeutet aber auch, dass der
Computer einen nicht warnt, wenn eine solche Angabe vergessen worden ist!) 
Es ist also durchaus möglich nur zu schreiben:

    GRAMMATIK  subst. unbestimmt

Werden entsprechende Angaben jedoch gemacht, dann sollte folgende Reihenfolge
eingehalten werden:

1. casus und numerus (falls sie angegeben werden, was nur in Sonderfällen
   oder bei Grammatik-Varianten geschieht.)
2. Flexionen
3. bei Substantiven das Genus.

Folgen auf die Wortart und Wortartklasse weitere Angaben wie Flexionsformen
etc., so müssen sie von der Wortart und Wortartklasse durch ein Semikolon
abgetrennt werden. Beispiel: `verbum I; -ari`

Zwischen den folgenden Angaben, also z.B. zwischen Flexion und Genus genügt
demgegenüber ein einfaches Leerzeichen, so wie das auch später im Druck
erscheint, z.B. beim Lemma facitergula: `subst. I; -ae f.`. Es ist ohne Weiteres möglich,
mehrere Wortarten anzugeben, die dann durch das Bindewort `vel` verknüpft werden
müssen, z.B.: `adv. vel praep.`. Anstelle der festgelegten Abkürzung darf auch die 
ausgeschriebene Wortart verwendet werden, z. B. `substantivum` anstelle von `subst.` 
Die erlaubten Wortartbezeichnungen und ihre Abkürzungen stehen
in der Tabelle im Anhang.

Die Flexion wird abgekürzt mit führendem Spiegelstrich angegeben, ggf. kann es
mehrere solcher Angaben geben, die durch Komma getrennt sind (z.B. `-us, -i`).

Das Genus kann in abgekürzter Form (`f.`) oder in ausgeschriebener Form
(`femininum`) angegeben werden. So kann statt `subst. I; -ae f.`
kürzer geschrieben werden: `substantivum I; -ae femininum`

Wie schon beim Lemma können in der Grammatik-Position mehrere
Grammatik-Varianten folgen. Bei den Grammatik-Varianten wird keine Wortart mehr
angegeben, sondern lediglich die Flexion und ggf. das Genus.
Bei häufig vorkommenden Varianten empfiehlt sich allerdings eine Mehrfachangabe 
wie unten in 4.2.2.1 beschrieben.

Nach der Angabe einer Grammatikvariante folgen, abgetrennt durch einen
Doppelpunkt, *immer* ein oder mehrere Belege. Bei den Belegen kann es sich um
einen einfachen Verweis handeln, oder um direkte Belege mit Werk, Stellenangabe
und ggf. wörtlichem Zitat. Belege sollten immer durch einen Stern eingeleitet
werden. (Nur bei dem ersten Beleg kann der Stern ausnahmsweise weggelassen
werden und das auch nur dann, wenn dem Beleg keine Verweise vorhergehen.)

Verweise werden wie Zusätze mit geschweiften Klammern umschlossen,
wobei direkt nach der öffnenden Klammer ein Pfeilsymbol (`=>`) steht,
auf das die Zielmarke folgt. (Näheres siehe unten).

Beispiel:

    GRAMMATIK
      subst. I; -ae f.

      -us, -i m.:  {=> ibi_1}
      -um, -i n.:  {=> ibi_2}

Die allgemeine Form lautet:

    GRAMMATIK
      Wortart; Flexion [Genus]

      Flexion [Genus]: Beleg [* Beleg] ...
            .
            .
            .

##### 4.2.2.1 Mehrfachangaben in der Grammatik-Position

Manche Wörter können in mehreren unterschiedlichen Wortarten, Flexionen oder Genera vorkommen. 
In der Grammatik-Position können daher jeweils hinter der ersten Wortart, Flexion 
bzw. Genus-Angabe weitere Angaben mit "vel" (oder "vel raro", "vel semel", "vel bis") 
oder "et" (nur bei Wortarten) angeschlossen werden. (Siehe auch die Erklärungen zum 
Mehrfachanstz in der Lemmaposition unter 
"4.2.1.1 Doppel- oder Mehrfachansatz (mit Schlüsselwort VEL)")

**WICHTIG:** Im Unterschied zum Mehrfachansatz eines Lemmas (siehe oben), wo *vel* als
großgeschriebenes Schlüsselwort verwendet wird, muss das *vel* bei Mehrfachangaben innerhalb
der Grammatik-Position immer kleingeschrieben werden. Sonst kann der Computer beides nicht
auseinanderhalten. 

Beispiel 1 (alternative Wortarten):

   LEMMA intus
   GRAMMATIK adv. et praep.

Beispiel 2 (alternative genera):

    LEMMA sacerdos
    GRAMMATIK subst. III; -otis m. vel raro {=> sacerdos_1; sacerdos_2} f.

Beispiel 3 (alternative genera):

    LEMMA sacricola
    GRAMMATIK 
       subst. I; -ae m. vel semel ({=> sacricola_1}) f.

Beispiel 4 (alternative Flexionen): 

    LEMMA  1. sancio (-ccio, -ctio)
    GRAMMATIK
        verbum IV; sanxi {vel} -i(v)i, -itum, -ire

Beispiel 5 (Sonderfall: Flexion wird mit abweichendem Kasus und Numerus angegeben):

    subst. III; nom. pl. -es m. vel f.

Beispiel 6 (undeklinierbares Substantiv):

    LEMMA satan
    GRAMMATIK  subst. unbestimmt; indecl. m.

Die Bestimmung `indecl.` steht dabei anstelle der Deklination nach dem
Semikolon - nicht zu verwechseln mit der Nomen-Klasse vor dem Semikolon.


##### 4.2.3 Etymologie-Position

Der Aufbau der Etymologieposition unterscheidet sich leicht von dem der
Artikelkopfpositionen im engeren Sinne. Die Etymologieposition wird durch das
Schlüsselwort `ETYMOLOGIE` eingeleitet und besteht aus einer oder mehreren
"Etymologieangaben" ggf. mit davon durch einen Doppelpunkt getrennten
Sekundärliteraturangaben oder Verweisen. In Vorschau und Druck wird die
Etymologie-Position immer automatisch von runden Klammern umschlossen. Bei der
Eingabe können und dürfen diese "Etymologieklammern" *nicht* mit eingegeben
werden.

Eine Etymologieangabe wird wiederum von einer der folgenden Kombinationen
gebildet:

1. einzelnes lateinisches oder griechisches Etymon, z.B. `sacrificare` oder `σάκωμα`

2. eine Sprachangabe, ggf. gefolgt von einem Etymon, z.B. `theod. vet. sagên`.
   (Siehe dazu auch die Liste der erlaubten Sprachangaben im Anhang)

4. einem Zusatz gefolgt von einer der ersten drei Kombinationen, z.B. `{cf.} ital. zafferano`

5. nur einem Zusatz, ohne weitere Angaben, z.B. `{orig. et signif. inc.}`

Jeder Etymologieangabe kann ein Fragezeichen `?` vorangestellt werden, um anzudeuten, dass
diese Angabe unsicher ist, z.B. `?franc. inf. vet. +fillo`

Jedes Etymon kann durch einen vorangestellten Stern `*` (= sechszackiger Stern für 
"nicht-klassich", mlw-intern "Sternwörter" genannt) 
oder einem Plus-Zeichen `+` (= fünfzackiger Erschließungsstern) gekennzeichnet werden.

Auf jede Etymologieangabe dürfen durch einen Doppelpunkt getrennt, eine oder
mehrere Sekundärliteraturangaben folgen. Falls es sich um mehrere Literaturangaben
handelt, sollten sie jeweils durch einen Stern `*` (als Aufzählungszeichen) 
von den vorherigen Angaben abgetrennt werden - ähnlich wie das bei den Belegen 
in der Bedeutungsposition gemacht wird.

Gibt es mehr als eine Etymologieangabe, so sollte jede Eytmologieangabe am
besten immer in eine neue Zeile geschrieben werden. Sofern keine
Literaturangaben vorkommen, können Etymologieangaben aber auch durch ein Komma,
Semikolon oder einen überleitenden Zusatz (z.B. `{et}`) auch ohne Zeilenwechsel
voneinander getrennt werden. Sofern Literaturangaben im Spiel sind, sollte eine
nachfolgende weitere Etymologieangabe aber immer in eine eigene Zeile
geschrieben werden, da Kommata und Semikola auch in Literaturangaben vorkommen
können, und dann vom Computer nicht mehr immer eindeutig als Trennzeichen für
Etymologieangaben erkannt werden können.

**Beispiel 1** (Sprachangabe und Etymon):

    ETYMOLOGIE
      theod. vet. gunderebe

Sprachangaben müssen nicht extra markiert werden, da es sich dabei um eine endliche Menge
feststehender Begriffe handelt, von denen keiner zugleich ein Etymon sein kann.
Siehe dazu auch die **Liste der Erlaubten Sprachangaben in der Etymologie-Position im Anhang**!

Liefert: (*theod. vet.* gunderebe)

**Beispiel 2** (Etymon mit Sekundärliteraturangabe mit voran gestelltem Zusatz):

    ETYMOLOGIE
      ἀκρόνυχος: * {de script. {/achronicus} cf.} ThLL. s. v.

Liefert:  (ἀκρόνυχος; *de script.* achronicus *cf. ThLL. s. v.*)

**Beispiel 3** (Mehrere aufeinander bezogene Etymologieangaben):

    ETYMOLOGIE
        {aut} theod. vet. hûnisc: [{v.} Althochdt. Wb. IV.; p. 1363sq.]
        {aut} theod. Hunne

Normalerweise werden die Literaturangaben in Vorschau und Druck durch ein Semikolon
abgetrennt. Falls die Literaturangaben jedoch im Quelltext in eckige Klammern
eingeschlossen werden, so wird das Semikolon zu Gunsten der Klammern unterdrückt.
Bei mehreren Literaturangaben müssen die eckigen Klammern immer den gesamten Block
von Literaturangaben umschließen.

(**Nicht zu vergessen:** Bei allen Literaturangaben (egal ob Sekundärliteratur oder 
Zitiertitel einer Primärquelle) müssen die Stellenangaben immer durch ein Semikolon 
von Autor und Werknamen abgegrenzt werden!)

Liefert:  *aut theod. vet.* hûnisc *[v. Althochdt. Wb. IV. p. 1363sq.] aut theod.* Hunne

**Beispiel 4a** (Mehrere Etymologieangaben, in jeweils einzelnen Zeilen notiert.):

    ETYMOLOGIE
        {cf.} francog. vet. huche
        {ex} huge
        {et} anglosax. hutch: * {v.} Stotz, Handb. 1,III; § 14.5

**Beispiel 4b**:

    ETYMOLOGIE
            Iudaica;
            {cf. ital.} Giudècca:
                * {v.} Battaglia, Dizionario. VI.; p. 862

**Beispiel 4c**:

    ETYMOLOGIE
        theod. vet. hûs[o]
        {et} thing: * {v.} Schade, Altdt. Wb. I.; p. 103^a. 434^a;
        anglosax. husting: * {cf.} DMLBS vol. I.; p. 1187^c

**Beispiel 5** (Etymologieangabe mit Fragezeichen und Erschließungsstern):

    ETYMOLOGIE
        ?franc. inf. vet. +fillo:
          * {v.} A. de Sousa Costa, Studien zu volkssprachigen
            Wörtern in karolingischen Kapitularien. 1993.; p. 334sqq.

**Beispiel 6** (Etymologieangabe mit "nicht-klassischem" Stern ("Sternwort")):

    ETYMOLOGIE
        *fello: {v.} Stotz, Handb. 2,III; § 34.6

**Beispiel 7** (Etymologie-Erläuterung statt Angabe eine Etymons mit mehreren Sekundärliteraturangaben):

    ETYMOLOGIE
      {fere in chartis Italiae septemtrionalis}:
        * {cf.} Wartburg, Frz. etym. Wb. IV.; p. 728sqq.
        * Battisti-Alessio, Diz. etim. ital. III.; p. 2111 s. v. "2. ischia"
        * F. Giger et al., Dicziunari Rumantsch Grischun X.; p. 130sq. s. v. "1. isla"

Im Druck und in der Vorschau sieht es so aus (hier ein etwas größerer
Ausschnitt, der auch das Lemma und die Grammatikangabe umfasst):

**insula** (-sola) *vel raro* (*fere in chartis Italiae septemtrionalis; cf. Wartburg, Frz. etym. Wb. IV. p. 728sqq.; Battisti-Alessio, Diz. etim. ital. III. p. 2111 s. v.* 2. ischia; *F. Giger et al. Dicziunari Rumantsch Grischun X. p. 130sq. s. v.* 1. isla) **iscla** (ys-), -ae *f.*

**Beispiel 8** (Eine relativ komplexe Etymologieangabe):

    ETYMOLOGIE 
      sanctus {et} monachus, 
      {per analogiam ad} sanctimonialis {formatum}, 
      {ut vid.}

Da hier keine Literaturangabe vorkommt, kann auch alles in eine Zeile geschrieben werden:
`sanctus {et} monachus, {per analogiam ad} sanctimonialis {formatum}, {ut vid.}`

Weitere Beispiele für Etymologien finden sich auf Sync&Share im 
Ordner "MLW\MLW-Testartikel\Musterwort".

**Übrigens**: "In manchen der obigen Beispiele wird das Kürzel *s. v. (sub voce)* verwendet. 
Damit weisen wir auf ein Stichwort in der zitierten Sekundärliteratur hin. Wir 
unterscheiden dabei im Druckbild, ob es sich bei dem Stichwort um ein "Originalwort" 
oder um einen modernen Begriff handelt. Handelt es sich um einen sprachlichen Eintrag 
(lat., ital., theod. vet. etc.), dann wird das Wort gerade und ohne Häkchen 
(und wohl meist auch klein) geschrieben. Handelt es sich um einen modernen 
Sachbegriff ('Realien'), dann kommt das Wort in Häkchen und wird kursiviert. 
In der Sache läuft das meistens darauf hinaus, dass Stichwörter aus Wörterbüchern 
gerade und klein formatiert werden, Stichwörter aus Sachlexika dagegen kursiv 
und in Häkchen formatiert werden:  *v. Lexer ..., s. v.* scheffel 
**ABER** *v. LexMA ..., s. v.'Scheffel'*.

In VSC wird das folgendermaßen eingegeben: gerade zu setzende Stichwörter werden 
in Anführungszeichen als "Stichwort" notiert, kursiv und in einfache Anführunsgzeichen 
zu setzende werden als 'Stichwort' notiert. 

Ein fiktives Beispiel einer Etymologieklammer zu facitergula:

    ETYMOLOGIE 
        facies
        {et} tergo: * {cf.} Dt. Rechtswb. VI.; p. 227 s. v. 'Schnupftuch'
        * Battaglia, Dizionario. IV.; p. 589 s. v. "fazzoletto"

Das wird im pdf so ausgegeben:
(facies *et* tergo; *cf. Dt. Rechtswb. V. p. 627 s. v. ‘Schnupftuch’; Battaglia, Dizionario. IX. p. 589 s. v.* fazzoletto)

Alternativ ist auch die folgende Schreibweise möglich, bei der mit die Kursivierung umgekehrt wird,
um die Grade-Stellung des Stichwortes zu erzwingen:

    ETYMOLOGIE 
        facies
        {et} tergo: * {cf.} Dt. Rechtswb. VI.; p. 227 s. v. 'Schnupftuch'
        * Battaglia, Dizionario. IV.; p. 589 s. v. {/fazzoletto}

Stichwörter dürfen dabei auch mit einem fünfzackigen Erschließungsstern annotiert werden, der im 
MLW-Quelltext durch ein Plus-Zeichen `+` symbolisiert wird (weil das Plus-Zeichen einen Zacken weniger hat,
als das Stern-Zeichen `*`, das bereits für den sechszackigen "nicht-klassische" Stern Verwedung findet:

    ETYMOLOGIE
        bis; {cf.} prov. besa, francog. vet. besson {sim.} :
        * {v.} Wartburg, Frz. etym. Wb.; I p. 381^b s. v. {/+bissa}'''

Diese Art der Annotation ist nicht auf die Etymologie beschränkt, sondern kann überall 
in Stellenangaben zur Sekundärliteratur verwendet werden.


#### 4.2.4 Artikelkopf-Positionen

Alle Positionen im Artikelkopf (*Schreibweisen-Position, 
Form-Position, Struktur-Position,
Gebrauchs-Position, Metrik-Position, Verwechslungs-Position*) sind nach
ein- und demselben Schema aufgebaut:

Am Anfang steht ein Schlüsselwort, das anzeigt, um welche
Artikelkopf-Position es sich handelt, z.B. `SCHREIBWEISE`, danach folgen
*Varianten* mit *Belegen*. Die *Varianten* können hierarchisch 
in Kategorien und Unterkategorien eingeordnet sein. 

**Wichtig**: Die Hierarchieebene wird im Artikelkopf anhand der
Einrückungstiefe festgestellt. Wenn die Einrückung nicht stimmt, wird
die Gliederungsebene falsch zugeordnet (Ihr könnt das in der
übersichtlichen Vorschau überprüfen), oder die Position im Artikelkopf
kann überhaupt nicht eingelesen werden!

Beispiel 1:

    SCHREIBWEISE
      script.:
        hym-:   {=> ziel_1}
        em-:    * CHRON. Fred.; 2,35sqq. capit.; p. 43.; 2,36 p. 60,10
        ym-:    * CHART. Sangall.; A 194
        impir-: {=> ziel_2}

Hier ist `script.` die Bezeichnung einer Kategorie, während `hym-`,
`em-`, `ym-`, `impir-` jeweils Varianten innerhalb dieser Kategorie
repräsentieren. Die auf die Varianten folgenden Belege können entweder
Verweise (z.B.: `{=> ziel_1}`), die auf eine frei vergebene
Sprungmarke ("Anker") verweisen, sein (siehe weiter unten die Erklärungen zu
"Verweise") oder Belegstellen, bei denen immer ein Werk gefolgt von
Stellenangaben und (in Ausnahmefällen) Zitaten angegeben wird. 
Es kann auch eine Kombination von Verweisen und Belegstellen angegeben werden. 
Dabei müssen Belegstellen mit dem Zusatz {adde} zwischen dem Stern 
und dem Autornamen markiert werden:

    sagr-: {=> sacro_1; sacro_2} 
         * {adde} DIPL. Karlom. (I.); 49 p. 68,37
         * FORM. Bitur.; 19

Das genaue Format wird weiter unten unter "Belege" näher beschrieben.

Die Bezeichnungen von Kategorien, Varianten etc. wie z.B: `script.`, `script et
form`, `acc.sg. n.`, müssen nicht (und können nicht) von Hand kursiv gesetzt
werden. Das macht das Programm automatisch. 

Beispiel 2:

    FORM
      form. sing.:
        gen.:
          -ri: * {=> ziel_3}
               * {adde} ANNAL. Plac.; a. 1266; p. 516,21
          -iae: * CHRON. Fred.; 2,33. p. 56,22.; 2,35
        abl.:
          -um: * CHRON. Fred.; 2,15.; 2,35sqq. capit. p. 43

Bei diesem Beispiel ist `form. sing.` eine Kategorie, die die
Unterkategorien `gen.` (Genitiv) und `abl.` (Ablativ) enthält.

Man beachte, dass für die Variante `-ri` unterhalb von `gen.`  mehrere
Belege angegeben sind. Sobald mehr als ein Beleg angeben ist, muss
jeder neue Beleg durch einen Stern `*` eingeleitet werden. Als ein
Beleg zählt dabei ein Verweis oder ein Werk mit einer oder
mehreren Stellenangaben. Eine weitere Stellenangabe innerhalb eines
Werks darf nicht mit einem Stern eingeleitet werden, sondern wird von
der vorhergehenden Stellenangabe oder vom Werk mit einem einfachen
Semikolon abgetrennt, wie z.B. `CHRON. Fred.; 2,33. p. 56,22.; 2,35.`

Die allgemeine Form für Artikelkopf-Positionen kann man grob so
darstellen:

    SCHLÜSSELWORT
      [Kategorie:]
        Unterkategorie:
          Variante: Beleg
                .
                .
                .
       .
       .
       .

*WICHTIG:* Kategorien, Unterkategorien und Varianten müssen jeweils in eine
**neue Zeile** gesetzt werden und die Einrückungen müssen die
Verschachtelungstiefe widerspiegeln! Die Verschachtelungstiefe ist gegenwärtig
auf *maximal vier Ebenen* beschränkt.

**Fehlermeldung wegen fehlender Belege unterdrücken**: 
Wenn auf der untersten Ebene auf den Doppelpunkt keine Belege oder Verweise
folgen, so wird das normalerweise mit einer Fehlermeldung wegen "fehlender 
Belege" quittiert. Es gibt aber Ausnahmefälle, in denen sich Verweise oder
Belege erübrigen z.B. dann, wenn der ganze Artikel sowieso nur aus einem Verweis 
besteht. In diesem Fall kann man durch einen Platzhalter in Form eines leeren
Zusatzes `{}` die Fehlermeldung unterdrücken. Beispiel:

    FORM
        in tmesi: {}

*Wichtig*: Das Platzhalterzeichen sollte wieder entfernt werden, sobald 
stattdessen die Belege und Verweise eingefügt werden. Andernfalls könnten
überschüssige Satzzeichen im Text erscheinen. 

Übrigens funktioniert dieser Platzhaltertrick nur im Artikelkopf, also nach
`SCHREIBWEISE`, `FORM`, `STRUKTUR` etc., nicht aber in der Grammatik-Position.

##### 4.2.4.1 Uneindeutige Artikelkopfpositionen

Nicht immer kann eine Angabe im Artikelkopf ganz eindeutig einer der sechs
Kopfpositionen Schreibweise, Form, Struktur, Gebrauch, Metrik, Verwechselbarkeit
zugeordnet werden (Etymologie und Grammatik gehören nicht in die Liste, da es
sich nicht im strengen Sinne um Positionen im Artikelkopf handelt). Der
Klassiker in dieser Hinsicht ist die Angabe `script. et form.`, die man mit
gleichem Recht der Schreibweisen-Position wie auch der Form-Position zuordnen
kann. In diesem Fall ist die Zuordnung zu jeder der in Frage kommenden
Positionen legitim.

*Aufgepasst: Zur Zeit wird die Richtigkeit der Zuordnung einer Angabe zu
einer der Kopfpositionen von MLW-DSL noch nicht überprüft. Man könnte
also `script. et form.` auch unter `METRIK` angeben. In einer künftigen
Version soll das mit einer Fehlermeldung quittiert werden. Bis dahin ist
leider etwas Sorgfalt gefragt, wobei solche Fehler aber glücklicherweise
ins Auge springen.*

##### 4.2.4.2 Besonderheit bei usu-Angaben

In der Druckausgabe und in der Vorschau werden aufeinanderfolgende Angaben
im Artikelkopf für gewöhnlich durch ein deutliches Spatium getrennt.
Bei Gebrauchsangaben ("usu...") wird davon abweichend stattdessen häufig
ein einfaches Semikolon verwendet, um verschiedene Gebrauchsangaben
voneinander zu trennen. "Usu" steht dabei nur einmal am Anfang, z.B.:
"partic. praes. usu adi.: ...; subst.: ..."

Um diese Art der Ausgabe zu bewirken, werden die zusammengehörigen Angaben
untereinander ohne Einrückung notiert und alle Folgeangaben durch ein `[usu]` in
eckigen Klammern eingeleitet:

    GEBRAUCH
        usu absol.:  {=> sancioA6}
        [usu] refl.: {=> sancioA4; sancioA5}

    GEBRAUCH
       partic. praes. usu adi.: {=> ziel_1}
       [usu] subst.: {=> ziel_2}

Bei der Ausgabe fällt das in eckigen Klammern notierte `[usu]` weg, und das
Ergebnis könnte etwa so aussehen: "usu absol.: l.6; refl.: l.25" bzw. "partic.
praes. usu adi.: l.12; subst.: l. 22"


#### 4.2.5 Bedeutungsposition

Die Bedeutungsposition besteht aus einer Folge von Bedeutungs-,
Unterbedeutungs-, Unterunterbedeutungs-, ...-Blöcken, die bis zu sechs
Ebenen tief verschachtelt werden können.

Wie schon die anderen Abschnitte des Wörterbuchartikels wird jeder
Bedeutungsblock durch ein Schlüsselwort eingeleitet. Auf der obersten
Bedeutungsebene lautet das Schlüsselwort `BEDEUTUNG`. Tiefere
Bedeutungsebenen werden mit den Schlüsselwörtern `UNTER_BEDEUTUNG`,
`UNTER_UNTER_BEDEUTUNG`, `UUU_BEDEUTUNG` bis hin zu `UUUUU_BEDEUTUNG`
(für die 6. Ebenentiefe) eingeleitet.

Ab der zweiten Bedeutungsebene MÜSSEN mehrere Bedeutungsblöcke derselben Bedeutungsebene 
aufeinander folgen (mit Ausnahme von Anhängern). Dazu leitet man die folgenden Blöcke
der gleichen Ebene einfach mit demselben Schlüsselwort ein:

    BEDEUTUNG ...
        UNTER_BEDEUTUNG ... /* erster Bedeutungsblock der zweiten Ebene */
        UNTER_BEDEUTUNG ... /* zweiter Bedeutungsblock der zweiten Ebene */

##### 4.2.5.1 Bedeutungsangabe

Auf das Schlüsselwort (`BEDEUTUNG`, `UNTER_BEDEUTUNG`, ...) folgen entweder die
Interpretamente, d.h. die deutsche und die lateinische Übersetzung, oder eine
kategoriale Angabe wie z.B. `proprie`, `in univ.`, `de rebus`, `sensu communi`
etc. Die kategoriale Angabe beschreibt in welcher Hinsicht oder in welchem Sinne
die folgenden Belege das beschriebene Wort verwenden.

Interpretamente werden immer als eine durch Kommata getrennte Liste von Wörtern
notiert. Dabei werden zuerst die lateinischen Interpretamente angegeben und dann
durch einen Doppelstrich bzw. zwei Minuszeichen `--` davon getrennt die
deutschen Interpretamente, z.B. `consecratus -- geweiht`. Später im Druck
erscheint dann statt des Doppelstrichs ein langer Strich.

Kategoriale Angaben bestehen einfach aus einer Folge von Wörtern. Die Bedeutungsangabe,
ob Interpretament oder kategoriale Angabe, wird immer durch einen Doppelpunkt
`:`abgeschlossen. (Der Computer erkennt anhand des Vorhandenseins oder Fehlens
des Doppelstriches `--`, ob es sich bei der Bedeutungsangabe um die Angabe von
Interpretamenten oder eine kategoriale Angabe handelt.)

Beispiel 1 (Interpretament):

    BEDEUTUNG  iussum, praeceptum, mandatum --
               Befehl, Anweisung, Auftrag:

Beispiel 2 (Interpretament und kategoriale Angabe):

    BEDEUTUNG  actio subeundi, obreptio (furtiva, illicita) --
               das Einschleichen, (heimliche, unbefugte) Eindringen, Herankommen (zu, an);
               usu attrib. {=> ziel}:

Dieses Beispiel ist zugleich ein Beispiel für eine mehrfache Bedeutungsamgabe.
(Näheres dazu siehe unten.) Wie an der zweiten Bedeutungsangabe in diesem Beispiel
(`usu attrib. {=> ziel}`) zu erkennen, dürfen Bedeutungsangaben auch
Verweise (`{=> ziel}`) oder andere redaktionelle Zusätze in
geschweiften Klammern (im Beispiel nicht zu sehen) enthalten.

Beispiel 3 (Kategoriale Angabe):

    UUU_BEDEUTUNG proprie:

In diesem Fall handelt es sich übrigens um eine Bedeutungsangabe der
vierten Ebene (dritte Unterbedeutungsebene). Die allgemeine Form für
Interpretamente sieht so aus:

    BEDEUTUNG  significatio 1, significatio 2, ... --
               Bedeutung 1, Bedeutung 2, ... :

Und für kategoriale Angaben:

    BEDEUTUNG  kategoriale Angabe:

Was weder bei Interpretamenten noch bei kategorialen Angaben vergessen
werden darf, ist der Doppelpunkt `:` am Ende der Bedeutungsangabe!

Unabhängig davon, ob die Bedeutungsangabe aus Interpretamenten oder
einer kategorialen Bedeutungsangabe besteht, schließen sich daran
entweder die Belege oder Unterbedeutungen an.

###### 4.2.5.1.1 Mehrfache Bedeutungsangaben und Qualifizierungen

In manchen Fällen ist es notwendig mehrere Paare von lateinischen und
deutschen Bedeutungen in ein und derselben Bedeutungsangabe unterzubringen
z.B. dann, wenn es um den transitiven oder intransitiven
Gebrauch desselben Wortes geht, bei dem sich die transitive oder
intransitive Bedeutung ja nicht wirklich unterscheiden. (Würden sich die
Bedeutungen unterscheiden, dann wäre es sowieso geboten, zwei
Bedeutungsblöcke mit je eigenen Belegen anzulegen.) Beispiel:

    interponere, inserere -- dazwischenstellen, (dazwischen) einfügen, einschieben (in);
    {intrans. i. q.} interesse, interpositum esse -- dazwischen liegen, stehen:

Solche mehrfachen Bedeutungsangaben werden so notiert, dass weitere
Bedeutungsangaben durch ein Semikolon getrennt an die erste Bedeutungsangabe
angefügt werden. Der Computer setzt die zusätzlichen Bedeutungen für die
Druckausgabe automatisch in Klammern. Im Druckbild sieht die Bedeutungsangabe
oben etwa so aus: "*interponere, inserere — dazwischenstellen, (dazwischen)
einfügen, einschieben (in; intrans. i. q. interesse, interpositum esse —
dazwischen liegen, stehen):*"

Nicht nur mehrfache Bedeutungen, auch andere Arten von Qualifizierungen,
seien es Ergänzungen zur Bedeutungsangabe oder nähere Bestimmungen,
können - übrigens in beliebiger Anzahl - durch Semikola getrennt an die
Bedeutungsangabe angefügt werden:

    pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch, Tuch; usu liturg.;
    de re v. {=> p.32,63|-}:    

Im Druckbild: "*pannus, faciale, sudarium — Gesichtstuch, Schweißtuch,
Tuch (usu liturg.; de re v. p.32,63):*". 

Alternativ können solche Ergänzungen
auch in doppelten rungen Klammern an die Bedeutunsangabe angehängt werden.
(Sie müssen allerdings vor dem die Bedeutungsangabe abschließenden Doppelpunkt
stehen!). Das Druckbild bleibt dabei dasselbe::

    pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch, Tuch; usu liturg.
    ((de re v. {=> p.32,63|-})): 

Ausnahmsweise dürfen Qualifizierungen statt am Ende, auch innerhalb
der Bedeutungsangabe stehen. Dann müssen sie allerdings in doppelten runden
Klammern notiert werden::

    subi. homines ((animalia: {=> sequor47}; res: {=> sequor24}))
    i. q. adipisci, assequi, mereri -- erlangen, (verdientermaßen) bekommen:
 

###### 4.2.5.1.2 (Sekundär-)Literaturangaben innerhalb von Bedeutungsangaben

Bedeutungsangaben dürfen Verweise auf die Literatur (sowohl Primärquellen
als auch Sekundärliteratur) enthalten. Solche Angaben sollten 
– ähnlich wie entsprechende Einschübe
innerhalb von Belegen - in doppelten Runden Klammern notiert werden:

    UUU_BEDEUTUNG in iunctura "regale #sacerdoti.|um"
                  ((* {spectat ad} $VULG.; I Petr. 2,9)) 
                  ((* {de re v.} J. Fried, DtArch. 19. 1973.; p. 508sqq.)):

Es dürfen - wie im Beispiel zu sehen - mehrere solcher Angaben aufeinander folgen. 
Sie sollten dann jeweils
in einen eigenen von doppelten Runden Klammern eingeschlossenen Block notiert werden.
Der Computer ersetzt die doppelten Klammern später (d.h. nach der Strukturerkennung)
durch einzelne runde Klammern und ersetzt angrenzende schließende und öffnende
Klammern ")(" durch ein Semikolon, so dass die Ausgabe den Konventionen der Druckausgabe
entspricht, wo eine solche Serie von Angaben als Ganzes in runde Klammern eingeschlossen
wird, die Angaben einer Serie aber jeweils durch Semikola voneinander getrennt sind.

**Hinweis auf eine Fehlerquelle**: Wenn mehrere Literaturangaben aufeinander
folgen, so ist es die sicherste Methode, jede davon in ein eigens Paar doppelter
runder Klammern einzuschließen. Andernfalls kann es zu subtilen, aber schwer
durchschaubaren Fehlern kommen! 

Schreibt man etwa mehrere Belege in ein- und dasselbe
Paar doppelter runder Klammern und trennt sie (ähnlich wie bei Belegfolgen 
außerhalb der Bedeutungsangabe) lediglich durch einen Stern `*`, so wird
in der Ausgabe kein Semikolon zwischen die Belege gesetzt. 

###### 4.2.5.1.3 Belegtext und Lemmawörter (Junkturen) in Bedeutungsangaben

Bedeutungsangaben dürfen Belegtext und Lemmawörter (mlw-intern: "Junkturen") 
enthalten oder auch
ausschließlich aus Belegtext bestehen. Dabei können das Lemmawort oder die
Wortgruppe, die das Lemmawort enthält, in Anführungszeichen gesetzt werden,
sofern sie eine Verwendungsweise wiedergeben, wie man sie in den Originalwerken
finden kann:

    BEDEUTUNG adi. in iunctura "mare #infer.|um" de mare Tyrrheno ((* de re v.} Allg.
    Encyklopädie d. Wissenschaften u. Künste. II/18. 1840.; p. 158sq.)):

In der Vorschau und Druckausgabe wird der Text in Anführungszeichen automatisch
aufrecht gesetzt, im Unterschied zur ansonsten kursiven Bedeutungsangabe:

**I** *adi. in iunctura* mare -um *de mare Tyrrheno (de re v. Allg. Encyklopädie d. Wissenschaften u. Künste. II/18. 1840. p. 158sq.):*

Es ist auch möglich, die Notation `{/ ... }` zu verwenden, um bestimmte Bereiche
physisch gerade zu setzen (oder präziser um die Kursivierung der Umgebung zu invertieren),
aber die Notation mit Anführungsstrichen ist semantisch präziser, wenn es um
charakteristische Originalvorkommen geht. Ebenso sollten Bereiche, die nicht
Teil der eigentlichen Bedeutungsangabe sind, sondern Zusätze dazu
repräsentieren, in geschweifte Klammern geschrieben werden, um den Unterschied
semantisch deutlich zu machen, auch wenn das rein optisch nicht auffällt, da
auch die Bedeutungsangabe kursiv dargestellt wird.

Die Lemmawörter in Bedeutungsangaben können genauso notiert werden wie in
Belegen (siehe unten). Lemmawörter
werden dann mit einem `#`-Zeichen eingeleitet und enthalten die
Zeichenkombination `|.` oder `.|` um die Verdichtung anzugeben. (Der Teil des
Wortes wo der Punkt steht, wird gestrichen. Beispielsweise wird `#intu.|s` zu
"-s" verdichtet.)

Lemmawörter in Bedeutungsangaben werde kursiv gesetzt, es sei denn sie kommen
als Belegtext oder Teil eines Belegtextes in Anführungsstrichen vor. Dann werden
sie gerade gesetzt.

###### 4.2.5.1.4 Linné und andere Namenskürzel

Manchmal verwenden wir in der Bedeutungsposition moderne wissenschaftliche 
Bezeichnungen für Pflanzen und Tiere, auch als lateinische Interpretamente. 
Dazu gehört oft ein Kürzel, das sich auf den namensgebenden Wissenschaftler 
bezieht. Es wird am besten als Zusatz in geschweiften Klammern eingeben: 
`{L.}`, `{Huds.}`, `{(L.) Scop.}`. 

###### 4.2.5.1.5 Beispiele zur Bedeutungsangabe

Beispiel einer Bedeutungsangabe mit nachfolgenden Belegen:

    BEDEUTUNG  capital, rica -- Kopftuch:

        * TRANSL. Libor.  I; 32 "raptis  feminarum #facitergul.|is  (fa[s]citergiis  {var.  l.})."

        * TRANSL. Libor. II; 20 "nuditatem  membrorum  illius {(puellae)}  tegere  festinarunt
          fideles clerici et  laici  inprimis cum eorum #faciter.|cula, dein vestibus solitis."

Beispiel einer Bedeutungsangabe mit nachfolgender Unterbedeutung:

    BEDEUTUNG potestas, dominatio, dicio --
      Macht, Herrschaft, Herrschaftsgewalt; plur. sensu sing.: {=> imperator40m_2}:

        UNTER_BEDEUTUNG  proprie:

            UNTER_UNTER_BEDEUTUNG  in univ.:

                * ALFAN. premn. phys.; prol. 4 p. 1 "semet ipsum ... iuste privat
                  #imperi.|o, quisquis nec se cognoscit nec ea, quibus imperat."

                * EPIST. Worm. I; 34 p. 63,1 "in maximo #imperi.|o minima est licentia
                  {v. notam ed.}" 

Beispiel für mehrere aufeinanderfolgende Bedeutungsblöcke derselben Ebene:

    BEDEUTUNG  pannus, faciale, sudarium -- Gesichtstuch, Schweißtuch,
      Tuch; usu liturg.; de re v. {=> p.32,63|-}:

      * CATAL. thes. Germ.; 28,11 (post 851) "#facitergul.|um III"

    BEDEUTUNG  capital, rica -- Kopftuch:

      * TRANSL. Libor.  I; 32
        "raptis  feminarum #facitergul.|is  (fa[s]citergiis  {var.  l.})."

**Sonderfall: Keine Bedeutungsangabe** In seltenen Fälle, z.B. bei
Ausrufen wie "hahaha" gibt es keine Bedeutungsangabe. In diesem Fall
wird *anstelle* der Bedeutungsangabe das Schlüsselwort "OHNE" notiert.
Beispiel:

    LEMMA hahae (ha ha he)
          {interi.} {(ir)ridendi} {(cf. ThLL. VI/3. p. 2513, 56sqq.)}

    BEDEUTUNG OHNE

      * LIUTPR. leg.; 21 "cum Nicenam Chalcedonensem {(sc. synodum)}
      ... edicerem : ‘#{ha! ha! he!}’, ait patriarcha, ‘Saxonicam dicere
      es oblitus.’"

      * VITA NORB. I; 10 p. 680,51 "demon insultando clamabat:
      ‘#{ha! ha! he!}’"


##### 4.2.5.2 Anhänger

Zu jeder Bedeutung, Unterbedeutung, Unterunterbedeutung etc. darf es einen oder
mehrere "Anhänger" geben, die eine Ergänzung oder Differenzierung der Bedeutung,
an die sie angehängt sind, vornehmen ohne aber eine eigene Bedeutung zu bilden.

Anhänger werden genauso notiert wie Bedeutungen, Unterbedeutungen
etc., nur dass sie durch das Schlüsselwort `ANHÄNGER` eingeleitet
werden. Es empfiehlt sich, den "Anhänger" genauso weit einzurücken,
wie die (Unter-)Bedeutung, an die der Anhänger geknüpft ist.

Beispiel:

    UNTER_BEDEUTUNG de religione prava, moribus pravis:

      * RADBERT. corp. Dom.; 6,68  (add rec. 4) "resonabat ...
      ecclesia #inept.|ae vocis {(sc. infidelis)} clamoribus."

      * CHART. Ror.; 10 p. 155,31 "personam vestram {(sc. Gerberti)}
      de pravo intellectu et #inept.|o dogmate multipliciter arguebat
      {homo arrogans.}" {al.}

    ANHÄNGER immoderatus, improbus -- (un-)mäßig, übermäßig:

      * CAND. FULD. Eigil. II; 5,3 "qui {(sc. abbas)} ... gregem sibi
      commissum ... stimulis agitabat #inept.|is."

In diesem Beispiel ist die Bedeutung `immoderatus, improbus --
(un-)mäßig, übermäßig` als Anhänger zu der Unterunterbedeutung
`de religione prava, moribus pravis` notiert, und folgt darauf
ohne Nummerierung.

##### 4.2.5.3 Aufzählungen

Eine weitere atypische Art von Bedeutungsblock ist die Aufzählungsliste,
bei der die Belege unterhalb der Bedeutung noch einmal unter bestimmte
Stichworte subsummiert werden. Diese Stichworte müssen jeweils durch
das Schlüsselwort `AUFZÄHLUNG` eingeleitet werden:

    UUU_BEDEUTUNG  c. synonymis vel explicationibus vel iuxtapositis:

        AUFZÄHLUNG cintgrevin:
            * CHART. Rhen. med.; III 1284 p. 933,22 (a. 1255) "testes ... sunt:
              ... Heinricus viceplebanus, Sifirdus et Cunradus #iudic.|es, 
              vulgo dicti cintgrevin."
        
        AUFZÄHLUNG inquisitor:
            * CHART. Burgenl.; I 347 p. 238,22 (a. 1255) "cum ... #iude.|x sive
              inquisitor essemus constitutus {(sc. episcopus)} {eqs.}"
        
        AUFZÄHLUNG missus:
            * OTTO MOR. hist.; p. 1,15 "hunc libellum a me Ottone #iudic.|e, 
              qui dicor Morena, ac misso domini Lotharii tercii imperatoris 
              ... scriptum perlege."

Die Stichworte - in diesem Beispiel: cintgrevin, inquisitor und missus - werden
in der Ausgabe als gesperrter Text angezeigt. Sie müssen daher nicht noch einmal
explizit mit "{! ... }" gesperrt gesetzt werden. Es ist, auch wenn das in diesem
Beispiel nicht vorkommt, möglich zu einem einzelnen Aufzählungspunkt mehrere
Belege anzugeben.

##### 4.2.5.4 Absätze zwischen sehr langen Bedeutungsblöcken

Gerade auf den obersten Gliederungsebenen können Bedeutungsblöcke sehr lang
werden und sich über eine oder sogar mehrere Spalten erstrecken. Als
Hilfe für die Leserinnen und Leser ist es daher möglich, vor den Schlüsselwörtern
`BEDEUTUNG` und `UNTER_BEDEUTUNG` das Schlüsselwort `ABSATZ` einzufügen und
dadurch einen Absatzumbruch vor dem entsprechenden Bedeutungsblock zu erzwingen.
Beispiel:

    LEMMA sanus
    ...
    BEDEUTUNG bona valetudine utens, incolumis, salvus -- gesund, unversehrt, heil:
    ...
    ... /* sehr großer Bedeutungsblock mit vielen, tief gestaffelten Unterbedeutungen
    ...
    
    ABSATZ

    BEDEUTUNG sanitati conducibilis, saluber -- der Gesundheit zuträglich, heilsam:
    ...

**HINWEIS**: Das Schlüsselwort `ABSATZ` muss dabei immer in eine eigene Zeile
geschrieben werden. Es darf *nicht* in derselben Zeile stehen wir das
Schlüsselwort `BEDEUTUNG` oder `UNTER_BEDEUTUNG`.

#### 4.2.6 Belege

Belege bestehen (fast) immer aus einer Autor-Werk-Angabe (mlw-intern: "Zitiertitel"), 
auf die - jeweils durch ein Semikolon abgetrennt - ein oder mehrere 
Stellenangaben folgen, die mit einem Zitat versehen sein können, aber 
nicht müssen. Belege werden immer durch
einen Stern `*` eingeleitet und sollten am besten immer in einer eigenen Zeile
beginnen. Gibt es nur einen einzigen Beleg, dann kann der Stern ausnahmsweise
auch weggelassen werden. Belege dürfen an jeder Stelle, auch innerhalb der
wörtlichen Zitate, Zusätze und Hinweise enthalten, die wie immer in geschweifte
Klammern eingeschlossen werden müssen. Im Druck erscheinen sie dann kursiv.

Beispiel 1:

    * WILLIB. Bonif.; 5 p. 19,3 "ut patris cuiuslibet spiritalis se
    subiungerent fratres #imperi.|o."

Autor und Werk werden in der abgekürzten Form angegeben. 
Die im MLW verwendeten Siglen für die Autor- und Werknamen sind im 
Abkürzungs- und Quellenverzeichnis (mlw-intern: Zitierliste, 
Opera-Maiora-Liste) und in der sog. Opera-Minora-Liste aufgeschlüsselt.
Der Autorname kann ebenso wie die Werkangabe aus mehreren Worten
bestehen und Abkürzungspunkte enthalten. Wichtig ist, dass der
Autorname komplett in Großbuchstaben geschrieben wird. Nur so kann der
Computer unterscheiden, wo die Autorangabe aufhört und wo die
Werkangabe anfängt. Die Stellenangabe folgt der bei dem gegebenen Werk
üblichen Form, so wie sie in der Liste der opera maiora oder der Liste
der opera minora verzeichnet ist.

Beispiel 2:

    * ADAM gest.; 4,16 p. 244,1 "sunt et aliae {(insulae)} interius,
    quae subiacent #imperi.|o Sueonum."

Dieses Beispiel enthält innerhalb des Zitats einen Zusatz, `{(insulae)}`

Beispiel 3:

    * OTTO FRISING gest.; 2,13 p. 116,20 "vix ... aliquis ... vir
    magnus ... inveniri queat, qui civitatis suae non sequatur
    #imperi.|um."; 2,16 p. 118,28 "qui {marchio} pene solus ex Italiae
    baronibus civitatum effugere potuit #imperi.|um."

Bei diesem Beispiel folgen auf die Werkangabe zwei Stellenangaben,
jeweils mit Zitat. Die zweite Stelle wird mit einem Semikolon an die erste angeschlossen.

Die allgemeine Form von Belegen kann man so beschreiben:

    * AUTOR 1 Werk 1; Stelle 1 ["Zitat 1"]; Stelle 2 ["Zitat 2"]; ...

    * AUTOR 2 Werk 1; Stelle ...
       .
       .
       .

Zusätze können einzelnen Belegen, Beleggruppen (Folge von Zitaten aus ein- und
demselben Werk) oder einem ganzen Bedeutungsblock zugeordnet werden.

Als Ausnahmefall kann anstelle eines Beleges auch nur ein Verweis auf einen 
an anderer Stelle zitierten Beleg angegeben werden:

    * {=> sancioA4}

##### 4.2.6.1 Fremdautoren

Auch die Namen von Fremdautoren (das sind Quellentexte, die nicht in den Bereich 
des MLW fallen) werden in der Eingabe immer groß geschrieben,
obwohl sie im Druckbild nicht in Kapitälchen gesetzt werden. Der Computer
erkennt sie als solche durch Nachschlagen in den MLW-Zitierlisten (maiora und minora). 
Wird ein Autorname dort nicht gefunden, dann handelt es sich um einen Fremdautor, 
dessen Name nicht in Kapitälchen gesetzt wird.

Es gibt aber Fälle, in denen Fremdautoren ein- und denselben Namen tragen wie
ein MLW-Autor. In diesen Fällen muss der Fremdautorname durch ein
vorangestelltes Dollarzeichen explizit als solcher markiert werden. Beispiele:

    (($PS. HIPPOCR. ad Maecen.; 11))
    
    (($COMPUT. Carth.; 2,14))

Es können aber auch konsequent alle Fremdautoren mit einem Dollarzeichen 
versehen werden: das klappt ebenfalls.

Die Septuaginta wird als Fremdautor mit `$LXX` notiert. Anders als andere
Fremdautoren erscheint sie in der Ausgabe nicht in normaler
Groß-Kleinschreibung, sondern - wie in der Fachliteratur - üblich mit drei
Großbuchstaben als "LXX". 

Vier griechische Fremdautoren, die bisher ohne Namen zitiert wurden, werden 
nun mit den folgenden Namen (und OHNE Werkangabe) zitiert:

`ARIST.` (nach Liddell/Scott und ThLL)

`NEMES.` (nach Lampe und ThLL)

`PAUL. AEG.` (nach Liddell/Scott und ThLL)

`DION. AR.` (nach Lampe)



##### 4.2.6.2 Punkte am Ende eines Belegs

Jeder Beleg wird mit einem Punkt abgeschlossen, außer es steht danach eine schließende Klammer.
Bei der Eingabe darf der Punkt am Ende des Belegs nur innerhalb der
Anführungsstriche des letzten Zitats oder innerhalb der geschweiften Klammern
des letzten Zusatzes stehen. Andernfalls kommt es zu einer Fehlermeldung. Ein
fehlender Punkt am Ende des gesamten Belegs wird übrigens vom Computer
stillschweigend selbst ergänzt. 

Die automatische Ergänzung gilt nur für Punkte am Ende des gesamten Belegs.
Punkte, auf die noch etwas folgt, wie z.B. ein Punkt am Ende eines Zitats, das
von einem Zusatz gefolgt wird, müssen jedoch von Hand eingegeben werden. Im
folgenden Beispiel muss der Punkt nach "excrevisse" daher explizit angegeben
werden:

    * CHART. Hall.; 226 p. 216,42 "ut non videatur {fluvius} ...
      #inunda.|cione superflua ultra metas solitas excrevisse." {saepius.}


##### 4.2.6.3 Häufigkeitsangaben als Zusatz im Anschluss an Belege

Am Ende eines Bedeutungsblocks oder eines einzelnen Belegs kann mit "al.", "saepe" etc. 
auf weitere Belege hingewiesen werden. Es handelt sich dabei um weggelassene Belege 
aus dem restlichen Material. Die Angaben "al.", "saepius.", "saepe.", "persaepe.", 
dienen als Indikator für die relative Häufigkeit. Dieser Zusatz gehört nicht zum 
Belegtext und wird daher NACH dem Zitat (und nach den schließenden 
Anführungszeichen) gesetzt: {al.}

Am häufigsten bezieht sich diese Aussage zur Häufigkeit auf einen gesamten Bedeutungsblock.

Beispiel 1:

    * TRACT. de aegr. cur.; p. 189,20 "recipe ... nitrum, corallos albos, 
      #salgemm.|am dulcem et salsam."

    * ALBERT. M. animal.; 23,95 "aliquantulum #salgemm.|ae."

    * IOH. sacerd.; 38 "funde #salgemm.|a cum auripigmento rubeo 
      equaliter massato." {persaepe.}

Es kann allerdings auch vorkommen, dass alle weggelassenen Belege aus dem gleichen,
als letztes zitierten Werk stammen. Dann wird nach MLW-Praxis an der besagten 
Stelle ein "ibid." vor die Häufigkeitsangabe gesetzt.

Der Computer unterscheidet nun anhand des Hinweises "ibid.", ob sich der angehängte Zusatz 
nur auf den letztzitierten Beleg oder auf alle Belege unterhalb der entsprechenden 
(Unter-)Bedeutung bezieht. Ist der Hinweis "ibid." vorhanden, geht der Computer 
davon aus, dass sich der Zusatz nur auf den letzten Beleg bezieht.

Beispiel 2:

    UUUUU_BEDEUTUNG  abundantia sanguinaria -- Übermaß an Blut:

        * AESCULAPIUS; 22 p. 34,39 "si ex #sanguin.|e paralisis venerit."

        * TRACT. de aegr. cur.; p. 206,44 "si ex #sanguin.|e fit passio, 
          fiat minutio, si ex fleumate {eqs.}" {ibid. saepe.}

Nun kommt es gelegentlich aber auch vor, dass mitten in einer Bedeutungsgruppe 
ein Beleg aus einem Werk zitiert wird, das noch einige andere Belege liefern könnte, 
die hier nicht zitiert werden. Als Indikator der Häufigkeit der weggelassenen Belege 
dieses Werkes, wird an dieser Stelle (also nicht an der Schlussposition einer 
Bedeutungsgruppe) ein bloßes "al.", "saepius." etc.  gesetzt. Auf "ibid." wird hier 
verzichtet, da von der Position her klar ist, dass sich dieser Indikator nur auf 
das letztgenannte Werk beziehen kann.

Beispiel 3:

    UUUUU_BEDEUTUNG  spectat ad horas #sanguin.|is q. d.:

        * PLATEAR. pract.; 3,27 "circa mediam noctem accedit {(sc. febris ex dulci 
          phlegmate effecta)}, scilicet inter horas #sanguin.|is et flematis." {saepius.}

        * MAURUS phleb.; p. 8^{b} "si ... #sangui.|s habundet, ante terciam in matutinis 
          horis {(sc. debet flebotomari)}, quae sunt horae #sanguin.|is."


##### 4.2.6.4 Verschachtelte oder eingeschobene Belege

Verschachtelte Belege sind Belege, die innerhalb von Belegen stehen,
möglicherweise sogar innerhalb von wörtlichen Zitaten, z.B. wenn man en passant
darauf hinweisen will, dass dieselbe oder eine ähnliche Verwendungsweise auch an einer
anderen Stelle nachgewiesen ist.

Verschachtelte Belege werden in doppelten runden Klammern angegeben. Es sind
beliebig tiefe Verschachtelungen möglich. Im Druck werden daraus später wieder
einfache runde bzw. eckige Klammern. Beispiel:

    * FRID. II. IMP. art. ven.; 3 p. 37,23 "ex malo tractamento pigri
    efficiuntur falcones et #inept.|i (({sim.} 4 p. 68,29 "#inept.|iores
    ad volandum"))."

Hier ist der Verweis auf die Stelle 4 p. 68,29 desselben Werkes als
eingeschobener Beleg am Ende des Belegtextes angegeben. Wie man sieht,
dürfen eingeschobene Belege wiederum Zitate (Belegtext) enthalten. 

Einschübe können auch vollständige Zitiertitel  (d.h. Autor+Werk-Angabe) 
enthalten und sie dürfen auch mehrere Zitiertitel enthalten. Vor 
jedem Zitiertitel sollte immer ein Sternzeichen `*` stehen.

Die einzelnen Belege dürfen allerdings nicht wie in der Druckausgabe durch ein
Semikolon getrennt werden, da das Semikolon schon als Trennzeichen zwischen
Werk- und Stellenangabe dient. (Der Computer erkennt anhand des `*`-Zeichens, wo
ein neuer Zitiertitel anfängt.) Im folgenden Beispiel stehen innerhalb des Einschubs 
Belege, die eine zusätzliche Editionsangabe erfordern: nämlich ein Opus minus 
und ein Opus maius mit einer vom Kanon abweichenden Edition.:

    * BERNOLD. CONST. chron.; p. 400,2 ((ed. Pertz)) "sedem apostolicam
    tirannice invasit eamque ... plures annos -vit {Guibertus}
    ((* {inde} ANNAL. Gottwic.; a. 1085 ((MGScript. IX; p. 601,38))
      * {sim.} BERNOLD. CONST. chron.; a. 1087 p. 463,16
      ((ed. Robinson))))."

Man beachte die vier(!) schließenden Klammern am Ende, die tatsächlich zwei
schließende Doppelklammern sind, eine für den Einschub mit weiteren
Belegen, eine für die in den Einschub eingeschobene Editionsangabe.

Es empfiehlt sich, tief verschachtelte Einschübe durch Einrückungen
optisch kenntlich zu machen, indem man öffnende und schließende
Doppelklammern jeweils den Beginn oder den Abschluss einer Zeile bilden
lässt:

    * ALBERT. M. mort.; 2,7 p. 363^a,25  "{!calor} modicus #intrane.|us
      ((* animal.; 16,115 "'venae ... et ossa ex <<calore #intern.|o>>
          ((* ARIST.; p. 743^a,17 "τῆς ἐντὸς θερμὸτητος"))
          desiccantur'."
        * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intern.|um"
      ))."
    * mot. proc.; 1,1 p. 48,54 "tactus ... organum perfectum ... est #intern.|um et est cor
      ((* {sim.} animal.; 12,173 "in duobus ... sensibus, qui per medium #intern.|um
          accipiunt sensibilia {eqs.}"))" {ibid. al.}

Noch deutlicher wird die Struktur, wann man die öffnenden und schließenden Doppelklammern
komplett in eine eigene Zeile schreibt:

    * ALBERT. M. mort.; 2,7 p. 363^a,25 "{!calor} modicus #intrane.|us
      ((
        * animal.; 16,115 "'venae ... et ossa ex <<calore #intern.|o>>
          ((
            * ARIST.; p. 743^a,17 "τῆς ἐντὸς θερμὸτητος"
          ))
          desiccantur'."
        * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intern.|um"
      ))."

    * mot. proc.; 1,1 p. 48,54 "tactus ... organum perfectum ... est #intern.|um et est cor
      ((
        * {sim.} animal.; 12,173 "in duobus ... sensibus, qui per medium #intern.|um
        accipiunt sensibilia {eqs.}"
      ))" 
      {ibid. al.}

Welche der oben gezeigten Darstellungen man bevorzugt, ist im Wesentlichen eine
Frage des persönlichen Geschmacks. Es sind dabei aber folgende Regeln zu beachten:

1. Immer wenn ein neues Werk zitiert wird (und nicht nur eine weitere Stelle
   mit Beleg aus demselben Werk), dann sollte vor der Werkangabe bzw. der Autor-
   Werk-Angabe ein Stern `*` stehen.

2. Vor einem Stern `*` sollte kein Semikolon als Trennzeichen mehr stehen. Der
   Stern erübrigt das Semikolon als Trennzeichen! Das Semikolon sollte nur
   verwendet werden, wenn Stellenangaben aus demselben Werk aneinandergereiht
   werden:

   `* CATAL. thes. Germ.; 76,15 "#faciterulae II."; 40,5 VI "#vizregule."; 129^a,5
   "#facisterculas II."; 24,8  "#facitella X."; 114,8 VIII "#fezdreglę.";  6,24
   "#fasciutercule VII."; 92,6 "#fascerculę tres."; 21,20 IIII "#festregelę."`

3. Der Stern am Anfang eines Einschubs, direkt nach den beiden öffnenden runden
   Klammern, darf der Übersichtlichkeit halber weggelassen werden. Das Beispiel
   von vorhin kann daher mit weniger Sternen auch so notiert werden:

    `* ALBERT. M. mort.; 2,7 p. 363^a,25  "{!calor} modicus #intrane.|us
     ((animal.; 16,115 "'venae ... et ossa ex <<calore #intern.|o>>
       ((ARIST.; p. 743^a,17 "τῆς ἐντὸς θερμὸτητος"))
       desiccantur'."
       * {sim.} mort.; 3,6 p. 359^a,28 "habet {omne vivum} duplex calidum #intern.|um"
     ))"`

     `* mot. proc.; 1,1 p. 48,54 "tactus ... organum perfectum ... est #intern.|um et est cor
       (({sim.} animal.; 12,173 "in duobus ... sensibus, qui per medium #intern.|um
         accipiunt sensibilia {eqs.}"))"
       {ibid. al.}`

**Wichtig:** Zitierter Text in Einschüben muss immer in Anführungszeichen
stehen. Andernfalls kommt es entweder zu einer Fehlermeldung oder - schlimmer
noch! - der Text wird ohne Fehlermeldung als Werkangabe missverstanden.
Beispiel:

    * THIETM. chron.; 6,50 "marchionis filiam duxit {(sc. Daedi comes)} et ob
    hoc omne adeo #intum.|uit (("superbia #intum.|uit" * CHRON. Thietm.)),
    ut regi molestiam ... offerret."

Wären hier die Anführungsstriche vor superbia weggelassen worden, dann würde der
Computer das als einen Hinweis auf das Werk *superbia* des Autors `THIETM`.
verstehen.

##### 4.2.6.5 Editionsangaben (bei Zitiertiteln)

Nicht selten werden einer Primärquellen-Angabe Editionsangaben (d.h. bibliographische Angaben)
beigefügt. Dabei handelt es sich in der Regel um die Angabe der Edition (oder
Ausgabe) des Werkes, aus der zitiert wird. Solange ein Werk nach der für das
mittellateinische Wörterbuch kanonischen Hauptedition zitiert wird, wird die
Edition nicht eigens angegeben. Nur wenn aus einer anderen Edition zitiert wird,
wird die Editionsangabe notiert. Die häufigsten Fälle sind allerdings die 
sogenannten opera minora, d.h. Werke, die nicht im Quellenverzeichnis des MLW 
(Zitierliste) erfasst sind und deswegen zusätzlich eine Editionsangabe 
zwecks Auffindbarkeit benötigen.

Beispiel:

    * GESTA Vill.; 1 ((MGScript. XXV; p. 221,18)) "nobiles et procera ..." 

Hier ist `GESTA Vill.` das zitierte "opus minus". Zitiert wird nach der Edition `MGScript. XXV`. 
In diesem Beispiel folgt eine Editionsangabe auf die Stellenangabe (letztere ist optional!).

Editionsangaben, egal ob bei opera majora oder minora, werden so wie andere Einschübe immer in doppelte
Klammern eingeschlossen. Sie werden automatisch als solche erkannt, sofern sie durch
einen der Zusätze "ed." oder "cod." eingeleitet werden oder mit zwei bis drei, aber
nicht mehr als drei Großbuchstaben beginnen, wie z.B. "MGScript.".

In allen anderen Fällen müssen die Editionsangaben durch die Markierung `OM:` ausdrücklich
also solche gekennzeichnet werden, da sie sonst mit Sekundärliteraturangaben verwechselt 
werden können:

    * INSCR. sigill. Col.; ((OM: ArchDipl. 23. 1977.; p. 280)) "#sanct.|a Colonia ..."

**Tipp:** *Erscheint in der HTML-Vorschau eine opera-minora-Angabe kursiv, so wurde sie nicht richtig
also solche erkannt. Spätestens dann sollte man sie im Quelltext ausdrücklich durch die
Markierung `OM:` kennzeichnen.*

Auch bei den Editionsangaben muss die Stellenangabe von der Werkangabe davor 
durch ein Semikolon `;` getrennt werden! Die Stellenangabe als solche ist
jedoch optional und darf auch weggelassen werden:

    * VITA Aniani; 10 ((MGMer. III; p. 116,19)) ...
    
    * ORAT. Gild.; 8 ((ed. B. Bischoff, Anecdota. 1984.; p. 157)) ...

Etwas komplexere Beispiele sind:

    * VITA Austrig.; 7 ((MGMer. IV; p. 196,11)) (c. s. $IX.) 
      "cum ... nullum custodem ... ibidem {(sc. in ecclesiis castelli deserti)} 
      reperisset nisi tantum patentibus hostiis et #sacrariol.|o cluso et seris 
      obfirmato, voluit missas in illa baselica celebrare 
      (({cf.} p. 196,17 "regressi ... invenerunt {(sc. adminiculi)} hostia de
        sacrario illo aperta, que antea serata viderant, et cum introissent {eqs.}"))."

    * ANNAL. Ceccan.; a. 1196 ((MGScript. XIX; p. 293,50)) 
      "in apparatu ciborum ... media libri piperis et cinnamomi et 
      #soffe|.ran.|ae {@ safranum_4}."; a. 1208| p. 297,8 "in #sophrana."


##### 4.2.6.6 Zusatzangaben zu Zitiertiteln und opera minora

Nach Zitiertitel (d.h. Autor + Werk) und Stellenangabe können verschiedene Zusatzangaben folgen. 
Dies ist nicht auf die opera minora beschränkt. Diese Angaben dienen dazu,
zusätzliche Variablen zu einer zitierten Belegstelle anzugeben, wie z.B. den
Umstand, dass ein Text(teil) in Versform verfasst ist, also ein versifizierter
Einschub in einem Prosatext (vs.), oder, dass es sich um einen Urkunden-Einschub
in einer z.B. Historiographie handelt (chart.). Oder dass der Text ein Diplom
eines Königs/ Kaisers (dipl.), oder des Papstes (epist.) ist. [Diesen
Zusatzangaben kann durchaus ein Datum folgen: (chart. a. 1126) oder (dipl.
Otton. I.) etc.] Alternativ gibt es auch die Möglichkeit, dass mit dieser
Zusatzangabe auf den Überlieferungsstrang hingewiesen wird (rec. A) oder (cod.
XY), (add. AA), (gloss.). Weiters gibt es auch eine Zusatzangabe zum Datum:
(spur.) und (interp.) weist darauf hin, dass der Text 'unecht' oder 'verunechtet' bzw. aus einer
anderen Zeit stammt, als die Datierung des Gesamtwerkes. Wenn diese tatsächliche
Datierung bekannt ist, wird das zusätzlich vermerkt (spur. s. XII.) oder (spur.
post 1114) etc.

Wenn es sowohl eine Editionsangabe als auch weitere Zusatzangaben gibt, kann es 
zu einem 'Stau' an Zusatzangaben kommen, die in der Druckausgabe
in einer runden Klammer nach der Autor-Werk-Angabe zusammengefasst sind: 
(Editionsangabe; Datierung; Zusatz) oder (Editionsangabe; Zusatz; Datierung).

Grundsätzlich werden Edtionsangaben auf dieselbe Weise wie verschachtelte Belege
mit doppelten Klammern notiert. Der Computer erkennt an der Art der Zitierweise, dass es
sich um eine Editionsangabe handelt. Beispiel:

    * HERM. ALTAH. inst.; 1 ((MGScript. XVII; p. 371,29))(a. 1242/53)
    "quod ex ea promotione {(sc. abbatis Godehardi ad episcopatum)}
    suo sancto corpore careat {(sc. monasterium)} #integrali."

Das opus minus im Beispiel oben erscheint entsprechend der Regel, dass eine
angrenzende schließende und öffnende Klammer ")(" in Vorschau und Druck
durch ein Semikolon ersetzt wird als: "(MGScript. XVII p. 371, 29; a. 1242/53)"

Die Datierung ist bei opera minora übrigens nicht immer, sondern nur nach
gewissen Regeln verpflichtend! Da diese Werke in keiner 'offiziellen' Liste
auftauchen, hat der Benutzer keine Handreiche, in welches zeitliche Umfeld sie
einzuordnen sind. Das ist dann von Bedeutung, wenn ein opus-minus-Beleg an
erster Stelle einer Bedeutungsgruppe zitiert wird, bzw., wenn zwischen dem
vorangehenden Beleg und dem op.-min. Beleg mehr als 400 Jahre liegen
(normalerweise kann man sich daran orientieren, dass bei uns die Belege einer
Bedeutungsgruppe chronologisch sortiert zitiert werden).

Wie oben beschrieben wird eine Editionsangabe in einigen Fällen noch mit
Zusatzangaben wie "vs." oder "epist." versehen, die in der Ausgabe vor oder nach der
Datumsangabe erscheinen sollen. Hierfür gibt es eine spezielle Notation, bei der
Werk (und ggf. Stellenangabe), Datierung und Zusatzangabe jeweils durch ein
doppeltes Semikolon angegeben werden:  

    * GISLEB. ELNON. Amand.; 14 ((MGScript. XV; p. 851,58;; c. 1066;; vs.)) 

Übrigens wäre es auch möglich die Editions- und Datumsangabe in dem Beispiel oben so
zu schreiben: `HERM. ALTAH. inst.; 1 ((MGScript. XVII; p. 371,29;; a. 1242/53)) ...`. Aber es ist nicht
möglich, Zusatzangaben wie "vs." nur in die eigenständig, d.h. mit umschließenden
einzelnen runden Klammern notierte Datierung, zu schreiben, ohne die Datierung
selbst innerhalb der opus-minus-Angabe unterzubringen. Mit Zusatzangaben und
Datierung haben opera minora immer die Form:

**((**Edition, ggf. mit Stellenangabe **;;** Datierung **;;** Zusatzangabe**))**

Dabei ist die Datierung nicht zwingend. Ohne Datierung, aber mit Zusatzangabe
gilt die Form:

**((**Edition, ggf. mit Stellenangabe **;;** Zusatzangabe**))**

Beispiel:

    * GISLEB. ELNON. Amand.; 14 ((MGScript. XV; p. 851,58;; vs.))

Auch bei opera maiora müssen Zusätze nach einer Editionsangabe 
in derselben Weise angegeben werden wie bei opera minora. Ein (fiktives) Beispiel:

    * BERNOLD. CONST. chron.; p. 400,2 ((ed. Pertz;; vs.)) 
      "sedem apostolicam tirannice invasit eamque ... plures annos -vit {Guibertus} 
      ((* {sim.} BERNOLD. CONST. chron.; a. 1087 p. 463,16 ((ed. Robinson;; epist. papae))))."

##### 4.2.6.7 Datumsangaben

Datumsangaben, die unmittelbar nach einer Stellenangabe
vorkommen, können wie bei der Druckausgabe einfach in runden Klammern
notiert werden. Es sollten alle unterschiedlichen Formen von
Datumsangaben erkannt werden, also z.B. sowohl römische als auch
lateinische Zahlen oder unterschiedliche Buchstaben zur Einleitung einer
Datumsangabe wie `a.`, `s.` etc.

Beispiel:

    (a. 1242/53)
    (s. VII.^ex.)
    (s. IX.^ex./X.^in.)
    (c. 1227-41?)
    (paulo post 1133)

Sollte eine Datumsangabe einen Fehler hervorrufen, dann handelt es sich
vermutlich um eine noch nicht berücksichtigte Form und der Fehler sollte
gemeldet werden, um das Einlese-Programm um diese Datumsangabe zu
erweitern.

*Bisher werden Datumsangaben nur als solche erkannt, aber nicht weiter
ausgewertet, d.h. der Computer weiß, dass es sich um eine Datumsangabe handelt,
aber nicht welches Jahr oder Jahrhundert darin bezeichnet ist. Eine solche
Auswertung wäre durchaus sinnvoll, da man sich gut Anwendungsfälle vorstellen
kann, die das voraussetzen. Etwa, wenn man den lateinischen Sprachgebrauch
innerhalb eines zeitlich begrenzten Abschnitts des Mittelalters nachvollziehen
will und daher Angaben, die eindeutig nicht in diesen Zeitabschnitt fallen
aussortieren möchte.*

##### 4.2.6.8 Lemmaworte und Verdichtung

###### 4.2.6.8.1 Kennzeichnung von Lemmawörtern mit "#"

Lemmaworte werden innerhalb von Belegen mit einem vorangestellten
Gitterzeichen `#` gekennzeichnet. Beispiel 1:
 
    * PONTIF. Rom.-Germ.; 94,54 "#sabbat.|um ... septima dies est."

Der Trennstrich in der Mitte des Wortes "sabbatum" dient dazu, 
dem Computer zu zeigen, wo der Wortstamm endet. Der Punkt neben 
dem Trennstrich gibt an, dass bei der Ausgabe der Teil des Wortes 
auf der Seite des Punktes gestrichen werden soll. In diesem Beispiel 
bleibt von dem Lemmawort nach der Streichung des Wortstammes 
die Endung "-um".

Beispiel 2:

    * TRAD. Ratisb.; 140 (a. 889/91) "tradidit advocatus aream unam Rihholfo 
      presbytero de rebus sancte Marie #I|.nferior.|is."

Hier soll vom Lemmawort "Inferioris" der große Anfangsbuchstabe und die Endung 
angezeigt werden: "I-is". Dazu wird der zu verbergende mittlere Teil 
zwischen Punkte gesetzt: #I|.nferior.|is."



    * TRAD. Ratisb.; 140 (a. 889/91) "tradidit advocatus aream unam
      Rihholfo presbytero de rebus sancte Marie #Inferior.|is
      Monasterii."

Der Trennstrich in der Mitte des Wortes "Inferioris" dient dazu,
dem Computer zu zeigen, wo der Wortstamm endet. Der Punkt neben
dem Trennstrich gibt an, dass bei der Ausgabe der Teil des Wortes
auf der Seite des Punktes gestrichen werden soll. In diesem Beispiel
bleibt von dem Lemmawort nach Streichung des Wortstammes die Endung
"-is".


###### 4.2.6.8.2. Sonderfall: Getrennt-Schreibung und Tmesis:

**Getrennt-Schreibung**

Gelegentlich kommt es vor, dass das Lemmawort in zwei oder mehreren 
'Teilen' geschrieben wird ("separatim scribitur") weil es je nach 
Überlieferung als ein oder mehrere Wörter aufgefasst wurde.  Z. B. 
"salgemma", das sowohl als ein Wort, als auch in zwei Wörtern 
"sal gemma" geschrieben wird.

In Ausnahmefällen können sogar Satzzeichen vorkommen. In allen Fällen 
setzt man das gesamte Lemmawort nach dem Gitterzeichen # in geschweifte 
Klammern, um zu kennzeichnen, was alles zum Lemmawort gehört:

Beispiel:

    #{sal gemma}
    #{ha! ha! he!}

In der HTML-Vorschau sieht man dann auch, dass alle Wörter 
als Lemmawort eingefärbt sind.

ACHTUNG: Das Lemmazeichen "#" muss ausserhalb der Klammer stehen!  

**Tmesis**

Nun bleibt aber noch das Phänomen, dass (v.a. in poetischem Kontext) ein Wort 
in zwei oder mehr Wortteile auseinandergerissen wird, die für sich genommen 
keine vollständigen Wörter sind ("in tmesi"). Diese können durchaus weit 
von einander getrennt stehen, z. B. "ac heresi - papae Guiberti 
scilicet - archae ..." (Lemma: "haeresiarcha")

In solchen Fällen können aufgeteilte Lemmawörter mit # vor dem ersten Lemma-Teil 
und doppeltem ## vor dem zweiten Lemma-Teil markiert werden, also etwa: 

"ac #heresi - papae Guiberti scilicet - ##archae ..."
 
Wenn der Computer auf ein doppeltes Gitterzeichen ## trifft, fügt er im Datenbestand 
das so gekennzeichnete Lemmawort an das vorhergehende Lemmawort an. 
Technisch wird dazu dem Attribut "lemma" des BelegLemma-Kennzeichens das 
ganze Lemmawort zugewiesen, das durch einfaches Zusammensetzen der einzelnen Worte 
unter Streichung der Bindestriche entsteht:

Das Ganze funktioniert unter zwei Voraussetzungen:

- Lemmawörter im Beleg haben als Ganze niemals Bindestriche. 
  Die kommen nur als Abkürzung vor.

- Eine Folge getrennter Lemmawörter besteht aus maximal zwei Lemmawörtern 
  zwischen denen alles Mögliche stehen darf, nur kein anderes mit # gekennzeichnetes 
  Lemmawort, es sei denn innerhalb eines Einschubs.

Eine andere Notations-Option ist folgende: Am Anfang bzw. am Ende eines Lemma-Teils 
wird jeweils ein Bindestrich gesetzt. Der Computer kann daran getrennte 
Lemma-Teile eindeutig erkennen, so z.B. `ac #heresi- papae Guiberti scilicet #-archae ...`

Der Computer könnte ohne Weiteres auch drei oder mehr Bestandteile erkennen. Beispiel:

"Vivat #a- hicks! #-rom- hicks! #-a vino gallico!"

wird im Datenbestand so ausgezeichnent, dass bei jedem Bestandteil das volle 
Lemmwort in ein Attribut "lemma" geschrieben wird. Durch das Attribut "tmesis" ist 
jeder Bestandteil deutlich als ein Teilstück im Gegensatz zu einem vollen 
Lemmawort gekennzeichnet.

FAZIT:
Wenn nicht Bindestriche eine Tmesis anzeigen, dann müssten beim zweiten 
Glied des geteilten Wortes zwei Kreuze ## gesetzt werden, damit der Computer 
es als zusammengesetzt mit dem vorhergehenden Wort, sprich als eine Tmesis, erkennt. 


###### 4.2.6.8.3 Sonderfall: *Varia Lectio*

Eine besondere Variante von Lemmawörtern ist die *varia lectio*. Solche
Lemmawörter sollten zusammen mit dem Hinweis "var. l." oder noch
geheimnisvolleren Markierungen wie etwa "S1" in einfache runde Klammern
eingeschlossen werden. Jedes Lemmawort, das innerhalb runder Klammern
steht, wird vom Computer als eine *varia lectio* interpretiert, wenn
innerhalb derselben runden Klammern ein entsprechender Hinweis wie z.B.
der Zusatz `{var. l.}` steht.

Beispiel:

    * ALBERT. METT. div. temp.; 1,1 "cuius {(Wicmanni)} maiores magnam partem
    Germanie et maxime circa littora Oceani #imperi.|o (#imperi.|a {var. l.})
    tenebant."

Im eben aufgeführten Beispiel ist `#imperi.|o` ein Lemmawort und `#imperi.|a`
eine varia lectio.

Beispiel 2:

    *  PAUL. AEGIN. cur.; 241 p. 182,4 "emplastrum apponendum super
       ventrem et #iscia (#iscias, #isciam {var. l.})."

Übrigens ist es bei varia lectio Angaben wie auch sonst nicht notwendig sich
darüber den Kopf zu zerbrechen, ob die Klammern rund oder eckig sind. Der
Computer bestimmt bei geschachtelten Klammern automatisch, ob sie rund
oder eckig sein müssen. So liefert:

    ((* {sim.} LEG. Lang.; p. 136,25 "cum legitimis #sag|.ramentali.|bus {@ sacramentalis_1c} 
      (#sacre|.mentalibus {@ sacramentalis_2}, #sacramentali.|bus, #sacramental.|es {var. l.}) 
      suis"))

dieselbe Ausgabe mit eckigen Klammern wie diese Eingabe:

    ((* {sim.} LEG. Lang.; p. 136,25 "cum legitimis #sag|.ramentali.|bus {@ sacramentalis_1c} 
      [#sacre|.mentalibus {@ sacramentalis_2}, #sacramentali.|bus, #sacramental.|es {var. l.}] 
      suis"))

nämlich: (*sim* LEG. Lang  p. 136,25 cum legitimis sag-bus [sacre-, -bus, -es *var. l.*] suis.)

Für die Verwendung der eckigen Klammern schon bei der Eingabe spricht jedoch die größere
Lesbarkeit des Quelltextes. 

Um eine bestimmte Art von Klammern zu erzwingen, genügt es vor die Klammer ein Ausweichzeichen
zu setzen, etwa um einen zu tilgenden Buchstaben zu kennzeichnen:

    * COMPOS. Luc.; U 25 "ter\[r\]es calcu cacaumenon utiliter ..."

Die so gekennzeichnenten eckigen Klammern werden nicht automatisch in Runde umgesetzt,
so dass das Wort, dass diese Klammern enthält, in Vorschau und Druck als "ter[r]es" erscheint.

###### 4.2.6.8.4 Verdichtung

In der Regel sollten die in einem Beleg vorkommenden Lemmaworte als
solche gekennzeichnet werden. Dies geschieht dadurch, dass das
Lemmawort im Belegtext durch ein vorangestelltes Gitterzeichen "#"
gekennzeichnet wird, z.B.:

     "de quiete #sabbati"

Im Quelltext sollte immer das vollständige Lemmawort stehen, auch wenn
in der Druckausgabe später nur eine verdichtete Form
erscheinen soll, also etwa "-i" für "sabbati".

Die Verdichtung wird im
Quelltext durch einen senkrechten Strich mit einem Punkt kenntlich
gemacht. Der Punkt zeigt an, welcher Teil in der Druckausgabe (und
in der Vorschau) gestrichen werden soll:

    "de quiete #sabbat.|i"

Der Teil, wo der Punkt steht, wird dann durch einen
Verdichtungsstrich "-" ersetzt, so dass der Text in Druck und
Vorschau als "de quite -i" wiedergegeben wird.

Soll nicht der Anfang oder das Ende eines Lemmawortes gestrichen
werden, sondern ein Segment innerhalb eines Lemmawortes, dann müssen
zwei Trenn-Striche mit Punkten gesetzt werden, wobei die Punkte wiederum
anzeigen, auf welcher Seite des Striches die Streichung vorzunehmen ist:

    "ne ... sompnolenti {(sic)} oblivionis #inh|.erti.|a depravata subtrahantur."

Im Druck wird nun `#inh|.erti.|a` durch die verdichtete Form "inh-a"
ersetzt werden.

Sollte keine Verdichtung erwünscht sein, genügt es auf die
entsprechenden Markierungen durch senkrechte Striche mit angrenzenden
Punkten zu verzichten. Wo keine Verdichtung angegeben wird, erscheint in
der Vorschau, wie im Druck immer das vollständige Wort.

Wird ein Lemmawort mit einem non negiert, wird das so notiert: 
`non-#Lemma`, z.B. `in silvis non-#sanctis`. Das Lemmawort wird dabei 
nicht verdichtet!

###### 4.2.6.8.5 Enklitische Partikel

Damit enklitische Partikel, wie z.B. ein nachgestelltes "que" oder "ve" 
nicht als Teil  des Lemmawortes missverstanden werden - was zu einem 
subtilen Fehler im Datenbestand führen würde - sollten sie durch einen 
Unterstrich abgetrennt werden:

    #sanct.|ae_que

In der Vorschau wird daraus "-aeque". Allerdings weiß der Computer, dass
"que" nicht Teil des Lemmawortes, sondern eine enklitische Partikel ist.
In der HTML-Vorschau wird das dadurch kenntlich, dass dieser Teil nicht
mehr rot hervorgehoben ist wie das eigentliche Lemmawort.

###### 4.2.6.8.6 Athetesen und Ergänzungen (cf. Ticket #37 und #71)

Athetesen, d.h. die Markierung von zu tilgenden Bestandteilen eines Wortes
mit eckigen Klammern sind auch in Lemmawörtern möglich:

    ... uoluntatem, #[p]s|.alt.|im ut sub mensam illius ...

Das Lemmawort erscheint dann (mit Verdichtung) als "[p]s-im".
Verdichtungszeichen innerhalb der eckigen Klammern sollten möglichst
vermieden werden. In analoger Weise können hinzuzufügende Bestandteile eines
Wortes mit spitzen Klammern markiert werden: 

    ... in hoc #se<c>olo vacare ...

###### 4.2.6.8.7 Semantisch explizite Abkürzungen

Verdichten lassen sich nur Lemmawörter. Wenn man andere Wörter abkürzen will,
kann man das einfach mit dem Abkürzungspunkt tun und zum Beispiel statt "hortus"
die Abkürzung "h." schreiben.

Es gibt aber Fälle, in denen man sich vorstellen kann, dass das abgekürzte Wort
Gegenstand einer Suche sein sollte. Wenn man nach "hortus" sucht würde der
Computer, die Stellen, an denen "hortus" durch "h." abgekürzt wurde, nicht
finden können, da die Abkürzung "h." ja auch für "homo" stehen könnte.

Die Suchbarkeit abgekürzter Ausdrücke lässt sich nur dadurch herstellen, dass
man ein Wort semantisch explizit abkürzt. Das funktioniert mit derselben
Notation wie die Verdichtungen, mit dem Unterschied, dass nur das Ende eines
Wortes abgekürzt werden kann, und dass statt eines Spiegelstrichs ein
Abkürzungspunkt erscheint.

Beispiel:

    * PAUL. DIAC. Lang.; 5,27 "in ipso sacratissimo #sabbat.|o {!paschali}
      ((* AMALAR. off.; 1,35,3 "dies quinquaginta, qui secuntur a
      #sabbat.|o p|aschali usque ad octavas pentecostes." * NOTKER. BALB.
      gest.; 2,21 p. 28,24))."

Hier wurde das Wort "paschali" im Einschub semantisch explizit abgekürzt, d.h.
im Datenbestand steht "paschali", im Druck und in der Vorschau steht "p.". Bei
Abkürzungen darf - wie im Beispiel - der Punkt hinter dem senkrechten Strich
weggelassen werden, da ja klar ist, dass immer das Ende des Wortes gestrichen
wird. Aus Gründen der Einheitlichkeit der Notation, wäre es aber auch möglich
"p|.aschali" zu schreiben.

##### 4.2.6.9 Textauszeichnungen

Neben der Kennzeichnung der Lemmata gibt es innerhalb der Belegtexte noch
weitere Möglichkeiten der Textauszeichnung, nämlich Junkturen, gesperrten Text
und die (schon bekannten und auch außerhalb des Belegtextes möglichen) Zusätze.
(Die verschachtelten Belege sind, da es sich nicht eigentlich um eine
Textauszeichnung handelt, hier nicht noch einmal aufgeführt.)

###### 4.2.6.9.1 Junkturen

Junkturen sind Bereiche, die in der Druckausgabe durch kleine liegende rechte
Winkel ausgezeichnet werden: `˻Junktur˼`. Da diese Zeichen auf der Tastatur
nicht zu finden sind, können im Quelltext stattdessen doppelte spitze Klammern
verwendet werden: `<<Junktur>>`. Dieser Ausdruck wird vom Computer dann nach
`˻Junktur˼` übersetzt.

**Hinweis:** Innerhalb von Junkturen werden aneinandergrenzende schließende
und wieder öffnende Klammer nicht zu einem Semikolon aufgelöst. Hier muss man
die Klammern ggf. von Hand auflösen, was zumindest dann unproblematisch ist,
wenn es nicht zu einem Verlust von Semantik führt (indem z.B. Einschübe nicht
mehr als solche in den XML-Daten kenntlich wären). In dem folgenden Beispiel 
ist das unproblematisch, d.h. anstelle von

    <<#scrut.|entur (#scrut.|ent {A1^a}) {(sc. insidiatores)} locum>>

kann man schreiben:

    <<#scrut.|entur (#scrut.|ent {A1^a; sc. insidiatores}) locum>> 


###### 4.2.6.9.2 Sperrungen

Gesperrter Text wird - äußerlich ähnlich einem Zusatz - mit geschweiften
Klammern eingeschlossen, wobei unmittelbar nach der öffnenden geschweiften
Klammer ein Ausrufezeichen stehen muss: `{!gesperrt}`. Im Druck erscheint dann
in etwa: "g e s p e r r t".

###### 4.2.6.9.3 Zusätze

Zusätze wurden weiter oben schon beschrieben. Sie sind durch die umschließenden
geschweiften Klammern gekennzeichnet und werden später kursiv wiedergegeben.
Innerhalb der geschweiften Klammern darf beliebiger Text stehen. Das erlaubt es
insbesondere aber auch, die immer wiederkehrenden feststehenden Hinweise, wie
"eqs.", "sim." etc., als Zusätze zu kodieren: `{eqs.}` `{sim.}`, etc. Zusätze
können nicht verschachtelt werden, aber sie dürfen Verweise enthalten, d.h.
`{sim. {=> Ankername}}` ist möglich. Ohne den Pfeil "=>" würde es aber eine
Fehlermeldung geben.

###### 4.2.6.9.4 Physische Kursivierungen

Manchmal benötigt man an einer bestimmten Stelle kursiven Text, ohne
dass es sich um einen Zusatz handelt, oder man benötigt innerhalb einer
an sich schon kursiven Umgebung, etwa innerhalb einer Bedeutungsangabe
geraden Text. In diesem Fall kann man `{/kursiv}` schreiben, also eine
geschweifte Klammer gefolgt von einem Schrägstrich, um eine Hervorhebung,
(kursiv innerhalb geraden Texts, und gerade innerhalb kursiven Textes) zu
bewirken.

*Physische Kursivierungen sollten nur äußerst sparsam verwendet werden.
Normalerweise wird die Kursivierung von dem MLW-DSL-System anhand
semantischer Kriterien automatisch richtig vorgenommen. Im Artikelkopf
sind physische Kursivierungen aus diesem Grund nicht erlaubt und führen
zu einer Fehlermeldung. Sollten die automatisch vorgenommenen
Kursivierungen in der Vorschau falsch dargestellt werden, so kann das
daran liegen, dass eine bestimmte Position im Artikelkopf falsch
strukturiert wurde.*

###### 4.2.6.9.5 Hoch- und tiefgestellter Text

Hochgestellter Text wird durch ein Dach ^ eingeleitet. Alle folgenden
Buchstaben und Zahlen und Abkürzungspunkte werden hochgestellt, bis ein
Zeichen folgt, das kein Buchstabe oder keine Zahl mehr ist. Bei

    LUDUS de Antichr.; p. 12^med.

würde die Zeichenkette `med.` hochgestellt werden. Hingegen bei:

    DIPL. Heinr. IV.; 36^a p. 45^a,5

nur beide Male das `a`, aber nicht mehr die Zeichenfolge `,5`.

Falls die Hochstellung zu viel oder zu wenig Zeichen umfasst, kann man durch
geschweifte Klammern `{ }`, die unmittelbar auf das Dach `^` folgen müssen,
eindeutig angeben, was hochgestellt werden soll. Bei

    Hoops, ^{2}RGA XXII.

wird nur die `2` hochgestellt, aber nicht mehr die nachfolgenden Buchstaben. In
den meisten Fällen sollte aber die Kurzschreibweise ohne geschweifte Klammern
bereits das gewünschte Ergebnis liefern.

In eher seltenen Ausnahmefällen kann es auch vorkommen, dass Zeichen tiefgestellt 
werden sollen. Die *Tiefstellung* von Text wird auf dieselbe Weise bewirkt, wie Hochstellung,
nur mit dem Unterschied, dass der Unterstrich `_` anstelle des Dach-Zeichens `^`
verwendet wird, etwa: `Wort_Anhängsel` oder `Wort_{tiefgestelltes Anhängsel}`.

**Hinweis**: Bei Lemmawörtern (und nur dort) hat der Unterstrich eine andere
Bedeutung und dient dazu enklitische Partikel zu kennzeichnen, z.B.
`#sanct.|ae_que` (siehe oben). Hoch- und Tiefstellungen sind innerhalb von mit
"#" gekennzeichneten Lemmawörtern dagegen nicht möglich.

###### 4.2.6.9.6 Römische Zahlen

Römische Zahlen werden in der Bedeutungsposition automatisch erkannt und außer 
bei Werktiteln in kleine (!) Kapitälchen gesetzt.

    "acta sunt hec ... anno Domini MCCLXXI ..."  /* MCCCLX erscheint in Kapitälchen */

Wichtig: Um die kleinen Kapitälchen zu bekommen, sollte die römische
Zahl trotzdem mit Großbuchstaben getippt werden. Kleine römische Zahlen, wie
z.B. "iv" erscheinen nicht als Kapitälchen, sondern einfach als Kleinbuchstaben.

Es gibt seltene Fälle, in denen römische Zahlen nicht als Kapitälchen gesetzt
werden sollen. In diesem Fall kann man durch das vorangestellte Dollarsymbol als
"Disambiguierungszeichen" erzwingen, dass sie in Großbuchstaben und nicht in
Kapitälchen gesetzt werden:

    * CHART. Rhen. med.; III 932 p. 700,28 
      "ipse {(Heinricus $II.)} dixit: {(eqs)}"

oder, wie hier innerhalb eines Zusatzes:

    {add. in marg.; v. et comm. ed. vol. $III. p. 53}

Außerhalb von Werktiteln gelten also folgende Regeln:

iv      = Kleinbuchstaben, also wirklich nur "iv"

IV      = kleine Kapitälchen

$IV     = Großbuchstaben

##### 4.2.6.10 Abkürzung von Stellen-, Autor- und Werkangaben und "op. cit."

###### 4.2.6.10.1 Abgekürzte Stellenangaben

(Siehe auch Ticket Nr. 80 im gitlab: <https://gitlab.lrz.de/badw-it/MLW-DSL/-/issues/80>)

Die Notation für das MLW beinhaltet keine allzu differenzierte bibliographische Notation. 
Vielmehr wird bei Belegen (und ganz ähnlich bei Sekundärliteratur) nur zwischen 
Autor, Werk und Stellenangabe unterschieden und es gibt keine weiteren Unterscheidungen wie
z.B. zwischen Bandnummer, Kapitel und Seitenzahl, die semantisch ausgezeichnet werden. Sind Bandnummern
Teil der Stellenangabe, könnte das zu einem fehlerhaften Datenbestand führen,
wenn man - wie in der Druckausgabe - bei mehreren aufeinanderfolgenden Stellenangaben die
Bandnummer weglässt. Um das zu vermeiden, kann man mittels eines senkrechten Strichs `|`
angeben, welcher Teil bei den *folgenden* Stellenangaben weggelassen wird:

    * CHART. Rhen. med.; III| 1284 "maximus."; 1365 "minimus."

liefert: "CART. Rhen. med. III 1289 maximus. 1365 minimus." Dabei versteht der Computer,
dass die verkürzte Stellenangabe "1365" im Datenbestand zu "III 1365" ergänzt werden muss.
(Im XML wird dazu an die Stellenangabe das Attribut `stelle` angehängt, dass immer die
vollständige Stellenangabe wiedergibt.)

Das Präfix der Stellenangabe (in diesem Beispiel `III`) wird innerhalb 
desselben Bedeutungsblocks solange jeder folgenden Stellenangabe hinzugefügt,
bis entweder ein neuer Autor oder ein neues Werk angegeben werden, oder bis 
eine weitere Stellenangabe mit dem Trennstrich `|` unterteilt wird, und
dadurch ein neues Präfix gesetzt wird. 

Dazu noch ein artifizielles Beispiel:

    * CHART. Rhen. med.; III| 1284 "maximus."; 1365 "minimus."
    * CHART. Rhen. med.; III| 1455 "proximus."; II| 1537 "ultimus."; 1666 "optimus."

Stellenangaben im Datenbestand: III 1284 ... III 1365 ... III 1455 ... II 1537 ... II 1666.

Angezeigte Stellenangaben: III 1284 ... 1365 ... 1455 ... II 1537 ... 1666

*Überprüfung*: In der HTML-Vorschau genügt es mit dem Mauszeiger kurz auf
einer Stellenangabe stehen zu bleiben, um die vollständige Literatur- und
Stellenangabe anzuzeigen.

Sollten sich bei der Überprüfung Fehler zeigen, z.B. eine verdoppelte
Kapitelangabe, dann hilft es in der Regel, wenn man bei der fehlerhaften
Stelle die Kapitelangabe explizit noch einmal durch einen senkrechten
Strich `|` abtrennt, auch wenn keine weiteren Stellenangaben mehr folgen.

Beispiel:

    * RIMB. Anscar.; 19| p. 39,32 "locus..."; p. 40,8 "quo..."; 20| p. 46,17.

Ohne den senkrechten Strich in der letzten Stellenangabe würde der Computer
die Stellenangabe als "19 20 p. 46, 17" missverstehen.

Wer ganz sicher gehen will, schreibt jedes Mal die Kapitelangabe explizit mit
senkrechtem Trennstrich dazu, und überlässt die Löschung identischer
Kapitelangaben (sofern sie durch den senkrechten Strich abgetrennt wurden)
bei allen Folgestellen dem Computer:

    * CHART. Rhen. med.; III| 1284 "maximus."; III| 1365 "minimus."

Ergebnis: "CHART. Rhen. med. III 1284 maximus. 1365 minumus."

    * RIMB. Anscar.; 19| p. 39,32 "locus..."; 19| p. 40,8 "quo..."; 20| p. 46,17.

Ergebnis: "RIMB. Anscar. 19 p. 39,32 locus... p. 40,8 quo... 20 p. 46,17."

**Sonderfall** Kapitelangabe ändert sich ohne Stellenangabe: Bei sehr kurzen
Kapiteln wird die Seitenzahl nicht eigens angegeben. Wenn sich nun die
Kapitelangabe ändert, aber keine Seitenzahl angegeben werden soll, genügt es,
die Seitenzahl einfach leer zu lassen. Wichtig ist aber, dass nach der
geänderten Kapitelangabe ein senkrechter Strich gesetzt wird, damit die
Kapitelangabe nicht als Stellenangabe fehlinterpretiert wird:

    * IONAS BOB. Columb.; 1,15| p. 177,17 "ad ipsum."; 2,3| "vir Dei."

Durch den senkrechten Strich nach "2,3" versteht der Computer, dass es sich bei
"2,3" um die erste Hälfte einer Stellenangabe, also um das Kapitel und nicht wie
z.B. bei der Angabe "p. 178,10" zuvor um eine Seitenangabe zu einem vorher schon
angegebenen Kapitel handelt.

###### 4.2.6.10.2 Abkürzung bei Werk und Bandangaben

Will man die Werktitel von Nachschlagewerken abkürzen, indem man bei
aufeinanderfolgenden Zitaten aus unterschiedlichen Bänden eines Werkes nur noch
die Bände angibt, kann man ebenfalls den senkrechten Strich `|` verwenden. (Die
Verwendung des senkrechten Striches unterscheidet sich in diesem Fall leider in
subtiler Weise von der bei der Abkürzung von Stellenangaben.) Beispiel:

    * HRG| II.; p. 566sqq. * HRG| V.; p. 1039sqq.

In der Ausgabe wird daraus: "HRG II. p. 566sqq.; V. p. 1039sqq." Im Unterschied
zur Abkürzung bei Stellenangaben muss bei jeder Stelle der volle Titel (also
Werkname und Bandnummer) angegeben und ggf. durch den Trennstrich das Ende des
Werknamens gekennzeichnet werden.

###### 4.2.6.10.3 Ersetzung von Werktiteln durch "op. cit."

Werktitel von Sekundärliteraturangaben oder Kürzel für Nachschlagewerke 
können durch "op. cit." ersetzt werden. 

Beispiel mit Autor und Werk:
   
    ((* {cf.} D. Goltz, Mineralnamen ({@ sal_23}); p. 145))
    ...
    ((* {cf.} D. Goltz, op. cit. ({=> sal_23}); p. 271 adn. 278))

Beispiel mit einem Kürzel für ein Nachschlagewerk:

    ((* {cf.} Dt. Rechtswb. {@ sal_34} XI.; p. 1451sqq.))
    ...
    ((* {cf.} op. cit. {(=> sal_34)}; p. 1451sqq.))

In diesen Beispielen wird durch die Zeilenverweise ({=> sal_23}) und 
({=> sal_34}) auf die an anderer Stelle vollständig zitierten Werke verwiesen.

Dieser Verweis kann weggelassen werden, wenn das zitierte Werk 
in unmittelbarer Nähe davor steht oder die einzige zitierte 
Sekundärliteratur im betreffenden Artikel ist:

    ((* {cf.} op. cit.; p. 154)) /* Verweis auf eine andere Seitenzahl des zitierten Werks */
    ((* {cf.} op. cit.)) /* Verweis auf die schon zitierte Stelle des zitierten Werks */

Bei Bedeutungsangaben sollte "op. cit." am Besten nur innerhalb eines 
Einschubs verwendet werden, da es sich um eine Sekundärliteratur-Angabe 
handelt; sonst könnte es semantisch als Teil der Bedeutungsangabe 
missverstanden werden.


Leider gibt es da noch ein ungelöstes PROBLEM: 
Notation mit * funktioniert nur, wenn danach ein Autorname folgt! Muss noch gelöst werden!


###### 4.2.6.10.4 Auflösung von Mehrdeutigkeiten mit Doppelklammern "[[...]]"

Das Weglassen von redundanten Autorangaben kann in einigen Fällen für den Computer zu 
Mehrdeutigkeiten führen, d.h. der Computer kann nicht mehr mittels einfacher Regeln
erschließen, welche Angaben ergänzt werden müssen, um die Daten an der entsprechenden Stelle
zu vervollständigen. Beispiel:

   * PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ...
   cum #scarifi.|catione ({B}, #scari.|fatione {A})
   ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως")).";
   71 p. 48,24 "ventosandum ... cum #scarifi.|catione 
   ((* 3,20,1 p. 168,23 "ἀμυχῶν"))."

Hier wird im zweiten Einschub die Autorangabe weggelassen. Für menschliche Leserinnen und Leser
is durch das griechische Zitat klar, dass es sich nur um denselben "Paul. Aeg." handeln kann,
der schon in dem vorhergehenden Einschub auf Griechisch zitiert wurde. Für den Computer käme
der unmittelbar zuvor zitierte "Paul. Aegin." aber ebenso in Betracht 
(wie im zweiten Beispiel weiter unten zu sehen). 

Die einzige Möglichkeit, diese Mehrdeutigkeit,
auf die der Computer übrigens mit einer Warnung hinweist, aufzulösen, besteht darin,
dass Autor- und Werk- dem Computer explizit mitgeteilt werden. Damit die Angaben dort,
wo sie für den Menschen redundant wären, aber nicht im Druck erscheinen, werde sie in 
doppelte eckige Klammern gesetzt, wie in den folgenden drei Beispielen:

   * PAUL. AEGIN. cur.; 46 p. 23,12 "oportet ... ventosas adducere in occipitio  ...
   cum #scarifi.|catione ({B}, #scari.|fatione {A})
   ((* $PAUL. AEG.; 3,9,3 p. 148,18 "ἐγχαράξεως")).";
   71 p. 48,24 "ventosandum ... cum #scarifi.|catione ((* [[$PAUL. AEG.]]; 3,20,1 p. 168,23 "ἀμυχῶν"))."
   
   
   * PAUL. AEGIN. cur.; 7 p. 4,4 "stipteree #cisse {@ scindere_1B}
   (($PAUL. AEG.; 3,1,7 "σχιστῆς")) ... ℥ IIII.";
   144 p. 82,6 "illine perfricans ... #sciss.|am {@ scindere_15} cum terenbintina
   (( {sim.} [[PAUL. AEGIN. cur.]]; 147 p. 84,1.; 149 p. 84,13))."
   
   
   * ALBERT. M. metaph.; 7,1,2| p. 318,62 "'dubitavit aliquis' ..., 'utrum vadere et #san.|are
   ((* $ARIST.; p. 1028^a,21 "τὸ ὑγιαίνειn")) et sedere'..., 'unumquodque' illorum 'sit ens aut non-ens'.";
   p. 318,74 "'si aliquid' praeinductorum 'est' de numero 'entium' sicut 'vadens' aut 'sedens et #san.|ans{@ sano_10}'
   ((* [[$ARIST.]]; p. 1028^a,25 "τὸ ὑγιαῖνον")) {eqs.}"


##### 4.2.6.11 Sonderzeichen

**Seltene oder spezielle Zeichen im Belegtext**:

Manchmal kommen im Belegtext seltene Zeichen vor, die nicht unbedingt ein Teil
des Alphabets sind. Dazu gehören Zeichen mit Diakritika, metrische Zeichen,
Symbole für Währungen. Solange diese Zeichen im Unicode-Zeichensatz enthalten
sind, kann man sie ohne Probleme in den Text hineinkopieren. Die meisten dieser
Zeichen werden bei der Übersetzung klaglos akzeptiert.

Falls nicht, sollte dafür ein Ticket angelegt werden, damit es in die Menge der
erlaubten Zeichen aufgenommen wird. Ebenso, falls bei der Druckausgabe das
Zeichen nicht erscheint, denn es kann sein, dass es (noch) nicht im
Druckschriftsatz vorhanden ist.

Aufstellungen aller Unicode-Zeichen finden sich im Netz, u.a. auf:
<https://unicode-table.com/en/blocks/>

Häufiger im MLW vorkommende Zeichen sind in diesen Dokumenten verzeichnet bzw. beschrieben:

* <https://syncandshare.lrz.de/getlink/fiNAqimH9RMZ2qxC6L4aJJJb/Sonderzeichen.mlw> 
  (Tanja's Sonderzeichenliste im Sync&Share-Ordner "MLW (Eckhart Arnold)/MLW-Testartikel/Sonderzeichen.mlw")
* <https://gitlab.lrz.de/badw-it/MLW-DSL/-/blob/master/Zusatzdaten/MLW%20Sonderzeichenliste.docx>

**Zeichen, die von der Notation schon besetzt sind**:

Zeichen, die als syntaktische Markierung dienen, wie z.B. "*" dürfen im
Ausnahmefall auch im Belegtext, und der Stern sogar in der Stellenangabe
vorkommen. Damit das funktioniert, muss man die entsprechenden Zeichen durch das
vorangehendes "Ausweichzeichen" `\` schützen.

So ist `* CHART. Tirol. I; 27\* (a. 1239)` eine legitime Stellenangabe in der
Vorschau und im Druck wird das Ausweichzeichen weggelassen und es erscheint nur
ein einfacher Stern. Dieser Stern hat keine semantische Bedeutung, sondern 
gibt nur rein optisch ein in der Edition verwendetes Zeichen wieder.

Das Ausweichzeichen kann auch vor eckige oder runde Klammern gesetzt werden,
um die automatische Ersetzung eckiger oder runder Klammern entsprechend der
Schachtelungstiefe zu umgehen. So werden z.B. auf der untersten Ebene normaler
weise alle runden durch eckige Klammern ersetzt, d.h. aus ``ar[r]ida`` wird
"ar(r)ida". Will man, wie in diesem Fall, wo die eckigen Klammern ein zu 
tilgendes Zeichen markieren, eine bestimmte Klammerart erzwingen, so muss
``ar\[r\]ida`` schreiben, was in der Ausgabe als "ar[r]ida" erscheint.

**Einfache Anführungszeichen**

Einfache Anführungszeichen (Zitat im Zitat) können der Bequemlichkeit halber als
Apostroph notiert werden. (Auf der Tastatur ist das dasselbe Zeichen oben auf
derselben Taste, wie das `#`-Zeichen.) Der Computer erkennt zusammengehörende
einzelne Anführungszeichen, die als Apostroph notiert worden sind und wandelt
sie in typographisch korrekte öffnende und schließende einfache
Anführungszeichen um. 

Alternativ kann man die einzelnen Anführungszeichen explizit wie ein accent aigu oder accent
grave über einem Leerschlag notieren, um sie als öffnendes oder schließendes
einzelnes Anführungszeichen zu kennzeichnen.

#### 4.2.7 Querverweise

Verweise und Querverweise können fast überall im Dokument angebracht
werden. Es gibt unterschiedliche Arten von Verweisen, die aber, soweit
sie überhaupt besonders gekennzeichnet werden, alle mehr oder weniger
derselben Notation folgen:

1. **Klassische Verweise**: Mit Ausdrücken wie cf. oder v. kann auf 
   andere Belegstellen verwiesen werden, z.B. 

   `* {cf.} FRID. II. IMP. art. ven.; 3 p. 37,23.`

2. **Digitale Verweise *innerhalb* desselben Artikels** (MLW-intern 
   "Zeilenverweis" genannt): Der häufigste
   Fall ist der, dass im Artikelkopf auf eine bestimmte Stelle
   innerhalb der Bedeutungsposition verwiesen wird. Dazu muss man an
   der Stelle, wohin verwiesen wird einen Anker setzen. Der Anker
   sieht ähnlich wie ein Zusatz aus, nur dass unmittelbar auf die
   öffnende geschweifte Klammer ein `@`-Zeichen folgt, also z.B.:

   `{@ fascitergulum}`

   Ein im Text eingefügter Anker markiert immer das unmittelbar
   vorhergehende Wort als Ziel des Verweises.

   Der Name des Ankers erscheint nicht im Ausgabetext, sondern dient nur
   dazu einen Zielort zu markieren! Irgendwo weiter oben (oder unten)
   steht dann der Verweis selbst, der ebenfalls in geschweiften Klammern
   steht, aber mit einem einleitenden Pfeilsymbol `=>`, z.B.:

   `{=> fascitergulum}`

   Auch hier erscheint der Verweisname nie im Text, sondern er wird,
   je nach Ausgabemedium, entweder durch die Zeilen und Spaltennummer
   ersetzt (Druckausgabe), oder durch einen Hyperlink (Digitale
   Ausgabe).

   In den meisten Fällen bezieht sich ein Anker auf ein Lemmawort innerhalb
   eines zitierten Textes. Dabei sollten Anker immer nachgestellt notiert
   werden. Im folgenden Beispiel bezieht sich der Anker `{@ iocus_6}` auf das
   vorhergehende Lemmawort `#ioc.|is`. Verweise auf diese Sprungmarke `{=>
   iocus_6}` zielen also auf `#ioc.|is`:

   `"...cupias quam famina #ioc.|is {@ iocus_6} ({corr. ex} vocis {m2}; #ioc.|i {ci. ed.})."`

   Mehrere aufeinanderfolgende Verweise auf unterschiedliche Ziele
   lassen sich einfach dadurch notieren, dass man die Ziele durch
   Semikolon getrennt nacheinander angibt:
 
   `{=> imperium_1; imperium_2}`

   Alternativ könnte man genauso gut natürlich schreiben:
   
   `{=> imperium_1}{=> imperium_2}`

   Aber das ist bei vielen Verweisen hintereinander weniger
   übersichtlich.

3. **Aliasnamen**: Es besteht die Möglichkeit vor einem Verweisziel
   und durch einen senkrechten Strich `|` davon getrennt ein "Alias"
   für den Verweis anzugeben. Das Alias erscheint nur im Text der
   elektronischen Ausgabe und wird mit dem
   Link auf das Ziel unterlegt, z.B.:
   
   `{=> siehe unten|fascitergulum}`

   Ist kein Alias angegeben, so wird in der Online-Ausgabe ein
   Pfeil nach rechts "⇒" als alias angezeigt.
   In der Druckausgabe wird der Alias bei Verweisen innerhalb
   des MLW durch die Stellenangabe ersetzt.

   *Bei Verweisen innerhalb des MLW sollten in der Regel keine
   Aliasnamen angegeben werden. Ausnahmen sind die unten beschriebenen
   Rückwärtsverweise auf die Druckausgabe und Vorwärtsverweise auf
   künftige Artikel außerhalb des gegenwärtig bearbeiteten Faszikels.
   (Siehe unten.)*

4. **Annotierte Verweise**: Das Ziel eines Verweises (nicht der Aliasname)
    darf durch ein Anhängsel in Form eines kurzen Textzusatzes annotiert werden.
    Der typische Fall ist die Annotation "sqq." um anzuzeigen, dass der
    Verweis auf einen sich über mehrere Textzeilen erstreckenden Bereich
    zielt, was der Computer selbst nicht feststellen kann: 
   
    `{=> imperium_1{sqq.}}`

    Im Drucktext erscheint dann z.B: "l.32sqq." 

5. **Digitale Verweise auf andere Artikel des MLW (MLW-intern:
   Zeilenverweise auf andere Artikel)**: Sofern der andere
   Artikel digital vorliegt, folgen solche Verweise genau derselben
   Logik, wieder wird am Ziel ein Anker gesetzt. Im Verweis jedoch
   wird nicht nur der Name des Ankers angegeben, sondern das Lemma
   gefolgt von einem Schrägstrich und dann der Name des Ankers, z.B.:

   `{=> facitergula/fascitergulum}`

   (Gültige Ankernamen dürfen keinen Schrägstrich "/" enthalten, daher
   weiß der Computer, dass, wenn ein Schrägstrich vorkommt, nur der
   letzte Teil der Ankername ist, und dass alles, was davor steht, eine
   Pfadangabe - in diesem Fall einfach das Lemma.)

   In der Druckausgabe werden solche Verweise, wenn sie auf existierende
   Artikel verweisen, durch die Seiten- bzw. Spalten- und Zeilenangabe
   ersetzt. In der Online-Ausgabe könnte stattdessen als Linktext das
   Ziellemma mit einem vorangestellten Pfeil nach rechts stehen, z.B. "⇒
   facitergula", wobei der Klick dann aber nicht einfach auf das Lemma,
   sondern auf die durch das Verweisziel angegebene Stelle innerhalb des
   Lemmas führen wird.

6. **Vorwärtsverweise**: Falls der Artikel, auf den verwiesen wird,
   noch nicht digital vorliegt, kann man entweder auf die Kennzeichnung
   des Verweises verzichten und lediglich in menschenlesbarer Form die
   Stellenangabe wie bisher notieren, oder einen Platzhalterverweis
   einfügen, bei dem die Zieladresse offen gelassen wird:

   `{=> zebra (noch nicht Vorhanden)| - }`

   Im Text erscheint dann lediglich "zebra (noch nicht vorhanden)" als
   "Alias" für den Verweis. Der Strich deutet an, dass es kein
   Verweisziel gibt. Wenn später der Artikel zu zebra erscheint, kann
   man das Ziel in dem früheren Artikel nachtragen und das Alias ändern
   oder ganz streichen.

7. **Rückwärtsverweise auf die Druckausgabe**: Bei Verweisen auf schon
   gedruckte Artikel, bietet es sich ebenfalls an die Spalten und
   Zeilennummer als Alias ohne Verweisziel anzugeben, etwa:
   
   `{=> vol. I. p. 1354,17|-}`

   Es ist denkbar, dass der Strich später, wenn die Druckausgabe später einmal 
   komplett retrodigitalisiert ist, durch einen Anker oder eine Internet-URL der
   entsprechenden Stelle in der retrodigitalisierten Ausgabe ersetzt wird,
   so dass nach einer Neu-Überetzung des Artikels ein digitaler Verweis wird.
   
   Auch wenn man später genausogut einen Zusatz wie `{vol. I. p. 1354,17}` 
   durch einen Verweis ersetzen könnte, ist es sinnvoll gleich einen Verweis
   mit `{=> ...|-}` zu notieren, da dann die nachzuarbeitenden Stellen
   leicht gefunden werden können.

9. **Digitale Verweise auf Quellen außerhalb des MLW**: Sofern das
   Quellenmaterial für das MLW digital vorliegt, z.B. in den DMGH,
   kann man die Stellenangabe mit einem elektronischen Link
   unterlegen. Aus der alten Stellenangabe:

   `* PETR. Dam. epist.; 10 p. 131,10`

   wird dann:

   `* PETR. Dam. epist.; {=> 10 p. 131,10|http://mgh.de/dmgh/resolving/MGH_Briefe_d._dt._Kaiserzeit_4,1_S._131}`

   Man bemerke, dass das Verweisziel in diesem Fall einfach als Internetadresse
   (URL) angegeben wird. Daran erkennt der Computer auch, dass es sich um einen
   externen Verweis, d.h. einen Verweis auf einen Text außerhalb des MLW handelt. 
   In diesem Fall wird sowohl in der Druck-
   als auch der Online-Ausgabe der angegebene Alias-Name übernommen.

#### 4.2.8 Verweisposition

Die Verweisposition wird durch das Schlüsselwort `VERWEISE`
eingeleitet. Es folgen ein oder mehrere Lemmawörter von
Wörterbuchartikeln, die mit dem beschriebenen Lemma in Zusammenhang
stehen. Die Lemmawörter können durch Leerzeilen oder
Kommata `,` voneinander getrennt sein. Der Hinweis `cf.` wird nicht
eingegeben, sondern in der Druck- oder Online-Ausgabe automatisch
ergänzt.

Die Lemmata in der Verweispostion dürfen - wie üblich - nummeriert sein 
und auch mit einem Stern für "nicht klassisch" markiert werden:

    VERWEISE  1. *sala 

#### 4.2.9 Autor/innen-Angabe

Die Autorangabe wird durch eines der Schlüsselworte `AUTOR`,
`AUTORIN`, `AUTOREN` oder `AUTORINNEN` eingeleitet. Darauf folgt der
Autorenname.

Beispiel:

    AUTORIN Weber

Mehrere Autornamen müssen durch das Wort "und" getrennt werden:

    AUTOREN Staub und Häberlin

## 5 Besondere Artikelformen

### 5.1 Nachtragsartikel

Nachtragsartikel enthalten Ergänzungen zur Druckausgabe, die
(ausschließlich) in den digital publizierten addenda auf der Homepage erscheinen.
(Bei der digitalen Ausgabe kann man Ergänzungen dagegen in ältere Artikel
nachträglich einarbeiten.) Sie sind genauso aufgebaut wie gewöhnliche
Wörterbucheinträge, allerdings wird der Lemma-Position noch eine
Nachtragsposition vorangestellt, die mit dem Schlüsselwort "NACHTRAG" eingeleitet
wird. In diese Nachtragsposition werden die "ad"- und
"post"-Angaben geschrieben, die dann im Druck den Nachtragsartikel einleiten.

        NACHTRAG ad p. 66,32; post attingo:

Die Nachtragsangaben haben immer einer der folgenden Formen:

1. `ad` Stellenangabe `post` Lemma. Beispiel: `ad p. 246,40; post 2. advena:`
2. `ad` Lemma `post` Stellenangabe. Beispiel: `ad affligo; post p. 366,8:`
3. `post` Stellenangabe. Beispiel: `post p. 130,24:`

*Wichtig*: Die `ad` und die `post`-Angabe müssen durch ein Semikolon
getrennt sein, da der Computer andernfalls vermutet, dass das Wort "post"
noch Teil der Stellenangabe ist.

Gesamtbeispiel für einen Nachtragsartikel:

    NACHTRAG post p. 130,24:

    LEMMA acronychus (achronicus)

    GRAMMATIK
        adiectivum I; -a, -um

    ETYMOLOGIE
      ἀκρόνυχος: {de script. achronicus cf.} ThLL. s. v.

    BEDEUTUNG
        nocte imminente eveniens --
        bei Einbruch der Nacht eintretend, ‘akronychisch’:

        * PS. BEDA mund. const.; 1,235 "Venus ... et Mercurius numquam
          in triceto vel in quadriceto regulariter distant a sole nec
          #ach|.ron.|ico ortu oriuntur aut occidunt, quia {eqs.}";
          1,246 "est ... ortus alius mundanus, alius heliacus, alius
          #ach|.ron.|icus."; 1,249 "achronicus, cum aliqua stella vel
          sidus occidit et oppositum illius per diametrum oritur;
          ‘chronos’ autem ‘tempus’ interpretatur."; 1,254 "achronicus
          est occasus, cum {eqs.}"

    AUTORIN
        Pörnbacher


### 5.2 Verweisartikel

#### 5.2.1 Struktur von Verweisartikeln

Verweisartikel sind Artikel, die ein oder mehrere Lemmata enthalten, 
zu denen jeweils ein Verweis ("vide" bzw. "v.") auf den Artikel steht, 
in welchem das Lemma beschrieben wird. 

Der einfachste Fall ist ein bloßer Verweis, bei dem von einem Wort 
(Schreibvariante) auf ein anderes, d.h. auf die Normform des Lemmas, 
verwiesen wird, unter dem es zitiert wird.

Beispiel 1: "**duntaxat** *v.* dumtaxat."

In der Druckfassung folgt oft eine ganze Liste solcher Verweise
aufeinander, deshalb wird bei der Eingabe von Verweislemmata eine
Ausnahme von der Regel: ein Lemma bzw. ein Artikel = eine .mlw-Datei
gemacht. Ein Verweis-Artikel kann also z.B. so aussehen:

    LEMMA edicula VIDE aedicula
    LEMMA edif-   VIDE aedif-
    LEMMA *ediica VIDE *aedoeicon
    LEMMA edil-   VIDE aedil-        PRAETER *edilinus VIDE *haedilinus
    LEMMA edinus  VIDE haedinus
    LEMMA ediosmus VIDE hedyosmus
    LEMMA edipnia VIDE dyspnoea
    LEMMA ediscero VIDE edissero

    AUTORIN Weber

Verweisartikel sind dadurch als solche kenntlich, dass sie nach jedem
Lemma das Schlüsselwort `VIDE` und danach das Lemma des Artikels enthalten, 
auf den verwiesen wird. Das Schlüsselwort `VIDE` wird in der
Vorschau und im Druck automatisch durch ein kleines kursives *v.*
ersetzt. Ebenso das Schlüsselwort `PRAETER` durch *praeter* und `ET`
durch *et*. Auf das Schlüsselwort `PRAETER` darf auch eine ganze Liste
von Vide-Paaren folgen, die dann aber jeweils in einer neuen Zeile
stehen müssen:

    LEMMA inkoo VIDE incoho
    LEMMA inl- VIDE ill- PRAETER
        inledo VIDE illido
        inleno VIDE illino
        inlicetus VIDE 1. illicitus
        ...

**Wichtig:** Für *et* sollte in Verweisartikeln (!) immer das
großgeschriebene Schlüsselwort `ET` geschrieben werden und nicht die
Zusatzauszeichnung `{et}`, die bei der Lemmaangabe ohnehin nicht
zulässig ist. (Der tiefere Grund, weshalb hier ein Schlüsselwort "ET"
erfordert wird, besteht darin, dass die Software die durch `ET`
getrennten Wörter als unterschiedliche Lemmata erfassen soll und nicht
als ein aus mehreren Wörtern bestehendes Lemma, in dem kontingenterweise
ein kursives *et* vorkommt. Analoges gilt für `PRAETER`.)

Wenn es sich allerdings um ein "unechtes" Lemma ("Geisterlemma") handelt, 
dann werden bloße Verweise in eckige Klammern gesetzt.

Beispiel 2:
    
    LEMMA [2. iniustus VIDE in ET iustus] 

Nach dem Ziellemma wird normalerweise kein Punkt gesetzt, den ergänzt 
VSC automatisch.

Wichtig: Die öffnende eckige Klammer darf erst nach dem Schlüsselwort LEMMA stehen!

In der Regel folgt auf einen Verweis kein Beleg. Weist der Verweis allerdings 
in die 'weitere Zukunft', dann wird als Service für den Benutzer der betreffende 
Beleg (mit oder ohne Belegtext) angeführt.
Diese Verweise werden ebenfalls in fette eckige Klammern eingespannt 
- der Grund ist rein optisch, um den Zusammenhalt besser aufzuzeigen.

Beispiel 3:

    LEMMA [ianti VIDE oenanthe: * ALPHITA; I 12]

Was in der Vorschau ungefähr so aussieht: "[**ianti** *v.* oenanthe: ALPHITA I 12.]"

**Wichtig**: Die eckige Klammer sollte im Quelltext nur dann gesetzt werden,
wenn es sich auch tatsächlich um ein unechtes Lemma handelt. In der Druckausgabe
werden daneben zwar auch solche Verweise in eckige Klammern gesetzt, die Belege
mit Belegtext mitführen. Das dient dazu, dass die Leserinnen und Leser diese
sehr langen Verweise leichter als Ganzes erfassen können. Aber diese eckigen
Klammern ergänzt das Übersetzungsprogramm ganz automatisch. Setzt man einen
solchen Verweis mit Belegtext bloße seiner Länge wegen im Quelltext in
eckige Klammern, dann schließt das Programm daraus, dass es sich um ein unechtes
Lemma handelt und annotiert das entsprechend in den XML-Daten, was
möglicherweise falsch ist. Also: Auch lange Verweise mit Belegtext sollten im
Quelltext nur dann in eckige Klammern eingeschlossen werden, wenn es sich um
unechte Lemmata handelt. 

Die Form eines Eintrags in einem Verweis-Artikel ist immer dieselbe:

    LEMMA Lemmawort VIDE Ziellemma (und/oder Verweis bzw. Zusatz) [ggf. ET..., PRAETER ...]

Anstelle des Lemmawortes dürfen auch mehrere durch Komma getrennte
Lemmaworte stehen, und an das Ziellemma dürfen mit `ET` weitere
Ziellemmata angehängt werden. Mit `PRAETER` können wie in dem Beispiel
oben zu sehen, Ausnahmen hinzugefügt werden, die bis auf die Tatsache,
dass sie durch das Schlüsselword `PRAETER` statt `LEMMA` eingeleitet
werden, dieselbe Form haben wie ein einfacher Lemma-Verweis mit `LEMMA`
und `VIDE`:

    LEMMA iamiam, iamiamque VIDE iam
    LEMMA [iammante VIDE iam ET ante: * DIPL. Otton I.; 340 p. 465,26]
    LEMMA iamscriptus VIDE iam ET scribo
    LEMMA iamtactus VIDE iam ET tango

**Wichtig**: die bisher in der Druckausgabe übliche Verdichtung "iamiam(que)"
ist nicht mehr möglich, da es notorisch schwierig ist, verdichtete
Formen mit einer Suche richtig zu erfassen. (Das gilt bereits für die
einfache Textsuche über das PDF oder das Trierer HTML.)

Eine dritte Art von Verweisen sind Nachtragsverweise. Hier wurde innerhalb 
eines Artikels eine spezielle Schreibung oder Form zur Zeit der Abfassung 
noch nicht erfasst. Es fehlt also im betreffenden Artikel der Beleg 
für diese Schreibung.

Solche Nachtragsverweise folgen immer einem fixen Schema.
Auch hier wird der Verweis in fette eckige Klammern gesetzt. Nach dem bloßen 
Verweis, der mit einem Punkt endet, folgt nach einem vergrößertem 
Spatium "{adde ad} vol. X p. X,Y: Beleg mit Belegtext". 

Beispiel 4:

    LEMMA [ideum VIDE antidotum. {adde ad} vol. I. p. 709,27: ANTIDOT. Glasg.; p. 106,36 "cousque <<conpleas antidotum>> ({cod.,} conplectant ideum {ed.})."]

Dies sieht in der Druckausgabe folgendermaßen aus:
[**ideum** *v.* antidotum.    *adde ad vol. I. p. 709,27:* ANTIDOT. Glasg. p. 106,36 cousque <conpleas antidotum> *(cod.,* conplectant ideum *ed.)*.]"

Wichtig: Die auf "add ad" folgende Stellenangabe sollte nicht mehr innerhalb des Zusatzes notiert werden!


#### 5.2.2 Formatierung von Verweisartikeln

Die voreingestellte Formatierung von Verweisartikeln besteht darin, nach jeweils zwei
Verweisen einen Zeilenumbruch einzufügen. Nicht immer ist diese Voreinstellung angemessen, 
so kann es bei besonders langen Verweisen vorkommen, dass die Länge der Zeile überschritten
wird (was leider erst in der Druckvorschau ersichtlich ist). In diesem Fall kann man 
durch Einfügen einer Leerzeile einen Zeilenwechsel erzwingen:

    LEMMA [sagoga VIDE isagoga.]
    LEMMA sagopinum VIDE sagapenum
    LEMMA sagra VIDE sacer
    LEMMA sagramentum VIDE sacramentum
    LEMMA sagrarium VIDE sacrarium
    
    LEMMA sagrelecus, sagrilicus VIDE sacrilegus
    
    LEMMA sagrum, sagrus VIDE sacer
    LEMMA saguis VIDE sanguis

In diesem Beispiel wird `LEMMA sagrelecus, sagrilicus VIDE sacrilegus` in eine enzelne
Zeile gesetzt. Das funktioniert auch innerhalb von "praeter"-Blöcken:

    LEMMA inl- VIDE ill- PRAETER

        inledo VIDE illido
        inleno VIDE illino
        inlicetus VIDE 1. illicitus
        1. inlisus VIDE illaesus
        2. inlisus VIDE illido

        inluctabilis VIDE ineluctabilis
        inlusdro VIDE illustris

Hier wird sowohl nach "praeter" ein Zeilenwechsel erzwungen als auch vor "inluctabilis".

Umgekehrt kann man einen Zeilenumbruch unterbinden, indem man im Quelltext, den nächsten
Verweis an den vorhergehenden anhängt. So führt beispielsweise:

    LEMMA [sagoga VIDE isagoga]
    LEMMA sagopinum VIDE sagapenum LEMMA sagra VIDE sacer

dazu, dass alle drei Verweise in eine Zeile geschrieben werden. Das funktioniert innerhalb 
von "praeter"-Blöcken jedoch nicht. Hier muss jeder Verweis in einer eigenen Zeile stehen,
sonst gibt es eine Fehlermeldung.


#### 5.2.3 Verbergen von Einträgen

In der Druckausgabe würde man in einen Verweisartikel keine Verweise auf Lemmata eintragen,
die in unmittelbarer Nähe des Verweises stehen. Für die Einheitlichkeit der Artikeldaten und die 
Online-Ausgabe wäre es aber durchaus sinnvoll solche Verweise mit in den Quelltext des
Artikels zu schreiben - nur sollten sie im Druck eben nicht angezeigt werden. Um das
zu erreichen, müssen Einträge auf in der Nähe stehende Lemmata, 
die nicht im Druck angezeigt werden sollen, mit dem
Schlüsselwort `VERBORGEN` gekennzeichnet. Das Schlüsselwort `VERBORGEN` wird unmittelbar 
vor das Lemma (und damit hinter das Schlüsselwort `LEMMA`) geschrieben. Beispiel:

    LEMMA  *salvacella VIDE *salvatella
    LEMMA  VERBORGEN *salvamen VIDE salvamentum

Bei durch eckige Klammern gekennzeichneten "unberechtigten" Lemmata muss 
das Schlüsselwort "VERBORGEN" innerhalb der eckigen Klammern notiert werden, wie
in dem folgenden fiktiven Beispiel: 

    LEMMA [VERBORGEN iammante VIDE iam ET ante: DIPL. Otton I.; 340 p. 465,26.]


#### 5.2.4 hic non tractatur - Einträge ohne Artikel

Manche Wörter werden im Mittellateinischen Wörterbuch zwar verzeichnet, aber mit
Ausnahme der Grammtikangabe nicht durch einen vollständigen Artikel beschrieben.

In diesem Fall beendet man den Artikel nach der Grammatikangabe mit dem Schlüsselwort:

    HIC NON TRACTATUR 

oder alternativ:

    HIC NON TRATANTUR

Im Ganzen könnte ein solcher abgekürzter Artikel dann so aussehen:

    LEMMA inter
   
    GRAMMATIK adv. vel praep.
   
    HIC NON TRACTATUR
   
    AUTOR Obelix


### 5.3 Mehrere Artikel in einer Datei

Gerade bei kurzen Artikeln, wie das typischerweise Nachtragsartikel sind, kann es umständlich 
sein für jeden dieser Artikel eine eigene Datei anzulegen, weil dann der Ordner mit den Dateien
aufgrund von deren schierer Menge schnell unübersichtlich wird.

Es ist aber ohne Probleme 
möglich mehrere Artikel hintereinander weg in eine Datei zu schreiben. Bei der Übersetzung
nach XML werden daraus zwar einzelne XML-Dateien erzeugt, deren Namen sich nach dem jeweiligen
Lemma richten, aber das sollte nicht weiter stören, da diese Dateien ohnehin nur im 
Ausgabe-Ordner liegen, von wo alle folgenden Verarbeitungsschritte automatisch durchgeführt werden. 

Hier ein Beispiel für eine Datei mit mehreren Artikeln:

    ÜBERSCHRIFT Addenda
   
    NACHTRAG post p. 517,52:
    LEMMA *altipetens
    GRAMMATIK
    adi. III; -entis
    BEDEUTUNG
        altipetax, in altum tendens -- hochstrebend, in die Höhe strebend:
        * INSCR. Germ.; LXX 79 B 5 (a. 1035/48?) ((vs.)) "#altipeten.|s vates 
          ... ille Iohannes {(sc. evangelista)}."
   
    NACHTRAG post p. 558,43:
    LEMMA *amezator (admiz-, am(m)iz-, amita-, amensa-, -zia-)
    GRAMMATIK subst. III; -oris m.
    ETYMOLOGIE
        {cf.} ital. mezzo, mezzatore:
        * {v. et} Sella, Gloss. Lat. Emil.; et Gloss. Lat. Ital.; s. v.
    BEDEUTUNG
        mediator -- Mittler:
        * CONST. Melf.; 1,81 "de iunctis ... eligi prohibemus."  // Beispiel gekürzt!
   
    AUTORIN Pörnbacher


Merke: Es genügt, die Autor- oder Autorinnenangabe einmal ganz am Schluss anzuführen.
Sie gilt dann für alle vorhergehenden Artikel innerhalb derselben Datei bis zur 
nächst früheren Autoren oder Autorinnenangabe oder bis zum Anfang der Datei.

### 5.4 Zwischenüberschrifen

Mit dem Schlüsselwort `ÜBERSCHRIFT` kann man vor jedem Nachtragsartikel (siehe Beispiel oben) 
und ebenso vor oder innerhalb von Verweisartikeln eine (Zwischen-)Überschrift einfügen:

    ÜBERSCHRIFT C

    LEMMA *cyrographicus VIDE *chirographicus
    LEMMA [cyrrhon VIDE *acerum.]
    LEMMA *cyrurgium VIDE *chirurgium
    LEMMA *cytola VIDE *citola
   
    ÜBERSCHRIFT D
   
    LEMMA daderia VIDE dataria
    LEMMA dediscalus VIDE didascalus
    LEMMA *dedularis VIDE *titillaris
   
    LEMMA [duleum VIDE solium: ANTIDOT. Berolin.; app. p. 77,8] 
   
    AUTORIN Pörnbacher


Diese Methode sollte nicht für Artikel verwendet werden, die in den Druck-Faszikel kommen, 
sondern nur für Nachtragsartikel, die auf der Website veröffentlicht werden!


## 6 Fehler und Probleme mitteilen

Wie jede einigermaßen komplexe Software enthält auch das Programm zur
Verarbeitung der MLW-Notation Fehler. Wenn Euch ein Fehler auffällt, 
oder wenn Ihr auch einfach nur irgendwo nicht weiter wisst, dann ist 
der beste Weg, Fehler zu melden, ein "Issue" (dt. etwa "Angelegenheit" 
oder "Problem", lat. vielleicht "eventum", besser nicht "exitus") 
anzulegen. Das geht so:

1. Rufe die Seite [gitlab.lrz.de/badw-it/MLW-DSL](https://gitlab.lrz.de/badw-it/MLW-DSL)
   auf. Dabei handelt es sich übrigens um das Quellcode-Repositorium 
   (genaugenommen eins von mehreren) für die MLW-Software.
   
2. In der Seitenleiste ist ein Menü mit einer ganzen Reihe, teils
   rätselhafter, Einträge. Irgendwo in der Mitte steht "Issues".  Wähle diesen
   Menüpunkt. Und es erscheint eine Liste mit bereits gemeldeten Fehlern.
   
3. Ist derselbe oder ein ähnlicher Fehler bereits gemeldet, dann klicke
   ihn an und ergänze die daraufhin erscheinende Meldung (oder vielleicht
   auch schon Diskussion) zu dem Fehler ggf. um einen eigenen Kommentar.
   
4. War der Fehler noch nicht gemeldet, dann klicke rechts oben auf den
   grün unterlegten Knopf "New Issue" um ein neues "eventum" mitzuteilen.
   
5. In dem sich nun öffnenden Formular kann u.a. ein Titel eingegeben werden,
   eine ausführliche Beschreibung des Fehlers und - sehr nützlich - es
   können Dateien angehängt werden.
   
6. Es ist extrem hilfreich, wenn zu dem gemeldeten eventum mindestens die
   .mlw-Quelldatei, am allerbesten dazu aber auch die .ausgabe.xml-Datei
   angehängt werden! Die ".ausgabe.xml"-Datei erscheint in dem Unterordner
   `Ausgabe/XML-Druckvorstufe` des Ordners in dem die .mlw-Datei steht,
   wenn eine HTML-Vorschau oder Druckvorschau erzeugt wurde.   

7. Mit dem Knopf "Submit Ticket" unten Links im "Issue"-Formular kann die
   Meldung abgeschlossen werden. Über Ergänzungen oder auch die Lösung
   des Fehler wird man bequem per E-Mail informiert.

## 7 Anhang

### 7.1 Erlaubte Wortarten und Wortartklassen

| **Wortartangabe** | Abkürzung | *Zusatzangaben*              | *Beispiel*   |
|-------------------|-----------|------------------------------|--------------|
| adverbium         | adv.      | -                            | `adv.`       |
| adiectivum        | adi.      | Nomen-Klasse (erforderlich)  | `adi.  I-II` |
| comparativus      | compar.   | Nomen-Klasse (erforderlich)  | `compar. I`  |
| interiectio       | interi.   | Wortart-Ergänzung (optional) | `interi. (ir)ridendi {(cf. ...)}` |
| littera           | -         | Wortart-Ergänzung (optional) | `littera`    |
| numerale          | numer.    | Nomen-Klasse (erforderlich)  | `numer. III` |
| particula         | -         | -                            | `particula`  |
| praepositio       | praep.    | -                            | `praep.`     |
| pronomen          | pron.     | Nomen-Klasse (erforderlich)  | `pron. anormal` |
| substantivum      | subst.    | Nomen-Klasse (erforderlich)  | `subst. V`   |
| superlativus      | superl.   | Nomen-Klasse (erforderlich)  | `superl. II` |
| verbum            | -         | Verb-Klasse (erforderlich)   | `verbum IV`  |

**Nomen-Klassen**: `I`, `II`, `I-II`, `III`, `IV`, `V`, `anormal`, `indecl.`, `unbestimmt`

**Verb-Klassen**: `I`, `II`, `III`, `IV`, `anormal`, `unbestimmt`

**Wortart-Ergänzung**: Beliebige (aber am besten nicht zu ausführliche)
weitere Angaben, Beispiele, Zusätze oder Verweise. z.B.:

    interiectio (ir)ridendi {(cf. {=> ThLL. VI/3. p. 2513,56sqq.|-})}

### 7.2 Erlaubte Sprachangaben in der Etymologie-Position

| Sprachangabe    | optionale Ergänzung   |
|-----------------|-----------------------|
| anglosax.       | vet.                  |
| arab.           |                       |
| bohem.          | vet.                  |
| catal.          |                       |
| dan.            |                       |
| finnice         |                       |
| franc.          | vet.; vet. inf.       |
| francog.        | vet.                  |
| francoprov.     |                       |
| frisic.         |                       |
| gall.           |                       |
| germ.           |                       |
| gr.             | byz.                  |
| got.            |                       |
| hebr.           |                       |
| hibern.         |                       |
| hisp.           | vet.                  |
| hung.           |                       |
| ital.           |                       |
| lombard.        |                       |                                  
| langob.         |                       |
| lat.            |                       |
| occ.            | vet.                  |
| pers.           |                       |
| polon.          | vet.                  |
| port.           |                       |
| prov.           |                       |
| raetoroman.     |                       |
| sard.           |                       |
| sicil.          |                       |
| slav.           |                       |
| theod.          | inf.; vet.; inf. vet. |
| turc.           |                       |
| val.            |                       |

### 7.3 Sonderzeichen und Texteingabe

#### 7.3.1 Unterstützte Sprachen und Zeichen

Im MLW kommen drei Sprachen vor: Deutsch, Lateinisch und Griechisch. Die MLW-DSL
unterstützt alle diese Sprachen mit sämtlichen diakritischen Zeichen, soweit sie
in Unicode definiert sind. (Für Latein und Griechisch ist das bereits
volltständig umgesetzt, aber noch nicht für Deutsch. Wird irgendein 
benötigtes Zeichen nicht unterstützt, schickt bitte einen Fehlerbericht an
<arnold@badw.de> oder <digitalisierung@badw.de>!) Sonderzeichen, die nicht in
Unicode enthalten sind, können ggf. durch eine dafür festzulegende Kombinationen
mehrerer Zeichen eingegeben werden. Der Fall kam bisher jedoch noch nicht vor. 

#### 7.3.2 Eingabe von Fremdsprachen oder Sonderzeichen

(Sonder-)zeichen, die nicht auf der Tastatur zu finden sind, lassen sich über
das mit Windows mitgelieferte Programm "Zeichentabelle" eingeben (Windows-Taste
drücken oder Windowsknopf in der Arbeitsleiste anklicken, und dann
"Zeichentabelle" tippen und das Programm durch Mausklick oder RETURN-Taste
starten). Dort kann man die Zeichen mit der Maus auswählen, kopieren und dann im
Text-Editor über das Bearbeiten-Menü oder die Tastenkombination STRG-V einfügen.

*Tip von Tanja Buckatz*: Speziell für **griechische** Schrift, gibt es noch eine
einfachere Methode, nämlich den Betacode-Converter. Auf der Website:
<https://graecum-latinum.de/gr_materialien/konverter/betacode_konverter.html>
kann man griechischen Text nach einer dort im Hilfe-Text erklärten Zuordnung mit
einer lateinischen Tastatur eingeben. Den griechen Text kann man dann einfach in
das eigene Dokument hinüber kopieren.

**Falsche Freunde (!)**: Es gibt Zeichen, die gleich aussehen, aber
unterschiedlichen Alphabeten zugeordnet werden. So kommt z.B. das Zeichen 'ó'
sowohl im lateinischen als auch im griechischen Alphabeth vor. Es hat aber
jeweils einen unterschiedlichen Zeichencode. Es sollte vermieden werden
innerhalb eines Wortes Zeichen aus unterschiedlichen Alphabethen einzugeben. Der
Computer versucht solche Fehler zu erkennen warnt dann vor "unechten
Buchstaben". Beispiel: `ἀκρóνυχος` enthält ein lateinisches "ó". Richtig wäre:
`ἀκρόνυχος`. Der Unterschied ist kaum zu erkennen. (Ab Version 1.63 kann man
Visual Studio Code in den EInstellungen so konfigurieren, dass solche "falsche
Freunde" optisch hervorgehoben werden: <https://code.visualstudio.com/updates/v1_63#_unicode-highlighting>)


#### 7.3.3 Anzeige von Zeichen

##### 7.3.3.1 Bei der Eingabe

Dass ein bestimmter Zeichencode unterstützt wird, heißt noch nicht, dass
das Zeichen bei der Eingabe auch angezeigt werden kann. Dafür muss für
das Zeichen auch die passende Vektorgrafik in dem Schriftsatz hinterlegt
sein, den Visual Studio Code im Eingabefenster verwendet. Die
standardmäßig verwendete Consolas-Schriftart unterstützt u.a. die
Sprachfamilien Lateinisch (einschließlich Deutsch) und Griechisch und
für jede dieser Familien eine große Menge der Sonderzeichen und
diakritischen Zeichen. Sollte sich während der Arbeit am Wörterbuch der
Fall eintreten, dass ein bestimmtes Zeichen nicht (richtig) angezeigt
wird, werden wir dann eine Ersatzzeichenkombination festlegen.

##### 7.3.3.2 Bei der Ausgabe

Sowohl bei der Druck- als auch bei der HTML-Ausgabe lassen sich anders
als bei der Eingabe mehrere unterschiedliche Schriftarten in ein- und
demselben Dokument umschalten. Für die Fälle, die damit nicht abgedeckt
werden, können eigene Zeichen mit einem Schriftart-Editor geschaffen
werden, was wir dann machen, wenn der Fall eintritt.

### 7.4 Allgemeine Computer-Probleme

#### 7.4.1 Gemeinsames Bearbeiten von Dateien

Sync & Share bietet das gleichzeitige gemeinsame Bearbeiten nur für 
OnlyOffice-Dokumente an, d.h. Textverarbeitungs-Text, Tabellen, Präsentationen,
aber nicht für andere Dateien und leider auch nicht für einfachen Text.

Bei anderen Dateien werden beim quasi-gleichzeitigen Zugriff kopien angelegt, 
denn weil bei sync&share sowieso jeder auf eine eigene Kopie derselben Daten
zugreift, bemerkt Sync&Share den Zugriffskonflikt immer erst zeitlich verzögert 
beim nächsten Synchronisationsversuch - zu spät für eine Warnung, 
daher das Anlegen der Kopie.

Man kann auf Sync&Share synchron arbeiten, wenn man im Browser mit OnlyOffice 
ein gemeinsames Dokument bearbeitet. Dann tauchen wie bei google-docs 
mehrere Cursor auf und jeder sieht, wer noch mitschreibt und wo. Das bietet
sync&share aber leider nur für Office-Dokumente und nicht für einfache
Textdateien an, obwohl das bei Textdateien im Grunde noch einfacher wäre 
("etherpad").

Gruppenlaufwerk: Wenn hier eine Datei von mehreren Programmen gleichzeitig 
geöffnet ist, werden Änderungen an der Datei allen Programmen mitgeteilt.
Ob und wie diese Programme dann reagieren, bleibet den Programmen selbst
überlassen. Bessere Texteditoren wie Visual Studio Code oder notepad++
weisen den Nutzer darauf hin, dass sich die Datei auf dem Laufwerk geändert
hat und fragen, ob die Änderungen neu geladen werden sollen. Dadurch
können nicht gespeicherte Änderungen im eigenen Texteditor aber
überschrieben werden.

Existieren bereits mehrere unterschiedliche Varianten derselben Text-Datei,
dann kann man sie man besten mit einem visuellen Editor wieder zusammenfügen.
Das geht mit Visual Studio Code ("Select for Compare" im Kontext-Menü der
ersten Datei wählen, und dann "Compare with selected" im Kontext-Menü der
zweiten.) Noch bequemer geht es mit einem Speziellen Zusammenführungs-Editor
wie meld: https://meldmerge.org/
