#!/bin/sh

# see: https://anaraven.bitbucket.io/blog/2018/numbering-pages-of-a-pdf.html
# see also: http://cazencott.info/index.php/post/2015/04/30/Numbering-PDF-Pages

md2pdf --wkarg="--margin-top=2.8cm" MLW_Notation_Beschreibung.md
mv MLW_Notation_Beschreibung.pdf MLW_Notation_Beschreibung_ohne_Seitenzahlen.pdf

enscript -F Helvetica10 -L1 -b'||Seite $%' -o- < <(for i in $(seq 1 500); do echo; done) | ps2pdf - | pdftk 'MLW_Notation_Beschreibung_ohne_Seitenzahlen.pdf' multistamp - output MLW_Notation_Beschreibung.pdf
