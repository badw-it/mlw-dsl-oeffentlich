#!/usr/bin/env python3

"""MLWParser.py - parses MLW-files, called by MLWCompiler

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2019 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


#######################################################################
#
# SYMBOLS SECTION - Can be edited. Changes will be preserved.
#
#######################################################################


import collections
from functools import partial
import os
import sys
from typing import cast, Tuple, List, Callable, Sequence

try:
    scriptpath = os.path.dirname(__file__)
except NameError:
    scriptpath = ''
dhparser_parentdir = os.path.abspath(os.path.join(scriptpath, r'DHParser'))
if scriptpath not in sys.path:
    sys.path.append(scriptpath)
if dhparser_parentdir not in sys.path:
    sys.path.append(dhparser_parentdir)

try:
    import regex as re
except ImportError:
    import re
from DHParser.configuration import set_config_value, get_config_value, access_thread_locals, \
    access_presets, finalize_presets, set_preset_value, get_preset_value, NEVER_MATCH_PATTERN
from DHParser.error import ErrorCode, Error, canonical_error_strings, has_errors, NOTICE, \
    WARNING, ERROR, FATAL, is_error
from DHParser.nodetree import Node, WHITESPACE_PTYPE, TOKEN_PTYPE, RootNode, Path, prev_path, \
    next_path
from DHParser.parse import Grammar, PreprocessorToken, Whitespace, Drop, AnyChar, Parser, \
    Lookbehind, Lookahead, Alternative, Pop, Text, Synonym, Counted, Interleave, INFINITE, ERR, \
    Option, NegativeLookbehind, OneOrMore, RegExp, Retrieve, Series, Capture, TreeReduction, \
    ZeroOrMore, Forward, NegativeLookahead, Required, CombinedParser, Custom, mixin_comment, \
    last_value, matching_bracket, optional_last_value, SmartRE
from DHParser.pipeline import create_parser_junction, create_preprocess_junction, \
    create_junction, Junction, PseudoJunction
from DHParser.preprocess import nil_preprocessor, PreprocessorFunc, PreprocessorResult, \
    gen_find_include_func, preprocess_includes, make_preprocessor, chain_preprocessors
from DHParser.toolkit import is_filename, load_if_file, cpu_count, RX_NEVER_MATCH, \
    ThreadLocalSingletonFactory, expand_table
from DHParser.transform import is_empty, remove_if, TransformationDict, TransformerFunc, \
    transformation_factory, remove_children_if, move_fringes, normalize_whitespace, \
    is_anonymous, name_matches, reduce_single_child, replace_by_single_child, replace_or_reduce, \
    remove_whitespace, replace_by_children, remove_empty, remove_tokens, flatten, all_of, \
    any_of, transformer, merge_adjacent, collapse, collapse_children_if, transform_result, \
    remove_children, remove_content, remove_brackets, change_name, remove_anonymous_tokens, \
    keep_children, is_one_of, not_one_of, content_matches, apply_if, peek, \
    remove_anonymous_empty, keep_nodes, traverse_locally, strip, lstrip, rstrip, \
    replace_content_with, forbid, assert_content, remove_infix_operator, add_error, error_on, \
    left_associative, lean_left, node_maker, has_descendant, neg, has_ancestor, insert, \
    positions_of, replace_child_names, add_attributes, delimit_children, merge_connected, \
    has_attr, has_parent, has_children, has_child, apply_unless, apply_ifelse, PositionType, \
    normalize_position_representation, pick_longest_content, AT_THE_END, traverse, is_token, \
    update_attr, contains_only_whitespace, transformer
from DHParser import parse as parse_namespace__

from MLWDaten import opera


#######################################################################
#
# PREPROCESSOR SECTION - Can be edited. Changes will be preserved.
#
#######################################################################

RE_INCLUDE = NEVER_MATCH_PATTERN
# To capture includes, replace the NEVER_MATCH_PATTERN
# by a pattern with group "name" here, e.g. r'\input{(?P<name>.*)}'

FATALER_FEHLER_TABULATOREN_IM_TEXT = ErrorCode(11000)


def neuTokenizer(original_text) -> Tuple[str, List[Error]]:
    # Here, a function body can be filled in that adds preprocessor tokens
    # to the source code and returns the modified source.
    errors: List[Error] =  []
    t = original_text.find('\t')
    while t >= 0:
        errors.append(Error('Tabulator entdeckt. Sollte durch Leerzeichen ersetzt werden! Vgl.: '
                            'https://gitlab.lrz.de/badw-it/MLW-DSL/-/blob/development/Dokumentation/MLW_Notation_Beschreibung.md#362-tabulatoren-entfernen',
                            t, FATALER_FEHLER_TABULATOREN_IM_TEXT))
        t = original_text.find('\t', t + 1)
    return original_text, errors


def preprocessor_factory() -> PreprocessorFunc:
    # below, the second parameter must always be the same as neuGrammar.COMMENT__!
    find_next_include = gen_find_include_func(RE_INCLUDE, '#.*')
    include_prep = partial(preprocess_includes, find_next_include=find_next_include)
    tokenizing_prep = make_preprocessor(neuTokenizer)
    return chain_preprocessors(include_prep, tokenizing_prep)


get_preprocessor = ThreadLocalSingletonFactory(preprocessor_factory)

#######################################################################
#
# PARSER SECTION - Don't edit! CHANGES WILL BE OVERWRITTEN!
#
#######################################################################

class MLWGrammar(Grammar):
    r"""Parser for a MLW source file.
    """
    BelegKern = Forward()
    BelegText = Forward()
    BelegeOderAufzählung = Forward()
    Beschreibung = Forward()
    DeutscheBedeutung = Forward()
    Einschub = Forward()
    FREITEXT = Forward()
    HOCHGESTELLT = Forward()
    Junktur = Forward()
    Kursiv = Forward()
    LateinischeBedeutung = Forward()
    Nachschlagewerk = Forward()
    Sperrung = Forward()
    Stelle = Forward()
    TEXTELEMENT = Forward()
    TIEFGESTELLT = Forward()
    VARIANTE = Forward()
    Verweise = Forward()
    Zusatz = Forward()
    Zusätze = Forward()
    om_erwartet = Forward()
    opus = Forward()
    opus_minus = Forward()
    unberechtigt = Forward()
    source_hash__ = "66a581cb1bf374921c6462c15eec53a0"
    disposable__ = re.compile('_\\w+$')
    static_analysis_pending__ = []  # type: List[bool]
    parser_initialization__ = ["upon instantiation"]
    error_messages__ = {'Artikel': [(re.compile(r'(?=:)'), 'Hier darf kein (weiterer) Doppelpunkt stehen »{1}«, ggf. sollte er in einen (verschachtelten) Kommentar verschoben werden.'),
                                    (re.compile(r'\s*ETYMOLOGIE'), 'Die Etymologie-Position muss unmittelbar auf die Grammatik-Position folgen.'),
                                    (re.compile(r'\s*[A-Z][A-Z][A-Z]+'), 'An dieser Stelle sollte {0} folgen, nicht »{1}«')],
                        '_LemmaStück': [(re.compile(r'\['), 'Die »unberechtigt«-Markierung [] muss immer am Anfang, ggf. noch vor der Lemma-Nr. stehen!')],
                        'Vide': [(re.compile(r'\s*{'), "Ein Zusatz ist an dieser Stelle nicht erlaubt: »{1}«"),
                                 (re.compile(r'\s*\*'), "Hier keine Belege erwartet, möglicherweise wurde ein Doppelpunkt vergessen oder irrtümlich innerhalb von Zusatzklammern notiert.")],
                        'NachtragsPosition': [(re.compile(r'(?=)'), 'ad oder post -Klausel fehlerhaft oder fehlend: »{1}«')],
                        'Nachtrag': [(re.compile(r'(?=)'), 'Doppelpunkt, ggf. gefolgt von einer Ziffer erwartet, aber nicht »{1}«')],
                        'ad': [(re.compile(r'(?=)'), 'Nachtragslemma oder Nachtragsstellenangabe fehlt oder ist nicht richtig formatiert: »{1}«')],
                        'post': [(re.compile(r'(?=)'), 'Nachtragslemma oder Nachtragsstellenangabe fehlt oder ist nicht richtig formatiert: »{1}«')],
                        'LemmaPosition': [(re.compile(r'(?=)'), 'Artikelkopf, Etymologie- oder Bedeutungsposition erwartet, aber nicht »{1}«!')],
                        'LemmaBlock': [(re.compile(r'(?=)'), '»{1}« ist kein gültiges Lemma!')],
                        'Grammatik': [(re.compile(r'(?=nomen)'), '»nomen« ist als wortangabe nicht erlaubt, verwende bitte »substantiv«!')],
                        'GrammatikAngabe': [(re.compile(r'(?=,)'), 'Nach der Wortartangabe sollte ein Semikolon oder ein Zeilenwechsel folgen, kein Komma!'),
                                            ('', "Semikolon oder Zeilenwechsel nach Wortartangabe erwartet!")],
                        'wortarten': [('', 'Auf »vel« darf an dieser Stelle nur »raro«, »rarius«, »semel«, ein Zusatz oder gleich eine weitere Wortart folgen, aber nicht »{1}«!')],
                        'genera': [('', 'Auf »vel« darf an dieser Stelle nur »raro«, , »rarius«, »semel«, »bis«, ein Zusatz oder gleich ein weiterer genus folgen, aber nicht »{1}«!')],
                        'GrammatikVariante': [(re.compile(r'(?=)'), "Fehlerhafte Angabe bei Grammatik-Variante oder fehlende Belege!")],
                        'EtymologiePosition': [(re.compile(r'\.'), 'Hier scheint ein Punk zu viel zu sein: {1}'),
                                               (re.compile(r'[A-Za-z]+'), "Das Einlesen der Etymologie-Position kommt hier nicht weiter: »{1}«. Möglicherweise wurde an dieser Stelle versäumt, ein Zitat durch Anführungsstriche zu kennzeichnen.")],
                        'EtymologieAngabe': [(re.compile(r'(?=)'), 'Sekundärliteratur erwartet, aber »{1}« nicht als solche erkannt. Möglicherweise ist der Titel falsch oder wurde dem Programm noch nicht bekannt gemacht.')],
                        'Position': [(re.compile(r'(?!\s*\n)'), "Zeilenwechsel nach Schlüsselwort vor »{1}« erforderlich!")],
                        'Besonderheit': [(re.compile(r'\n* +\w'), "Einrückung scheint nicht zu stimmen. Zu viele oder zu wenig Leerzeichen!"),
                                         ('', "Hier sollten Verweise »{{=> ...}}«, Belege oder Unterkategorien folgen, nicht »{1}«. Möglicherweise wurde die maximale Gliederungstiefe von vier Ebenen überschritten.")],
                        'Bedeutungsangabe': [(re.compile(r'(?=\()'), "»{1}« kann nicht verarbeitet werden, möglicherweise wurde die Markierung eines Zusatzes mit geschweiften Klammern »{{ ... }}« versäumt?"),
                                             (re.compile(r'\n'), "Neuer Block: »{1}« an dieser Stelle nicht erwartet. Wurde die letzte Bedeutungsangabe nicht durch einen Doppelpunkt \':\' abgeschlossen?"),
                                             (re.compile(r'(?=)'), "»{1}« an dieser Stelle nicht erwartet. Möglicherweise enthält ein Teil der (vorhergehenden) Bedeungsangabe unerlaubte Zeichen.")],
                        'FreierZusatz': [(re.compile(r'(?=\{)'), "»{1}« kann nicht verarbeitet werden. Zusätze können nicht geschachtelt werden, dürfen aber Verweise »{{=> ...}}« enthalten."),
                                         (re.compile(r'[\\]'), "Hier darf kein rückwärts gerichteter Schrägstrich »{1}« stehen. Vielleicht wurde er mit einem vorwärts gerichteten Schrägstrich »/« verwechselt."),
                                         (re.compile(r'--'), 'Unerwarteter Doppelstrich: »--« Bedeutungsangaben sollten als Nebenbedeutungen notiert und nicht in Zusätze verpackt werden.')],
                        'Belege': [(re.compile(r'(?=\$)'), "Autorennamen sollten immer GROSSGESCHRIEBEN werden, mit Ausnahme von Sekundärliteratur. Auch bei disambiguierten Autorennamen: »{1}«")],
                        'Einschub': [(re.compile(r'(?=)'), "Innerhalb eines Einschubs dürfen nur Verweise, Zusätze, Belege oder Belegtext folgen, nicht »{1}«!")],
                        'Beleg': [(re.compile(r'\.( *\n| *\*)'), "Belege bitte nicht mit einem Punkt ».« abschließen! (Entschuldigt diese umständliche Regel!)"),
                                  (re.compile(r';\s*\*'), "Nach einem Semikolon werden weitere Belegstellen desselben Autors UND Werks erwartet, nicht: »{1}«"),
                                  (re.compile(r';\s*"'), "Hier fehlt eine Stellenangabe: »{1}«"),
                                  (re.compile(r'(?<=")[.]'), 'Trennpunkte nach Zitaten müssen innerhalb der Anführungsstriche stehen, z.B. »punctum.«, nicht »punctum«. !'),
                                  (re.compile(r'"(?!$)'), 'An der Stelle:  »{1}« ist kein weiteres Zitat erlaubt. Wurde zuvor evtl. eine doppelte runde Klammer »((« vergessen?'),
                                  (re.compile(r'\s*$'), "Unerwartetes Ende des Textes erreicht!"),
                                  (re.compile(r'\^ '), 'Das Hochstellungszeichen "^" muss dem Buchstaben (unmittelbar) vorangestellt werden.'),
                                  (re.compile(r'[()][^()]'), '»{1}« passt hier nicht. Einschübe, opera minora etc. sollten in doppelte runde Klammern »(( ... ))« gesetzt werden'),
                                  (re.compile(r'(?=)'), "Hier darf es nicht weitergehen mit: »{1}« (Möglicherweise ein Folgefehler eines vorhergehen Fehlers.)")],
                        'BelegText': [(re.compile(r'(?=\|)'), 'Das Zeichen »|« darf nur in einem mit »#« gekennzeichneten Lemmawort oder einer Abkürzung vorkommen!'),
                                      (re.compile(r'(?=\*)'), 'Quellen- und Literaturangaben sind innerhalb von Zitaten nur in Einschüben »((...))« erlaubt!'),
                                      (re.compile(r'(?=@)'), 'Anker müssen in geschweifte Klammern eingeschlossen werden, z.B. »{{@ invado_3a}}«.')],
                        '_AutorWerk': [('', 'Unerwartete Zeichenfolge »{1}« im Autor- oder Werknamen. Wurde ein Semikolon vor der Stellenangabe »;« vergessen?')],
                        'VerweisKern': [(re.compile(r'[\w://=?.%&\[\] ]*,[^|]*}'), 'Komma als Trenner zwischen Verweisen nicht erlaubt. Verwende Semikolon »;«.'),
                                        (re.compile(r'[^|]*}'), 'Kein gültiges Verweisziel: »{1}« oder Platzhalter »|-« fehlt nach Alias!'),
                                        (re.compile(r'=>'), 'Kein gültiger Verweis »{1}«. Mglw. ein Verweiszeichen »=>« zu viel.'),
                                        (re.compile(r'(?=)'), 'Kein gültiger Verweis: »{1}«')],
                        'Anker': [(re.compile(r'(?=)'), "Ungültiger Ankername (erlaubte Zeichen: A-Z, a-z, 0-9 und _) oder keine schließende geschweifte Klammer.")],
                        'Verweis': [('', 'Hier muß eine gültige Zielangabe (z.B. »domus/domus_22«) oder NUR ein Platzhalter »-« stehen, aber kein weiterer Text es sei denn innerhalb von Kommentarzeichen /* ... */')]}
    COMMENT__ = r'(?://.*)|(?:/\*(?:.|\n)*?\*/)'
    comment_rx__ = re.compile(COMMENT__)
    comment__ = RegExp(comment_rx__)
    WHITESPACE__ = r'[\t ]*'
    WSP_RE__ = mixin_comment(whitespace=WHITESPACE__, comment=COMMENT__)
    wsp__ = Whitespace(WSP_RE__)
    NUMMER = SmartRE(f'(\\d+)(?P<:Whitespace>{WSP_RE__})', '/\\d+/ ~')
    UNTERDRÜCKE_WARNUNGEN = SmartRE(f'(?:(?P<:Text>UNTERDRÜCKE)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>WARNUNG)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>EN)(?P<:Whitespace>{WSP_RE__}))?)', '"UNTERDRÜCKE" "WARNUNG" ["EN"]')
    LZ = OneOrMore(Alternative(comment__, RegExp('\\s+')))
    Unterdrücke_Warnungen = Series(UNTERDRÜCKE_WARNUNGEN, Option(LZ), Option(Series(NUMMER, ZeroOrMore(Series(Option(Series(Text(","), wsp__)), Option(LZ), NUMMER)))))
    _LATEIN_GROSS_FOLGE = RegExp('(?x)(?:[\\x41-\\x5A]|[\\xC0-\\xD6]|[\\xD8-\\xDE]|\\u0100|\\u0102|\\u0104|\\u0106|\\u0108|\\u010A|\\u010C\n'
                  '|\\u010E|\\u0110|\\u0112|\\u0114|\\u0116|\\u0118|\\u011A|\\u011C|\\u011E|\\u0120|\\u0122|\\u0124\n'
                  '|\\u0126|\\u0128|\\u012A|\\u012C|\\u012E|\\u0130|\\u0132|\\u0134|\\u0136|\\u0139|\\u013B|\\u013D\n'
                  '|\\u013F|\\u0141|\\u0143|\\u0145|\\u0147|\\u014A|\\u014C|\\u014E|\\u0150|\\u0152|\\u0154|\\u0156\n'
                  '|\\u0158|\\u015A|\\u015C|\\u015E|\\u0160|\\u0162|\\u0164|\\u0166|\\u0168|\\u016A|\\u016C|\\u016E\n'
                  '|\\u0170|\\u0172|\\u0174|\\u0176|[\\u0178-\\u0179]|\\u017B|\\u017D|[\\u0181-\\u0182]|\\u0184\n'
                  '|[\\u0186-\\u0187]|[\\u0189-\\u018B]|[\\u018E-\\u0191]|[\\u0193-\\u0194]|[\\u0196-\\u0198]\n'
                  '|[\\u019C-\\u019D]|[\\u019F-\\u01A0]|\\u01A2|\\u01A4|\\u01A7|\\u01A9|\\u01AC|[\\u01AE-\\u01AF]\n'
                  '|[\\u01B1-\\u01B3]|\\u01B5|[\\u01B7-\\u01B8]|\\u01BC|[\\u01C4-\\u01C5]|[\\u01C7-\\u01C8]\n'
                  '|[\\u01CA-\\u01CB]|\\u01CD|\\u01CF|\\u01D1|\\u01D3|\\u01D5|\\u01D7|\\u01D9|\\u01DB|\\u01DE|\\u01E0\n'
                  '|\\u01E2|\\u01E4|\\u01E6|\\u01E8|\\u01EA|\\u01EC|\\u01EE|[\\u01F1-\\u01F2]|\\u01F4\n'
                  '|[\\u01F6-\\u01F8]|\\u01FA|\\u01FC|\\u01FE|\\u0200|\\u0202|\\u0204|\\u0206|\\u0208|\\u020A|\\u020C\n'
                  '|\\u020E|\\u0210|\\u0212|\\u0214|\\u0216|\\u0218|\\u021A|\\u021C|\\u021E|\\u0220|\\u0222|\\u0224\n'
                  '|\\u0226|\\u0228|\\u022A|\\u022C|\\u022E|\\u0230|\\u0232|[\\u023A-\\u023B]|[\\u023D-\\u023E]\n'
                  '|\\u0241|[\\u0243-\\u0246]|\\u0248|\\u024A|\\u024C|\\u024E|\\u0262|\\u026A|\\u0274|\\u0276\n'
                  '|[\\u0280-\\u0281]|\\u028F|\\u0299|[\\u029B-\\u029C]|\\u029F|[\\u1D00-\\u1D01]|[\\u1D03-\\u1D07]\n'
                  '|[\\u1D0A-\\u1D10]|\\u1D15|[\\u1D18-\\u1D1C]|[\\u1D20-\\u1D23]|\\u1D7B|\\u1D7E|\\u1DDB\n'
                  '|[\\u1DDE-\\u1DDF]|[\\u1DE1-\\u1DE2]|\\u1E00|\\u1E02|\\u1E04|\\u1E06|\\u1E08|\\u1E0A|\\u1E0C\n'
                  '|\\u1E0E|\\u1E10|\\u1E12|\\u1E14|\\u1E16|\\u1E18|\\u1E1A|\\u1E1C|\\u1E1E|\\u1E20|\\u1E22|\\u1E24\n'
                  '|\\u1E26|\\u1E28|\\u1E2A|\\u1E2C|\\u1E2E|\\u1E30|\\u1E32|\\u1E34|\\u1E36|\\u1E38|\\u1E3A|\\u1E3C\n'
                  '|\\u1E3E|\\u1E40|\\u1E42|\\u1E44|\\u1E46|\\u1E48|\\u1E4A|\\u1E4C|\\u1E4E|\\u1E50|\\u1E52|\\u1E54\n'
                  '|\\u1E56|\\u1E58|\\u1E5A|\\u1E5C|\\u1E5E|\\u1E60|\\u1E62|\\u1E64|\\u1E66|\\u1E68|\\u1E6A|\\u1E6C\n'
                  '|\\u1E6E|\\u1E70|\\u1E72|\\u1E74|\\u1E76|\\u1E78|\\u1E7A|\\u1E7C|\\u1E7E|\\u1E80|\\u1E82|\\u1E84\n'
                  '|\\u1E86|\\u1E88|\\u1E8A|\\u1E8C|\\u1E8E|\\u1E90|\\u1E92|\\u1E94|\\u1E9E|\\u1EA0|\\u1EA2|\\u1EA4\n'
                  '|\\u1EA6|\\u1EA8|\\u1EAA|\\u1EAC|\\u1EAE|\\u1EB0|\\u1EB2|\\u1EB4|\\u1EB6|\\u1EB8|\\u1EBA|\\u1EBC\n'
                  '|\\u1EBE|\\u1EC0|\\u1EC2|\\u1EC4|\\u1EC6|\\u1EC8|\\u1ECA|\\u1ECC|\\u1ECE|\\u1ED0|\\u1ED2|\\u1ED4\n'
                  '|\\u1ED6|\\u1ED8|\\u1EDA|\\u1EDC|\\u1EDE|\\u1EE0|\\u1EE2|\\u1EE4|\\u1EE6|\\u1EE8|\\u1EEA|\\u1EEC\n'
                  '|\\u1EEE|\\u1EF0|\\u1EF2|\\u1EF4|\\u1EF6|\\u1EF8|\\u1EFA|\\u1EFC|\\u1EFE|[\\u24B6-\\u24CF]|\\u2C2E\n'
                  '|\\u2C60|[\\u2C62-\\u2C64]|\\u2C67|\\u2C69|\\u2C6B|[\\u2C6D-\\u2C70]|\\u2C72|\\u2C75|\\u2C7B\n'
                  '|[\\u2C7E-\\u2C7F]|\\uA722|\\uA724|\\uA726|\\uA728|\\uA72A|\\uA72C|\\uA72E|[\\uA730-\\uA732]\n'
                  '|\\uA734|\\uA736|\\uA738|\\uA73A|\\uA73C|\\uA73E|\\uA740|\\uA742|\\uA744|\\uA746|\\uA748|\\uA74A\n'
                  '|\\uA74C|\\uA74E|\\uA750|\\uA752|\\uA754|\\uA756|\\uA758|\\uA75A|\\uA75C|\\uA75E|\\uA760|\\uA762\n'
                  '|\\uA764|\\uA766|\\uA768|\\uA76A|\\uA76C|\\uA76E|\\uA776|\\uA779|\\uA77B|[\\uA77D-\\uA77E]|\\uA780\n'
                  '|\\uA782|\\uA784|\\uA786|\\uA78B|\\uA78D|\\uA790|\\uA792|\\uA796|\\uA798|\\uA79A|\\uA79C|\\uA79E\n'
                  '|\\uA7A0|\\uA7A2|\\uA7A4|\\uA7A6|\\uA7A8|[\\uA7AA-\\uA7B4]|\\uA7B6|\\uA7B8|\\uA7BA|\\uA7BC|\\uA7BE\n'
                  '|\\uA7C2|[\\uA7C4-\\uA7C6]|\\uA7FA|\\uAB46|[\\uFF21-\\uFF3A]|[\\U0001F110-\\U0001F12C]\n'
                  '|[\\U0001F130-\\U0001F149]|[\\U0001F150-\\U0001F169]|[\\U0001F170-\\U0001F18A]|\\U0001F520\n'
                  '|[\\U000E0041-\\U000E005A])+')
    _LATEIN_GROSS = RegExp('(?x)[\\x41-\\x5A]|[\\xC0-\\xD6]|[\\xD8-\\xDE]|\\u0100|\\u0102|\\u0104|\\u0106|\\u0108|\\u010A|\\u010C\n'
                  '|\\u010E|\\u0110|\\u0112|\\u0114|\\u0116|\\u0118|\\u011A|\\u011C|\\u011E|\\u0120|\\u0122|\\u0124\n'
                  '|\\u0126|\\u0128|\\u012A|\\u012C|\\u012E|\\u0130|\\u0132|\\u0134|\\u0136|\\u0139|\\u013B|\\u013D\n'
                  '|\\u013F|\\u0141|\\u0143|\\u0145|\\u0147|\\u014A|\\u014C|\\u014E|\\u0150|\\u0152|\\u0154|\\u0156\n'
                  '|\\u0158|\\u015A|\\u015C|\\u015E|\\u0160|\\u0162|\\u0164|\\u0166|\\u0168|\\u016A|\\u016C|\\u016E\n'
                  '|\\u0170|\\u0172|\\u0174|\\u0176|[\\u0178-\\u0179]|\\u017B|\\u017D|[\\u0181-\\u0182]|\\u0184\n'
                  '|[\\u0186-\\u0187]|[\\u0189-\\u018B]|[\\u018E-\\u0191]|[\\u0193-\\u0194]|[\\u0196-\\u0198]\n'
                  '|[\\u019C-\\u019D]|[\\u019F-\\u01A0]|\\u01A2|\\u01A4|\\u01A7|\\u01A9|\\u01AC|[\\u01AE-\\u01AF]\n'
                  '|[\\u01B1-\\u01B3]|\\u01B5|[\\u01B7-\\u01B8]|\\u01BC|[\\u01C4-\\u01C5]|[\\u01C7-\\u01C8]\n'
                  '|[\\u01CA-\\u01CB]|\\u01CD|\\u01CF|\\u01D1|\\u01D3|\\u01D5|\\u01D7|\\u01D9|\\u01DB|\\u01DE|\\u01E0\n'
                  '|\\u01E2|\\u01E4|\\u01E6|\\u01E8|\\u01EA|\\u01EC|\\u01EE|[\\u01F1-\\u01F2]|\\u01F4\n'
                  '|[\\u01F6-\\u01F8]|\\u01FA|\\u01FC|\\u01FE|\\u0200|\\u0202|\\u0204|\\u0206|\\u0208|\\u020A|\\u020C\n'
                  '|\\u020E|\\u0210|\\u0212|\\u0214|\\u0216|\\u0218|\\u021A|\\u021C|\\u021E|\\u0220|\\u0222|\\u0224\n'
                  '|\\u0226|\\u0228|\\u022A|\\u022C|\\u022E|\\u0230|\\u0232|[\\u023A-\\u023B]|[\\u023D-\\u023E]\n'
                  '|\\u0241|[\\u0243-\\u0246]|\\u0248|\\u024A|\\u024C|\\u024E|\\u0262|\\u026A|\\u0274|\\u0276\n'
                  '|[\\u0280-\\u0281]|\\u028F|\\u0299|[\\u029B-\\u029C]|\\u029F|[\\u1D00-\\u1D01]|[\\u1D03-\\u1D07]\n'
                  '|[\\u1D0A-\\u1D10]|\\u1D15|[\\u1D18-\\u1D1C]|[\\u1D20-\\u1D23]|\\u1D7B|\\u1D7E|\\u1DDB\n'
                  '|[\\u1DDE-\\u1DDF]|[\\u1DE1-\\u1DE2]|\\u1E00|\\u1E02|\\u1E04|\\u1E06|\\u1E08|\\u1E0A|\\u1E0C\n'
                  '|\\u1E0E|\\u1E10|\\u1E12|\\u1E14|\\u1E16|\\u1E18|\\u1E1A|\\u1E1C|\\u1E1E|\\u1E20|\\u1E22|\\u1E24\n'
                  '|\\u1E26|\\u1E28|\\u1E2A|\\u1E2C|\\u1E2E|\\u1E30|\\u1E32|\\u1E34|\\u1E36|\\u1E38|\\u1E3A|\\u1E3C\n'
                  '|\\u1E3E|\\u1E40|\\u1E42|\\u1E44|\\u1E46|\\u1E48|\\u1E4A|\\u1E4C|\\u1E4E|\\u1E50|\\u1E52|\\u1E54\n'
                  '|\\u1E56|\\u1E58|\\u1E5A|\\u1E5C|\\u1E5E|\\u1E60|\\u1E62|\\u1E64|\\u1E66|\\u1E68|\\u1E6A|\\u1E6C\n'
                  '|\\u1E6E|\\u1E70|\\u1E72|\\u1E74|\\u1E76|\\u1E78|\\u1E7A|\\u1E7C|\\u1E7E|\\u1E80|\\u1E82|\\u1E84\n'
                  '|\\u1E86|\\u1E88|\\u1E8A|\\u1E8C|\\u1E8E|\\u1E90|\\u1E92|\\u1E94|\\u1E9E|\\u1EA0|\\u1EA2|\\u1EA4\n'
                  '|\\u1EA6|\\u1EA8|\\u1EAA|\\u1EAC|\\u1EAE|\\u1EB0|\\u1EB2|\\u1EB4|\\u1EB6|\\u1EB8|\\u1EBA|\\u1EBC\n'
                  '|\\u1EBE|\\u1EC0|\\u1EC2|\\u1EC4|\\u1EC6|\\u1EC8|\\u1ECA|\\u1ECC|\\u1ECE|\\u1ED0|\\u1ED2|\\u1ED4\n'
                  '|\\u1ED6|\\u1ED8|\\u1EDA|\\u1EDC|\\u1EDE|\\u1EE0|\\u1EE2|\\u1EE4|\\u1EE6|\\u1EE8|\\u1EEA|\\u1EEC\n'
                  '|\\u1EEE|\\u1EF0|\\u1EF2|\\u1EF4|\\u1EF6|\\u1EF8|\\u1EFA|\\u1EFC|\\u1EFE|[\\u24B6-\\u24CF]|\\u2C2E\n'
                  '|\\u2C60|[\\u2C62-\\u2C64]|\\u2C67|\\u2C69|\\u2C6B|[\\u2C6D-\\u2C70]|\\u2C72|\\u2C75|\\u2C7B\n'
                  '|[\\u2C7E-\\u2C7F]|\\uA722|\\uA724|\\uA726|\\uA728|\\uA72A|\\uA72C|\\uA72E|[\\uA730-\\uA732]\n'
                  '|\\uA734|\\uA736|\\uA738|\\uA73A|\\uA73C|\\uA73E|\\uA740|\\uA742|\\uA744|\\uA746|\\uA748|\\uA74A\n'
                  '|\\uA74C|\\uA74E|\\uA750|\\uA752|\\uA754|\\uA756|\\uA758|\\uA75A|\\uA75C|\\uA75E|\\uA760|\\uA762\n'
                  '|\\uA764|\\uA766|\\uA768|\\uA76A|\\uA76C|\\uA76E|\\uA776|\\uA779|\\uA77B|[\\uA77D-\\uA77E]|\\uA780\n'
                  '|\\uA782|\\uA784|\\uA786|\\uA78B|\\uA78D|\\uA790|\\uA792|\\uA796|\\uA798|\\uA79A|\\uA79C|\\uA79E\n'
                  '|\\uA7A0|\\uA7A2|\\uA7A4|\\uA7A6|\\uA7A8|[\\uA7AA-\\uA7B4]|\\uA7B6|\\uA7B8|\\uA7BA|\\uA7BC|\\uA7BE\n'
                  '|\\uA7C2|[\\uA7C4-\\uA7C6]|\\uA7FA|\\uAB46|[\\uFF21-\\uFF3A]|[\\U0001F110-\\U0001F12C]\n'
                  '|[\\U0001F130-\\U0001F149]|[\\U0001F150-\\U0001F169]|[\\U0001F170-\\U0001F18A]|\\U0001F520\n'
                  '|[\\U000E0041-\\U000E005A]')
    _LATEIN_KLEIN_FOLGE = RegExp('(?x)(?:[\\x61-\\x7A]|[\\xDF-\\xF6]|[\\xF8-\\xFF]|\\u0101|\\u0103|\\u0105|\\u0107|\\u0109|\\u010B|\\u010D\n'
                   '|\\u010F|\\u0111|\\u0113|\\u0115|\\u0117|\\u0119|\\u011B|\\u011D|\\u011F|\\u0121|\\u0123|\\u0125\n'
                   '|\\u0127|\\u0129|\\u012B|\\u012D|\\u012F|\\u0131|\\u0133|\\u0135|[\\u0137-\\u0138]|\\u013A|\\u013C\n'
                   '|\\u013E|\\u0140|\\u0142|\\u0144|\\u0146|[\\u0148-\\u0149]|\\u014B|\\u014D|\\u014F|\\u0151|\\u0153\n'
                   '|\\u0155|\\u0157|\\u0159|\\u015B|\\u015D|\\u015F|\\u0161|\\u0163|\\u0165|\\u0167|\\u0169|\\u016B\n'
                   '|\\u016D|\\u016F|\\u0171|\\u0173|\\u0175|\\u0177|\\u017A|\\u017C|[\\u017E-\\u0180]|\\u0183|\\u0185\n'
                   '|\\u0188|[\\u018C-\\u018D]|\\u0192|\\u0195|[\\u0199-\\u019B]|\\u019E|\\u01A1|\\u01A3|\\u01A5\n'
                   '|\\u01A8|\\u01AB|\\u01AD|\\u01B0|\\u01B4|\\u01B6|[\\u01B9-\\u01BA]|\\u01BD|[\\u01C5-\\u01C6]\n'
                   '|[\\u01C8-\\u01C9]|[\\u01CB-\\u01CC]|\\u01CE|\\u01D0|\\u01D2|\\u01D4|\\u01D6|\\u01D8|\\u01DA\n'
                   '|[\\u01DC-\\u01DD]|\\u01DF|\\u01E1|\\u01E3|\\u01E5|\\u01E7|\\u01E9|\\u01EB|\\u01ED\n'
                   '|[\\u01EF-\\u01F0]|[\\u01F2-\\u01F3]|\\u01F5|\\u01F9|\\u01FB|\\u01FD|\\u01FF|\\u0201|\\u0203\n'
                   '|\\u0205|\\u0207|\\u0209|\\u020B|\\u020D|\\u020F|\\u0211|\\u0213|\\u0215|\\u0217|\\u0219|\\u021B\n'
                   '|\\u021D|\\u021F|\\u0221|\\u0223|\\u0225|\\u0227|\\u0229|\\u022B|\\u022D|\\u022F|\\u0231\n'
                   '|[\\u0233-\\u0239]|\\u023C|[\\u023F-\\u0240]|\\u0242|\\u0247|[\\u0249-\\u024B]|\\u024D\n'
                   '|[\\u024F-\\u0293]|[\\u0299-\\u02A0]|[\\u02A3-\\u02AB]|[\\u02AE-\\u02AF]|[\\u0300-\\u0362]|[\\u0363-\\u036F]\n'
                   '|[\\u1D00-\\u1D23]|[\\u1D62-\\u1D65]|[\\u1D6B-\\u1D77]|[\\u1D79-\\u1D9A]|\\u1DCA\n'
                   '|[\\u1DD3-\\u1DF4]|\\u1E01|\\u1E03|\\u1E05|\\u1E07|\\u1E09|\\u1E0B|\\u1E0D|\\u1E0F|\\u1E11|\\u1E13\n'
                   '|\\u1E15|\\u1E17|\\u1E19|\\u1E1B|\\u1E1D|\\u1E1F|\\u1E21|\\u1E23|\\u1E25|\\u1E27|\\u1E29|\\u1E2B\n'
                   '|\\u1E2D|\\u1E2F|\\u1E31|\\u1E33|\\u1E35|\\u1E37|\\u1E39|\\u1E3B|\\u1E3D|\\u1E3F|\\u1E41|\\u1E43\n'
                   '|\\u1E45|\\u1E47|\\u1E49|\\u1E4B|\\u1E4D|\\u1E4F|\\u1E51|\\u1E53|\\u1E55|\\u1E57|\\u1E59|\\u1E5B\n'
                   '|\\u1E5D|\\u1E5F|\\u1E61|\\u1E63|\\u1E65|\\u1E67|\\u1E69|\\u1E6B|\\u1E6D|\\u1E6F|\\u1E71|\\u1E73\n'
                   '|\\u1E75|\\u1E77|\\u1E79|\\u1E7B|\\u1E7D|\\u1E7F|\\u1E81|\\u1E83|\\u1E85|\\u1E87|\\u1E89|\\u1E8B\n'
                   '|\\u1E8D|\\u1E8F|\\u1E91|\\u1E93|[\\u1E95-\\u1E9D]|\\u1E9F|\\u1EA1|\\u1EA3|\\u1EA5|\\u1EA7|\\u1EA9\n'
                   '|\\u1EAB|\\u1EAD|\\u1EAF|\\u1EB1|\\u1EB3|\\u1EB5|\\u1EB7|\\u1EB9|\\u1EBB|\\u1EBD|\\u1EBF|\\u1EC1\n'
                   '|\\u1EC3|\\u1EC5|\\u1EC7|\\u1EC9|\\u1ECB|\\u1ECD|\\u1ECF|\\u1ED1|\\u1ED3|\\u1ED5|\\u1ED7|\\u1ED9\n'
                   '|\\u1EDB|\\u1EDD|\\u1EDF|\\u1EE1|\\u1EE3|\\u1EE5|\\u1EE7|\\u1EE9|\\u1EEB|\\u1EED|\\u1EEF|\\u1EF1\n'
                   '|\\u1EF3|\\u1EF5|\\u1EF7|\\u1EF9|\\u1EFB|\\u1EFD|\\u1EFF|\\u2071|\\u207F|[\\u2090-\\u209C]|\\u2184\n'
                   '|[\\u249C-\\u24B5]|[\\u24D0-\\u24E9]|\\u2C5E|\\u2C61|[\\u2C65-\\u2C66]|\\u2C68|\\u2C6A|\\u2C6C\n'
                   '|\\u2C71|[\\u2C73-\\u2C74]|[\\u2C76-\\u2C7C]|\\uA723|\\uA725|\\uA727|\\uA729|\\uA72B|\\uA72D\n'
                   '|[\\uA72F-\\uA731]|\\uA733|\\uA735|\\uA737|\\uA739|\\uA73B|\\uA73D|\\uA73F|\\uA741|\\uA743|\\uA745\n'
                   '|\\uA747|\\uA749|\\uA74B|\\uA74D|\\uA74F|\\uA751|\\uA753|\\uA755|\\uA757|\\uA759|\\uA75B|\\uA75D\n'
                   '|\\uA75F|\\uA761|\\uA763|\\uA765|\\uA767|\\uA769|\\uA76B|\\uA76D|\\uA76F|[\\uA771-\\uA778]|\\uA77A\n'
                   '|\\uA77C|\\uA77F|\\uA781|\\uA783|\\uA785|\\uA787|\\uA78C|\\uA78E|\\uA791|[\\uA793-\\uA795]|\\uA797\n'
                   '|\\uA799|\\uA79B|\\uA79D|\\uA79F|\\uA7A1|\\uA7A3|\\uA7A5|\\uA7A7|\\uA7A9|[\\uA7AE-\\uA7AF]|\\uA7B5\n'
                   '|\\uA7B7|\\uA7B9|\\uA7BB|\\uA7BD|\\uA7BF|\\uA7C3|\\uA7FA|[\\uAB30-\\uAB5A]|[\\uAB60-\\uAB64]\n'
                   '|[\\uAB66-\\uAB67]|[\\uFB00-\\uFB06]|[\\uFF41-\\uFF5A]|\\U0001F1A5|\\U0001F521\n'
                   '|[\\U000E0061-\\U000E007A])+')
    _LATEIN_KLEIN = RegExp('(?x)[\\x61-\\x7A]|[\\xDF-\\xF6]|[\\xF8-\\xFF]|\\u0101|\\u0103|\\u0105|\\u0107|\\u0109|\\u010B|\\u010D\n'
                   '|\\u010F|\\u0111|\\u0113|\\u0115|\\u0117|\\u0119|\\u011B|\\u011D|\\u011F|\\u0121|\\u0123|\\u0125\n'
                   '|\\u0127|\\u0129|\\u012B|\\u012D|\\u012F|\\u0131|\\u0133|\\u0135|[\\u0137-\\u0138]|\\u013A|\\u013C\n'
                   '|\\u013E|\\u0140|\\u0142|\\u0144|\\u0146|[\\u0148-\\u0149]|\\u014B|\\u014D|\\u014F|\\u0151|\\u0153\n'
                   '|\\u0155|\\u0157|\\u0159|\\u015B|\\u015D|\\u015F|\\u0161|\\u0163|\\u0165|\\u0167|\\u0169|\\u016B\n'
                   '|\\u016D|\\u016F|\\u0171|\\u0173|\\u0175|\\u0177|\\u017A|\\u017C|[\\u017E-\\u0180]|\\u0183|\\u0185\n'
                   '|\\u0188|[\\u018C-\\u018D]|\\u0192|\\u0195|[\\u0199-\\u019B]|\\u019E|\\u01A1|\\u01A3|\\u01A5\n'
                   '|\\u01A8|\\u01AB|\\u01AD|\\u01B0|\\u01B4|\\u01B6|[\\u01B9-\\u01BA]|\\u01BD|[\\u01C5-\\u01C6]\n'
                   '|[\\u01C8-\\u01C9]|[\\u01CB-\\u01CC]|\\u01CE|\\u01D0|\\u01D2|\\u01D4|\\u01D6|\\u01D8|\\u01DA\n'
                   '|[\\u01DC-\\u01DD]|\\u01DF|\\u01E1|\\u01E3|\\u01E5|\\u01E7|\\u01E9|\\u01EB|\\u01ED\n'
                   '|[\\u01EF-\\u01F0]|[\\u01F2-\\u01F3]|\\u01F5|\\u01F9|\\u01FB|\\u01FD|\\u01FF|\\u0201|\\u0203\n'
                   '|\\u0205|\\u0207|\\u0209|\\u020B|\\u020D|\\u020F|\\u0211|\\u0213|\\u0215|\\u0217|\\u0219|\\u021B\n'
                   '|\\u021D|\\u021F|\\u0221|\\u0223|\\u0225|\\u0227|\\u0229|\\u022B|\\u022D|\\u022F|\\u0231\n'
                   '|[\\u0233-\\u0239]|\\u023C|[\\u023F-\\u0240]|\\u0242|\\u0247|[\\u0249-\\u024B]|\\u024D\n'
                   '|[\\u024F-\\u0293]|[\\u0299-\\u02A0]|[\\u02A3-\\u02AB]|[\\u02AE-\\u02AF]|[\\u0363-\\u036F]\n'
                   '|[\\u1D00-\\u1D23]|[\\u1D62-\\u1D65]|[\\u1D6B-\\u1D77]|[\\u1D79-\\u1D9A]|\\u1DCA\n'
                   '|[\\u1DD3-\\u1DF4]|\\u1E01|\\u1E03|\\u1E05|\\u1E07|\\u1E09|\\u1E0B|\\u1E0D|\\u1E0F|\\u1E11|\\u1E13\n'
                   '|\\u1E15|\\u1E17|\\u1E19|\\u1E1B|\\u1E1D|\\u1E1F|\\u1E21|\\u1E23|\\u1E25|\\u1E27|\\u1E29|\\u1E2B\n'
                   '|\\u1E2D|\\u1E2F|\\u1E31|\\u1E33|\\u1E35|\\u1E37|\\u1E39|\\u1E3B|\\u1E3D|\\u1E3F|\\u1E41|\\u1E43\n'
                   '|\\u1E45|\\u1E47|\\u1E49|\\u1E4B|\\u1E4D|\\u1E4F|\\u1E51|\\u1E53|\\u1E55|\\u1E57|\\u1E59|\\u1E5B\n'
                   '|\\u1E5D|\\u1E5F|\\u1E61|\\u1E63|\\u1E65|\\u1E67|\\u1E69|\\u1E6B|\\u1E6D|\\u1E6F|\\u1E71|\\u1E73\n'
                   '|\\u1E75|\\u1E77|\\u1E79|\\u1E7B|\\u1E7D|\\u1E7F|\\u1E81|\\u1E83|\\u1E85|\\u1E87|\\u1E89|\\u1E8B\n'
                   '|\\u1E8D|\\u1E8F|\\u1E91|\\u1E93|[\\u1E95-\\u1E9D]|\\u1E9F|\\u1EA1|\\u1EA3|\\u1EA5|\\u1EA7|\\u1EA9\n'
                   '|\\u1EAB|\\u1EAD|\\u1EAF|\\u1EB1|\\u1EB3|\\u1EB5|\\u1EB7|\\u1EB9|\\u1EBB|\\u1EBD|\\u1EBF|\\u1EC1\n'
                   '|\\u1EC3|\\u1EC5|\\u1EC7|\\u1EC9|\\u1ECB|\\u1ECD|\\u1ECF|\\u1ED1|\\u1ED3|\\u1ED5|\\u1ED7|\\u1ED9\n'
                   '|\\u1EDB|\\u1EDD|\\u1EDF|\\u1EE1|\\u1EE3|\\u1EE5|\\u1EE7|\\u1EE9|\\u1EEB|\\u1EED|\\u1EEF|\\u1EF1\n'
                   '|\\u1EF3|\\u1EF5|\\u1EF7|\\u1EF9|\\u1EFB|\\u1EFD|\\u1EFF|\\u2071|\\u207F|[\\u2090-\\u209C]|\\u2184\n'
                   '|[\\u249C-\\u24B5]|[\\u24D0-\\u24E9]|\\u2C5E|\\u2C61|[\\u2C65-\\u2C66]|\\u2C68|\\u2C6A|\\u2C6C\n'
                   '|\\u2C71|[\\u2C73-\\u2C74]|[\\u2C76-\\u2C7C]|\\uA723|\\uA725|\\uA727|\\uA729|\\uA72B|\\uA72D\n'
                   '|[\\uA72F-\\uA731]|\\uA733|\\uA735|\\uA737|\\uA739|\\uA73B|\\uA73D|\\uA73F|\\uA741|\\uA743|\\uA745\n'
                   '|\\uA747|\\uA749|\\uA74B|\\uA74D|\\uA74F|\\uA751|\\uA753|\\uA755|\\uA757|\\uA759|\\uA75B|\\uA75D\n'
                   '|\\uA75F|\\uA761|\\uA763|\\uA765|\\uA767|\\uA769|\\uA76B|\\uA76D|\\uA76F|[\\uA771-\\uA778]|\\uA77A\n'
                   '|\\uA77C|\\uA77F|\\uA781|\\uA783|\\uA785|\\uA787|\\uA78C|\\uA78E|\\uA791|[\\uA793-\\uA795]|\\uA797\n'
                   '|\\uA799|\\uA79B|\\uA79D|\\uA79F|\\uA7A1|\\uA7A3|\\uA7A5|\\uA7A7|\\uA7A9|[\\uA7AE-\\uA7AF]|\\uA7B5\n'
                   '|\\uA7B7|\\uA7B9|\\uA7BB|\\uA7BD|\\uA7BF|\\uA7C3|\\uA7FA|[\\uAB30-\\uAB5A]|[\\uAB60-\\uAB64]\n'
                   '|[\\uAB66-\\uAB67]|[\\uFB00-\\uFB06]|[\\uFF41-\\uFF5A]|\\U0001F1A5|\\U0001F521\n'
                   '|[\\U000E0061-\\U000E007A]')
    _LATEIN_FOLGE = RegExp('(?x)(?:[\\x41-\\x5A]|[\\x61-\\x7A]|[\\xC0-\\xD6]|[\\xD8-\\xF6]|[\\u00F8-\\u02AF]|[\\u0363-\\u036F]\n'
                   '|[\\u1D00-\\u1D25]|[\\u1D62-\\u1D65]|[\\u1D6B-\\u1D77]|[\\u1D79-\\u1D9A]|\\u1DCA\n'
                   '|[\\u1DD3-\\u1DF4]|[\\u1E00-\\u1EFF]|\\u2071|\\u207F|[\\u2090-\\u209C]|\\u2184|[\\u249C-\\u24E9]\n'
                   '|[\\u271D-\\u271F]|\\u2C2E|\\u2C5E|[\\u2C60-\\u2C7C]|[\\u2C7E-\\u2C7F]|[\\uA722-\\uA76F]\n'
                   '|[\\uA771-\\uA787]|[\\uA78B-\\uA7BF]|[\\uA7C2-\\uA7C6]|\\uA7F7|[\\uA7FA-\\uA7FF]\n'
                   '|[\\uAB30-\\uAB5A]|[\\uAB60-\\uAB64]|[\\uAB66-\\uAB67]|[\\uFB00-\\uFB06]|[\\uFF21-\\uFF3A]\n'
                   '|[\\uFF41-\\uFF5A]|[\\U0001F110-\\U0001F12C]|[\\U0001F130-\\U0001F149]\n'
                   '|[\\U0001F150-\\U0001F169]|[\\U0001F170-\\U0001F18A]|\\U0001F1A5|[\\U0001F520-\\U0001F521]\n'
                   '|\\U0001F524|[\\U0001F546-\\U0001F547]|[\\U000E0041-\\U000E005A]|[\\U000E0061-\\U000E007A])+')
    _LATEIN = RegExp('(?x)[\\x41-\\x5A]|[\\x61-\\x7A]|[\\xC0-\\xD6]|[\\xD8-\\xF6]|[\\u00F8-\\u02AF]|[\\u0363-\\u036F]\n'
                   '|[\\u1D00-\\u1D25]|[\\u1D62-\\u1D65]|[\\u1D6B-\\u1D77]|[\\u1D79-\\u1D9A]|\\u1DCA\n'
                   '|[\\u1DD3-\\u1DF4]|[\\u1E00-\\u1EFF]|\\u2071|\\u207F|[\\u2090-\\u209C]|\\u2184|[\\u249C-\\u24E9]\n'
                   '|[\\u271D-\\u271F]|\\u2C2E|\\u2C5E|[\\u2C60-\\u2C7C]|[\\u2C7E-\\u2C7F]|[\\uA722-\\uA76F]\n'
                   '|[\\uA771-\\uA787]|[\\uA78B-\\uA7BF]|[\\uA7C2-\\uA7C6]|\\uA7F7|[\\uA7FA-\\uA7FF]\n'
                   '|[\\uAB30-\\uAB5A]|[\\uAB60-\\uAB64]|[\\uAB66-\\uAB67]|[\\uFB00-\\uFB06]|[\\uFF21-\\uFF3A]\n'
                   '|[\\uFF41-\\uFF5A]|[\\U0001F110-\\U0001F12C]|[\\U0001F130-\\U0001F149]\n'
                   '|[\\U0001F150-\\U0001F169]|[\\U0001F170-\\U0001F18A]|\\U0001F1A5|[\\U0001F520-\\U0001F521]\n'
                   '|\\U0001F524|[\\U0001F546-\\U0001F547]|[\\U000E0041-\\U000E005A]|[\\U000E0061-\\U000E007A]')
    _DATEI_ENDE = SmartRE(f'(?!.)', '!/./')
    ROEM_TAUSENDER = RegExp('(?=[MDCLXVI][\\u0305])(?:M̅)*((?:C̅)+(?:M̅|D̅)|(?:D̅)?(?:C̅)*)((?:X̅)+(?:C̅|L̅)'
       '|(?:L̅)?(?:X̅)*)((?:I̅)+(?:X̅|V̅)|(?:V̅)?(?:I̅)*)')
    VARIANTEN_ABSCHLUSS = SmartRE(f'(?P<:Whitespace>{WSP_RE__})(:)', '~ /:/')
    L = Synonym(wsp__)
    KATEGORIENZEILE = Series(wsp__, Option(Series(Text("|"), wsp__)), ZeroOrMore(Alternative(SmartRE(f'([^":\\n/{{]+)(?P<:Whitespace>{WSP_RE__})', '/[^":\\n\\/{]+/ ~'), Zusatz)), wsp__, RegExp(':'), wsp__, RegExp('\\n'), SmartRE(f'(?!(?:{WSP_RE__})(?:\\*))', '!~ `*`'))
    KOMMENTARZEILEN = ZeroOrMore(Series(RegExp('[ \\t]*\\n?[ \\t]*'), comment__))
    ZEILENSPRUNG = SmartRE(f'(?P<:Whitespace>{WSP_RE__})(\\n)(?P<:Whitespace>{WSP_RE__})', '~ /\\n/ ~')
    LEERZEILEN = SmartRE(f'([ \\t]*(?:\\n[ \\t]*)+\\n)(?P<:Whitespace>{WSP_RE__})(\\n?)', '/[ \\t]*(?:\\n[ \\t]*)+\\n/ ~ /\\n?/')
    ZWW = Series(ZEILENSPRUNG, Option(LZ))
    LÜCKE = Series(KOMMENTARZEILEN, LEERZEILEN, Option(LZ))
    Direktiven = Series(ZWW, Unterdrücke_Warnungen)
    ZW = Series(NegativeLookahead(LÜCKE), ZEILENSPRUNG)
    SEMIKOLON = Series(Text(";"), wsp__)
    KOMMA = Series(Text(","), wsp__)
    DPP = SmartRE(f'(::?)(?P<:Whitespace>{WSP_RE__})', '/::?/ ~')
    ZLL = Synonym(ZWW)
    ABS = Alternative(Series(RegExp('\\s*'), Alternative(Series(RegExp(';;?'), LZ), RegExp('$'))), ZWW)
    LABS = Synonym(ABS)
    LL = Synonym(LZ)
    ADDE_AD = Alternative(Series(Text("ADDE"), L, Option(LZ), SmartRE(f'(?P<:Text>AD|ad)', '`AD`|`ad`'), L), Series(Series(Text("{adde"), wsp__), Series(Text("ad}"), wsp__)))
    MEHRZEILER = OneOrMore(Alternative(FREITEXT, RegExp('[ \\t+]*[\\n]?[ \\t]*(?=[\\w,;:.\\-])')))
    ECKIGE_ZU = SmartRE(f'(\\](?!\\]))(?P<:Whitespace>{WSP_RE__})', '/\\](?!\\])/ ~')
    ECKIGE_AUF = SmartRE(f'(\\[(?!\\[))(?P<:Whitespace>{WSP_RE__})', '/\\[(?!\\[)/ ~')
    KLAMMER_ZU = SmartRE(f'(\\)(?!\\)))(?P<:Whitespace>{WSP_RE__})', '/\\)(?!\\))/ ~')
    KLAMMER_AUF = SmartRE(f'(\\((?!\\())(?P<:Whitespace>{WSP_RE__})', '/\\((?!\\()/ ~')
    ZITAT_ENDE = SmartRE(f"(['´’])(?!\\w)(?P<:Whitespace>{WSP_RE__})", "/['´’]/ !/\\w/ ~")
    ZITAT_ANFANG = Series(NegativeLookbehind(RegExp('\\w')), RegExp("['‘`]"))
    tmesi = Series(Text("##"), wsp__)
    TAG_NAME = Capture(RegExp('\\w+'), zero_length_warning=True)
    XML = Series(Text("<"), TAG_NAME, Text(">"), wsp__, FREITEXT, Text("</"), Pop(TAG_NAME), Text(">"), mandatory=5)
    GRI_VERBOTEN = RegExp('[èéêëìíîïòóôõöùúûüü]')
    DEU_GWORT = Series(_LATEIN, ZeroOrMore(Alternative(_LATEIN_KLEIN_FOLGE, RegExp('-(?!-)'))), RegExp('\\.?'), NegativeLookahead(_LATEIN), NegativeLookahead(Text("|")), wsp__)
    GRÖßER_ZEICHEN = SmartRE(f'(>(?!>))(?P<:Whitespace>{WSP_RE__})', '/>(?!>)/ ~')
    TEIL_SATZZEICHEN = SmartRE(f"((?!->)(?:(?:,(?!,))|(?:-(?!-))|[.]+)|[`''‘’/?!])(?P<:Whitespace>{WSP_RE__})", "/(?!->)(?:(?:,(?!,))|(?:-(?!-))|[.]+)|[`''‘’\\/?!]/ ~")
    ROEM_NORMAL = SmartRE(f'((?!D[.|])(?![M][^\\w])(?=[MDCLXVI])M*(?:C+[MD]|D?C*)(?:X+[CL]|L?X*)(?:I+[XV]|V?I*)(?=1|[^\\w]|_)\\.?)(?P<:Whitespace>{WSP_RE__})', '/(?!D[.|])(?![M][^\\w])(?=[MDCLXVI])M*(?:C+[MD]|D?C*)(?:X+[CL]|L?X*)(?:I+[XV]|V?I*)(?=1|[^\\w]|_)\\.?/ ~')
    DATIERUNG_FEHLER = RegExp('vs\\.|[a-z]\\.[\\w\\d]')
    KLEINER_ZEICHEN = SmartRE(f'(<(?!<|/))(?P<:Whitespace>{WSP_RE__})', '/<(?!<|\\/)/ ~')
    ROEM_GROSSBUCHSTABEN = Series(Series(Text("$"), wsp__), ROEM_NORMAL)
    EINSCHUB_ENDE = SmartRE(f'(\\)\\))(?P<:Whitespace>{WSP_RE__})', '/\\)\\)/ ~')
    EINSCHUB_ANFANG = SmartRE(f'(\\(\\()(?P<:Whitespace>{WSP_RE__})', '/\\(\\(/ ~')
    LAT_GWORT = Series(_LATEIN, ZeroOrMore(Alternative(_LATEIN_KLEIN_FOLGE, RegExp('[_\\-](?![_\\-.|])'), Series(RegExp('(?:\\|\\.?)|(?:\\.\\|)'), _LATEIN_KLEIN_FOLGE), Series(Text("("), _LATEIN_KLEIN_FOLGE, Text(")")), Series(Text("["), _LATEIN_KLEIN_FOLGE, Text("]")), KLEINER_ZEICHEN, GRÖßER_ZEICHEN)), NegativeLookahead(_LATEIN), wsp__)
    DEU_KLEIN = Series(_LATEIN_KLEIN_FOLGE, ZeroOrMore(Alternative(_LATEIN_KLEIN_FOLGE, KLEINER_ZEICHEN, GRÖßER_ZEICHEN, RegExp('-(?!-)'))), RegExp('\\.?'), NegativeLookahead(_LATEIN), NegativeLookahead(Text("|")), wsp__)
    AUSGEWICHEN = SmartRE(f'(\\\\[#*{{}}\\[\\]()\\\\<>$])(?P<:Whitespace>{WSP_RE__})', '/\\\\[#*{}\\[\\]()\\\\<>$]/ ~')
    homothetisch = Series(Text("-:-"), wsp__)
    SPEZIAL = Synonym(homothetisch)
    UNICODE = RegExp('\\\\u\\d+')
    SONDERBUCHSTABE = RegExp('[ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎď'
       'ĐđĒ,ēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļ,ĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝ'
       'ŞşŠšŢţŤťŦŧŨũŪūŬ,ŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜ,ƝƞƟƠơƢƣƤƥƦƧƨƩƪƫ'
       'ƬƭƮƯưƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏ,ǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺ'
       'ǻǼǽǾǿ]')
    SONDERZEICHEN = RegExp('[\\u2014§°∻𐆒₰℈℥ℨ∋∻⊢⊣♮﹂𝔣ꝑϽ =]')
    DEU_GROSS = Series(Alternative(_LATEIN_GROSS, Text("˜")), ZeroOrMore(Alternative(_LATEIN_KLEIN_FOLGE, RegExp('-(?!-)'))), RegExp('\\.?'), NegativeLookahead(_LATEIN), NegativeLookahead(Text("|")), wsp__)
    GRI_WORT = Series(Option(GRI_VERBOTEN), OneOrMore(Alternative(Series(RegExp('(?x)(?!--)[\\-.\\[\\]]*\n'
                          '(?:(?:[\\u0300-\\u0362]|[\\u0370-\\u0377]|[\\u037A-\\u037F]|[\\u0384-\\u038A]|\\u038C|\n'
                          '[\\u038E-\\u03A1]|[\\u03A3-\\u03E1]|[\\u03F0-\\u03FF]|[\\u1D26-\\u1D2A]|\n'
                          '[\\u1D66-\\u1D6A]|[\\u1F00-\\u1F15]|[\\u1F18-\\u1F1D]|[\\u1F20-\\u1F45]|\n'
                          '[\\u1F48-\\u1F4D]|[\\u1F50-\\u1F57]|\\u1F59|\\u1F5B|\\u1F5D|[\\u1F5F-\\u1F7D]|\n'
                          '[\\u1F80-\\u1FB4]|[\\u1FB6-\\u1FC4]|[\\u1FC6-\\u1FD3]|[\\u1FD6-\\u1FDB]|\n'
                          '[\\u1FDD-\\u1FEF]|[\\u1FF2-\\u1FF4]|[\\u1FF6-\\u1FFE]|\\uAB65|\n'
                          '[\\U00010140-\\U0001018D]|\\U000101A0|[\\U0001D200-\\U0001D241]|\n'
                          '\\U0001D245)[\\-.\\[\\]]*)+(?<!--)'), Option(GRI_VERBOTEN)), KLEINER_ZEICHEN, GRÖßER_ZEICHEN)), wsp__)
    ZITAT_SATZZEICHEN = SmartRE(f'((?!->)(?:(?:,(?!,))|(?:;(?!;))|(?::(?!:))|(?:-(?!-))|[.]+)|[/?!])(?P<:Whitespace>{WSP_RE__})', '/(?!->)(?:(?:,(?!,))|(?:;(?!;))|(?::(?!:))|(?:-(?!-))|[.]+)|[\\/?!]/ ~')
    _STELLENKERN = OneOrMore(Alternative(TEXTELEMENT, TEIL_SATZZEICHEN, AUSGEWICHEN))
    SATZZEICHEN = SmartRE(f"((?!->)(?:(?:,(?!,))|(?:;(?!;))|:|(?:-(?!-))|(?:\\[(?!\\[))|(?:\\](?!\\]))|[.]+)|[`´''‘’/?!~])(?P<:Whitespace>{WSP_RE__})", "/(?!->)(?:(?:,(?!,))|(?:;(?!;))|:|(?:-(?!-))|(?:\\[(?!\\[))|(?:\\](?!\\]))|[.]+)|[`´''‘’\\/?!~]/ ~")
    BINDESTRICH = RegExp('(?:-(?!-))')
    OM_INDIKATOR = RegExp('rec\\.|ed\\.|cod\\. ')
    JAHRESZAHL = RegExp('[1-9][0-9](?:[0-9][0-9])?')
    PUNKT = Text(".")
    JAHR = RegExp('[1-9][0-9][0-9][0-9](?=[ .;\\n])')
    AUFLAGE = SmartRE(f'(?:(?:(?P<:Text>\\{{\\^)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>\\^\\{{)(?P<:Whitespace>{WSP_RE__}))([1-9][0-9]?[0-9]?)(?:(?P<:Text>\\}})(?P<:Whitespace>{WSP_RE__})))|(?:(?P<:Text>\\^)(?P<:Whitespace>{WSP_RE__}))([1-9])', '"{^"|"^{" /[1-9][0-9]?[0-9]?/ "}"|"^" /[1-9]/')
    Erscheinungsjahr = Series(Option(AUFLAGE), JAHR, Option(PUNKT), wsp__)
    _WERKTITEL_ZEICHEN = OneOrMore(Alternative(RegExp("(?:[a-zßäöüéáèàœæçëâûôîïāī,.?/]+|(?<=\\w)'(?=\\w))+"), SONDERBUCHSTABE))
    _WERKTITEL_KERN = OneOrMore(Alternative(_WERKTITEL_ZEICHEN, Series(KLAMMER_AUF, NegativeLookahead(OM_INDIKATOR), _WERKTITEL_ZEICHEN, Option(Alternative(Series(HOCHGESTELLT, NegativeLookahead(Series(wsp__, Erscheinungsjahr))), TIEFGESTELLT)), KLAMMER_ZU), Series(HOCHGESTELLT, NegativeLookahead(Series(wsp__, Erscheinungsjahr))), TIEFGESTELLT))
    WERKTITEL_FOLGE = Series(Option(ZITAT_ANFANG), OneOrMore(Series(Option(Alternative(ECKIGE_AUF, KLAMMER_AUF)), Alternative(Series(RegExp('[A-ZÄÖÜa-zßäöüéáèàœæçëâûóôîïíñ]?'), Alternative(Lookbehind(RegExp('y')), _WERKTITEL_KERN)), RegExp('[MDCLXVI]+|[A-ZÄÖÜ]{1,1}|-{1,1}'), Series(NegativeLookahead(Series(Lookbehind(RegExp('\\s*\\.')), Erscheinungsjahr)), Alternative(Series(NegativeLookahead(JAHR), RegExp('[0-9]+')), Series(RegExp('[0-9]+'), Lookahead(Series(Series(Text("."), wsp__), Erscheinungsjahr)))))), Option(Alternative(ECKIGE_ZU, KLAMMER_ZU)))), Option(ZITAT_ENDE), wsp__)
    WERKTITEL_ANFANG = Series(Option(ECKIGE_AUF), Alternative(Series(RegExp('[A-ZÄÖÜ-]{0,3}'), SmartRE(f'(?![A-ZÄÖÜ])', '!/[A-ZÄÖÜ]/'), _WERKTITEL_KERN), Series(RegExp('[A-ZÄÖÜ-]{1,3}'), SmartRE(f'(?![A-ZÄÖÜ])', '!/[A-ZÄÖÜ]/'), Option(_WERKTITEL_KERN)), SmartRE(f'([MDCLXVI]+|-{{1,1}})(?![A-ZÄÖÜ])', '/[MDCLXVI]+|-{1,1}/ !/[A-ZÄÖÜ]/')), Option(ECKIGE_ZU), wsp__)
    VERBORGEN = Series(Text("VERBORGEN"), wsp__)
    AUTORNAME = Series(NegativeLookahead(VERBORGEN), RegExp('\\[?(?:[A-ZÄÖÜ.\\-]+|[0-9\\-]+)\\]?(?=(?:\\s+|;|\\]\\]|$))'), wsp__)
    ROEMISCHE_ZAHL = Alternative(ROEM_GROSSBUCHSTABEN, Series(Option(ROEM_TAUSENDER), ROEM_NORMAL), Series(ROEM_TAUSENDER, RegExp('(?=1|[^\\w]|_)\\.?'), wsp__))
    SCHLUESSELWORT = Series(wsp__, RegExp('\\s*'), wsp__, NegativeLookahead(ROEMISCHE_ZAHL), RegExp('[A-ZÄÖÜ_]{4,}\\s+'))
    LAT_WORT = Series(_LATEIN_KLEIN_FOLGE, ZeroOrMore(Alternative(_LATEIN_KLEIN_FOLGE, Series(Text("("), _LATEIN_KLEIN_FOLGE, Text(")")), RegExp('-(?!-)'))), RegExp('\\.?'), NegativeLookahead(_LATEIN), wsp__)
    nicht_klassisch_verweis = Series(Text("*"), wsp__)
    KLEINE_ROEMISCHE_ZAHL = SmartRE(f'((?![md][^\\w])(?=[mdclxvi])m*(?:c[md]|d?c*)(?:x[cl]|l?x*)(?:i[xv]|v?i*)(?=1|[^\\w]|_)\\.?)(?P<:Whitespace>{WSP_RE__})', '/(?![md][^\\w])(?=[mdclxvi])m*(?:c[md]|d?c*)(?:x[cl]|l?x*)(?:i[xv]|v?i*)(?=1|[^\\w]|_)\\.?/ ~')
    GROSSBUCHSTABEN = Series(NegativeLookahead(ROEMISCHE_ZAHL), _LATEIN_GROSS_FOLGE, RegExp('(?=[ ,\\^\\t\\n)]|$)'), wsp__)
    DEU_WORT = Alternative(DEU_GROSS, DEU_KLEIN, GROSSBUCHSTABEN)
    AUTORANFANG = Alternative(Series(Text("LXX"), wsp__), Series(NegativeLookahead(VERBORGEN), NegativeLookahead(ROEMISCHE_ZAHL), RegExp('\\[?(?:[A-ZÄÖÜ][A-ZÄÖÜ.\\-]+)(?:\\](?!\\]))?(?=(?:\\s+|;|$|\\]))'), wsp__))
    STELLE_VERBOTEN = Alternative(VARIANTE, SCHLUESSELWORT)
    ARABISCHE_ZAHL = SmartRE(f'(\\d+)(?P<:Whitespace>{WSP_RE__})', '/\\d+/ ~')
    SEITENZAHL = SmartRE(f'(\\d+)(?P<:Whitespace>{WSP_RE__})', '/\\d+/ ~')
    LEMMAFRAGMENT = Series(Option(RegExp('[.|]*')), Alternative(LAT_GWORT, GRI_WORT, DEU_WORT))
    Athetese = Series(Option(RegExp('[.|]*')), Text("["), LEMMAFRAGMENT, Text("]"))
    GROSSSCHRIFT = Series(_LATEIN_GROSS_FOLGE, ZeroOrMore(Alternative(_LATEIN_GROSS_FOLGE, RegExp('-(?!-)'))), NegativeLookahead(_LATEIN), wsp__)
    LAT_ABK = SmartRE(f'(\\w+\\.)(?P<:Whitespace>{WSP_RE__})', '/\\w+\\./ ~')
    LEMMAWORT = Series(Alternative(Athetese, Series(Option(RegExp('-')), LEMMAFRAGMENT)), ZeroOrMore(Series(NegativeLookbehind(RegExp(' ')), Alternative(Athetese, LEMMAFRAGMENT))))
    _lemma_text = Series(Option(RegExp('-')), Option(Alternative(Series(RegExp('\\('), _LATEIN_KLEIN_FOLGE, RegExp('\\)')), RegExp('\\(-\\)'))), LAT_GWORT, Option(RegExp('\\(-\\)')), ZeroOrMore(Series(Option(AUSGEWICHEN), NegativeLookahead(Text("p.")), LAT_WORT, Option(RegExp('\\(-\\)')))))
    DEU_GEMISCHT = Series(_LATEIN, ZeroOrMore(Alternative(_LATEIN_FOLGE, RegExp('-(?!-)'))), RegExp('\\.?'), NegativeLookahead(_LATEIN), NegativeLookahead(Text("|")), wsp__)
    Lemmawort = Series(Alternative(tmesi, Series(Text("#"), wsp__)), Alternative(Series(Series(Text("{"), wsp__), Option(ZW), OneOrMore(Series(Alternative(LEMMAWORT, SATZZEICHEN), Option(ZW))), Series(Text("}"), wsp__), mandatory=2), Series(LEMMAWORT, mandatory=0)), mandatory=1)
    BelegLemma = Synonym(Lemmawort)
    ZITAT = Series(ZITAT_ANFANG, ZeroOrMore(Alternative(TEXTELEMENT, ZITAT_SATZZEICHEN, KLAMMER_AUF, KLAMMER_ZU, ECKIGE_AUF, ECKIGE_ZU, BelegLemma, Zusatz, opus_minus, Einschub, Verweise, Sperrung, Junktur, Kursiv)), ZITAT_ENDE)
    DEU_KOMP_GLIED = Series(_LATEIN, ZeroOrMore(_LATEIN_KLEIN_FOLGE), RegExp('\\.?'))
    DEU_KOMPOSITUM = Series(DEU_KOMP_GLIED, ZeroOrMore(Series(RegExp('-'), DEU_KOMP_GLIED)), NegativeLookahead(_LATEIN), NegativeLookahead(Text("|")), wsp__)
    ETYMOLOGIE_TEXT = OneOrMore(Alternative(DEU_GWORT, DEU_KOMPOSITUM, LAT_GWORT, GRI_WORT, KLAMMER_AUF, KLAMMER_ZU, ECKIGE_AUF, ECKIGE_ZU, BINDESTRICH, RegExp("[.']")))
    T_ELEMENT = Alternative(ROEMISCHE_ZAHL, DEU_WORT, DEU_GEMISCHT, GRI_WORT, SEITENZAHL, HOCHGESTELLT, TIEFGESTELLT, SPEZIAL, Series(SONDERZEICHEN, wsp__), AUSGEWICHEN)
    NAME = Series(_LATEIN_GROSS, _LATEIN_KLEIN_FOLGE, wsp__)
    NAMENS_ABKÜRZUNG = Series(_LATEIN_GROSS, Text("."), wsp__)
    ANKER_NAME = SmartRE(f'([a-zA-Z0-9_]+)(?P<:Whitespace>{WSP_RE__})', '/[a-zA-Z0-9_]+/ ~')
    PFAD_NAME = RegExp('[\\w=~?.,%&\\[\\]-]+')
    ziel = Series(PFAD_NAME, ZeroOrMore(Series(NegativeLookbehind(RegExp('s?ptth')), Text(":"), PFAD_NAME)), Option(Series(Alternative(Text("#"), Series(NegativeLookbehind(RegExp('s?ptth')), Text(":"))), RegExp('[\\w=?.:\\-%&\\[\\] /]+'))))
    pfad = Series(PFAD_NAME, RegExp('/(?!\\*)'))
    protokoll = RegExp('\\w+://(?!\\*)')
    nicht_klassisch = Series(Text("*"), wsp__)
    Textzusatz = SmartRE(f'(?:(?P<:Text>\\{{)(?P<:Whitespace>{WSP_RE__}))(\\w+\\.?)(?:(?P<:Text>\\}})(?P<:Whitespace>{WSP_RE__}))', '"{" /\\w+\\.?/ "}"')
    URL = Series(NegativeLookahead(Text("}")), Option(protokoll), ZeroOrMore(pfad), Option(ziel), wsp__)
    alias = Series(Option(nicht_klassisch), OneOrMore(Alternative(T_ELEMENT, GROSSSCHRIFT, SATZZEICHEN, SmartRE(f'([\\w/\\t\\n,.⇒→-]+)(?P<:Whitespace>{WSP_RE__})', '/[\\w\\/\\t\\n,.⇒→-]+/ ~'), Kursiv, Series(KLAMMER_AUF, ZeroOrMore(T_ELEMENT), KLAMMER_ZU))))
    Anker = Series(Option(ZW), Series(Text("{"), wsp__), Series(Text("@"), wsp__), Option(ZW), ANKER_NAME, Series(Text("}"), wsp__), mandatory=4)
    Verweis = Series(Alternative(Series(URL, SmartRE(f'(?=[;{{}})])', '&/[;{})]/')), Series(alias, Option(ZW), Series(Text("|"), wsp__), Option(ZW), Alternative(URL, Series(Text("-"), wsp__)))), Option(Textzusatz), SmartRE(f'(?=[;}})])', '&/[;})]/'), mandatory=2)
    VerweisKern = Series(Series(Text("=>"), wsp__), Option(ZW), Verweis, ZeroOrMore(Series(Series(Text(";"), wsp__), Option(ZW), Verweis)), mandatory=2)
    dot = SmartRE(f'(\\.)(?P<:Whitespace>{WSP_RE__})', '/\\./ ~')
    OriginalStelle = Synonym(Stelle)
    OM_WERK = SmartRE(f'(?!rec\\.)([A-Z][A-Z][A-Z]?(?![A-Z.]))', '!`rec.` /[A-Z][A-Z][A-Z]?(?![A-Z.])/')
    OM_MARKIERUNG = Series(Text("OM:"), wsp__)
    om_regeln = Alternative(OM_WERK, Series(Text("ed."), wsp__), SmartRE(f'(?:(?P<:Text>cod\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>Cod\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>Vat)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>Paris)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>Darmst)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>Guelf)(?P<:Whitespace>{WSP_RE__}))', '"cod."|"Cod." "Vat"|"Paris"|"Darmst"|"Guelf"'))
    OM_FALSCHE_ZUSATZANGABE = Synonym(FREITEXT)
    om_zusatzangabe = Alternative(Series(Text("vs."), wsp__), Series(Text("epist."), wsp__), OM_FALSCHE_ZUSATZANGABE)
    DOPPEL_SEM = Series(Text(";;"), wsp__)
    STELLENKERN = Series(Alternative(NegativeLookahead(TEIL_SATZZEICHEN), Lookahead(ZITAT)), OneOrMore(Alternative(ZITAT, TEXTELEMENT, TEIL_SATZZEICHEN, AUSGEWICHEN, Series(RegExp('\\['), wsp__, _STELLENKERN, RegExp('\\]'), wsp__))))
    _om_stelle = Series(Option(Series(Option(Series(Series(Text(";"), wsp__), NegativeLookahead(Text(";")), Option(ZW))), Stelle, Option(Series(Series(Text(";"), wsp__), NegativeLookahead(Text(";")), Option(ZW), OriginalStelle)))), Option(BelegText))
    STELLENTEXT = Alternative(STELLENKERN, Series(KLAMMER_AUF, SmartRE(f'(?!(?:dipl. )?[acs]\\.)', '!/(?:dipl. )?[acs]\\./'), STELLENKERN, KLAMMER_ZU, Option(TEIL_SATZZEICHEN)))
    EINZEILER = OneOrMore(Alternative(TEXTELEMENT, ZITAT, TEIL_SATZZEICHEN, KLAMMER_AUF, KLAMMER_ZU, ECKIGE_AUF, ECKIGE_ZU))
    LemmaNr = SmartRE(f'(\\d+\\.?)(?P<:Whitespace>{WSP_RE__})', '/\\d+\\.?/ ~')
    Zähler = Alternative(ROEMISCHE_ZAHL, SmartRE(f'([A-Y](?!\\w))(?P<:Whitespace>{WSP_RE__})', '/[A-Y](?!\\w)/ ~'), SmartRE(f'([a-z](?!\\w))(?P<:Whitespace>{WSP_RE__})', '/[a-z](?!\\w)/ ~'), SmartRE(f'(\\d\\d?)(?P<:Whitespace>{WSP_RE__})', '/\\d\\d?/ ~'), SmartRE(f'([αβγδεζηθικλμνξοπρστυφχψω])(?P<:Whitespace>{WSP_RE__})', '/[αβγδεζηθικλμνξοπρστυφχψω]/ ~'), Series(KLEINE_ROEMISCHE_ZAHL, SmartRE(f'(?!\\w)', '!/\\w/')))
    NachtragsStelle = Synonym(STELLENTEXT)
    Vide_Anschluss = Alternative(ZEILENSPRUNG, RegExp('\\s*\\]|\\s*$'), Series(Text("PRAETER"), wsp__), Series(Text("LEMMA"), wsp__), SmartRE(f'(\\s*)(?:(?P<:Text>BEDEUTUNG)(?P<:Whitespace>{WSP_RE__}))', '/\\s*/ "BEDEUTUNG"'))
    DATIERUNG = Series(NegativeLookahead(Text("p.")), Option(DATIERUNG_FEHLER), OneOrMore(Alternative(STELLENKERN, HOCHGESTELLT, TIEFGESTELLT, ROEMISCHE_ZAHL)))
    KeinAbschnitt = SmartRE(f'(?P<:Text>\\-|)', '`-`|``')
    _om_ergänzung = Series(DOPPEL_SEM, Alternative(Series(Option(ZW), DATIERUNG, Option(Series(DOPPEL_SEM, Option(ZW), om_zusatzangabe))), Series(Option(ZW), om_zusatzangabe)))
    fraglich = SmartRE(f'(?P<:Text>\\(\\?\\))(?P<:Whitespace>{WSP_RE__})|(?P<:Text>\\[\\?\\])(?P<:Whitespace>{WSP_RE__})', '"(?)"|"[?]"')
    PRAETER_Fehler = SmartRE(f'(?:(?:(?P<:Text>\\{{)?)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>Praeter)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>praeter)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>\\}})?))|(?P<:Text>\\{{)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>PRAETER)(?P<:Whitespace>{WSP_RE__}))(?P<:Text>\\}})', '[`{`] ["/"] "Praeter"|"praeter" [`}`]|`{` ["/"] "PRAETER" `}`')
    Werk = Series(Option(ZW), RegExp('(?![ap]\\.)(?![0-9])'), WERKTITEL_ANFANG, ZeroOrMore(Series(Option(ZW), WERKTITEL_FOLGE)))
    _Werk = Series(Werk, Option(Erscheinungsjahr))
    EinzelVerweis = Alternative(Series(Series(Text("{"), wsp__), VerweisKern, Series(Text("}"), wsp__), mandatory=2), Series(RegExp('\\('), Text("{"), VerweisKern, Text("}"), RegExp('\\)'), wsp__), Series(Lookbehind(RegExp('{')), RegExp('\\('), VerweisKern, RegExp('\\)'), wsp__, mandatory=3))
    FALSCHER_MARKER = RegExp('\\§ *\\n? *')
    disambig_marker = Alternative(RegExp('\\$ *\\n? *'), FALSCHER_MARKER)
    fehler_fraglich = Series(wsp__, fraglich)
    Autor = Series(Option(ZW), Option(disambig_marker), AUTORANFANG, ZeroOrMore(Series(Option(ZW), Alternative(ROEMISCHE_ZAHL, AUTORNAME))), Option(fraglich))
    _AutorWerk = Series(Autor, Option(Anker), Option(Series(_Werk, Option(Anker))), SmartRE(f'(?=;|\\)\\)|\\]\\])', '&`;`|`))`|`]]`'), mandatory=2)
    AW_Verborgen = Series(Series(Text("[["), wsp__), _AutorWerk, Series(Text("]]"), wsp__), mandatory=2)
    AutorWerk = Alternative(_AutorWerk, AW_Verborgen)
    om_kern_ungeprüft = Series(NegativeLookahead(Text("rec.")), Alternative(Series(OM_MARKIERUNG, Alternative(_Werk, Nachschlagewerk), mandatory=1), Alternative(_Werk, Nachschlagewerk)), Option(Alternative(Verweise, _om_stelle)), Option(_om_ergänzung), Interleave(Series(Option(ZW), om_erwartet), Series(Option(ZW), Zusatz), repetitions=[(0, 1), (0, 1)]))
    om_ungeprüft = Series(Series(Text("(("), wsp__), Option(ZW), om_kern_ungeprüft, ZeroOrMore(Series(Series(Text("*"), wsp__), om_kern_ungeprüft)), Series(Text("))"), wsp__))
    om_kern = Series(Alternative(OM_MARKIERUNG, Lookahead(om_regeln)), Alternative(_Werk, Nachschlagewerk), Option(Alternative(Verweise, _om_stelle)), Option(_om_ergänzung), Interleave(Series(Option(ZW), opus), Series(Option(ZW), Zusatz), repetitions=[(0, 1), (0, 1)]), mandatory=1)
    AddeAngabe = Series(Option(LZ), ADDE_AD, NachtragsStelle, Text(":"), wsp__, Option(LZ))
    _StellenVerweis = Series(KLAMMER_AUF, SmartRE(f'(?![acs]\\.)', '!/[acs]\\./'), OneOrMore(Series(Option(STELLENKERN), Verweise)), Option(STELLENKERN), KLAMMER_ZU)
    Stellenabschnitt = Alternative(EinzelVerweis, OneOrMore(Alternative(Series(Option(ZW), NegativeLookahead(STELLE_VERBOTEN), STELLENTEXT), Kursiv, _StellenVerweis)))
    nicht_gesichert = Series(Text("?"), wsp__)
    ET_Fehler = SmartRE(f'(?:(?:(?P<:Text>\\{{)?)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>Et)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>et)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>\\}})?))|(?P<:Text>\\{{)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>ET)(?P<:Whitespace>{WSP_RE__}))(?P<:Text>\\}})', '[`{`] ["/"] "Et"|"et" [`}`]|`{` ["/"] "ET" `}`')
    SEM = Series(Option(ZW), Series(Text(";"), wsp__))
    Datierung = Series(RegExp('\\((?=[\\w])'), wsp__, DATIERUNG, RegExp('\\)'), wsp__)
    Stellenangabe = Series(Alternative(om_erwartet, Stelle), Interleave(EinzelVerweis, Zusatz, Datierung, repetitions=[(0, INFINITE), (0, 1), (0, 1)]), ZeroOrMore(Series(Option(ZW), om_erwartet, Option(Series(Option(ZW), Datierung)))))
    BelegErgänzung = Series(Option(Alternative(ZW, wsp__)), Alternative(Series(NegativeLookahead(Beschreibung), Zusätze), Einschub))
    sim_ibid_kern = SmartRE(f'(sim\\. *ibid\\. *)(?P<:Whitespace>{WSP_RE__})', '/sim\\. *ibid\\. */ ~')
    Werktitel = Series(Option(ZW), RegExp('(?![ap]\\.)(?![0-9])'), WERKTITEL_FOLGE, ZeroOrMore(Series(Option(ZW), WERKTITEL_FOLGE)))
    Autorangabe = Series(ZeroOrMore(Series(DEU_GEMISCHT, ZeroOrMore(Series(Series(Text("-"), wsp__), DEU_GEMISCHT)))), Option(Anker), ZeroOrMore(Series(Series(Text(","), wsp__), ZeroOrMore(DEU_GEMISCHT), Option(Anker), Lookahead(Series(Series(Text(","), wsp__), DEU_GEMISCHT)))))
    _Verknüpfung = Alternative(Interleave(Anker, Alternative(Verweise, Series(KLAMMER_AUF, Verweise, KLAMMER_ZU), Series(ECKIGE_AUF, Verweise, ECKIGE_ZU)), repetitions=[(1, 1), (1, 1)]), Anker, Verweise, Series(KLAMMER_AUF, Verweise, KLAMMER_ZU), Series(ECKIGE_AUF, Verweise, ECKIGE_ZU))
    Band = Series(WERKTITEL_FOLGE, ZeroOrMore(Series(Option(ZW), WERKTITEL_FOLGE)))
    Werkname = Alternative(SmartRE(f'((?:Bibl\\.|CC|Cod\\.|Corp\\.|Font\\.|Hist\\.|MG|Mittelniederdt\\.|Althochdt\\.|Dt\\.|Frühmittelalterl\\.|Mitt|Mon\\. |Rer\\.|Scrin\\.|Script\\.|Sources|Sudhoffs) \\w+\\.(?: \\w+\\.)?)(?P<:Whitespace>{WSP_RE__})', '/(?:Bibl\\.|CC|Cod\\.|Corp\\.|Font\\.|Hist\\.|MG|Mittelniederdt\\.|Althochdt\\.|Dt\\.|Frühmittelalterl\\.|Mitt|Mon\\. |Rer\\.|Scrin\\.|Script\\.|Sources|Sudhoffs) \\w+\\.(?: \\w+\\.)?/ ~'), SmartRE(f'((?:Abh|Ahd\\. |Anal|Anz|Arab\\. |Arch|AS|Bonn|Bull|Dt|Edit|Forsch|Freib|GeschQuell|Hist|Jb|Journ|Lex|Liturg|Mediaev|Mittellat|Mitt|Münch|Nachr|Oberbayer|Quell|Rev|Rom|Röm|Schweiz|Stud|Theol|Thür|Verh|Veröff|Vjschr|Westdt|Z)[A-Z]\\w+\\.)(?P<:Whitespace>{WSP_RE__})', '/(?:Abh|Ahd\\. |Anal|Anz|Arab\\. |Arch|AS|Bonn|Bull|Dt|Edit|Forsch|Freib|GeschQuell|Hist|Jb|Journ|Lex|Liturg|Mediaev|Mittellat|Mitt|Münch|Nachr|Oberbayer|Quell|Rev|Rom|Röm|Schweiz|Stud|Theol|Thür|Verh|Veröff|Vjschr|Westdt|Z)[A-Z]\\w+\\./ ~'), RegExp('Lex\\.?(?: ?\\w+\\.?)+'), Series(Text("Der"), Option(LZ), Series(Text("Geschichtsfreund"), wsp__)), SmartRE(f'(?:(?P<:Text>N)?)(?:(?P<:Text>Arch\\.)(?P<:Whitespace>{WSP_RE__}))', '[`N`] "Arch."'), SmartRE(f'(?:(?P<:Text>MG\\ \\(Script\\.\\ rer\\.\\)\\ )(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>Lang\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>Mer\\.)(?P<:Whitespace>{WSP_RE__}))', '"MG (Script. rer.) " "Lang."|"Mer."'), Series(Text("SBBerl."), wsp__), Series(Text("Schweizerisches"), Option(LZ), Series(Text("Idiotikon"), wsp__)), Series(Text("Allg."), Option(LZ), Text("Encyklopädie"), Option(LZ), Text("d."), Option(LZ), Text("Wissenschaften"), Option(LZ), Text("u."), Option(LZ), Series(Text("Künste"), wsp__)), SmartRE(f'(Reallexikon (?: ?(?!\\d)\\w+\\.?)+)(?P<:Whitespace>{WSP_RE__})', '/Reallexikon (?: ?(?!\\d)\\w+\\.?)+/ ~'), SmartRE(f'(Chronologisches\\s+W(?: ?(?!\\d)\\w+\\.?)+)(?P<:Whitespace>{WSP_RE__})', '/Chronologisches\\s+W(?: ?(?!\\d)\\w+\\.?)+/ ~'), Series(Text("ThLL."), wsp__), Series(Text("DuC."), wsp__), Series(Text("Mél."), Option(LZ), Text("de"), Option(LZ), Series(Text("Ghellinck"), wsp__)), SmartRE(f'(Grzimeks Tierleben\\.?)(?P<:Whitespace>{WSP_RE__})', '/Grzimeks Tierleben\\.?/ ~'), Series(Series(Text("Der"), wsp__), Series(Text("Neue"), wsp__), Series(Text("Pauly"), wsp__), Option(Series(Text("."), wsp__)), Option(Series(ROEMISCHE_ZAHL, Option(Series(Text("."), wsp__))))), Series(Alternative(Text("DMLBS"), Text("ALMA"), Text("HRG"), Series(Text("MIÖG"), ZeroOrMore(WERKTITEL_FOLGE)), Series(Text("LThK."), wsp__), Text("PG"), Text("PL"), Text("UB"), Text("RAC"), Text("RDK"), Text("RE"), Text("DEAF")), Option(Text("|")), SmartRE(f'(?!(?!\\d)\\w)', '!/(?!\\d)\\w/'), wsp__))
    op_cit_werk = SmartRE(f'(?:(?:(?P<:Text>cf\\.)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>op\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>cit\\.)(?P<:Whitespace>{WSP_RE__}))', '["cf."] "op." "cit."')
    op_cit_autor_werk = SmartRE(f'(?:(?:(?P<:Text>cf\\.)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>op\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>cit\\.)(?P<:Whitespace>{WSP_RE__}))', '["cf."] "op." "cit."')
    VIDE_Fehler = SmartRE(f'(?:(?:(?P<:Text>\\{{)?)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>Vide)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>vide)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>\\}})?))|(?P<:Text>\\{{)(?:(?:(?P<:Text>/)(?P<:Whitespace>{WSP_RE__}))?)(?:(?P<:Text>VIDE)(?P<:Whitespace>{WSP_RE__}))(?P<:Text>\\}})', '[`{`] ["/"] "Vide"|"vide" [`}`]|`{` ["/"] "VIDE" `}`')
    sim_ibid = Series(Series(Text("{"), wsp__), sim_ibid_kern, Series(Text("}"), wsp__))
    Quelle = Series(Option(ZW), Option(Series(Text("*"), wsp__)), AutorWerk, Option(Series(NegativeLookahead(Beschreibung), Zusatz)), Option(Einschub), Option(Series(NegativeLookahead(Beschreibung), Zusatz)))
    BelegStelle = Series(ZeroOrMore(Series(NegativeLookahead(sim_ibid), Zusatz)), Option(ZW), Option(Series(Option(Series(Text("*"), wsp__)), _Werk, Option(Anker), SEM)), Stellenangabe, Option(Series(Option(ZW), BelegText)))
    _BelegAnschluss = Alternative(Series(ZeroOrMore(SmartRE(f'(\\s+)(?P<:Whitespace>{WSP_RE__})', '/\\s+/ ~')), RegExp('\\s*\\)\\)|\\s*\\*|\\s*[A-ZÄÖÜ{]')), SmartRE(f'(?P<:Whitespace>{WSP_RE__})(\\n\\s*[^:\\n]*:)', '~ /\\n\\s*[^:\\n]*:/'), RegExp('\\s*$'), RegExp('\\]|\\)'))
    Sekundärliteratur = Series(Option(Series(OneOrMore(Series(ZITAT, KOMMA)), Option(Zusatz))), Alternative(Series(Autorangabe, Option(Anker), Series(Text(","), wsp__), Option(ZW), Alternative(op_cit_werk, Series(Werktitel, Option(Erscheinungsjahr))), Option(Alternative(_Verknüpfung, Zusatz))), Series(Nachschlagewerk, Option(Alternative(_Verknüpfung, Zusatz))), Series(op_cit_autor_werk, Option(Alternative(_Verknüpfung, Zusatz)))), ZeroOrMore(Series(SEM, Option(ZW), NegativeLookahead(Text("*")), BelegStelle)))
    Zusatzzeilen = Series(LÜCKE, OneOrMore(Series(Option(ZLL), NegativeLookahead(Beschreibung), Alternative(Zusatz, Verweise))))
    ZielLemmaWort = Synonym(_lemma_text)
    VLZ_Fehler = Alternative(ET_Fehler, PRAETER_Fehler, VIDE_Fehler)
    FehlerhafterVerweis = Series(Option(LZ), Series(Text("["), wsp__), Series(Text("LEMMA"), wsp__), RegExp('(?:.|\\n)*?(?:\\n(?=LEMMA)|(?=\\n\\s*AUTOR))'))
    zusatz_spezialisierung = SmartRE(f'(?P<:Text>=>)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>@)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>\\^)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>!)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>/)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>_)(?P<:Whitespace>{WSP_RE__})|(?:(?P<:Text>\\-)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>\\}})(?P<:Whitespace>{WSP_RE__}))', '"=>"|"@"|"^"|"!"|"/"|"_"|"-" "}"')
    VariaLectioZusatz = Series(Series(Text("{"), wsp__), NegativeLookahead(zusatz_spezialisierung), OneOrMore(Series(TEXTELEMENT, Option(Series(Text("."), wsp__)), Option(ZW))), SmartRE(f'(?=\\}}\\)(?!\\)))', '&/\\}\\)(?!\\))/'), Series(Text("}"), wsp__))
    Quellenangabe = Series(Alternative(Quelle, SmartRE(f'(?:(?:(?P<:Text>\\*)(?P<:Whitespace>{WSP_RE__}))?)(?=;)', '["*"] &`;`'), Series(BelegStelle, Option(BelegErgänzung))), ZeroOrMore(Series(SEM, Option(ZW), BelegStelle, Option(BelegErgänzung))))
    Beleg = Alternative(Series(Zusatz, Lookahead(SCHLUESSELWORT), NegativeLookahead(Series(Option(ZW), AutorWerk))), Series(Zusatz, Lookahead(Series(Option(ZW), Series(Text("*"), wsp__)))), Series(Option(Series(NegativeLookahead(sim_ibid), Zusatz)), Alternative(OneOrMore(Series(Verweise, Option(Series(Option(ZW), NegativeLookahead(Beschreibung), Zusatz)))), Series(Option(Series(Text("*"), wsp__)), Sekundärliteratur, Option(BelegErgänzung)), Quellenangabe), Option(Series(Alternative(DOPPEL_SEM, SEM), NegativeLookahead(Beschreibung), Zusatz)), NegativeLookahead(VARIANTEN_ABSCHLUSS), Lookahead(_BelegAnschluss), mandatory=4))
    Belege = Series(Lookahead(Alternative(Series(Option(LZ), Series(Text("*"), wsp__)), Beleg)), Option(Beleg), ZeroOrMore(Series(Option(LZ), Series(Text("*"), wsp__), Beleg, mandatory=2)), Option(Zusatzzeilen))
    FesterZusatz = Series(Series(Text("{"), wsp__), Alternative(Series(Text("adde-ad"), wsp__), Series(Text("adde"), wsp__), Series(Text("al"), wsp__), Series(Text("ibid-al"), wsp__), Series(Text("ibid-per-saepe"), wsp__), Series(Text("ibid-saepe"), wsp__), Series(Text("ibid-saepius"), wsp__), Series(Text("persaepe"), wsp__), sim_ibid_kern, Series(Text("sim"), wsp__), Series(Text("saepe"), wsp__), Series(Text("saepius"), wsp__), Series(Text("vel-rarius"), wsp__), Series(Text("vel-semel"), wsp__), Series(Text("vel"), wsp__)), Option(Series(Text("."), wsp__)), Series(Text("}"), wsp__))
    Ueberschrift = Series(Series(Text("ÜBERSCHRIFT"), wsp__), EINZEILER, wsp__, mandatory=1)
    VERBINDE = SmartRE(f'(?=[^\\n]*LEMMA)', '&/[^\\n]*LEMMA/')
    VIDE = Alternative(Series(Text("VIDE"), wsp__), VIDE_Fehler)
    ZusatzInhalt = OneOrMore(Alternative(Verweise, FREITEXT, Anker, BelegText, BelegLemma, Kursiv, Sperrung, Junktur, Text("(("), Text("))")))
    FreierZusatz = Alternative(Series(Series(Text("{"), wsp__), NegativeLookahead(zusatz_spezialisierung), Option(ZusatzInhalt), Series(Text("}"), wsp__), mandatory=3), Series(Lookahead(Text("({=>")), EinzelVerweis))
    TRENNE = Lookahead(Alternative(LÜCKE, Series(Option(LZ), Ueberschrift)))
    Anschluss = Series(Option(LZ), Alternative(RegExp('BEDEUTUNG|ABSATZ|UNTER|U_|UU|AUFZÄHLUNG|SUB_LEMMA|LEMMA|VERWEISE|AUTOR|NACHTRAG'
       '|ÜBERSCHRIFT'), _DATEI_ENDE), mandatory=1)
    NullVerweis = Series(Series(Text("{"), wsp__), Series(Text("-"), wsp__), Series(Text("}"), wsp__))
    Stellenverweis = Series(AutorWerk, ZeroOrMore(Series(Option(ABS), Stelle, Alternative(NullVerweis, Verweise))))
    Verweisliste = ZeroOrMore(Series(Option(LZ), Series(Text("*"), wsp__), Stellenverweis))
    LemmaWort = Synonym(_lemma_text)
    unbekannt = Series(Text("unbekannt"), wsp__)
    Name = OneOrMore(Series(Alternative(NAME, NAMENS_ABKÜRZUNG, Series(Text("-"), wsp__), unbekannt), Option(Series(Text(","), wsp__))))
    Verfasser = Series(Name, ZeroOrMore(Series(Option(LZ), Text("/"), wsp__, Option(LZ), Name)))
    ArtikelVerfasser = Series(ZWW, SmartRE(f'(?P<:Text>AUTORINNEN)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>AUTOREN)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>AUTORIN)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>AUTOR)(?P<:Whitespace>{WSP_RE__})', '"AUTORINNEN"|"AUTOREN"|"AUTORIN"|"AUTOR"'), Option(LZ), Verfasser, mandatory=3)
    ZielLemma = Series(Interleave(nicht_klassisch_verweis, nicht_gesichert, repetitions=[(0, 1), (0, 1)]), ZeroOrMore(Zusatz), ZielLemmaWort)
    _Lemma = Series(Interleave(nicht_klassisch, nicht_gesichert, repetitions=[(0, 1), (0, 1)]), ZeroOrMore(Series(Zusatz, Option(LL))), LemmaWort)
    Stellenverzeichnis = Series(ZWW, Series(Text("STELLENVERZEICHNIS"), wsp__), Option(LemmaWort), ZWW, Verweisliste)
    BelegPosition = Series(Option(ZWW), Alternative(Series(Text("*"), wsp__), NegativeLookahead(SCHLUESSELWORT)), Belege)
    NBVerweise = Series(Series(Text(":"), wsp__), OneOrMore(Series(Option(ZW), Interleave(Verweise, Series(Option(ZW), Zusatz), repetitions=[(1, 1), (0, 1)]))))
    DeutscheKlammer = Series(KLAMMER_AUF, DeutscheBedeutung, Option(Series(Series(Text(";"), wsp__), Zusatz)), KLAMMER_ZU)
    LateinischeKlammer = Series(KLAMMER_AUF, LateinischeBedeutung, Option(Series(Series(Text(";"), wsp__), Zusatz)), KLAMMER_ZU)
    DEU_ZITAT_STÜCK = Alternative(DEU_GWORT, KLAMMER_AUF, KLAMMER_ZU)
    DEU_ZITAT = Series(ZITAT_ANFANG, DEU_ZITAT_STÜCK, ZeroOrMore(Series(Option(ZW), DEU_ZITAT_STÜCK)), ZITAT_ENDE)
    DeutschesWort = Alternative(DEU_KOMPOSITUM, DEU_GWORT, DEU_ZITAT, ARABISCHE_ZAHL, ROEMISCHE_ZAHL)
    LAT_WÖRTER = Alternative(LAT_WORT, GRI_WORT, LAT_ABK, LAT_GWORT, ARABISCHE_ZAHL, ROEMISCHE_ZAHL)
    LAT_ZITAT = Series(ZITAT_ANFANG, LAT_WÖRTER, ZeroOrMore(Series(Option(ZW), LAT_WÖRTER)), ZITAT_ENDE)
    iq_markierung = SmartRE(f'(?:(?P<:Text>i\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>q\\.)(?P<:Whitespace>{WSP_RE__}))|(?:(?:(?P<:Text>i\\.)(?P<:Whitespace>{WSP_RE__}))(?=(?:qui)(?:{WSP_RE__})|(?:que)(?:{WSP_RE__})))|(?:(?P<:Text>ea)(?P<:Whitespace>{WSP_RE__}))(?=(?:quae)(?:{WSP_RE__}))', '"i." "q."|"i." &"qui"|"que"|"ea" &"quae"')
    DeutscheEllipse = Alternative(Series(DEU_GWORT, Lookbehind(RegExp('\\s*-'))), Series(RegExp('-'), DEU_GWORT), Series(Text("..."), wsp__))
    LateinischeEllipse = Alternative(Series(Alternative(LAT_WORT, GRI_WORT), Lookbehind(RegExp('\\s*-'))), Series(RegExp('-'), Alternative(LAT_WORT, GRI_WORT)), Series(Text("..."), wsp__))
    Qualifizierung = SmartRE(f'(?:(?P<:Text>etwa:)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>viell\\.:)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>vielleicht:)(?P<:Whitespace>{WSP_RE__}))(?!\\s*(?:\\*|UNTER|U_|UU))', '"etwa:"|"viell.:"|"vielleicht:" !/\\s*(?:\\*|UNTER|U_|UU)/')
    DeutscherBestandteil = Series(Option(ZW), Alternative(Qualifizierung, DeutscheEllipse, DeutschesWort, DeutscheKlammer, Kursiv))
    LateinischesWort = Series(NegativeLookahead(iq_markierung), Alternative(LAT_WÖRTER, LAT_ZITAT))
    unsicher = Series(Text("?"), wsp__)
    DeutscherAusdruck = Series(Option(unsicher), OneOrMore(Series(Option(ZW), Interleave(DeutscherBestandteil, BelegText, Series(wsp__, Option(ZW), Alternative(Zusatz, EinzelVerweis)), repetitions=[(1, 1), (0, 1), (0, INFINITE)]))))
    Kategorienangabe = OneOrMore(Series(Alternative(EINZEILER, Kursiv, Sperrung, BelegText, Lemmawort), Option(ZW)))
    LateinischerBestandteil = Series(Option(ZW), Alternative(LateinischeEllipse, LateinischesWort, Lemmawort, LateinischeKlammer, Kursiv))
    Klassifikation = Series(Option(Series(Zusatz, Option(ZW))), OneOrMore(Series(Kategorienangabe, Interleave(Verweise, Zusatz, ZW, repetitions=[(0, INFINITE), (0, INFINITE), (0, INFINITE)]))))
    NBEinschub = Series(Option(ZW), EINSCHUB_ANFANG, ZeroOrMore(Series(Option(Series(Text(";"), wsp__)), Klassifikation, Option(NBVerweise))), EINSCHUB_ENDE, Option(ZW))
    ET = Series(Option(LZ), Alternative(Series(Text("ET"), wsp__), ET_Fehler), Option(LZ))
    WA_UNBEKANNT = Series(RegExp('\\w+\\.?'), ZeroOrMore(SmartRE(f'(?P<:Whitespace>{WSP_RE__})(\\w+\\.?)', '~ /\\w+\\.?/')))
    InterpretamentTrenner = Series(Option(ZW), Series(Text("--"), wsp__), Option(ZW))
    Interpretamente = Series(Option(LateinischeBedeutung), Option(Series(Option(SmartRE(f'(,)(?P<:Whitespace>{WSP_RE__})', '/,/ ~')), Option(ZW), Zusätze)), InterpretamentTrenner, DeutscheBedeutung, Option(Series(Option(SmartRE(f'(,)(?P<:Whitespace>{WSP_RE__})', '/,/ ~')), Option(ZW), Zusätze)), mandatory=3)
    _LatAusdruckKern = Series(Option(ZW), Interleave(Interleave(Alternative(LateinischerBestandteil, BelegText), Series(wsp__, Option(ZW), Alternative(Zusatz, EinzelVerweis)), repetitions=[(1, 1), (0, INFINITE)]), Series(wsp__, Option(ZW), NBEinschub), repetitions=[(1, 1), (0, 1)]))
    idem_qui = Series(OneOrMore(Series(Option(ZW), NegativeLookahead(iq_markierung), _LatAusdruckKern)), iq_markierung)
    LateinischerAusdruck = Series(Interleave(unsicher, idem_qui, repetitions=[(0, 1), (0, 1)]), OneOrMore(_LatAusdruckKern))
    Nebenbedeutungen = OneOrMore(Alternative(Series(Option(ZW), Series(Text(";"), wsp__), Option(ZW), Option(SmartRE(f'(\\()(?P<:Whitespace>{WSP_RE__})', '/\\(/ ~')), Alternative(Series(Interpretamente, Option(NBVerweise)), Series(Klassifikation, Option(NBVerweise))), Option(SmartRE(f'(\\))(?P<:Whitespace>{WSP_RE__})', '/\\)/ ~'))), Series(Option(ZW), EINSCHUB_ANFANG, Option(ZW), Alternative(Series(Interpretamente, Option(NBVerweise)), Series(Klassifikation, Option(NBVerweise))), Option(ZW), EINSCHUB_ENDE, Option(ZW)), Einschub, Series(Option(ZW), Option(Series(Text(";"), wsp__)), Option(ZW), Belege), Series(Option(ZW), Option(Series(Text(";"), wsp__)), Option(ZW), Zusatz)))
    Bedeutungsangabe = Alternative(SmartRE(f'(?:(?P<:Text>OHNE)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>:)(?P<:Whitespace>{WSP_RE__}))?)', '"OHNE" [":"]'), Series(NegativeLookahead(Series(Option(Series(Text("*"), wsp__)), AutorWerk)), Alternative(Series(Alternative(Interpretamente, Klassifikation), Option(Nebenbedeutungen)), Einschub), Option(SmartRE(f'(\\n)(?P<:Whitespace>{WSP_RE__})', '/\\n/ ~')), Series(Text(":"), wsp__), Lookahead(Alternative(BelegeOderAufzählung, Anschluss)), mandatory=2))
    VerweisErgänzung = Series(AddeAngabe, Option(Zähler), Option(Bedeutungsangabe), Belege)
    ExemplarText = Synonym(EINZEILER)
    Exemplar = Interleave(Alternative(Sperrung, ExemplarText), Alternative(Zusatz, Verweise, Einschub), repetitions=[(1, 1), (0, INFINITE)])
    AufzählungsPunkt = Series(ZWW, Series(Text("AUFZÄHLUNG"), wsp__), Option(LZ), Exemplar, Series(Text(":"), wsp__), BelegPosition, mandatory=3)
    Anhänger = Series(ZWW, Series(Text("ANHÄNGER"), wsp__), Option(LZ), Bedeutungsangabe, BelegPosition, mandatory=3)
    uuuuu_mit_warnung = Series(Text("UNTER_UNTER_UNTER_UNTER_UNTER_BEDEUTUNG"), wsp__)
    uuuu_mit_warnung = Series(Text("UNTER_UNTER_UNTER_UNTER_BEDEUTUNG"), wsp__)
    FehlendeBelege = Lookahead(Anschluss)
    Aufzählung = OneOrMore(AufzählungsPunkt)
    hic_non_tractatur = Series(ZWW, Series(Text("HIC NON"), wsp__), Alternative(SmartRE(f'(?:(?P<:Text>TRACTATUR)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"TRACTATUR" ["."]'), SmartRE(f'(?:(?P<:Text>TRACTANTUR)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"TRACTANTUR" ["."]'), Series(Custom(ERR("TRACTATUR oder TRACTANTUR erwartet!")), RegExp('.*?(?=\\n)'))))
    ABSATZ = Series(ZWW, Series(Text("ABSATZ"), wsp__))
    VERBOTENER_ABSATZ = Synonym(ABSATZ)
    U5Bedeutung = Series(ZWW, Alternative(Series(Text("UUUUU_BEDEUTUNG"), wsp__), Series(Text("U5_BEDEUTUNG"), wsp__), uuuuu_mit_warnung), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    U4Bedeutung = Series(ZWW, Alternative(Series(Text("UUUU_BEDEUTUNG"), wsp__), Series(Text("U4_BEDEUTUNG"), wsp__), uuuu_mit_warnung), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, OneOrMore(Series(Option(VERBOTENER_ABSATZ), U5Bedeutung)), FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    U3Bedeutung = Series(ZWW, SmartRE(f'(?P<:Text>UUU_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>UNTER_UNTER_UNTER_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>U3_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})', '"UUU_BEDEUTUNG"|"UNTER_UNTER_UNTER_BEDEUTUNG"|"U3_BEDEUTUNG"'), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, OneOrMore(Series(Option(VERBOTENER_ABSATZ), U4Bedeutung)), FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    U2Bedeutung = Series(ZWW, SmartRE(f'(?P<:Text>UU_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>UNTER_UNTER_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>U2_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})', '"UU_BEDEUTUNG"|"UNTER_UNTER_BEDEUTUNG"|"U2_BEDEUTUNG"'), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, OneOrMore(Series(Option(VERBOTENER_ABSATZ), U3Bedeutung)), FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    U1Bedeutung = Series(ZWW, SmartRE(f'(?P<:Text>U_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>UNTER_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>U1_BEDEUTUNG)(?P<:Whitespace>{WSP_RE__})', '"U_BEDEUTUNG"|"UNTER_BEDEUTUNG"|"U1_BEDEUTUNG"'), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, OneOrMore(Series(Option(ABSATZ), U2Bedeutung)), FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    Bedeutung = Series(ZWW, Series(Text("BEDEUTUNG"), wsp__), Option(LZ), Bedeutungsangabe, Alternative(BelegeOderAufzählung, OneOrMore(Series(Option(ABSATZ), U1Bedeutung)), FehlendeBelege), Lookahead(Anschluss), mandatory=3)
    BedeutungsPosition = OneOrMore(Series(Option(ABSATZ), Bedeutung))
    Ergänzung = Series(Option(Zusatz), OneOrMore(Interleave(EINZEILER, Zusatz, repetitions=[(1, 1), (0, 1)])))
    Lemma = Synonym(_Lemma)
    ER4 = Capture(RegExp(' +'), zero_length_warning=False)
    ER3 = Capture(RegExp(' +'), zero_length_warning=False)
    ER2 = Capture(RegExp(' +'), zero_length_warning=False)
    ER1 = Capture(RegExp(' *'), zero_length_warning=False)
    ZWS = OneOrMore(SmartRE(f'(?P<:Whitespace>{WSP_RE__})(\\n)', '~ /\\n/'))
    TAB_4 = Series(ZWS, Retrieve(ER1), Retrieve(ER2), Retrieve(ER3), Retrieve(ER4))
    TAB_3 = Series(ZWS, Retrieve(ER1), Retrieve(ER2), Retrieve(ER3))
    TAB_2 = Series(ZWS, Retrieve(ER1), Retrieve(ER2))
    TAB_1 = Series(ZWS, Retrieve(ER1))
    fehlende_Belege = RegExp('(?=\\n)')
    Variante = Series(NegativeLookahead(KATEGORIENZEILE), Beschreibung, Series(Text(":"), wsp__), Alternative(Series(Option(LZ), Belege), Zusatz, fehlende_Belege), mandatory=3)
    Varianten = Series(OneOrMore(Series(TAB_4, Variante)), Option(Lookahead(Pop(ER4, match_func=optional_last_value))))
    Unterkategorie = Series(TAB_3, Beschreibung, Series(Text(":"), wsp__), Alternative(Varianten, fehlende_Belege))
    Unterkategorien = Series(OneOrMore(Alternative(Series(TAB_3, Variante), Unterkategorie)), Option(Lookahead(Pop(ER3, match_func=optional_last_value))))
    KVarianten = OneOrMore(Series(TAB_3, Variante))
    Kategorie = Series(TAB_2, Beschreibung, Series(Text(":"), wsp__), Alternative(Unterkategorien, KVarianten, fehlende_Belege))
    Kategorien = Series(OneOrMore(Alternative(Series(TAB_2, Variante), Kategorie)), Option(Lookahead(Pop(ER2, match_func=optional_last_value))))
    Besonderheit = Series(TAB_1, Beschreibung, Series(Text(":"), wsp__), Alternative(Kategorien, Series(Option(LZ), Belege), Zusatz, fehlende_Belege), mandatory=3)
    Besonderheiten = Series(Besonderheit, ZeroOrMore(Besonderheit), Option(Lookahead(Pop(ER1, match_func=optional_last_value))))
    Position = Alternative(Lookahead(Series(ZWW, GROSSBUCHSTABEN)), Series(Lookahead(ZWW), Besonderheiten, Lookahead(Series(ZWW, GROSSBUCHSTABEN)), mandatory=0))
    VerwechselungsPosition = Series(ZWW, SmartRE(f'(?P<:Text>VERWECHSELBARKEIT)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>VERWECHSELBAR)(?P<:Whitespace>{WSP_RE__})', '"VERWECHSELBARKEIT"|"VERWECHSELBAR"'), Position)
    DeponensPosition = Series(ZWW, Series(Text("DEPONENS"), wsp__), Position)
    MetrikPosition = Series(ZWW, Series(Text("METRIK"), wsp__), Position)
    GebrauchsPosition = Series(ZWW, Series(Text("GEBRAUCH"), wsp__), Position)
    FormPosition = Series(ZWW, Series(Text("FORM"), wsp__), Position)
    StrukturPosition = Series(ZWW, Series(Text("STRUKTUR"), wsp__), Position)
    SchreibweisenPosition = Series(ZWW, Series(Text("SCHREIBWEISE"), wsp__), Position)
    ArtikelKopf = Interleave(SchreibweisenPosition, StrukturPosition, FormPosition, GebrauchsPosition, MetrikPosition, DeponensPosition, VerwechselungsPosition, repetitions=[(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1)])
    ECKIGE_ZU_KURSIV = Series(Text("]"), wsp__)
    ECKIGE_AUF_KURSIV = Series(Text("["), wsp__)
    erschlossen = Series(Text("+"), wsp__)
    fehlerhaftes_vet = OneOrMore(SmartRE(f'(?P<:Text>inf\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__})', '"inf."|"vet."'))
    fehlerhafte_Sprachangabe = SmartRE(f'(?P<:Text>graec\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>griech\\.)(?P<:Whitespace>{WSP_RE__})', '"graec."|"griech."')
    EtymologieSprache = Series(Alternative(SmartRE(f'(?:(?P<:Text>anglosax\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"anglosax." ["vet."]'), Series(Text("arab."), wsp__), SmartRE(f'(?:(?P<:Text>bohem\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"bohem." ["vet."]'), Series(Text("byz."), wsp__), Series(Text("catal."), wsp__), Series(Text("dan."), wsp__), Series(Text("finnice"), wsp__), SmartRE(f'(?:(?P<:Text>franc\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__})|(?:(?P<:Text>inf\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__})))?)', '"franc." ["vet."|"inf." "vet."]'), SmartRE(f'(?:(?P<:Text>francog\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"francog." ["vet."]'), Series(Text("francoprov."), wsp__), Series(Text("frisic."), wsp__), Series(Text("gall."), wsp__), Series(Text("germ."), wsp__), Series(Text("got."), wsp__), SmartRE(f'(?:(?P<:Text>gr\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>byz\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"gr." ["byz."]'), Series(Text("hebr."), wsp__), Series(Text("hibern."), wsp__), SmartRE(f'(?:(?P<:Text>hisp\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"hisp." ["vet."]'), Series(Text("hung."), wsp__), Series(Text("ital."), wsp__), Series(Text("lombard."), wsp__), Series(Text("langob."), wsp__), SmartRE(f'(?:(?P<:Text>occ\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"occ." ["vet."]'), Series(Text("pers."), wsp__), SmartRE(f'(?:(?P<:Text>polon\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"polon." ["vet."]'), Series(Text("port."), wsp__), Series(Text("prov."), wsp__), Series(Text("raetoroman."), wsp__), SmartRE(f'(?:(?P<:Text>saxon)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"saxon" ["vet."]'), Series(Text("sard."), wsp__), Series(Text("sicil."), wsp__), Series(Text("slav."), wsp__), SmartRE(f'(?:(?P<:Text>theod\\.)(?P<:Whitespace>{WSP_RE__}))(?:(?:(?P<:Text>inf\\.)(?P<:Whitespace>{WSP_RE__}))?)(?:(?:(?P<:Text>vet\\.)(?P<:Whitespace>{WSP_RE__}))?)', '"theod." ["inf."] ["vet."]'), Series(Text("turc."), wsp__), Series(Text("val."), wsp__), fehlerhafte_Sprachangabe), Option(fehlerhaftes_vet))
    VerweisLemmazeile = Series(Option(LemmaNr), ZielLemma, Option(dot), Option(EinzelVerweis))
    SekundärliteraturBlock = Series(ZeroOrMore(Zusatz), Option(SmartRE(f'(?P<:Text>\\*)( *)', '`*` / */')), ZeroOrMore(Zusatz), Option(ZW), Alternative(Sekundärliteratur, Verweise), ZeroOrMore(Series(Option(ZWW), Series(Text("*"), wsp__), ZeroOrMore(Zusatz), Option(ZW), Alternative(Sekundärliteratur, Verweise))))
    TLLVerweis = Series(Option(Series(Text("*"), wsp__)), ZeroOrMore(Zusatz), SmartRE(f'(?=(?:ThLL\\.)(?:{WSP_RE__})|(?:DuC\\.)(?:{WSP_RE__}))', '&"ThLL."|"DuC."'), Sekundärliteratur)
    Etymon = Series(Option(LemmaNr), Interleave(nicht_klassisch_verweis, erschlossen, nicht_gesichert, repetitions=[(0, 1), (0, 1), (0, 1)]), OneOrMore(ETYMOLOGIE_TEXT), Option(Anker))
    _Überleitung = Alternative(ZLL, Series(Alternative(KOMMA, SEMIKOLON), Option(ZWW)), Lookahead(Text("{")))
    EtymologieAngabe = Alternative(TLLVerweis, Series(Alternative(Series(Option(unsicher), Option(Zusatz), Option(ZWW), Alternative(Series(EtymologieSprache, Option(Etymon)), Etymon)), Zusatz), Option(Series(Zusatz, Lookahead(Alternative(Series(Text(":"), wsp__), _Überleitung, RegExp('\\s*$'))))), Option(Series(Series(Text(":"), wsp__), Option(ZWW), Alternative(SekundärliteraturBlock, Series(ECKIGE_AUF_KURSIV, SekundärliteraturBlock, ECKIGE_ZU_KURSIV)), mandatory=1))))
    _zsu = Alternative(Zusatz, unsicher)
    VERBOTENER_PUNKT = Series(Text("."), wsp__)
    verb_klasse = Series(SmartRE(f'(?P<:Text>IV)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>III)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>II)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>I)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>anormal)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>unbestimmt)(?P<:Whitespace>{WSP_RE__})', '"IV"|"III"|"II"|"I"|"anormal"|"unbestimmt"'), Option(VERBOTENER_PUNKT))
    nom_klasse = Series(SmartRE(f'(?P<:Text>IV)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>V)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>III)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>II)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>I\\-II)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>I)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>anormal)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>unbestimmt)(?P<:Whitespace>{WSP_RE__})', '"IV"|"V"|"III"|"II"|"I-II"|"I"|"anormal"|"unbestimmt"'), Option(VERBOTENER_PUNKT))
    _vel_qualifikator = SmartRE(f'(?P<:Text>raro)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>rarius)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>semel)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>bis)(?P<:Whitespace>{WSP_RE__})', '"raro"|"rarius"|"semel"|"bis"')
    ET_VERBOTEN = Series(Text("et"), wsp__)
    VEL_FEHLER = Alternative(Series(Series(Text("VEL"), wsp__), Option(LL), _vel_qualifikator), Series(NegativeLookbehind(RegExp(' *\\n')), Series(Text("VEL"), wsp__)))
    _vel = Series(Alternative(Series(Alternative(Series(Text("vel"), wsp__), ET_VERBOTEN), Option(LL), Option(_vel_qualifikator)), VEL_FEHLER), Option(Alternative(Series(Text("("), wsp__, Verweise, Text(")"), wsp__, mandatory=3), Zusatz)))
    vel = Alternative(Series(Series(Text("{"), wsp__), _vel, Series(Text("}"), wsp__), mandatory=2), _vel)
    numerus = SmartRE(f'(?P<:Text>singular)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>sg\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>sing\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>plural)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>pl\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>plur\\.)(?P<:Whitespace>{WSP_RE__})', '"singular"|"sg."|"sing."|"plural"|"pl."|"plur."')
    casus = SmartRE(f'(?P<:Text>nominativus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>nom\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>genitivus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>gen\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>dativus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>dat\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>accusativus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>acc\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>ablativus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>abl\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>vocativus)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>voc\\.)(?P<:Whitespace>{WSP_RE__})', '"nominativus"|"nom."|"genitivus"|"gen."|"dativus"|"dat."|"accusativus"|"acc."|"ablativus"|"abl."|"vocativus"|"voc."')
    _casus_nummerus = Alternative(Series(casus, Option(numerus)), Series(Series(Text("{"), wsp__), casus, Option(numerus), Series(Text("}"), wsp__), mandatory=3))
    genus = SmartRE(f'(?P<:Text>maskulinum)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>m\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>masc\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>femininum)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>f\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>fem\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>neutrum)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>n\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>neutr\\.)(?P<:Whitespace>{WSP_RE__})', '"maskulinum"|"m."|"masc."|"femininum"|"f."|"fem."|"neutrum"|"n."|"neutr."')
    genera = Series(Option(numerus), genus, ZeroOrMore(Series(Option(Series(Text(","), wsp__)), Option(LL), vel, Option(LL), Option(Zusätze), Option(LL), genus, mandatory=6)))
    wa_ergänzung = OneOrMore(Series(NegativeLookahead(Series(Option(ZW), vel)), Option(ZW), Alternative(EINZEILER, Zusatz, Verweise)))
    indeclinabile = SmartRE(f'(?P<:Text>indecl\\.)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>indeclinabile)(?P<:Whitespace>{WSP_RE__})', '"indecl."|"indeclinabile"')
    wortart = Alternative(Series(Text("adverbium"), wsp__), Series(Text("adv."), wsp__), Series(Series(Text("adiectivum"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("adi."), wsp__), nom_klasse, mandatory=1), Series(Series(Text("comparativus"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("compar."), wsp__), nom_klasse, mandatory=1), Series(Series(Text("coniunctio"), wsp__), Option(wa_ergänzung)), Series(Series(Text("coni."), wsp__), Option(wa_ergänzung)), Series(Series(Text("interiectio"), wsp__), Option(wa_ergänzung)), Series(Series(Text("interi."), wsp__), Option(wa_ergänzung)), Series(Series(Text("littera"), wsp__), Option(wa_ergänzung)), Series(Series(Text("numerale"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("numer."), wsp__), nom_klasse, mandatory=1), Series(Text("particula"), wsp__), Series(Text("praepositio"), wsp__), Series(Text("praep."), wsp__), Series(Series(Text("pronomen"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("pron."), wsp__), nom_klasse, mandatory=1), Series(Series(Text("substantivum"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("subst."), wsp__), nom_klasse, mandatory=1), Series(Series(Text("superlativus"), wsp__), nom_klasse, mandatory=1), Series(Series(Text("superl."), wsp__), nom_klasse, mandatory=1), Series(Series(Text("verbum"), wsp__), verb_klasse, mandatory=1))
    wortarten = Series(wortart, ZeroOrMore(Series(Option(Series(Text(","), wsp__)), Option(LL), vel, Option(LL), Option(Zusätze), Option(LL), wortart, mandatory=6)))
    VERBOTENES_KOMMA = Series(Text(","), wsp__)
    FLEX = OneOrMore(Series(NegativeLookahead(Alternative(genus, wortart, casus, numerus, vel)), RegExp('-?[a-z]+(?:\\([a-z]+\\)[a-z]+)*'), wsp__))
    flexion = Alternative(indeclinabile, Series(FLEX, ZeroOrMore(Series(Series(Text(","), wsp__), FLEX))))
    stammformen = Series(flexion, ZeroOrMore(Series(vel, flexion)))
    GVariante = Series(Option(_zsu), Lookahead(Alternative(wortarten, casus, flexion, genus, numerus)), Interleave(Series(wortarten, Option(_zsu), Option(ABS)), Series(_casus_nummerus, Option(_zsu)), Series(stammformen, Option(_zsu), Option(ABS)), Series(genera, Option(_zsu), Option(ABS)), repetitions=[(0, 1), (0, 1), (0, 1), (0, 1)]))
    GrammatikAngabe = Series(Alternative(Series(wortarten, Lookahead(ABS), mandatory=1), WA_UNBEKANNT), Option(Series(ABS, Option(_casus_nummerus), stammformen)), Option(Series(Option(Alternative(ABS, VERBOTENES_KOMMA)), genera)))
    GrammatikVariante = Series(GVariante, Option(Series(Series(Text(":"), wsp__), Belege, mandatory=1)))
    GrammatikVarianten = Alternative(Series(Series(Text("("), wsp__), GrammatikVariante, ZeroOrMore(Series(LABS, GrammatikVariante)), Series(Text(")"), wsp__)), OneOrMore(Series(LABS, GrammatikVariante)))
    Grammatik = Series(GrammatikAngabe, ZeroOrMore(Series(Option(LL), vel, Option(LL), Option(Series(Zusätze, Option(LL))), GrammatikAngabe)), mandatory=0)
    GrammatikPosition = Series(ZWW, Series(Text("GRAMMATIK"), wsp__), Option(LZ), Grammatik, Option(GrammatikVarianten), ZeroOrMore(Series(Option(LL), Zusatz)), mandatory=2)
    VERBOTENE_GRAMMATIKPOSITION = Synonym(GrammatikPosition)
    verwiesen_auf = Series(Option(LemmaNr), Option(nicht_klassisch_verweis), LemmaWort)
    VerweisPosition = Series(ZWW, Series(Text("VERWEISE"), wsp__), ZeroOrMore(Series(Interleave(LZ, Series(Text(","), wsp__), repetitions=[(0, 1), (0, 1)]), Alternative(Verweise, verwiesen_auf, Zusatz))), Option(Series(Text("."), wsp__)))
    LemmaVariante = Series(RegExp('\\-?'), _lemma_text, Option(Series(Option(LL), Zusatz)))
    LemmaVarianten = Series(Series(Text("("), wsp__), LemmaVariante, ZeroOrMore(Series(Series(Text(","), wsp__), Option(ZW), LemmaVariante)), Series(Text(")"), wsp__))
    EtymologiePosition = Series(ZWW, Series(Text("ETYMOLOGIE"), wsp__), Alternative(Lookahead(Series(ZWW, GROSSBUCHSTABEN)), Series(Option(LZ), EtymologieAngabe, ZeroOrMore(Series(_Überleitung, EtymologieAngabe)), SmartRE(f'(?=(?:{WSP_RE__})(?:\\n))', '&~ /\\n/'), mandatory=1)))
    _LemmaStück = Series(Option(Series(LemmaNr, NegativeLookahead(unberechtigt), mandatory=1)), Option(LZ), ZeroOrMore(Series(Zusatz, Option(LL))), Lemma, Option(Series(KLAMMER_AUF, LemmaWort, KLAMMER_ZU)), Option(Zusatz))
    _AusgangsLemma = Series(_LemmaStück, ZeroOrMore(Series(KOMMA, _LemmaStück)))
    AusgangsLemmata = Series(_AusgangsLemma, ZeroOrMore(Series(ET, _AusgangsLemma)))
    verbotener_Punkt = Series(Text("."), wsp__)
    LemmaBlock = Series(Option(LZ), ZeroOrMore(Series(Zusatz, Option(LL))), Option(LemmaNr), Lemma, Interleave(Series(Option(LL), Zusatz), Series(Option(LL), LemmaVarianten), repetitions=[(0, 1), (0, 1)]), Option(verbotener_Punkt), Option(Alternative(GrammatikPosition, EtymologiePosition)), mandatory=3)
    SubLemmaPosition = Series(Series(Text("SUB_LEMMA"), wsp__), LemmaBlock, ZeroOrMore(Series(Option(LZ), Series(Text("VEL"), wsp__), LemmaBlock)), Lookahead(Series(ZeroOrMore(SmartRE(f'(\\s+)(?P<:Whitespace>{WSP_RE__})', '/\\s+/ ~')), RegExp('[A-Z][A-Z][A-Z]'))), mandatory=3)
    unecht = Series(Text("UNECHT"), wsp__)
    LemmaPosition = Series(Option(ABS), Series(Text("LEMMA"), wsp__), Option(unecht), LemmaBlock, ZeroOrMore(Series(Option(LZ), Series(Text("VEL"), wsp__), LemmaBlock)), Option(GrammatikPosition), Lookahead(Alternative(Series(ZeroOrMore(SmartRE(f'(\\s+)(?P<:Whitespace>{WSP_RE__})', '/\\s+/ ~')), RegExp('[A-Z][A-Z][A-Z]')), _DATEI_ENDE)), mandatory=6)
    ZielLemmaBlock = Series(VerweisLemmazeile, ZeroOrMore(Series(ET, VerweisLemmazeile)), Alternative(VerweisErgänzung, Series(Option(Series(NegativeLookahead(VLZ_Fehler), Zusatz)), Option(Series(Text(":"), wsp__, Option(LZ), Option(Zähler), Option(Bedeutungsangabe), Belege)))))
    Ziffer = SmartRE(f'(\\d+\\.)(?P<:Whitespace>{WSP_RE__})', '/\\d+\\./ ~')
    NachtragsLemma = Series(NegativeLookahead(Series(Text("p."), wsp__)), Option(Series(LemmaNr, Lookbehind(RegExp('\\s*\\.')))), _Lemma)
    _nachtrags_bezug = Alternative(Series(NachtragsLemma, Option(NachtragsStelle), SmartRE(f'(?=[:;])', '&/[:;]/')), Series(NachtragsStelle, SmartRE(f'(?=[:;])', '&/[:;]/')))
    post = Series(Series(Text("post"), wsp__), _nachtrags_bezug, mandatory=1)
    ad = Series(Series(Text("ad"), wsp__), _nachtrags_bezug, mandatory=1)
    Nachtrag = Series(Option(Series(ad, Series(Text(";"), wsp__))), post, Series(Text(":"), wsp__), Option(LZ), Option(Ziffer), mandatory=2)
    NachtragsPosition = Series(Series(Text("NACHTRAG"), wsp__), Nachtrag, Lookahead(Series(ZeroOrMore(SmartRE(f'(\\s+)(?P<:Whitespace>{WSP_RE__})', '/\\s+/ ~')), Series(Text("LEMMA"), wsp__))), mandatory=1)
    UnterArtikel = Series(ZWW, SubLemmaPosition, Option(EtymologiePosition), ArtikelKopf, BedeutungsPosition, Option(VerweisPosition), mandatory=2)
    _ArtikelTeile = Series(Option(EtymologiePosition), Option(VERBOTENE_GRAMMATIKPOSITION), ArtikelKopf, Option(BedeutungsPosition), Option(VerweisPosition), ZeroOrMore(UnterArtikel))
    Vide = Series(Option(LL), VIDE, Option(LL), Alternative(EinzelVerweis, Series(Zusatz, Lookahead(Vide_Anschluss)), ZielLemmaBlock), Lookahead(Vide_Anschluss), mandatory=3)
    PRAETER = Alternative(Series(Text("PRAETER"), wsp__), PRAETER_Fehler)
    VerweisBlock = Series(Option(VERBORGEN), AusgangsLemmata, Vide, Option(VerweisErgänzung))
    ist_Verweisartikel = Alternative(Series(Option(LZ), Ueberschrift), Series(Option(LZ), Series(Text("LEMMA"), wsp__), Alternative(VerweisBlock, unberechtigt)))
    PraeterBlock = Series(Option(LL), PRAETER, Alternative(Series(Alternative(Series(Option(Alternative(VERBINDE, TRENNE)), VerweisBlock), Series(unberechtigt, Option(Alternative(VERBINDE, TRENNE)))), ZeroOrMore(Series(Option(LZ), NegativeLookahead(Series(Text("LEMMA"), wsp__)), Alternative(VerweisBlock, unberechtigt), Option(Alternative(VERBINDE, TRENNE))))), Lookahead(ArtikelVerfasser)), mandatory=2)
    _VerweisBlöcke = OneOrMore(Alternative(Series(Option(LZ), Alternative(Series(Series(Text("LEMMA"), wsp__), Alternative(VerweisBlock, unberechtigt), ZeroOrMore(PraeterBlock), mandatory=1), FehlerhafterVerweis), Option(Alternative(VERBINDE, TRENNE))), Series(Option(LZ), Ueberschrift)))
    VerweisArtikel = Series(Lookahead(ist_Verweisartikel), Option(Ueberschrift), _VerweisBlöcke, ArtikelVerfasser, Option(Direktiven), Option(LZ), mandatory=3)
    Artikel = Alternative(VerweisArtikel, Series(Option(LZ), Option(Ueberschrift), Option(LZ), Option(NachtragsPosition), OneOrMore(LemmaPosition), Alternative(hic_non_tractatur, _ArtikelTeile), Option(ArtikelVerfasser), Option(Stellenverzeichnis), Option(Direktiven), Option(LZ), mandatory=5))
    VARIANTE.set(Series(wsp__, ZeroOrMore(Alternative(SmartRE(f'([^":\\n/{{}}()]+)(?P<:Whitespace>{WSP_RE__})', '/[^":\\n\\/{}()]+/ ~'), RegExp('\\((?!\\()'), RegExp('\\)(?!\\))'), Zusatz)), VARIANTEN_ABSCHLUSS))
    FREITEXT.set(OneOrMore(Alternative(TEXTELEMENT, ZITAT, SATZZEICHEN, ZW, KLAMMER_AUF, KLAMMER_ZU, GROSSSCHRIFT, GRÖßER_ZEICHEN, KLEINER_ZEICHEN)))
    TEXTELEMENT.set(Alternative(ROEMISCHE_ZAHL, DEU_WORT, DEU_GEMISCHT, GRI_WORT, LAT_GWORT, SEITENZAHL, HOCHGESTELLT, TIEFGESTELLT, SPEZIAL, Series(SONDERZEICHEN, wsp__), Series(UNICODE, wsp__), AUSGEWICHEN))
    TIEFGESTELLT.set(Alternative(Series(SmartRE(f'(?P<:Text>\\{{_)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>_\\{{)(?P<:Whitespace>{WSP_RE__})', '"{_"|"_{"'), FREITEXT, Series(Text("}"), wsp__)), SmartRE(f'(?:(?P<:Text>_)(?P<:Whitespace>{WSP_RE__}))([\\w.]+)(?P<:Whitespace>{WSP_RE__})', '"_" /[\\w.]+/ ~')))
    HOCHGESTELLT.set(Alternative(Series(SmartRE(f'(?P<:Text>\\{{\\^)(?P<:Whitespace>{WSP_RE__})|(?P<:Text>\\^\\{{)(?P<:Whitespace>{WSP_RE__})', '"{^"|"^{"'), FREITEXT, Series(Text("}"), wsp__)), SmartRE(f'(?:(?P<:Text>\\^)(?P<:Whitespace>{WSP_RE__}))([\\w.]+)(?P<:Whitespace>{WSP_RE__})', '"^" /[\\w.]+/ ~')))
    Verweise.set(OneOrMore(Series(Option(Series(Text(","), wsp__)), Option(ZW), EinzelVerweis)))
    Ueberlieferungstraeger = Series(Series(Text("rec."), wsp__), Alternative(GRI_WORT, LAT_GWORT), Option(Alternative(Zusatz, Verweise)))
    opus_minus.set(Series(Series(Text("(("), wsp__), Option(ZW), om_kern, ZeroOrMore(Series(Series(Text("*"), wsp__), om_kern_ungeprüft)), Series(Text("))"), wsp__)))
    opus.set(Series(Lookahead(Series(Text("(("), wsp__)), Alternative(opus_minus, Einschub), mandatory=1))
    om_erwartet.set(Series(Lookahead(Series(Text("(("), wsp__)), Alternative(om_ungeprüft, Einschub), mandatory=1))
    Stelle.set(Alternative(Series(Lookahead(sim_ibid), FesterZusatz), Ueberlieferungstraeger, Series(Option(fehler_fraglich), Stellenabschnitt, Option(Series(Series(Text("|"), wsp__), Alternative(Stellenabschnitt, Series(KeinAbschnitt, wsp__)))))))
    BelegKern.set(ZeroOrMore(Alternative(MEHRZEILER, BelegLemma, Verweise, Zusatz, Sperrung, Junktur, Kursiv, opus, Series(KLAMMER_AUF, BelegKern, KLAMMER_ZU), Series(erschlossen, ZielLemmaWort))))
    BelegText.set(Series(RegExp('"'), wsp__, BelegKern, RegExp('"'), wsp__, mandatory=3))
    Nachschlagewerk.set(Alternative(Series(Werkname, Option(ZW), Option(Anker), Option(ZW), Band, Option(Erscheinungsjahr)), Series(Werkname, Option(Erscheinungsjahr), Option(ZW), Option(Anker))))
    Einschub.set(Series(Option(ZW), EINSCHUB_ANFANG, Option(ZW), Alternative(Ueberlieferungstraeger, Series(Interleave(Series(Verweise, Option(ZW)), Series(Zusatz, Option(ZW)), Series(BelegText, Option(ZW)), repetitions=[(0, 1), (0, 1), (0, 1)]), ZeroOrMore(Belege))), Option(ZW), EINSCHUB_ENDE, mandatory=3))
    Junktur.set(Alternative(Series(Series(Text("<<"), wsp__), BelegKern, Series(Text(">>"), wsp__), mandatory=1), Series(Series(Text("˻"), wsp__), BelegKern, Series(Text("˼"), wsp__), mandatory=1)))
    Kursiv.set(Series(Series(Text("{/"), wsp__), OneOrMore(Alternative(FREITEXT, Lemmawort, Series(Option(erschlossen), ZielLemmaWort), Verweise)), Series(Text("}"), wsp__), mandatory=2))
    Sperrung.set(Series(Series(Text("{!"), wsp__), OneOrMore(Alternative(FREITEXT, Lemmawort, Zusatz)), Series(Text("}"), wsp__), mandatory=2))
    Zusatz.set(OneOrMore(Alternative(FesterZusatz, VariaLectioZusatz, FreierZusatz, Anker)))
    Zusätze.set(Alternative(OneOrMore(Series(Option(ZW), Alternative(Zusatz, Verweise))), Series(KLAMMER_AUF, OneOrMore(Series(Option(ZW), Alternative(Zusatz, Verweise))), KLAMMER_ZU)))
    DeutscheBedeutung.set(Series(DeutscherAusdruck, ZeroOrMore(Series(Series(Text(","), wsp__), DeutscherAusdruck))))
    LateinischeBedeutung.set(Series(LateinischerAusdruck, ZeroOrMore(Series(Series(Text(","), wsp__), LateinischerAusdruck))))
    BelegeOderAufzählung.set(Alternative(Series(BelegPosition, ZeroOrMore(Anhänger)), Aufzählung))
    Beschreibung.set(Series(SmartRE(f'(?![A-Z][A-Z_][A-Z_]+)', '!/[A-Z][A-Z_][A-Z_]+/'), OneOrMore(Interleave(Alternative(Series(nicht_klassisch_verweis, ZielLemmaWort), EINZEILER, Lemmawort), Zusatz, repetitions=[(1, 1), (0, INFINITE)])), ZeroOrMore(Series(Series(Text(";"), wsp__), Ergänzung)), ZeroOrMore(Einschub), Lookahead(Series(Text(":"), wsp__))))
    unberechtigt.set(Series(Series(Text("["), wsp__), VerweisBlock, Series(Text("]"), wsp__), mandatory=2))
    _Mehrere_Artikel = Series(OneOrMore(Artikel), _DATEI_ENDE, mandatory=1)
    resume_rules__ = {'Ueberschrift': [re.compile(r'(?=LEMMA|$)')],
                      '_VerweisBlöcke': [re.compile(r'(?=\nAUTOR|$)')],
                      'VerweisBlock': [re.compile(r'(?=LEMMA|AUTOR|$)')],
                      'Vide': [re.compile(r'(?=\nAUTOR|[A-Z][A-Z][A-Z]|\]|$)')],
                      'SubLemmaPosition': [re.compile(r'(?=\nETYMOLOGIE|\nSCHREIBWEISE|\nSTRUKTUR|\nGEBRAUCH|\nMETRIK|\nVERWECHSELBAR|\nBEDEUTUNG)')],
                      'NachtragsPosition': [re.compile(r'(?=\nLEMMA)')],
                      'LemmaPosition': [re.compile(r'(?=\n\s*ETYMOLOGIE|\n\s*SCHREIBWEISE|\n\s*STRUKTUR|\n\s*GEBRAUCH|\n\s*METRIK|\n\s*VERWECHSELBAR|\n\s*BEDEUTUNG)')],
                      'GrammatikAngabe': [re.compile(r'(?=\n)')],
                      'wortart': [re.compile(r'(?=(?:[-:;\n{]|vel|raro|rarius|semel))')],
                      'genera': [re.compile(r'\.|um\n')],
                      'GrammatikVariante': [re.compile(r'(?=\n(?=(?:[^\n]*:)|(?:\s*\n)))')],
                      'EtymologiePosition': [re.compile(r'(?=\nSCHREIBWEISE|\nSTRUKTUR|\nGEBRAUCH|\nMETRIK|\nVERWECHSELBAR|\nBEDEUTUNG)')],
                      'EtymologieAngabe': [re.compile(r'(?=\n(?!\s*\*))')],
                      'Position': [re.compile(r'(?=\n *[A-Z][A-Z][A-Z])')],
                      'Beschreibung': [re.compile(r'(?=:)')],
                      'Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'U1Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER_B|\n *U_B|\n *U1_B|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'U2Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER_B|\n *U_B|\n *U1_B|\n *UNTER_UNTER_B|\n *UU_B|\n *U2_B|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'U3Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER_B|\n *U_B|\n *U1_B|\n *UNTER_UNTER_B|\n *UU_B|\n *U2_B|\n *UNTER_UNTER_UNTER_B|\n *UUU_B|\n *U3_B|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'U4Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER_B|\n *U_B|\n *U1_B|\n *UNTER_UNTER_B|\n *UU_B|\n *U2_B|\n *UNTER_UNTER_UNTER_B|\n *UUU_B|\n *U3_B|\n *UUUU_B|\n *U4_B|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'U5Bedeutung': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER_B|\n *U_B|\n *U1_B|\n *UNTER_UNTER_B|\n *UU_B|\n *U2_B|\n *UNTER_UNTER_UNTER_B|\n *UUU_B|\n *U3_B|\n *UUUU_B|\n *U4_B|\n *UUUUU_B|\n *U5_B|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'Anhänger': [re.compile(r'(?=\n *BEDEUTUNG|\n *ABSATZ|\n *UNTER|\n *U_|\n *UU|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'AufzählungsPunkt': [re.compile(r'(?=\n *BEDEUTUNG|\n *UNTER|\n *U_|\n *UU|\n *AUF|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'Bedeutungsangabe': [re.compile(r'(?=\n *BEDEUTUNG|\n *UNTER|\n *U_|\n *UU|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'FreierZusatz': [re.compile(r'(?=(?<=}).|\s*\n(?:\s*\n)*.|$)')],
                      'Belege': [re.compile(r'(?=\)\)|\*|\n *BEDEUTUNG|\n *UNTER|\n *U_|\n *UU|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|$)')],
                      'Einschub': [re.compile(r'\)\)')],
                      'Beleg': [re.compile(r'(?=\s*\*|\n *U_|\n *UU|\n *UNTER|\n *BEDEUTUNG|\n *ANHÄNGER|\n *SUB_LEMMA|\n *VERWEISE|\n *AUTOR|\)\)|\s*\n(?:\s*\n)+.|$)')],
                      'BelegText': [re.compile(r'(?=\s*\n(?:\s*\n)+.)'),
                                    re.compile(r'\)\)'),
                                    re.compile(r'\(\('),
                                    re.compile(r'(?<=")')],
                      'Lemmawort': [re.compile(r' |\n')],
                      'VerweisKern': [re.compile(r'(?=\})')],
                      'Anker': [re.compile(r'\}[ \t]*'),
                                re.compile(r'\n[ \t]*')],
                      'Verweis': [re.compile(r'(?=;|\})')]}
    skip_rules__ = {'Artikel': [re.compile(r'(?=\n\s*[A-Z][A-Z][A-Z]+)'),
                                re.compile(r'$')],
                    'VerweisArtikel': [re.compile(r'(?=$)')],
                    '_VerweisBlöcke': [re.compile(r'(?=LEMMA|$)')],
                    'unberechtigt': [re.compile(r'\]')],
                    'VerweisBlock': [re.compile(r'(?=\w|\n|$)')],
                    'Vide': [re.compile(r'(?=ET|\nAUTOR|[A-Z][A-Z][A-Z]|\]|$)')],
                    'Grammatik': [re.compile(r'[^\w.-] *')],
                    'EtymologieAngabe': [re.compile(r'(?=\n(?!\s*\*))|(?=\n\s*\*)')],
                    'Bedeutungsangabe': [re.compile(r'(?=:)')],
                    'AutorWerk': [re.compile(r'(?=\)\)|;|\s\*)|\s*\n(?:\s*\n)+.')]}
    root__ = _Mehrere_Artikel
        
parsing: PseudoJunction = create_parser_junction(MLWGrammar)
get_grammar = parsing.factory # for backwards compatibility, only


#######################################################################
#
# AST SECTION - Can be edited. Changes will be preserved.
#
#######################################################################

from typing import List, AbstractSet


WSP = 'L'      # whitespace tag
TEXT = 'TEXT'  # plain text


def is_wsp(context) -> bool:
    return context[-1].name in ('L', WHITESPACE_PTYPE)


def normalize_wsp_children(context):
    """Normalisiere alle :Whitespace-Kinder innerhalb des Teilbaums."""
    for node in context[-1].children:
        if is_wsp([node]):
            node.result = ' '


@transformation_factory(collections.abc.Set)
def show(context, tags: AbstractSet[str]=frozenset()):
    if not tags or context[-1].name in tags:
        print(context[-1].as_sxpr(""))

def show_parent(path):
    if len(path) > 1:
        print(path[-2].as_sxpr(""))

def show_context(context):
    print([nd.name for nd in context])


EBENEN_LISTE = ['Bedeutung', 'U1Bedeutung', 'U2Bedeutung',
                'U3Bedeutung', 'U4Bedeutung', 'U5Bedeutung']


def unter_artikel(path) -> Node:
    """Gibt die Wurzel des aktuellen Artikels oder - im Falle eines Kontextes
    innterhalb eines Sub-Lemmas - Unterartikels zurück."""
    for node in path[::-1]:
        if node.name in ("UnterArtikel", "Artikel"):
            return node
    # This does not work when testing snippets that do not represent full articles:
    # assert context[0].name == "Artikel"
    return path[0]  # kommt nur beim Testen vor: Kein (Unter-)Artikelknoten


def merke_bedeutungsebene(context):
    """
    Schreibt die maximale Ebenenetiefe eines Bedeutungsblocks in ein
    Attribut des wurzel-Knotens.
    """
    ebenen_bezeichnung = context[-1].name
    tiefe = EBENEN_LISTE.index(ebenen_bezeichnung)
    attrs = unter_artikel(context).attr
    if int(attrs.setdefault('unterbedeutungstiefe', '0')) < tiefe:
        attrs['unterbedeutungstiefe'] = str(tiefe)


@transformation_factory(collections.abc.Callable)
def only_child(context: List[Node], condition: collections.abc.Callable) -> bool:
    node = context[-1]
    assert len(node.children) == 1, node.as_sxpr(compact=True)
    return condition(context + [node.children[0]])


def assert_has_siblings(context: List[Node]):
    """Prüfe, dass noch weitere Knoten mit derselben Bezeichnung
    als Nachbarn vorhanden sind.
    (Es muss mindestens zwei Unterbedeutungen geben.)
    """
    if len(context) < 2:
        # kommt nur in Grammatik-Tests der Unterbedeutungsposition vor
        # und sollte, da nur ein Ausschnitt getestet wird, nicht mit
        # einem Fehler quittiert werden.
        return
    node = context[-1]
    tag_name = node.name
    parent = context[-2]
    for sibling in parent.children:
        if sibling.name == tag_name:
            break
    else:
        context[0].new_error(node, "Vereinzelte %s Unterbedeutung!" % tag_name)


# NAMED_WHITESPACE = MockParser('L', WHITESPACE_PTYPE)

def name_whitespace(context: List[Node]):
    node = context[-1]
    if node.name == WHITESPACE_PTYPE and node.content:
        node.name = WSP  # NAMED_WHITESPACE.name


drop_whitespace = remove_children_if(is_wsp)


def merge_token_and_whitespace(context: List[Node]):
    node = context[-1]
    result = []
    for child in node.children:
        if child.name in (TOKEN_PTYPE, WHITESPACE_PTYPE):
            if result and result[-1].name == TOKEN_PTYPE:
                result[-1].result += child.result
            else:
                child.name = TOKEN_PTYPE
                result.append(child)
        else:
            result.append(child)


def reduce_Stelle(ctx: List[Node]):
    """Faster special code for:
        apply_unless(reduce_single_child, lambda ctx: len(ctx[-1].children) == 1 and
                     ctx[-1].children[0].name not in ('Kursiv', 'HOCHGESTELLT', 'ROEMISCHE_ZAHL'))
    """
    node = ctx[-1]
    if len(node.children) == 1:
        child = node.children[0]
        if child.name not in ('Kursiv', 'HOCHGESTELLT', 'ROEMISCHE_ZAHL', 'GRI_WORT', 'Verweis'):
            node.result = child.result
            update_attr(node, (child,), cast(RootNode, ctx[0]))
            if child.has_attr():
                node._attributes = child._attributes


move_whitespace = partial(move_fringes, condition=is_one_of({WHITESPACE_PTYPE, WSP}))
# FALSCH!!! : move_whitespace = partial(move_fringes, is_one_of({WHITESPACE_PTYPE, WSP}))
# lstrip = partial(lstrip, condition=is_one_of({WHITESPACE_PTYPE, WSP}))


def normalisiere_Bedeutungsangabe(context: List[Node]):
    """Normalisiert die Angabe der lateinischen order deutschen Bedetung. Bedeutungen
    werden immer durch ein Komma gefolgt von einem Leerzeichen getrennt."""
    node = context[-1]
    assert node.children
    normalized = [node.children[0]]
    for child in node.children[1:]:
        if child.name not in ('TEXT', 'L', 'ZW'):
            normalized.extend([Node('TEXT', ', ', True).with_pos(child.pos)])
                               # Node('L', ' ', True).with_pos(child.pos + 4)])
            normalized.append(child)
    node.result = tuple(normalized)


HINWEIS_DOPPELSTRICH_FEHLT = ErrorCode(20)
WARNUNG_UUUU_VERWENDEN = ErrorCode(210)
WARNUNG_UUUUU_VERWENDEN = ErrorCode(211)
WARNUNG_VARL_SCHREIBWEISE = ErrorCode(221)
WARNUNG_FRAGEZEICHEN_SEMIKOLON = ErrorCode(222)
WARNUNG_DATIERUNG_ALS_STELLENANGABE = ErrorCode(223)
WARNUNG_FALSCH_ABGEKUERZTER_AUTORNAME = ErrorCode(225)

FEHLER_ABGETRENNTER_IBID_ZUSATZ = ErrorCode(5510)
FEHLER_UNBEKANNTE_WORTART = ErrorCode(5520)
FEHLER_UNECHTE_BUCHSTABEN = ErrorCode(5530)
FEHLER_FALSCHES_SCHLÜSSELWORT = ErrorCode(5540)
FEHLER_FALSCHES_TRENNZEICHEN = ErrorCode(5550)
FEHLER_VEL_GROSSGESCHRIEBEN = ErrorCode(5560)

ERROR_SYMBOL = '😢'  # '🙁', '☠'
WARN_SYMBOL = '🤔'  # '😔'
SEARCH_SYMBOL = '😯'


@transformation_factory(str)
def mark_error(context: List[Node], message: str, code: int):
    node = context[-1]
    message = message.format(content=node.content)
    add_error(context, message, code)
    if is_error(code):
        node.attr['davor'] = ERROR_SYMBOL
        node.attr['class'] = "error"
        node.attr['title'] = message.replace('"', "'")


def ist_Fremdautor(context: List[Node]):
    """Gibt `True` zurück, falls es der letzte Knoten in der Liste ein Autor-Knoten ist,
    und der Inhalt des Knotens nicht in der Menge der Autoren aus der opera-majora-
    Tabelle steht."""
    node = context[-1]
    autor = re.sub(r'\(.*?\)|\[.*?\]', '', node.content).strip()  # .restrip('. ')
    # print(author, author in opera_majora_autoren)
    if autor not in opera:
        variante = autor.rstrip('.') if autor[-1:] == '.' else (autor + '.')
        if variante in opera:
            cast(RootNode, context[0]).new_error(node, f'Wurde der Autorname richtig geschrieben? '
                f'Autor "{autor}" ist nicht in der opera-Liste, "{variante}" aber schon!?',
                WARNUNG_FALSCH_ABGEKUERZTER_AUTORNAME)
    return (node.name == 'Autor' and autor not in opera)


# TODO: Konsolidiere merge_adjacent(is_one_of(TEXT, WSP), TEXT), normalize_wsp_children etc

RX_AUTOR = re.compile(r'(?!LXX)(\w+[^\w]*)(\w+[^\w]*)?(\w+[^\w]*)?(\w+[^\w]*)?(\w+[^\w]*)?')

def uncapitalize(context: List[Node]):
    node = context[-1]
    if node.children:
        for nd in node.children:
            if nd.name != 'ROEMISCHE_ZAHL' and not nd.has_attr('roemische_zahl'):
                uncapitalize(context + [nd])
    else:
        m = RX_AUTOR.match(node.content)
        if m:
            namen = [name[0] + name[1:].lower() for name in m.groups() if name]
            node.result = ''.join(namen)


def lower(context: List[Node]):
    node = context[-1]
    if node.children:
        for nd in node.children:
            lower(context + [nd])
    else:
        node.result = node.content.lower()


def beseitige_Ausweichzeichen(context):
    """Beseitigt das Ausweichzeichen "\\", d.h. aus "\\#" wird einfach "#". """
    node = context[-1]
    assert node.name == "AUSGEWICHEN"
    pure_content = node.content[1:]
    node.result = pure_content
    if pure_content in ('(', ')', '[', ']', '{', '}'):
        node.name = "Erzwungene_Klammer"


def Internalisiere_XML(context):
    """Beseitigt das öffnende und schließende XML-tag eines XML-Bereichs und
    Bennent den Knoten in den Tag-Namen um.
    """
    node = context[-1]
    # print(node.as_sxpr())
    node.name = node["TAG_NAME"][0].content
    node.attr['xml'] = "internalisiert"
    del node["TAG_NAME"]


SPATIUM = node_maker('L', ' ')  # type: Callable
SPATIUM_1 = node_maker('L', ' ', {'spatium': '1'})     # type: Callable
SPATIUM_2 = node_maker('L', '  ', {'spatium': '2'})    # type: Callable
SPATIUM_3 = node_maker('L', '   ', {'spatium': '3'})   # type: Callable
SPATIUM_3_1 = node_maker('L', '    ', {'spatium': '3+1'})  # type: Callable
SPATIUM_4 = node_maker('L', '    ', {'spatium': '4'})  # type: Callable
SPATIUM_8 = node_maker('L', '            ', {'spatium': '8'})  # type: Callable
PARAGRAPH = node_maker('p', '\n')                      # type: Callable


@transformation_factory(int, tuple, collections.abc.Callable)
def convert_spatia(context: List[Node], positions: PositionType, spatium: str):
    """Konvertiert einfachen Leerraum (L-Knoten ohne attribute) vor (!) den
    gegebenen Positionen in ein spatium fesgelegter Größe (die explizit
    im Attribut 'spatium' angegeben wird.). Mögliche Werte für den
    `spatium`-Parameter sind: '1', '2', '3', '3+1', '4', '8'.
    """
    node = context[-1]
    pos_tuple = tuple(pos - 1 for pos in normalize_position_representation(context, positions))
    if not pos_tuple:
        return
    children = node.children
    if spatium == '3+1':
        whitespaces = '    '
    else:
        i = int(spatium)
        assert i in (1,2,3,4,8)
        whitespaces = ' ' * i
    assert max(pos_tuple) < len(children), \
        "Position %i exceeds number of children in %s" % (max(pos_tuple), node.as_sxpr())
    for pos in pos_tuple:
        L = children[pos]
        # assert L.name == 'L', "No L-tag: " + L.as_sxpr()
        if L.name != 'L':
            if L.name != "PRAETER":
                context[0].new_error(L, 'L-node exptected, not ' + L.name)
        else:
            assert not L.has_attr('spatium'), "Node %s already has a spatium-attribute" % L.as_sxpr()
            L.attr['spatium'] = spatium
            L.result = whitespaces
    # Eliminiere doppelte Spatien
    pred_set = {i - 1 for i in pos_tuple
                if i >= 1 and children[i].name == children[i - 1].name == 'L'}
    for i in node.indices('VERBINDE'):
        if i >= 1 and children[i - 1].name == 'L':
            pred_set.add(i - 1)
    # for i in node.indices('PRAETER'):
    #     if i < len(children) - 1 and children[i + 1].name == 'L':
    #         pred_set.add(i + 1)
    node.result = tuple(children[i] for i in range(len(children)) if i not in pred_set)


@transformation_factory(int, tuple, collections.abc.Callable)
def füge_spatien_ein(context: Path, position: PositionType, node_factory: Callable):
    """Inserts a delimiter at a specific position within the children. If
    `position` is `None` nothing will be inserted. Position values greater
    or equal the number of children mean that the delimiter will be appended
    to the tuple of children.

    Example::

        insert(pos_of('paragraph'), node_maker('LF', '\\n'))
    """
    pos_tuple = normalize_position_representation(context, position)
    if not pos_tuple:
        return
    node = context[-1]
    children = list(node._children)
    assert children or not node.result, "Cannot add nodes to a leaf-node!"
    L = len(children)
    pos_tuple = sorted(tuple((p if p >= 0 else (p + L)) for p in pos_tuple), reverse=True)
    for n in pos_tuple:
        n = min(L, n)
        text_pos = (children[n-1].pos + children[n-1].strlen()) if n > 0 else node.pos
        if n == 0 or children[n - 1].name not in ('L', 'P', 'LB'):
            children.insert(n, node_factory().with_pos(text_pos))
        elif children[n - 1].name == 'L':
            children[n - 1] = node_factory() # .with_pos(children[n - 1].pos)
    node.result = tuple(children)


def verweis_abgrenzer_positionen(context: List[Node]) -> Tuple[int]:
    node = context[-1]
    children = node.children
    positions = []
    try:
        start_index = node.index('Verweis') + 1
    except ValueError:
        return ()
    for i in range(start_index, len(children)):
        if children[i].name == "Verweis":  # and children[-1].name == "Verweis"
            positions.append(i)
    return tuple(positions)


TextAuszeichnungen = {'HOCHGESTELLT', 'TIEFGESTELLT', 'ROEMISCHE_ZAHL', 'GRI_WORT', 'Kursiv', 'Sperrung',
                      'Junktur', 'Anker', 'Abkuerzung', 'Verweis', 'ZITAT', 'BelegLemma',
                      'Erzwungene_Klammer'}


@transformation_factory(str)
def lstrip_str(ctx: List[Node], s: str):
    node = ctx[-1]
    assert not node.children
    node.result = node.content.lstrip(s)


def evaluate_unicode(ctx: List[Node]):
    """Replaces unicode the command (e.g. \u65938) by its unicode character (e.g. 𐆒)."""
    code = int(ctx[-1].content[2:])
    ctx[-1].result = chr(code)


# def keep_longest(s: Sequence[str]) -> str:
#     longest = '';  L = 0
#     for ws in s:
#         if len(ws) > L:
#             longest = ws
#             L = len(ws)
#     return longest
#
#
# @transformation_factory(collections.abc.Set)
# def merge_whitespace(context: Path,
#                      tokens: AbstractSet[str] = {WHITESPACE_PTYPE},
#                      merge_rule: Callable[[Sequence[str]], str] = keep_longest):
#
#     collapse_children_if(context, is_one_of(tokens))

#
# def FremdAutorFix(ctx: List[Node]):
#     node = ctx[-1]
#     try:
#         k = node.index('Autorangabe')
#         autorangabe = node[k]
#         i = node.index('Werk')
#         werk = node[i]
#         if not werk.children and werk.content.find(',') >= 0 \
#                 and not (''.join(nd.content for nd in node[:i])).find(',') >= 0:
#             pass
#
#     except KeyError, IndexError:
#         pass

    # try:
    #     autor = node['Autor']
    #     if ist_Fremdautor(ctx + [autor]):
    #         werk = node['Werk']
    #         if werk.content.find(',') >= 0:
    #             for i in range(len(werk.children)):
    #                 if werk[i].content.strip()[-1:] == ',':
    #                     break
    #             wcnt = werk[i].content
    #             k = wcnt.find(', ')
    #             werk[i].result = wcnt[:k]
    #             komma = Node('TEXT', wcnt[k:]).with_pos(werk[i].pos + k)
    #             for w  in werk[:i + 1]:
    #                 w.name = 'AUTORNAME'
    #             autor.result =  autor.children + werk[:i + 1]
    #             werk.result = werk[i +1:]
    #             k = node.index('Autor') + 1
    #             node.result = node[:k] + komma + node[k:]
    # except KeyError:
    #     pass

#
# def AbsatzNachÜberschrift(path: List[Node]):
#     node = path[-1]
#     for i in node.indices('Ueberschrift'):
#         if i < len(node.children) - 1:
#             L = node.children[i + 1]
#             if L.name == 'L':
#                 L.name = 'p'
#                 L.result = '\n'
#                 if L.has_attr('spatium'):
#                     del L.attr['spatium']


MLW_AST_transformation_table = {
    # AST Transformations for the MLW-grammar
    "<": [# apply_if(show_context, lambda ctx: ctx[-1].name == "HOCHGESTELLT" and ctx[-1].content.startswith("^v")),
          remove_children_if(lambda ctx: (is_empty(ctx) and (is_anonymous(ctx) or is_wsp(ctx))) or
                                         (is_one_of(ctx, {'LZ', 'DPP', 'COMMENT__', 'ABS'})) or
                                         (is_token(ctx) and is_anonymous(ctx)
                                          and not has_ancestor(ctx, {
                                                     'Name', 'wortart', 'wortarten', "Nachschlagewerk",
                                                     'EtymologieSprache', 'genus', 'genera', 'unsicher',
                                                     'DeutscheBedeutung', 'flexion', 'erschlossen',
                                                     'LateinischeBedeutung', 'Nachtrag', 'LAT_WORT',
                                                     'FesterZusatz', 'casus', 'numerus', 'LAT_GWORT',
                                                     'genus', 'nicht_klassisch', 'ziel', 'pfad', 'SPEZIAL',
                                                     'ET_Fehler', 'PRAETER_Fehler', 'VIDE_Fehler', 'Athetese',
                                                     'VIDE', 'ET', 'PRAETER', 'KOMMA', 'SEMIKOLON',
                                                     'ZITAT_ENDE', 'ECKIGE_AUF_KURSIV', 'ECKIGE_ZU_KURSIV',
                                                     'Überleitung', 'fraglich', 'idem_qui', 'vel', 'ZusatzInhalt',
                                                     'Autorangabe', 'op_cit_werk', 'op_cit_autor_werk', 'AUTORANFANG',
                                                     'Verfasser', 'AddeAngabe', 'hic_non_tractatur',
                                                     'Ueberlieferungstraeger'
                                                 }, 1000))),
          # apply_if(show, lambda ctx: ctx[-1].name == "HOCHGESTELLT")
          ],
    "unsicher": [move_whitespace, reduce_single_child],
    "nicht_klassisch": [move_whitespace, reduce_single_child,
                        apply_ifelse(replace_content_with('✲'),   # sechszackig dünn (Schneeflocke)
                                     replace_content_with('✱'),   # sechszackig fett
                                     has_ancestor({'Etymon', 'VerweisPosition', 'ZielLemma', 'Vide'}))],
    "nicht_klassisch_verweis": [remove_whitespace, reduce_single_child,
                                replace_content_with('✲')],  # sechzackig dünn (Schneeflocke)
    "erschlossen": [move_whitespace, reduce_single_child, replace_content_with('☆')],  # fünfzackig umriss
    "AutorWerk":
        [apply_if(reduce_single_child, has_child('AW_Verborgen')),
         flatten],
    "AW_Verborgen": [add_attributes({'class': 'versteckt', 'versteckt': '1'})],
    "Autorangabe": [merge_adjacent(not_one_of('Anker'), 'DEU_GEMISCHT'),
                    replace_child_names({'DEU_GEMISCHT': 'TEXT'}), reduce_single_child],  # Autorangabe wird für Fremdautoren verwendet!
    "Autor": [apply_if((remove_children('disambig_marker'), change_name('Autorangabe'),
                        add_attributes({'disambiguiert': '1'}), uncapitalize),
                       any_of(has_descendant({'disambig_marker'}), ist_Fremdautor)),
              flatten(lambda ctx: True, False), rstrip(is_empty), normalize_wsp_children,
              move_whitespace, merge_adjacent(is_one_of(TEXT, 'fraglich', WSP), TEXT), reduce_single_child],
    "fraglich": [move_whitespace, reduce_single_child],
    "Werk, Werktitel, Werkname": [flatten(not_one_of('Erscheinungsjahr'), False),
             rstrip(is_empty), normalize_wsp_children, move_whitespace,
             merge_adjacent(is_one_of(":Text", TEXT, WSP), TEXT),
             replace_child_names({':Text': 'TEXT'}), reduce_single_child,
             apply_unless(change_name("Werk"), has_ancestor({'Werk', 'Nachschlagewerk'}))],
    "Nachschlagewerk": [
        collapse_children_if(
            not_one_of(TextAuszeichnungen | {'Band', 'Werkname', 'Erscheinungsjahr', 'L'}),
            'TEXT'),
        reduce_single_child, change_name('Werk'), add_attributes({'nachschlagewerk': '1'})],
    "Band": [move_whitespace, collapse_children_if(not_one_of(TextAuszeichnungen), TEXT),
             merge_adjacent(is_one_of({'TEXT', 'L'}), TEXT),
             reduce_single_child],
    "Erscheinungsjahr": [move_whitespace, move_fringes(lambda ctx: ctx[-1].name == 'TEXT'),
                         reduce_single_child],
    "PUNKT": [change_name('TEXT')],
    "op_cit_autor_werk": [collapse, add_attributes({'werk': 'op_cit_AutorWerk'}), change_name('Werk')],
    "op_cit_werk": [collapse, add_attributes({'werk': 'op_cit_Werk'}), change_name('Werk')],
    "opus, om_erwartet": [move_whitespace, replace_by_single_child],
    "opus_minus, om_ungeprüft": [flatten, flatten(is_one_of('om_kern', 'om_kern_ungeprüft')), move_whitespace,
                                 change_name('opus_minus')],
    "om_kern, om_kern_ungeprüft": [flatten, remove_children('OM_MARKIERUNG')],
    "OM_MARKIERUNG": [],
    "Artikel, UnterArtikel":
        [flatten, remove_children('_DATEI_ENDE'), normalize_wsp_children,
         remove_children_if(lambda ctx: ctx[-1].name in {"ArtikelKopf"} and not ctx[-1].content.strip()),
         apply_if(insert(positions_of('LemmaPosition'), PARAGRAPH),
                  has_descendant({'NachtragsPosition'})),
         insert(positions_of('EtymologiePosition'), SPATIUM_3_1),
         insert(positions_of('ArtikelKopf'), SPATIUM_3_1),
         füge_spatien_ein(positions_of('BedeutungsPosition'), SPATIUM_4),
         insert(positions_of('UnterArtikel'), PARAGRAPH),
         insert(positions_of('VerweisPosition'), SPATIUM_8),
         insert(positions_of('ArtikelVerfasser', 'hic_non_tractatur'), SPATIUM_4),
         collapse_children_if(is_one_of('L'), 'L', pick_longest_content),
         strip(lambda ctx: ctx[-1].name == 'L' and not ctx[-1].get_attr('spatium', ''))],
    "LemmaPosition": [flatten, lstrip(is_wsp), move_whitespace, merge_adjacent(is_one_of('L'), 'L')],
    "SubLemmaPosition": [flatten],
    "verbotener_Punkt": [add_error('Hier sollte kein Punkt stehen.')],
    "NachtragsPosition": [move_whitespace],
    "Nachtrag": [remove_tokens(';'), replace_child_names({':Text': 'TEXT'})],
    "NachtragsStelle": [collapse],
    "ad, post, Ueberlieferungstraeger": [replace_child_names({':Text': 'TEXT'})],
    "ADDE_AD": [replace_content_with('adde ad '), change_name('TEXT')],
    "VerweisErgänzung": [move_whitespace],
    "AddeAngabe": [move_whitespace, traverse_locally({":Text": change_name(TEXT)})],
    "VerweisPosition": [strip],
    "verwiesen_auf": [],
    "VerweisArtikel": [merge_adjacent(is_one_of('L'), 'L'), convert_spatia(positions_of({'VerweisBlock', 'unberechtigt'}), '8'),
                       lstrip(lambda ctx: ctx[-1].name != "VERBORGEN" and contains_only_whitespace(ctx)),
                       remove_children('_DATEI_ENDE'),
                       # insert(positions_of('ArtikelVerfasser'), SPATIUM_4),
                       collapse_children_if(is_one_of('L'), 'L', pick_longest_content)],
    "PraeterBlock": [], # [convert_spatia(positions_of({'VerweisBlock', 'unberechtigt'}), '8')],
    "FehlerhafterVerweis": [add_error('Das "["-Zeichen muss nach dem Schlüsselwort LEMMA stehen!')],
    "unberechtigt": [move_whitespace, insert(0, node_maker('TEXT', '[')),
                     insert(AT_THE_END, node_maker('TEXT', ']'))],
    "AusgangsLemmata": [move_whitespace],
    "VerweisBlock": [move_whitespace, apply_if(
        add_attributes({'unberechtigt': 'leider wahr'}), has_ancestor({'unberechtigt'}))],
    "VERBORGEN": [replace_content_with('')],
    "Lemma": [move_whitespace],
    "unecht": [replace_content_with('')],
    "ZielLemma": [move_whitespace],
    "ZielLemmaBlock": [move_whitespace],
    "VerweisLemmazeile": [move_whitespace, replace_by_children],
    "dot": [move_whitespace, collapse, change_name('TEXT')],
    "Vide": [move_whitespace],
    "VIDE, ET, PRAETER":
        [move_whitespace, add_attributes({'class': 'kursiv'}), reduce_single_child,
         apply_unless(lower, has_descendant({'ET_Fehler', 'PRAETER_Fehler', 'VIDE_Fehler'})),
         apply_if(replace_content_with('v.'), content_matches('vide'))],
    "VIDE_Fehler, ET_Fehler, PRAETER_Fehler":
        [add_error('An dieser Stelle sollte das entsprechende großgeschreibene Schlüsselwort verwendet werden')],
    "LemmaNr": [move_whitespace, reduce_single_child,
                apply_ifelse(add_attributes({'class': 'mediaeval'}),
                             add_attributes({'class': 'fett versal'}),
                             has_ancestor({'VerweisLemmazeile', 'Etymon', 'VerweisPosition'}))],
    "LemmaBlock": [flatten, move_whitespace, normalize_wsp_children,
                   insert(positions_of('EtymologiePosition'), SPATIUM_3_1)],
    # "ErweiterterLemmaBlock": [flatten(lambda ctx: is_anonymous(ctx) or is_one_of(ctx, {'LemmaBlock'})),
    #                           move_whitespace, change_name('LemmaBlock')],
    "nicht_gesichert": [reduce_single_child],
    "LemmaWort, ZielLemmaWort": [flatten, lstrip, normalize_wsp_children, move_whitespace, collapse],
                                 # apply_unless(collapse, has_child('ETFehler'))],
    # "Stichwort" : [],
    "LemmaVariante": [flatten, strip, normalize_wsp_children,
                      collapse_children_if(not_one_of("Zusatz", "FesterZusatz", "L"), TEXT)],
    "LemmaVarianten": [flatten, drop_whitespace],
    "Grammatik": [move_whitespace, reduce_single_child],
    "GrammatikAngabe": [move_whitespace],
    "VERBOTENE_GRAMMATIKPOSITION": [add_error('Die Grammatikangabe sollte vor der Etymologieangabe steheh, es sei denn, es handelt sich um einen Mehrfachansatz!')],
    "GrammatikPosition": [flatten, move_whitespace],
    "GrammatikVarianten": [move_whitespace],
    "GrammatikVariante": [flatten, flatten(is_one_of('Belege')), drop_whitespace],
    "AlternativeGrammatik": [replace_child_names({':RegExp': 'TEXT'})],
    "GVariante": [],
    "wortart": [flatten, move_whitespace, reduce_single_child],
    "wa_ergänzung": [flatten, move_whitespace, reduce_single_child],
    "wortarten": [flatten, move_whitespace, replace_by_single_child, traverse_locally({":Text": change_name(TEXT)})],  # remove_tokens({"et"}),
    "nom_klasse": [flatten, move_whitespace, reduce_single_child,
                   apply_if(replace_content_with('indecl.'), content_matches('indeclinabile'))],
    "verb_klasse": [flatten, move_whitespace, reduce_single_child],
    "stammformen": [move_whitespace, replace_by_single_child],
    "flexion": [flatten, move_whitespace,
                merge_adjacent(is_one_of({'L', ':Text'}), ':Text'),
                replace_child_names({':Text': 'TEXT'})],  # apply_if(reduce_single_child, neg(has_descendant({'indeclinabile'})))],
    "indeclinabile": [move_whitespace, reduce_single_child],
    "FLEX": [flatten, move_whitespace, collapse],
    "genus, numerus, casus": [move_whitespace, reduce_single_child],
    "vel": [remove_tokens({'{', '}'}), move_whitespace,
            apply_ifelse(merge_adjacent(is_one_of({'L', ":Text"}), ":Text"), collapse,
                         has_descendant({'Verweise', 'Verweis', 'Zusatz'})),
            replace_child_names({':Text': 'TEXT'}),
            add_attributes({'class': 'kursiv'})],
    "hic_non_tractatur": [apply_ifelse(replace_content_with('hic non tractatur'),
                                       replace_content_with('hic non tractantur'),
                                       lambda path: path[-1].content.lower().find('tractatur') >= 0)],
    "EtymologiePosition": [strip, flatten],
    "Etymon": [apply_if(reduce_single_child, neg(has_descendant(TextAuszeichnungen))),
               move_whitespace, apply_if(reduce_single_child, has_child('TEXT'))],
    "EtymologieDetail": [flatten, flatten(is_one_of('Belege')), rstrip],
    "EtymologieAngabe, SekundärliteraturBlock": [flatten, move_whitespace,
                         remove_children_if(lambda ctx: ctx[-1].name == ":RegExp"),
                         merge_adjacent(is_one_of('L'), 'L')],  # remove WSP
    "EtymologieSprache": [move_whitespace, collapse, add_attributes({'class': 'kursiv'})],
    "fehlerhafte_Sprachangabe": [add_error('Die Sprachangabe "{content}" ist fehlerhaft oder unbekannt.')],
    "fehlerhaftes_vet": [add_error('Diese Sprachangabe kann nicht mit "{content}" kombiniert werden.')],
    "Überleitung": [],
    "Sekundärliteratur":
        [move_whitespace,
         apply_if(add_attributes({'class': 'kursiv'}), has_ancestor({'EtymologiePosition'}))],
    "ECKIGE_AUF_KURSIV, ECKIGE_ZU_KURSIV":
        [move_whitespace, reduce_single_child, add_attributes({'class': 'kursiv'}),
         change_name('Kursiv')],  # change_name('TEXT')
    "ArtikelKopf": [delimit_children(SPATIUM_3_1)],
    "SchreibweisenPosition, StrukturPosition, VerwechselungsPosition, GebrauchsPosition, FormPosition":
        [collapse_children_if(is_one_of('L'), 'L', pick_longest_content),
         strip, reduce_single_child],
    "Position": [flatten, strip],
    "Kategorien": [flatten, remove_children('L', 'TAB_2'), delimit_children(SPATIUM_3_1),
                   apply_ifelse(insert(0, SPATIUM_3_1), insert(0, SPATIUM),
                                has_descendant({'L'}, 1))],
    "Kategorie": [remove_children('TAB_2'), flatten(is_one_of('Unterkategorien', 'Varianten')),
                  move_whitespace, remove_whitespace],
    "Unterkategorien": [flatten, remove_children('L', 'TAB_3'), delimit_children(SPATIUM_3_1),
                        apply_ifelse(insert(0, SPATIUM_3_1), insert(0, SPATIUM),
                                     has_descendant({'L'}, 1))],
    "Unterkategorie": [remove_children('TAB_3'), flatten(is_one_of('Varianten')),
                       move_whitespace, remove_whitespace],
    "Varianten, KVarianten":
        [flatten, drop_whitespace, remove_children('L', 'TAB_3', 'TAB_4'), delimit_children(SPATIUM_3_1),
         apply_ifelse(insert(0, SPATIUM_3_1), insert(0, SPATIUM), has_descendant({'L'}, 1)),
         change_name('Varianten')],
    "Variante": [flatten(is_one_of('Belege')), strip],
    "fehlende_Belege" : [add_error('Hier fehlen entweder noch Belege oder Unterkategorien oder "{{}}" '
                                   'als Platzhalter, oder eine Beschreibung oder ein Beleg enthält '
                                   'unerlaubte Elemente oder Zeichen.')],
    "Beschreibung": [flatten, # move_fringes(lambda ctx: ctx[-1].name == 'Zusatz'),
                     collapse_children_if(is_one_of('L'), 'L', pick_longest_content), move_whitespace,
                     apply_if(reduce_single_child, lambda ctx: ctx[-1].children[0].name != 'Lemmawort')],
    "Besonderheit": [remove_children('TAB_1'), flatten(is_one_of('Kategorien')),
                     collapse_children_if(lambda ctx: ctx[-1].name == 'L', 'L'),
                     move_whitespace, remove_whitespace],
    "Besonderheiten": [flatten, remove_children('L'), delimit_children(SPATIUM_3_1),
                       apply_if(insert(0, SPATIUM_3_1), has_descendant({'L'}, 1))],
    "BedeutungsPosition": [flatten, remove_tokens("BEDEUTUNG"),
                           füge_spatien_ein(positions_of('Bedeutung'), SPATIUM_4),
                           move_fringes(lambda ctx: ctx[-1].name in ('LB', 'P')),
                           lstrip],
    "ABSATZ": [change_name('P'), replace_content_with('        ')],
    "VERBOTENER_ABSATZ": [change_name('P'),
                          add_error('Absätze sind nur bis zur 2. Unterbedeutungsebene erlaubt.')],
    "Bedeutung": [flatten, flatten(is_one_of('Belege')), strip, remove_empty,
                  merke_bedeutungsebene, normalize_wsp_children,
                  füge_spatien_ein(positions_of('U1Bedeutung'), SPATIUM_4),
                  insert(positions_of('Anhänger'), SPATIUM_3_1),
                  merge_adjacent(is_one_of({'L'}), 'L')],
    "Anhänger": [flatten, strip],
    "BelegeOderAufzählung": [replace_by_children],
    "Aufzählung": [],
    "AufzählungsPunkt": [strip],
    "Exemplar": [],
    "ExemplarText": [change_name('Sperrung'), reduce_single_child],
    "U1Bedeutung, U2Bedeutung, U3Bedeutung, U4Bedeutung, U5Bedeutung":
        [flatten, strip, assert_has_siblings, merke_bedeutungsebene,
         flatten(is_one_of('AutorWerk', 'Belege')),
         insert(positions_of('U2Bedeutung', 'U3Bedeutung', 'U4Bedeutung', 'U5Bedeutung', 'Aufzählung'), SPATIUM_4),
         insert(positions_of('Anhänger'), SPATIUM_3_1),
         merge_adjacent(is_one_of({'L'}), 'L')],  # NEU!!!
    "uuuu_mit_warnung": [error_on(lambda ctx: True, "Verwende besser die Kurzform UUUU_BEDEUTUNG!",
                                  WARNUNG_UUUU_VERWENDEN)],
    "uuuuu_mit_warnung": [error_on(lambda ctx: True, "Verwende besser die Kurzform UUUUU_BEDEUTUNG!",
                                  WARNUNG_UUUUU_VERWENDEN)],
    "Bedeutungsangabe": [flatten, traverse_locally({":Text, :RegExp": change_name("TEXT")}),
                         remove_if(content_matches('OHNE')), move_whitespace],  # [normalize_wsp_children, flatten],
    "Nebenbedeutungen, NBEinschub":
        [flatten, remove_children('EINSCHUB_ANFANG', 'EINSCHUB_ENDE'), move_whitespace, normalize_wsp_children],
    "NBVerweise": [normalize_wsp_children, move_whitespace],
    "Klassifikation": [flatten, move_whitespace], # collapse_children_if(is_one_of('Kategorienangabe'), PlainText),
    "Kategorienangabe":
        [error_on(lambda ctx: ctx[-1].content.find(' - ') >= 0,
                  'Falls es sich hier um die Angabe einer lateinischen und deutschen Bedetung '
                  'handelt, sollte dazwischen kein einfacher, sondern ein Doppelstrich " -- " '
                  'stehen.', HINWEIS_DOPPELSTRICH_FEHLT),
         replace_by_single_child, move_whitespace, merge_adjacent(is_one_of(TEXT, WSP), TEXT),
         apply_if(replace_by_children, is_one_of('Kategorienangabe'))],
    "Interpretamente": [flatten, remove_children('InterpretamentTrenner')],
    "InterpretamentTrenner": [],
    "LateinischeBedeutung": [flatten, traverse_locally({":Text": change_name("TEXT")}),
                             normalisiere_Bedeutungsangabe],
    "DeutscheBedeutung": [flatten, move_whitespace, traverse_locally({":Text": change_name("TEXT")}),
                          normalisiere_Bedeutungsangabe],
    "LateinischerAusdruck, DeutscherAusdruck, idem_qui":
        [flatten, move_whitespace, # strip(is_wsp),
         merge_connected(is_one_of('LateinischesWort', 'DeutschesWort', 'TEXT'), is_one_of(WSP), TEXT, 'L'),
         replace_or_reduce(has_descendant({'GRI_WORT', 'ZITAT', 'DeutscheKlammer', "LateinischeKlammer", 'BelegText'}))
         ],
    'iq_markierung': [collapse],
    "LateinischerBestandteil, DeutscherBestandteil": [move_whitespace, replace_by_single_child],
    "LateinischesWort, DeutschesWort, LateinischeEllipse, DeutscheEllipse":
        [move_whitespace, apply_if(collapse, neg(has_descendant({'GRI_WORT', 'ZITAT'}))),
         replace_or_reduce(has_descendant({'GRI_WORT', 'ZITAT'})),
         apply_if(change_name('TEXT'), has_ancestor({'idem_qui'}))],
        # [move_whitespace, apply_if(collapse, lambda ctx: "GRI_WORT" not in ctx[-1]),
        #  replace_or_reduce(lambda ctx: "GRI_WORT" not in ctx[-1])],
    'LAT_WÖRTER': [move_whitespace, replace_by_children],
    'DEU_ZITAT, LAT_ZITAT':
        [move_whitespace, apply_if(collapse, neg(has_descendant({'GRI_WORT'}))), change_name('ZITAT')],
    "Qualifizierung": [move_whitespace, collapse],
    "DeutscheKlammer, LateinischeKlammer": [move_whitespace],
    "BelegPosition": [move_whitespace, replace_by_single_child],
    "FehlendeBelege": [add_error('Hier fehlen noch die Belege!')],
    "Belege": [flatten, lstrip, replace_by_single_child, merge_adjacent(is_one_of(WSP)),
               normalize_wsp_children, move_whitespace],
    "Beleg": [flatten, remove_empty, move_whitespace,
              apply_if(replace_by_single_child, has_child({'FesterZusatz', 'Zusatz'}))],
        # error_on(
        # lambda ctx: len(ctx[-1].children) == 1 and (ctx[-1].children[0].name.endswith('Zusatz')),
        # 'Belege dürfen nicht nur aus einem Zusatz bestehen. Stelle ggf. den Zusatz einem '
        # 'nachfolgenden Beleg voran. Beispiele: "{{adde}} ANTIDOT. Berlin.; app. p. 77,30", "* {{adde}} imperator"')],
    "BelegText": [flatten, remove_content('"'), move_whitespace,
                  apply_unless(reduce_single_child, has_child(TextAuszeichnungen)),
                  move_whitespace,
                  merge_adjacent(is_one_of({WSP}), 'L'), normalize_wsp_children,
                  collapse_children_if(is_one_of({TEXT, WSP}), TEXT),
                  normalize_wsp_children, remove_empty,
                  apply_unless(reduce_single_child, has_child(TextAuszeichnungen))],
    "BelegStelle": [flatten, move_whitespace, normalize_wsp_children],
    "BelegErgänzung": [flatten, move_whitespace],
    "Stellenangabe": [move_whitespace],
    "DATIERUNG": [flatten, move_whitespace,
                  collapse_children_if(not_one_of(TextAuszeichnungen), 'TEXT'),
                  merge_adjacent(is_one_of(TEXT, WSP), TEXT),
                  reduce_single_child, change_name('Datierung')],
                  # traverse_locally({":RegExp": change_name(TEXT)})],
    "Datierung, AnnoDatierung":
        [flatten, move_whitespace, collapse_children_if(not_one_of(TextAuszeichnungen), 'TEXT'),
         reduce_single_child, change_name('Datierung')],
    # "AnnoStelle": [change_name('Stelle'),
    #                add_error('Stellenangabe besteht nur aus einer Datierung!? Ist das beabsichtigt?',
    #                          WARNUNG_DATIERUNG_ALS_STELLENANGABE)],
    "DOPPEL_SEM": [replace_content_with('; '), change_name(TEXT)],
    "OM_FALSCHE_ZUSATZANGABE": [add_error('Dies ist keine der erlaubten Zusatzangaben zu einem opus minus.')],
    "DATIERUNG_FEHLER": [add_error('Nach dem Punkt zu Anfang der Datierung muss ein Leerzeichen folgen!', ERROR)],
    "VEL_FEHLER": [add_error('Bei alternativen Flexionen, Wortarten, Genera wird "vel" kleingeschrieben. '
                             'Das großgeschriebene VEL leitet dagegen einen alternativen Lemma-Block ein.',
                             FEHLER_VEL_GROSSGESCHRIEBEN)],
    "BelegKern": [flatten(lambda ctx: is_anonymous(ctx) or is_one_of(ctx, {'MEHRZEILER', 'FREITEXT'}))],
    "tmesi": [replace_content_with('')],
    "Lemmawort":
        [flatten, remove_tokens('#', '{#', '}', '#~', '{#~'), rstrip(is_empty),
         move_whitespace,
         apply_if(add_attributes({'tmesi': ''}), has_descendant({'tmesi'})),
         collapse_children_if(lambda ctx: ctx[-1].name not in ("GRI_WORT", "L", "ZOMBIE__"), TEXT),
         apply_if(collapse, lambda ctx: "GRI_WORT" not in ctx[-1] and "ZOMBIE__" not in ctx[-1]),
         apply_if((collapse, change_name('ZOMBIE__')), lambda ctx: 'ZOMBIE__' in ctx[-1])],  # collapse],
    "BelegLemma": [move_whitespace,
                   apply_ifelse(reduce_single_child, replace_by_single_child,
                                lambda ctx: 'ZOMBIE__' not in ctx[-1])],
    "Quellenangabe, Quelle":
        [merge_adjacent(is_one_of('L'), 'L'),
         flatten(lambda ctx: is_anonymous(ctx) or is_one_of(ctx, {'AutorWerk'})),
         remove_empty, move_whitespace, normalize_wsp_children],  # neu: move_whitespace
    "ZusätzeUndQuerverweise": [move_whitespace, replace_by_children],
    "Zusatz": [replace_by_children],
    "Zusatzzeilen": [error_on(lambda ctx: ctx[-1].content.find('ibid.') >= 0,
                              'Zusätze mit "ibid." dürfen nicht durch eine Leerzeile '
                              'von dem Beleg getrennt werden, zu dem sie gehören!',
                              FEHLER_ABGETRENNTER_IBID_ZUSATZ),
                     replace_by_children],
    "FreierZusatz": [flatten, remove_tokens('{', '}'), move_whitespace,
                     reduce_single_child, change_name('Zusatz')],
    "FesterZusatz": [remove_tokens('{', '}'), move_whitespace, apply_if(
        collapse, neg(has_descendant(TextAuszeichnungen)))],
    "var_l": [apply_unless(add_error('"var. l." sollte genau so, d.h. mit Leerzeichen geschrieben werden', WARNUNG_VARL_SCHREIBWEISE),
                           content_matches(r'var\. l')),
              replace_content_with("var. l")],
    "VariaLectioZusatz": [remove_tokens('{', '}'), move_whitespace, apply_if(
        collapse, neg(has_descendant(TextAuszeichnungen))), apply_if(
        change_name('Zusatz'), has_ancestor({'LemmaBlock'}))],
    "ZusatzInhalt": [apply_if(reduce_single_child, neg(has_descendant(TextAuszeichnungen))),
                     replace_child_names({':Text': TEXT}), merge_adjacent(is_one_of(TEXT), 'TEXT')],
    "Sperrung, Kursiv, Junktur": [move_whitespace, remove_tokens,
        apply_unless(reduce_single_child, has_child('Lemmawort', 'Zusatz', 'Verweise', 'GRI_WORT'))],
    # "Kursiv": [move_whitespace, remove_tokens, add_attributes({'class': 'kursiv'}),
    #     apply_unless(reduce_single_child, has_child('Lemmawort', 'Zusatz', 'Verweise', 'GRI_WORT'))],
    "ArtikelVerfasser": [strip],
    "Stellenverzeichnis": [],
    "Verweisliste": [flatten],
    "Stellenverweis": [flatten],
    "Name": [move_whitespace, collapse],
    "Stelle": [move_whitespace, apply_unless(reduce_single_child, has_child('FesterZusatz', 'Ueberlieferungstraeger')),
               apply_if(replace_by_single_child, has_child('Ueberlieferungstraeger'))],
    "fehler_fraglich": [collapse, add_error('Möglicherweise wurde ein Semikolin falsch gesetzt. Die "(?)"-Markierung gehört normalerweise zur Autorangabe!', WARNUNG_FRAGEZEICHEN_SEMIKOLON)],
    "Stellenabschnitt": [flatten(name_matches(r':.*|STELLENKERN|STELLENTEXT')),
                         move_whitespace,
                         collapse_children_if(not_one_of(TextAuszeichnungen), TEXT),
                         reduce_Stelle],
    "KeinAbschnitt": [replace_content_with(''), change_name('Stellenabschnitt')],
    "OriginalStelle": [reduce_single_child],
    "STELLENKERN": [flatten, move_whitespace, replace_by_single_child],
    "STELLENTEXT": [flatten, move_whitespace, replace_by_single_child],
    "TEIL_SATZZEICHEN, SATZZEICHEN, GRÖßER_ZEICHEN, KLEINER_ZEICHEN":
        [move_whitespace, reduce_single_child, change_name(TEXT),
         transform_result(lambda content: {'`': '‘', '´': '’'}.get(content, content))],
    "Verweise": [flatten(any_of({is_anonymous, is_one_of({'Verweise'})})),
                 move_fringes(lambda ctx: ctx[-1].content in ('(', ')'), merge=False),
                 move_whitespace, remove_children('L'),
                 traverse_locally({":Text": change_name(TEXT)}),
                 insert(verweis_abgrenzer_positionen, node_maker('L', ' ')),
                 merge_adjacent(lambda ctx: ctx[-1].name in {'TEXT', 'L'}, TEXT),
                 move_fringes(is_one_of({'TEXT', 'Zusatz'}))],
                 # collapse_children_if(is_one_of(':Text', 'TEXT', 'L'), TEXT)],
    "Verweis": [flatten, remove_tokens, move_whitespace, move_fringes(is_one_of({'TEXT', 'Zusatz'}))],
    "Textzusatz": [remove_tokens, collapse, change_name('Zusatz')],
    "EinzelVerweis": [flatten, remove_tokens, move_whitespace, change_name('Verweise')],
    "VerweisKern": [flatten, remove_tokens, lstrip, replace_by_children], # drop_whitespace],
    "alias": [strip, collapse_children_if(not_one_of(TextAuszeichnungen), TEXT),
              merge_adjacent(lambda ctx: ctx[-1].name == 'TEXT'), reduce_single_child],
    "NullVerweis": [drop_whitespace],
    "pfad, ziel": [drop_whitespace, collapse], # [apply_if(transform_result(lambda s: ''), has_parent("URL"))],
    "Anker": [lstrip, remove_children_if(is_wsp), remove_anonymous_tokens, reduce_single_child],
    "Einschub": [move_whitespace, remove_children({'EINSCHUB_ANFANG', 'EINSCHUB_ENDE'}),
                 strip, flatten, move_whitespace],
    "EINSCHUB_ANFANG, EINSCHUB_ENDE": [move_whitespace],
    "URL": [flatten, keep_nodes('protokoll', 'domäne', 'pfad', 'ziel'), strip],
    "AUTORANFANG, AUTORNAME":
        [flatten, move_whitespace, reduce_single_child, change_name('TEXT')],
    "WERKTITEL_ANFANG, WERKTITEL_FOLGE":
        [flatten, move_whitespace, collapse_children_if(not_one_of(TextAuszeichnungen), 'TEXT'),
         replace_by_single_child, apply_if(change_name('TEXT'), not_one_of(TextAuszeichnungen))],
    "NAMENS_ABKÜRZUNG": [],
    "NAME": [move_whitespace],
    "XML": [Internalisiere_XML],
    "DEU_WORT":
        [reduce_single_child, move_whitespace, reduce_single_child, change_name(TEXT)],
    "DEU_GEMISCHT":
        [reduce_single_child, move_whitespace, collapse],
    "GRI_WORT":
        [replace_child_names({':RegExp': 'TEXT'}), reduce_single_child, move_whitespace,
         reduce_single_child],
    "GRI_VERBOTEN":
        [mark_error('Unechte griechische Buchstaben in griechischem Wort!', FEHLER_UNECHTE_BUCHSTABEN)],
    "DEU_GWORT, DEU_KOMPOSITUM, DEU_KOMP_GLIED, DEU_GROSS, DEU_KLEIN, LAT_WORT, GROSSSCHRIFT, GROSSBUCHSTABEN":
        [move_whitespace, collapse, change_name(TEXT)],
    "LAT_GWORT":
        [move_whitespace, collapse, apply_ifelse(
            change_name('Abkuerzung'), change_name(TEXT),
            lambda ctx: ctx[-1].content.find('|') >= 0)],
    "LEMMAWORT, LEMMAFRAGMENT": [move_whitespace, replace_by_single_child], # collapse],
    "TEXTELEMENT": [replace_by_single_child, move_whitespace, replace_by_single_child],
    "UNICODE": [evaluate_unicode],
    "SPEZIAL": [replace_by_single_child, move_whitespace],
    "homothetisch": [move_whitespace, reduce_single_child, replace_content_with('∻')],
    "EINZEILER, FREITEXT, MEHRZEILER, ETYMOLOGIE_TEXT":
        [strip(is_empty), flatten, move_whitespace,
         merge_adjacent(is_one_of({WSP}), 'L'), normalize_wsp_children,
         collapse_children_if(neg(any_of(is_one_of(TextAuszeichnungen),
                                         has_attr('xml'))), TEXT),
         replace_by_children],  # replace_by_single_child
    "ZITAT": [move_whitespace,
              apply_unless(collapse, has_child(
                  'BelegLemma', 'Zusatz', 'VariaLectioZusatz', 'FesterZusatz', 'Anker', 'Verweis', 'Abkuerzung',
                  'Einschub', 'opus_minus', 'Verweise', 'Sperrung', 'Junktur', 'Kursiv')),
              collapse_children_if(not_one_of(
                  'BelegLemma', 'Zusatz', 'VariaLectioZusatz', 'FesterZusatz', 'Anker', 'Verweis', 'Abkuerzung',
                  'Einschub', 'opus_minus', 'Verweise', 'Sperrung', 'Junktur', 'Kursiv'), 'TEXT'),
              apply_if(add_attributes({'sprache': 'deutsch'}), has_parent({"Bedeutungsangabe"}))],
    "ZITAT_ANFANG": replace_content_with("‘"),
    "ZITAT_ENDE": [move_whitespace, replace_content_with("’")],
                   # apply_ifelse(replace_content_with("’"), replace_content_with("’ "),
                   #              lambda ctx: not ctx[-1].content.endswith(' '))],
    "ABS": [replace_or_reduce],
    "LABS": [change_name('L'), replace_content_with(' ')],
    "SEM": [apply_if(add_attributes({'class': 'versteckt', 'versteckt': '1'}),
                     lambda trl: prev_path(trl)[-1].has_attr('versteckt')),
            replace_content_with(' '), change_name('L')],
    "KOMMA, SEMIKOLON": [move_whitespace, reduce_single_child, change_name('TEXT')],
    "ZW, ZWW, LÜCKE": [change_name(WSP), replace_content_with(' ')],
    "LL, ZLL": [replace_content_with(' '), change_name('L'), normalize_whitespace],
    "LZ": [change_name('L'), replace_content_with(' ')],
    "ZEILENSPRUNG": [replace_content_with('\n')],
    "KOMMENTARZEILEN": [],
    "_DATEI_ENDE": [],
    "HOCHGESTELLT, TIEFGESTELLT, SEITENZAHL, ROEM_GROSSBUCHSTABEN, ROEM_NORMAL":
        [flatten, move_whitespace, remove_tokens('_{', '^{', '}'), collapse, lstrip_str("^"), lstrip_str("_")],
    "ROEMISCHE_ZAHL":
        [move_whitespace, apply_if(
            (change_name('TEXT'), add_attributes({'roemische_zahl': 'ja'})),
            any_of(has_child('ROEM_GROSSBUCHSTABEN'),
                   has_ancestor({'Werk', 'Autor', 'Autorangabe', 'Stelle', 'Stellenangabe'},
                                until={'Einschub', 'opus_minus'}),
                   neg(has_ancestor({'BelegText', 'Autor'}, until={'Einschub', 'opus_minus'})))),
         collapse, lstrip_str("$")],
    "AUSGEWICHEN": [beseitige_Ausweichzeichen],
    "KLAMMER_AUF, KLAMMER_ZU, ECKIGE_AUF, ECKIGE_ZU":
        [flatten, move_whitespace, collapse, change_name('TEXT')],
    ":Text": [], # move_whitespace, reduce_single_child],
    ":Whitespace, L": [normalize_whitespace],
    ":RegExp": [normalize_whitespace],
    "WA_UNBEKANNT": [collapse, mark_error(
        'Unbekannte Wortart(en) in "{content}"! Bitte verwende eine der möglichen lateinischen '
        'Angaben oder Verdichtungen.', FEHLER_UNBEKANNTE_WORTART)],
    "ET_VERBOTEN":
        mark_error('Unerlaubtes Schlüsselwort "et"! Verwende bitte "vel"', FEHLER_FALSCHES_SCHLÜSSELWORT),
    "VERBOTENER_PUNKT":
        [mark_error('An dieser Stelle ist Punkt nicht erlaubt und auch nicht notwendig.', FEHLER_FALSCHES_TRENNZEICHEN)],
    "VERBOTENES_KOMMA":
        [mark_error('An dieser Stelle ist ein Komma nicht erlaubt und auch nicht notwendig.', FEHLER_FALSCHES_TRENNZEICHEN)],
    "FALSCHER_MARKER":
       [mark_error('Für die Markierung von Sekundärquellen muss das Dollarzeichen $ verwendet werden',
                   FEHLER_FALSCHES_SCHLÜSSELWORT)],
    # "ZOMBIE__": [],
    # ":RegExp": [change_name('TEXT')],
    "*": replace_by_single_child,
    ">": name_whitespace
}


def dbg_traverse(root, transformation_table):
    print(root.as_sxpr())
    traverse(root, transformation_table)


def MLWTransform() -> TransformerFunc:
    # return partial(dbg_traverse, transformation_table=MLW_AST_transformation_table.copy())
    return partial(transformer, transformation_table=MLW_AST_transformation_table.copy(),
                   src_stage='CST', dst_stage='AST')


get_transformer = ThreadLocalSingletonFactory(MLWTransform)


def transform_MLW(cst):
    get_transformer()(cst)


# ASTTransformation: Junction = create_junction(
#     MLW_AST_transformation_table, "cst", "ast", "transtable")

ASTTransformation = Junction('CST', get_transformer, 'AST')

#######################################################################
#
# COMPILER SECTION - Can be edited. Changes will be preserved.
#
#######################################################################

# befindet sich in MLWCompiler.py

#######################################################################
#
# END OF DHPARSER-SECTIONS
#
#######################################################################

import timeit

def benchmark_parser_file(path):
    parser = parsing.factory()
    assert path[-4:] == '.mlw'
    with open(path, 'r', encoding='utf-8') as f:
        data = f.read()
    root = None
    def call_parser():
        nonlocal root
        root = parser(data)
    time = timeit.timeit(call_parser, number=1)
    print(f'Parsing of "{path}" fnished in {time} seconds')
    if root.errors:
        for e in root.errors:
            print(e)
    return time


def benchmark_parser_dir(path):
    files = os.listdir(path)
    time = 0.0
    for fname in files:
        if fname[-4:].lower() == '.mlw':
            time += benchmark_parser_file(os.path.join(path, fname))
    print(f'Parsing of all files fnished in {time} seconds')
    return time

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("python MLWParser.py DATEINAME oder VERZEICHNISNAME")
    else:
        path = sys.argv[1]
        if os.path.isdir(path):
            benchmark_parser_dir(path)
        else:
            benchmark_parser_file(path)