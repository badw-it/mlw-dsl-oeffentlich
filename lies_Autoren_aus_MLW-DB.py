#!/usr/bin/env python3


"""lies_Autoren_aus_MLW-DB.py - Liest die opera majora und opera minora
Autoren aus der MLW-Datenbank auf dem Dienste-Server aus und schreibt die
Informationen in folgende Dateien:

1. MLWDaten.py - um die Daten dem MLW-Parser, -Compiler und Sprachserver
   bereit zu stellen.

2. mlw.tmLanguage.json - um die bekannten Autoren im SyntaxHighlighting
   von Visual Studio Code (und potentielle anderer Editoren, die sich für
   die syntaktische Hervorhebung einer TextMate-Grammatik bedienen)
   bereit zu stellen.

3. VERALTET: mlw.snippets.json - für die Autovervollständigung durch Visual Studio
   Code, bevor diese Funktion vom Language-Server übernommen wurde.
   Es wird (für den Notfall) immer noch eine Datei mlw.snippets.json

Damit aktualisierte Daten für diese Funktionen in der Eingabe-Umgebung der
Bearbeiterinnen und Bearbeiter erscheinen, müssen folgende Schritte
ausgeführt werden:

1. Dieses Skript (lies_Autoren_aus_MLW-DB.py) in einem git-Repository der
   MLW-DSL ausführen.
2. Die Änderungen mit "git commit" verabschieden und mit "git push" auf
   den gitlab-Server schieben.
3. Im Verzeichnis "Sync&Share/MLW (Eckhart Arnold)/MLW-Software" mit
   git pull die Software aktualisieren.
4. Auf dem Bearbeiter/innen-Rechner: Visual Studio Code starten und
   irgendeine MLW-Datei übersetzen (egal wie fehlerhaft) und
   Visual Studio Code noch einmal neu starten. Oder, alternativ, auf
   dem Bearbeiterinnen-Rechner MLWCompiler.py ausführen, um die
   mlw-Erweiterung für Visual Studio Code zu aktualisieren.
"""

import configparser
import datetime
import json
import os
import re
import requests
import sys
from typing import Tuple, Set, Union, Dict, List
import urllib.parse


## copyied from Alexander Häberlin's arachne.py-script

class ArachneTable(object):
    def __init__(self, tblName, url, token):
        self.tblName = tblName
        self.__token = token
        self.__url = url

    def describe(self):
        '''
            returns col names of current table
        '''
        first_row = self.getAll(limit=1)
        if len(first_row) != 1: raise ValueError(f"error while retrieving columns from table '{self.tblName}'")
        return list(first_row[0].keys())

    def version(self):
        '''
            returns dict:
                "max_date" => datetime stamp of update
                "length" => number of rows in table
        '''
        rows = self.getAll(select=["u_date"], order=["u_date"])
        return {
            "max_date": rows[len(rows) - 1]["u_date"],
            "length": len(rows)
        }

    def getAll(self, count=False, limit=0, offset=0, select=[], order=[], group=[]):
        return self.search([{"c": "id", "o": ">", "v": 0}], count, limit, offset, select, order, group)

    def get(self, query, count=False, limit=0, offset=0, select=[], order=[], group=[]):
        return self.search([{"c": item[0], "o": "=", "v": item[1]} for item in query.items()], count, limit, offset,
                           select, order, group)

    def search(self, query, count=False, limit=0, offset=0, select=[], order=[], group=[]):
        """
            searches database:
                query:  search query in form of a list of dictionaries [{"c": c, "o": "v": v}, {"c": c, "o": "v": v}, ...]
                        where c = column; o = operator (=, !=, >=, <=, LIKE); v = value (str, int)
                        dictionaries will be connected by AND
                count:  returns only number of results (=COUNT(*)); BOOL
                limit:  limits number of results; INT
                offset: offset, only works if limit is set; INT
                select: returns given columns i.e. ["work", "author"] if empty all columns will be returned; LIST
                order:  order results by list of columns; LIST
                group:  groups results by list of columns; LIST
        """
        if not isinstance(query, list): raise ValueError("query must be a list!")
        params = {"query": json.dumps(query)}
        if count: params["count"] = 1
        if limit > 0: params["limit"] = limit
        if offset > 0:
            if limit == 0:
                raise ValueError("cannot set offset without setting limit!")
            else:
                params["offset"] = offset
        if len(select) > 0: params["select"] = json.dumps(select)
        if len(order) > 0: params["order"] = json.dumps(order)
        if len(group) > 0: params["group"] = json.dumps(group)
        # url = f"{self.__url}/{self.tblName}?query={json.dumps(query)}"
        url = f"{self.__url}/data/{self.tblName}?" + urllib.parse.urlencode(params)
        print("url:", url)
        re = requests.get(url, headers={"Authorization": f"Bearer {self.__token}"})
        if re.status_code == 200:
            return re.json()
        else:
            raise ConnectionRefusedError("cannot connect to database!")

    def delete(self, rowId):
        url = f"{self.__url}/data/{self.tblName}/{rowId}"
        data = None;
        if isinstance(rowId, list):
            url = f"{self.__url}/data_batch/${self.tblName}"
            data = json.dumps(rowId);
        re = requests.delete(
            url,
            headers={
                "Authorization": f"Bearer {self.__token}",
                "Content-Type": "application/json",
            },
            json=data
        )
        if re.status_code == 200:
            return True
        else:
            raise ConnectionRefusedError("cannot connect to database!")

    def save(self, newValues):
        # newValues is an object containing col/values as key/value pairs.
        # when no id is given, a new entry will be created.
        # for batch saving: newValues = [{col: val}, {col. val}, ...]
        method = "POST"
        url = ""
        rId = 1
        if isinstance(newValues, list):
            url = f"{self.__url}/data_batch/${self.tblName}"
        else:
            url = f"{self.__url}/data/{self.tblName}"
            rId = newValues.get("id", None)
            if newValues.get("id", None):
                url += f"/{newValues['id']}"
                method = "PATCH"
                del newValues["id"]

        if method == "POST":
            re = requests.post(
                url,
                headers={
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {self.__token}"
                },
                json=newValues
            )
        elif method == "PATCH":
            re = requests.patch(
                url,
                headers={
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {self.__token}"
                },
                json=newValues
            )
        else:
            raise ValueError("unknown method.")

        if re.status_code == 201 and method == "POST":
            if isinstance(newValues, list):
                return rId
            else:
                return int(re.text)
        elif re.status_code == 200 and method == "PATCH":
            return rId
        elif isinstance(newValues, list):
            return True
        else:
            raise ConnectionRefusedError(f"cannot connect to database! status code: {re.status_code}")


class Arachne(object):
    def __init__(self, user, pw, url='https://dienste.badw.de:9999/mlw', tbls=None):
        self.__url = url
        re = requests.post(f"{self.__url}/session", json={"user": user, "password": pw})
        if re.status_code == 201:
            self.__token = re.text
        elif re.status_code == 401:
            raise PermissionError("Server is online but login credentials are refused!")
        else:
            raise ConnectionRefusedError(f"login failed. Check accessibility of url: {self.__url}")

        if tbls == None: raise ConnectionRefusedError("No tables specified! Check documentation for more information.")
        for tbl in tbls:
            setattr(self, tbl, ArachneTable(tbl, url, self.__token))

    def close(self):
        ''' call close() at the end of the session to delete active token from server.'''
        re = requests.delete(f"{self.__url}/session", headers={"Authorization": f"Bearer {self.__token}"})
        if (re.status_code == 200):
            del self.__token
        else:
            raise ConnectionRefusedError("logout failed. Are you already logged out?")

### end of copy


DEBUG = False

script_pfad = os.path.abspath(__file__)
mlwdsl_pfad = script_pfad[:script_pfad.find('MLW-DSL') + len('MLW-DSL')]
sys.path.append(mlwdsl_pfad)
sys.path.append(os.path.join(mlwdsl_pfad, 'DHParser'))

zugangsdaten_pfad = os.path.abspath(os.path.join('H:', 'Geheim', 'dMLW.ini'))
if not os.path.exists(zugangsdaten_pfad):
    zugangsdaten_pfad = os.path.abspath(os.path.join(os.path.expanduser('~'), 'Dokumente', 'Zugangsdaten', 'dMLW.ini'))
opera_majora_pfad = os.path.join(os.path.expanduser('~'), 'LRZ Sync+Share', 'MLW', 'MLW-Daten', 'MLW-opera_majora_NEU.xlsx')
schnipsel_pfad = os.path.join(mlwdsl_pfad, 'VSCode', 'mlwquelle', 'snippets', 'mlw.snippets.json_DISABLED')
python_schnipsel_pfad = os.path.join(mlwdsl_pfad, 'MLWSchnipsel.py')
tmLanguage_pfad = os.path.join(mlwdsl_pfad, 'VSCode', 'mlwquelle', 'syntaxes', 'mlw.tmLanguage.json')

SPALTEN = ['id', 'in_use', 'opus', 'example', 'work_abbr', 'work_abbr_sort',
           'work_full', 'author_id', 'author_abbr', 'author_full',
           'date_display', 'date_sort', 'date_type', 'txt_info',
           'bibliography', 'citation', 'is_maior', 'editions']

def export_json_data():
    global zugangsdaten_pfad
    print('Lese aktuelle Autor- und Werk-Daten aus der Werkdatenbank.')
    if not os.path.exists(zugangsdaten_pfad):
        zugangsdaten_pfad = zugangsdaten_pfad.replace('Dokumente', 'Documents')
    config = configparser.ConfigParser()
    if config.read(zugangsdaten_pfad):
        credentials = config['dMLW']
        user = credentials['user']
        password = credentials['password']
        URL = credentials['URL']
    else:
        print('Konnte Zugangsdaten nicht finden: ' + zugangsdaten_pfad)
        user = input('User: ')
        password = input('Passwort: ')
        URL = 'https://dienste.badw.de:9999/mlw'
    print('Hole JSON Daten von', URL, 'ab.')

    table = 'work'
    reader = Arachne(user, password, tbls=[table])
    print(f"\t\"{table}\" last updated:", getattr(reader, table).version()["max_date"])
    j_dump = getattr(reader, table).get({"in_use": 1}, order=["ac_vsc"])
    j_str = json.dumps(j_dump, ensure_ascii=False, indent=2)
    dateiname = 'opera_%s.json' % str(datetime.date.today())
    with open(dateiname, "w", encoding="utf-8") as f:
        f.write(j_str)
    reader.close()
    print('Aktuelle Autor- und Werk-Daten ausgelesen: ' + os.path.abspath(dateiname))


# def export_json_data():
#     print('Lese aktuelle Autor- und Werk-Daten aus der Werkdatenbank.')
#     config = configparser.ConfigParser()
#     if config.read(zugangsdaten_pfad):
#         credentials = config['dMLW']
#         user = credentials['user']
#         password = credentials['password']
#         URL = credentials['URL']
#     else:
#         print('Konnte Zugangsdaten nicht finden: ' + zugangsdaten_pfad)
#         user = input('User: ')
#         password = input('Passwort: ')
#         URL = 'https://dienste.badw.de:9999/session'
#         table = 'opera_view'
#     print('Hole JSON Daten von', URL, 'ab.')
#     reader = dMLW(user, password, URL)
#     response, j_dump = reader.load('opera_view', {'in_use': 1}, ['*'],
#                                    ['author_abbr', 'work_abbr_sort'])
#     j_str = json.dumps(j_dump, ensure_ascii=False, indent=2)
#     dateiname = 'opera_%s.json' % str(datetime.date.today())
#     with open(dateiname, 'w', encoding='utf-8') as f:
#         f.write(j_str)
#     print('Aktuelle Autor- und Werk-Daten ausgelesen: ' + os.path.abspath(dateiname))


RX_VERALTETE_ZITIERWEISE = re.compile(r'\s*\[[\w .]*\]')
RX_AUTOR = re.compile(r'[A-Z.]+(?:\s[A-Z][A-Z.]*(?= |$))*')


def strip_HTML(s: str) -> str:
    s = s.replace('&gt;', '>').replace('&lt;', '<')
    s = s.replace('<sup>', '^{').replace('</sup>', '}')
    re.sub(r'<.*?>', '', s)
    return s

HINWEIS_UND_LIZENZ = '''

ACHTUNG: Diese Datei wird automatisch von dem Skript:
'MLW-Software/lies_Autoren_aus_MLW-DB.py' generiert. Alle 
von Hand in diese Datei eingetragenen Änderungen werden überschrieben!


Author: Eckhart Arnold <arnold@badw.de>

Copyright 2020 Bavarian Academy of Sciences and Humanities


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""'''


COMPLETION_HEADER = '''#!/usr/bin/env python3

"""MLWSchnipsel.py - Editor-Vorschläge für Autoren und Werke.'''\
+ HINWEIS_UND_LIZENZ + '\n\n'

def convert_json_to_snippets() -> Tuple[Set[str], Set[str], Dict[str, Set[str]]]:
    opera_files = [f for f in os.listdir()
                   if f.startswith('opera_') and f.endswith('.json')]
    if not opera_files:
        print('Keine opera_DATUM.json Datei gefunden.')
        return

    opera_files.sort()
    with open(opera_files[-1], 'r', encoding='utf-8') as f:
        data = json.load(f)

    assert isinstance(data, list)
    # ambiguities = dict()
    snippets = dict()
    completions = []
    majora_autoren = {'ANON. I', 'ANON. II', 'ANON. III', 'ANON. IV', 'ANON. V', 'ANON. IV'
                      'ANON. VII', 'ANON. VIII', 'ANON. IX', 'ANON. X', 'ANON. XI', 'ANON. XII',
                      'ANON. XIII', 'ANON. XIV', 'ANON. XV', 'ANON. XVI'}
    minora_autoren = set()
    werke = dict()
    for entry in data:
        if entry.get('in_use', True):
            if entry.get('author_abbr', '') == '- - -':
                entry['author_abbr'] = ''
            if entry.get('work_abbr', '') == '- - -':
                entry['work_abbr'] = ''

            # autor = entry.get('author_abbr', '').upper()
            autor_werk = entry.get('ac_vsc', '')
            m = RX_AUTOR.match(autor_werk)
            autor = m.group(0) if m else ''
            if DEBUG and not autor:  print(autor_werk)
            if autor and autor != '- - -':
                if autor.rfind('(?)') >= 0:
                    autor = autor.replace('(?)', '').replace('[?]', '').strip()
                if autor.endswith(']'):
                    autor = RX_VERALTETE_ZITIERWEISE.sub('', autor)
                autor = autor.strip()
                if int(entry['is_maior']):
                    majora_autoren.add(autor)
                else:
                    minora_autoren.add(autor)
                werk = autor_werk[len(autor):].split(';')[0]
                werk = werk.replace('(?)', '').replace('[?]', '').strip()
                # werk = re.sub('\([^)]]*\)|\[[^\]]*\]', '', werk).strip()
                if werk == '- - -':  werk = ''
                # TODO: Interpretiere eckige Klammern als optionale Angaben
                werke.setdefault(autor, set()).add(werk)

            work_abbr = entry.get('abbr', entry.get('full', '') or '') or ''
            example = entry.get('ac_vsc', '')
            if not example.startswith(autor):
                print('KEIN AUTOR:', autor, ":", example)
            if example and example.find(';') >= 0:
                prefix = '* ' + example
                body = strip_HTML(prefix)
            else:
                example = strip_HTML(entry.get('ac_web', ''))
                if not example:
                    print('KEIN EDITOR-BEISPIEL', autor, work_abbr)
                    example = strip_HTML(entry.get('example', ''))
                prefix = '* ' + example
                citation = (entry.get('citation', '') or '')
                body = strip_HTML('%s; %s' % (prefix, citation))
            description = '%s %s %s\n\n%s %s %s\n\n%s' % (
                autor,
                entry.get('date_display', '') or '',
                entry.get('txt_info', '') or '',
                entry.get('full', '') or work_abbr,
                entry.get('author_display', '') or '',
                entry.get('date_display', '') or '',
                # entry.get('citation', 'KEINE ZITATION') or 'KEINE ZITATION',
                entry.get('bibliography', '') or ''
            )
            description = re.sub('  +', ' ', description)
            description = re.sub('\n\n\n+', '\n\n', description)
            if entry.get('editions', None):
                appendix = ['\n']
                editions = json.loads(entry['editions'])
                for edition in editions:
                    appendix.append('%s: %s' % (edition['label'], edition['url']))
                    # appendix.append('%s (%s): %s'
                    #                 % (edition['title'], edition['label'], edition['url']))
                description += '\n'.join(appendix)
            description = strip_HTML(description)

            key = "%s (%s) %s" % (autor, entry['id'], work_abbr)

            assert key not in snippets, 'Ups! Dieser Schlüssel kommt doppelt vor: ' + key

            snippets[key] = { 'body': body,
                              'description': description,
                              'prefix': prefix }
            completionItem = { 'label': body,
                               'kind': 15,
                               'documentation': description,
                               'textEdit': {
                                  'range': None,
                                  'newText': body }}
            completions.append(completionItem)
            completions.sort(key=lambda item: item['label'].upper())

    for a, w in list(werke.items()):
        if w == {''}:  werke[a] = frozenset()

    with open(schnipsel_pfad, 'w', encoding='utf-8') as f:
        json.dump(snippets, f, ensure_ascii=False, indent=2)

    def completion_repr(c):
        return f"{{'label': {repr(c['label'])}, 'kind': {repr(c['kind'])},"\
               f" 'textEdit': {repr(c['textEdit'])}}}"

    def compl_doc_repr(c):
        return f"  {repr(c['label'])}: {repr(c['documentation'])}"

    with open(python_schnipsel_pfad, 'w', encoding='utf-8') as f:
        f.write(COMPLETION_HEADER)
        f.write('completion_items = [\n')
        f.write(',\n'.join(completion_repr(item) for item in completions))
        f.write(']\n')
        f.write('completion_docs = {\n')
        f.write(',\n'.join(compl_doc_repr(item) for item in completions))
        f.write('}\n')

    print('Vorschlagsliste für Visual Studio Code aktualisiert: '
          + os.path.abspath(schnipsel_pfad))
    print('Vorschlagsliste für MLW-Sprachserver aktualisiert: '
          + os.path.abspath(python_schnipsel_pfad))

    return majora_autoren, minora_autoren, werke


DATA_HEADER = '''#!/usr/bin/env python3

"""MLWDaten.py - stellt zusätzliche Daten zum MLW bereit.

''' + HINWEIS_UND_LIZENZ


def schreibe_Autoren_ins_MLW_Datenmodul(majora_autoren: Set[str],
                                        minora_autoren: Set[str],
                                        werke: Dict[str, Set[str]],
                                        datenmodulname: str = ""):

    def pp(namen_menge: Set[str], indentation: int = 4) -> str:
        namen = list(namen_menge)
        namen.sort(key=str.upper)
        s = []
        zeile = []
        l = 0
        indent = '{'
        indentation_str = ' ' * indentation
        L = len(namen)
        for i, name in enumerate(namen):
            zeile.append(repr(name))
            l += len(zeile[-1])
            if l > 68:
                if i < L - 1:  zeile.append('')
                s.append(indent + ', '.join(zeile))
                indent = indentation_str
                zeile = []
                l = 0
        if zeile:  s.append(indent + ', '.join(zeile))
        if s:  s[-1] += '}'
        else:  s = ["empty"]
        return '\n'.join(s)

    for autor in list(werke.keys()):
        teile = autor.split(' ')
        if len(teile) > 1:
            werke.setdefault(teile[0], set())

    if not datenmodulname:
        datenmodulname = os.path.join(mlwdsl_pfad, 'MLWDaten.py')

    assert os.path.exists(datenmodulname) and os.path.isfile(datenmodulname)

    with open(datenmodulname, 'w', encoding='utf-8') as f:
        f.write(DATA_HEADER)
        f.write('\n\n')
        f.write('daten_vom = "%s"\n\n' % str(datetime.date.today()))
        f.write('empty = frozenset()\n')
        f.write('opera = {\n')
        for autor, werkliste in werke.items():
            f.write(f"'{autor}': {pp(werkliste)},\n")
        f.write('}\n\n')


    # def pp(namen_menge: Set[str]) -> str:
    #     namen = list(namen_menge)
    #     namen.sort()
    #     s = ['{']
    #     zeile = []
    #     l = 0
    #     for name in namen:
    #         zeile.append(repr(name))
    #         l += len(zeile[-1])
    #         if l > 68:
    #             zeile.append('')
    #             s.append('    ' + ', '.join(zeile))
    #             zeile = []
    #             l = 0
    #     s.append('    ' + ', '.join(zeile))
    #     s.append('}')
    #     return '\n'.join(s)
    #
    # if not datenmodulname:
    #     datenmodulname = os.path.join(mlwdsl_pfad, 'MLWDaten.py')
    #
    # assert os.path.exists(datenmodulname) and os.path.isfile(datenmodulname)
    #
    # autoren = majora_autoren | minora_autoren
    # for autor in frozenset(autoren):
    #     teile = autor.split(' ')
    #     if len(teile) > 1:
    #         autoren.add(teile[0])
    #
    # with open(datenmodulname, 'w', encoding='utf-8') as f:
    #     f.write(DATA_HEADER)
    #     f.write('\n\n')
    #     f.write('daten_vom = "%s"\n\n' % str(datetime.date.today()))
    #     f.write('opera_majora_autoren = ')
    #     f.write(pp(majora_autoren))
    #     f.write('\n\nopera_minora_autoren = ')
    #     f.write(pp(minora_autoren))
    #     f.write('\n\nopera_autornamen = ')
    #     f.write(pp(autoren))
    #     f.write('\n\n')

    print('Datenmodul aktualisiert: ' + os.path.abspath(datenmodulname))


def spalte(l: List[str], countdown: int=1, i: int=0) -> Dict[str, Union[List[str], Dict]]:
    """Sortiert alphabetisch und spaltet dann eine Liste von Zeichenketten
    ungefähr in der Mitte in zwei Hälften, so dass die Einträge der zweiten
    Hälfte auf mit einem anderen Buchstaben beginnen als die der ersten Hälfte.
    Zurückgegeben wird ein Verzeichnis mit zwei Einträgen, dass als Schlüssel
    Bereiche möglicher Anfangsbuchstaben der Zeichnketten der ersten bzw.
    der zweiten Liste enthält und als Werte Listen der jeweils um das
    erste Zeichen verkürzten Zeichenketten.
    `countdown`  gibt die Rekursionstiefe an.
    Beispiel:

        # >>> spalte(['AY', 'BX', 'CW', 'DV'])
        # {'[A-B]': ['AY', 'BX'], '[C-D]': ['CW', 'DV']}
    """
    def R(s: str) -> str:
        return s.replace('[', r'\[').replace(']', r'\]').replace(r'\b', 'A')
    if countdown <= 0:
        m = set(l)
        if '' in m:
            m.remove('')
            m.add(r'\b')
        l = list(m)
        # fülle zum Sortien auf, damit kürze Teilstrings auf längere folgen
        l.sort(key=lambda s: (s + chr(255) * 30)[:30])
        return l
    l.sort(key=lambda s: s[i])
    L = len(l)
    k = L // 2 + L % 2
    while 1 <= k < L and l[k-1][i] == l[k][i]:
        k -= 1
    c = '.' * i
    try:
        result = {
            f'{c}[{R(l[0][i])}-{R(l[k - 1][i])}]': spalte(l[:k], countdown - 1, i + 1),
            f'{c}[{R(l[k][i])}-{R(l[-1][i])}]': spalte(l[k:], countdown - 1, i + 1)
        }
    except IndexError:
        print(l, k, i)
        assert False
    return result


def erzeuge_autoren_regex(autoren_menge: set, tiefe: int = 2) -> str:
    """Erzeugt aus der übergebenen Menge von Autoren einen regulären Ausdruck, der
    auf alle Autorennamen passt."""
    def R(s: str) -> str:
        return s.replace(']', r'\]').replace('[', r'\[').replace('.', r'\.')
    def re_block(data: Union[Dict, List]) -> str:
        if isinstance(data, list):
            return '|'.join(R(autor) for autor in data)
        else:
            assert isinstance(data, dict)
            keys = list(data.keys())
            return f'(?:(?={keys[0]})(?:' + re_block(data[keys[0]]) + '))' + \
                   f'|(?:(?={keys[1]})(?:' + re_block(data[keys[1]]) + '))'
    autoren = list(autoren_menge)
    autoren.sort()
    return r'(?:\b|(?=\[))(?<![$])(?:' + re_block(spalte(autoren, tiefe)) + r')(?:\b|(?<=[\].]))'


def schreibe_autoren_syntax_hilighting(restr: str):
    with open(tmLanguage_pfad, 'r', encoding='utf-8') as f:
        tmLanguage = json.load(f)
    tmLanguage['repository']['opera_majora_author_name']['patterns'][0]['match'] = restr
    with open(tmLanguage_pfad, 'w', encoding='utf-8') as f:
        json.dump(tmLanguage, f, indent=4)
    print('Syntax Higlighting von Autorennamen in Visual Studio Code aktualisiert: '
          + tmLanguage_pfad)


def erhöhe_package_json_version(nur_testen=False):
    """Erhöht die Versionsnummer der VSCode/mlwquelle/package.json Datei."""

    def version_str_pos(json: str) -> Tuple[int, int]:
        i = json.find('"version"') + len('"version"')
        if i < 0:
            return -1, -1, -1
        i = json.find('"', i) + 1
        k = json.find('"', i)
        return i, k

    package_json_path = os.path.join(mlwdsl_pfad, 'VSCode', 'mlwquelle', 'package.json')
    with open(package_json_path, 'r', encoding='utf-8') as f:
        package_json_str = f.read()
    a, b = version_str_pos(package_json_str)
    version = list(int(part) for part in package_json_str[a:b].split('.'))
    version[-1] += 1
    version_str = '.'.join(str(part) for part in version)
    package_json_str = package_json_str[:a] + version_str + package_json_str[b:]
    if nur_testen:
        import json
        j = json.loads(package_json_str)
        print(json.dumps(j, indent=2))
    else:
        with open(package_json_path, 'w', encoding='utf-8') as f:
            f.write(package_json_str)


def main():
    if not DEBUG:
        export_json_data()
    majora_autoren, minora_autoren, werke = convert_json_to_snippets()
    SEPTUAGINTA = 'LXX'
    majora_autoren.add(SEPTUAGINTA)
    schreibe_Autoren_ins_MLW_Datenmodul(majora_autoren, minora_autoren, werke)
    om_re = erzeuge_autoren_regex(majora_autoren|minora_autoren)
    schreibe_autoren_syntax_hilighting(om_re)
    erhöhe_package_json_version()


if __name__ == "__main__":
    main()
    # erhöhe_package_json_version(nur_testen=True)
