#!/bin/sh

"""generiere_Buchstaben.py - unicode-Bereiche für bestimmte Buchstabenmengen

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2018 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import sys
from typing import List, Tuple
import unicodedata


Letter = Tuple[str, int]
Range = Tuple[int, int]


def uc_name(i: int) -> str:
    try:
        return unicodedata.name(chr(i))
    except ValueError:
        return ''


def alle_Buchstaben(kennungen = ["GREEK"]) -> List[Letter]:
    """
    Liefert eine Liste aller Buchstaben der durch die Kennung angegebenen
    Sprache samt ihrer unicodes.
    """
    if isinstance(kennungen, str):
        kennungen = [kennungen]
    def prüfe(i):
        name = uc_name(i)
        return all(kennung in name for kennung in kennungen)
    buchstaben = [(chr(i), i) for i in range(0x10FFFF) if prüfe(i)]
    return buchstaben


def extrahiere_Bereiche(buchstaben: List[Letter]) -> List[Range]:
    """
    Liefert für eine gegebene Buchstabenliste eine Liste zusammenhängender
    unicode-Bereiche zurück, die die Buchstabenliste abdecken.
    """
    bereiche = []
    i = 0
    while buchstaben:
        anfang = buchstaben[0][1]
        ende = anfang
        buchstaben = buchstaben[1:]
        while buchstaben and buchstaben[0][1] == ende + 1:
            ende = buchstaben[0][1]
            buchstaben = buchstaben[1:]
        bereiche.append((anfang, ende))
    return bereiche


def bytesize(number: int) -> int:
    assert number >= 0 and number <= 2**32 - 1
    return 1 if number < 256 else 2 if number < 65536 else 4


def unicode_str(code: int, länge: int=0) -> str:
    """
    Liefert eine für reguläre Ausdrücke geeignete Zeichenkettenrepräsentation
    des (uni-)codes zurück.
    >>> unicode_str(ord('γ'))
    '\\\\u03B3'
    >>> unicode_str(0x10141)
    '\\\\U00010141'
    """
    if länge == 0:
        länge = 1 if code < 256 else 2 if code < 65536 else 4
    if länge == 1:
        assert code < 256
        return r'\x{:0>2}'.format(hex(code)[2:].upper())
    elif länge == 2:
        assert code < 65536
        return r'\u{:0>4}'.format(hex(code)[2:].upper())
    else:
        assert länge == 4, \
            "Illegale Längenangabe {} sollte 1, 2 order 4 sein!".format(länge)
        return r'\U{:0>8}'.format(hex(code)[2:].upper())


def als_re(bereiche: List[Range]) -> str:
    """
    Liefert einen regulären Ausdruck für den gegebenen Buchstabenbereich zurück.
    """
    def ucode_bereich(anfang, ende):
        länge = max(bytesize(anfang), bytesize(ende))
        if ende > anfang:
            return '[{}-{}]'.format(unicode_str(anfang, länge), unicode_str(ende, länge))
        else:
            assert anfang == ende
            return unicode_str(anfang, länge)
    return '|'.join(ucode_bereich(b[0], b[1]) for b in bereiche)


# def unicode_griechisch():
#     griechisch = griechische_Buchstaben()
#     print("Anzahl griechischer Zeichen: ", len(griechisch))
#     # print(''.join(c for c, i in griechisch))
#     bereiche = extrahiere_Bereiche(griechisch)
#     print("Anzahl griechischer Zeichenbereiche: ", len(bereiche))
#     print(als_re(bereiche))


def unicode_von(kennungen = ['GREEK']):
    if isinstance(kennungen, str):
        kennungen = [kennungen]
    print('Suche Buchstaben für %s im unicode-Zeichenbereich:' % str(kennungen))
    sprache = alle_Buchstaben(kennungen)
    kennungen_str = ' '.join(kennungen)
    print("Anzahl Zeichen von %s: " % kennungen_str, len(sprache))
    print(''.join(c for c, i in sprache))
    # for c, i in sprache:
    #     print(c, i, uc_name(i))
    bereiche = extrahiere_Bereiche(sprache)
    print("Anzahl Zeichenbereiche von %a: " % kennungen_str, len(bereiche))
    print(als_re(bereiche))
    print()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        unicode_von(sys.argv[1])
    else:
        unicode_von('GREEK')
        unicode_von('LATIN')
        unicode_von(['LATIN', 'CAPITAL'])
        unicode_von(['LATIN', 'SMALL'])
