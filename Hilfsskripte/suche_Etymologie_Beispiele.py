#!/usr/bin/env python3

"""suche_Werktitel_Beispiele.py - sucht aus allen Beispielen
die Werkangaben heraus, die runde Klammern enthalten."""

import os
import re
import sys

script_pfad = os.path.abspath(__file__)
mlwdsl_pfad = script_pfad[:script_pfad.find('MLW-DSL') + len('MLW-DSL')]
# beispiele_pfad = os.path.join(mlwdsl_pfad, "Beispiele")
beispiele_pfad = os.path.join(os.path.expanduser('~'), 'LRZ Sync+Share')

sys.path.append(os.path.join(mlwdsl_pfad, 'DHParser'))

def source_files():
    print(beispiele_pfad)
    for root, dirs, files in os.walk(beispiele_pfad):
        if root.find('MLW') >= 0:
            for f in files:
                if f.lower().endswith('.mlw'):
                    yield os.path.join(root, f)


def read_file(fname):
    with open(fname, 'r', encoding='utf-8') as f:
        text = f.read()
    return text

RX_ETYMOLOGIE_ENDE = re.compile(r'\n[A-Z][A-Z][A-Z]')

def analysiere_daten(text):
    i = text.find('ETYMOLOGIE')
    if i >= 0:
        m = next(RX_ETYMOLOGIE_ENDE.finditer(text, i))
        assert m
        k = m.start()
        print(text[i:k], '\n')
        return text[i:k].rstrip()
    return ''


if __name__ == "__main__":
    beispiele = []
    n = 0
    for f in source_files():
        text = read_file(f)
        beispiel = analysiere_daten(text)
        if beispiel:
            print(f + ':')
            n += 1
            s = '\n\n; %s\n' % f \
                + 'M%i: """\n    ' % n + '\n    '.join(beispiel.split('\n')) + ' """'
            beispiele.append(s)
    print(''.join(beispiele))






