# md_gliederung.nim:
# Fügt eine Nummerierung zu den Überschriften einer markdown-Datei 
# hinzu. Vorerst nur Überschriften im "#"-Stil...
#
# Dieses Skript ist in der Sprache "nim" geschrieben:
# https://nim-lang.org


import strutils


const
    erste_zähl_ebene: int = 2

var 
    md: string = read_file("MLW_Notation_Beschreibung.md")
    lines: seq[string] = md.split("\n")
    sections: array[1..6, int] = [0, 0, 0, 0, 0, 0]

write_file("MLW_Notation_Beschreibung.md.Sicherung", md)

for i, line in lines.pairs:
    if line.high > 0 and line[0] == '#':
        
        # Bestimmte Gliederungsebene
        var k: int = 0
        while k <= line.high and line[k] == '#':
            k += 1

        var gliederungsmarke: string = line[0 .. k]

        # Erhöhe Zähler dieser Gliederungsebene
        # und setze alle folgenden Zähler zurück

        sections[k] += 1
        for l in countup(k + 1, sections.high):
            sections[l] = 0
        
        # Stelle fest, wo die eigentliche Überschrift anfängt
        # und sichere die Überschrift
        while k <= line.high and line[k] == ' ':
            k += 1
        while k <= line.high and line[k] in {'0', '1' .. '9', '.'}:
            k += 1
        while k <= line.high and line[k] == ' ':
            k += 1
        var überschrift: string
        if k <= line.high:
            überschrift = line[k .. line.high]
        else:
            überschrift = ""

        # Erzeuge Nummerierungszeichenkette
        k = erste_zähl_ebene
        var zähler: string = ""
        while k <= sections.high and sections[k] > 0:
            if zähler != "":
                zähler &= "." & $sections[k]
            else:
                zähler = $sections[k]
            k += 1
        zähler &= " "

        lines[i] = gliederungsmarke & zähler & überschrift

var enriched: string = lines.join("\n")
write_file("MLW_Notation_Beschreibung.md", enriched)

echo "fertig."
