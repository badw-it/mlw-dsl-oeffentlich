#!/usr/bin/env python3

"""suche_Werktitel_Beispiele.py - sucht aus allen Beispielen
die Werkangaben heraus, die runde Klammern enthalten."""

import os
import sys

script_pfad = os.path.abspath(__file__)
mlwdsl_pfad = script_pfad[:script_pfad.find('MLW-DSL') + len('MLW-DSL')]
beispiele_pfad = os.path.join(mlwdsl_pfad, "Beispiele")

sys.path.append(os.path.join(mlwdsl_pfad, 'DHParser'))

from DHParser.syntaxtree import parse_xml, flatten_sxpr


def data_files():
    for root, dirs, files in os.walk(beispiele_pfad):
        if root.rstrip('/').endswith('XML-Daten'):
            for f in files:
                if f.lower().endswith('.xml'):
                    yield os.path.join(root, f)


def read_file(fname):
    with open(fname, 'r', encoding='utf-8') as f:
        xml = f.read()
    tree = parse_xml(xml)
    return tree

def analysiere_daten(tree):
    for node in tree.select('Werk'):
        if node.content.find('(') >= 0:
            print('    ' + flatten_sxpr(node.as_sxpr()))


if __name__ == "__main__":
    for f in data_files():
        tree = read_file(f)
        print(f + ':')
        analysiere_daten(tree)



