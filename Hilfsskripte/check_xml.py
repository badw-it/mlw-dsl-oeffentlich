#!/usr/bin/env python3

"""check_xml.py - Prüft XML-Dateien auf Gültigkeit (keine Schema-Validierung,
lediglich XML-Konformität mittels Einlesen mit ElementTree.

Nutzung:
    check_xml.py  DATEINAME.xml
    check_xml.py  VERZEICHNIS
    check_xml.py
"""

import sys
import os
import xml.etree.ElementTree as ET


def check(path):
    print('Prüfe:', path)
    try:
        tree = ET.parse(path)
        root = tree.getroot()
    except ET.ParseError as err:
        print('    XML-ERROR:', err)

def xml_files(path):
    if os.path.isdir(path):
        save = os.getcwd()
        files = os.listdir(path)
        os.chdir(path)
        for fname in files:
            for xmlfile in xml_files(fname):
                yield xmlfile
        os.chdir(save)
    elif path.lower().endswith('.xml') and os.path.isfile(path):
        yield path

if __name__ == "__main__":
    if len(sys.argv) > 1:
        name = sys.argv[1]
        for file in xml_files(name):
            check(file)
    else:
        print(__doc__)



